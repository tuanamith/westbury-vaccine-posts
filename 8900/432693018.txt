


Trying to Solve Autism's Mysteries 
Autism in America: Seeking Solutions to Autism's Elusive Questions By JOSEPH BROWNSTEIN ABC News Medical Unit Nov. 5, 2008 

When Preston Brown was a young child, doctors diagnosed him with autism. Now they say he doesn't have it anymore. 
"He's very aware of his surroundings," said his mother, Jamie Brown, who added that the changes in recent months have been drastic and that doctors have dropped his autism diagnosis. 
"He communicates, he has favorites, he has opinions, he has humor," she said. "He makes friends, he is learning at warp speed, he's intelligent. He's happy, he's not sick anymore." 
Preston's case is among the mysteries that currently surround autism. While publicity for the condition has brought money and researchers into the area, autism has many unanswered or partially answered questions. 
Hear Jamie Brown tell her story. 
For example, when autism is considered a lifelong condition, why are some children, like Preston, "cured"? What causes the disease in the first place? And why, in recent years, has the number of children diagnosed with autism climbed so rapidly? 
Dr. Dmitriy Niyazov is trying to solve many of those riddles as head of pediatric medical genetics at Ochsner Health Services in New Orleans. 
"I want to give [parents] answers," Niyazov said. "I can't give them all answers, but I'd like to give them some of them." 
According to Niyazov, the increase in autism diagnoses stems from a combination of overdiagnosis and a desire to label an illness before discovering the cause. However, medicine does not have a clear answer for the increase in autism cases. 
Niyazov describes the case of a child who doesn't maintain eye contact and bangs his head; a psychologist will typically diagnose the case as autism and refer the parents to a specialist. But in this case, autism is "not a diagnosis, that's just a description," said Niyazov. 
Autism is like many chronic diseases in that the symptoms are similar, but the causes vary. A few genetic diseases can give children autistic symptoms for a period of time, while most other causes of autism lead to the lifelong condition with which most autistic people live. 
Click Here to Visit the OnCall+ Autism Center and Get Answers From Top Autism Experts 
In other words, a dropped diagnosis is not the same as a cure, and it doesn't happen in most children because the origins of their autism are different. 

Genetic Testing for Autism Genetic testing can sometimes explain the course that autism will take in a child, but even in cases where that cause doesn't mean a cure, it can be reassuring to the parents. "Parents need to be the ones that are empowered by this," said Niyazov. 
Susan Bordelon of New Orleans said she needed that reassurance after her son Clarke, now 14, was diagnosed with autism five years ago. 
A volunteer for the Shots for Tots program, which encourages vaccinations for children, Bordelon said she worried that vaccines had caused it. But after taking her son to see Niyazov, she received a different perspective. 
"He filled in a missing puzzle piece," said Bordelon. "When he said that, did you know your son's autism could be a symptom ... of another problem, I was like, no, his main diagnosis is autism. I just never knew that it had a genetic cause." 
After running lab tests, they learned that some of Clarke's genes had an extra copy -- part of chromosome 16 had three copies, instead of the usual two. With that knowledge, Bordelon knew that while the disease originated in Clarke's genes, it was not something she could have controlled. 
"Now, I feel... it wasn't something I did. It's more a problem after fertilization, when he started dividing, that chromosome made three instead of just two," said Bordelon. "I wish it had been available earlier, that we would have known." 
Doctors also note that by knowing a child has autism earlier, intervention to aid in behavior can have greater effects. As genetics researcher will frequently note, genes are not destiny. 

Parents, Doctors Still Searching for Answers Bordelon is one of many parents benefiting from the increase in autism research of recent years. And while many researchers are looking at genetic origins -- which in some cases are known -- others are looking at the less developed area of environmental causes for autism. 
"We think autism has a very strong genetic component. To what extent environmental factors influence autism is not fully understood," said Dr. Walter E. Kaufmann, director of the Center for Genetic Disorders of Cognition and Behavior at Kennedy Krieger Institute in Baltimore. 

Environmental Causes of Autism? While acknowledging the lack of evidence for environmental causes, he believes that may come with time. "That's not because environment doesn't play a role, but because we don't have the data yet," Kaufmann said. "It could be environmental factors, but we don't know that yet." 
He estimates that doctors can determine the origin of between 30 percent and 50 percent of all autism cases. But he emphasizes that is not the same thing as knowing the cause. 
"The problem has been, and continues to be ... there is no one cause that anyone has isolated for autism," said Katherine Loveland, a psychologist who is director of the Center for Human Development Research at the University of Texas - Houston Medical School. 
That cause -- the pathway the proteins take from the gene's orders to causing the disease -- may be the key to a cure. 

Where Does This Go From Here? Because of how complex autism is as a disease, breakthroughs may be piecemeal. 
"We're not going to find a single cause and we're not going to find a single cure," said Loveland. 
But at the same time, Loveland and other researchers are optimistic that with the influx of money and minds to the field of autism research, those advances will come. 
"I'm confident that we're going to make some breakthroughs in autism in the next 10 or 12 years," she said. 
In recent months, research has given even more clues to autism's roots. In July, Harvard researchers published a study linking specific genes to processes in the brain that control learning and memory. Studies like this may give researchers better clues into how autism operates and insight into how to counteract it. 
And that sense of hope among researchers is also felt among parents. 
Read/Watch the Entire Series: Autism in America 

Autism Voices and Views In Search of the Elusive 'Autism Answer' How Families Cope After Diagnosis The Mysteries of Autism Not Seeing Social Cues a Danger of Autism Hope for the Future 
"I think it's exciting because I think when they find out more, they'll know how to help my son better," said Bordelon. 
 <URL>  

