


Over 1.7b people in India, Africa do not get essential drugs PTI/UNB, New Delhi 
Over 1.7 billion people, mostly concentrated in India and Africa, do not have access to essential drugs and the situation is worsening with the dwindling public healthcare spending in the developing countries. "More than 1.7 billion people in India and Africa do not have access to essential drugs... across the world, about 14 million people die due to infectious diseases and 10 million children die due to vaccine preventable diseases," K Satyanarayana, Head of the Intellectual Property Rights Unit, Indian Council of Medical Research (I.C.M.R.) told PTI. However, India has one of the best drug procurement systems in the world especially in states like Tamil Nadu and Delhi, Satyanarayana said quoting a W.T.O. report. "These should be replicated in other states as well," he said. The pharma industry, he said, should play vital role in making healthcare affordable. "The pharma industries can form strategic partnership with the government to bring out drugs which poor people can afford." "For chronic diseases, there are reports of the industry willing to consider dual pricing so that poor people can access drugs for cancer, diabetes, cardiovascular diseases etc," he said. Satyanarayana said the Indian Patent Office should not hesitate to invoke provisions of compulsory licensing and flexibilities available under Agreement on Trade Related Aspects of Intellectual Property Rights (T.R.I.P.S.) and India Patent Act whenever the drugs required by poor remain out of reach. 

"Torpedo" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... preventable Unit, quoting 



