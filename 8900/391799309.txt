


 <URL>  

<QUOTE ON>---------------------------------------- 
John Simkin 
Yesterday [June 24, 2008], 06:04 PM 
Robin Ramsay has reviewed Dr. Mary's Monkey in Lobster (Summer, 2008) 
[QUOTE ON] 
The Kennedy assassination literature has produced some oddities over the  years but this takes the biscuit. A sense of this is conveyed by what must  be one of the longest subtitles in publishing history: 
"How the unsolved Murder of' a doctor, a secret laboratory in New Orleans  and cancer-causing monkey viruses are linked to Lee Harvey Oswald, the JFK  assassination and emerging global epidemics" 
Kennedy assassination initiates will glimpse a little bit of the story  from that subtitle. Cancer and New Orleans? Wasn't David Ferric keeping  thousands of mice in his apartment? Yes, he was. And he was, apparently,  doing cancer research on the mice. In his apartment when lie died was  found an anonymous treatise on cancer. (Haslam thinks he has identified  the author.) Skip to page 329 and the author provides a handy summary of  his story so far: 
"In the morning, the young cancer-researcher rides the bus to work with  the "detector" who is about to be accused of assassinating the President.  In the afternoon, she goes to the underground medical laboratory run by a  known Mafia asset to develop a biological weapon. In between the two, she  works at a cover-job under the supervision of an ex-FBI agent, who sends  her on errands to deliver "envelopes" to the office of the Congressman who  chairs the House Committee on Un- American Activities." 
The "young cancer researcher" is Judyth Vary Baker who has claimed for  years to have been the lover of Lee Harvey Oswald. The underground lab is  "known Mafia asset" David Ferrie's mouse research where, says Baker, they  were trying to develop a rapid-acting cancer with which to kill Fidel  Castro (which is just - within the extant parameters of the attempts to  kill him, not much crazier than some of the CIA's other wheezes). The  "cover-job" was at the Reilly Coffee Company " where Lee Harvey Oswald  also had a "cover-job". 
A surprising amount of this is sort of stood up by Haslam but an awful lot  of it hangs on the story of Baker, whose status in the JFK world is  ambiguous at best; and there are a great many connecting suppositions  between the bits Haslam has stood up and the wider thesis. This involves:  the unsolved and very strange death (murder? freak accident?) of another  cancer expert, Dr Mary Sherman; a particle accelerator at a nearby  university lab which may or may not have been the cause of Sherman's death  and which may or may not have been used to modify viruses; not to mention  the final layer of the cake, the strange tale of the monkey-viruses in the  polio vaccines and their possible links to the epidemic of soft tissue  cancers in America. 
Is this enormous thesis linking JFK's death to a rise in cancer America  credible? No, it isn't. There are just too many places in the story where  guesswork takes the place of evidence. But oddly fascinating this  profusely illustrated account certainly is. 

<QUOTE OFF>--------------------------------------- 

Dave 


