


ALLIANCE FOR HUMAN RESEARCH PROTECTION Promoting Openness, Full Disclosure, and Accountability  <URL>  and  <URL>  
FYI 
The Associated Press / USA Today report in testimony before the House Science and Technology subcommittee, Christopher De Rosa, a top scientist at the Centers for Disease Control and Prevention's toxic substances agency, testified said his bosses ignored pleas to alert Gulf Coast hurricane victims about formaldehyde dangers in government-issued trailers.  His bosses, he testified, told him last year not to write e-mails about the potentially widespread health problems-- because his warnings of a "pending public health catastrophe" could be misinterpreted if publicly released.  
Formaldehyde can cause respiratory problems and has been classified as a carcinogen by the International Agency for Research on Cancer and a probable carcinogen by the U.S. Environmental Protection Agency. Yet, CDC and FEMA officials exposed unwitting Katrina victims to the dangers and ordered a responsible scientist to be silent. 
It's time for a clean sweep and rid federal agencies of officials who betray the trust of the American people. Public officials who deliberately conceal health hazards linked to government approved drugs, vaccines, medical devices, and even trailors issued to hurricane victims, should be ousted, if not criminally indicted. 
Contact: Vera Hassner Sharav  <EMAILADDRESS>  212-595-8974 
 <URL>   Scientist: CDC bosses ignored warnings WASHINGTON (AP) - A federal scientist said Tuesday his bosses ignored pleas to alert Gulf Coast hurricane victims about formaldehyde dangers in government-issued trailers and told him last year not to write e-mails about his warnings of potentially widespread health problems. 
Christopher De Rosa, a top scientist at the Centers for Disease Control and Prevention's toxic substances agency, said his bosses told him that his warnings of a "pending public health catastrophe" could be misinterpreted if publicly released. 
De Rosa's comments came Tuesday at a House Science and Technology subcommittee hearing on how the CDC and other agencies handled complaints about potentially high levels of formaldehyde in trailers issued by the Federal Emergency Management Agency to victims of hurricanes Katrina and Rita. 
Committee Democrats have accused FEMA of manipulating scientific research to play down the dangers of high levels of formaldehyde found in the trailers. They say the CDC and its Agency for Toxic Substances and Disease Registry went along with misleading residents. 
In mid-2006, FEMA enlisted the CDC's help in analyzing the results of air-quality tests on unoccupied trailers. But the CDC didn't start testing the air quality in occupied FEMA trailers - or study the possible health effects of long-term formaldehyde exposure - until late last year. 
The CDC said in February that tests on hundreds of occupied FEMA trailers and mobile homes found formaldehyde levels that were, on average, about five times higher than what people are exposed to in most modern homes. The results prompted FEMA to step up efforts to move roughly 35,000 families still living in the trailers after the 2005 hurricanes Katrina and Rita. 
Formaldehyde can cause respiratory problems and has been classified as a carcinogen by the International Agency for Research on Cancer and a probable carcinogen by the U.S. Environmental Protection Agency. 
FEMA officials say the number of occupied trailers on the Gulf Coast, which peaked at more than 143,000 after the hurricanes, has dropped to about 34,000 as FEMA rushes to move people into safer housing. 
Copyright 2008 The Associated Press. All rights reserved. This material may not be published, broadcast, rewritten or redistributed.   
_______________________________________________ Infomail1 mailing list to unsubscribe send a message to  <EMAILADDRESS>  

