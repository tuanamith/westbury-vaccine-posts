


Job Title:     Site Compliance Inspector Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-04-25 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Site Compliance Inspector &#150; QUA001658 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Please note, this position will be filled at one of several levels, depending on the qualifications of the chosen candidate: * Site Compliance Inspector * Senior Site Compliance Inspector * Site Compliance Specialist   Information on the Senior Site Compliance Inspector and Site Compliance Specialist positions may be found in requisition number QUA001659. 
The Site Compliance Inspector performs Shop Floor Presence Inspections and ongoing compliance monitoring activities in West Point Operations, ensuring that all observations area addressed and remediated. Evaluates manufacturing/packaging/laboratory facilities to assure compliance with internal, FDA and other regulatory agency requirements. Additional responsibilities include, but are not limited to: * Reviewing departmental standard operating procedures, and other formal writtten procedures for accuracy, consistency, and compliance with cGMPs and regulatory commitments. * Providing support for regulatory inspections by preparing daily minutes during the inspection, assisting the regulatory inspectors with obtaining required information, coordinating the preparation of responses, and reviewing commitment closeout a major in Biology, Microbiology, Biochemistry, Virology, or Engineering is preferred. 1-2 years of broad based quality control and/or quality assurance audit/inspection experience, or other experience in a variety of manufacturing, packaging, testing, or techinical support areas, is required. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001658. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  04/23/2008, 06:57 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/30/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-395156 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



