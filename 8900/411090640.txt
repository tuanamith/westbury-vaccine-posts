


Job Title:     Quality Associate - GMP Quality Commercialization and... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-08-16 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Quality Associate - GMP Quality Commercialization and Early Development &#150; QUA001723 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career.  Under supervision or as part of a team, supporting Merck Research Labs clinical material, the Quality Associate will:  * Perform audits and inspections of assigned contractors, areas or systems to assess compliance with regulatory and Merck Standards.  * Issue reports summarizing deficiencies and work with areas to execute remedial actions, notify appropriate management of inspection results. * Audit and approve GMP margin-BOTTOM: 0px"Travel to the contractor sites is required for this position; this travel could include Developing World Countries and generally ranges from a few days to a week. 
Please note, relocation assistance is not provided for this position. 
Qualifications 
A B.S. and/or M.S. in an appropriate science or engineering discipline is required for consideration for this position. Other fields will be considered if accompanied by appropriate work experience. A minimum of 3 years of relevant experience is required with a B.S., and a minimum of 1 year of relevant experience is required with an M.S. Such experience must be in the pharmaceutical/chemical industry within one or more of the following areas: * product development; * technical service; * manufacturing or packaging operations; * an analytical function; * or a quality function. A working knowledge of cGMP regulations and demonstrated analytical, problem-solving, and communication (oral/written/interpersonal) skills are required.  A working knowledge of compendial regulations and effective writing and verbal communication skills are necessary.  Flexibility to travel as described above, including to Developing World Countries, is strongly preferred. Auditing experience, experience in quality control/quality assurance, or engineering experience for Mercks manufacturing facilities is preferred. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001723. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
 Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  Yes, 25 % of the Time  
Additional Information   Posting Date  08/13/2008, 11:06 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  08/27/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-418516 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



