


Job Title:     Principle Validation Scientist / Associate Director /... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-06-18 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Principle Validation Scientist / Associate Director / Director &#150; ENG001642 
Job Description  
Submit 
Qualifications Demonstrated leadership, project management, communication and teamwork skills required. Minimum 5 years biologics process validation experience required. Experience with matrix organizations preferred. Strong knowledge of GMPs and QA preferred. Experience with new facility qualifications preferred. Experience preparing regulatory submissions and managing interactions during regulatory inspections is preferred. * BS/MS/PhD required in Chemical, Biochemical, or Biomedical Engineering or Chemistry, Biology, Biochemistry, or Molecular Biology, or other comparable Life or Health Science field. * Ph.D. with minimum of 5 years biologics process validation experience preferred or MS/BS with comparable overall experience will be considered.  * Demonstrated leadership, communication and teamwork skills required.  * Minimum 5 years biologics process validation experience required.  * Experience with matrix organizations preferred. Strong knowledge of GMPs and QA preferred.  * Experience preparing regulatory submissions and managing interactions during regulatory inspections is preferred. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #ENG001642. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  06/14/2008, 11:12 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  07/14/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-402336 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



