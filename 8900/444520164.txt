


On Dec 19, 8:09 pm, "Jan Drew" < <EMAILADDRESS> > wrote: an =3D=3D=3D=3D=3D=3D=3D=3D=3D 
Your sources show strong biases against medicine, which undermines the credibility of their reports because they are pushing their own agenda: 
The Center for Medical Consumers, a non profit 501(c) 3 advocacy organization, was founded in 1976 with ambitious goals. One was to provide access to accurate, science-based information so that consumers could participate more meaningfully in medical decisions that often have profound effects upon their health.  Another was to hold medicine more accountable by revealing that much of the treatment advice proffered by doctors and other health professionals is based on little or no evidence of safety and effectiveness. 
Dr. Martin holds a science certification from the University of South Dakota School Of Medicine, as well as being board certified in Anti- Aging Medicine through the American Academy of Anti-Aging Medicine. (A.B.A.A.H.P.) Dr. Martin is double board certified as a Clinical Nutritionist (C.C.N.) through both the International and American Associations of Clinical Nutritionists and as a Diplomate of the American Clinical Board of Nutrition (D.A.C.B.N). Additionally, Dr. Martin is a board certified Chiropractic Physician and Physiotherapist in the states of Arizona, Colorado and Kentucky. He is certified in Applied Kinesiology and has training in the fields of medicine, acupuncture, herbology, sports medicine, and exercise physiology. 
The CBC story was more balanced than you suggest, judging from these excerpts: 
"There isn't enough evidence that the flu vaccine is effective to support public programs advocating widespread use of flu shots, a controversial vaccine epidemiologist suggests." 
... 
"We certainly believe, looking at all of the evidence, that for the elderly =97 who are at high risk =97 even if the vaccine effectiveness is lower [than in healthy adults], it's still worth it," said Tam, who directs the agency's immunization and respiratory infections division." 
... 
"That the BMJ would consider publishing an editorial from somebody saying that we should be thinking about not vaccinating at-risk people for influenza is a really bad thing," said Dr. Allison McGeer, an infectious diseases specialist at Toronto's Mount Sinai Hospital." 
... 
"How can you say that we don't have a safety record when we give 20 million vaccinations a year?" she asked with exasperation in her voice." 
... 
"Flu shot advocates do admit the vaccine isn't as effective as they would like. 
Efficacy varies from year to year, depending on how well matched the strains in the shot are to those circulating and causing disease. And the amount of protection depends on age =97 the shots provoke a better immune response in healthy adults than they do among the elderly, whose immune systems are on the wane. 
"Even a vaccine that is not as effective as we would like could still have a substantial benefit," said Tam, who noted that as far as the agency is concerned, there is enough evidence to conclude vaccinating people against the flu lowers the annual toll of the disease." 
Conducting the types of trials Jefferson advocates wouldn't be easy, given that the amount of flu activity varies from year to year, the vaccine isn't always a perfect match for circulating strains and different ages respond differently to the shots." 
.... 
My take: 
Jefferson, the researcher with the controversial stance, is a strong proponent of evidence-based medicine, which is great on its face. However, proposing double-blind studies in which an entire group of at- risk elderly patients receive a placebo instead of the flu vaccine would be unethical. 

