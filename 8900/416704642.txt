


On Sep 1, 12:16 pm, "Bill Reid" < <EMAILADDRESS> > wrote: 
Excuse me, Bill, but you are wrong. 
Let me count the ways. 
Biological evolution is a change in the genetic characteristics of a population over time. Evolution and genetics together are part-and- parcel of the same body of science that brought us, for example, recombinant vaccines like Gardasil. 
"Conflated"?  I think not. 
However, I *do* think your Mendel reference is a bit dated. You might be interested in Fisher's 'Genetical Theory of Natural Selection' (1930) , which is one of the most important books of the modern evolutionary synthesis. 
Furthermore, much of our contemporary understanding of this science HAS changed since 1859 when Charles Darwin introduced the concept of natural selection to explain how evolution might occur. Contrary to your assertion that evolutionary science has been stagnant for 130 years, in fact this field of study has witnessed dramatic changes: 
<snip> During the first part of this century the incorporation of genetics and population biology into studies of evolution led to a Neo- Darwinian theory of evolution that recognized the importance of mutation and variation within a population. Natural selection then became a process that altered the frequency of genes in a population and this defined evolution. This point of view held sway for many decades but more recently the classic Neo-Darwinian view has been replaced by a new concept which includes several other mechanisms in addition to natural selection. Current ideas on evolution are usually referred to as the Modern Synthesis which is described by Futuyma; 
    "The major tenets of the evolutionary synthesis, then, were that populations contain genetic variation that arises by random (ie. not adaptively directed) mutation and recombination; that populations evolve by changes in gene frequency brought about by random genetic drift, gene flow, and especially natural selection; that most adaptive genetic variants have individually slight phenotypic effects so that phenotypic changes are gradual (although some alleles with discrete effects may be advantageous, as in certain color polymorphisms); that diversification comes about by speciation, which normally entails the gradual evolution of reproductive isolation among populations; and that these processes, continued for sufficiently long, give rise to changes of such great magnitude as to warrant the designation of higher taxonomic levels (genera, families, and so forth)."     - Futuyma, D.J. in Evolutionary Biology, Sinauer Associates, 1986; p.12 
-more-  <URL>  
You said, "evolution remains mired at the same level it was 130 years ago", which is clearly flat-out wrong, (e.g. the recent shift towards the random genetic drift vs.the adaptively directed model.) 
Having pointed out your errors of fact, Bill, let me also point out your errors of behavior: you are rude and bullying in your conduct towards me. 
You may be only half ignorant but you're a complete asshole, and as far as I'm concerned, you can go fuck yourself. 

.-=d00b . 

