


Job Title:     Site Compliance Inspector Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-07-05 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Site Compliance Inspector &#150; QUA001695 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career.  Please note, this position will be filled at one of several levels, depending on the qualifications of the chosen candidate: * Site Compliance Inspector * Senior Site Compliance Inspector * Site Compliance Specialist   Information on the Senior Site Compliance Inspector and Site Compliance Specialist positions may be found in requisition number QUA001659. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001695. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  07/03/2008, 01:27 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  07/10/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-410576 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



