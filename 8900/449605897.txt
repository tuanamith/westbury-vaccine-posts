


 <URL>  
Ill. House Panel Recommends Blago Impeachment  
Thursday, January 8, 2009 
SPRINGFIELD, Ill.  An Illinois House committee has unanimously recommended that Gov. Rod Blagojevich be impeached for abuse of power. The decision sets the stage for the full House to take action Friday and make Blagojevich the first governor impeached in Illinois history. The 21-member committee began studying impeachment after the Democratic governor was arrested on a variety of federal corruption charges.  
The panel based its recommendation on those charges and other allegations of misconduct by Blagojevich. They include evidence he circumvented state hiring laws, misspent tax money and expanded programs without proper authority.  
If the House impeaches Blagojevich, the Senate would then conduct a trial on whether he should be removed from office.  
THIS IS A BREAKING NEWS UPDATE. Check back soon for further information. AP's earlier story is below.  

SPRINGFIELD, Ill. (AP)  The Illinois House committee investigating Gov. Rod Blagojevich released a draft report Thursday that concludes the Democratic governor has abused his power and was poised to recommend he be impeached by the full chamber.  

"The citizens of this state must have confidence that their governor will faithfully serve the people and put their interests before his own," the report reads. "It is with profound regret that the committee finds that our current governor has not done so."  

The panel could vote to accept the report and recommend impeachment when it resumes work Thursday afternoon, which would set up a full House vote Friday. If the House votes to impeach, the matter would go to the state Senate for a trial. No Illinois governor has ever been impeached.  

Blagojevich was arrested Dec. 9 on federal charges that include allegations he schemed to profit from his power to name President-elect Barack Obama's replacement in the Senate.  

The man Blagojevich eventually appointed, Roland Burris, was scheduled to appear before the impeachment committee Thursday afternoon. Burris was picked after Blagojevich's arrest and denies any improper conduct. 
The committee's 59-page draft recounts the federal charges, relying on a sworn affidavit from an FBI agent describing tape-recorded conversations in which Blagojevich discussed using the seat to land a job for himself or his wife. The second-term governor also is quoted on the need to hide any evidence of a trade-off.  

"The committee believes that this information is sufficiently credible to demonstrate an abuse of office of the highest magnitude," the report says.  

The draft also lays out allegations separate from the criminal charges  that Blagojevich expanded a health care program without proper authority, that he circumvented hiring laws to give jobs to political allies, that he spent millions of dollars on foreign flu vaccine that he knew wasn't needed and couldn't be brought into the country.  

Blagojevich's office and his attorney had no immediate comment.  

The governor has denied any wrongdoing, but the draft report notes he did not appear before the committee to explain himself. "The committee is entitled to balance his complete silence against sworn testimony from a federal agent," it says.  

The draft does not include a formal article of impeachment for the House to consider laying out the charges against Blagojevich, which could be written separately. But it says there is cause to believe Blagojevich engaged in pay-to-play politics.  

The report was released as the impeachment panel prepared to hear afternoon testimony from Burris on why he accepted a position offered by the disgraced governor and whether he promised Blagojevich anything in return.  

Blagojevich's Dec. 30 appointment of Burris created a furor, coming just three weeks after the governor's arrest.  

Burris returned Wednesday from an encouraging two-day visit to Washington, D.C., yet without being able to take the oath of office with the newest members of the 111th Congress.  

"I would like to specifically ask, under oath, if there was any quid pro quo for the appointment," said Rep. Mike Bost, a Republican member of the committee.  

Lawmakers also plan to ask Burris about contributions to the governor's campaign, how Blagojevich's wife got a job with a group affiliated with Burris' business partner and why the governor's criminal lawyer approached Burris about the Senate instead of a staff member.  

The panel also is awaiting a federal court ruling Thursday on whether it will get to hear some of the secretly recorded conversations federal prosecutors made of Blagojevich allegedly scheming to trade government action for campaign contributions.  

In Washington, D.C., U.S. Senate leaders have said they would be open to recognizing Burris' appointment after he deals with lingering legal obstacles.  

They're also waiting for a decision from the Illinois Supreme Court on whether Illinois Secretary of State Jesse White must sign off on Blagojevich's appointment of Burris. Senate rules appear to bar seating anyone whose appointment isn't properly signed by state officials.  

When Burris showed up at the Capitol to be sworn in Tuesday, he was turned away in the rain. But on Wednesday, he was invited in to meet with Senate Majority Leader Harry Reid of Nevada and the No. 2 Senate Democrat, Dick Durbin of Illinois.  

Later, Reid and Durbin reported that they thought highly of Burris and they were merely waiting for procedural matters to be resolved before he could be seated.  

Burris, 71, said he should be able to join the Senate "very shortly."  
  

