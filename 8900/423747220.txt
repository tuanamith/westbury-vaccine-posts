


Job Title:     Biochemist / Biochemical Engineer (SOURCING REQ) Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-10-05 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Biochemist / Biochemical Engineer (SOURCING REQ) &#150; BIO001943 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Under the direction of the purification or fermentation group leader perform process development &amp; scale-up studies. Participate in the manufacture of safety-assessment materials for biological product candidates, and in the manufacture of GMP clinical supplies. Support technology transfer to commercial-scale manufacturing. Duties will include: lab scale process development, in-process assay support, process scale-up and engineering, cGMP Due to the nature of the research, flexibility in work hours are necessary including evening and/or weekend hours. Moreover, travel to other sites will be required on occasion for technology implementation and transfer. 
Qualifications Required -Must have strong problem solving and hands-on laboratory skills. Excellent organizational, interpersonal, and verbal/written communication skills are needed. Desired -1-4 years of experience in bioprocess R&amp;amp;D preferable. Experience with computer/equipment interfacing, equipment set-up, and biochemical laboratory skills are preferred. Experience with the culture or purification processes for production of therapeutic proteins is desired. Education Requirement -Must have BS or MS in chemical engineering/biochemistry or a closely related discipline. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # BIO001943. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm RepresentativesPlease Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  10/02/2008, 10:23 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/01/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-423596 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



