


SECRECY NEWS from the FAS Project on Government Secrecy Volume 2008, Issue No. 114 December 3, 2008 
Secrecy News Blog:   <URL> / 
Support Secrecy News  <URL>  
** NEW LIGHT ON THE OFFICE OF LEGAL COUNSEL **  INDEX ON CENSORSHIP, AND SECRECY IN CONGRESS **     CONGRESSIONAL RESOURCES ON ARMS CONTROL **     IRAN'S "HOOT" TORPEDO DOCUMENTED 
NEW LIGHT ON THE OFFICE OF LEGAL COUNSEL 
In 2004, Congress enacted an ambiguously-worded statute that made it a crime to produce, possess, or use the variola virus (which causes smallpox) or any derivative of it that has more than 85 percent of the variola gene sequence. 
Scientists were alarmed because the statute could be read to prohibit possession of other viruses that are used in research, and to compromise the development of vaccines.  The National Science Advisory Board for Biosecurity, which advises the Secretary of Health and Human Services, recommended that the statute be repealed. 
But it wasn't repealed.  Instead, it was "interpreted" by the Justice Department Office of Legal Counsel (OLC) to eliminate the problem.  The OLC opinion (pdf), published by the Justice Department this week, said that the prohibition extends only to viruses that cause smallpox and those that are deliberately engineered by human manipulation of the smallpox virus itself.  The vaccine developers and other scientific researchers were off the hook. 
No sensible person would be likely to object to the conclusion reached by OLC.  What seems problematic, however, is the interpretive process and the quasi-legislative authority that OLC now has to define and redefine the terms of existing laws.  In this case, OLC interpretation was the functional equivalent of statutory amendment or repeal.  And though unclassified, the July 2008 OLC ruling remained unpublished for several months.  One can only imagine what OLC may be doing in classified areas. 
Steven G. Bradbury, the head of the Office of Legal Counsel, recently responded to numerous questions (and deflected others) regarding the conduct of his office posed by members of the House Judiciary Committee following a February 14, 2008 hearing. 
Substantive replies were given (pdf) to questions such as these:  If an OLC opinion is incorrect, would it still confer immunity on a person who relied on it?  (Basically, yes.) How does the Department decide which OLC memoranda and opinions are to be classified and which are to be released to the public?  (OLC does not have original classification authority, and proceeds on a case by case basis.)  Can the President change his interpretation of an Executive Order without formally amending the order or issuing a new one?  (Yes, he said.) 
Other questions, such as why Deputy Attorney General James Comey said that officials would be "ashamed" when the public learned of certain OLC legal interpretations, were not directly answered. 
And still others, such as the limits of Presidential authority, cannot be definitely answered at all, Mr. Bradbury said.  "The President's independent constitutional powers, including his powers as Commander in Chief, have never been fully defined, and cannot be, because their contours necessarily depend on the concrete exigencies of particular events." 
Mr. Bradbury's answers to questions for the record were transmitted on August 18, 2008, and published in the PDF version of "Justice Department's Office of Legal Counsel," hearing before the House Judiciary Committee, February 14, 2008. 
INDEX ON CENSORSHIP, AND SECRECY IN CONGRESS 
Index on Censorship, the British magazine on freedom of expression, devotes its latest issue to secrecy, surveillance and executive authority in the United States at the end of the Bush Administration.  It features articles by Jameel Jaffer, Geoffrey R. Stone, Eric Lichtblau, Patrick Radden Keefe, and myself, among others.  Many of the articles can be viewed online. 
"For all its apparent openness, its televised debates and public hearings, Congress is more secretive than its reputation suggests," writes Tim Starks in a Congressional Quarterly Weekly cover story.  "Critics of congressional secrecy argue that the practice is not only undemocratic, it is particularly hypocritical, and it undercuts the public's confidence in government."  See "A Dome Under Lock and Key" by Tim Starks, CQ, November 30. 
CONGRESSIONAL RESOURCES ON ARMS CONTROL 
Noteworthy new Congressional publications on arms control-related topics include the following. 
"North Korea and Its Nuclear Program -- A Reality Check" (pdf), Report to the Senate Committee on Foreign Relations, October 2008. 
"International Convention for Suppression of Nuclear Terrorism," Report of the Senate Committee on Foreign Relations, September 11, 2008. 
"Technologies to Combat Weapons of Mass Destruction," hearing before the Senate Armed Services Committee, March 12, 2008. 
IRAN'S "HOOT" TORPEDO DOCUMENTED 
In April 2006, Iran successfully test-fired a new high-speed torpedo called Hoot.  It was test-fired again last July, along with various other missiles. 
"The torpedo is capable of destroying the largest warships and any other vessel on the surface or beneath the water, and split it into two parts," according to an Iranian Naval Forces official. 
Technical specifications (pdf) for components of the Hoot torpedo are presented in an Iranian document (in Farsi) that was provided to Secrecy News.  The document appears to have been produced by a subunit of Iran's Aerospace Industries Organization, according to a colleague who reviewed it. 
"Only Iran and another country possess the technology to build this [torpedo]," the Iranian press reported after last July's test, apparently referring to Russia and its Shkval torpedo.  On  4 April 2006, Izvestiya Moscow said that the Hoot resembles the Shkval technically and in appearance, and that Shkval torpedoes may have found their way to Iran via China, where they were delivered in the mid-1990s.  But Iranian officials insist the Hoot is a completely original production. 
"From a tactical point of view," said Rear Admiral Morteza Safari of the Islamic Revolutionary Guards Corps naval forces, "what is of critical importance is that we are everywhere, while we are nowhere!"  (Fars News Agency, July 10, 2008, via OSC). 
"Let me briefly say that the intelligence that the Americans have about us is very different from the intelligence that they do not have about us," he went on.  "What I mean is that they have only little information, and there is a lot of intelligence that they are not aware of." 
The Secrecy News Blog is at:  <URL> / 
To SUBSCRIBE to Secrecy News, go to:  <URL>  
To UNSUBSCRIBE, go to  <URL>  
OR email your request to  <EMAILADDRESS>  
Secrecy News is archived at:  <URL>  

