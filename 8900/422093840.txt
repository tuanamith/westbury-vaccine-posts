


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Staff / Research Biochemist &#150; BIO001939 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Protein PurificationThis exciting position within Merck Global Structural Biology at the West Point, PA site will support drug lead discovery and optimization through the production and characterization of proteins for use in structural biology and screening. The incumbent will perform protein purification and characterization of recombinant proteins in prokaryotic and eukaryotic hosts for structural biology studies and emphasize continuing improvement in efficient production of target proteins. 
Qualifications Required B.S. degree in biochemistry or related discipline with at least 3-4 years experience or MS degree with at least 0-2 years of experience for the Staff Biochemist position or B.S. degree in biochemistry or related discipline with at least 4-8 years experience or MS degree with at least 3-6 years of experience for the Research Biochemist position. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careers to create a profile and submit your resume for requisition #BIO001939.Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm RepresentativesPlease Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  09/23/2008, 04:24 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  10/23/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-423176 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



