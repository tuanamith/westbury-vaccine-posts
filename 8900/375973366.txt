


 <URL>  
Irena Sendler, who died yesterday aged 98, is credited with having saved the lives of some 2,500 Jewish children in the Warsaw ghetto during the Second World War. 
By 1942 the Germans had herded some 500,000 Polish Jews into the ghetto - an area of about one square kilometre - to await transportation to the extermination camps. Starvation and disease, especially typhoid, were endemic. 
Irena Sendler was a Polish Roman Catholic social worker in the city who already had links with Zegota, the code name for the Council for Aid to Jews, and in December 1942 Zegota put her in charge of its children's department. 
Wearing nurses' uniforms, she and a colleague, Irena Schultz, were sent into the ghetto with food, clothes and medicine, including a vaccine against typhoid. It soon became clear, however, that the ultimate destination of many of the Jews was to be the Treblinka death camp, and Zegota decided to try to save as many children as possible. 
Using the codename "Jolanta", and wearing a Star of David armband to identify herself with the Jewish population, Irena Sendler became part of this escape network. One baby was spirited away in a mechanic's toolbox. 
Some children were transported in coffins, suitcases and sacks; others escaped through the sewer system beneath the city. An ambulance driver who smuggled infants beneath the stretchers in the back of his van kept his dog beside him in the front seat, having trained the animal to bark to mask any cries from his hidden passengers. 
In later life Irena Sendler recalled the heartbreak of Jewish mothers having to part from their children: "We witnessed terrible scenes. Father agreed, but mother didn't. We sometimes had to leave those unfortunate families without taking their children from them. I'd go back there the next day and often found that everyone had been taken to the Umschlagsplatz railway siding for transport to the death camps." 
The children who were taken by Irena Sendler were given new identities and placed with convents, sympathetic families, orphanages and hospitals. Those who were old enough to talk were taught Christian prayers and how to make the sign of the Cross, so that their Jewish heritage would not be suspected. 
Like the more celebrated Oskar Schindler, Irena Sendler kept a list of the names of all the children she saved, in the hope that she could one day reunite them with their families. 
On the night of October 20 1943 Irena Sendler's house was raided by the Gestapo, and her immediate thought was to get rid of the list: "I wanted to throw it out of the window but couldn't, the whole house was surrounded by Germans. So I threw it to my colleague and went to open the door. 
"There were 11 soldiers. In two hours they almost tore the whole house apart. The roll of names was saved due to the great courage and intelligence of my colleague, who hid it in her underwear." 
The Nazis took Irena Sendler to the Pawiak prison, where she was tortured; although her legs and feet were broken, and her body left permanently scarred, she refused to betray her network of helpers or the children whom she had saved. Finally, she was sentenced to death. 
She escaped thanks to Zegota, one of whose members bribed a guard to set her free. She immediately returned to her work using a new identity. Having retrieved her list of names, she buried it in a jar beneath an apple tree in a friend's garden. 
In the end it provided a record of some 2,500 names, and after the war she attempted to keep her promise to reunite the children with their families. Most of the parents, however, had been gassed at Treblinka. 
Irena Sendler was born Irena Krzyzanowska in Warsaw on February 15 1910 into a Polish Roman Catholic family. Her father was a physician who ran a hospital at the suburb of Otwock, and a number of his patients were impoverished Jews. 
Although he died of typhus in 1917, his example was of profound importance to Irena, who later said: "I was taught that if you see a person drowning, you must jump into the water to save them, whether you can swim or not." 
After the war Irena Sendler continued in her profession as a social worker and also became a director of vocational schools. In 1965 she became one of the first Righteous Gentiles to be honoured by Yad Vashem, the Holocaust Martyrs' and Heroes' Remembrance Authority in Jerusalem. At that time Poland's Communist leaders would not allow her to travel to Israel, and she was unable to collect the award until 1983. 
In 2003 she was awarded Poland's highest honour, the Order of the White Eagle; and last year she was nominated for the Nobel Peace Prize, eventually won by Al Gore. 
A play about her wartime experiences, called Life in a Jar, was written in 2000 by a group of American schoolgirls. It was performed on more than 200 occasions in the United States, Poland and Canada. 
She was also the subject of a biography by Anna Mieszkowska, Mother of the Children of the Holocaust: The Story of Irena Sendler. Last year it was reported that Irena Sendler's exploits in Warsaw were to be the subject of a film, with Angelina Jolie in the starring role. 
In her latter years Irena Sendler was cared for in a Warsaw nursing home by Elzbieta Ficowska, who - in July 1942, at six months old - had been smuggled out of the ghetto by Irena in a carpenter's workbox. 
In 2005 Irena Sendler reflected: "We who were rescuing children are not some kind of heroes. That term irritates me greatly. The opposite is true - I continue to have qualms of conscience that I did so little. I could have done more. This regret will follow me to my death." 
Irena Sendler's first husband was Mieczyslaw Sendler. The marriage was dissolved, and she later married Stefan Zgrzembski, with whom she had two sons and a daughter. One of the boys died in infancy, and her second son in 1999. Her daughter survives her.  



