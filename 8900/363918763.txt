



"bf" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... On Apr 11, 3:51 pm, "BC" < <EMAILADDRESS> > wrote: 


Interesting, since I did not see the ep, I did not know that. Another crazy thing about this show is that Canary is a "good guy", yet someone like Bizzero who reformed himself without any help from Clark and genuinely turned the corner was still sentenced to death by Clark.. 
I mean, that's a big problem with this show. the characters are predestined/prejudged or pre-labeled as "bad" or "good". Their actions are not relevant. Then Clark is the sole judge and punisher/executioner.. or he can decide that they are "good" and deserve a fresh start, regardless of their past crimes. 
Canary is treated the same as GA and Flash.  We see flash as a kid, ripping  off people and stores.  We see GA pay kidnappers to torture Lex and shoot  him and in the process they killed a private eye and imprisoned Lana.  Later  GA kills Lex but Lex miraculously is brought back to life by a drug.  Clark  has his bank robbing spree as well.  Canary too is doing bad deeds but  somehow on all of them we are supposed to "forgive" them their  transgressions because ultimately they will take on the mighty Lex who has  made one reanimated soldier.  Lex while devious and cunning, has been little  or no threat in this show.  The Zoners, Brainiac and Zod -- even Zor-el have  been the really dangerous villains.  Clark's uncle Zor-el blocked out the  Sun, Zod created the mayhem from Black Thursday that still has the college  closed I assume, since they never went back, and the Zoners were tearing out  spines and eating people all over the place.  Lex stole Clark's girl and  faked a pregnancy maybe, made one soldier and one brain dead clone, tried to  make a vaccine for all the diseases in the world but failed, tried to make  an undersea sound weapon for our navy but failed, has possibly stolen a key  to something by killing a childhood friend lastly, and that will probably  fail for some reason because it is the fate of Lex in this show to fail, to  always be frustrated by an unknown force that is Clark.  We are asked to  forgive the costumed good guys here but not to forgive Lex.  Seems like the  show has a sort of bias. 




