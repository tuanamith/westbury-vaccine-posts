


Grandad jabs at top doctors over MMR vaccine Oct 19 2008 by Phil Doherty, Sunday Sun 
It marks a dramatic twist to the debate about the alleged link between the  measles, mumps and rubella vaccine and autism. 
Mr Walsh said: "I brought this complaint because many children are suffering  a life of pain." 
"The complaint is as rigorous as possible so it makes it so much more  difficult for them to try to close it down. 
"There cannot be one rule for Andrew Wakefield and another for those in  powerful positions." 
A GMC spokesman said: "We do not comment on investigations as we have a duty  of confidentiality to all parties involved." 
 <URL> / 



