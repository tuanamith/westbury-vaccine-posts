



< <EMAILADDRESS> > wrote in message   <NEWSURL> ... On Sep 2, 6:00 am, "f. barnes" < <EMAILADDRESS> > wrote: 

I'm pretty sure teens get pregnant with or without comprehensive sex education.  I, too, would like to see some stats.  The difficulty would be to control all the variables.  For instance, how much of a role does the parent's attitudes about sex matter and how much does their attitudes about contraception matter?  Why haven't there been more studies? 
 <URL>  has this: 
  DiCenso et al. have compared comprehensive sex   education programs with abstinence-only programs.[38]   Their review of several studies shows that abstinence-only   programs did not reduce the likelihood of pregnancy of   women who participated in the programs, but rather   increased it. Four abstinence programs and one school   program were associated with a pooled increase of 54%   in the partners of men and 46% in women (confidence   interval 95% 0.95 to 2.25 and 0.98 to 2.26 respectively). 
UNITED NATIONS, October 13, 2005 (LifeSiteNews.com) - The United Nations'  envoy to Africa, Canadian Stephen Lewis, is highly critical of an abstinence  campaign that has downplayed the role of condoms but been hugely successful  at reducing HIV transmission in Uganda.  Population Researcher Institute's  Joseph A. D'Agostino suggests that the success in combating AIDS in Uganda  "isn't good enough for UN officials, whose love affair with condoms knows no  bounds, and who are also angry with America for funding her own AIDS  initiative in Africa instead of giving the money to them." 
Uganda, whose abstinence campaign has been so successful as to be likened to  a highly effective vaccine, has reduced HIV transmission rates from 18% to  5-7%. "No other nation in the world has achieved such success," writes  D'Agostino. "Most sub-Saharan African nations, following the pro-condoms  model, continue to suffer from rising HIV infection rates. Ugandan surveys  show a reduction in premarital sexual activity among Ugandan youth and a  reduction in extramarital activity among adults," D'Agostino added. "The  result: less AIDS." 
 <URL>  



