


Job Title:     Senior Research Biologist (Imaging Department) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-10-18 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Research Biologist (Imaging Department) &#150; PHA000781 
Job Description  
Submit 
We are searching for a unique individual with a diverse skill set. The successful candidate will be expected to work as part of a team using cutting edge optical imaging technologies such as Fluorescent Molecular Tomography to develop and evaluate novel imaging agents and pre-clinical animal models to support drug discovery. Experience with a variety of imaging methodologies such as Fluorescence imaging, microPET/microCT, Ultrasound, Flow Cytometry, experimental design and the ability to work effectively both independently and in a team environment is essential.Record keeping, data analysis, graphics and study report preparation in a timely manner is essential. Excellent interpersonal (team player) skills and demonstration of written and verbal communication skills are required for interactions and collaborations with other scientists across Merck Research Laboratories. 
Qualifications Education Requirement: PhD in Physiology, Biochemistry, Pharmacology or related field. Required: - Strong experience in optical imaging technologies, experimental knowledge of molecular biology and imaging methodologies such as microPET, microCT - Demonstrated ability to design studies -Flow cytometry - Experience in writing scientific publication and good oral presentation skills  Desired: - Experience with Ultrasound imaging - Ability to learn drug discovery process and manage change Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site at www.merck.com/careers to create a profile and submit your resume for requisition #PHA000781. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  Yes, 5 % of the Time  
Additional Information   Posting Date  10/16/2008, 04:46 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/15/2008, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-424656 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



