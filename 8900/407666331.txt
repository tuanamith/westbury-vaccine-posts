


 <URL>  
 <URL>  
U.S. Lacks Warning System on Artificial Joints 
Excpert: 
Last week, Zimmer, based in Warsaw, Indiana, announced it was suspending  marking and distribution of the Durom Cup.  Surgeons are receiving letters  from the company advising them to stop implanting the medical device. 
Zimmer admits that some U.S. surgeons may need better training and the label  on the product needs to be updated. 
The company in a withdrawal announcement, says it plans to "provide more  detailed surgical technique instructions to surgeons and implements its  surgical training program in the U.S.  The Durom Cup will continue to be  marketed outside of the U.S. where Europeans have enjoyed "excellent  clinical outcomes since the product launched in 2003." 
Patients are being given an 800-number to report any concerns and problems,  though the company believes that a "low" percentage of the 13,000 who've  received the socket will need replacement. 
Zimmer hopes to remarket the device by 2009. 
The move has cost the company about $20 to $30 million and could open the  company to defective product litigation. Not that it hasn't faced that  before. 
While Zimmer representatives would not talk to IB News about this topic,  Zimmer has successfully argued a blanket immunity defense used by many  medical device makers. 
It's called federal pre-emption. Since the FDA oversees and conducts  pre-market review of a devices design and label and okays the device for the  market, the manufacturer is protected from lawsuits filed in state courts. 
Stephen Sheller, of Sheller P.C., a Philadelphia lawyer who handles  defective drug and medical device cases (and an IB member), predicts if  litigation results from the Durom Cup withdrawal,  Zimmer's admission may  preclude a pre-emption argument. 
"The company says the fact is that the reason the problem is occurring is  that doctors are not adequately trained by the Zimmer people on how to use  product," he tells IB News.  "The complaint could be negligent training and  a failure to train doctors properly, not the medical device per se. The  doctor is not necessarily at fault. It takes it out of pre-emption." 
Litigation or not, how is a consumer to know if an artificial hip or knee is  causing problems and has to be replaced in patients? 
If you live in countries such as Australia, Britain, Norway and Sweden,  Zimmer may have had to pull the controversial hip socket before 13,000 were  implanted, and consumers and doctors would have access to that information. 
Those countries track problems with artificial joints in a national data  registry, called a joint registry. Not so in the U.S. 
While this country tracks automobiles, trademarks, voters, refrigerators,  domain names, clinical trials, and even, occasionally -   sex offenders -  the U.S. lacks a federal registry to monitor artificial joints. 
Elsewhere, a registry works like this - a patient number is matched with  information on the device, including the manufacturer, the surgeon who  implanted it and the outcome of the procedure. 
A registry could potentially police poor performing artificial hips and  knees. Their manufacturers would have to justify their continuation on the  market. 
As it stands now -  for the roughly one million U.S. patients who need  artificial hips or knees -  the lack of oversight that ultimately shields  manufacturers at the expense of consumers, means patients have roughly  double the risk of a problematic replacement procedure, whether with the  device or the surgical technique, Dr. Henrik Malchau of Massachusetts  General Hospital tells the New York Times. 
Who would oversee the registry? 
The Food and Drug Administration (FDA) could oversee such a federal registry  but -  as the recent search for the elusive salmonella tomato highlighted-  the agency has its plate full trying to regulate 80 percent of the food  Americans eat and all of the prescription drugs we take. 
Medicare has the incentive to oversee a federal registry, saving billions in  procedures that need to be repeated.  And patients would be saved from  enduring the pain and suffering associated with having an artificial hip or  knee removed and replaced. 
In Sweden, where a federal registry exists, doctors knew there was a problem  with the Sulzer Orthopedics artificial hip after 30 patients experienced  complications. In the U.S., Sulzer took six months to withdraw the device  after Dr. Dorr's patients began experiencing problems, but not before 3,000  patients had the procedure. 
Earlier this year when Zimmer began investigating complaints about the Durom  Cup failures, because there is no federal registry the company had to go  through 1,300 patient records. 
Meanwhile 1,300 new patients had the procedure. 
At an estimated $5 to $10 million annually, the cost of a federal registry  is far less than the cost of settling lawsuits. 
Last year, Zimmer was among five medical device companies that agreed to pay  $310 million to avoid criminal charges and to settle civil charges resulting  from a Department of Justice investigation into doctor kickbacks. 
The investigation, from 2002 to 2006, found that orthopedic surgeons were  paid "exorbitant" amounts (tens to hundreds of thousands of dollars) and  were given trips to be consultants and to use their products exclusively. 
Zimmer paid more than $1 million to 21 consultants in 2007, according to  Bloomberg News. The company paid $169.5 million and was to be monitored (by  former U.S. Attorney General John Ashcroft). 
Zimmer did not admit any wrongdoing. 
Today, Zimmer and the other manufacturers are paying lawyers tens of  millions to ensure compliance. 
"We could have used some of that money for a registry," Dr. Malchau tells  the New York Times. 
The hip and knee replacement business generated about $9.7 billion worldwide  in 2007, according to one analyst speaking to Bloomberg News. 
Zimmer's  2007 sales of orthopaedic, spinal and trauma devices, dental  implants, and orthopaedic surgical products in more than 25 companies  totaled approximately $3.9 billion, according to a company statement. 
There were 220,000 total hip replacements performed in the United States in  2003 according to CDC statistics.  60% are performed on women and two-thirds  are older than age 65. 
Replacement is done to relieve the pain and degeneration in the hip due to  arthritis or trauma. # 
 <URL>  
Study Finds Surgical Errors Cost Nearly $1.5 Billion a Year 
 <URL>  
Medical errors are costing us billions and leading to preventable deaths.  That from the fifth annual Patient Safety in American Hospitals study, by  HealthGrades, a leading hospital rating organization. It finds from 2004  through 2006 there were 238,337 preventable deaths among Medicare patients.  That cost the program and ultimately taxpayers $8.8 billion. 
Drug Mix-ups Affect One in 15 Hospitalized U.S. Kids 
The near-death experience of the newborn twins of actor Dennis Quaid along  with a new study are highlighting the frequency of medication related harm  to children in U.S. hospitals. Researchers find that hospital mix-ups  involving drugs affect about seven percent of hospitalized U.S. children.  That translates to one in 15 hospitalized children or 540,000 kids each  year. 
Merck Busted For Ghostwriting Vioxx Studies 
Drug maker, Merck & Co., has always characterized its conduct as above board  and ethically appropriate among pharmaceutical companies. "We employ  rigorous scientific methods to design, conduct, analyze, and report results  of clinical trials in the development of innovative drugs and vaccines, with  a focus on meeting unmet medical needs and with an ethic that puts the  interests of the patient first." 
$4,600 For Celebrity Medical Records Brings Indictment The first in what likely will be several indictments was unsealed Tuesday in  connection with UCLA Medical Center celebrity record-leaking. In this  indictment, a workers is alleged to have sold records for $4,600. She is  facing up to 10 years in prison for violating the HIPAA privacy act. 
Las Vegas Hepatitis C Doc Blocked From Medicine 
Two doctors who owned clinics where an outbreak of hepatitis C is linked to  unsafe medical practices, face temporary restraining orders that could lead  to a permanent lose of their medical licenses. 
A New Low of Indifference- Hospital Staff Ignores Dying Woman A new low in human behavior is observed in this surveillance videotaped  death of a woman in a Brooklyn, N.Y. psychiatric hospital. No one comes to  help. While the hospital is instituting changes, how many among us are  capable of showing indifference? 
Baby Dies After Receiving Heparin Overdose at Texas Hospital 
Fourteen babies in the neonatal intensive care unit of Christus Spohn  Hospital South were given overdoses of the pediatric version of the blood  thinner Heparin, according to hospital officials. And one baby has died. 
Texas Hospital Heparin Error (Part Two) 
The CEO of a Corpus Christi hospital is apologizing for a medication error  that may have caused the death of an infant. Another is in critical  condition. Meanwhile, caps on damages in Texas mean that many medical  malpractice lawsuits will never be filed. 
You're Not In Good Hands When It's Time To Pay - Worst Insurers Report 
The American Association for Justice names the Top Ten Worst  Insurers who  try to avoid paying when you need them most. Tactics include using a "boxing  glove" strategy against policyholders while the industry enjoys record  profits. 


