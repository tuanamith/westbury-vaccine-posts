


Job Title:     Bio-Informatics Co-op assignment - RNA Therapeutics,... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-11-23 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Bio-Informatics Co-op assignment - RNA Therapeutics, West Point, PA &#150; BIO001997 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;will have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. 
The successful candidate will work closely in a team environment with Merck biologists, chemists, and mathematicians in the development of RNA Therapeutics. Primary responsibilities include characterization of potential targeted delivery reagents and siRNA vehicles through novel assays and the elucidation of pathway mechanisms. The candidate will be required to develop and validate biochemical and cell-based assays for evaluating targeting ligands, determining binding properties and kinetics, and assessing internalization mechanisms and subcellular distribution. The candidate will also develop stable cell lines and reporter systems for monitoring intracellular trafficking and endosomal escape.    Note that this is a paid co-operative assignment whereby a weekly stipend will be provided. Housing subsidy is not available as part of this program and if required by the student must be funded 100% by the student. 
Qualifications 
Required- Pursuing M.S. and/or PhD degree in Biology, Biomedical Engineering or in related field. Candidate must have experience with targeting ligands, binding assays, nanoparticles, bioconjugations, siRNA, cell culture, stable cell line generation, confocal microscopy, and image analysis. .  .  Desired- Experience in molecular biology, cell biology, fluorescence microscopy, and statistics; as well as, trong ommunication and problem solving skillsare strongly preferred,  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition # BIO001997.  Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular  
Additional Information   Posting Date  11/21/2008, 03:43 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/28/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-430556 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



