


On Nov 14, 3:56 pm, Bill Hudson < <EMAILADDRESS> > wrote: 
That is what I thought up until I received the November 2008 Scientific American.  In an article about HIV titled "The vaccine search goes on" there is a diagram titled: "A SELF-COPYING COMMANDO" with a caption which reads in part: 
"... A viral enzyme, reverse transcriptase, then copies the virus's RNA genome into double-stranded DNA (3), often making errors that generate diversity in the virus copies. ..." 
ant the implications are discussed further in the article. 
There is of course a VAST amount that I do not understand in all this, but it does appear that at least some of HIV's copying operations are performed by its own enzymes. 
Thank you for your intervention, which is appreciated. 


