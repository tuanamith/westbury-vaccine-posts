



"mrbawana2u" < <EMAILADDRESS> > wrote 
Mr, Gene will create a custom DNA sequence for you today. 
My understanding is that some gene suppliers go as low as 69 cents per base  pair. 
Mr Gene designs and orders constructs in minutes By Dr Matt Wilkinson 
22-Nov-2007 - Geneart has launched the first fully on-line gene construct  design and ordering system, dubbed Mr Gene, which is targeting the emerging  'low-price' segment of the synthetic biology market. 
The use of synthetic gene constructs to produce engineered cell lines that  increase the efficiency of biological drug and vaccine formation or speed-up  the drug discovery process has been growing rapidly. 
According to Geneart, the production of synthetic genes is fast replacing  laborious cloning strategies, with gene constructs being tailored to exactly  match each customer's project requirements. 
As such, there has been an explosion in the number of company's offering  custom gene synthesis services and Geneart has now launched a new brand into  this highly competitive arena to complement its high-end gene construction  service offering. 
Dubbed Mr Gene, the new eCommerce system is the first fully on-line design  and ordering system that allows customers to optimise their gene sequences  to produce more of the proteins they are being designed to code for. 
"This is the first fully we-based process, the first online tool where you  can optimise your gene for a specific application and at the same time order  the sequence," said Bernd Merkl, Business Development Manager at Geneart. 
"The customer designs the sequence and enters it directly into the computer,  enabling Geneart the ability to offer them attractive prices." 

Synthetic genes produce using the service will cost ?0.69 per base pair,  giving scientists with smaller budgets easier access to the benefits of  using gene constructs. 
"Universities and public research institutes now show a strongly growing  demand for competitively priced synthetic genes," said Professor Wagner,  Geneart's chief scientific officer. 
"We are delighted that Mr Gene will now bring many advantages of using  synthetic genes to academic research groups as well." 
To benefit from the new offering, customers need to go to www.MrGene.com and  fill in a profile to enable the company to check that they are reputable  scientists before they build the constructs for them. 
According to Merkl, the ordering process can be completed in minutes once  the profile has been entered into the system depending on the complexity of  the construct and how long the researcher wants to spend optimising its  design. 
The sequences are then checked before synthesis to ensure that no hazardous  sequences are produced. Customers should then expect their constructs in  around three weeks. 
 "Mr Gene as a second brand is our answer to the anticipated strong sales  growth in this market segment," said Christian Ehl, chief financial officer  at Geneart. 
"Our medium term projections put the Mr Gene sales at 25 percent of the  Geneart sales. The operating margin for Mr Gene products will be equally  attractive thanks to the high level of automation using the eCommerce  platform." 






