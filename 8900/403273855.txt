


 <URL>  
MY GIRL DIED AS 'GUINEA PIG' FOR GARDASIL 
By SUSAN EDELMAN 
 <URL>  
Posted: 4:26 am July 20, 2008 
She loved SpaghettiO's, pepperoni, lilies, listening to her iPod and making her pals laugh. 
In her senior yearbook, she wrote, "The best things in life aren't things, they're friends." 
Now that's the quote chiseled into her gravestone. 
Jessica Ericzon, 17, was "an all-American teenager," as described by one of her upstate LaFargeville teachers. 
Last February, she was working on her softball pitches, getting ready for a class trip to Universal Studios in Florida and hitting the slopes to snowboard with her older brother. 
Then one day, the blond, blue-eyed honors student collapsed dead in her bathroom. 
It started with a pain in the back of her head. 
On the advice of her family doctor, Jessie had taken a series of three Gardasil shots. 
The vaccine, marketed for females ages 9 to 26, is the first found to ward off strains of the sexually transmitted human papillomavirus, or HPV, which can cause cervical cancer. 
Jessie got the first injection in July 2007. 
After her second shot in September, she complained of a pain in the back of her head, fatigue and soreness in some joints, said her mom, Lisa. 
On Feb. 20, while on winter break from school, she got her third and final dose of the vaccine. 
The next night, "she told me the spot on the back of her head was bothering her again," her mom said. 
The next morning, Feb. 22, Lisa, a hospital technician, left for work just after 5 a.m., leaving Jessie asleep. 
Jessie never showed up for the class she was taking at Jefferson Community College. 
When her mom got home at 3:20 p.m., she found Jessie sprawled on her back on the bathroom floor, with blood spots on her head where it had hit a flowerpot. 
Jefferson County Medical Examiner Samuel Livingstone is stumped. 
"She was essentially dead by the time she hit the floor. Whatever it was, it was instantaneous," Livingstone said. His autopsy found no cause. 
He speculates she suffered a cardiac arrhythmia, or irregular heartbeat, extremely rare in young people. 
Jessie had been on birth-control pills for a year to treat acne, records show. 
Livingstone reported Jessie's death to the federal Vaccine Adverse Events Reporting System. 
Run by the FDA and the Centers for Disease Control and Prevention, it has collected 8,000 reports of problems after Gardasil shots, including paralysis, seizures and miscarriages. 
Seventeen other deaths following the vaccine have been reported since Merck & Co. introduced it in 2006. 
Officials have confirmed 11 of the reported deaths so far, said CDC spokesman Curtis Allen. 
They have found "no pattern or connection" to Gardasil in eight deaths and are still reviewing three, he said. 
Lisa Ericzon now feels her daughter was "a guinea pig" for Gardasil, and is urging parents to research the vaccine before letting their daughters get it. 
"I want other mothers to know," said Lisa, the first parent of a girl who died after Gardasil to speak publicly. 
"I don't want them to go through what I went through." 
Jessie planned to major in psychology at SUNY Plattsburgh and pursue her greatest ambition - to become a New York state trooper. 
Just six days before she died, she got to ride along with a trooper canine unit. She was ecstatic. 
Her family started the Jessica Ericzon Memorial Fund to award scholarships to her classmates. 





-------------------------------------------------------------------------------- 






