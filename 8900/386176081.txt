


Job Title:     Specialty Pharmaceutical Sales Rep - Cardio JV - Great... Job Location:  NY: Great Neck Pay Rate:      Open Job Length:    full time Start Date:    2008-06-05 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Specialty Pharmaceutical Sales Rep - Cardio JV - Great Neck, NY &#150; SPE003040 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, youll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. As a Merck Cardiovascular JV Sales Representative, you will help educate physicians and other health care professionals about Merck medicines by providing scientific information in our Great Neck, NY territory. Why Work for Merck * Customer Focus Selling Approach * Commitment to Diversity * Best-in-class benefits including paid vacation and holidays, pension, and 401k plans, health, dental and life insurance plans * Total compensation includes base salary and bonus opportunities * Career growth and promotional opportunities * Initial comprehensive training curriculum and continuing education * Flexible work arrangements to meet personal and professional needs * Company car and laptop computer Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Mercks finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Mercks retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site at www.merck.com/careersto create a profile and submit your resume for requisition # "SPE003040". Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someones hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
 Profile   Locations  US-NY-Great Neck   Employee Status  Regular  
Additional Information   Posting Date  06/03/2008, 12:22 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  06/17/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-402816 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



