


Job Title:     Co-op assignment 2008 - Imaging Department Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-08-22 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-op assignment 2008 - Imaging Department &#150; SCI003604 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We currently have interesting opportunities for talented students to join a well resourced imaging group with state-of-the-art imaging facilities supporting Mercks drug development and discovery efforts. In this exciting role, you will assist in processing and analysis of data from functional magnetic resonance imaging (fMRI) experiments and help in evaluating the use of this technique as a provider of markers for diagnosing disease processes and their response to drug therapy in humans and inin vivopre-clinical models. Responsibilities for this position include: management of the fMRI data, pre-processing, processing, production of written and oral summary reports. 
Qualifications This position requires applicant to be in pursuit of a graduate degree in physics, engineering and related fields, information technology, and mathematical subjects. Ideal candidate will possess strong applied mathematics and programming skills and experience. Previous experience with functional MRI data processing and analysis is preferred; in particular, experience with one of the following software packages: SPM, FSL, AFNI and Brian Voyager. Good working knowledge of Matlab software is necessary and programming in C/C++ is welcome. Successful candidate will possess good interpersonal skills; proficient oral and written communication skills; desire to work in a team environment. Strong project or program management skills are desired, but not required. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for SCI003604. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Standard   Employee Status  Regular  
Additional Information   Posting Date  08/20/2008, 04:30 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  08/29/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-418976 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



