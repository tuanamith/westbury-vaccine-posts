


Job Title:     Site Compliance Specialist Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-09-12 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Site Compliance Specialist &#150; QUA001712 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The Site Compliance Specialist:  * Participates in activities in the execution of quality system functions as it relates to pharmaceutical and vaccine GMP Compliance. Additionally, is frequently called upon to work in multi-department, site based teams regarding various compliance initiatives and assessments. * Maintains compliance proficiency by using available information channels (inspections, quality forums, regulatory guidelines and publications). Additionally, utilizes this information to identify and evaluate emerging trends for incorporation into Quality System requirement. * Review inspection reports from other sites/agencies.  * Maintain surveillance on regulatory agency publications. * Support the governance process to turn identified emerging trends into actionable items for site consideration. * Track strategic initiatives and new actionable items meet timing and true requirements. * Support change initiative teams to ensure the actions or changes meet compliance requirements. * Maintains leadership principles to effectively sponsor and lead change, set the example as a role model, holds people accountable for supporting the supply strategy with appropriate positive and negative consequences, and has courage to make the tough decisions. Please note, relocation assistance is not available for this position. 
Qualifications *  A Bachelor of Science degree is required for this position, preferably in a life science or engineering. *  A minimum of 3 years experience in a manufacturing/research environment or related field and solid understanding of regulatory requirements and GMPs is required.   *  Familiarity with vaccine/pharmaceutical processing is preferred.  *  Strong analytical, problem solving, oral and written communication skills, leadership, and attention to detail are needed.  *  Experience with a database tracking system such as Trackwise/QCTS is preferred. *  Lean Six Sigma training or experience is preferred.  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001712. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  09/10/2008, 01:00 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  09/17/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-416736 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



