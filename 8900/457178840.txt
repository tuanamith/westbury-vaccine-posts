



"Jan Drew" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
You're repeating yourself. What's more, it's out of date and is just a  random collection of people grasping at straws. 

 Why don't you try getting a grip? From the Washington Times, dated 26  January 2009: 
A new study from Italy adds to a mountain of evidence that a mercury-based  preservative once used in many vaccines doesn't hurt children, offering more  reassurance to parents. 
In the early 1990s, thousands of healthy Italian babies in a study of  whooping cough vaccines got two different amounts of the preservative  thimerosal from all their routine shots. 
Ten years later, 1,403 of those children took a battery of brain-function  tests. Researchers found small differences in only two of 24 measurements  and those "might be attributable to chance," they wrote in the February  issue of the journal Pediatrics, which was to be released Monday. 
Only one case of autism was found, and that was in the group that got the  lower level of thimerosal. 
Autism is a complex disorder featuring repetitive behaviors and poor social  interaction and communication skills. Scientists generally think genetics  plays a role in causing the disorder. A theory that thimerosal is to blame  has been repeatedly discounted in scientific studies. 
"Put together with the evidence of all the other studies, this tells us  there is no reason to worry about the effect of thimerosal in vaccines,"  said the new study's lead author, Dr. Alberto Tozzi of Bambino Gesu Hospital  in Rome. 
The debate over thimerosal and autism has been much stronger in the U.S.  than in Italy, Dr. Tozzi said. But the researchers recognized a chance to  examine the issue by going back to the children who had taken part in the  1990s whooping cough research. 
Randomization sets the new study apart. The random assignment of children  rules out the chance that factors other than thimerosal, such as education  or poverty, caused the results. 
Thimerosal, used in some vaccines to prevent the growth of bacteria and  fungus, hasn't been in U.S. childhood vaccines since 2001, except for  certain flu shots. Italy and other European nations began removing it in  1999. U.S. health officials recommended the removal of thimerosal as a  precaution and to reduce the overall exposure of children to mercury. 
Safety regulations still require multidose vials of vaccines to contain some  type of preservative to prevent the spread of infection from contaminated  vials. 
The study, funded by the U.S. Centers for Disease Control and Prevention,  drew praise from outside experts. 
"It's yet another well-done, peer-reviewed research study that has  demonstrated there is no risk of any neurodevelopmental outcomes associated  with thimerosal in vaccines," said epidemiologist Jennifer Pinto-Martin of  the University of Pennsylvania. 
"This becomes the fourth study to look for subtle signs of mercury toxicity  and show the answer was 'no,'" said Dr. Paul Offit, chief of infectious  diseases at the Children's Hospital of Philadelphia, the author of a book on  autism research and the co-inventor of a rotavirus vaccine. 




