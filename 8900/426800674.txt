


Job Title:     Co-Operative Assignment 2008 - Material Characterization... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-10-10 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-Operative Assignment 2008 - Material Characterization &amp; Tech Assessment (Procopio) &#150; PHA000771 
Job Description  
Submit 
This is a 6 month full-time co-operative assignment. Housing is not provided under this program. Students requiring housing will be responsible for location and full funding of accommodations. 
Qualifications 
- Pursuing BS or MS degree inMaterial Science and Engineering, Mechenical Engineering, Chemical Engineering, Engineering Mechanics or related field - Grade Point Average (GPA) of at least 3.0 or higher preferred - Applicants must be available for full time employment for 6 months. - Currently enrolled in an academic program and returning to school following this assignment Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition # PHA000771.  Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us.Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular  
Additional Information   Posting Date  10/09/2008, 12:00 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/07/2008, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-418956 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



