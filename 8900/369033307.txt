


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Sr. Manufacturing Facilitator/Supervisor &#150; PRO006854 
Job Description  
Submit 
**Please note that this is a 3rd shift (night shift) opening** Job Description: 
Qualifications 
Senior Manufacturing Facilitator / Supervisor:  * 1-3 years of manufacturing or staff support experience or equivalent.  * Good leadership and interpersonal skills. * B.S., B.A. or M.S. degree in Science, Engineering, or Business or equivalent experience. Manufacturing Facilitator / Supervisor:  * Leadership and interpersonal skills and previous manufacturing or staff support experience or equivalent are desirable. * B.S. or B.A. degree in Science Engineering, or Business equivalent. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # PRO006854. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  04/23/2008, 11:41 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/30/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-394136 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



