


Mercury Toxicity and Systemic Elimination Agents 
The following paper has been a long time in the making. I first wrote it nearly three years ago and it was initially rejected by the Lancet and the British Medical Journal but was published last month in the Journal of Nutritional and Environmental Medicine (March 2001). 
The end of the article has the bibliography which took quite awhile to compile and has 124 of the best literature documentation I could find on mercury detoxification. 
For a practical summary of the paper and exactly what one should do, please review my mercury detoxification protocol. 
Dr. Klinghardt is widely recognized as one of the most knowledgeable physicians in mercury detoxification and it was a privilege to be able to help him with this paper. 
The timing is especially appropriate in light of the mercury lawsuit that was filed last week 
Later this month on April 24 I will be involved in a press conference that will announce massive additional lawsuits relating to the toxicity of mercury. These lawsuits have the potential to make the tobacco issue look like small potatoes as the liabilities could run in the trillions of dollars. 
Abstract 
This paper reviews the published evidence supporting amalgam toxicity and describes practical and effective clinical techniques that facilitate mercury elimination. A literature review is provided which documents effective mercury elimination strategies to reduce mercury toxicity syndromes. 
Considering the weight of evidence supporting mercury toxicity, it would seem prudent to select alternate dental restoration materials and consider effective mercury elimination strategies if mercury toxicity is present. 
Mercury Exposure And Toxicity Is A Prevalent And Significant Public Health Threat. 
Chronic mercury exposure from occupational, environmental, dental amalgam, and contaminated food exposure is a significant threat to public health.1 
Those with amalgam fillings exceed all occupational exposure allowances of mercury exposure of all European and North American countries. Adults with four or more amalgams run a significant risk from the amalgam, while in children as few as two amalgams will contribute to health problems.2 In most children, the largest source of mercury is that received from immunizations 3 4 5 6 or that transferred to them in utero from their mother.7 8 
Dental Amalgams Are A Major Source Of Mercury Toxicity 

A single dental amalgam filling with a surface area of only 0.4 sq.cm is estimated to release as much as 15 micrograms of mercury per day primarily through mechanical wear and evaporation.1 9 10 11 
The average individual has eight amalgam fillings and could absorb up to 120 micrograms of mercury per day from their amalgams. These levels are consistent with reports of 60 micrograms of mercury per day collected in human feces.12 By way of contrast, estimates of the daily absorption of all forms of mercury from fish and seafood is 2.3 micrograms and from all other foods, air and water is 0.3 micrograms per day. 13 Currently, Germany, Sweden and Denmark severely restrict the use of amalgams.1 
A "silver" filling, or dental amalgam, is not a true alloy. Amalgams are made up of 50% mercury. The amalgam also consists of 35% silver, 9% tin, 6% copper and a trace of zinc.6 More than 100 million mercury fillings are placed each year in the U.S. as over 90% of dentists use them for restoring posterior teeth.14 
The mercury vapor from the amalgams is lipid soluble and passes readily through cell membranes and across the blood brain barrier. 15 The vapor serves as the primary route of mercury from amalgams into the body. It is clear that amalgam mercury transfers to human tissues, accumulates with time, and presents a potential health threat. The mercury escapes continuously during the entire life of the filling primarily in the form of vapor, ions but also abraded particles.16 17 Chewing, brushing, and the intake of hot fluids stimulates this release.18 19 20 
Statements made by dental authorities which claim that the amount of mercury exposure encountered by patients from dental amalgams is too small to be harmful, are contradicted by the literature.21 
Animal studies show that radioactively labeled mercury released from ideally placed amalgam fillings appear quickly in the kidneys22, brain and wall of the intestines.23 The fact that mercury amalgam fillings are banned in some European countries is strong evidence of the clinical toxicity of this material. 
Any metal tooth restoration placed in the mouth will also produce electrogalvanic effects. When dissimilar metals are placed in the oral cavity they exert a battery-like effect because of the electroconductivity of the saliva. The electrical current causes metal ions go into solution at a much higher rate, thereby increasing the exposure to mercury vapor and mercury ions manyfold. Gold placed in the vicinity of an amalgam restoration produces a 10-fold increase in the release of mercury.24 
Mercury's Long Half-Life In The Central Nervous System 

Mercury in the central nervous system (CNS) causes psychological, neurological, and immunological problems in humans.25 26 27 Mercury bonds very firmly to structures in the CNS through its affinity for sulfhydryl-groups on amino acids. Other studies have shown that mercury is taken up in the periphery by all nerve endings and rapidly transported inside the axon of the nerves (axonal transport) to the spinal cord and brainstem.28 29 30 Unless actively removed, mercury has an extremely long half-life of somewhere between 15 and 30 years in the CNS.1 31 
Mercury Toxicity Symptoms 

The overt clinical effects resulting from toxic exposure to mercury have been clearly described.32 33 The scientific literature shows that amalgam fillings have been associated with a variety of problems such as Alzheimer's Disease,34 35 autoimmunity,36 37 38 kidney dysfunction, 39 infertility,40 41 42 polycystic ovary syndrome, 43 neurotransmitter imbalances,44 food allergies,45 multiple sclerosis,46 thyroid problems, 47 and an impaired immune system.48 
Patients with many amalgam fillings will also have an increase in the prevalence of antibiotic resistant bacteria.49 Subclinical neuropsychological and motor control effects were also observed in dentists who had documented high mercury exposure levels.50 51 Amalgam use may also be related to fatigue, poor memory and certain psychological disorders.52 
There has been a recent epidemic of autism in the US53 54 and many investigators believe that this may be partially related to the increased exposure infants have had to mercury through the preservative thimerosal that was included in nearly all vaccines until recently.55 
The nervous system is more sensitive to mercury toxicity than any other organ in the body. Mercury has recently been documented to be associated with arrhythmias and cardiomyopathies as hair analysis showed mercury levels to be 20,000 higher in those with these cardiac abnormalities.56 Mercury exposure has also been associated with other neurological problems such as tremors,57 insomnia, polyneuropathy, paresthesias, emotional lability, irritability, personality changes, headaches, weakness, blurred vision, dysarthria, slowed mental response and unsteady gait.1 58 59 
Systemic Mercury Elimination 

There are a number of agents that have been demonstrated to have clinical utility in facilitating the removal of mercury with someone who has demonstrated clinical signs and symptoms of mercury toxicity. The urine and feces are the main excretory pathways of metallic and inorganic mercury in humans.1 60 
The most important part of systemic elimination is to remove the source of mercury. 
For most this involves amalgam removal. Individuals should seek a dentist who is specially trained in this area as improperly removed amalgam may result in unnecessarily high exposure to mercury.61 The following is a summary of the most effective agents that have been documented in the peer-reviewed literature. 
DMPS 

DMPS (Sodium 2,3-dimercaptopropane-1-sulfonate) is an acid-molecule with two free sulfhydryl groups that forms complexes with heavy metals such as zinc, copper, arsenic, mercury, cadmium, lead, silver, and tin. DMPS was developed in the 1950s in the former Soviet Union and has been used to effectively treat metal intoxication since the 1960s there.62 It is a water-soluble complexing agent. 
Because it had potential use as an antidote for the chemical warfare agent, Lewisite, it was not available outside of the Soviet Union until 1978, at which time Heyl, a small pharmaceutical company in Berlin, Germany started to produce it. It has an abundance of international research data and an excellent safety record in removing mercury from the body63 and has been used safely in Europe as Dimaval for many years.64 65 66 67 
DMPS is registered in Germany with the BGA (their FDA) for the treatment of mercury poisoning but is still an investigational drug in the United States.68 
The best and only brand of DMPS that should be used is Heyl from Germany. Great care should also be exercised in making certain the DMPS is compounded properly from the pharmacist. If the DMPS contacts metal during it will be oxidized, so the compounding pharmacist must use nonmetal needles must be used in preparing the product. 
DMPS Can Be Used To Eliminate Mercury Systemically 

The use of DMPS to treat mercury toxicity is well established and accepted. 69 70 71 DMPS has clearly demonstrated elimination effects on the connective tissue.72 73 The DMPS dose is 3-5 mg /kg of body weight once a month which is injected slowly intravenously over five minutes. DMPS-stimulated excretion of all heavy metals reaches a maximum 2-3 hours after infusion and decreases thereafter to return to baseline levels after 8 hours.74 
DMPS Safety 

DMPS is not mutagenic, teratogenic or carcinogenic.75 Ideally intravenous DMPS should never be used in patients that still have amalgam fillings in place, although investigators have done this as diagnostically, as a one-time dose, without complications.76 DMPS appears in the saliva and may mobilize significant amounts of mercury from the surface of the fillings and precipitate seizures, cardiac arrhythmias, or severe fatigue. 
One should use DMPS with great caution and NEVER use it in patients with amalgam fillings. Ideally DMPS should be administered after 25 grams of ascorbic acid administered intravenously. This will minimize any potential toxicity from the DMPS. 
Even though DMPS has a high affinity for mercury, the highest affinity appears to be for copper and zinc77 and supplementation needs to be used to not avoid depleting these beneficial minerals. Zinc is particularly important when undergoing mercury chelation.78 DMPS is administered over a five-minute period since hypotensive effects are possible when given intravenously as a bolus.79 80 Other possible side effects include allergic reactions and skin rashes. 
DMSA 

DMSA (meso-2, 3-dimercaptosucccinic acid) is another mercury chelating agent. It is the only chelating agent other than cilantro and d- penicillamine81 that penetrates brain cells. DMSA removes mercury both via the kidneys and via the bile.82 The sulfhydryl groups in both DMPS and DMSA bind very tightly to mercury. 
DMSA has three distinct disadvantages relative to DMPS. 
First, DMPS appears to remain in the body for a longer time than DMSA. 83 
Secondly, DMPS acts more quickly than DMSA, probably because its distribution is both intracellular and extracellular.84 
Thirdly, preparations of DMPS are available for intravenous or intramuscular use, while DMSA is available only in oral form.85 Since succinic acid is used in the citric acid cycle inside the cell, DMSA has been suspected for displacing mercury towards the inside of the cell86 after binding mercury somewhere on its way from the intestine to the succinic acid deficient cell. 
We propose therefore that DMSA be used late in the mercury elimination process, after the connective tissue mercury load has been reduced with DMPS. The standard dose of DMSA is 5-10 mg/kg twice a day for two weeks. The DMSA is then stopped for two weeks and then the cycle is repeated. 
Chlorella 

Algae and other aquatic plants possess the capacity to take up toxic trace metals from their environment, resulting in an internal concentration greater than those of the surrounding waters.87 This property has been exploited as a means for treating industrial effluent containing metals before they are discharged, and to recover the bioavailable fraction of the metal.88 
Chlorella has been shown to develop resistance to cadmium contaminated waters by synthesizing metal-binding proteins.89 A book written for the mining industry, Biosorption of Heavy Metals,90 details how miners use these organisms to increase the yield of precious metals in old mines. The mucopolysaccharides in chlorella's cell wall absorb rather large amounts of toxic metals similar to an ion exchange resin. 
Chlorella also enhances mobilization of mercury compartmentalized in non-neurologic structures such as the gut wall,91 muscles, ligaments, connective tissue, and bone. 
High doses of chlorella have been found to be very effective in Germany for mercury elimination.92 
Chlorella is an important part of the systemic mercury elimination program, as approximately 90% of the mercury is eliminated through the stool. Using large doses of chlorella facilitates fecal mercury excretion. After the intestinal mercury burden is lowered, mercury will more readily migrate into the intestine from other body tissues from where chlorella will effectively remove it. 
Chlorella is not tolerated by about one-third of people due to gastrointestinal distress. Chitosan can be effectively used as an alternative in these individuals. Chitosan makes up most of the hull of insects shellfish and also bind metals like mercury from the lumen of the intestines.93 94 95 
Cilantro 

Omura determined that cilantro could mobilize mercury and other toxic metals rapidly from the CNS.96 97 
Cilantro mobilizes mercury, aluminum, lead and tin stored in the brain and in the spinal cord and moves it into the connective tissues. The mobilized mercury appears to be either excreted via the stool, the urine, or translocated into more peripheral tissues. 
The mechanism of action is unknown. Cilantro alone often does not remove mercury from the body; it often only displaces the metals form intracellularly or from deeper body stores to more superficial structures, from where it can be easier removed with the previously described agents. The use of cilantro with DMSA or DMPS has produced an increase in motor nerve function.98 
Potentiating Agents 

Adequate sulfur stores are necessary to facilitate mercury's binding to sulfhydryl groups. 
Many individual's sulfur stores are greatly depleted which impairs sulfur containing chelating or complexing agents, such as DMPS or DMSA, effectiveness as they are metabolized and utilized as a source of sulfur. Sulfur containing natural substances, like garlic99 100 and MSM (methylsulfonylmethane) may also serve as an effective agent to supply organic sulfur for detoxification.101 Fresh garlic is preferred as it has many other recently documented benefits.102 103 104 The garlic is consumed just below the threshold of social unacceptability, which is typically 1-2 cloves per day. 
Antioxidants 

Vitamin E doses of 400 I.U per day have been shown to have a protective effect when the brain is exposed to methyl-mercury.68 105 Selenium, 200-400 mcg daily,106 107 108 109 is a particularly important trace mineral in mercury elimination and should be used for most patients. 
Selenium facilitates the function of glutathione, which is also important in mercury detoxification.110 111 112 Some clinicians find repetitive high dose intravenous glutathione useful, especially in neurologically compromised patients. 
There is a suggestion in a rat model that lipoic acid may also be useful,113 but some clinicians are concerned about the potential of lipoic acid to bring mercury into the brain early in the stages of chelation, similar to DMSA and N-acetylcysteine (NAC), which has also been used in mercury chelation.114 Doses larger than 50-100 mg per day should be used with caution. 
Vitamin C is also a helpful supplement for mercury elimination as it will tend to mobilize mercury from intracellular stores.115 116 117 118 119 120 
Some clinicians will use it intravenously in doses of 25-100 grams IV in preference to DMPS and DMSA. 
Hyaluronic acid (HA) is a major carbohydrate component of the extracellular matrix and can be found in the skin, joints, eyes and most other organs and tissues.121 HA is utilized in many chemotherapy protocols as a potentiating agent.122 HA is also being utilized for many novel applications in medicine.123 124 Personal experience has shown that the addition of 2 ml with the DMPS tends to improve the excretion of mercury by two to four fold with virtually no toxicity. 
Conclusion 

We have described the significant toxicities associated with mercury amalgams and treatment agents that both authors have used successfully over the past two decades to eliminate mercury and resolve many chronic health complaints. Considering the weight of evidence supporting amalgam toxicity it would seem prudent to select alternative dental restoration materials. 
Joseph Mercola, DO. Medical Director Optimal Wellness Center 1443 W. Schaumburg Schaumburg, IL 60194  Dietrich Klinghardt, M.D., Ph.D. Medical Director American Academy of Neural Therapy 2802 E.Madison #147 Seattle, WA 98112 
Bibliography 

---------------------------------------------------------------------------= ----- 
    POSTED BY Dr. Mercola 




 URL:  <URL>  

