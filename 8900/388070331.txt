


Hello, I am pleased to announce the first in a series of US speaking  engagements coming up - These events are designed to encourage debate and  discussion, so please forward to supporters and critics of the subject  alike! 


More information on other events in this series will be announced shortly. 


In addition to Brown, on June 19, I will also be speaking in Manhattan at  NYU Law School on Thursday, June 26 (hosted by NYU Law Professor Mary  Holland) and in Boston at Northeastern University on Friday, June 27 (hosted  by NE University Professor Dr. Richard Deth). 


Other events still to be confirmed for June and July will take place in Long  Island, Central New Jersey and Southern California. 


In addition to the events in this series, I am also speaking this Saturday  morning, June 14, in Dayton Ohio at the DAFEAT conference   <URL>  


And I am speaking at the SEPTA Spring Gala benefit in Pleasantville,  Westchester Co., NY on Monday June 23 from 5:30-8:00PM   <URL> / 


Cheers - DK 



THE VACCINE-AUTISM DEBATE: 
WHY WON'T IT GO AWAY? 
TOWN HALL DISCUSSION 


JOIN DAVID KIRBY, BESTSELLING AUTHOR OF THE BOOK "EVIDENCE OF HARM - MERCURY  IN VACCINES AND THE AUTISM EPIDEMIC: A MEDICAL CONTROVERSY" 
FOR A FREE LECTURE AND TOWN HALL DISCUSSION 



David Kirby, the New York based investigative journalist and author of the  NY Times Bestseller, "Evidence of Harm," will speak and take questions from  the public during a free event at Brown University. 



Thursday, June 19, 2008 
Brown University, The Salomon Center, Room 101, PROVIDENCE 
6:30 - 9:00 PM 




FREE AND OPEN TO THE PUBLIC 



For Exact Location and Directions, Please Visit www.brown.edu 


Among the subjects Kirby will address and take questions on: 


1)    A recent case in the US Vaccine Court in which the government conceded  that vaccines induced autism in one little girl, and updates on other cases. 
2)    Growing evidence of a link between mitochondrial dysfunction and  autistic regression, and case studies of several ASD children with  mitochondrial issues. 
3)    State-of-the-art research underway at top universities on the  connection between environmental toxins, mitochondrial function, oxidative  stress, glutathione depletion, neuro-inflammation and autistic  encephalopathy. 
4)    Declarations by US Presidential candidates that autism is epidemic and  calling for more research into vaccines and mercury as possible causes. 
5)    Recent studies linking ASD with heavy metals and contaminants in air  pollution. 


Kirby is a former contributor to The New York Times and a regular writer for  the The Huffington Post. This event is sponsored by Generation Rescue,  Autism Research Institute, National Autism Association, SAFE MINDS, and Talk  About Curing Autism. 


Information on Evidence of Harm is at www.evidenceofharm.com 
Kirby's Huffington Post essays may be viewed at  www.huffingtonpost.com/david-kirby 



