


Job Title:     Senior Stability Analyst/Stability Analyst Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-06-28 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Stability Analyst/Stability Analyst &#150; QUA001685 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The Stability Analyst or Senior Stability Analyst will be responsible for the design and execution of stability programs within the West Point Quality Operations Stability Unit. In this position, you will support the development of new products as well as monitor all licensed and in-line products to ensure compliance with established Quality Standards and procedures. In this position, you will have the opportunity to: * Assist with the development of stability strategies for post-approval changes to analytical methods or products. * Be responsible for the development and implementation of stability protocols, evaluation of stability data (including statistical analysis), and investigation of out-of-specification stability results in accordance with all applicable regulatory requirements. * Generate reports and/or prepare data summary tables and issue stability reports for annual product reviews and for use in regulatory submissions and inspections. * Provide 100% second person review support for team members stability Working knowledge of analytical or biological testing methods and GMP compliance is also necessary. You must also possess the ability to independently develop clear, accurate The ability to understand and utilize fundamental statistics is desirable.  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001650. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  06/26/2008, 11:45 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  07/10/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-408236 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



