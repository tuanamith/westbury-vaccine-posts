


Job Title:     Medical Records Coordinator Job Location:  MA: Medford Pay Rate:      Open Job Length:    full time Start Date:    2008-05-08 
Description:   Interested applicants should contact Linda Twohig at 781-391-5400 ex231, fax resume at 781-391-4254, or email  <EMAILADDRESS> . 




RESPONSIBILITIES/ACCOUNTABILITIES: 


1.  Maintains accurate order of open charts; 


2.  Types physicians orders/transcriptions as needed; 


3.  Prepares closed charts; 


4.  Assumes responsibility for diagnostic admission and discharge coding index; 


5.  Ensures that all Admission Orders are signed by the attending physician in a timely manner; 


6.  Ensures all Medicare Certifications and Re-certifications are signed in a timely manner; 


7.  Notifies MDs in reference to compliance of customer visits and progress notes; 


8.  Communicates with Pharmacy in reference to additions/deletions of customer MARs, POFs; 


9.  Assists nursing staff with monthly editing; 


10.  Tracks yearly CBC and UAs; 


11.  Initiates yearly flu/pneumonia vaccine program and maintains accurate record keeping; 


12.  Maintains forms/office supplies inventory under the direction of the Director of Nursing or his/her designee; 


13.  Answers telephone inquires and may answer correspondence; 


14.  Performs miscellaneous duties pertaining to medical records; 




16.  Obtains all appropriate departmental and physicians signatures on discharge charts within 30 days of discharge; 


17.  Assists Director of Nursing Services with any other duties that may be assigned; 






20.  Performs other duties as requested. 
SPECIFIC EDUCATIONAL/VOCATIONAL REQUIREMENTS: 


1.  The Medical Records Coordinator must have a high school degree. 




3.  Must be able to read, write and understand the English language. 




19871   


Please refer to Job code 19871 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



