



NEWS RELEASE 

For Release: Friday, January 30, 2009 
Contact Information Contact Name: Steve Baragona Contact E-mail:  <EMAILADDRESS>  Contact Phone: 703-299-0412 
Vaccines and Autism: Many Hypotheses, But No Correlation 

An extensive new review summarizes the many studies refuting the claim of a link between vaccines and autism.  The review, in the February 15, 2009 issue of Clinical Infectious Diseases and now available online, looks at the three main hypotheses and shows how epidemiological and biological studies refute these claims. 
=93When one hypothesis of how vaccines cause autism is refuted, another invariably springs up to take its place,=94 said study author Paul Offit, MD, of the Children=92s Hospital of Philadelphia.  Fears about vaccines are pushing down immunization rates and having a real impact on public health, he added.  Vaccine refusal is contributing to the current increase in Haemophilus influenzae cases in Minnesota=97 including the death of one child=97and was a factor in last year=92s measles outbreak in California. 
The controversy began with a 1998 study in The Lancet that suggested a link between the combination measles-mumps-rubella (MMR) vaccine and autism.  Dr. Offit and co-author Jeffrey Gerber, MD, PhD, also of the Children=92s Hospital of Philadelphia, reviewed more than a dozen large studies, conducted in five different countries, that used different methods to address the issue, and concluded that no data supported the association between the MMR vaccine and autism.  The correlation between MMR vaccine and the appearance of autism symptoms is merely coincidental, the authors say, because the MMR vaccine is given at the age when autism symptoms usually appear. 
Also hypothesized as a cause has been the ethylmercury-containing preservative thimerosal, which was used in vaccines for over 50 years.  However, the authors review seven studies from five countries that show that the presence or absence of thimerosal in vaccines did not affect autism rates. 
The third suggestion has been that the simultaneous administration of multiple vaccines overwhelms or weakens the immune system.  The authors explain that children=92s immune systems routinely handle much more than the relatively small amount of material contained in vaccines.  Furthermore, today=92s vaccines contain many fewer immune- triggering components than those from decades past. Regardless, autism is not triggered by an immune response, the authors say. 
With outbreaks of vaccine-preventable diseases on the rise due to some worried parents choosing not to vaccinate their children, Dr. Offit said, =93Parents should realize that a choice not to get a vaccine is not a risk-free choice.  It's just a choice to take a different, and far more serious, risk.=94 
### 
Founded in 1979, Clinical Infectious Diseases publishes clinical articles twice monthly in a variety of areas of infectious disease, and is one of the most highly regarded journals in this specialty. It is published under the auspices of the Infectious Diseases Society of America (IDSA). Based in Arlington, Virginia, IDSA is a professional society representing more than 8,600 physicians and scientists who specialize in infectious diseases. For more information, visit www.idsociety.org. 

Who loves ya. Tom 

Jesus Was A Vegetarian!  <URL>  

Man Is A Herbivore!  <URL>  

DEAD PEOPLE WALKING  <URL>  

