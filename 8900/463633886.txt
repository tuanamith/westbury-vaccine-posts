


On Wed, 25 Feb 2009 15:26:25 -0000, "JOHN" < <EMAILADDRESS> > wrote: 

Wonderfully inventive. 
"A key potential rebuttal witness, Dr Andrew Wakefield could not come forward because of the journalists long-running personal campaign to discredit research into the link between the MMR vaccine and autism. " 
He did come forward.  He was on the original list of witnesses the claimant was going to call and it was their lawyers who decided not to call him.  He is now resident in the USA and making a fortune out of peddling his discredited theories.  What was to stop him testifying? 
"Dr Andrew Wakefield, the British  gastroenterologist formerly of The Royal Free Hospital, London who first raised the alarm over a possible MMR vaccine-autism link has been silenced during the continuing controversial and long-running UK legal proceedings before the UK General Medical Council. " 
Far from being silenced it was Wakefield who worked assiduously to use the courts to silence his critics and suppress for years the evidence of his failure to find measles virus in the gut as he claimed to have done.   
"1997 clinical investigations by a team of 13 medical specialists at The Royal Free Hospital London into children with autism and serious bowel conditions claimed linked to the MMR vaccine" 
The study did not look at MMR at all and contains no reference to it. It most certainly did not claim any link between the two.  Only Wakefield (at a press conference) claimed that single vaccines (one of which he had fortuitously patented shortly beforehand) should be given in place of triple vaccine.  Most of his fellow researchers rapidly withdrew their names from the paper as it supported no such claim. 
"Journalist Brian Deer was also a defendant in litigation with Dr Wakefield." 
It would have been more honest to say he was a successful defendant. Wakefield sued a number of people and threatened others then proceeded to drag out the proceedings for as long as possible to suppress discussion.  When a Judge refused to allow him to delay prosecuting his case for even longer than he already had Wakefield withdrew all the allegations he had made and paid the costs of the defendants in full. 

