



In France, a certain number of vacinations are required to enter school. 
Aujourd'hui les maladies dont la vaccination est obligatoire à l'école sont les suivantes : la diphtérie, le tétanos, la poliomyélite et la tuberculose. D'autres sont vaccins sont vivement recommandées comme le ROR, contre la rougeole, les oreillons et la rubéole, et celui contre l'hépatite B pour les adolescents. 
rougeole = measeles, oreillons = mumps, rubéole=German measels. 

  May 2, 2008 
Measles in U.S. at Highest Level Since 2001 By DENISE GRADY 

Measles outbreaks in at least seven states are expected to produce more cases in 2008 than in any other recent year, federal health officials said Thursday, warning that measles is highly contagious and can cause severe illness and even death. 
Most of the cases have occurred in people who were never vaccinated. 
There were 64 cases from January through April 25, more than in all of 2006 and the highest number during that four-month period since 2001. None have yet proved fatal, but officials said they expected the total to keep rising. 
³We haven¹t seen the end of this,² said Dr. Anne Schuchat, director of the National Center for Immunization and Respiratory Diseases at the Centers for Disease Control and Prevention. 
Fourteen patients, or 22 percent, have been hospitalized, mostly for pneumonia. 
The largest outbreak, 22 cases, is under way in New York City, mainly in the Borough Park section of Brooklyn, where it was most likely introduced by travelers from other countries, including Israel and Belgium. 
³There may be more cases,² said Dr. Jane R. Zucker, assistant commissioner for the Bureau of Immunization in the city¹s Department of Health and Mental Hygiene. Dr. Zucker said the New York outbreak was still being investigated. 
As in New York, the other outbreaks are occurring because travelers bring the measles virus in from other countries  worldwide there are 20 million cases a year  and spread it to unvaccinated people. The unvaccinated include babies under a year old, who are too young to receive the vaccine, and children and young adults from families who refuse vaccination for personal or religious reasons. 
The disease can then keep spreading. Dr. Schuchat said doctors were finding clusters with as many as five generations of transmission. She said many of today¹s parents, doctors and nurses were unfamiliar with measles and not on the lookout for it. 
In 17 cases, patients were infected in clinics and doctors¹ offices, including a year-old baby who contracted the disease in a pediatrician¹s office during a routine visit  for a measles shot. 
Health officials are warning doctors and nurses to take special precautions to avoid spreading the disease in clinics. Children with fevers and rashes should not sit in waiting rooms, and other children should not be brought into an examining room that a child suspected of having measles has just left, because the virus can linger and remain infectious for about two hours. 
 In the current outbreak, 13 patients were under a year old and therefore too young to have been vaccinated, and 7 others were 12 to 15 months old, with parents who had not yet taken them for their first vaccination, which is due at 1 year. Sixteen others, who were older, came from families that refused vaccination. Fourteen more had what officials described as ³unknown or undocumented vaccination status.² Only one person had proof of having received the standard two doses of measles vaccine. 
In one family in Washington State, eight siblings came down with measles, and three of them had signs of pneumonia, a serious complication. These cases were reported after April 25 and so are in addition to the 64 described by the disease centers on Thursday. 
The eight siblings are believed to have contracted measles at a religious conference attended by about 2,000 people from 5 countries and 19 states. None of the eight had been vaccinated. Forty-eight states allow exemptions from vaccine requirements for religious reasons, and 21 for personal beliefs, the C.D.C. said. 
Growing numbers of parents in the United States and other countries have begun refusing to vaccinate their children because of unproven fears that vaccines cause autism or other illnesses. Health officials blame the trend for the resurgence of measles in many regions. Israel, Switzerland, Austria, Ireland and Britain have had large outbreaks recently, linked to pockets of people who shun vaccination. 
Given the outbreaks overseas, travelers need to be immunized, Dr. Schuchat emphasized, acknowledging that many people do not think of Europe or Israel as places where they have to worry about catching infectious diseases. Babies who are going to be taken on trips can be given a measles shot at 6 months instead of 1 year, officials said. 
People who have not been immunized and have been exposed to measles can often be protected with a vaccination or treatment with immune globulin, but the treatment must be given soon after the exposure. Health departments are supposed to track all the contacts of infected people and advise them about what to do, officials said. 
Counting the Washington occurrence, 10 states have measles cases, though only seven have three or more, the disease centers¹ definition of an outbreak. Besides New York City, the highest numbers are in Pima County in southern Arizona, with 15, and San Diego, with 11. The San Diego and Arizona cases have been traced to travelers from Switzerland. Cases in other states have come from Italy, India and probably China. 
The remaining states with cases are Hawaii, Illinois, Michigan, Wisconsin, Pennsylvania and Virginia. 
 ³I think it¹s important for states who aren¹t on that list to have their alerts up,² Dr. Schuchat said. ³We know there are unimmunized people out there, and measles is extremely infectious. Not being on the list shouldn¹t be reassuring.² 
Before 1963, when the vaccine became available in this country, there were three million to four million cases of measles annually. The disease killed 400 to 500 children a year and put 48,000 in the hospital. 
The vaccine wiped out transmission here by 2000, but the disease can easily be imported because there are so many cases overseas. Worldwide, measles still kills 242,000 children a year. 
A report on the outbreaks is online at cdc.gov. 
   


