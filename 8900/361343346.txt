


On Mar 16, 8:50 am, Nashton < <EMAILADDRESS> > wrote: 
Not exactly. You can't repeat a theory. 
You can repeat observations. Until they have been verified, they are not reliable. A theory is true if: 1. It fits all the verifiable facts. 2. It makes unique, testable predictions. When new facts come to light that contradict the predictions, the theory must be modified or discarded. 
In principle, it is possible for more than one theory to be true - that is, as true as any statement about the world can be. If they are incompatible, then further data will eventually leave one (or both) by the wayside. 

True, if by "dealing with anything philosophical" you mean agreeing with you. 

No, most, I think, do not. However, if religion destroys legitimate teaching of science in public schools, and the powers that be continue to teach anti-scientific attitudes, then all Hell could easily break loose. 
One example  would be anti-vaccine activists encouraging parents to not vaccinate their children; this could lead to epidemics of real and preventable diseases, costing money and possibly lives. 

Kermit 


