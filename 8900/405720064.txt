


Plaintiff's witness: Dr. Elizabeth Mumper Plaintiff's Counsel: Mr. Thomas Powers Respondent's counsel: Mr. Voris Johnson The Court: Special Master Denise Vowell 
--- 
MR. VORIS JOHNSON, DOJ: So in the medical records, there?s only one test that showed mercury outside of the reference range. Is that correct? 
DR. ELIZABETH MUMPER: That?s true. 
MR. JOHNSON: And that was the provoked test from September 22, 2002, is that right? 
DR. MUMPER: That?s correct. 
MR. JOHNSON: Doesn?t Doctor?s Data say in bold, right on the test report, that ?reference ranges are representative of a healthy population under non-challenged or non-provoked conditions?? 
DR. MUMPER: That?s true. 
MR. JOHNSON: So we just don?t know what the normal range would be for a provoked test. Is that right? 
DR. MUMPER: It is difficult to know what that would be on a provoked test on either sick populations or healthy populations. 
--- 
MR. JOHNSON: Would you agree that the single post-provocation test from September 2002 is the only evidence on the record specific to mercury? 
DR. MUMPER: That would be true. 
MR. JOHNSON: If that test result were not reliable ? take it away, you can?t rely on it ? would you still be able to offer an opinion in this case that thimerosal-containing vaccines contributed to Colin?s autism? 
DR. MUMPER: Without that piece of evidence, I would be left with a number of lab tests that would be consistent with but not specifically suggestive of that, so I guess that would be true. 
MR. JOHNSON: And the post-provocation test from September 2002 is not specific to a particular species of mercury, is that right? 
DR. MUMPER: That is true. 
MR. JOHNSON: So it tells us nothing about Colin?s exposure to ethylmercury as opposed to methylmercury, is that right? 
DR. MUMPER: That?s correct. 
MR. JOHNSON: And none of the other tests that you?re relying are diagnostic of mercury toxicity, is that right? 
DR. MUMPER: That?s correct. 
MR. JOHNSON: In fact, none of the other tests that you?re relying are diagnostic of exposure to mercury in any amount, is that right? 
DR. MUMPER: That would be true. 
--- 
MR. THOMAS POWERS: Whatever the reference is, would you describe Colin Dwyer?s post-provocation urine test where it was five times beyond the reference level ? would you describe that as normal or abnormal? 
DR. MUMPER: Abnormal. 
[five minutes later] 
MR. JOHNSON: Dr. Mumper, with respect to the September 22, 2002 post-provocation mercury test, you just testified that it is your belief that that result is abnormal. 
DR. MUMPER: That?s correct. 
MR. JOHNSON: There is no data that would support that statement, is that correct? There is no data to show what normal reference ranges would be for post-provocation testing, is that correct? 
DR. MUMPER: Uhm, to my knowledge, that is true. 
MR. JOHNSON: Thank you. 
SPECIAL MASTER DENISE VOWELL: Let me get this straight, Dr. Mumper, I want to make sure I understand that. If I took a hundred three-year olds off the street out in front of the White House today and we chelated them, you?re telling me that there is no data that would give us a reference range for where they would fall on mercury post-chelation? 
DR. MUMPER: I?m not aware that that has been done. It desperately needs to be done. It?s one of the things that we are doing at our research institute is to try to compare porphyrin testing in normal children versus controls. Because that data has not been established. It?s classically hard to get people to volunteer their children at very young ages for research experiments in which they?re being used just to set a control. I?ve tried to do it in my practice, especially if it involves anything either invasive or troublesome like taking home a kit and collecting a first morning urine and bringing it back. It?s difficult to get people to participate in that. But I agree that it definitely needs to be done. 
SPECIAL MASTER VOWELL: Okay. And there is no data, then, that would show in anyone the increase between pre-chelation and post-chelation levels of lead or mercury? 
DR. MUMPER: There is data that shows that it increases, but the quantification of the amounts that correlate with a specific body-burden have not been determined to my knowledge. 
SPECIAL MASTER VOWELL: When we chelate and we measure the amount of mercury excreted afterwards ? mercury, lead, whatever heavy metal ? you?re saying ? I understood that to be a measurement of body-burden. 
DR. MUMPER: Right. It is reflective of an increased body-burden. I?m saying that what I don?t have the data to tell you that a four-year old child would go from .01 mcg/g creatinine to 17 mcg/g creatinine if he had a total body-burden of X grams of mercury. I don?t know how to get that information. 
SPECIAL MASTER VOWELL: What I?m having trouble understanding is why you can say that 17 is extraordinarily high. What do you base that on? I?m not arguing with you, Doctor, I just want to understand what the basis for your opinion is if you have no reference. 
DR. MUMPER: The basis for my opinion is, I would have to say, is looking, is discussions with leaders in the toxicology field and extrapolations from experiences in older populations, but there is a dearth of that information in the pediatric population. 
SPECIAL MASTER VOWELL: Okay. Questions from either side based on mine? ?All right. Dr. Mumper, you may step down. 

