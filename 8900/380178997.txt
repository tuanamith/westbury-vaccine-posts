


Pat's Note: I find it increasing perverse that the government veterinary industry, although rightly much reduced in size, as all failed industries should be, can afford so much time attacking hobby and small farmers. 
I appreciate that hobby farmers generally stand up to the state vets bullying, but that is no reason to blame the small people for the long series of animal and now human health fiascos in Britain. 
That is down to Britain's corrupt and incompetent government vets. 
The Orwellian future prophesied and much encouraged by vets desperate for guaranteed work for their increasing surplus of young vets, is neither wanted nor valued by anyone else. 
They would be better getting does to the task of testing Britain's sick pigs for MRSA and C.Diff. Their refusal to do so has been well documented here and will be taken as a sign of guilt that will need explaining. 
Having a pop at hobby farmers will do nothing to help get them off the hook and will just make some of us ever more determined to force a full scale criminal investigation into just what has been going on for the last decade inside the government veterinary service. 
I rather think it is a case of hobby farmers teaching the government vets their responsibilities in respect of animal health. 
Arrogance before the downfall. 
 <URL> / 
Chief vet predicts a polarised future for farming industry May 20 2008 by Sally Williams, Western Mail  
THE next decade will see a polarisation of the agricultural industry in Wales and the deadly bluetongue disease is likely to be endemic. 
This is the candid view of Chief Veterinary Officer for Wales, Dr Christianne Glossop. 
We asked her what she thought Welsh farms were going to be like in 2018; and what impact diseases such as bluetongue, foot-and-mouth, avian flu and bovine TB will have on Welsh agriculture by that date. 
She responded: I envisage there being a number of very large, professionally-run farms and big dairy farms in Wales. 
A polarisation of the industry could lead to large professional outfits. 
They will be organising regular veterinary visits to their farms, as part of their extensive animal health plans. 
And they will give a high priority to bio security. 
We will still have hill farmers. But some may find it difficult to afford the economics of production and the vaccines and medicines that need to be in place. 
There is likely to be an increase of hobby farmers in Wales too. 
The challenge for us will be to make sure that they understand their responsibilities. 
Because even though farming is a hobby to them, any animal health problems they could encounter are likely to have a huge impact on the large professionally run farm next door. 
If a farmer is not full-time, they might not check their animals so often and that is how diseases like foot-and-mouth might not be identified. 
We will need to keep on top of that. And we will need to get our heads around these three groups of farmers, their approach to animal health and how they farm the Welsh countryside. 
How it all shapes up is anyones guess. 
Regarding the challenges presented by animal diseases in future, Dr Glossop admitted many of the current challenges and threats were likely to continue to be prevalent. 
With avian flu, like today, we could still get a swan with the virus landing in Cardiff Bay, she warns. But in 10 years time, we are likely to be regarding bluetongue as endemic in the country, rather than an exotic disease. 
This is not defeatist. It is logical. We cant get rid of every midge. 
We dont know when the disease will first come, and vaccines are still being prepared. 
We have vaccines for type eight. But there are likely to be other types. 
In 10 years time, not just one but two or three types of bluetongue are likely to be endemic. We will be learning how to live with it and it wont be simple. 
But all the protection zones will be sorted out by then and private vets will be taking more of a leadership role working with farmers. 
She also admitted that further cases of foot-and-mouth could not be ruled out either. I dont anticipate vaccination for all diseases because it makes no sense economically when the chances are we will not get it and not all vaccines are appropriate. 
We got rid of BSE with a huge determined effort. And by 2018, we shall be well over halfway through our bovine TB evaluation programme. 
I would expect that the annual rate of increase of the disease would have levelled off by then and it would be on a downward turn. 
But for that to happen, it is important for people to buy into our programme this year. 
TB in cattle will not be eradicated by 2018; it will take years. 
It is possible to eradicate bTB but the wildlife element is diverse and makes it harder. 
But in 10 years time the pain of tackling the disease would have been worth it, even though the end game will not necessarily be in sight. 
--  Regards Pat Gardiner Release the results of testing British pigs for MRSA and C.Diff now! www.go-self-sufficient.com  





