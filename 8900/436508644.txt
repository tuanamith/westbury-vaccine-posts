


On Nov 18, 8:08=A0am, rbwinn < <EMAILADDRESS> > wrote: 
That's true, but that didn't just suddenly start happening in 1905. I still don't know why you think relativity has anything to do with the pattern of public investment in science. There is more money poured into medical research by the government than there is into physics. This suggests to me that the vaccination for polio is more responsible for public funding for science than relativity. Why aren't you complaining about the polio vaccine? 

That is a simple error of fact, Bobby. Alternative theories have been continually checked the whole time, and public funds have been spent doing so. Does this surprise you? 

On the contrary, killing relativity would immediately generate a Nobel prize and a rash of new funding. It would seem to be more motivating to try to kill it. 

Then I believe you have a couple of options: - Campaign for, and elect, public stewards that are more sympathetic to your way of thinking. - Remove yourself from the system you don't believe in. 
I have to tell you, Bobby, that I don't think many people have much sympathy for folks that just like to complain about a situation but not do anything about it. 

I take it you feel the same way about military folks who happen to have graduated from college, and who being supported by taxes while they serve. I take it you feel the same way about fire fighters, police officers and elementary school teachers. You do know that elementary school teachers all went to college, don't you? 

The easiest way to do that, Bobby, is to relocate where you are no longer under the influence of the government. This seems to be a whole lot more productive than whining about this government, don't you think? 

Not at all. Convincing you is not the reason I post to you. Why would you expect it to be? 
PD 

