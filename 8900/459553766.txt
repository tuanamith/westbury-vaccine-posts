


"Call for Formal Enquiry into Wakefield Witch-hunt" 




Dear Friends 

We really need your help and support in signing the petition linked below -  ignore the donation request it is not relevant to our campaign.  Dr. Andrew  Wakefield has worked so hard to maintain his work and to help autistic  children and he (and his family) have paid the high price of leaving our  home country in order that this can continue. Now, 5 years on with a 2 year  disciplinary enquiry still ongoing the smear has started again and it is  clearly intended to destroy his reputation world-wide. The evidence  presented to the UK enquiry by Andy and his two colleagues went so well that  it would seem that this is a last ditch attempt and one which must be  thwarted. 


Please sign the petition with everyone in the family also signing and  forward it to as many people as you possibly urgently. 
Thanks so much. 


www.ipetitions.com/petition/wakefield 




(Ignore the second screen where it asks you to pay - you can just log out). 


Isabella Thomas 


The petition 


Five years after launching a campaign to discredit the work of Dr Andrew  Wakefield, freelance journalist Brian Deer and the Sunday Times in the UK  made additional allegations on Sunday 8th February 2009. Three pages of  articles while offering no new evidence accused Dr Wakefield of having  'fixed' research data. These allegations have no basis in fact and have been  fully addressed during Dr Wakefield's response to the GMC prosecution, now  well into its second year. 


This petition will bring attention to the scientific evidence in support of  Dr Wakefield's position and demand a formal enquiry into the activities of  Brian Deer, the Sunday Times and their connection and co-operation with the  vaccine industry. 


Throughout the 1990's Dr Andrew Wakefield, a research scientist at the Royal  Free Hospital and his colleagues, were contacted by parents of a large  number of children suffering from a form of inflammatory bowel disease with  a regressive developmental disorder. Many of the parents who brought these  children to the hospital reported that their children had become seriously  ill after receiving the MMR vaccination. 


In February 1998, a press briefing was organized by the Dean of the medical  school at the Royal Free Hospital to coincide with the publication of a  peer-reviewed case series in the The Lancet. At the press briefing, Dr  Wakefield suggested the precautionary alternative that there could be a  return to single vaccines whilst concerns regarding MMR were investigated.  This signalled the beginning of an orchestrated and oppressive campaign to  discredit Dr Wakefield. 


In 2004, Brian Deer lodged a complaint against Dr Wakefield, with the  General Medical Council (GMC) of the UK. Since that time Deer has been  supported in his drive to discredit Dr Wakefield by influential lobbyists  and lobby groups supported by major pharmaceutical companies. No parent has  ever complained to the GMC about Dr Wakefield, or the other doctors whose  cases are only now being heard. 


The GMC took more than three years to frame their charges against Dr  Wakefield, Professor Simon Murch and Professor Walker-Smith, none of which  questioned the underlying scientific and medical findings in the original  paper. 


The 'trial' of these three doctors - all co-authors of The Lancet paper -  began in July 2007 and although this hearing was due to finish within four  months, it is still continuing, almost two years later. 


In the attempt to discredit Dr Wakefield and the other defendants, the  prosecution, employed and paid for by the GMC, has suggested that none of  the children referred to the Royal Free Hospital were actually ill. In order  to support this suggestion, they have had to ensure that none of the parents  of vaccine damaged children appear at the hearing. Meanwhile, in the  background, Brian Deer has mounted significant attacks on these parents,  accusing them of manufacturing information about the illnesses suffered by  their children, and about its cause. 


The GMC panel finished hearing the evidence and, at the time of writing, has  been in recess awaiting the closing speeches of counsel, due to begin in  March. The prosecution, during presentation of evidence from 'witnesses to  fact' and the cross examination of the three defendants, has consistently  failed to make a case out of Deer's allegations. In the last weeks of the  hearing, it became clear that this failure was disturbing Deer. As his case  collapsed, he resorted to writing to the GMC re-affirming his views about Dr  Wakefield's unproven guilt and instructing the GMC in how they should have  conducted the prosecution. 


So upset was Deer by the evidence of Professor Murch who claimed that Deer  might have breached the Data Protection Act and gained access to patient  records, that he allegedly confronted and attempted to intimidate the  professor in the foyer of the GMC hearing rooms while Professor Murch was  giving evidence. 


In the new attack on Dr Wakefield that appeared in the Sunday Times on  February 8th 2009 Deer cited no new evidence. Furthermore, if allegations  relating to 'altered records' held any merit at all, they would have been  included in the hundred odd original GMC charges that the prosecution pored  over for three years, whilst preparing its case. 


The current attack comes five years - almost to the day - after Deer first  began his campaign against Dr Wakefield in February 2004. It also comes as a  clear indication that the government, the vaccine industry and the Sunday  Times are fearful of losing 'their' case at the GMC. There is a  transatlantic aspect to this fear, as more legal and medical decisions are  made public in the US, which endorse the clinical and research positions  taken by Dr Wakefield and his colleagues in Britain. 


This fear of losing is generated by the fact that if the GMC do not find the  three doctors guilty of the charges against them, the voice of the parents  will finally be heard and the scandal of over one and a half thousand  unrecognised vaccine damaged children in the UK and many, many thousands  worldwide will be brought to the attention of the public. 


Deer's new attack on Dr Wakefield is equally an attack on the parents and  children who sought treatment at the Royal Free Hospital and represents a  new low for British journalism. The parents of vaccine damaged children will  not be silenced and research into possible causes and treatments will  continue. Members of the public, parents, doctors and scientists worldwide  are now calling for a formal enquiry into the activities of Brian Deer, the  Sunday Times and their connection and co-operation with the vaccine  industry. 


In the following weeks, the organisations endorsing this statement will join  together in providing clear documentation in support of the science to date.  Please follow the links given in the right hand panel for a progress update. 



