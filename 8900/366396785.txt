



"trinlay" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
Well, disability means a tight budget and I don't envy you that but I'm glad  to hear that you are still creative and functioning still. 

And I can relate to that.  I remember a childhood illness that left me weak  and fatigued.  I was nine.  It could possibly have been a mild case of polio  caused by the vaccine, according to this Dr..  Forever after I have suffered  the most debilitating episodes after "normal" physical activities like  horseback riding (which I used to love) or a camping trip.  Now days I can't  drive for more than a few hours at a time or I'm frazzled the next day.  I  try to excercise and I'm so sore no one can touch me.  I have been to Dr  after Dr and there's "nothing" wrong with me - until finally I got some  better results, thank goodness. 

Yes, I'm trying hard not to "overdo" when I do feel good.  That's hard  because I'm sooo type A - driven to "finish".  Doing a little at a time  helps and I have to let some things go - with claw marks all over them, of  course :)  I got a message today to start Vitamin D and Fish Oil (yek) and I  will.  I am noticing that my feet do not hurt so badly in the mornings - any  improvement is welcome. 

There is alt.med.fibromyalgia also.  I found many good links as well as  people there too.   Do you find yourself unable to tolerate crowds of  people?  I was in Home Depot (in Arizona) and I nearly ran out of the place.  I was tired already and suddenly I just couldn't stand the hustle & bustle -  the stuff piled to the ceiling and the NOISE.   I'd almost say it was a  panic attack but I wasn't afraid, just very irritated - too stimulated.  A  friend suggested it might have been caused by the air conditioning, which I  am unaccustomed to.  Ever happen to you? 

Happy Birthday!!!!  I'd love to have a cat fountain - our water here is so  full of lime, I'm afraid it would clog up though.  A coffee pot lasts on  average 4 months then it won't pump the water any more in spite of weekly  cleaning with vinegar.  Rats! 

I'm glad you're doing ok.  I hope you'll do even better.  I am very lucky to  have a very tolerant and understanding boss.  If I can't make it in the  morning, I can go in later in the day.  My job is rarely stressful and I'm  learning to keep the drama from other people from distressing me.  Stay in  touch and share new developments, ok? 
How old is KT now?  Sheesh - time just runs so fast anymore :) 
MK  



