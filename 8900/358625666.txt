


Anonymous Infidel - the anti-political talking head wrote:  and ow dare ey. How $ on a 1 d? ne ow 


 <URL>  www.usatoday.com/news/politicselections/nation/president/2004-01-25-dean-= iraq-today_x.htm 


Living Conditions in Iraq: A Criminal Tragedy by Ghali Hassan 

www.globalresearch.ca 3 June 2005 
The URL of this article is:  <URL> = l 

History will acknowledge that the criminal policy of the U.S-Britain and = 
the illegal invasion of Iraq led to the current tragedy of the Iraqi=20 people. In addition, history will have to acknowledge that the Iraqi=20 people, alone, have resisted the genocidal sanctions and the U.S-British = 
Occupation of their country. 

A detailed study by the U.N. and Iraqi officials found that life in Iraq = 
has decayed significantly since U.S-led foreign forces invaded and=20 occupied the country, following a general trend seen in most sectors=20 since the imposition of the genocidal sanctions in 1990. Iraqi=20 civilians, mostly children, have suffered the consequences of this=20 criminal tragedy. 

The United Nations Development Program (UNDP) conducted the survey=20 (study), titled "Iraq Living Conditions Survey 2004," (ILCS) in=20 cooperation with Iraq's Ministry of Planning under Occupation. It should = 
be noted that the study is not independent. The survey was conducted by=20 Iraqi officials, who are serving the Occupation, with officials from the = 
U.N. 

Iraq had one of the best national health-care systems in the Middle=20 East. For example, Saudi Arabia with all her petrodollar earnings had=20 just a fraction of that of Iraq's. 

Iraq boasted a modern social infrastructure with a first-class range of=20 health-care facilities, and the Iraqi people enjoyed one of the highest=20 standards of living in the Middle East. In 1991, there were 1,800=20 health-care centres in Iraq. More than a decade later, that number is=20 almost half, and almost a third of them require major rehabilitation.=20 Iraq had used its oil revenues, which accounted for 60% of its gross=20 domestic product (GDP), to build a modern health-care system with large=20 Western-style hospitals and modern technology. Iraqi medical and nursing = 
schools attracted students from throughout the Middle East, and many=20 Iraqi doctors were trained in Europe or the U.S.A. Primary health-care=20 services reached about 97% of the urban population and 78% of the rural=20 population in 1990. But the Gulf war of 1991 and more than 13 years of=20 U.S-Britain sponsored genocidal sanctions have left the country's=20 economy and infrastructure in ruins. 

UNICEF reported on March 28, 2003 that, "The Education system in Iraq,=20 prior to 1991, was one of the best in the region, with over 100% Gross=20 Enrolment Rate for primary schooling and high levels of literacy, both=20 of men and women. The Higher Education, especially the scientific and=20 technological institutions, was of an international standard, staffed by = 
high quality personnel". In the 1980s, a successful government program=20 to eradicate illiteracy among Iraqi men and women was implemented. 

According to the World Health Organisation (WHO), 
"Iraq had a modern sanitary infrastructure with an extensive network of=20 water-purification and sewage-treatment systems. Water networks=20 distributed clean, safe water to 95% of the urban population and to 75%=20 of those in rural areas. In 1990, Iraq was ranked 50th out of 130=20 countries on the UNDP Human Development Index, which measures national=20 achievements in health, education, and per capita GDP". 

It has fallen to 127, one of the most dramatic declines in human welfare = 
in recent history, as a result of the U.S-Britain-sponsored sanctions=20 and wars, which needlessly killed civilians en mass. 

The UN ILCS study , which took less than five months to complete and=20 covered all of Iraq's provinces, reveals that some 24,000 Iraqis, 12 per = 
cent of them children under the age of 18 years old, died as a result of = 
the U.S-British invasion and the first year of Occupation. The three=20 volumes report, which was based on interviews conducted with some 22,000 = 
Iraqi households in 2004. The report estimates that the total number of=20 Iraqi deaths is between 18,000 and 29,000. However, this estimate is=20 misleading and does not take into account households where all members=20 were lost, crimes that occurred very often in the indiscriminate=20 bombings of population centres. 

The most credible study so far was published in November 2004 in the=20 Lancet, the highly reputable British medical journal. It shows that U.S. = 
occupation forces in Iraq have killed more than 100,000 civilians=20 between March 2003 and October 2004, the great majority of them are=20 women and children. The estimate is considered "conservative" because it = 
excludes the high death toll in areas such as Fallujah, where the U.S.=20 committed crimes against humanity by obliterating the entire city of=20 300,000 people. Further, the Lancet study also shows that 14 per cent of = 
U.S. soldiers and 28 per cent of U.S. marines had killed a civilian:=20 U.S-authorised war crimes ignored in the ILCS Report. 

Consistent with other studies, the ILCS study reveals that Iraqi=20 civilians, mostly children, have suffered from lack of health care and=20 adequate nutrition. The Data shows that 23 per cent of children under=20 the age of 5 suffer from chronic malnutrition, and 12 percent suffer=20 from general malnutrition, 8 per cent suffer acute malnutrition. 

In a study published in November, the Norwegian-based Fafo Institute for = 
Applied Social Science found that acute malnutrition among Iraqi=20 children between the ages of six months and 5 years has increased from=20 4% before the invasion to 7.7% since the US invasion of Iraq. In other=20 words, despite the 13-years sanctions, Iraqi children were living much=20 better (by 3.7%) under the regime of Saddam Hussein than under the=20 Occupation. Officials from the Institute revealed that the Iraqi=20 malnutrition rate is similar to the level in some hard-hit African=20 countries. A generation ago, obesity was the main nutrition-related=20 public health concern, today at 7.7 per cent, Iraq's child malnutrition=20 rate is roughly equal to that of Burundi, an African nation ravaged by=20 more than a decade of war. The study was substantiated by new study=20 prepared for the U.N. Human Rights Commission by the reputed Swiss=20 professor of Sociology and expert on the right to food, Dr. Jean Ziegler.= 


Infant mortality and malnutrition findings show clearly that, ''the=20 suffering of children due to war and conflict in Iraq is not limited to=20 those directly wounded or killed by military activities",' says the=20 study. With children under the age of 15 make up 39 per cent of the=20 country's total population of 27 million, the ILCS study notes that,=20 "Most Iraqi children today have lived their whole lives under sanctions=20 and war". In other words, most Iraqi children today have lived their=20 lives in constant fear of U.S-British sponsored terrorism. "We find=20 record of not a single significant demonstration protesting the=20 wholesale destruction of Iraqi children," wrote Professor Ward Churchill = 
of the University of Colorado. 

A detailed study by the British-based charity organisation (Medact) that = 
examines the impact of war on health, revealed cases of=20 vaccine-preventable diseases were rising and relief and reconstruction=20 work had been mismanaged. Gill Reeve, deputy director of Medact, said,=20 "[t]he health of the Iraqi people has deteriorated since the 2003=20 invasion. The 2003 war not only created the conditions for further=20 health decline, but also damaged the ability of Iraqi society to reverse = 
it". 

And as a consequence of the war, "Hundreds of thousands of children born = 
since the beginning of the present war [March 2003] have had none of=20 their required vaccinations, and routine immunization services in major=20 areas of the country are all but disrupted. Destruction of refrigeration = 
systems needed to store vaccines have rendered the vaccine supply=20 virtually useless", writes Dr. C=E9sar Chelala, an international public=20 health consultant. "Even antibiotics of minimal cost are in short=20 supply, increasing the population's risk of dying from common=20 infections. Hospitals are overcrowded, and many hospitals go dark at=20 night for lack of lighting fixtures. The Iraqi minister of health claims = 
that 100 percent of the hospitals in Iraq need rehabilitation", added=20 C=E9sar Chelala. The "current major problems" includes "lack of health=20 personnel, lack of medicines, non-functioning medical equipments and=20 destroyed hospitals and health centres", the study reveals. It is a=20 U.S-made and a U.S-accelerated tragedy. 

After health, Iraq's education system has also deteriorated. Again,=20 Iraqis youngsters are hard hit under Occupation. The literacy rate among = 
Iraqis between the ages of 15 and 24 is just 74 per cent, which is=20 according to the study is only "slightly higher than the literacy rate=20 for the population at large". The figure is lower than that for those=20 25-34, "indicating that the younger generation lags behind its=20 predecessors on educational performance", said the study. As a result of = 
high unemployment (over 70 per cent), males have neglected their=20 education and are in search of work to support their families. Contrary=20 to the ILCS study, like males, women literacy has declined markedly. 

In reference to the past, the study acknowledge that while the previous=20 regime (of Saddam Hussein) built up many of the country's service=20 networks, like electricity grids, sewage systems and water, the systems=20 are widely in disrepair, the study reveals. However, in scathing over=20 the sanctions and war, the ILCS study fails to condemn and attribute the = 
causes of Iraq's current conditions to the deliberate and systematic=20 U.S-British bombings campaign (since 1991) to destroy the entire of=20 Iraq's civilian infrastructure, including water purification plants,=20 sewage treatment plants, electricity grids, and communications. 

The deliberate destruction of Iraq's water and sewage systems by U.S.=20 bombings has been the major cause (for a decade) of an outbreak of=20 diarrhoea and hepatitis, particularly lethal to pregnant women and young = 
children. Diarrhoea killed two out of every 10 children before the 1991=20 Gulf War and four in 10 after the war. The study indicates that only 54=20 per cent of households nationwide have access to a "safe and stable"=20 supply of drinking water. An estimated 722,000 Iraqis, the report also=20 notes, rely on sources that are both unreliable and unsafe. 

Conditions are worse in rural areas, with 80 per cent of families=20 drinking unsafe water, the report says. According to researchers, "the=20 situation is alarming" in the southern governorates of Basra, Dhiqar,=20 Qadisiyah, Wasit, and Babel, located near the Tigris and Euphrates=20 Rivers. A large percentage of the population in this region relies on=20 water from polluted rivers and local streams. 

Although 98 per cent of Iraqi households are connected to the electrical = 
grid, 78 per cent of them experience "severe instability" and low=20 quality in the service, according to the survey. One in three Iraqi=20 families now relies on electricity generators, most of which are shared=20 between households. In all, daily living conditions under the Occupation = 
have deteriorated markedly. 

According to Barham Salih, Iraq's minister of planning, "This survey=20 shows a rather tragic situation of the quality of life in Iraq". Staffan = 
de Mistura, the U.N. secretary general's deputy special representative=20 in Iraq, said the study "not only provides a better understanding of=20 socio-economic conditions in Iraq, but it will certainly benefit the=20 development and reconstruction processes". The study will help address=20 the grave disparities between urban and rural [areas] and between=20 governorates in a more targeted fashion", Mistura added. 

Despite its reluctant to blame this criminal tragedy on U.S-Britain=20 genocidal policy toward Iraq and the violent Occupation, the ILCS study=20 is a strong indicator of a failed colonial policy and an illegal war of=20 aggression against the Iraqi people. The 'world community' should use=20 the study as a benchmark to demand the full withdrawal of U.S-British=20 forces from Iraq and prevent the acceleration of this criminal tragedy. 






on, 
ou 
Sonny, you won't gain any understanding of world events from Hannity,=20 O'Really, Limburger, or george's press briefings. 
If you had any evidence to back up any opinion you express as fact,=20 you'd provide it. But then, fox news doesn't provide anything but opinion= =2E 



LOL. Sonny, allow me to make it easier for you. 
Progressives =3D all that is good. Conservatives =3D EVIL in everything they do. 




NOTE: 
one of the things the Iraqi government has failed to provide is that oil = 
contract Bush demands. You know the one-- it forces Iraq to give a huge=20 percentage of the profits from Iraqi oil to international oil companies. = 
63% of Iraqi citizens oppose this privatization plan. 


Published on Thursday, March 17, 2005 by CommonDreams.org Secret U.S. Plans For Iraq's Oil by Greg Palast 
The Bush administration made plans for war and for Iraq's oil before the = 
9/11 attacks sparking a policy battle between neo-cons and Big Oil,=20 BBC's Newsnight has revealed. 
Two years ago today - when President George Bush announced US, British=20 and Allied forces would begin to bomb Baghdad - protestors claimed the=20 US had a secret plan for Iraq's oil once Saddam had been conquered. 
In fact there were two conflicting plans, setting off a hidden policy=20 war between neo-conservatives at the Pentagon, on one side, versus a=20 combination of "Big Oil" executives and US State Department "pragmatists.= " 
"Big Oil" appears to have won. The latest plan, obtained by Newsnight=20 from the US State Department was, we learned, drafted with the help of=20 American oil industry consultants. 
Insiders told Newsnight that planning began "within weeks" of Bush's=20 first taking office in 2001, long before the September 11th attack on=20 the US. 
An Iraqi-born oil industry consultant Falah Aljibury says he took part=20 in the secret meetings in California, Washington and the Middle East. He = 
described a State Department plan for a forced coup d'etat. 
Mr. Aljibury himself told Newsnight that he interviewed potential=20 successors to Saddam Hussein on behalf of the Bush administration. 
Secret sell-off plan 
The industry-favored plan was pushed aside by yet another secret plan,=20 drafted just before the invasion in 2003, which called for the sell-off=20 of all of Iraq's oil fields. The new plan, crafted by neo-conservatives=20 intent on using Iraq's oil to destroy the Opec cartel through massive=20 increases in production above Opec quotas. 
The sell-off was given the green light in a secret meeting in London=20 headed by Ahmed Chalabi shortly after the US entered Baghdad, according=20 to Robert Ebel. Mr. Ebel, a former Energy and CIA oil analyst, now a=20 fellow at the Center for Strategic and International Studies in=20 Washington, flew to the London meeting, he told Newsnight, at the=20 request of the State Department. 
Mr Aljibury, once Ronald Reagan's "back-channel" to Saddam, claims that=20 plans to sell off Iraq's oil, pushed by the US-installed Governing=20 Council in 2003, helped instigate the insurgency and attacks on US and=20 British occupying forces. 
"Insurgents used this, saying, 'Look, you're losing your country, your=20 losing your resources to a bunch of wealthy billionaires who want to=20 take you over and make your life miserable," said Mr Aljibury from his=20 home near San Francisco. 
"We saw an increase in the bombing of oil facilities, pipelines, built=20 on the premise that privatization is coming." 
Privatization blocked by industry 
Philip Carroll, the former CEO of Shell Oil USA who took control of=20 Iraq's oil production for the US Government a month after the invasion,=20 stalled the sell-off scheme. 
Mr Carroll told us he made it clear to Paul Bremer, the US occupation=20 chief who arrived in Iraq in May 2003, that: "There was to be no=20 privatization of Iraqi oil resources or facilities while I was involved."= 

The chosen successor to Mr Carroll, a Conoco Oil executive, ordered up a = 
new plan for a state oil company preferred by the industry. 
Ari Cohen, of the neo-conservative Heritage Foundation, told Newsnight=20 that an opportunity had been missed to privatize Iraq's oil fields. He=20 advocated the plan as a means to help the US defeat Opec, and said=20 America should have gone ahead with what he called a "no-brainer" decisio= n. 
Mr Carroll hit back, telling Newsnight, "I would agree with that=20 statement. To privatize would be a no-brainer. It would only be thought=20 about by someone with no brain." 
New plans, obtained from the State Department by Newsnight and Harper's=20 Magazine under the US Freedom of Information Act, called for creation of = 
a state-owned oil company favored by the US oil industry. It was=20 completed in January 2004, Harper's discovered, under the guidance of=20 Amy Jaffe of the James Baker Institute in Texas. Former US Secretary of=20 State Baker is now an attorney. His law firm, Baker Botts, is=20 representing ExxonMobil and the Saudi Arabian government. 
View segments of Iraq oil plans at: www.GregPalast.com/opeconthemarch.htm= l 
Questioned by Newsnight, Ms Jaffe said the oil industry prefers state=20 control of Iraq's oil over a sell-off because it fears a repeat of=20 Russia's energy privatization. In the wake of the collapse of the Soviet = 
Union, US oil companies were barred from bidding for the reserves. 
Jaffe said "There is no question that an American oil company ... would=20 not be enthusiastic about a plan that would privatize all the assets=20 with Iraq companies and they (US companies) might be left out of the=20 transaction." 
In addition, Ms. Jaffe says US oil companies are not warm to any plan=20 that would undermine Opec, "They [oil companies] have to worry about the = 
price of oil." 
"I'm not sure that if I'm the chair of an American company, and you put=20 me on a lie detector test, I would say high oil prices are bad for me or = 
my company." 
The former Shell oil boss agrees. In Houston, he told Newsnight, "Many=20 neo-conservatives are people who have certain ideological beliefs about=20 markets, about democracy, about this that and the other. International=20 oil companies without exception are very pragmatic commercial=20 organizations. They don't have a theology." 
Greg Palast's film - the result of a joint investigation by BBC=20 Newsnight and Harper's Magazine - will broadcast on Thursday, 17 March,=20 2005. You can watch the program online - available Thursday, March 17=20 after 7pm EST for 24hrs - from the Newsnight website:=20  <URL> . You can=20 also read the story in greater detail in the latest issue of Harper's=20 magazine - now available at your local newsstand. 


s 
no problem. I'll repost it. 
Contrary to what you righturds think, the oil market is controlled now=20 as it has always been by the big oil companies. They pocketed more than=20 $100 billion in the past year: they certainly have enough cash on hand=20 to cut out that middle man broker they blame for inflating the prices. 

eas g ot 
l h 
p? ] l 

of course they cost more--now. Force the oil companies to invest a=20 portion of their amoral profits in alternative energy-- by, say,=20 installing solar panels on the roofs of a few thousand homes--and you'd=20 see a marked decrease in energy demand here in the US. 

a 

"Sry," I think you are laughably ignorant. For every action there is an=20 equal reaction. Pure science, nothing "primative" about it. Life itself=20 is nothing more than action/reaction of a mixture of carbon, minute=20 amounts of other minerals, and water. Sun spots send out heat, and=20 radiation that soar through the universe, through all things living and=20 dead, and all sorts of things happen here on Earth. 
Radiation. What is radiation exactly? do you have a clue? Radioactive=20 materials leak subatomic particles that then penetrate all manner of=20 things. These active, high energy particles are unstable, and thusly=20 destabilize the materials they permeate. All things --even inanimate=20 objects--interact to some small degree with all other things in their=20 environment. And the center of the Earth is itself a molten nuclear=20 fire. Commonsense alone ought to tell even you that the Earth itself=20 does indeed respond to what happens in its atmosphere, on its surface,=20 and even miles beneath its surface. 
n 
my point, doofus, is that billionaires don't lose money. only the little = 
people lose, and they lose at an especially high rate when the=20 billionaires win the most. The Great Depression caused thousands to die=20 of starvation and malnutrition; but, it also created dozens of new=20 multi-millionaires who benefited from the suffering of so many. 



No, doofus, he lost no money. It was an unsecured loan, doofus. That he=20 did not have to repay. If his daddy had not been president, he too would = 
have gone to prison. 


k 

s at 
sunlight is free. Wind energy is free. Steam from the Earth is free.=20 Although the conglomerates will take control and reap outrageous profits = 
from them. That is principally why nothing has been done with solar:=20 they will though, once they figure out a way to profit from it to the=20 detriment of the little people. 

Europe has much better mass transit systems than the US. The governments = 
there spent a lot of money to build them, and they want people to use=20 them. High gas taxes discourage private automobile driving. 

The US has terrible mass transit. It spent enormous amounts of money=20 building a superhighway system. We do not want to discourage people from = 
using the highways, so we keep gas taxes low. 



your ignorance is astounding. Bush and his republiCON pals have=20 eliminated social programs since day one. 


at 
are you so dense???? bush sure has hell tried to privatize it--and John=20 McBush plans to bring it up again if he manages to steal a third term. 




Stalin was no socialist, he was a Communist--and there is a big=20 difference. He was a bloody dictator. Allow me to educate you. 
social democrats today 
In general, contemporary social democrats support: 
* A mixed economy consisting mainly of private enterprise, but with=20 government owned or subsidized programs of education, healthcare, child=20 care and related services for all citizens. * Government bodies that regulate private enterprise in the interests of = 
workers, consumers and fair competition. * Advocacy of fair trade over free trade. * An extensive system of social security (although usually not to the=20 extent advocated by democratic socialists or other socialist groups),=20 with the stated goal of counteracting the effects of poverty and=20 insuring the citizens against loss of income following illness,=20 unemployment or retirement. * Moderate to high levels of taxation (through a value-added and/or=20 progressive taxation system) to fund government expenditure. 

Social democrats also tend to support: 
* Environmental protection laws (although not always to the extent=20 advocated by Greens), such as combating global warming and increasing=20 alternative energy funding. * Support for immigration and multiculturalism. * A secular and progressive social policy, although this varies markedly = 
in degree. Most social democrats support gay marriage and abortion rights= =2E * A foreign policy supporting the promotion of democracy, the protection = 
of human rights and where possible, effective multilateralism. * As well as human rights, social democrats also support social rights,=20 civil rights and civil liberties. 

Social democratic political parties Democracy 

* Anticipatory democracy * Athenian democracy * Christian democracy * Consensus democracy * Deliberative democracy * Demarchy * Direct democracy * Grassroots democracy * Illiberal democracy * Islamic democracy * Liberal democracy * Messianic democracy * Non-partisan democracy * Participatory democracy * Religious democracy * Representative democracy * Republican democracy * Social democracy * Soviet democracy * Totalitarian democracy 


Social democratic political parties are a feature of many democratic=20 countries, and are found in Australia, Brazil, Canada, Europe, New=20 Zealand and elsewhere. Over the course of the twentieth century, parties = 
such as the British Labour Party, the German SPD and the Australian=20 Labor Party have stood in elections on political platforms that included = 
policies such as stronger labor laws, the nationalization of major=20 industries, and a strong welfare state. Most European social democratic=20 parties are members of the Party of European Socialists[11], which is=20 one of the main political parties at the European level[12], and most=20 social democratic parties are members of the Socialist=20 International[13], which is the historical successor to the Second=20 International. The United States and Japan are the only first world=20 nations which do not possess a competitive social democratic or=20 democratic socialist party. 

During the latter part of the twentieth century, most social democratic=20 parties distanced themselves from socialist economic policies (i.e.=20 public ownership and a planned economy) and, in the opinion of many if=20 not most democratic socialists, of socialism in general. Many modern=20 social democrats have broadened their objectives to include aspects of=20 environmentalism, feminism, racial equality and multiculturalism. 

Since the 1980s, a number of social democratic parties have adopted=20 policies which support a relatively lightly regulated economy and=20 emphasize equality of opportunity rather than equality of outcome as the = 
benchmark for social justice. This trend, known as the Third Way, is=20 controversial among some of the left, many of whom argue that Third Way=20 parties (such as New Labour in the United Kingdom[9]) have embraced=20 elements of liberal, neoliberal, and even conservative ideology, and=20 have ceased to be social democratic. 


The prime example of social democracy is *Sweden*, which prospered=20 considerably under the leadership of Olof Palme. Sweden has produced a=20 strong economy from sole proprietorships up through to multinationals=20 (e.g., Saab, Volvo, Ikea, and Ericsson), while maintaining one of the=20 longest life expectancies in the world, low unemployment, inflation,=20 infant mortality, national debt, and cost of living, all while=20 registering sizable economic growth[16]. 

Others also point to *Norway* as an example of a social democratic=20 nation, where the Norwegian Labour Party played a critical role in=20 Norway's recent political history by making social democratic reforms=20 after WWII. In Norway, progressive taxation was introduced and the=20 public sector greatly increased in size. Recently, Norway's economy has=20 experienced an acceleration in economic growth, aided, in part, by the=20 exploitation of oil deposits. 

Another prominent example is the Canadian province of Saskatchewan,=20 which has been politically dominated by the Cooperative Commonwealth=20 Federation and its successor the New Democratic Party since 1944. While=20 in office the CCF and NDP have nationalized major industries, initiated=20 wide ranging public works, and introduced generous social services such=20 as universal health care (later implemented nationally in Canada), as=20 well as the establishment of public automobile insurance. Today,=20 however, while retaining its social democratic philosophy, the=20 Saskatchewan NDP is no longer as far to the left as it once was, in=20 comparison with the federal NDP. 

To a lesser extent, the Canadian Province of Manitoba is viewed as=20 social democratic, with nationalized businesses such as Manitoba Hydro.=20 However the Manitoba NDP is also more moderate in comparison to the=20 Federal NDP. Generally speaking, the provincial wings of the NDP that=20 are major contenders for government (British Columbia, Saskatchewan,=20 Manitoba & Nova Scotia) tend to be more in the modern Third Way mould of = 
social democracy, as opposed to the federal party and smaller provincial = 
wings that still follow the older style of democratic socialism=20 (reminiscent of the Cooperative Commonwealth Federation). 

The standard of living in social democracies is far higher than here in=20 the US. 




