


 <URL>  
New combination vaccines for infants and toddlers approved Thursday by the  Advisory Committee on Immunization Practices will soon appear in doctors'  offices in Chicago and across the country. 
And parents are sure to have a lot of questions. 
Moms and dads will want to know if giving babies a single shot containing  protection against four or five infections could be dangerous. Might it  overwhelm an infant's immune system and are adverse side effects more  likely? 
Don't worry, says Dr. Tina Tan, a pediatric infectious disease specialist at  Children's Memorial Hospital in Chicago. 
The new vaccines are as safe and effective as individual vaccines already on  the market, she adds, and combining existing immunizations into a single  shot doesn't raise the risk of harm. 
"It's not going to overload your infants' immune system," the physician  says. 
"If you think about what you or I or a baby is exposed to every day in the  environment, you are being challenged by more antigens [proteins] than you  get through these vaccines," Tan explains. 
But others are concerned. 
"There are too many unknowns here; I'd like to see more research on the  effects of combining so many vaccines at once," said Anne Dachel, a member  of the board of Advocates for Children's Health Affected by Mercury  Poisoning. 
She and others suspect a link between thimerosal - a mercury-based  preservative once used in vaccines -- and autism. More than 99 percent of  vaccines no longer use the preservative. But medical experts say research doesn't support that suspicion. 
"When you look at the science, there's no evidence that there is any  connection," says Tan, who tells anxious parents that the illnesses they're  guarding babies against are a much more considerable threat. 
The benefit of the new combination vaccines is a reduction in the number of  shots babies get. Instead of four or five shots per visit at the age of 2  months, 4 months, 6 months, and 15 to 18 months, infants will now endure one  or two needle sticks. 
My Tribune colleague and fellow blogger, Julie Deardorff, notes today that  many parents are alarmed by the sheer number of immunizations kids are now  asked to get. 
"In 1982, The Centers for Disease Control recommended 23 doses of 7 vaccines  for children up to age 6," she writes. "Today, the CDC recommends that  children get 48 doses of 12 vaccines by age 6." With flu shots, the total  expands to 69 doses of 16 vaccines by age 18. 
Pentacel, manufactured by Sanofi Pasteur, protects again five infections at  the same time (diopththeria, tetanus, pertussis, polio and Haemophilus  influenza type B) and was tested on more than 5,000 children. 
Kinrix, made by GlaxoSmith Kline, protects against four infections  (diphtheria, tetanus, pertussis, and polio). 
Both vaccines were approved Thursday by the Advisory Committee on  Immunization Practices, which advises the U.S. Centers for Disease Control  and Prevention on vaccine policy. ================== No connection is a blatant and proven lie. 
 <URL>  
 <URL>  
 <URL>  
 <URL>  


