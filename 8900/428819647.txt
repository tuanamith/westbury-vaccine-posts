


Job Title:     BA/BS/MS - Information Technology Intern  (NJ, PA, NC, VA) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-10-19 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   BA/BS/MS - Information Technology Intern (NJ, PA, NC, VA) &#150; INF003847 
Job Description  
Submit 
Merck continues to lead the pharmaceutical industry in the application of computer technologies as a key element of our business and technology strategies. As information is the competitive advantage, Merck&#8217;s Information Services (IS) group provides IT solutions and support to Merck&#8217;s Research, Manufacturing, Sales, Marketing, Finance, Human Resources, and Corporate groups with the following positions: Application Developers &amp; Web Designers; E-commerce &amp; Technology Specialists; Systems Analysts, Architects &amp; Engineers; Network &amp; Server Specialists &amp; Administrators; Computational, Mathematical, &amp; Information Scientists; Technology Support Specialists; Database Architects &amp; Administrators; Process &amp; Control Engineers; Business Process Consultants; and Information Security Analysts. Responsibilities of these positions include providing: analysis, requirements, design, development, testing,  design, development, and maintenance of global networking and server infrastructure; computer simulation, mathematical modeling, bioinformatics, computational biology, and pattern recognition; information management, databases, and office tools such as email &amp; collaborative groupware; IT architecture &amp; tools and customer support; Development and maintenance of applications for manufacturing &amp; process control and laboratory automation. and Business Process Re-engineering, Project Management, IT Integration (people, process, information, technology), and Change Management. 
Qualifications 
Positions require pursuit of an undergraduate, graduate degree in computer science, computer engineering, chemical engineering, engineering management or related information technology disciplines, or pursuit of a degree in engineering, mathematics, business, information/library sciences, or the natural sciences (chemistry, biology, biochemistry, pharmacology) and strong IT skills and experience. Applicants must have demonstrated academic achievement. A successful candidate will also have a demonstrated aptitude for and interest in working with computer technologies that support: drug discovery and development; automation of factory and laboratory processes; novel &amp; creative sales, marketing, and e-commerce initiatives; or building the computing architecture that provides the foundation for innovative development.   Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition INF003847 oropportunities that meet your qualification. If you know the req number, use the first clause, if req unknown use the second clause. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point     US-NJ-Rahway   Job Type  Internship   Employee Status  Regular  
Additional Information   Posting Date  09/22/2008, 04:40 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-422737 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



