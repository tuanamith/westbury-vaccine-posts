



"WindingHighway" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... Trial for Vaccine Against H.I.V. Is Canceled 
By LAWRENCE K. ALTMAN New York Times Published: July 18, 2008 
A number of other H.I.V. vaccines are in various stages of testing around the world. But there had been high hopes for the governments trial because the potential vaccine was among a new class that sought to stimulate the immune system in a different way. 
The government vaccine  known as PAVE, for Partnership for AIDS Vaccine Evaluation  was similar to a much-heralded vaccine that failed last year. That vaccine was developed by Merck, and Dr. Faucis agency helped pay for the Merck trials. 
Show me that the vaccine works by lowering the amount of H.I.V. in the blood, Dr. Fauci said. Then we will move to a larger trial that will document the link with a particular immune response. He added that until then, doing a large trial is not justified. 
Dr. Alan Bernstein, executive director of the Global HIV Vaccine Enterprise, said that his organization supported Dr. Faucis decision and that there was an urgent need for a diversity of new approaches to H.I.V. vaccine design. 
For instance, Dr. Bernstein said, recent laboratory advances, which allow scientists to look at hundreds of genes simultaneously, offer immense promise in helping us understand how to design new H.I.V. vaccine candidates that can achieve long-lasting immune protection. 
The Merck vaccine was the first of a new class of H.I.V. vaccines to get to an advanced stage in human testing. The vaccine was made from a weakened version of a common cold virus, adenovirus type 5, which served as a way to deliver three synthetically produced genes  gag, pol and nef  from the AIDS virus. Three doses of the vaccine were injected over six months. 
The findings were based on a study of 30 individuals newly infected with H.I.V., and the National Institutes of Health paid for the study. --------------------------------------------- 
Oh well is a strange reaction from a public health crusader.  



