


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Associate Clinical Sample Receiving Specialist &#150; SCI003572 
Job Description  
Submit 
Qualifications * Bachelor Degree required. Concentration in Logistics, Business, or Science related field preferred. Minimum 4 years experience in clinical trial related area or relevant experience a plus. * Attention to detail, familiarity with handling inventory of a sensitive nature, understanding of sample shipment criteria and experience in a medical, clinical or scientific environment is helpful. * Able to communicate well and work with others in a team environment and leadership * Safety and Compliance oriented * Technical writing * DOT49CFR, IATA training * Website understanding computer skills (MS Word and Excel)  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #SCI003572. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-Wayne   Employee Status  Regular   Travel  No  



Please refer to Job code merck-408376 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



