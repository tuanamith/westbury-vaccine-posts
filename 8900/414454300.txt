


Coleah wrote: 
Does not count in my book for a long term track record. Track records starts when the product reaches the market, this is when real life testing begins. 
First, there are too few patients in clinical trials compared to mass market, things could be missed and pop up later. 
Second, and more importantly, the trials are under control of the company, and the goal is to bring the product to the market as soon as possible. 
The industry PR line is that doing thorough testing is in the self-interest of the company. The reality is not so idyllic however. Yes, the company will lose money if people get sick or die but so what? It's a business decision, cold risks to benefits analysis: risk of delays in delivering a product to the market (if more testing) vs risk of delivering an unsafe drug (if less testing). If cutting corners pays then the company will cut corners, especially if being first in the market is at stake. Unsafe drugs are just part of business, in the same manner as paying fines for pollution is part of business for polluters. 
The industry has a real long term record in this regards, including willful suppression of evidence. See articles about Vioxx and Dr. Topol, there are many. 
The incentive to cut corners is especially tempting with *vaccines*. While Vioxx victims and their families sue for millions, victims of vaccine injuries have to go to vaccine court where the award for death is $250,000. 
To sum it up: Gardasil does not have a long term track record. 

