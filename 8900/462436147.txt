


NYT February 13, 2009 Editorial 
Vaccines Exonerated on Autism 
A special federal vaccine court issued three devastating verdicts on Thursday that should help demolish lingering fears that childhood vaccines can and have caused autism. The verdicts won=92t satisfied die- hard adherents of the theory that the medical establishment is recklessly harming their children. But the vast majority of parents ought to accept the verdicts as persuasive evidence that no child need forgo vaccinations against dangerous diseases out of fear that the vaccines might cause autism. 
A slew of major health organizations and scientific studies long ago concluded that there was no link between vaccines and autism, a condition whose victims lack social skills, can=92t communicate well and engage in repetitive behaviors. But thousands of parents are seeking federal compensation for alleged vaccine injuries and pinning their hopes on a special court in Washington to find merit in their claims. 
On Thursday, in a ruling on the first three test cases, three different special masters, or judges, demolished a leading speculative theory as to how vaccines might cause autism. Their judgments rendered it unlikely that the other leading theory will survive verdicts still to come. 
These first cases =97 based on the theory that the vaccine for mumps, measles and rubella can work in combination with vaccines containing thimerosal, a mercury-based preservative, to cause autism =97 were not even a close call. As one special master saw it, the evidence was overwhelmingly against the parents. Medical studies around the world have come down strongly against their contentions, and the experts who testified in defense of the vaccines were far better qualified than those speaking for the parents, in his judgment. 
There was no doubt that the child in question had autism and other severe disorders but no evidence that vaccines caused those problems. Another child=92s case was deemed =93speculative and unpersuasive=94 by the special master who heard it. 
Many parents had contended that the measles-mumps-rubella vaccine alone could cause autism, but this round of decisions also demolished that theory as well. The court must still rule on test cases contending that thimerosal-containing vaccines alone could have caused autism, a theory that medical authorities have long dismissed as contrary to the evidence. 
At any rate, thimerosal has been removed from most vaccines for children, allowing fearful parents to dodge it. The verdicts on Thursday suggest that fear of autism was never a valid reason to forego vaccinations that can protect children from illness and even death. 



