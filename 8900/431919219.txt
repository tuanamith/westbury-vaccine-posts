


Scientists create =91good bugs' to fight =91bad bugs' By ROBERT S. BOYD McClatchy Newspapers 

WASHINGTON -- For years, you've been able to walk into a drugstore or health food outlet and buy a host of "probiotics" - natural dietary supplements such as Acidophilus or Lactinex - off the shelf to treat conditions such as children's eczema or traveler's diarrhea. 
Unlike antibiotics, these self-help products don't kill germs, but they supposedly confer health benefits, the way vitamins and certain minerals do. Existing probiotics haven't been approved by the Food and Drug Administration or subjected to rigorous clinical trials. When tested, their effectiveness has been mixed, medical researchers say. 
Now scientists are trying to design "good bugs," novel forms of bacteria created in the laboratory to prevent or cure specific diseases, including HIV and cancer. 
"Perhaps the only hope of winning the war against 'bad bugs' will be achieved by recruiting 'good bugs' as our allies," e-mailed Roy Sleator, a microbiologist at University College in Cork, Ireland. Sleator is the editor of a forthcoming scientific journal called "Bioengineered Bugs." 
He said his laboratory had engineered a new generation of "designer probiotics," which are tailored to target certain disease-causing microbes or toxins. His "good bugs" mimic receptor proteins on the surface of harmful bacteria and block their ability to infect healthy cells. 
"Designer probiotics bind to bacterial toxins in the gut ... thereby preventing disease," Adrienne and James Paton, researchers at the University of Adelaide, Australia, reported in the journal Nature Microbiology. 
In a e-mail, James Paton said his lab had designed a probiotic that works against E. coli O157, a notorious microbe that's caused serious, sometimes fatal, outbreaks of intestinal disease. 
The need for more effective antibiotics is widely recognized because of an alarming increase in the ability of bacteria to resist standard medicines. A special concern is the virulent MRSA - methicillin- resistant staphylococcus aureus - a bacterium that infects and sometimes kills hospital patients. 
"It is becoming increasingly apparent that alternative approaches to conventional antibiotic therapy are required to control infectious diseases in humans and animals in the 21st century," Paton said. 
"Increasing incidence of antibiotic resistance ... has forced clinical research to explore alternative therapeutic and prophylactic avenues," Sleator wrote in a British microbiological journal. "Probiotics are finally beginning to represent a viable alternative to traditional drug-based therapy." 
Sleator said his laboratory had genetically engineered a harmless strain of E. coli to secrete a substance that might be useful against HIV. He's also working on probiotics that may assist in the prevention and decreased recurrence of certain cancers. 
Researchers caution that designer probiotics are still under development, need further testing and government approval, and suffer a number of shortcomings. 
The "good bugs" are fragile and short-lived, Sleator said, and scientists don't understand very well how they work. 
Paton said there was also "substantial public mistrust" of genetically modified organisms, such as good bugs, "which may lead to marketplace resistance even to potentially lifesaving products." 
Nevertheless, scientists have faith that designer probiotics eventually will outperform nature's products. 
"If designed carefully, and with absolute attention for biological safety in its broadest sense, the development of GM (genetically modified) probiotics has the potential to revolutionize medicine," Lothar Steidler and Sabine Neirynck, researchers at University College, Cork, wrote in a 2005 book on probiotics. 
"Scientists and clinicians alike are only now beginning to realize the significant medical applications of probiotic cultures," Sleator said. He said useful applications might include novel vaccines and ways to deliver drugs to where they were most needed. 
He predicted that the work ultimately will lead to the development of "artificial microorganisms ... tailored to fulfill all the requirements of an ideal therapeutic agent." 
 <URL>  

