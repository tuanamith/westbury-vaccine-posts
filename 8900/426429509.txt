


Scientists map genomes of two malaria parasites 
By Will Dunham 39 minutes ago 
 <URL>  
WASHINGTON (Reuters) - Scientists have mapped the genomes of the parasite that causes most cases of malaria outside Africa and a monkey parasite that is emerging as an important cause of malaria in people in Southeast Asia. 
This information should help guide efforts to develop new drugs and vaccines to fight the mosquito-borne disease, two teams of researchers wrote in the journal Nature on Wednesday. 
"It's going to be a very powerful tool," Jane Carlton of New York University Langone Medical Center said. 
A team led by Carlton worked out the complete genetic sequence of the parasite Plasmodium vivax, which causes malaria in Latin America and Asian countries including India, Thailand, Vietnam, Indonesia, Melanesia and the Korean peninsula. 
It accounts for up to 40 percent of malaria globally, with an estimated 2.6 billion people threatened by the parasite. 
Although the malaria it causes is only occasionally fatal, it triggers severe symptoms such as repeated episodes of high fever followed by headache, chills and profuse sweating, vomiting, diarrhea and enlargement of the spleen. 
The vivax parasite can remain dormant in the liver only to re-emerge and cause relapses months or years after the initial illness. The researchers found genes that may be responsible for this dormancy, perhaps paving the way for scientists to find ways to disrupt it. 
The researchers identified genes in the parasite that seem to help it invade a person's red blood cells and evade the immune system. The parasite is becoming resistant to some antimalarial drugs. 
GROWING THREAT 
A team led by Arnab Pain of the Wellcome Trust Sanger Institute in Britain deciphered the full genetic sequence of the monkey parasite Plasmodium knowlesi. 
This parasite is rapidly establishing itself as the fifth human-infecting malaria parasite and has emerged as a considerable health problem in Southeast Asia, Pain said. 
The researchers also found a trick used by the knowlesi parasite to avoid detection by the immune system. Some of its genes closely resemble a human gene involved in regulation of the immune system. 
The World Health Organization said malaria killed 881,000 people and infected 247 million people worldwide in 2006, the latest year for which figures were available. Some malaria experts say those numbers underestimate the problem. 
Most deaths occur in Africa and are caused by the Plasmodium falciparum parasite, whose genome was mapped in 2002. 
The researchers found the vivax genome was similar in many ways to the falciparum parasite, meaning that certain vaccine approaches being tried against the African parasite may be worth trying against this one. 
"During the course of evolution, malaria parasites have devised different tricks to avoid being detected and dampen the host immune responses," Pain said by e-mail. 
"Thus, it has been rather difficult to find a single parasite protein that could be used as an effective vaccine candidate which would provide effective and long-term protection against all parasite strains circulating within a given population at a given time," he said. 
(Editing by Maggie Fox and Xavier Briand) 
--  Bob. 


