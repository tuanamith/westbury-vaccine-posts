


Job Title:     Director, Scientific Liaison Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-11-11 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Director, Scientific Liaison &#150; RES000673 
Job Description  
Submit 
Qualifications 
Required - PhD degree in biological/medical with 3-5 yrs Post-Doctoral research experience plus &amp;gt;12-17 years additional experience in the Pharma industry. Thorough understanding of Mercks scientific/medical needs with knowledge in a broad range of scientific disciplines and knowledge of drug development processes and timelines. Broad understanding of global pharmaceutical and biotech industries. Ability to communicate and understand key methods of action of all relevant compounds. Strong leadership skills and proven ability to influence others. Must possess the executive presence to facilitate the decision making process among Merck Senior Executives.  Demonstrated success in identifying significant partnerships and alliances resulting in substantial impact to Merck. Impeccable organizational skills and ability to memorialize scientific meetings with potential partners.  Desired - MD/PhD or DVM/PhD (dual degree) with 3-5 yrs Post-Doctoral research experience plus &amp;gt;9-15 yrs additional experience in the Pharma industry. Degree concentrations and/or post-doctoral research that overlaps with MRLs strategic areas of interest (e.g., Neuroscience, Oncology, Cardiovascular, Metabolic disease, Immunology, Infectious disease, Vaccines, Genetics/RNAi, Biologics/mAb, or Research technologies).  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careers  to create a profile and submit your resume for requisition # RES000673. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway     US-PA-West Point   Employee Status  Regular   Travel  Yes, 25 % of the Time  
Additional Information   Posting Date  11/06/2008, 04:18 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/06/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-413436 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



