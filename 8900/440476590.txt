


On Dec 6, 2:25=A0pm, Ilena Rose < <EMAILADDRESS> > wrote: 
Interesting... 
Cervical cancer vaccine safe, Australian study indicates 380,000 shots caused just 35 allergic reactions By MARY ENGEL Los Angeles Times Dec. 6, 2008, 5:29PM 
A large study of girls and young women who received the HPV vaccine Gardasil, designed to guard against cervical cancer, found three confirmed cases of severe allergic reaction out of 380,000 shots. 
The study, published last week in the British Medical Journal, was based on vaccination data from Australia, which has had a nationwide program to vaccinate females ages 12 to 26 in schools since April 2007. 
Its findings bolstered the argument that the vaccine is relatively safe, despite lingering concerns from some doctors and parents. 
The study, led by researchers at the Royal Children's Hospital in the Australian state of Victoria, was prompted by media and medical reports of girls suffering allergic reactions after receiving the shot. Some patients reported dizzy spells, fainting and even temporary paralysis. 
Similar concerns over Gardasil's safety have been raised in the United States since the Food and Drug Administration approved the vaccine in June 2006 to protect against four strains of human papillomavirus that cause most cervical cancers and genital warts. 
Out of the 380,000 doses of Gardasil studied in Australia, the researchers found 35 reports of allergic reaction, including hives, rash and, in two cases, anaphylactic shock =97 a severe reaction that can cause airways to tighten, among other symptoms. 
Of the 35 patients, 25 agreed to skin-prick and injection testing to confirm their reactions. Just three were found to have a "probable hypersensitivity" to the vaccine, including the two with anaphylaxis. 
------------------- 
Safer than cancer, that is for sure. And, not funded by anyone in the USA. 
BOOM! 
(Sound of the conspiracy exploding) 

