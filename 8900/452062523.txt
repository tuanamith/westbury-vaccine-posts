


On Sun, 18 Jan 2009 17:38:28 -0800 (PST), RedDog < <EMAILADDRESS> > wrote: 

I refuse to get involved in the"the whole world and it's dog is B&C" nonsense..but on points of information.. 





Incorrect.. 
 <URL>  
Course Requirements  Programs of study are planned in relation to specified foci of nursing scholarship in concert with program design, supervisor expertise, and anticipated contributions to knowledge. 

All doctoral students must write and publicly defend a research proposal and have it approved by their supervisory committee before continuing the research process. All doctoral students are required to prepare a dissertation upon which a public examination and defense is conducted. The dissertation must qualify as a significant and original contribution to disciplinary knowledge. 


incorrect.. 


Doctor Of Medicine Prizes 

 <URL>  
Four doctors have received prizes from the Universitys School of Clinical Medicine in recognition of outstanding work on their MD dissertations. 
An MD, the Doctor of Medicine degree, is the highest medical degree awarded by the University, given to medically qualified graduates who submit a dissertation on an approved new area of research. 
All the dissertations for the MD degree in each academical year are considered by the MD Committee for the Raymond Horton-Smith Prize and those in appropriate fields for the Sir Lionel Whitby Medal, the Ralph Noble Prizes and the Sir Walter Langdon-Brown Prize. 
The Raymond Horton Prize is for the best MD dissertation of each academic year. It went to Andrew Sutton of Selwyn College for his dissertation 'Diagnosis and management of failed thrombolysis for acute myocardial infarction'. 
Dr Sutton is currently working as a consultant cardiologist at the James Cook University Hospital, Middlesborough. His work was conducted over seven years at the South Cleveland Hospitalk and has been widely praised in cardiological circles. 
Dr Stephen Barclay, Macmillan Cancer Relief Clinical Fellow, GP and Honorary Consultant in Palliative Medicine, was awarded the Ralph Noble Prize for his MD thesis "General Practitioner provision of palliative care in the UK". 
His thesis specifically addresses research into GP's training in palliative care, their knowledge of controlling pain and other symptoms in advanced cancer, the accuracy of GPs' estimates of prognosis, and their assessment of the severity of patients' symptoms in clinical practice. He carried out the work for his MD in the General Practice & Primary Care Research Unit at Cambridge. 
The Sir Lionel Whitby Medal was awarded to Dr Mark Roberts of St John's College for his work 'Characterisation of the immune response to vaccine antigens that exacerbate disease in a murine model of cutaneous leishmaniasis'. 
His work on the development of a vaccine for the life-threatening disease Leishmania was carried out at the Cambridge Institute for Medical Research. 
Dr Mark Rochester of Trinity College received the Sir Walter Langdon-Brown Prize for the dissertation "The type 1 insulin-like growth factor receptor in prostate and bladder cancer". 
Work for the dissertation was carried out at the Weatherall Institute of Molecular Medicine, John Radcliffe Hospital, Oxford. Dr Rochester had mastered a great deal of molecular biology and laboratory techniques within a short space of time. He is working at Ipswich at present as an Specialist Registrar in urological surgery 
The presentation was led by the Chairman of the MD Committee, Professor E S Paykel. 


