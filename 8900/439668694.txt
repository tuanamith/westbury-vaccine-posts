


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   MS/PhD Summer 2009 Internship - Safety Assessment (System Toxicology - Xu) &#150; BIO001969 
Job Description  
Submit 
As part of the Merck Research Laboratories, Safety Assessment (SA) is a global organization with facilities in West Point (USA), Mirabel (France), and Tsukuba (Japan). The goal of SA is to help advance high quality drug candidates into development by defining the non&#8212;clinical safety and selectivity of lead compounds. SA employees evaluate the toxicity of drug development candidates and assess implications for human safety. Safety Assessment also addresses regulatory requirements to support drug registration. Major departments within SA include Toxicology, Pathology, Laboratory Technical Operations, Laboratory Sciences &amp; Investigative Toxicology and Compound Management Coordination We have an intern position with the System Toxicology group focus on development of new statistical and visualization tools to support genomics, metabolomics and porteomics data analysis. The intern will also provide assistance in development of additional tools as required. This is a summer internship; applicants must be available for 10-12 week duration during the months of May to August. Applicant must be currently enrolled in a degree program at the time of the application. 
Qualifications * Candidates pursuing degrees in: biological sciences, biochemistry, analytical chemistry, computer science biostatistics or related majors * Basic knowledge in analytical chemistry preferred. * Candidate must demonstrated proficient programming skills in R, and working knowledge in multivariate statistics * Strong organizational and project management skills. * Detail oriented and able to work independently  If you are the kind of individual who thrives on challenge and possess the technical, leadership and business skills that are of value to our business, we invite you to apply. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers/university/to create a profile and submit your resume for requisition #BIO001969. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point   Job Type  Internship   Employee Status  Regular  
Additional Information   Posting Date  11/03/2008, 01:29 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/15/2009, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-425317 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



