


A vaccine given to babies could increase the risk of childhood asthma By Beezy Marsh Last updated at 12:12 AM on 21st October 2008 
A vaccination given to babies has been linked to asthma. 
Experts believe the diphtheria, tetanus and whooping cough jabs might  provoke an immune system response which predisposes the body to the lung  condition. 
But delaying the vaccines by two months from the recommended age  dramatically reduces the risk, doctors found. 

They set out to test a theory that the timing of the triple jab affects the  development of childhood asthma. 
Vaccine: Is it too early? 
For the study, 11,531 children received four doses of the combined DTP jab.  Babies are supposed to have their first dose by the age of two months. 
It was found the likelihood of developing asthma by the age of seven was  halved if this initial dose was delayed by two months. 
Of nearly 5,000 babies studied who had the jab at the scheduled age, 13.8  per cent developed asthma. 
This compared with a rate of 5.9 per cent in babies who were four months or  older at first DTP immunisation. 
The second, third and fourth doses of DTP were to be given at four months,  six months and 18 months. 
Researchers at Manitoba Institute for Child Health and the University of  Manitoba, in Canada, also found a decreased likelihood of asthma if the  other doses were delayed, but the strongest evidence was seen in relation to  the delay of the first dose. 
Only five per cent of children who had delays in all of their DTP jabs went  on to develop asthma. 
This figure jumped to 12 per cent among children who followed the  immunisation schedule, it was reported in the Journal of Allergy and  Clinical Immunology. The UK has the highest prevalence of asthma in children  aged 13 and 14 worldwide. One in 11 children is affected. 
Vaccination could increase the risk of asthma 
The Canadian children mainly received a type of DTP jab given to millions of  UK children until 2004. 
However, some also received the newer form of the jab, containing a  different type of whooping cough vaccine, called acellular pertussis. 
This DTaP vaccine was phased into the UK during 2004. It is given at two  months in combination with vaccines against polio and Hib disease  -  a type  of influenza affecting babies. 
Babies also receive vaccination against pneumococcal disease at the same  time. Some believe the volume of jabs overloads their immune systems. 
This theory, however, is discounted by the Department of Health and all the  Royal Colleges. 
Dr Richard Halvorsen, author of The Truth About Vaccines, who advocates  delays between immunisations, said: 'This is a very interesting study which  the Government should look at. 
'This study doesn't prove the immunisation schedule we use causes a problem  but it is stupid not to consider it.' 
A Department of Health spokesman said 'Several large studies have looked at  whether childhood vaccines can cause asthma or allergies  -  they have found  no evidence for this.' 
 <URL> # 



