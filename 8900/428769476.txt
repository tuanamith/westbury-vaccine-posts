


Job Title:     Quality Auditor/Associate -GMP Quality Commercialization... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-10-16 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Quality Auditor/Associate -GMP Quality Commercialization and Early Development &#150; QUA001725 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The successful candidate will support GMP Compliance activities for the packaging, labeling and distribution of clinical trial materials. This includes audits of In this role, you would also be responsible for release of clinical supplies, outsourcing support, and complaint processing. 
Qualifications A B.S. in an appropriate science (such as biochemistry) or related field is required. At the Auditor level, a minimum of one year of experience is required. At the Associate level, required experience is BS plus three three years, or MS plus one year. Experience should be in one or more of the following areas in the pharmaceutical/chemical industry: * product development * technical service * manufacturing or packaging operations * an analytical function * a quality function. Working knowledge of cGMP regulations and demonstrated analytical, problem-solving, and effective communication (oral/written/interpersonal) skills are necessary, as are srong organizational skills and computer skills.  Working knowledge of GMPs as applicable to packaging/labeling operations in clinical supply area, along with prior auditing experience desired. Experience in supporting quality functions with outsourced packaging/labeling vendors is preferred.  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001725. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 


Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  Yes, 10 % of the Time  
Additional Information   Posting Date  10/14/2008, 11:47 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-419097 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



