


Job Title:     Sr. Research Biologist / Research Fellow-Cardiovascular... Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-04-25 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Sr. Research Biologist / Research Fellow-Cardiovascular Disease Molecular Profiling Scientist &#150; SCI003537 
Job Description  
Submit 
Description 
We currently have an opening in the Molecular Profiling Department for aSenior Research Biologist / Research Fellowthat will focus on leveraging molecular profiling technologies for biomarker and novel target discovery for Cardiovascular Disease. This position, which will be based at Mercks research facility in Rahway, NJ, will have the primary focus of working closely with Merck basic research and clinical scientists as well as Molecular Profiling scientists to discover pharmacodynamic, efficacy, and responder/non-responder biomarkers to help drive decision making in pre-clinical and clinical drug development. Specifically, the successful candidate will have the primary responsibilities of: 1) defining pre-clinical and clinical biomarker strategies for Cardiovascular Disease drug programs. 2) designing and analyzing large scale Molecular Profiling experiments, including mRNA and miRNA profiling, genotyping, deep sequencing, proteomics and metabolomics. 3) applying the findings to the development of robust and cost effective pre-clinical or clinical assays.  In addition, this position will work with licensing professionals to review external opportunities, and will be expected to publish patents and peer-reviewed scientific papers. 
 . 
Qualifications 
Ph.D. and/or M.D. plus at least two years post-doctoral experience required. Experience should include extensive research in the application of Molecular Profiling techniques to human disease, and preferably to cardiovascular disease research, and direct experience with the application of biomarkers in clinical development is preferred. Preference will be given to candidates with training in the interpretation of large gene expression profiling data sets and strong statistical skills, and excellent communication and organizational skills are essential. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world.We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # SCI003537. Merck is an equal opportunity employer, M/F/D/V. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  04/23/2008, 11:28 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-395136 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



