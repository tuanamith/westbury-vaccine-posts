


Job Title:     Systems and Administration Specialist Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-07-19 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Systems and Administration Specialist &#150; ADM003676 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. This position will support groups within the Analytical area of Regulatory and Analytical Sciences (RAS). In collaboration with and under limited supervision of the Sr. Director, support the leadership team in the administration of financial, performance management process (PMP) and organizational management systems in accordance with established standard operating procedures for the Company, Division and department. Also as a member of an administrative staff team, performs general duties including: travel arrangements, Company-related expenses, office supply and maintenance coordination, maintaining confidentiality, answering the phones, distributing mail, maintaining calendars, scheduling meetings, making copies, and maintaining filing systems, and This position requires the coordination and performance of multiple tasks with specific focus on the Merck financial (expense reporting, monthly estimated actuals and profit planning), PMP and organizational management. These activities require confidentiality, time management, stewardship, flexibility and good judgment. The candidate must be effective in interactions and coordination of workflow with other departments, groups and individuals.  
Qualifications The candidate must demonstrate leadership in flexibility, collaboration, willingness to learn and be stretched out of comfort zone. The candidate must be detail-oriented with the ability to handle multiple priorities, requiring flexibility and judgment in prioritizing assignments with little or no direct supervision. Strong written and oral communication skills, problem solving and organizational skills are essential. Must be able to work in a fast-paced changing environment. A working knowledge of Company policies and procedures and the ability to provide assistance to staff when questions arise is important. High school diploma and a minimum of 3-5 years administrative experience is required. Proficiency in Microsoft Office suite of products required: Word, Excel, PowerPoint, and Outlook. Prior experience with other Merck applications, including PeopleInfo, CareerLink, eZExpense, eTrip, ezBuy, E-Rooms Unanet, other Merck applications and the intranet is required. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # ADM003676. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  07/17/2008, 01:50 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  07/31/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-411996 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



