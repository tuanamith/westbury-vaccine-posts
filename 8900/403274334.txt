



Hi, An interesting article......hope things keep improving and quick non- agony-creating permanent cures are found ASAP! 
Ciao, Sam 
CHICAGO: A personalized vaccine made using tobacco plants =97 normally associated with causing cancer rather than helping cure it =97 could aid people with lymphoma in fighting the disease, US researchers said. 
The treatment, which would vaccinate cancer patients against their own tumour cells, is made using a new approach that turns genetically engineered tobacco plants into personalized vaccine factories. 
"This is the first time a plant has been used for making a protein to inject into a person," said Ron Levy of Stanford University School of Medicine in California, whose research appears in the journal Proceedings of the National Academy of Sciences. 
"This would be a way to treat cancer without side effects," Levy said in a statement on Monday. "The idea is to marshal the body's own immune system to fight cancer." 
Levy was working with a team of scientists from the now defunct Large Scale Biology Corp, which helped fund the study, as well as Bayer AG's Bayer HealthCare, CBR International Corp, Integrated Biomolecule Corp, The Biologics Consulting Group Inc and Holtz Biopharma Consulting. 
They were working on a type of cancer known as follicular B-cell lymphoma, a kind of non-Hodgkin's lymphoma that attacks the immune system. The cancer makes a specific antibody that is not found in healthy cells. 
The technology exploits the tobacco plant's vulnerability a virus that only attacks tobacco plants, which most people associate with causing cancer, and not curing it. 
The researchers altered the virus, adding the specific antibody gene from a patient's cancer cells. Then, they infected the tobacco plants with the gene-carrying virus. 
"You scratch it on the leaves and it turns the plants into a protein- producing factory for the protein of interest," Levy said. 
Other approaches that use animals to make the vaccines can take months, but the plant-based approach is very fast. 
"A week later, you extract the protein. It's that fast." 
In a test of 16 patients with follicular B-cell lymphoma, 70% of people injected with a made-to-order vaccine developed an immune response, and none had any side effects. 
Levy said the study suggests personalized cancer vaccines could be produced efficiently and cheaply using plants. The early-stage study only focused on the safety and immune-stimulating ability of the plant- produced vaccines. 
Future studies will be needed to show how effective they are as a treatment. End..... 

