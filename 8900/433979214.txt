


Note from Health Lover, Ilena Rosenthal:  <URL>  
For years, people harmed by GWS, breast implants, and other chemical timebombs have been further battered by the chemical industry's vast propaganda team ... much of it led by Stephen Barrett. He sues and smears fine scientists working to find the causes and cures ... while being paid to write the chemical industry POV. He has caused millions of people more suffering and harm as the chemical & vaccination industry harm denier. 
His nonsense: 

"Multiple chemical sensitivity, sick building syndrome, food-related hyperactivity, mercury amalgam toxicity, candidiasis hyperactivity, Gulf War syndrome-these are all costly misbeliefs and fad diagnoses, says Barrett. "Many Americans believe that exposure to common foods and chemicals makes them ill," he says. "This book is about people who hold such beliefs but are wrong." Stephen Barrett 
www.BreastImplantAwareness.org/QuackWatchWatch.htm 

Toxic Chemicals Blamed for Gulf War Illness  <URL>  
By Steven Reinberg HealthDay Reporter Monday, November 17, 2008; 12:00 AM 
MONDAY, Nov. 17 (HealthDay News) -- Gulf War illness, dismissed by some as a psychosomatic disorder, is a very real illness that affects at least 25 percent of the 700,000 U.S. veterans who took part in the 1991 Gulf War. 
Its likely cause was exposure to toxic chemicals that included pesticides that were often overused during the war, as well as a drug given to U.S. troops to protect them from nerve gas, a frequent weapon of choice of former Iraqi leader Saddam Hussein. 
And no effective treatments have been devised for the disorder. 
Those are three key conclusions of a Congressionally mandated landmark report released Monday by a federal panel of scientific experts and veterans. 
"It is very clear that Gulf War illness is a real condition that was not caused by combat stress or other psychological factors," said Lea Steele, scientific director of the Research Advisory Committee on Gulf War Veterans' Illnesses, which issued the report, and an associate professor at Kansas State University. 
"This is something we need to take seriously," Steele said. "These folks were injured in wartime service, much as people who were shot with bullets or hit with bombs." 
The committee presented the 450-page report to Secretary of Veterans Affairs James Peake. 
Gulf War illness is frequently described as a collection of symptoms that includes memory and concentration problems, chronic headaches, fatigue and widespread pain. Other symptoms can include persistent digestive problems, respiratory symptoms and skin rashes. 
The panel also said Gulf War veterans have much higher rates of amyotrophic lateral sclerosis (ALS, or Lou Gehrig's Disease) than other veterans, and soldiers who were downwind from large-scale munitions demolitions in 1991 have died from brain cancer at twice the rate of other Gulf War veterans. 
In reaching its conclusions, the panel reviewed evidence about a wide range of possible environmental exposures that could cause Gulf War illness. That review included hundreds of studies of Gulf War veterans, research in other groups of populations, animal studies of toxic exposures, and government investigations about events and exposures during the Gulf War, which began after Hussein invaded Kuwait. 
Speculation about the causes of Gulf War illness has included exposure to depleted uranium munitions, vaccines, nerve agents and oil well fires. 
The new report says the illness was caused by soldiers' exposure to certain chemicals, Steele said. 
"When you put all the evidence together there are two chemicals that jump out as the main causes," she said. One is a drug called pyridostigmine bromide, which is a cholinesterase inhibitor that was given to the troops to protect them against nerve gas. 
"It turns out that people who took those pills have a higher rate of Gulf War illness," Steele said. "And people who took more pills have even higher rates of Gulf War illness." 
In addition, soldiers were exposed to pesticides that were also cholinesterase inhibitors, Steele said. "The strongest evidence points to pyridostigmine bromide and pesticides as causal factors," she said. "This type of illness has not been seen after other wars." 
While pyridostigmine bromide is still in use, its use is more limited than it was in the first Gulf War. It's currently being used against one type of nerve agent, but is not being given out on a widespread basis, Steele said. 
"The Gulf War was the only time a lot of people used this drug," she said. 
Steele added that the U.S. military has also cut back on its use of pesticides since the 1991 war. 
There are other factors that, while not likely causes of Gulf War illness, can't be ruled out, Steele said. These include exposure to nerve agents, exposure to smoke from oil well fires, and vaccines given to the troops. The panel ruled out depleted uranium and anthrax vaccine as causes. 
The panel also found government research and funding into Gulf War illness wanting. "There has not been sufficient attention given to Gulf War illness. It's a real problem," Steele said. 
"In recent years, both the Department of Defense and the Department of Veterans Affairs have reported a lot of studies that weren't Gulf War illness as Gulf War research," Steele added. "Some of the money was misused." 
The panel noted that overall federal funding for Gulf War research has declined substantially in recent years; the group urged lawmakers to devote $60 million annually to such programs. 
When veterans with Gulf War illness go to Veterans Administration hospitals for treatment, their problems often aren't taken seriously, Steele said. "VA docs often know nothing about it and aren't able to help them. Sometimes they treat them as if they are head cases or malingering," she said. 
James Binns is chairman of the U.S. Department of Veterans Affairs' Research Advisory Committee on Gulf War Veterans' Illnesses. 
"We have no treatments that work," said Binns, a Vietnam veteran and former Pentagon official. "I would like to see the new administration take this more seriously. When you look at all the studies, it's as clear as the nose on your face that this [Gulf War illness] is real." 
It took 20 years to admit that Agent Orange, a defoliant used in the Vietnam war, caused illness, Binns said. "It's now coming up to 17 years on Gulf War illness," he said. "Troop exposures [to these chemicals] were a serious but honest mistake. Covering it up rather than trying to help them has been unconscionable." 
More information 
Learn more about Gulf War illness from the University of Chicago Medical Center. 
SOURCES: Lea Steele, Ph.D., associate professor, Kansas State University, Manhattan, and scientific director, Research Advisory Committee on Gulf War Veterans' Illnesses; James Binns, chairman, U.S. Department of Veterans Affairs' Research Advisory Committee on Gulf War Veterans' Illnesses 

