


The Christian Science Monitor   Aug 2, 8:08 PM EDT Army declines comment on anthrax researcher  PAULINE JELINEK Associated Press Writer 
WASHINGTON (AP) -- The Army refused Saturday to say whether it  had been reviewing the security clearance of the chief suspect  in the anthrax attacks who had mental problems and killed  himself as federal prosecutors were planning to indict him. 
Bruce E. Ivins was removed from his lab in Maryland by police  on July 10 and temporarily hospitalized, according to court  records, because it was feared that he was a danger to himself  and others. But it was unclear whether he was still employed  by the lab at the time of his death Tuesday. 
That raises the question of whether Ivins still had his  security clearance and, if so, how he kept it, given that his  social worker said Ivins had been viewed as homicidal and  sociopathic by his psychiatrist. 
Army spokesman Paul Boyce declined to comment on Ivins' case. 
"The U.S. government never discusses the specific security  clearance of an individual employee or military member," he  said. 
He noted only that there are "time-honored procedures to  examine security clearances on a regular basis, to verify  information provided by the security-clearance holder, and  traditional steps to ensure that only the appropriate level of  security access is granted, largely based on the nature of the  person's government job." 
Boyce didn't respond to a question on what type of clearance  microbiologists at the lab would have to hold. 
Army regulations require that security clearances be reviewed  every five years, but special investigations can be done if  there are allegations against an employee or there is some  other cause for concern. 
In 2006, the last year for which data is available, less than  0.05 percent of some 800,000 people investigated for  clearances to work in the military and other federal jobs were  rejected on the sole issue of their mental health profile,  officials have said. 
That's because the clearance process is done on the whole- person concept - that is, it weighs a number of factors about  the person's past and present, favorable and unfavorable.  People can be prevented from getting a clearance if they have  been convicted and imprisoned, are addicted to any controlled  substance, have been discharged dishonorably from the service  or are currently mentally incompetent. 
David Danley, who used to work with Ivins other scientists at  the lab at Fort Detrick, told National Public Radio Saturday  that the facility had little security more than 30 years ago  when Ivins was hired to study anthrax vaccines. 
It wasn't until 1997 that the Centers for Disease Control and  Prevention established what it called a select-agent program  to regulate toxins that can pose a severe threat to health and  public safety. Danley said things changed at the lab only  after five people died from anthrax-laced letters in 2001, the  case in which Ivins had become the main suspect. Among changes  was that everyone needed security clearances. 
It's unclear how background checks apparently failed to catch  what his therapist has called "a history dating to his  graduate days of homicidal threats, actions, and plans." 
Applicants for security clearances are required to fill out a  form that asks about previous mental health problems. 
"I don't believe that reviewing a person's medical record is  part of a typical security investigation unless there is an  indication that there's a concern there," Michael Woods, who  used to be chief of national security law at the FBI, told  NPR. 
Woods said investigators only find what they look for. And  what they look for depends on what the applicant reports and  what other people say about that person. 
"Any system will miss someone somewhere," he told NPR. "And  the alternative here is to have a much higher level of  scrutiny for everybody in the system. I don't particularly  want investigators going through my medical records if there's  not a reason, a security-related reason to do that." 
For years, the applications for clearance to work in sensitive  jobs included a question on whether the person had consulted a  mental health professional in the last seven years. If so,  they were asked to list the names, addresses and dates they  saw the doctor or therapist. 
Troops often said they were afraid to get psychiatric care  because getting it - and acknowledging it on their forms -  might mean denial of the clearance or hurt their careers in  other ways. Seeking to encourage troops to get help for post- traumatic stress and other war-time mental problems, the  Pentagon in May changed its application form, making it  unnecessary to report therapy related to help they needed  adjusting to combat duty or marriage or grief counseling not  related to any violence behavior. 
--- 
Associated Press writer David Dishneau contributed to this  report from Frederick, Md. 
 <URL>  ECURITY_CLEARANCE? SITE=MABOC&SECTION=HOME&TEMPLATE=DEFAULT&CTIME=2008-08-02-20- 08-34 

