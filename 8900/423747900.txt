


Job Title:     Electrical Engineer (High Voltage Power) Job Location:  NJ: Lebanon Pay Rate:      Open Job Length:    full time Start Date:    2008-10-05 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Electrical Engineer (High Voltage Power) &#150; ENG001727 
Job Description  
Submit 
Qualifications To qualify you must have at least a BS degree in Electrical Engineering, preferably licensed, with a minimum of at least 8 years experience in the engineering and design of electrical utility systems for bulk chemical, pharmaceutical, and biotech manufacturing facilities and laboratories. Knowledgeable with electrical codes and industrial standards and a working knowledge of electrical equipment. Excellent verbal, written communications and leadership skills are required. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #ENG001727. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Lebanon   Employee Status  Regular   Travel  Yes, 25 % of the Time  
Additional Information   Posting Date  10/02/2008, 03:14 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  10/16/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-422876 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



