


 <URL>  
GlaxoSmithKline trimming US sales force by 1,000 
TRENTON, N.J. (AP) - British drugmaker GlaxoSmithKline PLC is restructuring its U.S. operations, starting with reducing its U.S. sales force by 1,000, following many of its top competitors in eliminating sales jobs. 
The world's No. 2 drugmaker by revenue also will switch from having dual U.S. headquarters, in Philadelphia and in Research Triangle Park, N.C., to operating just the North Carolina headquarters. 
U.S.-traded shares of GlaxoSmithKline fell $3.23, or 8 percent, to close at $36.96 Wednesday. 
Spokeswoman Mary Anne Rhyne said Wednesday that the headquarters change is meant to eliminate confusion and is largely symbolic. There are currently no plans to cut jobs or close offices in Philadelphia related to the headquarters move, she said. 
"The goal is to build a business that's more streamlined, that's more customer focused, that meets the challenge of the current marketplace and is better prepared to manage the future portfolio of medicines," Rhyne said. 
GlaxoSmithKline, the maker of antidepressant Wellbutrin and Requip for Parkinson's disease and restless legs syndrome, currently has about 8,500 U.S. sales representatives, out of a total of 22,500 employees in the U.S. and 100,000 worldwide. It plans to cut about 1,800 sales rep positions, some currently vacant, but at the same time beef up its vaccine sales force. The company expects to end up with about 7,500 U.S. sales representatives, for a net loss of 1,000 jobs, according to Rhyne. 
There also will be an unspecified number of jobs cut among sales support staff, which includes sales management and workers who track sales representatives' activities and provide them with supplies such as free samples and product brochures. 
"If you reduce your field staff, you can reduce the number of staff required to serve them," Rhyne said. 
Workers losing jobs are being told this week and will be out of work by the end of the year. 
David Heupel, pharmaceuticals portfolio manager at Thrivent Large Cap Growth Fund, said GSK's move continues an industry trend to reduce expenses in areas from sales and marketing to supply purchases, manufacturing and even research and development. He said this right-sizing "is a healthy thing for the industry." 
"You're going to see plenty more of this as these businesses mature over the next several years," Heupel said, referring to patent expirations, pricing pressures from payers and other factors weighing on the revenue. 
Edward Jones analyst Linda Bannister said besides those pressures and the lack of blockbusters coming to market anytime soon, drug companies also are concerned the new "Democratic administration could push for lower prices." 
This year, companies from Bristol-Myers Squibb Co. to Wyeth and even No. 1 drugmaker Pfizer Inc. have announced reductions in their U.S. sales forces. Just in the last six weeks, Merck & Co. and partner Schering-Plough Corp. have both done so. 
"This is the new reality for the pharmaceutical industry," said Bannister. 
She said Glaxo might have made its cuts sooner, but new Chief Executive Andrew Witty just took over in May - and the company likely was hoping it could reignite growth of its diabetes drug Avandia and so would need more sales reps. But last month, the American Diabetes Association and its European counterpart removed Avandia from their recommended treatments, and last week the consumer group Public Citizen urged U.S. regulators to ban Avandia because of its link to several life-threatening risks, including heart and liver damage. 
Rhyne said that many of the functions in Philadelphia and Research Triangle Park are similar, covering fields such as accounting, communications, finance, human resources and information technology. 
There are about 5,000 employees in Research Triangle Park, spread over 35 buildings owned by GlaxoSmithKline there. The area in central North Carolina, bordered by the cities of Raleigh, Durham and Chapel Hill, is a major hub for pharmaceutical and biotech research. 
GSK leases its offices in downtown Philadelphia, where it has about 1,500 employees. 
The company also has about 3,000 workers in research and development operations in Philadelphia's western suburbs, on property straddling Upper Merion and Upper Providence townships. No cuts have been announced there. 


