


Job Title:     Co-Operative Assignment 2008 - Molecular Endocrinology... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-05-09 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-Operative Assignment 2008 - Molecular Endocrinology (Sept - March) &#150; BIO001776 
Job Description  
Submit 
Description 
The candidate will work closely in a team environment with biologists and biochemists in the Dept. of Molecular Endocrinology, to carry out biochemical and cell-based biological assays that support the development of novel therapeutics for inflammation. The responsibilities of this position include evaluating potential drug candidates in novel assays. Part of the duties may include conducting HTS, Western blotting, PCR analysis, and RNA extractions. Basic lab skills required. This is a paid 6 month co-operative assignment targeted to start in September 2008 &#8212; March 2009, whereby a weekly stipend will be provided. Housing subsidy is not available as part of this program and if required by the student must be funded 100% by the student. 
Qualifications 
- Pursuing Bachelors degree in biology, biochemistry, or a related field. - Grade Point Average (GPA) of at least 3.0 or higher preferred. - Basic lab skills including HTS, Western blotting, PCR analysis and RNA extractions required. - Applicants must be available for full-time employment for 6 months with a targeted start date in September 2008, and - Applicants must be currently enrolled in an academic program and will returning to school following this assignment. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition #BIO001776.  Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular  



Please refer to Job code merckcollege-397776 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



