


Job Title:     Senior Tax/Finance Advisor, HR Operations Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-04-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Tax/Finance Advisor, HR Operations &#150; HUM001239 
Job Description  
Submit 
Description 
Position supports the overall workflow, control, analysis and reporting related to the end-to-end processing of the payroll operations tax area with an emphasis on International tax related functions. Responsible for performing and ensuring that all responsibilities related to these functions are completed in a timely and accurate manner, in compliance with Company policies and procedures. Participates in the identification of requirements, design and implementation of new systems and existing system enhancements. Acts as a business subject expert for data, processes and systems. Major Activities include: * Responsible for authorizing the taxation processing for the assignee population. * Interacts with outside vendors and departments regarding tax related functions/issues. * Participates in external/external audits. * Makes recommendations for and institutes process efficiencies and improvements. * Works with members of the HR operations teams and the Relocation and International Assignment areas in support of ongoing duties and initiatives. * Ensures data integrity of the data residing in the Atlas system with regard to such topics as hypothetical tax and expense item taxability. * Leads small scale projects or subcomponents of large scale projects. * Defines changes required to support system changes. * Fully comprehends and administers the year-end process as it relates to the processing of W2 assignee data. 
Qualifications Required: * Advanced knowledge/experience with HR systems and applications, including PeopleSoft. * Expanded knowledge of domestic and international payroll tax processing. * Thorough functional knowledge of one or more HR business areas. * Strong oral and written communication skills. * Excellent analytical, problem solving and organizational skills. Desired: *  Knowledge/experience working with the Atlas system Education Requirements:  Bachelors degree and four years relevant business experience. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.                           To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #HUM001239. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means.  
Profile   Locations  US-NJ-Rahway   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  04/15/2008, 04:51 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  05/02/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-389456 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



