


On Mar 28, 3:00=A0pm, Citizen Jimserac < <EMAILADDRESS> > wrote: m 
It may be that Hahnemann believed the allopathic use of herbs was inherently inferior to a homeopathic approach using the same herbs. I'm not sure about that.  It's interesting that despite the word "allopath" being created by him, the definition we use today has drifted to the point that we use it only as a reference to reductionism, which is an incorrect usage.  His term was meant to describe a simple differentiation based on the principle of homeopathy as a way to invoke the body's natural defenses in the presence of a minute challenge, as opposed to the principle of "allopathy" as a way to combat (or compensate for) something gone wrong.  Neither has anything to do with holism vs. reductionism.  Ironically, vaccine is based on EXACTLY the same principle as homeopathy, although our resident drug apologists (and others) will frantically deny it, whereas I don't believe at all in the safety or effectiveness of vaccine.  This illustrates why it doesn't matter what something is *called* but rather what something DOES, and WHY.  In my opinion, we may *think* we are using an herb allopathically and not homeopathically, when in reality we are doing BOTH at the same time. The key is whether or not those substances are natural to the human body, properly prepared, and beneficially metabolized. 
Sorry I ran on.  Thanks for the reference to Kent's material, I'll check it out. 


