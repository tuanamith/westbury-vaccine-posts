


Computer Game's High Score Could Earn The Nobel Prize In Medicine ScienceDaily (May 9, 2008)   Gamers have devoted countless years of collective brainpower to rescuing princesses or protecting the planet against alien invasions. This week researchers at the University of Washington will try to harness those finely honed skills to make medical discoveries, perhaps even finding a cure for HIV. 
 <URL>  
A new game, named Foldit, turns protein folding into a competitive sport. Introductory levels teach the rules, which are the same laws of physics by which protein strands curl and twist into three-dimensional shapes -- key for biological mysteries ranging from Alzheimer's to vaccines. 
"People, using their intuition, might be able to home in on the right answer much more quickly." 
"I imagine that there's a 12-year-old in Indonesia who can see all this in their head," Baker says. 
Beginning in the fall, Foldit problems will expand to involve creating new proteins that we might wish existed -- enzymes that could break up toxic waste, for example, or that would absorb carbon dioxide from the air. Computers alone cannot design a protein from scratch. The game lets the computer help out when it's a simple optimization problem -- the same way that computer solitaire sometimes moves the cards to clean up the table -- letting the player concentrate on interesting moves. 
"Long-term, I'm hoping that we can get a significant fraction of the world's population engaged in solving critical problems in world health, and doing it collaboratively and successfully through the game," Baker said. "We're trying to use the brain power of people all around the world to advance biomedical research." 

-------------------------------------------------------------------------------- 
Adapted from materials provided by University of Washington. Need to cite this story in your essay, paper, or report? Use one of the following formats:   APA 
 MLA University of Washington (2008, May 9). Computer Game's High Score Could Earn The Nobel Prize In Medicine. ScienceDaily. Retrieved May 11, 2008, from  <URL> ­ /releases/2008/05/080508122520.htm 
Screen shot from the computer game Foldit. The player twists the protein and pulls its arms to move it into its most stable position, which is the shape it would take in nature. He earns the title "H-bond master" for forming new hydrogen bonds; "clash clear expert" for avoiding conflicting electrically charged side chains; and "packing expert" for wrangling the protein into a more compact shape. (Credit: Image courtesy of University of Washington) 

