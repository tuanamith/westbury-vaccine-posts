



J wrote: 


We have what I call a "dumping ground problem" in our community. Where people from surrounding areas take their unwanted cat and figure our community is a good site to dump their unwanted cat. Whether for financial or for health or for simply tired of taking care of their pet. Nearby a College has some students who lend to this problem. 
So there is a steady flow of unwanted cats that go by. My mother cat was all gray and sort of a Persian long hair, who was probably sick when someone dumped her in our area. 
I guess the big lesson to be learned in all of this is the question of whether your cat is going to be "indoor" or "outdoor". Vaccines are assumed to be applied. But vaccines will simply not keep a outdoor cat surviving longer than an indoor cat. 
So the biggest decision on whether you have a cat or not, is whether your cat is going to be indoor or outdoor. If outdoor, it probably will cut the lifespan of the cat in half. The exposure to things outdoors is a long list of hazards. One of the biggest hazards is bad tomcats. I had my female spayed, and a wandering tomcat bite her ear off and then crippled her paw. Tomcats really beat up on other tomcats but they also sense when a female is spayed and will begin to beat up on them. 
So before anyone gets a cat for their home, should consider their surrounding environment and where the home for that cat is going to be. How much exposure to the outdoors. If the cat is going to be fully outdoors, it probably will have 1/2 the lifespan of an indoor cat. And an indoor cat is very much lacking of what I consider a cat's life should be. 

