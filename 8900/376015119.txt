


In our last episode, < <EMAILADDRESS> >, the lovely and talented  <EMAILADDRESS>  broadcast on alt.politics: 


Does the word "alarmist" mean anything to you?  Triage is a standard part of planning for every kind of emergency that responders plan for. 

Of course it is.  When medical resources are limited in a crisis, it only makes sense to allocate them where they will do the most good. 

First, your FUX news story was about treatment.  Flu vaccines are useless as treatment.  How vaccine might be allocated amoung the well may depend upon how a particular strain is manifest.  One of the most chilling aspects of the "Spanish" flu pandemic was that it attacked young, healthy adults, not just the 'usual suspects' for flu susceptability.  In a similar pandemic, if there is a vaccine, it might be best to vaccinate healthy people first  --- like the airline warning: put your own mask on first. 




Now that house Republicans have voted against motherhood, perhaps you think there is a real question here.  But what candidate do think would say "We should save the money, even if there is major pandemic"? 
For flu-like diseases there isn't much problem finding a vaccine.  The problem is getting it into mass production.  You could develop a prototype vaccine for just about every strain you could identify for considerably less than $100 billion.  But stockpiling mass quantities of vaccine for every apparent potential threat does not make much sense.  Flu viruses evolve too fast. 
--  Lars Eighner < <URL> />  <EMAILADDRESS>                           Countdown: 254 days to go. 

