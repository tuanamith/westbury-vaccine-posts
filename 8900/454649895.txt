


 <URL>  
Protest Federal Autism Committee's Deceitful Reversal on Vaccine-Autism  Research 
The inexcusable actions of the Federal members of the Interagency Autism  Coordinating Committee (IACC) in retracting vaccine-autism studies must be  stopped. Sound science must move forward, not thwarted by Federal agencies  with vested interests in on-going vaccine-autism injury litigation. 
The autism advocacy organizations listed below implore parents of children  with autism - and all those who care about the burgeoning rate of autism and  its toll on the health of our children - to take immediate action. 
We are asking you to write a letter of disapproval to key government  decision-makers on autism. Click here and scroll down to "TAKE ACTION" for a  sample letter and instructions on sending. Your letter will be sent to  President Obama, your Senators and Representatives, HHS Secretary Tom  Daschle, the Senate HELP Committee, Senators Christopher Dodd, Joe  Lieberman, and Edward Kennedy, and Congressmen Chris Smith and Joe Barton. 
Your letters are needed NOW. The next IACC meeting is Wednesday, February 4,  2009 - less than two weeks away! 
Here is what happened: 
In a highly unusual departure from procedure, government representatives to  the IACC voted on January 14th against conducting studies on vaccine-autism  research despite approval of the same studies at their prior meeting on  December 12, 2008. The research was supported by numerous autism  organizations and requested by IACC's scientific work groups and Congress.  The maneuver to re-vote was initiated by the IACC's representative from the  CDC and pushed through by the IACC Chair, Dr. Tom Insel, Director of the  National Institute of Mental Health of NIH. Review of the studies was not  listed on the committee's official agenda, in violation of normal committee  practice. 
Unlike most Federal advisory committees, the IACC is dominated by government  representatives occupying 12 of the 18 seats. Dr. Insel admitted at the  meeting that HHS agencies (which includes NIH and CDC) have a conflict of  interest in conducting vaccine-autism research due to "Vaccine Court"  litigation in which HHS is the defendant. Of the 6 non-government (public)  members, 5 voted to retain the vaccine research at the January meeting. The  lone dissenting public member resigned from her organization, Autism Speaks,  the night before the meeting. Autism Speaks has issued a statement  denouncing her vote. 
Click here for more information. 
The Federal members of the IACC must know that the autism community objects  to their manipulation of committee procedures to block unbiased research on  the possible link between vaccines and autism. 
They must hear that parents demand reinstatement by the IACC of the vaccine  studies that were already part of the IACC's Autism Research Strategic Plan,  and to restore the funding already allocated to this research. 
They must acknowledge the inherent conflict of interest of the NIH/CDC in  conducting research on vaccine safety. Research initiatives MUST be  coordinated by an independent committee that includes equal numbers of  representatives from the autism-vaccine injured community and conducted by  independent and non-biased entities. 
We also urge parents to attend the February 4th IACC meeting in Washington  DC if they are able to. Click here for the IACC website to register. We  encourage you to sign up to make a public comment at the meeting. 
Autism Action Network (AAN) Autism One Autism Research Institute Generation Rescue National Autism Association (NAA) SafeMinds Schafer Report Talk About Curing Autism (TACA) U.S. Autism & Asperger Association Unlocking Autism 




