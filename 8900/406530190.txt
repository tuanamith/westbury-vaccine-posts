



U.S. Justice Department was about to file criminal charges against Bruce Ivins 
Ivins' laboratory had been at center of FBI's investigation of anthrax attacks 

WASHINGTON (AP) -- A top U.S. biodefense researcher apparently committed suicide just as the Justice Department was about to file criminal charges against him in the anthrax mailings that traumatized the nation in the weeks following the September 11, 2001, terrorist attacks, according to a published report. 

Mail was detained in November 2001 at the Hamilton, New Jersey, Post Office due to anthrax contamination. 
 The scientist, Bruce E. Ivins, 62, who worked for the past 18 years at the government's biodefense labs at Fort Detrick, Maryland, had been told about the impending prosecution, the Los Angeles Times reported for Friday editions. The laboratory has been at the center of the FBI's investigation of the anthrax attacks, which killed five people. 
Ivins died Tuesday at Frederick (Maryland) Memorial Hospital. The Times, quoting an unidentified colleague, said the scientist had taken a massive dose of a prescription Tylenol mixed with codeine. 
Tom Ivins, a brother of the scientist, told The Associated Press that another of his brothers, Charles, told him Bruce had committed suicide. 
A woman who answered the phone at Charles Ivins' home in Etowah, North Carolina, refused to wake him and declined to comment on his death. "This is a grieving time," she said. 
A woman who answered the phone at Bruce Ivins' home in Frederick declined to comment. 
Justice Department spokesman Peter Carr and FBI Assistant Director John Miller declined to comment on the report. 
Don't Miss Scientist's anthrax lawsuit settled for $2.8 million Henry S. Heine, a scientist who had worked with Ivins on inhalation anthrax research at Fort Detrick, said he and others on their team have testified before a federal grand jury in Washington that has been investigating the anthrax mailings for more than a year.  Watch FBI director discuss "breakthroughs" in anthrax case =BB 
Heine declined to comment on Ivins' death. 
Norman Covert, a retired Fort Detrick spokesman who served with Ivins on an animal-care and protocol committee, said Ivins was "a very intent guy" at their meetings. 
Ivins was the co-author of numerous anthrax studies, including one on a treatment for inhalation anthrax published in the July 7 issue of the journal Antimicrobial Agents and Chemotherapy. 
Just last month, the government exonerated another scientist at the Fort Detrick lab, Steven Hatfill, who had been identified by the FBI as a "person of interest" in the anthrax attacks. The government paid Hatfill $5.82 million to settle a lawsuit he filed against the Justice Department in which he claimed the department violated his privacy rights by speaking with reporters about the case.  Watch U.S. government reach settlement with "person of interest" in anthrax attacks =BB 
The Times said federal investigators moved away from Hatfill and concluded Ivins was the culprit after FBI Director Robert Mueller changed leadership of the investigation in 2006. The new investigators instructed agents to re-examine leads and reconsider potential suspects. In the meantime, investigators made progress in analyzing anthrax powder recovered from letters addressed to two U.S. senators, according to the report. 
Besides the five deaths, 17 people were sickened by anthrax that was mailed to lawmakers on Capitol Hill and members of the news media in New York and Florida just weeks after the September 11 terrorist attacks. The victims included postal workers and others who came into contact with the anthrax. 
In January 2002, the FBI doubled the reward for helping solve the case to $2.5 million, and by June officials said the agency was scrutinizing 20 to 30 scientists who might have had the knowledge and opportunity to send the anthrax letters. 
After the government's settlement with Hatfill was announced in late June, Ivins started showing signs of strain, the Times said. It quoted a longtime colleague as saying Ivins was being treated for depression and indicated to a therapist that he was considering suicide. Family members and local police escorted Ivins away from the Army lab, and his access to sensitive areas was curtailed, the colleague told the newspaper. He said Ivins was facing a forced retirement in September. 
The colleague declined to be identified out of concern that he would be harassed by the FBI, the report said. 
Ivins was one of the nation's leading biodefense researchers. 
In 2003, Ivins and two of his colleagues at the U.S. Army Medical Research Institute of Infectious Diseases at Fort Detrick received the highest honor given to Defense Department civilian employees for helping solve technical problems in the manufacture of anthrax vaccine. 

In 1997, U.S. military personnel began receiving the vaccine to protect against a possible biological attack. Within months, a number of vaccine lots failed a potency test required by federal regulators, causing a shortage of vaccine and eventually halting the immunization program. The USAMRIID team's work led to the reapproval of the vaccine for human use. 
The Times said Ivins was the son of a Princeton-educated pharmacist who was born and raised in Lebanon, Ohio. He received undergraduate and graduate degrees, including a Ph.D. in microbiology 

