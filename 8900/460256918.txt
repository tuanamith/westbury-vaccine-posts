



"Robert Seago" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
The question put was 
"Respondents to the survey were asked the following question: 
Do you believe that grey squirrels should be killed in Scotland or not in an  attempt to conserve red squirrels? 
Yes [34%]    No [54%]    Don't know [11%] 
For people aged 16-24 the responses were: 
Yes [18%]    No [72%]    Don't know [11%] 
They were given the following information beforehand: 
Grey squirrels were introduced to the UK over a hundred years ago and are  now well established. 
Grey squirrels are blamed by some as one of the causes for declining red  squirrel populations as they can out-compete them in many habitats and can  carry the squirrel pox virus which can be lethal for red squirrels. 
Government agencies and conservation organisations now intend to spend  hundreds of thousands of pounds killing thousands of grey squirrels over the  next few years, in an attempt to conserve red squirrel populations.  Most of  the squirrels will be trapped and either shot or struck on the head with a  blunt instrument.  Some animal welfare groups argue that this is unethical  and unlikely to be effective in the long term.  Alternative proposed  solutions include habitat management and the development of a vaccine  against squirrel pox. " 


From  <URL> / 



