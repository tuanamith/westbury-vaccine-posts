


Drugs made by Gilead Sciences Inc. that have been shown to treat the AIDS virus will be tested in healthy people to see if they can prevent the lethal disease. 
Seven large studies using daily doses of Gilead medicine to prevent HIV in people at high risk of infection have begun or are planned to start over the next four years, according to the AIDS Vaccine Advocacy Coalition, a New York-based group that promotes prevention. 
The backfire of Merck & Co.'s AIDS vaccine, along with similar disappointing results from gels designed to protect women from HIV, have sent researchers in search of new prevention tools. Studies of drugs by Gilead, based in Foster City, California, might show whether the approach works years before research on new vaccines and microbicides is complete, said Mitchell Warren, AVAC's executive director. 
``This looks like it's going to get us answers faster than anything else,'' Warren said in an interview yesterday at the 17th International AIDS Conference in Mexico City. ``We need to be ready to start implementing this approach if these trials give positive results.'' 
New approaches to prevention are a top concern for AIDS researchers, advocates and patients at the conference. About 2.7 million people catch the virus each year, and 33 million are infected worldwide, according to UNAIDS, the United Nations agency that coordinates research and care. 
The trials will examine the use of single daily doses of Gilead's Viread and Truvada, alone or in combination, for pre- exposure prophylaxis, or PrEP. As many as 16,000 healthy, uninfected people will be in PrEP studies by late 2009, more than are planned to be in all the world's late-stage vaccine and preventive gel trials combined at that time, Warren said. 
Tenofovir Studies 
Early studies of tenofovir, the active ingredient in Viread, suggested that it might play a role in prevention, said Jim Rooney, Gilead's vice president of clinical research. An injectable form of the drug prevented monkeys from getting SIV, an HIV-related virus, he said. 
Gilead went on to develop the drug as a pill for treatment. Discussion of its preventive possibilities began almost as soon as it got market clearance, said Lynn Paxton, who's coordinating prevention trials of Gilead's drugs for the U.S. Centers for Disease Control and Prevention in Atlanta. 
Doctors use drugs to prevent HIV in people known to have been exposed to the virus through accidental needle sticks. Drugs can also prevent babies from getting the virus from their infected mothers at birth or during breastfeeding. 
Years of Use 
PrEP, however, might involve giving people antiviral drugs for years at a time. That might breed drug-resistant strains in people who become infected with HIV in spite of the drugs, said Charles Gilks, treatment coordinator for the World Health Organization's department of HIV/AIDS. 
``We don't know which way it's going to go at the moment,'' he said July 31 in a telephone interview. ``You could argue that probably only a very few people will develop resistance, but for individuals who do, it would limit their treatment options.'' 
To be worth the risk of giving to healthy people, PrEP would have to prevent infections in at least 60 percent of people who take the drugs, said Robert Grant, an associate investigator at the Gladstone Institute of Virology and Immunology in San Francisco, who's running one study of Gilead's drugs in the U.S. If it works, PrEP would probably drive the number of drug-resistant cases down, he said. 
``The best way to prevent HIV drug resistance is to prevent HIV infections,'' he said at the conference. 
Side Effects 
Other antiviral drugs are associated with side effects such as anemia, pancreatitis, and liver damage, she said. Only a drug with an excellent safety record could be used for prevention in healthy people, she said. HIV also has some difficulty mutating into forms that resist Viread, CDC's Paxton said. 
``When tenofovir came down the pike, it fulfilled a lot of the needed criteria,'' Paxton said July 30 in a telephone interview. ``That's when the idea of PrEP really took off.'' 
A 2006 study of about 900 women, sponsored by the Durham, North Carolina-based public health research group Family Health International, found no more health problems among those taking tenofovir as part of a vaginal gel than in those that got a gel with no drug in it. 
Now, Family Health, along with the CDC, the National Institutes of Health, the Seattle-based Bill & Melinda Gates Foundation, and the Pittsburgh-based Microbicide Trials Network that studies HIV prevention gels are all sponsoring studies of Viread and Truvada. 
Gilead is trying to determine whether demand for the drugs may increase, should any or all of the trials under way show promise, said Gregg Alton, the company's senior vice president and general counsel. 
Demand for PrEP drugs will probably be restricted just to high-risk U.S. patients, and supplies for most poor nations could be produced by makers of licensed generic versions. 
``We're not looking at this as a financial opportunity,'' he said at the conference. ``We're providing the drug for clinical trials along with our expertise and know-how.'' 
SOURCE:  <URL>  
-------------------- 
So the nightmare begins ...  Taka 

