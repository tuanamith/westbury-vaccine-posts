


Job Title:     Flu Program Coordinator Job Location:  NY: Hempstead Pay Rate:      Open Job Length:    full time Start Date:    2008-08-01 
Description:   Flu Program Coordinator &#150; ADM000AF 
Job Description  
Apply Online 
Description Maxim Healthcare Services is one of the leading providers of medical staffing, home health and wellness services in the United States. Founded in 1988, Maxim has rapidly expanded to include 12 divisions and over 400 branch offices in 44 states and the District of Columbia. We have earned a position as an innovative leader in the healthcare industry through our emphasis on patient care and customer service. Today, Maxim is one of the largest privately owned companies in our industry. Our local office is hiring a Flu Program Coordinator with the following responsibilities. Responsibilities and duties include: * Help recruit nurses and administrative staff * Interview, complete human resource process and establish pay rates within a specified range * Orient nursing and administrative staff to Maxim flu program policies and procedures, as well as specific policies and procedures required by Maxim&#8217;s clients * Coordinate scheduling of staff to cover EVERY clinic (100% of clinics MUST be fully staffed &#8212; no exceptions) * Manage inventory of supplies and vaccine * Manage all s corporate office in an organized and timely manner to ensure timely billing * Data entry into Maxim specific management programs and Microsoft Excel to aid in the organization and checks-and-balances of the program * On-Call: Regular office hours are M-F, 8 AM &#8212; 6 PM. Flu Coordinator is expected to perform all customer service responsibilities that may occur outside of the regular office hours. Maxim will provide a cell phone for the purposes of these responsibilities 
Qualifications Requirements:  *  Exceed customer service expectations for each clinic by providing excellent skills in communication, professionalism, organization, pre-clinic planning, post-clinic follow-up, and a consultative approach to serving all of our clients&#8217; needs *  High School Diploma or Equivalent *  Experience in healthcare setting preferred Our company is committed to maintaining a challenging environment that promotes personal accountability, personal growth, and an active role in the driving vision of the company. We offer competitive pay, full benefits including medical, dental and vision coverage as well as 401(k), 529 college savings plan, basic life insurance with the option of supplemental and 17 days paid time off in addition to holidays. To learn more about our services, please visit our website:www.maxhealth.com. APPLY ONLINE today atwww.joinmaxim.com. EOE/AAE 
Profile   Job Field  Administrative   Locations  NY-Hempstead   Schedule  Full-time   Job Level  Entry Level   Education Level  High School Diploma/GED (&amp;plusmn;11 years)   Shift  Day Job  
Additional Information   Posting Date  Jul 9, 2008, 01:03 PM, Montreal, New York, Washington D.C. - (UTC -5:00) 



Please refer to Job code aximhealthcare-57140 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



