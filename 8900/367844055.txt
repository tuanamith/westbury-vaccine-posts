


HPV Vaccine Questions and Answers 
Content reviewed August 2006 
 <URL>  
Why is the HPV vaccine recommended for such young girls? 
Will sexually active females benefit from the vaccine? 
Why is the HPV vaccine only recommended for girls/women ages 9 to 26? 
What about vaccinating boys? 
Should pregnant women get the vaccine? 
The vaccine was less effective in young women who had already been exposed to one of the HPV types covered by the vaccine. 
This vaccine does not treat existing HPV infections, genital warts, precancers or cancers. 
How long does vaccine protection last? Will a booster shot be needed? 
What does the vaccine not protect against? 
Will girls/women be protected against HPV and related diseases, even if they don=92t get all three doses? 
Does this vaccine contain thimerosal or mercury? 
The retail price of the vaccine is $120 per dose ($360 for full series). 
Will the HPV vaccine be covered by insurance plans? 
What kind of government programs may be available to cover HPV vaccine? 
Federal health programs such as Vaccines for Children (VFC) will cover the HPV vaccine. 
Federally Qualified Health Centers or Rural Health Centers, if their private health insurance does not cover the vaccine. 
Will girls/women who have been vaccinated still need cervical cancer screening? 
Yes. There are three reasons why women will still need regular cervical cancer screening. 
Should girls/women be screened before getting vaccinated? 
Will girls be required to get vaccinated before they enter school? 
How is HPV related to cervical cancer? 
How common is HPV? 
How common is cervical cancer in the U.S.? How many women die from it? 
How common are Genital Warts? 
About 1% of sexually active adults in the U.S. (about 1 million people) have visible genital warts at any point in time. 
Is HPV the same thing as HIV or Herpes? 
Can HPV and its associated diseases be treated? 
Are there other ways to prevent cervical cancer? 
Regular Pap tests and follow-up can prevent most, but not all, cases of cervical cancer. 
it cannot tell which types of HPV she has. 
Are there other ways to prevent HPV? 
Sources 
Koutsky LA. Epidemiology of genital human papillomavirus infection. Am J Med. 1997; 102 (5A):3-8. 
 <URL>  

