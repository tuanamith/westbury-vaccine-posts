


Job Title:     Research Biologist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-08-08 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Research Biologist &#150; BIO001886 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career.  The Department of Lead Optimization Pharmacology at the Merck Research Laboratories in Rahway, NJ is seeking an in vivo scientist to join their drug discovery team. Under general scientific supervision, the successful candidate will be primarily responsible for the generation and analysis of data from a broad range of in vivo based studies. The position involves performing establishedin vivoassays and, in addition, involves the development, validation and implementation of novel in vivo models used to evaluate experimental therapeutic agents. 
Qualifications 
Required - A BS degree or higher from an accredited college or university with a minimum of 2-5 years of research experience in pharmaceutical or related industry in experimentalin vivomodeling and surgery. The position requires experience with a broad variety of in vivotechniques including in life procedures, small molecule delivery (P.O., I.V., I.P., S.C., etc.), tissue sample collection and preparation, and a working knowledge of micro-surgical techniques. Furthermore, the incumbent must demonstrate understanding of recognized methods of humane animal treatment and the ability to work effectively both independently and in a team environment. Education Requirements - A BS degree or higher from an accredited college or university. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # BIO001886. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm RepresentativesPlease Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  08/06/2008, 03:25 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  08/13/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-417236 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



