


 <URL>  
Doc MatthewJ's Health Tips 

Saturday, November 8, 2008 "Flu Vaccine" Another influenza season is beginning, and the U.S. Center for Disease Control and Prevention (CDC) will strongly urge Americans to get a flu shot. In fact, the CDC mounts a well-orchestrated campaign each season to generate interest and demand for flu shots. 

But a recent study published in the October issue of the Archives of Pediatric and Adolescent Medicine found that vaccinating young children against the flu appeared to have no impact on flu-related hospitalizations or doctor visits during two recent flu seasons. At first glance, the data did suggest that children between the ages of 6 months and 5 years derived some protection from vaccination in these years. But after adjusting for potentially relevant variables, the researchers concluded that "significant influenza vaccine effectiveness could not be demonstrated for any season, age, or setting" examined. 

Additionally, a Group Health study found that flu shots do not protect elderly people against developing pneumonia--the primary cause of death resulting as a complication of the flu. Others have questioned whether there is any mortality benefit with influenza vaccination. Vaccination coverage among the elderly increased form 15% in 1980 to 65% now, but there has been no decrease in deaths from influenza or pneumonia. 

There is some evidence that flu shots cause Alzheimer's disease, most likely as a result of combining mercury with aluminum and formaldehyde. Mercury in vaccines has also been implicated as a cause of autism. Three other serious adverse reactions to the flu vaccine are joint inflammation, arthritis, anaphalactic shock (and other life-threatening allergic reactions), and Guillain-Barre syndrome, a paralytic autoimmune disease. One credible hypothesis that explains the seasonal nature of flu is that influenza is a vitamin D deficiency disease. Vitamin D levels in your blood fall to their lowest point during flu seasons. Unable to be protected by the body's own antibiotics (antimicrobial peptides) that are released by vitamin D, a person with low vitamin D blood level is more vulnerable to contracting colds, influenza, and other respiratory infections. 

Studies show that children with rickets, a vitamin D-deficient skeletal disorder, suffer from frequent respiratory infections, and children exposed to sunlight are less likely to get a cold. The increased number of deaths that occur in winter, largely from pneumonia and cardiovascular diseased, are most likely due to vitamin D deficiency. Unfortunately, now, for the first time, flu vaccination is also being pushed for virtually all children - not just those under 5. 

This is a huge change. Previously, flu vaccine was recommended only for yougsters under 5, who can become dangerously ill from influenza, This year, the government is recommending that children from age 6 months to 18 years be vaccinated, expanding inoculations to 30 million more school age children. 

The government argues that while older children seldom get as sick as the younger ones, it's a bigger population that catches flu at higher rates, so the change should cut missed school, and parents' missed work when they catch the illness from their children. 

Of course, this policy ignores the fact that a systematic review of 51 studies involving 260,000 children age 6 to 23 months found no evidence that the flu vaccine is any more effective than a placebo. 

Sources: 


There are three major reasons why this government push to vaccinate 84% of the U.S. population with a yearly flu vaccine is so incomprehensible: 

1. The majority of flu shots contain 25 micrograms of mercury; an amount considered unsafe for anyone weighing less than 550 pounds! And which groups are most sensitive to the neurological damage that has been associated with mercury? Infants, children, and the elderly. 2. No studies have conclusively proven that flu shots prevent flu-related deaths among the elderly, yet this is one of the key groups to which they're pushed. 3. If you get a flu shot, you can still get the flu (or flu-like symptoms). This is because it only protects against certain strains, and it's anyone's guess which flu viruses will be in your area. 

So why would you take a flu shot-EVERY YEAR- that has NEVER been proven to be effective, that can give you the very illness you're trying to prevent, and has potential long-term side effects that are far worse then the flu itself? 

The powers that be have done an excellent job of instilling fear into the population so they believe that they must get a shot to stay healthy, but the simple reality is it's doing you more harm than good. And, even if the flu vaccine could effectively prevent the flu, there have been several examples in past years where government health officials have chosen the incorrect influenza strains for that year's vaccine. In 2004, the National Vaccine Information Center described how CDC officials told everyone to line up for A FLU SHOT THAT DIDN'T EVEN CONTAIN THE INFLUENZA STRAIN CAUSING MOST OF THE FLU THAT YEAR! 

TWO-THIRDS OF THIS YEAR'S FLU VACCINES CONTAIN A FULL DOSE OF MERCURY. 

According to Dr. Donald Miller, MD, two-thirds of this year's flu vaccines contain 25 micrograms of thimerosal. Thimerosal is 49% mercury by weight. 

Each dose of these flu vaccines contains more than 250 times the Enviromental Portection Agency's safety limit for mercury. 

By now, most people are well aware that children and fetuses are most at risk of damage from this neurotoxin, as their brains are still developing. Yet the CDC still recommends that children over 6 months, and pregnant women, receive the flu vaccine each year. 

In addition to mercury, flu vaccines also contain other toxic or hazardous ingredients like: Formaldehyde-- a known cancer-causing agent Aluminum-- a neurotoxin that has been linked to Alzheimer's disease Triton X-100--a detergent Phenol (carbolic acid) Toxic to all cells and disables the immune systems primary response mechanism. Ethylene glycol (antifreeze) 

THE EVIDENCE AGAINST FLU VACCINES 

For those of you who are still unconvinced, know that there's plenty of scientific evidence available to back up the recommendation to avoid flu vaccines - if nothing else, then for the simple reason that they don't work, and don't offer any real benefit to offset their inherent health risks. For example: 

A brand new study published in the October issue of the Archives of Pediatric and Adolescent Medicine found that vaccinationg young children against the flu had no impact on flu related hospializations or doctor visits during two recent flu seasons. The researchers concluded that "significant influenza vaccine effectiveness could not be demonstrated for any season, age, or setting" examined. 

A study published in the Lancet just two months ago found that influenza vaccination was NOT associated with a reduced risk of pneumonia in older people. This supports a study done five years ago, published in the New England Journal of Medicine. Research published in the American Journal of Respiratory and Critical Care Medicine last month also confirms that there has been no decrease in deaths from influenza and pneumonia, despite the fact that vaccination coverage among the elderly has increased form 15% in 1980 to 65% now. Last year, researchers with the National Institute of Allergy and Infectious Diseases, and the National Institutes of Health published this conclusion in the Lancet Infectious Diseases: "We conclude that frailty selection bias and use of non-specific endpoints such as all-cause mortality have lead cohort studies to greatly exaggerate vaccine benefits." A large-scale, systematic review of 51 studies, published in the Cochrane Database of Systematic Reviews in 2006, found no evidence that the flu vaccine is any more effective than a placebo in children. The studies involved 260,000 children, age 6 to 23 months. 

Might Influenza be Little More Than a symptom of Vitamin D Deficiency? 

Vitamin D, "the sunshine vitamin" may very well be one of the most beneficial vitamins there is for disease prevention. Unfortunately it's also one of the vitamins that a vast majority of people across the world are deficient in due to lack of regular exposure to sunshine. 

Published in the journal Epidemiology and infection in 2006, the hypothesis presented by Dr. John Cannell and colleagues in the paper Epidemic Influenza and Vitamin D actually makes a lot of sense. 

They raise the possibility that influenza is a symptom of vitamin D deficiency! 

The vitamin D formed when your skin is exposed to sunlight regulates the expression of more than 2,000 genes throughout your body, including ones that influence you immune system to attack and destroy bacteria and viruses. Hence, being overwhelmed by the "flu bug" could signal that your vitamin D levels are too low, allowing the flu virus to overtake your immune system. 

How to Prepare For Flu Season Without Getting a Flu Shot. 

I often find that some of the simplest explanations are the truest, and this sounds about as simple as it gets. And, getting appropriate amounts of sunshine (or taking a vitamin D supplement when you can't get healthy amounts of sun exposure) is one of my KEY preventive strategies against the cold and flu, as it has such a strengthening effect on your immune system. 

Interestingly, last week the American Academy of Pediatrics doubled it recommended dose of vitamin D. Unfortunately this is still a woefully inadequate recommendation as the dose should be TEN times larger. Rather than going from 200 to 400 units per day, it should have increased to about 2,000 units per day. 

For most of you reading this it is "vitamin D winter," which means there simply isn't enough sunshine to make significant amounts of vitamin D, so you will need to use a tanning bed or take oral supplements. 

Although supplements are clearly inferior to sunlight or safe tanning beds, I am becoming more convinced of the value of vitamin D supplements as they are less potentially toxic than my intial impression, and they are certainly more convenient and less expensive than a tanning bed. 

For those in the winter with no or very limited exposure to sunshine, 4,000-5000 units per day wold seem appropriate for most adults. If you are very heavy you may need to double that dose, and for children the dose can be half that. 

The key though is to make sure yo monitor your vitamin D levels by blood testing, to make sure your levels are therapeutic and not toxic. 

I advocate getting your vitamin D levels tested regularly, but you need to be aware of where you're getting your tests done. For an in-depth explanation of what you MUST know before you get tested, please read the article Test Values and Treatment for Vitamin D Deficiency. 

You can also use vitamin D therapeutically to TREAT the flu. 

But  please understand that if you are taking the above doses of vitamin d the odds of you getting the flu are VERY remote. The dose of vitamin D you can use would be 2,000 units per kilogram of body weight (one pound is 0.45 kg). The dose would be taken once a day for three days. 

This could be a very large dose if you were very heavy (2-300,000 units per day). This is the dose that Dr. John Cannell, founder of the Vitamin D council, has been using very successfully for a number of years. 

I have not received a flu shot nor had the flu in over 15 years. Here are the other "secrets" I use to keep the flu (and other illnesses) at bay: 


If you battle with the cold or Flu on a regular basis, think about this article and make some life style changes to better you life and your health. 

Dr. Matt 


  


