


TRUST WILL NOT CULL BADGERS   Date : 29.04.08       <URL>      The National Trust will not co-operate with a proposed cull of TB-infected badgers, provoking fury among farmers battling to control an epidemic of the disease in cattle. 
The charity, a powerful landowner and lobbying voice with more than 3.5 million members, said it did not intend to help with an imminent badger cull in Wales - or any similar operation in England. 
It said it would actively encourage its tenant farmers to do like wise but conceded that, in many cases, it did not have the power to force them. The trust's refusal to co-operate could effectively squash any cull plans but has brought it into conflict with its tenant farmers. 
The National Trust owns 70,000 acres of land in Devon and Cornwall and its tenant farmers in the region - faced with record levels of bovine TB - have demanded an urgent cull of badgers, which help to spread the disease. Unlike the Welsh Assembly, the UK Government has refused to sanction any cull. 
The National Farmers' Union (NFU) in the South West accused the National Trust of making a "cowardly" bid to placate its wildlife-loving members. But the Badger Trust, which plans to ask for a judicial review of the cull decision in Wales, welcomed the announcement, saying it was based on a sound review of the science. 
The Welsh Assembly has agreed to support a pilot cull of badgers as part of a larger programme which will include more cattle testing and support for a vaccine. 
Although the National Trust, which has more than 123,000 acres of land in Wales, said it would not rule out supporting any type of cull, it did not believe there was a scientific basis for the cull of "hot spots" in Wales because of the danger of that method simply dispersing infected badgers. 
However, the Welsh Assembly has yet to decide where culls will take place and what method will be used, so it is not known if the National Trust's lack of co-operation will halt the plans. 
Iwan Huws, the National Trust's director for Wales, said: "We feel that a targeted badger cull in an intensive action pilot area would be counter productive and would not make a major contribution to controlling bovine TB in cattle." 
The trust co-operated with culling trials in the South West between 1998 and 2005. If that trial had shown culling could reduce TB in cattle by more than 80 per cent, the trust said it could not have objected to a cull - but results seemed to show localised culling could increase TB by dispersing infected badgers. 
David Bullock, the trust's head of nature conservation, said: "We have obligations both to badgers and the people who use our land. We are not persuaded it is the right thing to do. We want a resolution of this issue. We worry that investment in research and development for vaccines will not be maintained when we are so nearly there." 
The trust insisted it was not opposed to culling "per se" but wanted a "strategic approach" which included testing, vaccination and biosecurity measures. 
Outlining the distinction between land managed directly by trust wardens and farmed land controlled by tenants, a trust spokesman said: "We would have to have discussions with them and try to encourage them not to participate. 
"Some of our tenancies are effectively hereditary. Our property teams are talking to people all the time. Because of the nature of the agreement, some farmers could chose to take part in this cull. It is how we present the case." 
Devon beef farmer Richard Haddock is a National Trust leaseholder. His farm is clear of bovine TB but neighbouring trust tenants have not been as lucky. 
He said: "Some of the people at National Trust headquarters do not live in the real world. The UK Government does not want to do anything about the badgers - it's quite happy to put the livestock industry out of business and has no intention of a badger cull. 
"Many National Trust tenant farmers will go to the Court of Human Rights for compensation. Many tenants are already thinking about coming together to set up a tenants association - there is so much feeling out there. 
"I'm against a mass slaughter. I want to see a targeted cull of setts and I think that could be achieved." 
The Government's Independent Scientific Group on Cattle TB reported last June that killing wildlife was not an effective way to control the disease in cattle because culling caused badgers to move more freely and more widely, increasing the spread of the disease. 
However, seemingly in direct contradiction, the then chief scientific adviser Professor Sir David King argued that a cull could play a "meaningful" part in tackling the disease. 
A spokesman for the Department for the Environment, Food and Rural Affairs said yesterday there was no "time frame" for a decision on culling, but minister Hilary Benn had said it would happen "on his watch". 
Farmers have been pushing for a decision; the cost of testing and compensation will cost taxpayers more than £1 billion over the next five years. 
NFU spokesman Ian Johnson said many farmers would regard the National Trust's position as a "short-sighted public relations game". He added: "It might be a populist stance but it's certainly not the practical one. If you are a member of the National Trust or a tenant farmer or a visitor, this impacts on you one way or another. 
"The farmer is under extreme pressure and the visitor, the taxpayer, is having to pay to keep the lid on an untenable situation. 
"Without the backing of people like the National Trust, we are not going to move forward with this problem." 
But wildlife campaigners welcomed the National Trust's opposition to the Welsh cull plans. Somerset Wildlife Trust spokesman Lisa Schneidau said: "We do not believe that a badger cull would be effective or practical in helping to control TB in cattle. The science simply does not support a cull." 
Badger Trust spokesman Trevor Lawson said: "We know the National Trust has fully evaluated the science behind badger culling and we are confident its members will accept its judgment."     Reader comments   
We had all our healthy cattle and sheep illegally slaughtered during the Foot & Mouth outbreak in 2001. We will not allow DEFRA to enter our land again to slaughter healthy animals. Just like in 2001 there is no scientific justification for the mindless slaughter of healthy animals.  Didi Phillips, North Cornwall  



It is entirely understandable that so many farmers want something to be done, but before embarking on widespread killing of wild animals you need to be very sure that this would produce the required outcome. The research simply does not support that. For that reason, I support the NT's decision and like Sara, will not allow any cull on our land (where we do have badgers). The sooner the vaccines are ready, the better.  David, South Hams  



This reaction from some landowners against culling was entirely predictable and is the main reason why it probably won't succeed if it is carried out. It only needs a few pockets of refusers for the dangers predicted by the research to come about - i.e. that a cull might actually make things worse. A cull will cause deep division within the population of the south west, with severe resentment on both sides. Hilary Benn was never going to make a decision until after the local elections, but then he will still be in a no-win situation after that. What a mess!  Francis, Crediton  



I am really pleased to hear that "The National Trust" are not going to co-operate with the barbaric culling of Badgers. I would like to echo Saras' comments, anyway who gave the TB to the Badgers in the first place.  Peter, Mid-Cornwall  



Well done the National Trust! I am a small farmer in Cornwall and I also will not cooperate with this ridiculous and barbaric cull. TB was eradicated in humans by a combination of vaccination and improved hygiene. Anyone who has seen how dairy cattle are housed in this country on wet cold concrete would not be surprised at the problem with TB. Interesting how the damper parts of the country are worst affected.  sara, cornwall  



