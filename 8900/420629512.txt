


 <URL>  
Analysis Confirms AIDS Epidemic Hits Men Hard  
Thursday, September 11, 2008 
WASHINGTON - AIDS remains largely a disease of gay and bisexual men in the United States but also disproportionately infects black women, according to an analysis published on Thursday.  
Last month, the U.S. Centers for Disease Control and Prevention reported that more than 56,000 people in the United States become newly infected with the human immunodeficiency virus each year, far more than previous estimates of about 40,000.  
Now the CDC has further analyzed those numbers to find the fatal and incurable virus largely infects men who have sex with men, or MSM -- a group that includes gays, bisexuals and men who may have the occasional sexual encounter with other men.  
"The male-to-male sexual contact transmission category represented 72 percent of new infections among males, including 81 percent of new infections among whites, 63 percent among blacks, and 72 percent among Hispanics," the report said.  
Of the new infections in 2006, more than half were among gay and bisexual men, the CDC found. Of these, 46 percent of new infections were among whites, 35 percent among blacks and 19 percent in Hispanics.  
But among the overall U.S. population, more blacks are affected -- 46 percent of new infections were among blacks.  
The CDC said it needed to redouble prevention efforts, especially in the black community.  
"The alarming number of new infections among young black MSM underscores the need to ensure that each new generation has the knowledge and skills to prevent HIV infection beginning early in their lives," the report reads.  
Girls and women made up 27 percent of new infections, with high-risk sexual contact with men causing 80 percent of new infections.  
"Among females, 61 percent of infections were in blacks, 23 percent were in whites, and 16 percent were in Hispanics," the CDC report reads.  
There is no cure for the AIDS virus, which is transmitted in bodily fluids such as blood, semen and breast milk. Around the world, sexual contact is by far the most common mode of transmission although people who use contaminated needles can be infected, and blood transfusions also can cause infection.  
"African-Americans make up 12 percent of the total U.S. population, yet represented 45 percent of new HIV infections in the United States in 2006," the CDC wrote.  
Globally, 33 million people are infected with HIV and 25 million have died of it. There is no vaccine or cure although drug cocktails can help control the infection. 

