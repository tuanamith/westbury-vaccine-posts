



To update you on Wyoming, below is a copy of a letter I sent on March 30th to state officials.   
On Thursday, I had an e-mail from Representative Sue Wallis  <EMAILADDRESS>  , who said, *-The Wyoming Legislature has no intention of usurping what rightfully belongs to the cities, towns, and counties. *-  I do not know if she is reflecting the sentiment of all members of Wyoming's Legislature, but it certainly is contrary to the declaration she makes on her website 'Home' ( <URL> ) that *-With hard work, and your help, I know that we will be able to accomplish good legislation, and hopefully argue against that which is unnecessary, expensive, unproductive or harmful to the citizens of Campbell County and the State of Wyoming. *- 
Kris 
March 30, 2008 
Governor Dave Freudenthal                    Attorney General Bruce A. Salzburg State Capitol, 200 West 24th Street         State Capitol, 200 West 24th Street             Cheyenne, WY 82002-0010                      Cheyenne, WY 82002-0010 

RE:  WYOMINGS ANNUAL RABIES LAWS/ORDINANCES 
Greetings Governor Freudenthal and General Salzburg:   
The time for Wyoming to adopt a uniform, state-wide 3 year canine rabies immunization protocol conforming to the national standard is long overdue.  Scientific data demonstrating a minimum duration of immunity for the canine rabies vaccine of 3 years by challenge and 7 years serologically was incorporated into the 2003 American Animal Hospital Associations Canine Vaccine Guidelines more than 5 years ago. 

Because the rabies vaccine is the most potent of the veterinary vaccines and associated with significant adverse reactions, it should not be given more often than is necessary to maintain immunity.  Adverse reactions such as autoimmune diseases affecting the thyroid, joints, blood, eyes, skin, kidney, liver, bowel and central nervous system; anaphylactic shock; aggression; seizures; epilepsy; and fibrosarcomas at injection sites are linked to rabies vaccinations. 
The rabies is a *killed* vaccine and contains adjuvants to enhance the immunological response.  In 1999, the World Health Organization "* classified veterinary vaccine adjuvants as Class III/IV carcinogens with Class IV being the highest risk," *[1] and the results of a study published in the August 2003 Journal of Veterinary Medicine documenting fibrosarcomas at the presumed injection sites of rabies vaccines stated, *In both dogs and cats, the development of necrotizing panniculitis at sites of rabies vaccine administration was first observed by Hendrick & Dunagan (1992).* [2]  According to the 2003 American Animal Hospital Associations Canine Vaccine Guidelines, *"...killed vaccines are much more likely to cause hypersensitivity reactions (e.g., immune-mediated disease)."  * 
Many, if not all, annual rabies vaccines are the 3 year vaccine relabeled for annual use -- Colorado State University's Small Animal Vaccination Protocol for its veterinary teaching hospital states: *Even with rabies vaccines, the label may be misleading in that a three year duration of immunity product may also be labeled and sold as a one year duration of immunity product. * Wyoming city laws/ordinances requiring annual rabies boosters do not enhance an animals immunity and needlessly expose dogs to the risk of adverse reactions.  The- American Veterinary Medical Association's 2001 Principles of Vaccination- state that *Unnecessary stimulation of the immune system does not result in enhanced disease resistance, and may increase the risk of adverse post-vaccination events.*  
Of importance to policy makers is data indicating that compliance rates are no higher in states with annual rabies immunization requirements than in those with triennial protocols.  A 2002 report compiled by the Banfield Corporation for the Texas Department of Health on rabies vaccination rates determined that a* comparison of the one-year states and the three-year states demonstrates no difference in the delinquency rates* and that, *A paucity of scientific data exists to demonstrate a clear public health benefit of a one-year vaccination protocol versus a three-year vaccination protocol.* [3]  
The Center for Disease Controls National Association of State Public Health Veterinarian's- Compendium of Animal Rabies Prevention and Control 2007- recommends that: "*Vaccines used in state and local rabies-control programs should have at least a 3-year minimum duration of immunity. * They state further that, *No laboratory or epidemiologic data exist to support the annual or biennial administration of 3- or 4-year vaccines following the initial series.* 
Medical, epidemiological, and scientific data, as well as the recommendations of the Center for Disease Controls National Association of State Public Health Veterinarians -Compendium of Animal Rabies Prevention and Control 2007 -and the- American Animal Hospital Associations Canine Vaccine Guidelines -for 2003 and 2006 evidence the need for Wyoming to revise its state-wide rabies immunization requirements for dogs by adopting the national 3 year standard in all municipalities. 
Perhaps Attorney General Salzburgs department could issue a legal opinion on whether or not municipalities with annual rabies vaccination requirements are violating Wyomings consumer protection laws by mandating that dog owners pay for yearly veterinary procedures from which their animals derive no benefit.  
I strongly urge you to conform all of the states rabies immunization requirements to the 3 year standard so that Wyoming dogs are no longer put at unnecessary risk of harmful side affects by being forced to receive medically unwarranted annual rabies boosters. 
Sincerely, 
Kris L. Christine Founder, Co-Trustee THE RABIES CHALLENGE FUND 
cc:       Cheynne Mayor Jack Spiker Cheyenne City Council Director of Department of Agriculture, John Etchepare Deputy Director of Department of Agriculture, Jason Fearneyhough    Wyoming Legislature 
-------------------------------------------------------------------------------- 
[1] IARC Monographs on the Evaluation of Carcinogenic Risks to Humans:  Volume 74, World Health Organization, International Agency for Research on Cancer, Feb. 23-Mar. 2, 1999, p. 24, 305, 310. 
[2] Fibrosarcomas at Presumed Sites of Injection in Dogs: Characteristics and Comparison with Non-vaccination Site Fibrosarcomas and Feline Post-vaccinal Fibrosarcomas; Journal of Veterinary Medicine, Series A August 2003, vol. 50, no. 6, pp. 286-291(6) 
[3] The White Paper, Texas Department of Public Health, Zoonosis Control, Options for Rabies Vaccination of Dogs and Cats in Texas, 2002 



--  Kris L. Christine 

