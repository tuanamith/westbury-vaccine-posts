


O wrote: 
Scientists Question FBI Probe On Anthrax Ivins Could Not Have Been Attacker, Some Say 
By Joby Warrick, Marilyn W. Thompson and Aaron C. Davis Washington Post Staff Writers Sunday, August 3, 2008; A01 
 <URL>  
For nearly seven years, scientist Bruce E. Ivins and a small circle of fellow anthrax specialists at Fort Detrick's Army medical lab lived in a curious limbo: They served as occasional consultants for the FBI in the investigation of the deadly 2001 anthrax attacks, yet they were all potential suspects. Over lunch in the bacteriology division, nervous scientists would share stories about their latest unpleasant encounters with the FBI and ponder whether they should hire criminal defense lawyers, according to one of Ivins's former supervisors. In tactics that the researchers considered heavy-handed and often threatening, they were interviewed and polygraphed as early as 2002, and reinterviewed numerous times. Their labs were searched, and their computers and equipment carted away. 
The FBI eventually focused on Ivins, whom federal prosecutors were planning to indict when he committed suicide last week. In interviews yesterday, knowledgeable officials asserted that Ivins had the skills and access to equipment needed to turn anthrax bacteria into an ultra-fine powder that could be used as a lethal weapon. Court documents and tapes also reveal a therapist's deep concern that Ivins, 62, was homicidal and obsessed with the notion of revenge. 
Yet, colleagues and friends of the vaccine specialist remained convinced that Ivins was innocent: They contended that he had neither the motive nor the means to create the fine, lethal powder that was sent by mail to news outlets and congressional offices in the late summer and fall of 2001. Mindful of previous FBI mistakes in fingering others in the case, many are deeply skeptical that the bureau has gotten it right this time... 
< click link to continue > --  






