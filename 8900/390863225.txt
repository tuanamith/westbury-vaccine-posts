


VENEZUELA: Co-operatives Turn Idle Landed Estates Green 
By Humberto Marquez Inter Press Service June 15, 2008 
Barinas, Venezuela, Jun 14 (Tierramirica) - "As far as you can see, there was not one litre of milk produced, not even an ear of corn," says Josi Tapia Coiran, turning with his arms outstretched, pointing to the horizon of the Venezuelan savannah dotted by trees. "Now we produce 500 litres of milk per day and we harvested one million kilos of maize." 
He is referring to the achievements of the Brisas del Masparro co-operative, set in the plains of Barinas, in southwest Venezuela. Coiran, as he is known by everyone, is a former day labourer and tractor driver for large farms in the area, and is now the co-operative's president. 
"Once there was a forest here, but the large estate owners took all the lumber. They left a few trees and thousands of hectares of stubble that we are cleaning up little by little and planting with forage grass and maize," 
says Coiran, adding "they had abandoned this, left it lying fallow, and that is why we took it over." 
He and his fellow co-operative members show this reporter vast stretches of plains that are as flat as a billiard table amidst weeds, a marsh here and there, pastures and fields being ploughed for planting, underscoring the co-operative's explanation that what they had occupied was unproductive land. 
We come across flocks of herons, scarlet ibis, and some flickers. "We want to conserve all that we can. We decided not to take down any trees, but rather get rid of weeds and pests as we progress," says Miguel Mindez, another co-operative member. 
President Hugo Chavez launched a "war" against large estates with a 2001 land act that laid the groundwork for a government "recovery" of rural land whose private ownership and productivity could not be proved. There continue to be clashes over land between large landowners and small farmers. 
In 1999, large rural estates covered six million hectares in Venezuela. Two million hectares have been confiscated by the government, which handed over 60 percent of that to more than 100,000 rural families, according to official figures. 
Furthermore, 98,500 farms that cover 4.3 million hectares have been regularised through the agrarian charter, which grants possession, but not ownership, of the land, which belongs to the government. 
The Santa Rita "hacienda", or rural estate, on the banks of the Masparro river, extends across 31,000 hectares but has no more than 1,800 head of cattle, according to the co-operative. Peasant groups occupied it in 2002 and 2003, and the government assigned them some 16,000 hectares, leaving the rest to the former owners. 
The co-operative that has made the most progress is Brisas del Masparro, with 56 members on 803 hectares. Five years ago they received a loan of 156,000 dollars that was invested in cattle, horses, equipment and inputs, and in the first crops. 
They now have a double-purpose herd, for meat and milk, based on crosses between Cebz and Holstein breeds acclimated to the tropical plains. 
COMMON PROPERTY 
A large house once used as a bunkhouse for labourers and as a storage facility by the former estate has been turned into a community centre. The first impression is one of disorder. A pile of tractor parts in the yard marks the only point in the area where there is a signal for the satellite phone. 
Pigs and chickens follow a young man as he rubs the kernels off corn cobs. 
Another man cleans the floor of the corridor, which is also the site of co-operative assemblies. It has been a while since the walls have received a fresh coat of paint. 
In the back are a kitchen and a large dining table for those who are working on a given day and the families that have settled in improvised homes in the surrounding area. On one wall there are faded posters of Chavez and of the Salvadoran revolutionary Farabundo Martm (1893-1932). 
"We are socialists. We work as a community, according to the abilities of each, and we take turns so that we aren't always doing the same thing, and to learn about everything. We realised that if we were each on our own it would be very difficult to get ahead and leave behind our days as labourers, as employees enriching someone else," says Neptalm Quintana, who for many years worked in artificial insemination of cows on the region's large ranches. 
He is leaning against a fence of the dairy, where children are milking cows for the second time today. "We get about five litres of milk per animal per day -- above the average" in the area, which is less than four litres per cow, says Quintana. 
Every day, the co-operative donates 20 litres of milk to the two small schools nearby. "We provide the cup of milk that each child needs," says Mindez proudly. 
"But if in addition to communally owned animals one of us has a cow or a horse, or gets a pig, it can be raised with the others and sold by the individual owner. Some portion will be given to the co-operative, but we don't oppose that sort of ownership. What we do want is the land and other life-sustaining projects," says Coiran. 
The income "is used for the expenses that are also shared, for production or for food, and each member receives an additional 400 bolmvares (186 dollars) per month as an advance of what would be due for their role in managing the co-operative at the end of the year," explains Iraima Benaventa, a young mother of two who is in charge of logistics. 
Benaventa, who is taking part in a secondary-level distance learning programme, records the purchases that another member has brought from the city -- pasta, rice, cattle vaccines -- and supervises the younger members in clean-up and kitchen activities. The meal today is rice and beef. 
Brisas del Masparro will begin construction this year of housing units for 56 families, with a self-construction plan backed by the government. "We will build them together in the style of a little town in order to facilitate and reduce costs of services like water, electricity and gas, with a sports field, a town square and a community centre, and perhaps even a pool," says one member. 
BETTER COMMUNITY 
Las Piedras, one corner of the Masparro co-operative, is an hour's drive from Barinas, the regional capital, passing by Sabaneta, President Chavez's birthplace. Then comes another hour of driving over open land and gravel that the co-operative members are requesting to be paved in benefit of the entire community. 
"The farms in this sector were very unproductive five years ago. But with our efforts, the government programmes arrived. The road was opened up, a land plan was begun, possession papers were given to individual farmers or co-operativists, and credits were granted," says Coiran. 
In Las Piedras "we went from nearly zero to 21,000 litres of milk per day (national output is 1.3 to 1.7 million litres daily, according to different sources). Now there are people raising more cattle, planting maize, fruit trees and pastures," says the co-op president. 
Caracciolo Rammrez, an independent farmer, has around 40 hectares near the co-operative's land. 
"The government has helped with agrarian charters, with some financing, and with the road. I will do some home improvements, my oldest daughter began university -- I am seeing the results," says Rammrez, offering this reporter a cool oat drink with ice under the porch roof at his brick home. 
Meanwhile, the co-operative is preparing a larger area than last year to plant maize, building a new cow barn and refurbishing the old one for mechanised milking, and seeking financing to install some cooling tanks that will help them benefit more from each litre of milk. 
"All around the world there is a food crisis. They want to take food and make it into fuel. We don't agree with that and we pay back the government's support by producing more food. This country can't continue feeding the people based on imports when there is so much land waiting to be worked," 
says Coiran. 
In the 2004-2007 period, Venezuela's food production grew 3.4 percent, from 18.9 to 19.6 tonnes annually, according to government figures. 
But former agriculture minister Hiram Gaviria points to how much is still lacking: in per capita terms, Venezuela today produces 88 percent of the food it generated in 1998, he told Tierramirica. 
A long way from Barinas, across the Atlantic in Rome, world leaders gathered Jun. 3-5 at the United Nations Food and Agriculture Organisation (FAO) summit to debate ways to overcome the current food crisis. 
At the former Santa Rita hacienda, thousands of hectares "recovered" by the government were handed over to other co-operatives or small farmers' associations that have not had the same success as Brisas del Masparro. 
"We hold assemblies for the zone and we offer support. Even farther away, to Apure (in the country's far southwest) we have taken our experience and the young milk cows we have produced, which we sell them at low prices, but the individualism of many people means that what they are looking for is their own land," says Coiran. 
Back in Barinas, one such individual, Alejandro, accompanies Tierramirica through the countryside. "We want to form a co-operative to work, but each one has his parcel of land that is free to be sold. With the agrarian charter, the land can't be transferred and will always belong to the government." 
But Alejandro says that the neighbours of Brisas del Masparro are sympathetic to the experiment of the co-operative, and would like to take it as testimony of what can be achieved when working together. 
"They have their reasons, the support of the revolutionary government, and that's good, but what will happen tomorrow if the government changes? One wants a piece of land to work, but also to leave to one's children," he says, as the orange sun sets over the plains of southwest Venezuela. 
(*Humberto Marquez is an IPS correspondent. Originally published by Latin American newspapers that are part of the Tierramirica network. Tierramirica is a specialised news service produced by IPS with the backing of the United Nations Development Programme, United Nations Environment Programme and the World Bank.) (END/2008) 
 <URL>  
This email was cleaned by emailStripper, available for free from  <URL>  
------------------------------------ 
Yahoo! Groups Links 
<*> To visit your group on the web, go to: 
 <URL> / 
<*> Your email settings: 
Individual Email | Traditional 
<*> To change settings online go to: 
 <URL>  (Yahoo! ID required) 
<*> To change settings via email: 
 <EMAILADDRESS>   <EMAILADDRESS>  
<*> To unsubscribe from this group, send an email to: 
 <EMAILADDRESS>  
<*> Your use of Yahoo! Groups is subject to: 
 <URL> / 

