


What is the Real Cause of Influenza Epidemics? 

Influenza does not follow the predicted patterns for infectious diseases. In fact, there are several conundrums associated with influenza epidemics, such as: 
1. Why is influenza both seasonal and ubiquitous -- and where is the virus between epidemics? 
2. Why are influenza epidemics so explosive? 
3. Why do epidemics end so abruptly? 
4. What explains the frequent coincidental timing of epidemics in countries of similar latitudes? 
5. Why did epidemics in previous ages spread so rapidly, despite the lack of modern transport? 
A theory gaining weight in the scientific community explains influenza epidemics as a result of a dormant disease, which become active in response to vitamin D deficiency. This theory provides answers for many of the above questions. A disease that remains dormant until vitamin D-producing sunlight exposure is reduced by a winter or rainy season would explain a widespread seasonal disease with a rapid onset and decline. 
There is compelling epidemiological evidence that indicates vitamin D deficiency is just such a "seasonal stimulus." Recent evidence confirms that lower respiratory tract infections are more frequent, sometimes dramatically so, in those with low levels of vitamin D. Researchers have also found that 2,000 IU of vitamin D per day abolished the seasonality of influenza, and dramatically reduced its self-reported incidence. 
Sources: Virology Journal 2008, 5: 29 CBCNews.ca October 22, 2008 

---------------------------------------------------------------------------= ------------------ 
Find Out More 

 Dr. Mercola's Comments: With flu season in full swing here in the United States, you need to be aware that your vitamin D levels play a direct role in your risk of getting the flu. 
The vitamin D levels in your blood fall to their lowest point during flu season, which generally coincide with low-sunlight seasons. Less than optimal vitamin D levels will significantly impair your immune response and make you far more susceptible to contracting colds, influenza, and other respiratory infections. 
Dr. John Cannell and colleagues introduced the hypothesis that influenza is merely a symptom of vitamin D deficiency in their paper Epidemic Influenza and Vitamin D, published in the journal Epidemiology and Infection two years ago, which adds even more weight to this latest research in the Virology Journal. 
Unfortunately, conventional medicine=92s answer to preventing the flu is not to increase vitamin D levels, but rather to encourage, or even mandate, flu shots. What they don=92t tell you is that flu shots, at best don=92t work, and at worst, can make your health worse. 
New Studies Show Flu Shots Don=92t Even Work 
A recent study published in the October issue of the Archives of Pediatric & Adolescent Medicine found that vaccinating young children against the flu had no impact on flu-related hospitalizations or doctor visits during two recent flu seasons. 
Additionally, no studies have conclusively proven that flu shots prevent flu-related deaths among the elderly, even though this is one of the key groups to which they=92re pushed. 
Yet despite these findings, physicians like Dr. Danuta Skowronski, epidemiologist at the B.C. Centre for Disease Control, keep insisting that the benefits of the flu vaccine outweigh the risks for most people. 
I wholeheartedly disagree. 
If you actually weigh the benefits found by these studies (no apparent benefits) against the reported risks (fever, malaise, the flu, allergic reactions and Guillain-Barr=E9 syndrome, just to name a few), you=92d have to employ some fairly odd math to come to that conclusion. 
In the CBC News article above, a man who contracted Guillain-Barr=E9 syndrome and was paralyzed for almost five months after receiving his flu shot wants to warn others of the risks inherent in this widely pushed vaccine. 
Guillain-Barr=E9 syndrome is an autoimmune disease that attacks your nervous system =96 similar to muscular sclerosis. The B.C. Centre for Disease Control estimate your chances of developing that particular disease from the flu shot as one in a million. However, if you get it, the consequences can be severe. 
Research Shows Optimal Vitamin D Levels Protect You From the Flu, Naturally 
In the United States, the late winter average vitamin D is only about 15-18 ng/ml, which is considered a very serious deficiency state. It=92s estimated that over 95 percent of U.S. senior citizens may be deficient, along with 85 percent of the American public. No wonder the flu runs rampant each year. 
Remarkably, researchers have found that 2,000 IU of vitamin D per day abolished the seasonality of influenza! 
Please note that this is far higher than the recommended daily allowance (RDA) spouted by public health agencies like the American Academy of Pediatrics, which just announced that they=92re doubling the RDA of vitamin D for children to 400 IU. This new guidance still falls absurdly short of what=92s needed to keep kids healthy, especially during flu season. 
In order to prevent the flu, children need 2,000 IU a day of vitamin D, while adults need anywhere between 4,000 to 5,000 IU per day. The key is to make sure you monitor your vitamin D levels by blood testing, to make sure your levels are therapeutic and not toxic. 
For more information about safe sun exposure and tanning, vitamin D testing, and the recommended forms of supplementation when sufficient sunlight is not available, please see my article Test Values and Treatment for Vitamin D Deficiency, which includes recent, and vital, updates. 
Vitamin D Can Also TREAT the Flu! 
If you are taking the above doses of vitamin D the odds of you getting the flu are very remote.  However, if you do come down with the flu, keep in mind that you can also use vitamin D therapeutically to TREAT the flu. 
The therapeutic dose of vitamin D is 2,000 units per kilogram of body weight (one pound is 0.45 kg).  The dose would be taken once a day for three days. (This could be a very large dose if you are very heavy =96 as high as 2-300,000 units per day). 
This is the dose that Dr. John Cannell, founder of the Vitamin D Council, has been using very successfully for a number of years. 
If you start this program early on in the illness, it should be able to completely wipe out the flu in short order. 
Another useful supplement you could try, should you come down with a case of the flu, is olive leaf extract, which you can find in most any health food store. Olive leaf extract has been found to be a potent broad-spectrum antiviral agent, active against all viruses tested, including numerous strains of influenza and para-influenza viruses. 


Related Articles: 

  Avoid Flu Shots With the One Vitamin that Will Stop Flu in Its Tracks 
  Flu Shot Facts -- Battling Influenza 
  Mandatory Flu Shots for Preschoolers Cause Outrage 
 <URL> = eal-cause-of-influenza-epidemics.aspx 

