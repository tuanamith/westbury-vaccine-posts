


Black Invention Myths 
Smallpox Vaccine  Onesimus the slave in 1721? No! Onesimus knew of variolation, an early inoculation technique practiced in several areas of the world before the discovery of vaccination.  English physician Edward Jenner developed the smallpox vaccine in 1796 after finding that the relatively innocuous cowpox virus built immunity against the deadly smallpox. This discovery led to the eventual eradication of endemic smallpox throughout the world. Vaccination differs from the primitive inoculation method known as variolation, which involved the deliberate planting of live smallpox into a healthy person in hopes of inducing a mild form of the disease that would provide immunity from further infection. Variolation not only was risky to the patient but, more importantly, failed to prevent smallpox from spreading. Known in Asia by 1000 AD, the practice reached the West via more than one channel. 

