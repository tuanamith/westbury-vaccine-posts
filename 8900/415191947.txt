


On Aug 22, 8:11=A0am, "JC" < <EMAILADDRESS> > wrote: - <EMAILADDRESS> ... 

But it is truthful nonetheless.   Look at nature as an example.   Of the millions of baby turtles (alligators, birds, monkeys....you name it)  that are born, most become food for other creatures.   Only a small percentage survive to adulthood.   Same with so many other species including ours.   It is just not possible to love and raise all the unwanted babies in the world. 
I am a great animal lover.   I have cats and dogs, and they are our "babies" now that we are older.   Right now the shelters are overflowing with cats and dogs, good, well behaved, blameless family pets that people no longer can afford to feed and vet etc.   If only I could go down to the pound and rescue ALL of them, give them ALL good and loving homes, prevent them from being euthanized..... but it is NOT POSSIBLE!   I must turn a blind eye, knowing that for just a few animals, those I can comfortably care for, a better life has been assured. 
Those who try to rescue them all end up being "collectors" .....people who live in a tiny house with 80 cats and 30 dogs or something similar.   It is just not possible to care for that many animals properly.   They always find someone like that living in feces up to their ankles, with dead and sick animals in every corner!  The animals are confiscated and treated at great public expense, the house gets condemned, and ultimately most of the animals get euthanized anyway. 
Years ago abortion was not legal.   They gathered up all the unwanted children off the streets (where most of them ended up living) and put them into orphans homes, which were total hell holes.   They were deprived of love, personal attention of any kind, they barely had enough to eat, and as soon as they were able, were sent off to live and work on some farm to provide hard manual labor to pay for their keep.   I wouldn't call that a very satisfactory existence.   A life unwanted, can be a very sad thing.   Where are the right to lifers then?   Probably picketing somewhere. 
So our grandparents in years gone by founded orphans homes to deal with all the unwanted children.   Do you really think it is a good way?     Then think of the mothers of those children.   Undoubtedly they suffered as well.   Do you ever think any solution for unwanted children could ever be right, fair or good?    Think of all the kids in the foster care system right now.   All the stories of child abuse and the shuffling around till they finally "time out" at the age of 18, thrown out onto the streets to find a way to survive. 
Is it any wonder that people seek an early way to end that kind of unwanted situation? 
This is what is meant when buddhists try to explain that they are NOT for abortion, but in some circumstances it may be called for as the most compassionate path, even the least harmful one of all the options available. 
It is my deeply considered opinion that those who are so adamantly pro- life, should get off their duffs, roll up their sleeves, and DO SOMETHING about the unwanted, unloved, hungry children of this world.   Aborted fetuses may actually be better off than to have been born to join the vast numbers of hungry and uncared for children that are starving to death in so many countries all over the world. 
As for myself, I put my efforts into the Rotary Club.   They are doing things actively all over the world to provide clean water, medicines, vaccines, surgery to correct congenital deformities, (and so much more I cannot begin to list it all here).   The only difference is that these are LIVING, already BORN children. 
I double dog dare you to do something real about them!!!!!   Throw down your picket signs and roll up your sleeves! 
Evelyn 






