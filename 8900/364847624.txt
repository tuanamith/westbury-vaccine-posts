


" <EMAILADDRESS> " < <EMAILADDRESS> > wrote in  <NEWSURL> :  


Ah, yes, dust. When I decided to get back into modelling, I had already  packed all my remaining models surviving from previous decades. I took  the opportunity, after moving to our present location, to sort through  them. A few unbuilts, but most of the builts were in boxes with the dust  they had gathered when last on display. (As a matter of fact, more of my  models met their demise from my mother's attempt at dusting them with a  feather duster than from any other activity, such as nephews or  firecrackers.) I picked out a few to refurbish (Revell 1/72 Me-262;  Revell/Monogram (don't remember which) 1/48 Bf-109E, Monogram 1/48 P- 51D), and, after making sure I had all the fiddly bits, I used a soft- bristle toothbrush and Ivory soap to clean them off, rinsed with tap  water. I did lose some decals, but I had intended to strip off the  paint, anyway, so no big loss. 
When I get my own house (not this rental place), I'll get a glassed-in  display case. Well, I can dream... 
--  "Evolution can be mean -- there's no 'dumb-ass' vaccine." -- Jimmy  Buffett 

