


Job Title:     Payroll Tax Analyst Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-05-09 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Payroll Tax Analyst &#150; HUM001247 
Job Description  
Submit 
Description 
Position supports the overall workflow, control, analysis and reporting related to the end-to-end processing of the payroll operations tax area. This includes performing the duties related to reconciling the payroll tax transmissions to Treasury, garnishments, ensuring tax payments are completed as required by law, reconciling quarterly and year-end tax data as well as fully supporting the W2 analysis, reconciliation and year-end process. * Supports the daily payroll tax functions within the Shared Business Services organization. * Provides data analysis and problem resolution support to customers. * Performs data integrity validation by monitoring and resolving issues associated with periodic audits. * Participates in the testing of new and modified applications and tools. * Participates in and incorporates approved updates to internal procedural Education: An Associates or Bachelors degree preferred and related experience to carry out essential functions of the job is required. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.                           To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #HUM001247. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means.  
Profile   Locations  US-NJ-Rahway   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  05/07/2008, 11:47 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  05/12/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-397697 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



