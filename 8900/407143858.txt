




How Effective Is Male Circumcision at Preventing Sexually Transmitted Infections? By Kate Bourne, RH Reality Check Posted on July 23, 2008, Printed on July 31, 2008  <URL> / 

Adult male circumcision is being pushed as the latest magic bullet for the HIV pandemic. There is good reason for the enthusiasm about a new use for the world's oldest surgical intervention. But let's be clear about what circumcision will and will not offer a man and his partner or partners. 

Circumcision programs have captured the attention and funding of governments in East and Southern Africa, global funders, and policymakers. Job postings for "male circumcision specialists" are circulating, and the Bill and Melinda Gates Foundation and the President's Plan for AIDS Relief (PEPFAR), two of the largest funders of global HIV/AIDS programs, have incorporated male circumcision work into their efforts. 

First, the good news: Three recent trials have shown that circumcised men are about half as likely to contract HIV from unprotected vaginal intercourse as their uncircumcised counterparts. Circumcision also protects against some other sexually transmitted infections (STIs). 

But the reality is that circumcision offers only partial protection. A circumcised man still has a significant risk of contracting HIV and other STIs if he engages in unprotected sex with an infected partner. Circumcision does not offer the man's current female partners any protection from contracting HIV. 

On a larger scale, the predictive models that show a significant reduction in the number of new HIV infections assume that between that 80 to 100 percent of men are circumcised. Currently approximately 30 percent of men worldwide are circumcised, although this varies widely between different communities and countries. 

Surveys have shown that many men are willing to be circumcised, and the promise of surgical prevention may bring throngs of men into local clinics that do not routinely use health services. It would be shortsighted not to couple circumcision services with education on HIV prevention and safe sex, provider-initiated HIV counseling and testing, and referrals to HIV/AIDS care and treatment. 

But given the protection that circumcision provides, men may assume and assert that they are "safe" and insist on having sex without condoms. There are questions about whether newly circumcised men will be willing to abstain from sex for the six weeks necessary for the wound to heal so as to not possibly increase their or their partner(s) risk of contracting HIV or other STIs. 

In the communities where the demand for circumcision is high and resources are scarce, unqualified circumcisers may begin offering the surgery to meet men's demands. It would take only a very few unqualified safe surgeons to taint the safety and acceptability of these programs. We cannot allow this to happen. 

Clearly, circumcision is an imperfect solution to HIV prevention. Even so, it promises to be one of the most effective strategies we currently have to curb new HIV infections in men. 

So, what about those of us who never had a foreskin? 

By the most optimistic predictions, male circumcision will not translate into fewer HIV infections in women for decades. The reality of HIV and the epidemic is that women account for the majority of people living with HIV in the most affected region, Sub-Saharan Africa. For biological reasons, a woman is between two to eight times more likely than a man to contract HIV during vaginal intercourse with an HIV-positive partner. 

On top of women's biologic predilection to infection, gender inequalities have sustained and feminized the epidemic by limiting women's abilities to negotiate safe sex, refuse unwanted sex, and ask their partners to be monogamous. We must be cautious about how male circumcision may change these realities. 

Too often women are blamed for bringing HIV into the household. They are cast out of their homes, subjected to discrimination and violence, and shunned by their communities. Will circumcised men be more likely to blame their female partner(s) if either of them becomes HIV- positive? As fewer men contract HIV, will it increasingly be seen as a women's disease and become more stigmatized? Will women living with HIV become more vulnerable to violence and abandonment than they already are? 

The Way Forward 

Male circumcision offers new hope that more men can remain HIV- negative, and this advance is good news for men and potentially good news for women. But it would be inexcusable if we allow a potentially positive advance in HIV prevention for men to harm women. 

Women's health advocates, including those who met at the Expert Consultation on Male Circumcision and HIV Prevention: Implications for Women, are recommending that investments in male circumcision be matched by concurrent investments that directly benefit women. Among these investments is sexuality education, which teaches young women and men how to communicate with their partners about safe sex, to establish equality in relationships, and to respect the right to consent in sex and marriage. And, in this age of rising HIV infections, it teaches young people how to protect themselves and each other. 

Sexuality education takes us part of the way by equipping women with the skills to negotiate safe sex, but we must also ensure women have access to male and, in particular, female condoms. As the only existing woman-initiated method of HIV prevention, female condoms offer women a way to protect themselves when faced with a circumcised partner who refuses to use a male condom. Current supplies of male and female condoms do not satisfy the global demand. But the female condom shortage is particularly dire: there are 700 male condoms for every female condom. Protecting women in a climate of male-focused prevention means we must make a meaningful commitment to promoting and widely distributing the female condom. 

Although the main beneficiaries of male circumcision will be men in the near and medium term, we must not lose sight of the fact that women are stakeholders in these programs. Before these programs ramp up, women's health groups, including women living with HIV, should be involved in the analysis of how circumcision will affect women. Once the programs kick off, they should be closely monitored and evaluated in collaboration with these women's health groups. We must also initiate new research into what biomedical, structural, and behavioral interventions can best help women protect themselves from HIV infection such as microbicides. 

Perhaps the most important thing we can do before the snipping begins is to communicate honestly about what male circumcision will, and won't, do for a man and his female partner(s). We should undertake public education campaigns and individual and couples counseling to ensure that women and men understand that: 




Circumcision does not completely protect men from HIV. Circumcised men should continue using condoms to protect themselves and their partner(s) from infection. 

Having sex with a circumcised man does not protect his female partners from HIV. Partners of circumcised men should continue to insist on safe sex. 


Men must abstain from sex for a full six weeks after circumcision to protect themselves and their partners. 

A man who is already circumcised is not 
necessarily HIV-negative, and if he is, there is no guarantee he will remain so. 


Male circumcision is not a vaccine, and it is not a cure-all. It is simply one of the best ways to prevent HIV infections in men, right now. But we cannot overlook the fact that circumcised men, and their partners, are still vulnerable to HIV. 

=A9 2008 RH Reality Check All rights reserved. View this story online at:  <URL> / 


How Effective Is Male Circumcision at Preventing Sexually Transmitted Infections? By Kate Bourne, RH Reality Check Posted on July 23, 2008, Printed on July 31, 2008  <URL> / Adult male circumcision is being pushed as the latest magic bullet for the HIV pandemic. There is good reason for the enthusiasm about a new use for the world's oldest surgical intervention. But let's be clear about what circumcision will and will not offer a man and his partner or partners. 









So, what about those of us who never had a foreskin? 




The Way Forward 












Men must abstain from sex for a full six weeks after circumcision to protect themselves and their partners. 

A man who is already circumcised is not 
necessarily HIV-negative, and if he is, there is no guarantee he will remain so. 



=A9 2008 RH Reality Check All rights reserved. View this story online at:  <URL> / 

