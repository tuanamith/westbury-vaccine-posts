


Job Title:     Senior Manufacturing Supervisor/Manufacturing Supervisor Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-12-07 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Manufacturing Supervisor/Manufacturing Supervisor &#150; PRO007176 
Job Description  
Submit 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # PRO007176. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
 Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  12/05/2008, 01:04 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/14/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-430536 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



