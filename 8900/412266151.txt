


Job Title:     Senior Investigator Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-08-16 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Investigator &#150; CLI001881 
Job Description  
Submit 
Individual will support a histology/immunohistochemistry (IHC)group in the analysis of tissue-based biomarkers in human clinical studies for Clinical Pharmacology, Experimental Medicine, and Clinical Oncology. Successful candidate will be involved in providing direction for development and validation of histology and IHC assays; reviewing tissue slides to identify presence of tumor cells; and reading slides to determine staining intensity and percent cell positivity. Responsibilites will also include presentation of data, liaison work with various stakeholders, and setting up and maintaining a cadre of contract pathologists who would be available to provide input in their respective areas of expertise. 
Qualifications 
Required-. Experience in development and fit-for-purpose validation of histological, immunohistochemical, and in situ hybridization assays as readout of drug pharmacodynamics or for use in patient stratification. Clinical expertise in reading tumor biopsy slides. Familiarity with methods for quantitative IHC/FISH image analysis including experience with analytical software. Strong interest in implementing new technologies for multiplexing, quantitation, and digital imaging technologies. Strong communication skills and people management experience. Desired- experience working in a regulated laboratory (eg. Good Laboratory Practices), experience with Design of Experiment methodologies, and experience with assay development/validation in non-IHC assays (eg. immune-based, enzyme, mass spec-based assays). Education requirement- M.D.in anatomic pathology Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # CLI001881. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  08/12/2008, 12:08 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  09/11/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-418016 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



