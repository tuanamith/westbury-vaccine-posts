


Job Title:     Senior Annual Reviewer Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-04-10 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Annual Reviewer &#150; QUA001651 
Job Description  
Submit 
Description 
The Senior Annual Reviewer position includes the oversight of Annual Product Review generation to ensure consistent content and timely issuance. This is in addition to the preparation and follow-up of the Annual Product Review. The Annual Review is a The Senior Annual Reviewer: * Manages the Annual Product Review templates assists with implementing process enhancements and initiatives * Ensures consistency within and between Annual Product Review manufacturing operations, quality performance, and market performance * Ensures any abnormalities or trends identified from the analysis are investigated by the appropriate departments * Follows-up on any questions, comments or requests for further investigation from Senior Management review of the final Three or more years experience in Manufacturing, Quality, or Technology organization is also required, as are strong attention to detail, a business/technical writing foundation, and familiarity with MS Excel, MS Word and MS Access.  It is desirable to have familiarity with IMPACT, DocCompliance, and/or LIMS. You should also be able to comprehend technical and/or analytical data and draw accurate conclusions and/or summarize key information. The ability to understand and utilize fundamental statistics is a strong plus. The position requires the ability to handle multiple tasks simultaneously and coordinate information with several departments. Must have strong verbal and written communication skills as well as proven ability to lead team efforts. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. 
We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001651. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  04/07/2008, 02:55 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/14/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-391576 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



