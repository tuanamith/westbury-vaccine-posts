


Ran across the following today. Sounds promising..... 
Fred 




USC team develops promising vaccine against prostate cancer 

03/07/08 

USC researchers have developed a prostate cancer vaccine that prevented the development of cancer in 90 percent of young mice genetically predestined to develop the disease. In the Feb.1 issue of Cancer Research, they suggest the same strategy might work for men with rising levels of PSA (prostate specific antigen), a potential diagnostic indicator of prostate cancer. "By early vaccination, we have basically given these mice life-long protection against a disease they were destined to have," said the study's lead investigator, W. Martin Kast, a professor of Molecular Microbiology and Immunology at the USC/Norris Comprehensive Cancer Center and the Keck School of Medicine of USC. "This has never been done before and, with further research, could represent a paradigm shift in the management of human prostate cancer." 
Now, men with rising PSA levels but no other signs of cancer are advised "watchful waiting" - no treatment until signs of the cancer appear, Kast said. "But what if instead of a watchful wait, we vaccinate? That could change the course of the disease." 
The study findings also represent a new way to think about the use of therapeutic prostate cancer vaccines, Kast said. Vaccines now in testing are designed to treat men whose cancers are advanced and unresponsive to therapy, and results have offered limited clinical benefit, he said. This novel approach targets the precancerous state with the aim of preventing cancer from developing, he said. 
The Kast team's preventive vaccine is designed to mount an immune response against prostate stem cell antigen (PSCA), the protein target of some therapeutic vaccines under development. PSCA, a membrane protein, is over-expressed in about one-third of early-stage prostate cancers, but expression ramps up in all prostate tumors as they grow and advance. PSCA is also expressed at low-levels in normal prostate gland tissue as well as in the bladder, colon, kidney and stomach. 
The researchers created a prime-boost vaccination scheme using two kinds of vaccines and tested it in 8-week-old mice that were genetically altered to develop prostate cancer later in life. The first vaccine simply delivered a fragment of DNA that coded for PSCA, thus producing an influx of PSCA protein to alert the immune system. The booster shot, given two weeks later, used a modified horse virus to deliver the PSCA gene. 
"Confronting the immune system in two different ways forces it to mount a strong response," Kast said. 
In the experimental group, two of 20 mice developed prostate cancer at the end of one year, but all control mice had died of the disease. Researchers found that mice in the experimental group had all developed very small tumors that did not progress. "There were tiny nodules of prostate cancer in the mice that were surrounded by an army of immune system cells," Kast said. "The vaccination turned the cancer into a chronic, manageable disease." 
The vaccination strategy also works with other antigens, Kast said. The researchers recently tried another prostate cancer membrane target and found that after 1.5 years, 65 percent of experimental mice were still alive, and of those that died, the suspected cause was old age. Crucially, investigators further found that treated mice did not develop autoimmune disease, a side effect that could develop if the vaccine had also targeted PSCA expression in normal cells. 
"Theoretically, the vaccine could produce a response in any tissue that expresses the antigen, but the fact that PSCA is expressed in such low levels in normal tissue may prevent that complication," he said. 
Still, studies in humans are needed to ensure autoimmunity does not develop, Kast said. 
"We feel this is a very promising approach," he said. "With just two shots, the vaccine will prime immune cells to be on the lookout for any cell that over-expresses PSCA." 
The study was funded by a pre-doctoral training grant from the National Institutes of Health and a grant from the Margaret E. Early Medical Research Trust. USC co-authors include Maria de la Luz Garcia- Hernandez, Andrew Gray and Otto J. Klinger as well as Bolyn Hubby from AlphaVax, Inc., of Research Triangle Park, North Carolina. 




