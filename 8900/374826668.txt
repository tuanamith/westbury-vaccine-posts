


Job Title:     Co-operative Assignment 2008- Viral Vaccine Engineering... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-05-09 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-operative Assignment 2008- Viral Vaccine Engineering Services (Sept-March) &#150; CHE001318 
Job Description  
Submit 
Qualifications This position requires applicants to be pursuing degree in Biomaterial &amp; Tissue Engineering, Biomedical, Biochemical, Chemical, Mechanical Engineer, Manufacturing Process or related field. A Grade Point Average (GPA) of at least 3.0 is required. Candidate must be available for full time employment (40 hours per week) for a period of 6 months targeted to begin in September 2008. Currently enrolled in an academic program and returning to school following this assignment. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition # CHE001318. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular  



Please refer to Job code merck-398236 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



