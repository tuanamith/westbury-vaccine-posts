


- 
Virus Kills 22 Children in China 
By ANDREW JACOBS [New York Times] 
BEIJING [China] =97 A fast-spreading viral outbreak in eastern China has killed 22 children, sickened nearly 3,600 others and caused panic among parents in an impoverished corner of Anhui Province, government health officials said Friday [May 2, 2008]. 
All of the fatalities have been in children younger than 6, the majority of them under 2. 
The outbreak, caused by an intestinal virus, has been spreading in the city of Fuyang [China] since early March but local health officials only announced the outbreak this week, raising questions about whether they were trying to conceal it. 
In recent days, the Chinese media has heavily criticized the government response, offering comparisons to the SARS epidemic of 2003, which drew widespread attention to China=92s shaky public health system and official attempts to cover up the outbreak. The official Xinhua news agency published the latest figures on Friday [May 2, 2008]. 
On Thursday [May 1, 2008], the World Health Organization warned that the disease, which thrives in warm weather and passes easily between children, could spread in the coming summer months. It advised child- care centers and schools in the city and surrounding region to stay closed until the spread of new infections was curtailed. 
The virus, commonly know as hand, foot and mouth disease, has no relation to the foot-and-mouth disease that infects livestock. 
The illness begins with a fever and often leads to mouth ulcers and to blisters on the hands, feet and buttocks. There is no vaccine or cure but most patients recover in a week without treatment. In severe cases, however, brain swelling can lead to paralysis or death. Rigorous hygiene dramatically reduces the spread of the pathogen. 
=2E.. 
 <URL> = login 




