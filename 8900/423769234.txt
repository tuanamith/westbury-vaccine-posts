


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Manufacturing Engineer/Manufacturing Engineer &#150; ENG001740 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Under the supervision of a Senior Engineer or Manager, is responsible for assisting in demonstrating process improvements, monitoring process performance, and troubleshooting. Provides day-to-day technical support for assigned area which may include vaccine bulk manufacturing, filling, lyophilization, packaging, and/or mechanical support. Actively ensures quality, safety, environmental, and operating cost goals are achieved. Completes projects and prepares reports while adhering to Good Manufacturing Practices, safety, and environmental regulations. * Reports to and receives supervision from a Senior Engineer or Manager. On routine process operations, is guided by standard procedures. Confers with immediate supervisor in resolving atypical events. * Provides supervisory coverage when required. Assist in the training of hourly employees. Promotes open communications and team work in work area. * Maintain systems to monitor process performance. Assist in troubleshooting process control difficulties related to safety, quality, environmental compliance, and cost control. * Carries out projects that may involve facilities, equipment, or process based on specific guidance from supervisor. Actively supports productivity improvement program and resolution of atypical events. * Maintain a high level of knowledge and may coordinate safety, Good Manufacturing Practices, and environmental compliance programs. Assist in schedule of audits for the department. * Summarizes project progress, delays, or additional opportunities in reports. Frequently communicates with immediate supervisor about status of projects or assignments. Please note: This position is evening (2nd) shift, Wednesday - Sunday 
Qualifications B.S. or B.A. degree in Science, Engineering, or Business or equivalent experience. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # ENG001740. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  10/06/2008, 02:23 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  10/20/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-424016 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



