


X-No-Archive: AIDS vaccine focus shifts after disappointments (Reuters) 
By Andrew Quinn Sun Oct 12, 8:17 AM ET 


ADVERTISEMENT 
'A REAL SWING BACK' 
"There's no guarantee that basic researchers are going to come up with the answers," Morris said. 
It will also give scientists a chance to delve more deeply into the results of the failed Merck vaccine trial. 
Morris said this year's disappointments could not be allowed to derail the pace of research. 

