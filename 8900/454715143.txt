


Spend $68 billion and lay-off workers cause you didn't make money??? I guess I should took some business classes. 
Drugmaker Pfizer To Buy Wyeth, Cut Jobs Pfizer Will Buy Rival In $68 Billion Deal 
NEW YORK -- Pfizer Inc. is buying rival drugmaker Wyeth in a $68 billion cash-and-stock deal that will increase its revenue by 50 percent, solidfy its No. 1 rank in the troubled industry and transform it from a pure pharmaceutical company into a broadly diversified health care giant. 
At the same time, Pfizer announced cost cuts that include slashing more than 8,000 jobs. 
The deal comes as Pfizer's profit takes a brutal hit from a $2.3 billion legal settlement over allegations it marketed certain products for indications they have not been approved. The New York-based company is also cutting 10 percent of its work force of 83,400, slashing its dividend, and reducing the number of manufacturing sites. 
Early Monday, Pfizer, the maker of cholesterol treatment Lipitor, the world's top-selling drug, and impotence pill Viagra, said it will pay $50.19 per share under for Wyeth, valuing Madison, N.J.-based Wyeth at a 14.7 percent premium to the company's closing price of $43.74 Friday. 
Both companies' boards of directors approved the deal but Wyeth shareholders must do so, antitrust regulators must review the deal and a consortium of banks lending the companies $22.5 billion must complete the financing. 
Pfizer has been under pressure from Wall Street to make a bold move as it faces what is referred to as a patent cliff in the coming years. As key drugs lose patent protection they will face generic competition and declining sales. Lipitor is expected to face generic competition starting in November 2011. It brings in nearly $13 billion per year for the company. 
Acquiring Wyeth helps Pfizer diversify and become less-dependent on individual drugs -- Lipitor now provides about one-fourth of all Pfizer revenue -- while adding strength in biotech drugs, vaccines and consumer products. Wyeth makes the world's top-selling vaccines, Prevnar for meningitis and pneumococcal disease, and co-markets with Amgen Inc. the world's No. 1 biotech drug, Enbrel for rheumatoid arthritis. 
"The combination of Pfizer and Wyeth provides a powerful opportunity to transform our industry," Pfizer Chairman and Chief Executive Jeffery B. Kindler said in a statement. "It will produce the world's premier biopharmaceutical company whose distinct blend of diversification, flexibility, and scale positions it for success in a dynamic global health care environment." 
Together, and the two companies will have 17 different products with annual sales of $1 billion or more, including top antidepressant Effexor, Lyrica for fibromyalgia and nerve pain, Detrol for overactive bladder and blood pressure drug Norvasc. 
Shortly after announcing the Wyeth deal, Pfizer said fourth-quarter profit plunged on a charge to settle investigations into off-label marketing practices. The company earned $268 million, or 4 cents per share, compared with profit of $2.72 billion, or 40 cents per share, a year prior. Revenue fell 4 percent to $12.35 billion from $12.87 billion. 
Excluding about $2.3 billion in legal charges, the company says profit rose to 65 cents per share. 
Analysts polled by Thomson Reuters expected profit of 59 cents per share on revenue of $12.54 billion. 
Looking ahead, New York-based Pfizer expects earnings per share between $1.85 and $1.95 in 2009, below forecasts for $2.49. 

