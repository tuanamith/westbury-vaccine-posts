


Job Title:     SAP - Team Leader-SCM Job Location:  NJ: Whitehouse Station Pay Rate:      Open Job Length:    full time Start Date:    2008-04-18 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   SAP - Team Leader-SCM &#150; INF003696 
Job Description  
Submit 
Description 
This challenging opportunity will support our Global ERP program-COMET. The program&#8217;s scope includes: Order to Cash (OTC), Supply Chain Management (SCM), Source to Settle (STS), and Finance Business Management (FBM) and Human Capital Management/Human Resources (HCM/HR). Having completed global business blueprint activities, the team is currently focusing on the solution design. Merck will deploy this solution to its worldwide businesses. This role will report to the Executive Director, ERP COE, and/or Project Manager. The incumbent will be responsible for the translation of new business requirements into technical design of the global SAP template. In this role you will support the design and configuration of the SCM solution set, execute testing and write technical In this role, you will communicate and coordinate with business management, project executives and team leads to insure appropriate integration of processes and modules across organization. The incumbent must have hands on configuration experience coupled w/ technical knowledge in the SCM solution. Areas of expertise may include: APO (Advanced Planning Optimizer), DP, SNP, PP/DS, SD, FI/CO . Incumbent will be responsible for the planning, directing, reviewing and reporting activities and work products of the perspective functional area (i.e., Finance &amp; Accounting, Procurement &amp; Accounts Payable, Sales &amp; Distribution, Inventory Management, or Warehouse Management).  Responsibilities to include but not limited to: Communicate and collaborate with the functional project manager, and/or division heads of assigned area, in an effort to support the business needs of the company. Review and approve work of assigned staff and guides the collaboration of business analysts, divisional super-users and area department heads. Ensure that assigned work is planned and coordinated across functional areas with other functional area leads and that assigned consultants are working efficiently. Review and approve resolution to system deviations. Review and approve supporting Work closely with training lead to review and approve curricula, courses and lessons and reviews and provides input to proposed training schedule. 
Qualifications 
BA/BS required. SAP knowledge is required and must be highly focused, possessing in-depth knowledge of the capabilities within SCM. SAP Certification within assigned area is a plus. Minimum 7 years experience in business and/or functional departments is required with a minimum of 5 years of SAP implementation experience from design to test is required. Must be willing to travel up to 25% during project.  COMMUNICATIONS: Strong oral, written and presentation skills needed. PROBLEM-SOLVING: Must have the ability to analyze complex business processes of all kinds, including multi-divisional. Solutions require fact finding, creativity, and potentially multiple recommendations. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #INF003696. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-NJ-Whitehouse Station     US-NJ-Lebanon   Employee Status  Regular  
Additional Information   Posting Date  04/16/2008, 10:50 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/30/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-393516 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



