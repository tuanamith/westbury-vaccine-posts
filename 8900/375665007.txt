


The Christian Science Monitor   May 12, 7:34 AM EDT Families will make case for vaccine link to autism  By KEVIN FREKING Associated Press Writer 
WASHINGTON (AP) -- The Institute of Medicine said in 2004  there was no credible evidence to show that vaccines  containing the preservative thimerosal led to autism in  children. But thousands of families have a different take  based on personal experience. 
Some of them are going to court Monday as attorneys will  attempt to show that the mercury-based preservative triggers  symptoms of autism. 
Two 10-year-old boys from Portland, Ore., will serve as test  cases to determine whether many of the children and their  families should be compensated. Attorneys for the boys will  attempt to show the boys were happy, healthy and developing  normally - but, after being exposed to vaccines with  thimerosal, they began to regress. 
Thimerosal has been removed in recent years from standard  childhood vaccines, except flu vaccines that are not packaged  in single-doses. The CDC says single-dose flu shots currently  are available only in limited quantities. In 2004, a committee  with the Institute of Medicine concluded there was no credible  evidence that vaccines containing thimerosal caused autism. 
Overall, nearly 4,900 families have filed claims with the U.S.  Court of Claims alleging that vaccines caused autism and other  neurological problems in their children. Lawyers for the  families are presenting three different theories of how  vaccines caused autism. 
The Office of Special Masters of the claims court has  instructed the plaintiffs to designate three test cases for  each of the three theories - nine cases in all - and has  assigned three special masters to handle the cases. Three  cases in the first category were heard last year, but no  decisions have been reached. 
The two cases beginning Monday are among the three that focus  on the second theory of causation: that thimerosal-containing  vaccines alone cause autism. The plaintiff in the third case  originally scheduled for hearing this month has withdrawn and  lawyers and court officials are working to agree on substitute  case. 
Hearings in the test cases for the third theory of causation  are scheduled in mid-September. 
Lawyers for the petitioning families in the cases being heard  this month say they will present evidence that injections with  thimerosal deposit a form of mercury in the brain. That  mercury excites certain brain cells that stay chronically  activated trying to get rid of the intrusion. 
"In some kids, there's enough of it that it sets off this  chronic neuroinflammatory pattern that can lead to regressive  autism," said attorney Mike Williams. 
In the end, the families' attorneys hope to convince the  special master hearing their case that thimerosal belongs on  the list of causes for the inflammation that leads to  regressive autism. 
To win, the attorneys for the two boys, William Mead and  Jordan King, will have to show that it's more likely than not  that the vaccine actually caused the injury. 
Many members of the medical community are skeptical of the  families' claims. They worry that the claims about the dangers  of vaccines could cause some people to forgo vaccines that  prevent illness. 
"I think that what's so endearing to me about the anti-vaccine  people, is they're perfectly willing to go from one hypothesis  to the next without a backward glance," said Dr. Paul Offit,  director of the Vaccine Education Center at the Children's  Hospital of Philadelphia. 
Autism is a developmental disability that typically appears  during the first three years of life and affects a person's  ability to communicate and interact with others. Dr. Andrew  Gerber, a psychiatrist, said that medical experts don't have a  comprehensive understanding of what causes autism, but they do  know there is a strong hereditary component. 
Toxins from the environment could play a role, but currently,  data does not support that they do, Gerber said. 
Arguments are scheduled to go on throughout the month. A final  decision could take several more months. Claims that are  successful would result in compensation taking into account  lost earnings after age 18 and up to $250,000 for pain and  suffering. 
The families or the federal government can also appeal the  decision of the special master to the Court of Federal Claims  or to a federal appeals court. 
The court Web site says more than 12,500 claims have been  filed since creation of the program in 1987, including more  than 5,300 autism cases, and more than $1.7 billion has been  paid in claims. It says there is now more than $2.7 billion in  a trust fund supported by an excise tax on each dose of  vaccine covered by the program. 
---- 
On the Net: 
Background on thimerosal trial:   <URL>  
 <URL> ? SITE=MABOC&SECTION=HOME&TEMPLATE=DEFAULT&CTIME=2008-05-12-07- 34-57 --  A government, of, by, and, for: Rich, Elite, Freemasons. But all things that are reproved are made manifest by the  light:  for whatsoever doth make manifest is light. The light shineth in darkness;  and the darkness comprehended it not. The light of the body is the eye: if therefore thine eye be  single,  thy whole body shall be full of light.  But if thine eye be evil, thy whole body shall be full of  darkness.  If therefore the light that is in thee be darkness, how great  is that darkness! Awake thou that sleepest, and arise from the dead,  and Christ shall give thee light. For my yoke is easy, and my burden is light. 

