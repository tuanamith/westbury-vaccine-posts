


Job Title:     Laboratory Supervisor Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-04-16 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Laboratory Supervisor &#150; QUA001653 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The Laboratory Supervisor reports to and receives general technical and administrative guidance from the Lead Laboratory Supervisor, Laboratory Area Head, or Laboratory Manager and works as a functional group member to meet objectives within the group. Responsibilities include, but are not limited to: * The activities involved in conducting tests and/or assaying of raw material, intermediates or finished products and is knowledgeable in the analytical techniques of their area of responsibility. Technologies include any which may be employed in the disciplines of Biochemistry, Chemistry, Microbiology, Virology or other Biological Sciences.  * Supervision of hourly employees performing testing and support activities, including scheduling of work assignments based on priorities, being knowledgeable of cGMP, administration of local agreement, company personnel policies, OSHA, Company safety procedures, along with the promotion of team work and open communications. * Updating SOPs, Process Specific Training Modules, Control procedures, etc. * Training personnel to ensure employees are competent and qualified to perform duties, while complying with departmental policies and procedures. * Critically evaluating data generated and recommend acceptance or rejection of samples, and performing lab work accurately and in a timely fashion. * Maintaining records, developing productivity improvement plans, maintaining adequate inventory of supplies, training records, tracking functions such as CAFUs, atypical reports, special projects, etc. * Providing technical assistance and cooperating with MMD support departments as required to resolve technical problems. 
Qualifications In order to qualify for this position, you must possess a B.S/B.A. or M.S in Virology, Microbiology, Biology, Biochemistry with a minimum of 2 years of relevant laboratory, manufacturing or quality control experience. Good verbal and written communication skills with the ability to work in a team setting; good interpersonal and supervisory skills; and demonstrated ability or aptitude for continuous learning and analytical problem solving. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # QUA001653. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  04/14/2008, 04:00 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/21/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-393096 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



