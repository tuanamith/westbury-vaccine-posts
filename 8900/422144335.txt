


Job Title:     Project Development Engineer - Pharmaceutical... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-09-28 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Project Development Engineer - Pharmaceutical Commercialization Technology &#150; ENG001735 
Job Description  
Submit 


Qualifications 
 BS of MS Degree in Chemical Engineering or Materials Engineering/Science with a minimum of 4 years experience in process research and development of technical operations support. Strong Chemical Engineering skills with a desire to perform hands-on work including design and execution of experiments at various scales (lab/ pilot plant etc) is also required. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site at www.merck.com/careers to create a profile and submit your resume for requisition # ENG001735. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  09/26/2008, 11:10 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  10/03/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-423496 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



