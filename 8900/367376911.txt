


UNICEF Press Centre "Change the World With Children"  <URL>  
Press Centre Business-like approach to funding health programs in poor countries may save more than two million lives in 5 years NEW YORK, 1 February - Two years after its official launch at the World Economic Forum annual meeting in Davos, Switzerland, the Global Alliance for Vaccines and Immunization, or GAVI, reports that its goal- oriented approach to development aid could raise basic immunization rates in funded countries by 17 percentage points and increase coverage of hepatitis B vaccine from 18 to 65 percent by 2007, ultimately saving more than two million lives, according to new data released at the World Economic Forum today. 
# # # # 
For further information, contact: 
Lisa Jacobs +41 79 447 1935 
Heidi Larson, UNICEF New York, e-mail  <EMAILADDRESS>  1 212 326 7762 or +1 646 207 5179 




