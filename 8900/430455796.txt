


Job Title:     Staff Chemist / Research Chemist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-10-25 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Staff Chemist / Research Chemist &#150; CHE001526 
Job Description  
Submit 
We are seeking an individual with experience in the field of high throughput mass spectrometry. The successful candidate will join a dynamic team to provide high throughput assay services in support of Basic Research lead discovery and optimization efforts. The incumbent will be responsible for the day-to-day operation and maintenance of high throughput MS (HTMS) instruments, as well as the generation of high quality quantitative data. A current application involves the use of quantitative HTMS data in support of P450 inhibition studies for MRL Global Counterscreening. The ideal candidate will have strong knowledge and expertise of bio-analytical methods for detecting and measuring small molecules using hyphenated mass spectrometry techniques. The position includes: .      Sample preparation and coordination of time sensitive analyses .      Operation, maintenance, and troubleshooting of mass spec instrumentation (Applied BioSystems API4000) .      Use of automated liquid handling workstations and robotic autosamplers (BioTrove RapidFire, Caliper Twister) .      Generation of dose response titration curves and IC50 calculations 
Qualifications Required .      Background in Analytical Chemistry and quantitative analysis .     Experience with solid phase extraction .      Hands-on expertise with LC/MS, preferably with API4000 Desired .      Experience in laboratory automation and high-throughput screening .      Ability to perform routine work with high degree of accuracy and precision .      High-level of self-motivation and ability to thrive in a goal-oriented setting .      Excellent organizational, written and oral communication skills with experience working in a multi-disciplinary team environment Education Requirement .      Bachelors or Masters degree in chemistry or related field with at least 2 years of relevant experience 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # CHE001526. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm RepresentativesPlease Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  10/23/2008, 01:56 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/22/2008, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-425656 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



