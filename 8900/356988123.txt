


Donna/Michael:  I realize you probably don't have this as your intent, but your "piled on" incredulity makes me feel very defensive...and I'm not sure why.  I'm sure it's just my reactions. 
I think I have some fundamental disagreements with you two about Passions, and it probably starts from the fact that I had ZERO bias for or against it from the outset, AND I appreciate ANY AND ALL experiments with the soap form.  I'd tolerate a porn soap.  I'd tolerate an animated soap. Whatever...I care about the genre. 
But there are two premises you've both advanced, separately or together, with which I do not agree. 
1.  Passions initially wanted the full audience (say 18-49)...and only settled for 12 year old girls and gay males later...when it turned out that's all who was watching. 
I don't believe it.  ON THE DEBUT EPISODE there was a TALKING DOLL and a WICKED WITCH.  There was Princess Diana's friend, being hunted down in Paris. 
Uh uh.  They were going for a tongue-in-cheek, wink, broad style from the beginning.  There was no intent to capture a broad based audience...I simply don't BELIEVE that with the storylines that--from the outset--Shirl would call *outrageously far fetched*. 
The show was HIGH CAMP from the get-go, and that is calculated to capture a niche audience. 
2.  Passions was "dumbed down". 
Again, Passions was NOT my taste.  But I think the words for it are "silly", "campy", "lampoon". 
From the past 1-2 years, here are some examples: 
- Julian is in bed, in costume, with Esme suspended above him on a trapeze - Fox and Miguel, shirtless of course, stare each other down, in close proximity, wearing (as I recall) glittery short shorts and cupid costumes.  Both have a "John Black style" squint. - The hermaphrodite pregnant by the spawn of its' father 
THOSE ARE WINKS.  They are written NOT from the perspective of "what would low-IQ viewers wish to watch", but--more Melrose Place style--"what would high intellect viewers understand to be parody".  I think of Passions more like an Andy Samberg SNL skit...his "D*ck in a Box" with Justin Timberlake took the music video form and made fun of it.  Weird Al does this too.  That's Passions. 
I do not consider that dislike of the genre.  I consider that affection...enough love to have fun with it. 
-- 
I'm not trying to convince you, so it's fine with me if we leave this as "agree to disagree".  But the heaping on of this outraged incredulity..."you can't be serious"..."you really can't mean that"...."are you for real" (not words you necessarily wrote, but what I'm left with as a lingering feeling)...makes me feel really uncomfortable :-). 
I believe there is merit in almost anything.  I believe EVEN Passions had merit.  I believe that, concretely, daytime goes from nine television soaps to eight, and I DO NOT think that is a good thing. 
If I try a new vaccine on a series of lab rats, and one of my nine lab rats dies because of it, the experiment does not bode well for the future of my vaccine.  I respect EACH AND EVERY experiment, because I want the genre to succeed.  (Plus, I like that people are still trying to find the rescue formula). 
This may be dispositional.  Heck, I even liked LML for her first year :-).  EVERYONE gets the benefit of the doubt with me :-). 

