


On Oct 10, 12:21=A0pm, Ilena Rose < <EMAILADDRESS> > wrote: 

---------------------------------------------------------------------------= ----- 
Grades available: Reagent, technical. 



---------------------------------------------------------------------------= ----- 
There are over 7,000 references on adverse effects of alumium. See lists of sources of aluminum in diet at middle of page 

---------------------------------------------------------------------------= ----- 
Aluminum neurotoxicity in preterm infants receiving intravenous- feeding solutions. 
Bishop N.J. =96 Morley R. =96 Day J.P. =96 Lucas A. 
From:   N Engl J Med (1997 May 29) 336(22):1557-61 

---------------------------------------------------------------------------= ----- 
Aluminum-containing emboli in infants treated with extracorporeal membrane oxygenation. 
Vogler C. =96 Sotelo-Avila C. =96 Lagunoff D. =96 Braun P. =96 Schreifels J= .A. =96 Weber T. 
From:   N Engl J Med (1988 Jul 14) 319(2):75-9 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 
aluminum in parenteral solutions revisited =97 again. Klein G.L. From: Am J Clin Nutr (1995 Mar) 61(3):449-56 

---------------------------------------------------------------------------= ----- 
Aluminum-induced anemia. From:   Am J Kidney Dis (1985 Nov) 6(5): 348-52 
Arch Dermatol (1984 Oct) 120(10):1318-22 

---------------------------------------------------------------------------= ----- 
Persistent subcutaneous nodules in patients hyposensitized with aluminum-containing allergen extracts. 
Garcia-Patos V. =96 Pujol R.M. =96 Alomar A. =96 Cistero A. =96 Curell R. = =96 Fernandez-Figueras M.T. =96 de Moragas J.M. 
From:   Arch Dermatol (1995 Dec) 131(12):1421-4 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 
Postvaccinal sarcomas in the cat: epidemiology and electron probe microanalytical identification of aluminum. 
Hendrick M.J., Goldschmidt M.H.. Shofer F.S. Wang Y.Y. Somlyo A.P. From: Cancer Res (1992 Oct 1) 52(19):5391-4 

---------------------------------------------------------------------------= ----- 
Aspects of aluminum toxicity. Hewitt C.D. =96 Savory J. =96 Wills M.R From:   Clin Lab Med (1990 Jun) 10(2):403-22 
. 

---------------------------------------------------------------------------= ----- 
Soft tissue sarcoma associated with aluminum oxide ceramic total hip arthroplasty. A case report. 
Ryu R.K. =96 Bovill E.G. Jr =96 Skinner H.B. =96 Murray W.R. From:   Clin Orthop (1987 Mar)(216):207-12 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 
Sawchuk W.S. =96 Friedman K.J. =96 Manning T. =96 Pinnell S.R. From:   J Am Acad Dermatol (1986 Nov) 15(5 Pt 1):982-9 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 
From:   J Pharmacol Exp Ther (1985 Jun) 233(3):715-21 

---------------------------------------------------------------------------= ----- 
Aluminum toxicity and albumin. Kelly A.T. =96 Short B.L. =96 Rains T.C. =96 May J.C. =96 Progar J.J. 
From:   ASAIO Trans (1989 Jul-Sep) 35(3):674-6 

---------------------------------------------------------------------------= ----- 
The role of aluminium for adverse reactions and immunogenicity of diphtheria-tetanus booster vaccine. 
Mark A. =96 Granstrom M.From:   Acta Paediatr (1994 Feb) 83(2):159-63 


Potrpoom palsy? Neurologic disorder in three aluminum smelter workers. 
Heyer N.J. 
From:   Arch Intern Med (1985 Nov) 145(11):1972-5 

---------------------------------------------------------------------------= ----- 
Reducing aluminum: an occupation possibly associated with bladder cancer 
Theriault G. =96 De Guire L. =96 Cordier S. From:   Can Med Assoc J (1981) 124(4):419-422,425 

---------------------------------------------------------------------------= ----- 
From:   Acta Neuropathol (Berl) (1991) 82(5):346-52 

---------------------------------------------------------------------------= ----- 
Neurotoxic effects of aluminium on embryonic chick brain cultures. From:   Acta Neuropathol (Berl) (1994) 88(4):359-66 

---------------------------------------------------------------------------= ----- 
Aluminium in tooth pastes and Alzheimer's disease. Verbeeck R.M. =96 Driessens F.C. =96 Rotgans J. 
From:   Acta Stomatol Belg (1990 Jun) 87(2):141-4 
The role of aluminium from tooth pastes may be even more important than that from the drinking water. 

---------------------------------------------------------------------------= ----- 
Persistent subcutaneous nodules in children hyposensitized with aluminium-containing allergen extracts. 

---------------------------------------------------------------------------= ----- 
Contact sensitivity to aluminium in a patient hyposensitized with aluminium precipitated grass pollen. 
lemmensen O. =96 Knudsen H.E. From:   Contact Dermatitis (1980 Aug) 6(5): 305-8 
Standard patch testing of a patient with eczema revealed positive reactions to the aluminium discs used for testing. 

---------------------------------------------------------------------------= ----- 
Behavioural effects of gestational exposure to aluminium. 
Rankin J. =96 Sedowofia K. =96 Clayton R. =96 Manning A. From:   Ann Ist Super Sanita (1993) 29(1):147-52 

---------------------------------------------------------------------------= ----- 
The absence of extracellular calcium potentiates the killing of cultured hepatocytes by aluminum maltolate. 
Snyder J.W. =96 Serroni A. =96 Savory J. =96 Farber J.L. From:   Arch Biochem Biophys (1995 Jan 10) 316(1):434-42 

---------------------------------------------------------------------------= ----- 
Sensitization to aluminium by aluminium-precipitated dust and pollen extracts. 

---------------------------------------------------------------------------= ----- 
Allergy to non-toxoid constituents of vaccines and implications for patch testing. 
Cox N.H. =96 Moss C. =96 Forsyth A. From:   Contact Dermatitis (1988 Mar) 18(3):143-6 

---------------------------------------------------------------------------= ----- 
Aluminium allergy in patients hyposensitized with aluminium- precipitated antigen extracts. 

---------------------------------------------------------------------------= ----- 
Aluminium allergy. Veien N.K. =96 Hattel T. =96 Justesen O. =96 Norholm A. 
From:   Contact Dermatitis (1986 Nov) 15(5):295-7 

---------------------------------------------------------------------------= ----- 
Vaccination granulomas and aluminium allergy: course and prognostic factors. 
Kaaber K. =96 Nielsen A.O. =96 Veien N.K. From:   Contact Dermatitis (1992 May) 26(5):304-6 

---------------------------------------------------------------------------= ----- 
From:   Arch Environ Contam Toxicol (1989 Jan-Apr) 18(1-2):233-42 

---------------------------------------------------------------------------= ----- 
H Differentiated neuroblastoma cells are more susceptible to aluminium toxicity than developing cells. 
E. Meiri From:   Arch Toxicol (1989) 63(3):231-7 

---------------------------------------------------------------------------= ----- 
Reversal of an aluminum-induced behavioral deficit by administration of deferoxamine. 
Connor D.J. =96 Harrell L.E. =96 Jope R.S. From:   Behav Neurosci (1989 Aug) 103(4):779-83 

---------------------------------------------------------------------------= ----- 
From:   Behav Neurosci (1988 Oct) 102(5):615-20 


                 Estimates of dietary exposure to aluminium. Pennington J.A. =96 Schoen S.A. 
From:   Food Addit Contam (1995 Jan-Feb) 12(1):119-28 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 
Aluminum, a neurotoxin which affects diverse metabolic reactions. Joshi J.G. 
From:   Biofactors (1990 Jul) 2(3):163-9 

---------------------------------------------------------------------------= ----- 
Distribution of aluminum in different brain regions and body organs of rat. 
Vasishta R.K. =96 Gill K.D. From:   Biol Trace Elem Res (1996 May) 52(2): 181-92 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 
Aluminium release from glass ionomer cements during early water exposure in vitro. 
Andersson O.H. =96 Dahl J.E. From:   Biomaterials (1994 Sep) 15(11): 882-8 

---------------------------------------------------------------------------= ----- 
Impaired control of information transfer at an isolated synapse treated by aluminum: is it related to dementia? 
Banin E. =96 Meiri H. From:   Brain Res (1987 Oct 13) 423(1-2):359-63 

---------------------------------------------------------------------------= ----- 
Chronic aluminum-induced motor neuron degeneration: clinical, neuropathological and molecular biological aspects. 
Strong M.J. =96 Garruto R.M. From:   Can J Neurol Sci (1991 Aug) 18(3 Suppl):428-31 

---------------------------------------------------------------------------= ----- 
Some commonly unrecognized manifestations of metabolic arthropathies. 
Cobby M.J. =96 Martel W. From:   Clin Imaging (1992 Jan-Mar) 16(1):1-14 

---------------------------------------------------------------------------= ----- 

---------------------------------------------------------------------------= ----- 


Transverse fractures of the spinous process of the 7th cervical vertebra in RDT patients: an Al related disease? 
From:   Int J Artif Organs (1987 Mar) 10(2):93-6 

---------------------------------------------------------------------------= ----- 
Risk of aluminum accumulation in patients with burns and ways to reduce it. 
Klein G.L. =96 Herndon D.N. =96 Rutan T.C. =96 Barnett J.R. =96 Miller N.L.=  =96 Alfrey A.C. 
From:   J Burn Care Rehabil (1994 Jul-Aug) 15(4):354-8 

---------------------------------------------------------------------------= ----- 
Aluminum concentrations in tissues of rats: effect of soft drink packaging. 
Kandiah J. =96 Kies C. 
From:   Biometals (1994 Jan) 7(1):57-60 
Environmental Effects of Aluminum 
---------------------------------------------------------------------------= ----- 
CT Aluminum in acidic surface waters: chemistry, transport, and effects. 
From:   Environ Health Perspect (1985 Nov) 63:93-104 

---------------------------------------------------------------------------= ----- 


A mechanism for acute aluminum toxicity in fish 
Exley C. =96 Chappell J.S. =96 Birchall J.D. 
From:   J Theor Biol (1991 Aug 7) 151(3):417-28 

---------------------------------------------------------------------------= ----- 
Can the mechanisms of aluminum neurotoxicity be integrated into a unified scheme? 
Strong M.J. =96 Garruto R.M. =96 Joshi J.G. =96 Mundy W.R. =96 Shafer T.J. 
From:   J Toxicol Environ Health (1996 Aug 30) 48(6):599-613 

---------------------------------------------------------------------------= ----- 
Aluminum toxicity following intravesical alum irrigation for hemorrhagic cystitis. 
Kanwar V.S. =96 Jenkins J.J. 3rd =96 Mandrell B.N. =96 Furman W.L. 
From:   Med Pediatr Oncol (1996 Jul) 27(1):64-7 

---------------------------------------------------------------------------= ----- 
Progressing encephalomyelopathy with muscular atrophy, induced by aluminum powder. 
Bugiani O. =96 Ghetti B. 
From:   Neurobiol Aging (1982 Fall) 3(3):209-22 

---------------------------------------------------------------------------= ----- 
Aluminium foil as a wound dressing 
Poole M.D. =96 Kalus A.M. =96 von Domarus H. 
From:   Br J Plast Surg (1979 Apr) 32(2):145-6 

---------------------------------------------------------------------------= ----- 



