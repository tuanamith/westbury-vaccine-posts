


Job Title:     Senior Chemical Engineer Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-12-06 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Chemical Engineer &#150; ENG001818 
Job Description  
Submit 
Qualifications 
Senior Chemical Engineer The position requires either a BS and/or MS in Chemical Engineering or Chemistry from an accredited college/university plus at least 5 years of relevant work experience in process development, or a PhD in Chemical Engineering or Chemistry from an accredited college/university. Relevant work experience may include: * Experience with scale-up and technology transfer, moving from laboratory to pilot plant and production scale. * Demonstrated capability to develop and execute an experimental program to address issues of process robustness, productivity, and cost, integrating efforts with specialists in other technical disciplines. * Demonstrated proficiency in balancing resource requirements within projects and among multiple assigned projects. * Familiarity with concepts and application of cGMP during pharmaceutical development and production. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # ENG001455. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  12/04/2008, 02:49 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  01/03/2009, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-431856 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



