


EIGHT DEATHS LINKED TO LABOUR'S NEW SEX JAB FOR SCHOOLGIRLS 
 <URL>  
EIGHT deaths have been linked to the cervical cancer jab which will be given  to every 12-year-old girl in Britain under Government plans announced last  week. 
Oct 28, 2007 
 <URL>  
Doctors suspect the jab, which protects against a sexually transmitted human  papilloma virus that causes the cancer, may be implicated in 3,461 adverse  reactions, including paralysis and seizures. 
Last week Health Secretary Alan Johnson revealed plans to vaccinate all  girls aged between 12 and 13 to cut Britain's death rate from the disease.  He said: "Prevention is better than cure and this vaccine will prevent many  women from catching the virus in the first place." 
However, reports from the US, where the Gardasil vaccine has been used for  nearly a year in some states, reveal that eight victims died soon after  receiving it. The victims, aged from as young as 11 to 22, suffered blood  clots or heart attacks. 
 Thousands of side effects have also been linked, many serious. These  include 15 cases of paralysis and 239 cases of temporary loss of  consciousness. Other suspected reactions include convulsions and numbness. 
In one case Jessica Vega, 14, from Gardnerville, California, became  paralysed from the knees down after a booster shot of Gardasil in May. 
Jessica, who has two older brothers and enjoys horse-riding and football,  had been sitting in a school lesson and started feeling weak. 

She could barely walk to the car by the end of the day and late that  afternoon she was rushed to hospital. "I was getting weak, it felt weird to  walk and to open the car door," she said. 

Although the cause has not been officially determined, her paralysis is a  known and rare side effect of the vaccine and doctors have indicated this  may be the cause. 

In June, Shannon Nelson, 18, an athlete and artist from Chicago, received  Gardasil with vaccines against meningitis and chickenpox. Within days she  developed tingling, numbness and muscle weakness. 
 Her symptoms worsened and weeks later she could barely walk or raise her  arms. She became paralysed on July 5. The paralysis lasted for two months  and she is still recovering. "I couldn't sit up in bed, I could not lift up  my arms," she said. 
 Of 42 women who received the vaccine while pregnant, 18 have been reported  to have experienced side effects ranging from spontaneous abortion to foetal  abnormalities. 
 The reports, mostly made by doctors and other health professionals,  were  logged with the US Vaccine Adverse Events Reporting System, co-sponsored by  the powerful Food and Drug Administration (FDA). Back in Britain, Jackie  Fletcher, of Warrington, is the founder of Jabs, a support group for parents  who believe their children have been damaged by vaccines. 
 She said: "We're talking about a new vaccine. It has been used in the  States and already there are reports of deaths and serious reactions. 
"There are too many uncertainties. The vaccine should not be used until all  these reports have been properly investigated and it's been shown to be safe  over long-term trials." 
 She said a safer alternative would be to give smear tests, which pick up  early signs of cervical cancer. Dr John Oakley, a west Midlands GP, said the trials for Gardasil had been so  limited that the children taking it would be like "guinea-pigs". 
Gardasil, which costs £300 a dose, was approved for use in the US in June  2006 and since then has been in widespread use throughout America. 
 Tom Fitton, head of US health lobby group Judicial Watch, said: "The  adverse event reports on the HPV vaccine reads like a catalogue of horrors.  Any state or local government now beset by lobbying campaigns to mandate  this vaccine for young girls ought to take a look at these adverse health  reports. It looks as if an unproven vaccine with dangerous side effects is  being pushed as a miracle drug." 
Cervical cancer is the second most common cancer in women in Britain,  killing more than 1,120 every year. The UK vaccine programme is scheduled to  begin next September for all girls up to the age of 18. 
"We need to make the NHS a service that prevents ill-health and prioritises  keeping people well," said Mr Johnson. 
A spokeswoman for the Department of Health said long-term safety trials had  shown Gardasil was safe. 

She said: "Given that this vaccine will save the lives of around 400 women  each year and, in the absence of any scientific evidence that points to  safety concerns, it would be irresponsible to raise inappropriate public  fears over vaccine safety." 
 Nicholas Kitchin, medical director of Sanofi Pasteur MSD, which  manufactures the vaccine, said: "There is no good evidence to show the  events were linked with the vaccine. "Some of the reports are not even  valid. Over 13 million doses have been distributed worldwide and extensive  monitoring has shown it to be safe." 
He said just because someone suffered a symptom after a vaccine, it did not  necessarily mean it was to blame. Prelicence trials and monitoring had shown  the vaccine to be "relatively safe" with side effects that were mostly "mild  and self-limiting". 



