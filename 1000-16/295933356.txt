


Is it Safe? 
A new vaccine can protect against HPV 
By TANYA ENBERG 
In a diary entry written when I was nine, I jotted down the names of friends I'd hoped would be in my class the upcoming school year. 
In another, I griped about my brother "hogging" the remote control. The words, "He never lets me watch what I want!" scream off the page. 
What isn't present in this day-to-day tracking of a child's life is mention of visiting the doctor for an HPV vaccination. 
Heck, at age nine, I'd never even heard of HPV (human papillomavirus) or any other STD, for that matter. 
STDs were so alien in my world of Easy Bake Ovens and Girl Scout outings, I hadn't the remotest inkling about what they entailed. 
But back-to-school time isn't what it used to be, kids. 
Until recently, HPV was barely a whisper in the widespread public consciousness. Now, it's become big news with talk of HPV literally spreading everywhere. 
It hit airwaves with the friendly campaign slogan, "Tell Someone," and is suddenly rolling off the tongues of politicians, health care workers, drug companies and parents, who are not only busy stocking up on school supplies for their youngsters but asking, 'Should my daughter get the HPV vaccine?' 
The vaccine in question is called Gardasil, manufactured by Merck Frosst Canada Ltd. 
Recently approved, Gardasil protects against four strains of HPV, including types 16 and 18, linked to 70% of cervical cancer cases, which kills about 400 Canadian women annually. 
In March, Ottawa committed $300 million to help fund a national HPV vaccination program targeting females from age nine to 26. 
Ontario will provide more than 80,000 Grade 8 girls with free Gardasil shots. Not a bad deal, considering a three-shot vaccine costs about $400. 
Also starting the voluntary vaccination program are Nova Scotia, Newfoundland and P.E.I. All other provinces plan to follow in 2008. 
NOT EVERYONE'S THRILLED 
Not everyone is thrilled about how quickly Gardasil was approved, or how hurriedly the government started pushing the shots. 
The longest follow-up data spans just five years -- a blip in the radar of an average lifetime. Also consider that the trials were largely conducted by the drug makers. 
The prominent women's health website Women to Women issued a release warning against the vaccination hype. 
"This new vaccine has not been studied in sufficient depth or over sufficient time to ascertain its long-term safety," Marcy Holmes, a practitioner at Women to Women said. 
Gardasil climbed to higher controversial peaks when a Maclean's cover story appeared this month bearing the headline, "Our Girls Aren't Guinea Pigs." 
The five-page piece called into question the safety of Gardasil and suggested that Canadian girls are being used as experimental subjects in the national vaccination plan. 
Needless to say, the article ruffled feathers, namely those of the Public Health Agency of Canada, which fired back in a letter. 
"The suggestion that public health officials would support a vaccine that would put the health, or worse, the lives, of girls and women at risk, is irresponsible," reads an excerpt from the rebuttal. 
But who can blame anyone for posing questions yet to be answered? 
Before encouraging thousands of girls to line up like cattle for a round of shots, shouldn't we all be asking, "What are the potential long-term effects?" 
It's irresponsible not to. 
An article in this month's Canadian Medical Association Journal, by a group of health researchers led by epidemiologist Abby Lippman of McGill University, calls the immunization program premature. 
"I think people should be asking questions before they get vaccinations or their daughters vaccinated," Lippman said in a phone interview. 
The authors note that it's unclear how long the vaccine lasts, its effectiveness when administered with other vaccinations, and whether booster shots will be required. 
There are, she insists, too many unknowns. 
"Why the rush?" she asks. 
"Why this September and not next September?" 
It's a good question. 
--- 
GET THE FACTS 
HPV is the leading cause of cervical cancer. 
- Getting pap exams is imperative for detecting HPV. 
- The virus is contracted through skin-to-skin contact. 
- There are numerous types of HPV. Many are considered "low risk" and are not associated with cancer. 
- An estimated 75% of Canadians will have an HPV infection in their lifetime. 
- HPV can manifest as genital warts but is known as the silent infection because it often carries no symptoms at all. 

 <URL>  


