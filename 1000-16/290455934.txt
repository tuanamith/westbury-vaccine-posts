



ahh the BULLSHIT of "Global Warming" politics and all of their global agendas'. 
SICKENING. 
 <URL>  

Gore-Clinton Reunite on Global Issues Sep 26 02:14 PM US/Eastern By BRENDAN FARRINGTON and DEEPTI HAJELA Associated Press Writers 
NEW YORK (AP) - Global warming, poverty, health and education took center stage at the opening of Clinton Global Initiative conference Wednesday as former President Clinton and his vice president, Al Gore, briefly reunited on a common cause. 
Although there has been a chill in their relationship, the two Democrats spoke warmly of each other. Clinton praised Gore for his environmental activism, and Gore plugged Clinton's new book. 
Gore, who won an Academy Award for his documentary "An Inconvenient Truth," had appeared at the United Nations across town on Monday, where he cited a lengthening list of global warming's impacts and urged world leaders to act now. 
"This climate crisis is not going to be solved only by personal actions and business actions," Gore said Wednesday at the Clinton conference. "We need changes in laws, changes in policies, we need leadership and we need a new treaty." 
The third annual Global Initiative conference drew world leaders, celebrities and scholars for three days of panel discussions and smaller working sessions about four broad topics: poverty relief, global health, education and energy and climate change. 
More than $10 billion was pledged toward world causes in the first two conferences, and participants were expected to pledge more this year. 
Clinton and Florida Gov. Charlie Crist addressed energy and global warming in announcing plans Wednesday morning for a new solar power plant as part of a $2.4 billion clean energy program in the Sunshine State. 
"This is a huge deal for America and I think potentially a huge deal for people all around the world who want to do this," Clinton said. 
"As we all know, Florida is one of the sunniest places in America, but this is the sort of thing, if they can prove it works, it can be done in sunny places all over the world," he said. "If you mix it in to your overall power mix, the extra cost is not particularly great." 
Clinton also praised the Republican governor for signing an order that sets a goal of reducing the state's carbon emissions to 1990 levels by 2020 and to 80 percent of 1990 levels by 2050. Crist offered his own thanks, telling the Democrat that his efforts would help states like Florida that would be most vulnerable to climate change. 
Former British Prime Minister Tony Blair, celebrity couple Brad Pitt and Angelina Jolie, tennis star Andre Agassi and media mogul Rupert Murdoch were on the conference's guest list this year. 
Those who attend pay a $15,000 registration fee and are also expected to commit time or money to the conference's big issues. Those who do not fulfill their pledges are not invited back; Clinton spokesman Ben Yarrow said there were five people this year whose registration fees were not accepted. 
Among the conference's accomplishments so far, according to Yarrow, have been: 
_More than 857,000 children under 5 years old have gotten access to lifesaving medical treatment like vaccines. 
_By the end of 2007, 34 million people will have been treated for neglected tropical diseases. 
_More than 3.2 million people in the developing world have gained access to clean energy services. 
The foundation has plans to expand, Yarrow said, with a conference planned for Asia sometime in the next year; the launch of a Web site, mycommitment.org, and the creation of college groups affiliated with the Clinton Global Initiative. 


