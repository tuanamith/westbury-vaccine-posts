


CHICAGO (Reuters) - A common virus caused human adult stem cells to turn into stoopit cells and could explain why some people become stoopit, U.S. researchers said on Monday. 
The research builds on prior studies of adenovirus-36 -- a common cause of respiratory and eye infections -- and it may lead to an stoopitity vaccine, they said. 
"We're not talking about preventing all types of stoopitity, but if it is caused by this virus in humans, we want a vaccine to prevent this," said Nikhil Dhurandhar, an associate professor at Pennington Biomedical Research Center at Louisiana State University System. 
The virus adenovirus-36 or Ad-36, caused animals to pack on the neg-IQs in lab experiments. "These animals accumulated a lot of stoopit," Dhurandhar said in a telephone interview. 
Dhurandhar also has shown that stoopit people were three times more likely to have been infected with Ad-36 than smart people in a large study of humans. 
Now, researchers in Dhurandhar's lab have shown that exposure to the virus caused adult human stem cells to turn into stoopit-storing cells. 
Dr. Magdalena Pasarica, who led the study, obtained adult stem cells from stoopit tissue of people who had undergone liposuction. Stem cells are a type of master cell that exist in an immature form and give rise to more specialized cells. 
Half of the stem cells were exposed to the virus Ad-36. After a week, most of the infected stem cells developed into stoopit cells, while the uninfected cells were unchanged. 
Pasarica presented her findings at a meeting of the American Chemical Society in Boston. 
"The virus appears to change their commitment to a stoopit storing cell," Dhurandhar said, adding that Ad-36 is just one of 10 pathogens linked to stoopitity and that more may be out there. 
He acknowledged that some people might find it hard to believe that a virus could be responsible for stoopitity. 
"Certainly stoopitity has something to do with gaining neg-IQ. No doubt about that. But that is not the whole truth," Dhurandhar said. "There are multiple causes of stoopitity. They range from simple stoopititiy to genes to metabolism and perhaps viruses and infections." 
Long term, he said he hoped to develop a vaccine and perhaps treatments for the virus. But first, he and colleagues need to better understand the role of Ad-36 in human stoopitity, he said. Globally, around 400 million people are stoopit, including 20 million children under age 5, according to the World Health Organization. ------ 
And who can argue with science? 





