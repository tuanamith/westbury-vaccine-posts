


ColeahSpeak: 
So what is the big scary point?  
~~~~~~~~~~~~~~~~~ 
 <URL>  
Grades available: Reagent, technical.  
------------------------------------------------------------------ 

Basal PTH when elevated appears to protect bone and thereby favor CNS toxicity.  
Other factors favoring one form of toxicity over another are not well understood.  
  
Distilled Water Placed in Various Containers     


-------------------------------------------------------------------------------- 

From:   N Engl J Med (1997 May 29) 336(22):1557-61  


-------------------------------------------------------------------------------- 

From:   N Engl J Med (1988 Jul 14) 319(2):75-9  


-------------------------------------------------------------------------------- 

From:   Pediatrics (1992 May) 89(5 Pt 1):877-81  


-------------------------------------------------------------------------------- 

Aluminum in parenteral solutions revisited  again.  Klein G.L.  
From:   Am J Clin Nutr (1995 Mar) 61(3):449-56  


-------------------------------------------------------------------------------- 

Aluminum-induced anemia.  From:   Am J Kidney Dis (1985 Nov) 6(5):348-52  
Arch Dermatol (1984 Oct) 120(10):1318-22  


-------------------------------------------------------------------------------- 

From:   Arch Dermatol (1995 Dec) 131(12):1421-4  


-------------------------------------------------------------------------------- 

Trace metals and degenerative diseases of the skeleton.  Savory J.  Bertholf R.L.  Wills M.R.  
From:   Acta Pharmacol Toxicol (Copenh) (1986) 59 Suppl 7:282-8  

From:   Cancer Res (1992 Oct 1) 52(19):5391-4  


-------------------------------------------------------------------------------- 

Aspects of aluminum toxicity.  Hewitt C.D.  Savory J.  Wills M.R.  
From:   Clin Lab Med (1990 Jun) 10(2):403-22  


-------------------------------------------------------------------------------- 

From:   Clin Orthop (1987 Mar)(216):207-12  


-------------------------------------------------------------------------------- 

Aluminum-induced granulomas in a tattoo.  McFadden N.  Lyberg T.  Hensten-Pettersen A.  
From:   J Am Acad Dermatol (1989 May) 20(5 Pt 2):903-8  


-------------------------------------------------------------------------------- 

From:   J Am Acad Dermatol (1986 Nov) 15(5 Pt 1):982-9  


-------------------------------------------------------------------------------- 

Aluminium and injection site reactions.  Culora G.A.  Ramsay A.D.  Theaker J.M.  
From:   J Clin Pathol (1996 Oct) 49(10):844-7  


-------------------------------------------------------------------------------- 

From:   J Pharmacol Exp Ther (1985 Jun) 233(3):715-21  


-------------------------------------------------------------------------------- 

Aluminum toxicity and albumin.  Kelly A.T.  Short B.L.  Rains T.C.  May J.C.  Progar J.J.  
From:   ASAIO Trans (1989 Jul-Sep) 35(3):674-6  


-------------------------------------------------------------------------------- 

From:   Acta Paediatr (1994 Feb) 83(2):159-63  


-------------------------------------------------------------------------------- 

Potroom palsy? Neurologic disorder in three aluminum smelter workers.  Heyer N.J.  
From:   Arch Intern Med (1985 Nov) 145(11):1972-5  


-------------------------------------------------------------------------------- 

Reducing aluminum: an occupation possibly associated with bladder cancer  Theriault G.  De Guire L.  Cordier S.  
From:   Can Med Assoc J (1981) 124(4):419-422,425  


-------------------------------------------------------------------------------- 

From:   Acta Neuropathol (Berl) (1991) 82(5):346-52  


-------------------------------------------------------------------------------- 

Neurotoxic effects of aluminium on embryonic chick brain cultures.  From:   Acta Neuropathol (Berl) (1994) 88(4):359-66  


-------------------------------------------------------------------------------- 

Aluminium in tooth pastes and Alzheimer's disease.  Verbeeck R.M.  Driessens F.C.  Rotgans J.  
From:   Acta Stomatol Belg (1990 Jun) 87(2):141-4  
The role of aluminium from tooth pastes may be even more important than that from the drinking water.  


-------------------------------------------------------------------------------- 

From:   Allergy (1985 Jul) 40(5):368-72  


-------------------------------------------------------------------------------- 

From:   Contact Dermatitis (1980 Aug) 6(5):305-8  
Standard patch testing of a patient with eczema revealed positive reactions to the aluminium discs used for testing.  


-------------------------------------------------------------------------------- 

Behavioural effects of gestational exposure to aluminium.  Rankin J.  Sedowofia K.  Clayton R.  Manning A.  
From:   Ann Ist Super Sanita (1993) 29(1):147-52  


-------------------------------------------------------------------------------- 

From:   Arch Biochem Biophys (1995 Jan 10) 316(1):434-42  


-------------------------------------------------------------------------------- 

From:   Contact Dermatitis (1988 Jul) 19(1):58-60  


-------------------------------------------------------------------------------- 

Allergy to non-toxoid constituents of vaccines and implications for patch testing.  Cox N.H.  Moss C.  Forsyth A.  
From:   Contact Dermatitis (1988 Mar) 18(3):143-6  


-------------------------------------------------------------------------------- 



-------------------------------------------------------------------------------- 

Aluminium allergy.  Veien N.K.  Hattel T.  Justesen O.  Norholm A.  
From:   Contact Dermatitis (1986 Nov) 15(5):295-7  


-------------------------------------------------------------------------------- 

Vaccination granulomas and aluminium allergy: course and prognostic factors.  Kaaber K.  Nielsen A.O.  Veien N.K.  
From:   Contact Dermatitis (1992 May) 26(5):304-6  


-------------------------------------------------------------------------------- 

From:   Arch Environ Contam Toxicol (1989 Jan-Apr) 18(1-2):233-42  


-------------------------------------------------------------------------------- 

H Differentiated neuroblastoma cells are more susceptible to aluminium toxicity than developing cells.  E. Meiri  
From:   Arch Toxicol (1989) 63(3):231-7  


-------------------------------------------------------------------------------- 

From:   Behav Neurosci (1989 Aug) 103(4):779-83  


-------------------------------------------------------------------------------- 

From:   Behav Neurosci (1988 Oct) 102(5):615-20  


-------------------------------------------------------------------------------- 

Aluminum, a neurotoxin which affects diverse metabolic reactions.  Joshi J.G.  
From:   Biofactors (1990 Jul) 2(3):163-9  


-------------------------------------------------------------------------------- 

Distribution of aluminum in different brain regions and body organs of rat.  Vasishta R.K.  Gill K.D.  
From:   Biol Trace Elem Res (1996 May) 52(2):181-92  


-------------------------------------------------------------------------------- 

From:   Biomaterials (1996 Oct) 17(20):1949-54  


-------------------------------------------------------------------------------- 

Aluminium release from glass ionomer cements during early water exposure in vitro.  Andersson O.H.  Dahl J.E.  
From:   Biomaterials (1994 Sep) 15(11):882-8  


-------------------------------------------------------------------------------- 

From:   Brain Res (1987 Oct 13) 423(1-2):359-63  


-------------------------------------------------------------------------------- 

From:   Can J Neurol Sci (1991 Aug) 18(3 Suppl):428-31  


-------------------------------------------------------------------------------- 

Some commonly unrecognized manifestations of metabolic arthropathies.  Cobby M.J.  Martel W.  
From:   Clin Imaging (1992 Jan-Mar) 16(1):1-14  


-------------------------------------------------------------------------------- 

From:   Clin Nephrol (1988 Jul) 30(1):48-51  


-------------------------------------------------------------------------------- 

Estimates of dietary exposure to aluminium.  Pennington J.A.  Schoen S.A.  
From:   Food Addit Contam (1995 Jan-Feb) 12(1):119-28  


-------------------------------------------------------------------------------- 



-------------------------------------------------------------------------------- 

From:   J Burn Care Rehabil (1994 Jul-Aug) 15(4):354-8  


-------------------------------------------------------------------------------- 

Aluminum concentrations in tissues of rats: effect of soft drink packaging.  Kandiah J.  Kies C.  
From:   Biometals (1994 Jan) 7(1):57-60  


Environmental Effects of Aluminum 

-------------------------------------------------------------------------------- 



-------------------------------------------------------------------------------- 

From:   J Exp Zool (1992 Jun 1) 262(3):247-54  


-------------------------------------------------------------------------------- 

A mechanism for acute aluminium toxicity in fish  Exley C.  Chappell J.S.  Birchall J.D.  
From:   J Theor Biol (1991 Aug 7) 151(3):417-28  


-------------------------------------------------------------------------------- 

From:   J Toxicol Environ Health (1996 Aug 30) 48(6):599-613  
Institutional address:  
Department of Clinical Neurological Sciences  
University of Western Ontario  
London, Canada.  
 <EMAILADDRESS>   


-------------------------------------------------------------------------------- 

From:   Med Pediatr Oncol (1996 Jul) 27(1):64-7  


-------------------------------------------------------------------------------- 

Progressing encephalomyelopathy with muscular atrophy, induced by aluminum powder.  Bugiani O.  Ghetti B.  
From:   Neurobiol Aging (1982 Fall) 3(3):209-22  


-------------------------------------------------------------------------------- 

Aluminium foil as a wound dressing  Poole M.D.  Kalus A.M.  von Domarus H.  
From:   Br J Plast Surg (1979 Apr) 32(2):145-6  
ISBN: 0007-1226  


-------------------------------------------------------------------------------- 


Diseases Associated with Aluminium Intoxication  H. Tomlinson, M.B., Ch.B., MRCS., LRCP  

-------------------------------------------------------------------------------- 

    

