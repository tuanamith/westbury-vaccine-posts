


Job Title:     Staff Scientist/Scientist Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-10-31 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Staff Scientist/Scientist &#150; ENG001445 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The Scientist or Staff Scientist will work independently and in cooperation with MMD and MRL colleagues to provide s, process descriptions, atypical process reports, and change requests. *  Development of good working relationship within MMD manufacturing. *  Providing regulatory and internal inspection support. *  Responsible for project related support for business process improvements, resolving compliance and regulatory related issues with respect to the change request business process. 
Qualifications * B.S./M.S. degree in appropriate engineering/scientific or related field. * Good interpersonal skills including flexibility and ability to work in a team environment. Demonstrated analytical abilities. * Good written and verbal communication skills. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # ENG001445. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  10/28/2007, 04:12 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  10/31/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-349516 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



