


Michael Richardson: Human infections get harder to treat 5:00AM Monday September 17, 2007 By Michael Richardson The outbreak of highly contagious influenza among horses in Australia is a reminder that the world faces an unprecedented risk from disease. 
Shortly before the outbreak, the first in an island continent that imposes stringent bio-security measures, the World Health Organisation warned that infectious diseases were emerging more quickly and spreading faster around the globe than ever. 
The WHO, an agency of the United Nations, said in its annual report that population growth, settlement of previously uninhabited areas, rapid urbanisation, intensive farming practices, environmental degradation and the misuse of medical drugs had disrupted the largely unseen world of microbes. As a result, nearly 40 diseases exist today that were unknown barely a generation ago. Many have the potential to infect humans as well as animals. 
Meanwhile, the WHO says that human infections are becoming harder to treat as mainstay antimicrobial drugs fail to counter target diseases faster than replacement drugs are being developed. 
AdvertisementIn a world drawn ever closer by trade and communications, and with billions of people moving from country to country every year (more than two billion by air alone), an epidemic in one region may be only a few hours from becoming an imminent threat somewhere else. 
This does not appear to be a risk in the case of the equine influenza, although it is causing Australia's horseracing industry heavy losses. 
This form of flu is highly infectious and there is no vaccine available. So the transport of horses and the racing in the two states affected so far, New South Wales and Queensland, have been halted. But the flu does not spread to humans and in nearly all cases infected horses recover. 
Many experts worry that the biggest threat to animal, and potentially human, health is still the H5N1 strain of avian influenza virus. 

Since it re-emerged in Asia in 2003, it has spread to around 60 countries as far removed as Europe and Africa. 
The main victims have been poultry. Hundreds of millions of chickens and ducks have died or been culled, causing large losses to farmers. 
Most alarming for humans, the virus has killed at least 200 people in the past four years, out of 328 people known to have been infected. This is a death rate of well over 50 per cent. Indonesia has had more fatalities than any other country. 
Fortunately, the H5N1 virus moves from birds to humans only when there is very close contact between the two. And human-to-human transmission of the virus has so far been extremely rare. The WHO believes there have been just three cases - in Vietnam, Cambodia and Indonesia. In each case, the victim had prolonged and direct contact with another infected person. 
Amid the alarm about viral havoc has come some good news: H5N1 is not making much progress as a human invader, at least so far. 
Like most flu viruses, the H5N1 strain mutates. The concern is that if it makes the right changes, it could pass easily from person to person, just as the different strains of seasonal flu that infect humans around the world each year are able to do. 
Researchers linked to the National Institutes of Health in the US have focused on mutations that would let the H5N1 strain more easily recognise and enter human cells. 
Bird-adapted H5N1 binds to places on bird cells called surface receptors. These differ slightly from receptors on human cells, which is partly why bird-adapted H5N1 can infect but not spread easily among people. As a result of this research, new vaccines and drugs are being developed that could target predicted H5N1 mutants before the viruses evolve naturally. 
This work could possibly help to contain a pandemic early on. Another group of researchers in the US said recently that they had identified for the first time 32 major differences between avian and human flu. They found that the H5N1 virus had so far made only a few of these changes and that it would probably need to make at least 10 more to jump easily from birds to animals. 
Still, as the WHO report makes clear, better global co-operation is needed not just in research but in outbreak alert and response if the threat of spreading disease is to be countered. Improved animal and human health systems, especially in developing countries, are also crucial in meeting the challenge. 
* The writer, a former Asia editor of the International Herald Tribune, is a security specialist at the Institute of South East Asian Studies in Singapore. 


