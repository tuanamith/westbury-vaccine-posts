


On Sat, 15 Sep 2007 10:29:14 -0700, MsBeckaboo < <EMAILADDRESS> > wrote: 

Well one response would be "evidence, what evidence?" :) 
I'm not familiar with the books which you referenced, so I'm responding to commonly raised points - if there's anything specific which isn't covered could you please post that. 
(1) The older autistics here (late diagnosed usually) simply weren't subject to this sort of vaccination regime. MMR hadn't been brought in when I was a child, and I only had a couple of vaccinations total in infancy. 
The "late diagnosed" is important, because the historical figures you see used to claim an autism "epidemic" are *not* back corrected - IOW my diagnosis should have been included in the levels for nineteen umpty ump, but isn't. 
(2) The "evidence" appears to be almost exclusively UScentric - the evidence from the rest of the "western" world, countries with *different* vaccination regimes and practices to the US appears to be ignored, or even actively dismissed. That sound more like politics than science. 
Japan actually replaced MMR by single vaccines in 1993 (for reasons having nothing to do with autism or mercury), yet their rates of autism diagnoses continued to *rise* (just as in other western countries).  <URL>  (Note - this continuing rise *includes* the less common "regressive autism" which some people suggest as indicative of a vaccine "trigger".) 
(3) Wakefield's work has simply proven false - not only did he have a conflict of interests, but the actual lab work didn't show what he claimed it did. 
(4) It's only by selective "cherry picking" that mercury poisoning and autism can be made to look similar - there are key features of mercury poisoning which don't occur in autism, and common autistic traits which don't occur in mercury poisoning. 
(5) Twin studies (identical twins versus non-identical twins) show a strong genetic component to autism. Which doesn't necessarily exclude a "genetic susceptibility to an environmental trigger" model, but so far (despite a fair bit of searching) no such trigger has been found. 
(6) What has been found in at least one study, is something similar to Down's syndrome, but in the case of autism it's older *fathers* rather than older mothers. (And of course there are plenty of autistics born to younger fathers too, so it's just a trend).  <URL>  
(7) Recent work suggests that something called "copy number variation" (having more or fewer copies of sections of the genes) may be associated with autism. And interestingly this is associated with "spontaneous" autism.  <URL> / 

(8) Apart from Wakefield's claim, the main "support" for the claim appears to be anecdotal - child receives vaccination and shortly thereafter starts showing autistic traits. The trouble with this is that the age at which a number of vaccinations are done also happens to be much the same as the age at which Drs are willing to start identifying autism. 
So yes, this happens to individual families, but when you look at a *lot* of children, those who were vaccinated are no more likely to be autistic than those who weren't - So it does look very much as if this mental association of vaccination and autism is just that - the mind constructing a link between two events which occurred fairly close in time, but which are not otherwise related. 
I think that covers most of the points, but if there's something you've read which appears especially convincing to you, then do ask. (Though as another poster has noted this topic has been pretty much "talked to death" here, unless some new evidence comes up).    --  
Terry 

