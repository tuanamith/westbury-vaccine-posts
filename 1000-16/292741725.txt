


On Sun, 30 Sep 2007 22:22:22 -0000, MsBeckaboo < <EMAILADDRESS> > wrote: 

Splitting my response to your original post - so this one is just about age of vaccination. 
From the UK NHS (Q) "Why is the recommended age that a child should receive the mmr vaccine between 12 and 18 months? Surely if the vaccine is safe, it should be given much earlier to avoid getting measles, mumps and rubella?" 
(A) MMR is given shortly after the first birthday at around 13 months of age. This is because all babies are born with immunity (protection) which they get from their mums via the placenta. In the case of measles, mumps and rubella, this immunity lasts for about the first year of life. Giving MMR before the first birthday may mean that it doesn't work, as any remaining maternal immunity will wipe out any protective effect of the vaccine. 
[Presumably that means that the antibodies in the infant's system will latch on to the antigens in the vaccine, effectively "masking" them and thus blocking them from triggering an immune response. OTOH there are indications from work in developing countries that the first shot of (at least) measles vaccine can be effectively administered at 6-9 months. 
So this (and some other work) is suggestive that the vaccines could effectively be administered *earlier* - thus providing protection sooner.] 
As for 24 months rather than 15 - that's another 9 months unprotected exposure.  
And I doubt that delaying only until 24 months would convince. According to a study published in the Lancet in 2004 the mean age of diagnosis in the UK was 5.4 years 
 <URL>  
See figure 1 - where it looks as if less than 15% were diagnosed before 2 years (24 months), so 85% of cases could still be "blamed" on the vaccination. [I'm guessing that early age diagnosis in the US isn't *that* much greater than in the UK]. --  
Terry 

