


Media Mum on Oral Sex and Throat Cancer Risk Among Kids 
By Miriam Grossman, M.D. CNSNews.com Commentary August 8, 2007 
Smoking rates have gone down in New York City thanks to a campaign called "Nothing Will Ever Be the Same," featuring an unfortunate individual by the name of Ronaldo Martinez. 
"Smoking gave me throat cancer at 39," says a poster showing Mr. Martinez holding a metal device against his neck. "Now I breathe through a hole in my throat and need this machine to speak." 
The TV ad is more disturbing. Kids swim underwater in slow motion and happily splash about in a pool to the sound of a guitar. Mr. Martinez strolls nearby, fully clothed. An eerie, synthesized voice tells us, "I was born on an island where swimming was a way of life. I never thought that anything could keep me from the water. Then I got cancer from smoking cigarettes and lost my voice and have to speak through a hole in my throat. If water gets inside, it will drown me-I used to love swimming." 
Scare tactics? You bet. But as a result, says The New York Times, thousands of New Yorkers have quit the habit. The campaign, created in Massachusetts, was praised by public health officials in both states. The implication? The ends justify the means. 
I'm with The New York Times on this one. When it comes to prevention of a life-threatening illness, you remove the kid gloves and tell it like it is. 
Then why, I wonder, do we hear nothing of another widespread behavior associated with malignant tumors of the mouth and throat? 
The New England Journal of Medicine recently reported that cancer of the tonsils and base of the tongue are rising annually, and the evidence that oral HPV infection can cause these tumors is "compelling." Having more than five oral-sex partners increased the risk of these malignancies by 250 percent. 
The Journal's conclusion: "The widespread oral sex practices among adolescents may be a contributing factor in this increase." 
Teens consider oral sex less risky and more acceptable. No doubt that's why 20 percent of ninth graders and 50 percent of all teens have engaged in it. Ninth graders, by the way, are around 14 years old. 
What to do? The scientists contend: Because these oral cancers occur in both sexes, and are associated with the same HPV strains targeted by the new vaccine, we now have a reason to vaccinate both boys and girls. 
Was that Merck's stock I just heard go through the roof? Or was it the sound of lawmakers clamoring to legislate mandatory vaccinations of all fourth graders against an STI? 
Sure, the vaccine is a formidable biotechnological feat. But it provides incomplete protection, it has unknowns, and costs $360 per child. 
Why then is the solution to risky behavior a vaccine, and not behavioral change? Because radical liberalism permeates the field of sexual health, and to the Left, smoking is a loathsome evil, while casual sex is empowering and fun. 
As we contemplate this calamity, I have a suggestion for health educators: Stop encouraging our children to "explore" and "experiment" with sexuality. Replace that message with a hearty dose of scare tactics, a la Mr. Martinez and his artificial voice. 
Here's how I see it. A middle-aged woman is in her kitchen. "It was a long time ago," she tells us, "when I was in middle school. I thought it would be fun and make me popular. I'd stay a virgin, and not have to worry about getting pregnant. Now I have cancer of my tongue. I'll never eat my favorite foods again. The doctor says I have a 50 percent chance of dying in the next five years. I never thought a rainbow party could kill me." 
If you're a parent of a tween, and unfamiliar with rainbow parties, you've got some homework to do. And for those of you who put your trust in latex, you need to know that "protection" is used in 9 percent of these encounters. 
Mr. Martinez must see a doctor every three months to see if the cancer has returned: "I'm always afraid of bad news." He holds the tobacco companies responsible for his pain and suffering. 
The study showing an association between oral sex and oral cancer was published in May of this year. But I've seen no urgent warnings, red alerts, or press releases being issued by pediatricians or specialists in adolescent health. 
Who will today's seventh-grader blame in the future, when she learns that health experts whitewashed the dangers of her cancer-causing activities? 
Miriam Grossman, M.D., is a senior fellow with the Clare Boothe Luce Policy Institute and the author of the book, "Unprotected: A Campus Psychiatrist Reveals How Political Correctness in Her Profession Endangers Every Student." 

-------------------------------------------------------------------------------- 
This commentary was first published by Cybercast News Service, www.cnsnews.com 


