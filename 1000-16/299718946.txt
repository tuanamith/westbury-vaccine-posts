




A British judge ruled on the eve of Al Gore co-winning the Nobel Peace Prize that students forced to watch "An Inconvenient Truth" must be warned of the film's factual errors. But would there be any science at all left in Gore's "truth" if these errors and their progeny were excised? 
Minutes of non-science filler dominate the opening sequence - images of the Gore farm, Earth from space, Gore giving his slideshow and the 2000 election controversy. Gore then links Hurricane Katrina with global warming. But the judge ruled that was erroneous, so the Katrina scenes would wind up on the cutting-room floor. 
Another 12 minutes of filler go by - images of Gore in his limo, more Earth photos, a Mark Twain quote, and Gore memories - until about the 16:30 minute mark, when, according to the judge, Al Gore erroneously links receding glaciers - specifically Mt. Kilimanjaro - with global warming. 
The Mt. Kilimanjaro error commences an almost 10-minute stretch of problematic footage, the bulk of which contains Gore's presentation of the crucial issue in the global warming controversy - whether increasing levels of atmospheric carbon dioxide drive global temperatures higher. As the judge ruled that the Antarctic ice core data presented in the film "do not establish what Mr. Gore asserts," this inconvenient untruth also needs to go. 
After still more filler footage about Winston Churchill, the 2000 election, and rising insurance claims from natural disasters, Gore spends about 35 seconds on how the drying of Lake Chad is due to global warming. The judge ruled that this claim wasn't supported by the scientific evidence. 
More filler leads to a 30-second clip about how global warming is causing polar bears to drown because they have to swim greater distances to find sea ice on which to rest. The judge ruled however, that the polar bears in question had actually drowned because of a particularly violent storm. 
On the heels of that error, Gore launches into a 3-minute "explanation" of how global warming will shut down the Gulf Stream and send Europe into an ice age. The judge ruled that this was an impossibility. 
Two minutes of ominous footage - casting Presidents Reagan and George H.W. Bush, and Sen. James Inhofe (R-OK) in a creepy light and expressing Gore's frustration with getting his alarmist message out - precede a more-than-9-minute stretch that would need to be cut. 
In this lengthy footage, Gore again tries to link global warming with discrete events including coral reef bleaching, the melting of Greenland, catastrophic sea level rise, Antarctic melting and more. But like Hurricane Katrina, these events also shouldn't be linked with global warming. 
Based on the judge's ruling, the footage that ought to be excised adds up to about 25 minutes or so out of the 98-minute film. What's left is largely Gore personal drama and cinematic fluff that has nothing to do with the science of climate change. 
It should also be pointed out that Gore makes other notable factual misstatements in the film that don't help his or his film's credibility. 
He says in the film that polio has been "cured," implying that we can cure "global warming." While a preventative polio vaccine does exist, there is no "cure" for polio. 
Gore attempts to smear his critics by likening them to the tobacco industry. In spotlighting a magazine advertisement proclaiming that "more doctors smoke Camel than any other brand," he states that the ad was published after the Surgeon General's 1964 report on smoking and lung cancer. But the ad is actually from 1947 - 17 years before the report. 
Gore also says in the film that 2005 is the hottest year on record. But NASA data actually show that 1934 was the hottest year on record in the U.S. - 2005 is not even in the top 10. 
Perhaps worse than the film's errors is their origin. The BBC reported that Gore knew the film presented incorrect information but took no corrective steps because he didn't want to spotlight any uncertainties in the scientific data that may fuel opponents of global warming alarmism. 
"An Inconvenient Truth" grossed about $50 million at the box office and millions more in DVD and book sales. Gore charges as much as $175,000 for an in-person presentation of his slide show that forms the basis for the film. 
Considering that a key 25 percent of "An Inconvenient Truth" is not true - and perhaps intentionally so - it seems only fair that Gore offer a refund to moviegoers, DVD/book purchasers and speaking sponsors. Where are the class action lawyers when you need them? 


