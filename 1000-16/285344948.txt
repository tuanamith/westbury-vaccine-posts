


On Sep 6, 1:35 am, Mark Hickey < <EMAILADDRESS> > wrote: 

6 March 2003 APPENDIX A (sic) HISTORICAL ACCOUNT OF IRAQ'S PROSCRIBED WEAPONS PROGRAMMES ... 
Destruction ...   During the bombing campaign the main CW facilities at   Al Muthanna and Al Fallujah were heavily damaged. In   addition, some of the CW weapons stored at airfields and   other locations were also destroyed. However, Iraq had   evacuated  [note:  'evacuated' to other locations in Iraq,FF]   much of its strategic materials and equipment prior to the   war... 
  Thus, several hundreds of tonnes of Mustard and Sarin   were buried in the desert surrounding Al Muthanna during t   he war and survived the bombing. The agents was (sic)   subsequently destroyed by UNSCOM. ... 
. It was clear, even from this first inspection, that the site had   been severely disabled, but not completely destroyed. The   scene was one of smashed production plants and leaking...    the second chemical inspection team visited the precursor   plants at Al Fullujah and inspected similar destruction levels. 
... 
  Before UNSCOM could begin its work on the elimination remaining   CW capabilities, Iraq secretly began its own unilateral destruction.    Iraq declared that, in July 1991, under instruction from Lieutenant-   General Hussein Kamal, it began the unilateral destruction of selected   chemicals and munitions; this activity was not disclosed to UNSCOM   at the time. ...It is probable that one of the reasons for this unilateral   destruction was an effort to bring what UNSCOM might find more   into line with the serious inadequacies in Iraq's initial declaration   of its holdings of proscribed weapons and materials. ...   In all, Iraq declared the destruction of over 28,000 filled and unfilled   munitions, about 30 tonnes of bulk chemical precursors for Sarin   and Cyclosarin, and over 200 tonnes of key precursors relating to Vx. [I presume this refers to a subsequent declaration, perhaps as late as 2002, FF] ...   The remaining weapons, materials and equipment declared by Iraq,    that could be identified and located by UNSCOM, were destroyed   under its supervision, mainly between 1992 and 1994. Thus, over 28,000   munitions, 480 tonnes of CW agent and 100,000 tonnes of precursor   chemicals were disposed of. About 400 major pieces of chemical   processing equipment and some hundreds of items of other equipment,   such as bomb-making machinery, were also destroyed under UNSCOM s   upervision. ... Dual-use capabilities to 1998 ... 
 Much of this civilian chemical industry used dual-capable technology   and was, therefore, under monitoring by UNSCOM until the end of 1998. 
Herein lay the concern, that during tthe gap between UNSCOM and UNMOVIC Iraq might have converted dual-use facilities to CW production, or rebuilt the destroyed factories.  NO evidence to support those fears was found by UNMOVIC before the invasion or ISG afterwards.    As noted by Dr David Kay, " no factories, no weapons.". ] 

Conclusions 
  UNMOVIC has a good understanding of the nature and scope   of Iraq's CW programme. The areas of greatest uncertainty   relate to questions of material balance and whether there may   be items still remaining. In this regard, Iraq's unilateral destruction   of large quantities of chemicals and weapons, in July 1991, has   complicated the accountancy problem. The questions of uncertainty   are discussed further in the Clusters of Unresolved Disarmament   Issues. 
Understand??? 
...   By some standards, the technology levels achieved by Iraq in the   production of its CW agents and weapons, were not high. The agents   were often impure and had a limited shelf-life. ... 
[IOW, CW not disposed of during the 1990s would no longer be effective by 2003.  No new factories, no new weapons, FF] .. 
  It is evident that Iraq's CW capabilities posed a significant regional threat. 
[ IN 1991, not in 2003! ] 
IRAQ'S BIOLOGICAL WARFARE PROGRAMME 
... 
  Iraq went to considerable lengths, including the destruction of   documents and the forging of other documents, to conceal its   BW efforts from UNSCOM. After intensive investigations by    UNSCOM, Iraq disclosed some details of its offensive BW   programme on, 1 July 1995. ... in August 1995, Iraq   revealed a much more comprehensive BW programme. 
[Note:  UNSCOM pre-dated UNMOVIC and ceased activity in Iraq in 1998.  The secrecy and obstruction pre-dated UNMOVIC.] 
  Iraq's efforts to conceal the programme, particularly the   destruction of documentation and its declared unilateral   destruction of BW weapons and agents, have complicated   UNMOVIC's task of piecing together a coherent and accurate   account of its BW programme. ...   In May/June 1996, all of the facilities, related equipment and   materials declared by Iraq as belonging to its BW programme   were destroyed under UNSCOM supervision.  Thus, the vaccine f   ermenters at Al Daura that Iraq had declared had produced   botulinum toxin were destroyed, as was the entire Al Hakam   complex, including all its equipment and materials. ...   These (other ostensibly civilian, FF] facilities were included in   routine monitoring by UNSCOM; no proscribed activities were    detected at these sites up to the end of inspections in December   1998. 
[Once again the concern was that during the gap between UNSCOM and UNMVIC, Iraq could have resumed production of BW.  Again, UNMOVIC found NO EVIDENCE of renewed production.] 

Uncertainties regarding Iraq's BW programme 
Unilateral destruction 
  The almost complete lack of documentation on unilateral   destruction activities in 1991 gives rise to the greatest uncertainties   regarding Iraq's declaration of BW activities. Although there   is physical evidence that some such destruction took place,    it was difficult for UNSCOM inspectors to quantify the numbers   and amounts. This, in turn, has repercussions on assessment   of material balance and whether all materials and weapons   have been accounted for. 

*** 
In summary, the numerous unresolved WMD issues in the report are ubiquitously matters left over from UNSCOM 1990s and in no way constitute evidence of post turn of the century WMD  production or obstruction of UNMOVIC. 
THAT is what the report says. 
The argument that Iraq was a threat in 2003 relied on confabulating UNSCOM of the 1990s with UNMOVIC of 2002-3,  ignoring the short shelf-life of Iraqi munitions, ignoring the absence of manufacturing facilities, ignoring the 'unprecedented'  cooperation with UNMOVIC and requiring that Iraq  achieve the logical impossibility of proving a negative hypothesis. 
To argue that the March 2003 UNMOVIC report was evidence that Iraq was a threat to the United States, defies reason. 
-- 
FF 


