



Yahoo! News  <URL> = dnqH_0an_boHOH.7D8EsR.3QA 
Staph fatalities may exceed AIDS deaths 
By LINDSEY TANNER, AP Medical Writer Tue Oct 16, 7:37 PM ET 
More than 90,000 Americans get potentially deadly infections each year from a drug-resistant staph "superbug," the government reported Tuesday in its first overall estimate of invasive disease caused by the germ. 
Most drug-resistant staph cases are mild skin infections. But this study focused on invasive infections - those that enter the bloodstream or destroy flesh and can turn deadly. 
Researchers found that only about one-quarter involved hospitalized patients. However, more than half were in the health care system - people who had recently had surgery or were on kidney dialysis, for example. Open wounds and exposure to medical equipment are major ways the bug spreads. 
An invasive form of the disease is being blamed for the death Monday of a 17-year-old Virginia high school senior. Doctors said the germ had spread to his kidneys, liver, lungs and muscles around his heart. 
The researchers' estimates are extrapolated from 2005 surveillance data from nine mostly urban regions considered representative of the country. There were 5,287 invasive infections reported that year in people living in those regions, which would translate to an estimated 94,360 cases nationally, the researchers said. 
Most cases were life-threatening bloodstream infections. However, about 10 percent involved so-called flesh-eating disease, according to the study led by researchers at the federal Centers for Disease Control and Prevention. 
There were 988 reported deaths among infected people in the study, for a rate of 6.3 per 100,000. That would translate to 18,650 deaths annually, although the researchers don't know if MRSA was the cause in all cases. 
If these deaths all were related to staph infections, the total would exceed other better-known causes of death including AIDS - which killed an estimated 17,011 Americans in 2005 - said Dr. Elizabeth Bancroft of the Los Angeles County Health Department, the editorial author. 
The results underscore the need for better prevention measures. That includes curbing the overuse of antibiotics and improving hand-washing and other hygiene procedures among hospital workers, said the CDC's Dr. Scott Fridkin, a study co-author. 
Some hospitals have drastically cut infections by first isolating new patients until they are screened for MRSA. 
The bacteria don't respond to penicillin-related antibiotics once commonly used to treat them, partly because of overuse. They can be treated with other drugs but health officials worry that their overuse could cause the germ to become resistant to those, too. 
A survey earlier this year suggested that MRSA infections, including noninvasive mild forms, affect 46 out of every 1,000 U.S. hospital and nursing home patients - or as many as 5 percent. These patients are vulnerable because of open wounds and invasive medical equipment that can help the germ spread. 
Dr. Buddy Creech, an infectious disease specialist at Vanderbilt University, said the JAMA study emphasizes the broad scope of the drug- resistant staph "epidemic," and highlights the need for a vaccine, which he called "the holy grail of staphylococcal research." 
The regions studied were: the Atlanta metropolitan area; Baltimore, Connecticut; Davidson County, Tenn.; the Denver metropolitan area; Monroe County, NY; the Portland, Ore. metropolitan area; Ramsey County, Minn.; and the San Francisco metropolitan area. ____ On the Net: JAMA:  <URL>  CDC:  <URL>  =A9 2007 The Associated Press. 


