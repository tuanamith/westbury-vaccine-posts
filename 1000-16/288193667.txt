


Hillary: Proof Of Health Insurance Required For Work Democrat Hillary Rodham Clinton said Tuesday that a mandate requiring every  American to purchase health insurance was the only way to achieve universal  health care but she rejected the notion of punitive measures to force  individuals into the health care system. "At this point, we don't have anything punitive that we have proposed," the  presidential candidate said in an interview with The Associated Press.  "We're providing incentives and tax credits which we think will be very  attractive to the vast majority of Americans." 
She said she could envision a day when "you have to show proof to your  employer that you're insured as a part of the job interview - like when your  kid goes to school and has to show proof of vaccination," but said such  details would be worked out through negotiations with Congress. 
Clinton unveiled her health care plan Monday in Iowa, promising to bring  coverage to every American by building on the current employer-based system  and using tax credits to make insurance more affordable. 
She told the AP she relished a debate over health care with her political  opponents, including Republicans "who understood that we had to reform  health care before they started running for president." 
On Tuesday, Clinton began airing a 30-second ad statewide in Iowa and New  Hampshire promoting her new health care plan. The ad reminds viewers of her  failed effort to pass universal health care in the early 1990s, trying to  portray a thwarted enterprise as one of vision. 
"She changed our thinking when she introduced universal health care to  America," the ad's announcer says. 
The ad also highlights her support as senator for an expanded Children's  Health Insurance Program and for more affordable vaccines. 
Her health care plan would require every American to buy health insurance,  offering tax credits and subsidies to help those who can't afford it. The  mandatory aspect of her proposal, however, gets glossed over in the ad. 
"Now she has a health care plan that lets you keep your coverage if you like  it, provides affordable choices if you don't, and covers every American,"  the ad says. 
The ad also continues her campaign's effort to appropriate the mantle of  change away from rivals Barack Obama and John Edwards. The word change or  its variations appears four times in the ad, which ends: "So, if you're  ready for change, she's ready to lead." 
Though her ads are airing in major markets in both states, they are  appearing with greater frequency in Iowa. Polls of voters in New Hampshire  show her with a double digit lead over Obama and Edwards, but polls in Iowa  show the three of them clustered together. 
Source:  Associated Press 

--   "We gave peace a chance and got 9-11"  



