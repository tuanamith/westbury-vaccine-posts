


- 
Edward N. Brandt Jr., a Leader on AIDS, Dies at 74 
By LAWRENCE K. ALTMAN [New York TImes] 
Dr. Edward N. Brandt Jr., who oversaw the nation's initial response to AIDS as the top-ranking doctor in the government in the early 1980s, died Aug. 25 [2007] at his home in Oklahoma City [Oklahoma]. He was 74. 
The cause was lung cancer, his son, Edward III, said. 
Dr. Brandt, who also eventually led several medical schools and a university, became assistant secretary of the Department of Health and Human Services and acting surgeon general of the Public Health Service in 1981, when doctors were discovering AIDS. At the time, its cause was unknown, and it was regarded as inevitably fatal. 
Soon, many AIDS activists and health officials criticized the Reagan administration as being slow to investigate and find effective therapies for the disease, which has now infected an estimated 65 million people worldwide, killing 25 million of them. 
In 1983, Dr. Brandt said that investigating the disease had become "the No. 1 priority" of the Public Health Service. At the time, only 1,450 AIDS cases had been reported. 
Dr. Brandt, coming to his government job as a medical school administrator, found himself in a difficult position. While AIDS was his first priority, it was not for the rest of the Reagan administration, whose policy was to cut costs. But as a physician, Dr. Brandt knew that scientists needed money to study AIDS, although money alone would not suffice. 
Dr. Brandt maintained a public position that the government was spending enough money on AIDS and other diseases. But he worked internally and with great difficulty to try to overcome bureaucratic and political obstacles to allocating more money for AIDS. 
For example, the Reagan administration set out to eliminate 700 jobs at what is now known as the Centers for Disease Control and Prevention, the federal agency responsible for investigating and tracking AIDS and other infectious diseases. 
William H. Foege, the agency's director at the time, said in an interview Wednesday that "Ed Brandt continually fought on the inside" to reduce the number of staff and budget cuts. 
Dr. Brandt was also instrumental in finding a way for the disease control centers to build a maximum security laboratory, which the agency needed to investigate a growing number of dangerous microbes, but which the Reagan administration had blocked, Dr. Foege said. 
In addition to his medical degree, Dr. Brandt had a Ph.D. in mathematics, and he used that training in his various jobs, so it was not easy to fool him with numbers, Dr. Foege said. Yet Dr. Brandt drew widespread criticism for making an overly optimistic prediction for when an AIDS vaccine would be available. 
At a news conference in 1984, Dr. Brandt's boss, Health and Human Services Secretary Margaret M. Heckler, announced that scientists at the National Institutes of Health in Bethesda, Md., had discovered the AIDS virus. But she gave little credit to Dr. Luc Montagnier's team from the Pasteur Institute in Paris, which had also discovered the virus. 
Ms. Heckler also said that an AIDS vaccine would be tested in two years and Dr. Brandt said he was optimistic that a marketable one would be available in three years, or 1987. 
When reporters said that Dr. Montagnier had told them it would take 5 to 10 years, Dr. Brandt stood by his three-year prediction. 
Now experts say that making an AIDS vaccine is far more complex than originally believed. Any vaccine is years away from being licensed and might protect a much smaller proportion of recipients than other standard immunizations. 
Edward Newman Brandt Jr. was born in Oklahoma City on July 3, 1933. He earned his undergraduate, medical and Ph.D. degrees from the University of Oklahoma and a master's from Oklahoma State University. 
After training in internal medicine at the University of Oklahoma, he joined its medical faculty in 1961, teaching preventive medicine and eventually becoming associate dean and associate director of the medical center. In 1970, he moved to the University of Texas Medical Branch in Galveston, where he eventually became executive dean. 
health affairs for the University of Texas System. 
Dr. Vivian Pinn, who directs the Office of Research on Women's Health at the National Institutes of Health, said Dr. Brandt was also recognized as "the godfather of women's health" for his efforts as assistant secretary of health and human services to encourage more study of the issue. "He was instrumental in promoting the careers of many people, especially women in science and women's health," Dr. Pinn said. 
After leaving his job as assistant secretary of health in 1984, Dr. Brandt became president of the University of Maryland at Baltimore. In 1989, he returned to the University of Oklahoma as executive dean and, though officially retired, continued working and teaching until shortly before his death. 
Surviving him are his father, Edward Sr., and stepmother, Patricia; his wife of 54 years, the former Patricia Lawson; three sons, Patrick J., of Dallas, Edward III, of Dallas [Texas], and Rex C., of Clinton, Iowa; a sister, Jennifer Wessel of Dallas; a brother, Carlin, of Austin [Texas], and seven grandchildren. 
 <URL>  


