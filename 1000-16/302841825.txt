



AIDS Vaccine Doesn't Guard Against Virus 
By LINDA A. JOHNSON Associated Press Writer 
Nov 7th, 2007 | TRENTON, N.J. -- New data on an experimental AIDS vaccine that failed to work shows volunteers who got the shots were far more likely to get infected with the virus through sex or other risky behavior than those who got dummy shots. 
The new details, released Wednesday by drugmaker Merck & Co., don't answer the crucial question of whether failure of the vaccine also spells doom for many similar AIDS vaccines now in testing. 
And researchers weren't sure why more of the vaccinated volunteers wound up getting HIV than those who got dummy shots. 
"One of the possibilities is that the increase in the number of infections was related to the vaccine," meaning it could have made people more susceptible to HIV infection, said Dr. Keith Gottesdiener, vice president of clinical research at Merck Research Laboratories. He couldn't say how likely that was but said other factors, even coincidence, could be the explanation. 
Merck, based in Whitehouse Station, N.J., announced on Sept. 21 that it was stopping the study because the vaccine didn't work. It was a stunning setback in the push to develop an AIDS vaccine. 
The vaccine is made from a common cold virus with three synthetic HIV genes tucked inside. It's designed to stimulate the immune system to kill any HIV-infected cells encountered in the future. 
However, the researchers found that volunteers with pre-existing immunity to this particular cold virus were much more likely to get infected with HIV if they got the AIDS vaccine than if they got the dummy shot. 
Some 3,000 people, mostly gay men and female sex workers, had volunteered to get the experimental vaccine or dummy shots. All were warned to protect themselves from AIDS exposure. 
At the time the study was halted in September, Merck said 24 of 741 volunteers who got the vaccine in one segment of testing later developed HIV; 21 of 762 participants who got dummy shots also were infected. 
New data released Wednesday showed that to date, 49 of 914 vaccinated men became infected with HIV, compared with 33 of the 922 men who got dummy shots. Only one woman and a small number of heterosexual men were infected. 
"In my mind, this doesn't damn anything," said Dr. Anthony Fauci, head of National Institute of Allergy and Infectious Diseases, said of the vaccine's failure. "It tells you you need to be very careful with every aspect" of vaccine design and testing. The international testing was partly funded by the National Institutes of Health. 
Merck's head of medical affairs for vaccines, Mark Feinberg, said it could be a few years before further data mining and results of other drugmakers' vaccine tests clear up the mystery. 
In trading Wednesday, Merck shares fell $1.79, or 3.2 percent, to $54.20. 
--- 
On the Net:  <URL>  
HIV Vaccine Trials Network:  <URL>  

"Huh?"--L'il Jak 
"I ain't ascared a no virus!"--Faggy Greggy 
"Huh?"--L'il Jak 


