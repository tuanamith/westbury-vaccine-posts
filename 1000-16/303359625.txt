



"Yuri Kuchinsky" < <EMAILADDRESS> > wrote in message 
   <URL>  
POSTED: 4:06 pm CST November 7, 2007 

WASHINGTON -- New data on an experimental AIDS vaccine that failed to work show volunteers who got the shots were far more likely to get infected with the virus through sex or other risky behavior than those who got dummy shots. 
The new details, released Wednesday by drug maker Merck & Co., don't answer the crucial question of whether failure of the vaccine also spells doom for many similar AIDS vaccines now in testing. 
And researchers weren't sure why more of the vaccinated volunteers wound up getting HIV than those who got dummy shots. 
Some 3,000 people, mostly homosexual men and female sex workers, had volunteered to get the experimental vaccine or dummy shots. All were warned to protect themselves from AIDS exposure. 
Merck's head of medical affairs for vaccines, Mark Feinberg, said it could be a few years before further data mining and results of other drug makers' vaccine tests clear up the mystery. 



