


On Sun, 30 Sep 2007 20:54:18 -0700, MsBeckaboo < <EMAILADDRESS> > wrote: 

Are they *really* though? Or is that just wishful thinking - or at least an unjustified overgeneralisation from "*sometimes* useful"? 
I can think of plenty of examples from families, to politics, to international treaties, where compromise and middle ground have just "papered over the cracks" temporarily, but since the compromise isn't really acceptable to one or both sides, has failed - sometimes with worse consequences *because* people have acted on the assumption that the issue has been "dealt with".   I would say that they can be useful where appropriate, and reasonably & fairly done, otherwise they're just a pretence, and sometimes a dangerous one. 

Which is not to say that an alternative form might not have developed. (Isn't it ninereeds who regualrly mentions the difference in social structure between our close relatives, the chimps and bonobos? :) 

We're always (at least for the foreseeable future) going to be in that sort of situation - What I am arguing is that people need to be better trained in dealing both with the uncertainty, and with the information that we *do* have. 

Evidence? Or emotion? - Children go "out of the womb, into the world" (and a world much less hygienic than ours is today). They were *naturally* and uncontrollably exposed to many more potential pathogens than the number which are included in vaccines. 
Infants may be "immature" if you define maturity as "being like an adult" - but infants are good at what infants *do*, growing, learning, and self-evidently surviving (otherwise their genes wouldn't be in the present day population). 
And if their immune system is that "immature", then how come vaccines *work*? How come that childhood deaths from X fall drastically after the introduction of a vaccine for X? 
There's a major difference between a "naive" immune system (one which hasn't been exposed to a range of pathogens), and an "immature" one (one which can't react to them). 
And while the infant immune system may not be 100% as effective as an adults' (e.g. against pathogens with sugars in their coat IIRC [read the reference earlier, now can't find it again]), there's increasing evidence that it's more effective / effective earlier than the "common wisdom" expects. 
 <URL>  
 <URL>  

This is really a circular argument, it *assumes* that vaccination (but somehow *not* natural exposure) *must* overload -  With natural exposure, people *don't* assume that a child's immune system has been "overloaded" unless they actually fall ill - so *why*? 
[I think that like many things autistic, immune system issues are "more common but far from universal".]  

But since no one knows *which* ones, how does that help them?  
Either to vaccinate and protect against the disease but to take an unknown (but so far undetectable) risk, or not to vaccinate and risk their child catching the disease (and passing it on to other children and in the case of rubella, pregnant women). 

Given that (anecdotally at least) autistic kids seem if anything to be *more* prone to allergies than the norm, how is that supposed to work out? 

No I don't expect you to be devoid of feeling - but ATM your fears seem to be paralysing you. 
The quoted chances of actually dying from measles in a modern western country seem to have a wide range of variation; One site does indeed give the 1:1000 figure;  <URL>  "The fatality rate from measles for otherwise healthy people in developed countries is low: approximately 1 death per thousand cases."  <URL>  gives "Meningitis / encephalitis 1 in 1000" "Death 1 in 2500 to 5000"  <URL>  gives "encephalitis (inflammation of the brain, which occurs in about one in 5,000 cases)" and "complications involving the nervous system occur in fewer than one in 1,000 cases" but no mortality figures. 
NHS Direct (which you'd expect to be an authoritative source) lists the complications but gives no frequency or mortality rates. 
But of course the problem is, that measles is known to sometimes lead to permanent damage or death - but MMR isn't known to sometimes lead to autism. For instance Japan withdrew MMR in 1993 and in the 300,000 population area studied there were *no* MMR vaccinations after this date (so no "using up old stock" problem) 
"This study examined cumulative incidence of ASD up to age seven for children born from 1988 to 1996 in Kohoku Ward (population approximately 300,000), Yokohama, Japan. ASD cases included all cases of pervasive developmental disorders according to ICD-10 guidelines. RESULTS: The MMR vaccination rate in the city of Yokohama declined significantly in the birth cohorts of years 1988 through 1992, and not a single vaccination was administered in 1993 or thereafter. In contrast, cumulative incidence of ASD up to age seven increased significantly in the birth cohorts of years 1988 through 1996 and most notably rose dramatically beginning with the birth cohort of 1993." 
IOW if there's any effect, at best it's too small to detect.  
I understand what's happening - people tend to discount known risks compared to unknown ones. In the UK, spot checks on infant car seats all over the country show regualrly over half (and in one case 72%) to be faulty, badly fitted or secured. And yet they must know that children are killed and seriously injured in car accidents. 
Yet one would assume most of these parents *believe* that they are concerned about their children's safety - Do you begin to see *why* I'm so concerned about people "thinking with their feelings"? --  
Terry 

