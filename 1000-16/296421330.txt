


Is it Safe? 
A new vaccine can protect against HPV 
By TANYA ENBERG 
What isn't present in this day-to-day tracking of a child's life is mention of visiting the doctor for an HPV vaccination. 
Heck, at age nine, I'd never even heard of HPV (human papillomavirus) or any other STD, for that matter. 
But back-to-school time isn't what it used to be, kids. 
The vaccine in question is called Gardasil, manufactured by Merck Frosst Canada Ltd. 
NOT EVERYONE'S THRILLED 
The prominent women's health website Women to Women issued a release warning against the vaccination hype. 
But who can blame anyone for posing questions yet to be answered? 
It's irresponsible not to. 
There are, she insists, too many unknowns. 
"Why the rush?" she asks. 
"Why this September and not next September?" 
It's a good question. 
--- 
GET THE FACTS 
HPV is the leading cause of cervical cancer. 
- Getting pap exams is imperative for detecting HPV. 
- The virus is contracted through skin-to-skin contact. 
- There are numerous types of HPV. Many are considered "low risk" and are not associated with cancer. 
- An estimated 75% of Canadians will have an HPV infection in their lifetime. 
- HPV can manifest as genital warts but is known as the silent infection because it often carries no symptoms at all. 

 <URL>  


