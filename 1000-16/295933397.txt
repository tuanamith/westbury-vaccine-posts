


UNICEF Press Centre "Change the World With Children" 
 <URL>  
The GAVI data are based on the plans prepared by the countries and partners in the 53 approved countries. Five-year commitments to these countries total more than $800 million. GAVI partners estimate that this investment could result in more than two million lives saved, based on current data of disease burden and immunization costs. The projected results are subject to change, both because some countries may not reach their targets, and others may surpass them. 
# # # # 
The Global Alliance for Vaccines and Immunization (GAVI) is a public- private partnership formed in response to stagnating global immunization rates and widening disparities in vaccine access among industrialized and developing countries. The GAVI partners include: national governments, the vaccine industry, NGOs, foundations, research and public health institutions, the United Nations Children's Fund (UNICEF), the World Bank Group and the World Health Organization (WHO). The Vaccine Fund is a new financing resource created to support the GAVI immunization goals, providing financial support directly to low-income countries to strengthen their immunization services and to purchase new and under-used vaccines. 
For further information, contact: Lisa Jacobs +41 79 447 1935 
Heidi Larson, UNICEF New York, e-mail  <EMAILADDRESS>  1 212 326 7762 or +1 646 207 5179 


