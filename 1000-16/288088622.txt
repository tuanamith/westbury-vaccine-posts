


The Christian Science Monitor   Sep 18, 9:45 PM EDT Studies: TB Can Be Treated in Few Months  By MARILYNN MARCHIONE AP Medical Writer 
CHICAGO (AP) -- New research gives hope for successfully  treating tuberculosis in a few months rather than the six  months or more currently needed to beat the contagious lung  disease, doctors reported Tuesday. 
Adding the antibiotic moxifloxacin to the usual TB drugs  shortened the time to cure to an estimated four months in a  study in Brazil, Johns Hopkins University scientists reported  at an American Society for Microbiology conference in Chicago. 
A second study by Hopkins researchers cured mice of TB in 10  weeks instead of the usual six months with moxifloxacin plus  the TB drug rifapentine at higher doses. 
"It sounds fantastic," said Dr. Melvin Spigelman, research and  development director for the nonprofit Global Alliance for TB  Drug Development in New York. "The science is there" and just  needs to be verified in larger studies, he said. 
The group will launch a 2,400-patient study later this year. 
Also on Tuesday, the Seattle-based Bill & Melinda Gates  Foundation announced its largest grants ever to fight TB -  $280 million for research on vaccines, diagnostics and drugs. 
"If everything goes well, it should be feasible to shorten  treatment time," possibly even to ultra-short regimens of two  weeks to a month, said Ken Duncan, the foundation's program  director. 
More than 8 million people worldwide develop TB each year, and  nearly 2 million die of it. The disease is mostly a problem in  poor countries, but the recent case of Andrew Speaker, an  Atlanta attorney who created an international health scare by  traveling while he had a multidrug-resistant strain of TB,  shows the danger in the United States as well. 
Recently, a Mexican teenager was jailed in Georgia and  threatened with deportation after refusing to take his  recommended nine months of TB treatment. 
On Tuesday, a man with a multiple drug-resistant form who had  been detained after walking in public without a mask was  returned to Arizona after doctors treating him in Colorado  said he was no longer infectious. 
Treatment now consists of three or four antibiotics taken  daily for six months or more. But half of patients do not take  all their pills, allowing resistant bacteria to grow and  spread, said Dr. Jacques Grosset, the Hopkins researcher who  led the study of several hundred mice. 
The Brazil study involved about 170 men and women in Rio de  Janeiro who had active TB. All were given three standard anti- TB drugs plus either moxifloxacin or an older drug,  ethambutol. 
After two months, 85 percent of those on moxifloxacin tested  negative for the infection compared to 68 percent on  ethambutol. The treatment advantage showed up in as little as  two weeks. 
"Based on what we know, if you get that big a difference at  two months, you should be able to shorten the duration of  treatment ... down to four," said Dr. Richard Chaisson,  director of TB research at Hopkins. 
In a third study of about 400 TB patients throughout Africa,  60 percent who received moxifloxacin plus three other drugs  tested negative for TB at two months versus 55 percent given  isoniazid and the other medications. 
The federal government paid for the studies, and Bayer  Healthcare AG donated moxifloxacin, which it sells as Avelox  in the United States for short-term use against pneumonia and  other respiratory illnesses. The pill costs $10 a day, but  researchers said Bayer has promised to make it available in  poor countries for less if it is approved to treat TB. 
--- 
On the Net: 
Microbiology conference:  <URL>   
Gates foundation:  <URL>   <URL>  ENT?SITE=MABOC&SECTION=HOME&TEMPLATE=DEFAULT&CTIME=2007-09-18- 21-45-50 --  A government, of, by, and, for: Rich, Elite, Freemasons. But all things that are reproved are made manifest by the  light:  for whatsoever doth make manifest is light. The light shineth in darkness;  and the darkness comprehended it not. The light of the body is the eye: if therefore thine eye be  single,  thy whole body shall be full of light.  But if thine eye be evil, thy whole body shall be full of  darkness.  If therefore the light that is in thee be darkness, how great  is that darkness! Awake thou that sleepest, and arise from the dead,  and Christ shall give thee light. For my yoke is easy, and my burden is light. 

