


The Christian Science Monitor   Oct 22, 5:28 PM EDT Vaccine Plan for Flu Pandemic Drafted  By LAURAN NEERGAARD AP Medical Writer 
WASHINGTON (AP) -- Pregnant women, babies and toddlers would  join doctors, emergency workers and soldiers at the head of  the line for scarce vaccine if a super-strain of flu triggers  the next pandemic, says a draft government plan to be released  Tuesday. 
Once more vaccine is brewed, older children along with workers  who keep the electricity, water and phones running could be  next to roll up their sleeves. 
At the end of the line: The elderly and healthy younger  adults. 
It's a priority list quite different from the usual winter  pleas for older Americans to get vaccinated against regular  flu. And it reflects growing agreement that curbing a super- flu would require protecting workers who care for the sick and  maintain crucial services - plus targeting the people most  likely to spread flu, not just die from it. 
"Children are not only highly susceptible to influenza,  children are also very good at spreading it," said William  Raub, emergency planning chief at the Department of Health and  Human Services. "Protecting them also protects those in the  population." 
The list will prove no surprise to state and local health  authorities struggling to plan how they would ration vaccine  for a panicked population. The Bush administration has long  signaled its key priority groups. 
But the new draft plan puts a rationale for step-by-step  vaccination to paper, opening it to formal debate before the  list is finalized - not as set-in-stone rules, but as  guidelines for states. 
"Some local discretion is going to be imperative here," Raub  said. 
Pandemics can strike when the easy-to-mutate flu virus shifts  to a strain that people have never experienced. Scientists  cannot predict when the next pandemic will arrive, although  concern is rising that the Asian bird flu known as H5N1 might  trigger one if it starts spreading easily from person to  person. 
Vaccine must be custom-brewed to each circulating flu strain,  something that would take several months after a pandemic  began. The government is stockpiling vaccine against the  current bird flu strain in hopes it could offer some  protection while better shots are brewed. That stockpile is  expected to contain enough for 13 million people by year's  end, said HHS spokesman Bill Hall. 
The eventual goal is to stockpile enough for 20 million  people, roughly the number the draft plan designates to be  first in line. 
A bigger question is whether health authorities would be  physically able to vaccinate that many people - if there are  enough syringes stockpiled and plans for how to use them, said  Kim Elliott of the Trust for America's Health, a public health  advocacy group that tracks the preparations. 
"We can't just rest on our laurels that we have priority  groups in place. We have to think about how we get it into  arms," she said. 
The draft plan provides flexibility if the next pandemic isn't  as deadly as feared. A more severe outbreak requires more  aggressive rationing to protect critical workers, while a  milder one could allow for more widespread shots. 
 <URL> ? SITE=MABOC&SECTION=HOME&TEMPLATE=DEFAULT&CTIME=2007-10-22-17- 28-53 

