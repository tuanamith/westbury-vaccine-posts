



 <URL> / 
Boom in biodefense labs puts country at higher risk for dangerous disease outbreaks October 29th, 2007  
       * Biodefense facilities dangers cited 
       * 4 million Square feet of new biodefense research facilities coming online in the next few years. 
       * The Bush administration and federal agencies have given a green light to construction of 90 more acres of lab space, experts say  equivalent to about 36 Wal-Mart stores  to experiment with pathogens such as Ebola (see Related below - PW) 


Dallas Morning News | Oct 26, 2007  <URL>  
Boom in biodefense labs sparks security debate 
By EMILY RAMSHAW 

WASHINGTON  Since Sept. 11, the federal government has spent billions of dollars on research to protect the public from an invisible but devastating threat: biological attack. 
But a lack of supervision over the hundreds of labs and thousands of scientists now handling deadly germs  as demonstrated by recent problems at Texas A&M University  has put the country at higher risk for dangerous disease outbreaks than before 2001, federal investigators say. 
The labs are pretty much overseeing themselves at this point, Keith Rhodes, an investigator with the U.S. Government Accountability Office, said this month. I would have to say we are at greater risk today of an infectious disease epidemic. 
Biological weapons watchdogs say theres no end in sight to the biodefense research spree. 
The Bush administration and federal agencies have given a green light to construction of 90 more acres of lab space, experts say  equivalent to about 36 Wal-Mart stores  to experiment with pathogens such as Ebola, anthrax and the avian flu. 
With more researchers, more private labs and more university campuses across the country authorized to handle these diseases, they say, the chance of an epidemic, by mistake or by a rogue insider, is higher than that of a terrorist attack. 
Its like were building labs and hoping the germs will come, said Rep. Bart Stupak, the Michigan Democrat who chaired a congressional hearing this month on biodefense labs and the disease exposures at Texas A&M that helped bring national attention to the issue. 
Homeland Security officials, who lawmakers say turned down an invitation to testify at the congressional hearing, vehemently disagree. 
They acknowledge the federal program is growing fast and is divided among many agencies, but they say these layers provide greater oversight, not less. They and supportive lawmakers say the research, which rarely leads to accidental illness, is essential to protect the U.S. from real biological threats  those created by terrorists, and those existing naturally in the environment. 
The A&M breaches uncovered this spring  including the universitys failure to report one illness and several infections in its labs for more than a year  are unfortunate but isolated, officials say. A&M has had its research suspended pending safety upgrades. 
What you do is, you balance the risk, said Dr. John Vitko, the director of the Department of Homeland Securitys chemical and biological division. It is, in my mind, much more prudent to be prepared. 
The lab building boom kicked off in 2001, when mail-based anthrax attacks highlighted the governments limited research on biological agents. In response, Congress raised funding for biodefense research, and in particular, for high-security labs. Millions were authorized for construction in federal facilities, college campuses and the private sector. 
The goal was for these federal partners to develop vaccines and medical treatments for defense purposes, government officials vowed, not to create biological weapons. But they never specified how many labs were necessary. 
The result today is a cottage industry bankrolled by federal dollars. Opponents say it has become too easy to get money to study infectious agents. 
Since 2001, analysts say, the federal government has spent more than $16 billion on biodefense research and development  a tenth of it for construction of new labs. Though no official count exists, federal investigators estimate there are between 400 and 1,200 high-security labs operating in private and academic settings, many of them in or around major urban centers. 
Fifteen of those labs are Bio-Safety Level 4, or BSL-4?  the facilities equipped to handle the worlds most dangerous pathogens. Thats up from just five BSL-4 labs operating in 2001. A&Ms lab is a BSL-3, the next step down. In Dallas, UT Southwestern Medical Center has a BSL-3 lab. 
ENDGAME - Blueprint for Global Enslavement 
Elite Eugenics Plan to Exterminate 80% of Humanity 
--------------  <URL>  -------------- 
Critics concerns 
Currently, nearly 15,000 people in the U.S. are authorized to work with select agents  the most infectious pathogens overseen by the Centers for Disease Control. But they make up just a fraction of the researchers and lab workers studying other biological agents across the country. 
After 2001, some increase in the U.S. biodefense program was merited, said Edward Hammond, whose anti-weapons Sunshine Project uncovered the problems at A&M. But we have gone way too far, to the point that I believe that the most likely source of a bioterrorist event in the U.S. is a U.S. biodefense lab. 
In a report prepared for lawmakers this month by the GAO, Congress investigative arm, officials questioned 12 federal agencies involved with biodefense research  from the Environmental Protection Agency to the Department of Defense  to find out whether they tracked the growing number of infectious disease labs in the U.S. 

None did, the report said. 
Nor were any of the agencies solely responsible for determining how many more labs were necessary, monitoring the research performed there, or analyzing the risks associated with the burgeoning biodefense program. The findings have been confirmed by independent researchers. 
University of Maryland security studies expert John Steinbruner and his colleagues, who have pushed since 2003 for stronger oversight of biodefense research, say serious safety measures havent been a priority in this results-driven national program. The current system gives scientists virtually free rein with their experiments, they say, with few guidelines and even fewer consequences for their mistakes. 
Congress poured a lot of money at the problem without thinking this out in much detail, Dr. Steinbruner said. And people responded to that money on the table. 

Defending the labs 
Federal officials say while there may not be a single government body that oversees the countrys biodefense research, each of the 12 agencies plays a specific, designated role  whether its reviewing grant proposals, overseeing experiments and results or reporting lab accidents. 
We actually have a very formal process, said Dr. Vitko, the Homeland Security official. We do it in an interagency community  so that it is coordinated in a scientific sense. 
Dr. Vitko said every research proposal is reviewed at the front end to ensure that it is necessary and complies with international weapons treaties. Every experiment is monitored for safety and efficiency. And every individual handling the nations most dangerous agents is subject to criminal background checks and strict CDC supervision. 
If I come and say I want to do an experiment, it gets exposed to a lot of people  its not like you just go ahead and do something, Dr. Vitko said. 
The CDC has conducted more than 600 lab inspections since 2003 and referred nearly 40 lab operators to federal investigators for violating select agent regulations, said Dr. Richard Besser, the agencys director of terrorism preparedness. 
Officials with the National Institutes of Health, who are funding construction of new labs in the next few years, offer dramatically expanded biosecurity training, they say. 
And despite the fears surrounding biodefense research, U.S. biosafety experts say, public health risks are remarkably low. Of the 105 biosecurity breaches involving select agents reported to the CDC since 2003, only three involved lab worker illnesses. 
In spite of these things that youre seeing [at A&M]  the public and the environment have been protected, Dr. Vitko said. 
Austin bureau staffer Amy Rosen contributed to this report. 


BIODEFENSE LABS AT A GLANCE 
5 Top-security (BSL-4) U.S. biodefense labs before 2001 
15 BSL-4 labs today 
4 million Square feet of new biodefense research facilities coming online in the next few years 
16 Federal agencies that play a role in biodefense research or operate labs 
0 Federal agencies that keep track of how many U.S. biodefense labs there are 
14,400 Number of people in the U.S. authorized to work with CDC-protected select agents, the worlds most infectious diseases 
. . . 
Related 
Top Scientist Advocates Mass Culling 90% Of Human Population Fellow professors and scientists applause and roar approval at elites twisted and genocidal population control agenda A top scientist gave a speech to the Texas Academy of Science last month in which he advocated the need to exterminate 90% of the population through the airborne ebola virus. Dr. Eric R. Piankas chilling comments, and their enthusiastic reception again underscore the elites agenda to enact horrifying measures of population control.  <URL>  

Professors Kill 90% of Population Comments Echo UN, Elite NGO policies Alex Jones yesterday interviewed Dr Forrest M. Mims, III, the Editor of Citizen Scientist Magazine and Chairman of the Environmental Science Section of the Texas Academy of Science. The discussion centered around the controversial University of Texas professor who advocates the mass death of 90% of the worlds human population. Dr. Eric R. Pianka gave a speech to the Texas Academy of Science last month in which he advocated the need to exterminate 90% of the population through the airborne ebola virus. Piankas chilling comments, and their enthusiastic reception again underscore the elites agenda to enact horrifying measures of population control.  <URL>  

Dr. Death & The Religion Of Genocide Disciple of Pianka goes further, wants 100% of humans dead The elite have created a religion of genocide and the nations universities are the churches for the communication of an environmental jihad that threatens to decimate the human species as we know it. The furore surrounding the comments of Professor Eric R. Pianka, who told a crowd at Arlington UT that 90% of the world population need to be culled to solve overpopulation, have been characterized on the one hand by an expected outrage but on the other by a sycophantic mainstream media who have collaborated to spin the story and obfuscate the real issues.  <URL>  



------------------------------------------------------------------------------------ 
      Mike // Oct 30th 2007 at 5:29 pm 
      The recent outbreaks of Foot and Mouth in southern England were due to  genetically enhanced F&M viruses being flushed down the drain into watercourses from such a lab. It was blamed on a broken drain there and flooding in the area at the time spreading it far and wide.       Just as well they were not working on a doomsday virus at the time.     * 


      Ergohead // Oct 30th 2007 at 5:30 pm 

 <URL>      * 


      Donna // Oct 31st 2007 at 1:33 am 
      I live in Ct., a stones throw from Plum Island. Many in our state believe that Lyme Disease, Legionaires Disease, and who knows what else escaped from there. Possibly our government could stop poisoning us like guinea pigs. 



