


Parents claim religion to avoid vaccines for kids Vaccination fears fuel increase in exemptions sought, AP finds The Associated Press Updated: 1:24 p.m. PT Oct 17, 2007 
BOSTON - Sabrina Rahim doesn't practice any particular faith, but she had no problem signing a letter declaring that because of her deeply held religious beliefs, her 4-year-old son should be exempt from the vaccinations required to enter preschool. 
She is among a small but growing number of parents around the country who are claiming religious exemptions to avoid vaccinating their children when the real reason may be skepticism of the shots or concern they can cause other illnesses. Some of these parents say they are being forced to lie because of the way the vaccination laws are written in their states. 
"It's misleading," Rahim admitted, but she said she fears that earlier vaccinations may be to blame for her son's autism. "I find it very troubling, but for my son's safety, I feel this is the only option we have." 
An Associated Press examination of states' vaccination records and data from the Centers for Disease Control and Prevention found that many states are seeing increases in the rate of religious exemptions claimed for kindergartners. 
"Do I think that religious exemptions have become the default? Absolutely," said Dr. Paul Offit, head of infectious diseases at Children's Hospital in Philadelphia and one of the harshest critics of the anti-vaccine movement. He said the resistance to vaccines is "an irrational, fear-based decision." 
Risking an outbreak The number of exemptions is extremely small in percentage terms and represents just a few thousand of the 3.7 million children entering kindergarten in 2005, the most recent figure available. 
But public health officials say it takes only a few people to cause an outbreak that can put large numbers of lives at risk. 
"When you choose not to get a vaccine, you're not just making a choice for yourself, you're making a choice for the person sitting next to you," said Dr. Lance Rodewald, director of the CDC's Immunization Services Division. 
All states have some requirement that youngsters be immunized against such childhood diseases as measles, mumps, chickenpox, diphtheria and whooping cough. 
Twenty-eight states, including Florida, Massachusetts and New York, allow parents to opt out for medical or religious reasons only. Twenty other states, among them California, Pennsylvania, Texas and Ohio, also allow parents to cite personal or philosophical reasons. Mississippi and West Virginia allow exemptions for medical reasons only. 
in some cases doubled or tripled, in 20 of the 28 states that allow only medical or religious exemptions, the AP found. Religious exemptions decreased in three of these states - Nebraska, Wyoming, South Carolina - and were unchanged in five others. 
The rate of exemption requests is also increasing. 
For example, in Massachusetts, the rate of those seeking exemptions has more than doubled in the past decade - from 0.24 percent, or 210, in 1996 to 0.60 percent, or 474, in 2006. 
In Florida, 1,249 children claimed religious exemptions in 2006, almost double the 661 who did so just four years earlier. That was an increase of 0.3 to 0.6 percent of the student population. Georgia, New Hampshire and Alabama saw their rates double in the past four years. 

The numbers from the various states cannot be added up with accuracy. Some states used a sampling of students to gauge levels of vaccinations. Others surveyed all or nearly all students. 
Fifteen of the 20 states that allow both religious and philosophical exemptions have seen increases in both, according to the AP's findings. 
While some parents - Christian Scientists and certain fundamentalists, for example - have genuine religious objections to medicine, it is clear that others are simply distrustful of shots. 
Some parents say they are not convinced vaccinations help. Others fear the vaccinations themselves may make their children sick and even cause autism. 

Even though government-funded studies have found no link between vaccines and autism, loosely organized groups of parents and even popular cultural figures such as radio host Don Imus have voiced concerns. Most of the furor on Internet message boards and Web sites has been about a mercury-based preservative once used in vaccines that some believe contributes to neurological disorders. 
Spreading diseases Unvaccinated children can spread diseases to others who have not gotten their shots or those for whom vaccinations provided less-than- complete protection. 
In 1991, a religious group in Philadelphia that chose not to immunize its children touched off an outbreak of measles that claimed at least eight lives and sickened more than 700 people, mostly children. 
And in 2005, an Indiana girl who had not been immunized picked up the measles virus at an orphanage in Romania and unknowingly brought it back to a church group. Within a month, the number of people infected had grown to 31 in what health officials said was the nation's worst outbreak of the disease in a decade. 
Rachel Magni, a 35-year-old stay-at-home mother in Newton, Mass., said she is afraid vaccines could harm her children and "overwhelm their bodies." Even though she attends a Protestant church that allows vaccinations, Magni pursued a religious exemption so her 4-year-old daughter and 1-year-old son, who have never been vaccinated, could attend preschool. 
"I felt that the risk of the vaccine was worse than the risk of the actual disease," she said. 
Barbara Loe Fisher, co-founder and president of the National Vaccine Information Center, one of the leading vaccine skeptic groups, said she discourages parents from pursuing religious exemptions unless they are genuine. Instead, Fisher said, parents should work to change the laws in their states. 
"We counsel that if you do not live in a state that has a philosophical exemption, you still have to obey the law," she said. 
Even so, Fisher said, she empathizes with parents tempted to claim the religious exemption: "If a parent has a child who has had a deterioration after vaccination and the doctor says that's just a coincidence, you have to keep vaccinating this child, what is the parent left with?" 
Offit said he knows of no state that enforces any penalty for parents who falsely claim a religious exemption. 
"I think that wouldn't be worth it because that's just such an emotional issue for people. Our country was founded on the notion of religious freedom," he said. 
In 2002, four Arkansas families challenged the state's policy allowing religious exemptions only if a parent could prove membership in a recognized religion prohibiting vaccination. The court struck down the policy and the state began allowing both religious and philosophical exemptions. 
Religious and medical exemptions, which had been climbing, plummeted, while the number of philosophical exemptions spiked. 

In the first year alone, more parents applied for philosophical exemptions than religious and medical exemptions combined. From 2001 to 2004, the total number of students seeking exemptions in Arkansas more than doubled, from 529 to 1,145. 
Dr. Janet Levitan, a pediatrician in Brookline, Mass., said she counsels patients who worry that vaccines could harm their children to pursue a religious exemption if that is their only option. 
"I tell them if you don't want to vaccinate for philosophical reasons and the state doesn't allow that, then say it's for religious reasons," she said. "It says you have to state that vaccination conflicts with your religious belief. It doesn't say you have to actually have that religious belief. So just state it." 
URL:  <URL> / 


