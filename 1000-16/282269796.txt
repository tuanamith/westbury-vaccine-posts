


Job Title:     Prin Res Sci I - Bio Job Location:  NY: Pearl River Pay Rate:      competitive Job Length:    full time Start Date:    2007-09-05 
Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 
Job Summary 
The purpose of this laboratory-based position is to support early formulation development of vaccines through the use of analytical technologies and testing. The incumbent will leverage a variety of analytical technologies to understand the macromolecular behavior of vaccine candidates, which primarily consist of proteins, but may include nucleic acids, polysaccharide-protein conjugates, or viral particles.  The incumbent will have expertise in at least one of the following areas of protein analysis: analytical separations (HPLC,CE), spectroscopy (CD, Fluorescence, FTIR, UV, Light Scattering), or antibody-based in vitro Potency Assays (ELISA, Biacore, Luminex, ECL). The incumbent is responsible for writing/reviewing technical reports and protocols that describe the test methods, experimental results, or the operation of  laboratory instruments. Ensures the accuracy, completeness, and compliance of all data and tests results. 
Job Responsibilities 
*Working independently, the incumbent designs and executes experiments and test methods to understand the behavior of vaccine candidates in a variety of liquid formulations. Developed test methods for routine use will be qualified and potentially transferred to other staff or laboratories. Assay qualification is consistent with ICH guidelines. The incumbent trains other staff in new methods and analytical technologies. Tests nonGMP samples to support formulation development. Characterizes formulations through the application of analytical techniques. 
*Writes and reviews technical documents including, but not limited to, test methods, development reports, qualification reports, and experimental summaries. 
*Performs daily, weekly and monthly laboratory activities, such as daily and weekly monitoring of equipment, completion of equipment logbooks, periodic laboratory cleaning and managing the laboratory waste, checking PMO status of laboratory equipment and reporting discrepancies to management. Maintains all experimental data in laboratory notebooks and reviews data and laboratory notebooks of other analysts. 
*Satisfactorily completes all job related training in conformance with Departmental requirements. Where applicable, performs job responsibilities in compliance with GLP and all other regulatory agency requirements. 
*Performs other duties as assigned. 
Basic Qualifications 
*A Ph.D. Degree in Biology, Chemistry, Biochemistry or other area of Life Science. 
*Training or experience in a variety of analytical technologies as applied to macromolecules, especially proteins. Training in basic laboratory techniques, laboratory safety, computer operation and software, and technical writing skills. 
*Experience with analyzing biomolecule liquid formulations is necessary.  Experience with a variety of analytical technologies as applied to proteins is necessary. Skills in planning and organization and the ability to work with minimal supervision are required. Requires a minimum of 5+ years of relevant experience beyond the Ph.D. with at least three years experience in an industrial setting analyzing proteins. 
For more information and to apply online, please visit us at: www.wyeth.com/careers 
Wyeth is an Equal Opportunity Employer, M/F/D/V. 
Search Firm Representatives: 
Please Read Carefully. 


Please refer to Job code 17833 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



