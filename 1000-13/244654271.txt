


On Thu, 19 Apr 2007 20:33:13 -0500, " Death" < <EMAILADDRESS> > wrote: 


JULY 13, 2004 
LIAM SCHEFF ORPHANS ON TRIAL 

Art: Eugene Richards/Magnum Photos 


When I first spoke with Mona, she was stressed and nervous. Sean had twice been sent to the Incarnation Children's Center (ICC), a "home for HIV positive children" located in Washington Heights. First, as an infant, then again four years ago. And Dana was there until June. 
"Why did they take her?" I asked. 
"They said I was a negligent parent because I didn't want to give the drugs." 
Shed been taking them to a naturopath, and the children were healthy, but it didnt matter. When city agencies found out that the children weren't on the drugs, they took them away for mandatory treatment at a clinic and then transferred them to ICC. There, they were locked up and pumped full of drugs day and night. 
"What drugs?" 
"AZT, Nevirapine, Epivir, Zerit. All kinds of drugs." 
There's even a study on HIV-negative children born to HIV-infected mothers that uses an experimental HIV vaccine. 
Mona was never informed that Sean had once participated in clinical trials at ICC. 
"But they're always changing the children's medications," she said. 
I asked Mona how the children tolerate so many medications. 
Almost all of this is found on the warning labels. 
If you look in the medical literature, you'll find that neither of these assumptions is true. 
MONA'S SON SEAN has lived in a virtual coma his entire life. He was put on AZT in infancy. The drug made him so sick that he couldn't swallow solid food and, as a result, he ate through a tube in his nose until he was three. He had no energy. He was constantly ill. He couldn't play or even walk without becoming exhausted. Sean got sicker every time Mona gave him the drugs, so she cut down the doses. His energy level began to improve. She continued to wean him off the drugs and started taking him to a naturopath. 
The Administration for Children's Services (ACS) came down hard on Mona for not drugging him. She was sent to a new doctor, an AIDS specialist at Beth Israel, who put Sean on a "miracle drug," Nevirapine. Within six months, he was on life support due to organ failure. 
Mona showed me Sean's medical records. They told the same story: AZT, Nevirapine, the ICU. 
"Now they have Dana on the drugs." 
INCARNATION CHILDREN'S CENTER is housed in a four-story brick building, a converted convent with barred windows. At the entrance, there are glass panes on either side of a large, solid door with a camera above it. The day I went to ICC, there were children pushing up against the glass beside the closed door looking at me. 
Beyond the reception area was a large, dark room with stained-glass windows on the far back wall. Children were grouped around folding tables. 
Getting ready to leave, I noticed a girl with a bloated stomach. She was probably 12 or 13 years old. I looked downthere was a clear, hollow plastic tube curling out of her sweatpants. 
The thick, stale air was overwhelming, and it's then that I realized the windows were not only barred, but shut. 
"If they were open," Mona would later tell me, "the kids would try to get out." 
As I left, I again noticed the massive steel elevator door. According to Mona, it led to the clinic. 
Where do the kids come from? 
What's the current protocol for treating HIV-positive children? 
I read on your website that you're participating in clinical trials. What kind of trials? 
What kind of funding do you get for participating? 
Castro replied: "ICC appreciates your interest in our services but regretfully declines to participate in your project." 
I interviewed Dr. Painter for about an hour. Painter responded to my questions in extremely cautious, academic language. 
I asked her if ICC participated in clinical trials. 
I noted that the NIH clinical trial database listed hundreds of drug studies using children. 
"There are loads and loads of trials going on in children," she replied. 
But "for some cases," she said, "it's better administered through a g-tube." That's the stomach tube. 
The LTNPs I know are involved in health-supportive regimens, they avoid immune-damaging practices, foods and substancesincluding the AIDS drugs. 
And there it was, exactly what Dr. Rasnick had said: "AIDS doctors always assume that their patients are going to die." 
"No," she said. 
"But HIV tests cross-react with antibodies produced from drug abuse." 
"No," she protested. 
I looked at the other children. Same arms, same legs, same faces. One boy on half-crutches tried to dance to the music. His legs dangled beneath him, his feet at odd angles to the ground. I knelt by the boy in the wheelchair. He made a slight sound, like a panic deep inside trying to get out. I didn't want to alarm him, so I got up. 
Five months later, Mona saw Amir in the hospital. "My stomach is swollen; it got big," he told her. "They cut me, they cut me." He pointed to an incision on his side. 
"I think it's the tube," Mona told me. "I think it's infected." 
Something certainly failed with Amir. Two weeks after Mona saw him in the hospital, he was dead. 
Volume 17, Issue 28 
© 2007 New York Press 

