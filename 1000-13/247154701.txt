



"Jeff" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
Poor Jeff. 
 <URL>  
Study links hepatitis B vaccine to multiple sclerosis 

A study by researchers at the Harvard School of Public Health has linked hepatitis B vaccines with multiple sclerosis, with people receiving the vaccination shown to be at three times greater risk of developing MS over the following three years than those not inoculated, the British Medical Journal reports. The study may have particular significance for gay and bisexual men, who are urged to be vaccinated against the sexually transmitted hepatitis A and B viruses. The researchers analyzed the United Kingdom's General Practice Research Database, which contains data on more than 3 million adults, and found 163 cases of MS diagnosed between 1993 and 2001. About 6.7% of the adults with MS had received a hepatitis B vaccination; MS was detected in about 2.4% of patients in a control group that weren't vaccinated against hepatitis B. Previous studies by other researchers had shown either no link between MS and hepatitis B vaccines or a significantly lower risk than discovered by the Harvard team. 

The scientists say their findings don't necessarily mean that those at high-risk for hepatitis B infection should avoid vaccination. "Any decision concerning hepatitis B vaccination needs to take into account the large benefits derived from the prevention of a common and potentially lethal infection," the researchers say. A spokesman for the Multiple Sclerosis Society says that the Harvard study's finding could be taken "in the context of other recent research, which has shown no link between the vaccination and MS." The World Health Organization advocates for the inclusion of hepatitis B vaccines in routine childhood immunizations for children in every country. The Gay and Lesbian Medical Association and other gay health groups urge all sexually active gay and bisexual men to be vaccinated for both hepatitis A and B in order to prevent infection by the sexually transmitted diseases. Fewer than half of gay men at risk for hepatitis A and B have received the vaccines, according to the GLMA 


 <URL>  
 <URL>  
VACCINES  HEPATITIS-B HEP-B VACCINE,  an  UNJUSTIFIED  HEALTH  HAZARD 




 <URL> / 


 <URL>  

Until recently the vaccine was given in a three dose schedule - the first  dose usually started in the fall at the beginning of the new school term, a  second injected dose about a month later, and the third dose approximately 6 months later. In July 2000, The National Advisory Committee on Immunization (NACI) announced a revised schedule under the heading "The Statement on Alternate Adolescent Schedule for Hepatitis B Vaccine", published in the Canada Communicable Diseases Report - Vol. 26 (ACS-5), July 1, 2000. This brief  report announces that children age 11-15 will have available a two dose schedule to  be given in the school setting, and identifies the vaccine as Merck Frosst's Recombivax HB, but omits pertinent product information and does not disclose details of how the new 2 dose hepatitis B vaccine differs from the old 3  dose vaccine. The NACI statement can be viewed online at: 

 <URL>  






 <URL>  



