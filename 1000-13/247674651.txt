


Job Title:     Sr. Director Global Strategy Job Location:  PA: Collegeville Pay Rate:      competitive Job Length:    full time Start Date:    2007-05-31 
Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 

*Position is responsible for leading specific global strategies and tactics in support of Wyeth vaccines. The position will focus on working with affiliates to increase the utilization of Prevenar in targeted Asian emerging markets, including China, India, Thailand, Malaysia, and others.  Responsible for participation in the Global Brand Team and responsibility for meeting brand team objectives; act as liaison with key internal departments including PES, conventions, marketing planning, and others; and effective interactions with affiliates and regional personnel to communicate global strategy and assist in maximizing opportunities and performance for product. The position drives implementation of effective marketing plans in assigned emerging markets. Position responsible for establishing alliances with key advisors both internally and externally to determine the best marketing approach in light of key trends in the market. 
JOB RESPONSIBILITIES: 
*Collaborate with Market Research to determine research needs, ensure projects meet objectives and timelines to develop optimally positioned and branded products. In collaboration with Forecasting department and through the AFR process, develop/utilize patient based models to capture targeted patient population, potential uptake, ultimate number of patients treated, and competitive market share. 
BASIC QUALIFICATIONS: 
*BA/BS required. 
*Advanced degree preferred. 
*Bachelors degree with a minimum of 12 years pharmaceutical industry experience with at least 10 years product marketing experience is required or with an MBA minimum of 10 years pharmaceutical industry experience with at least 7 years product marketing experience. Sales and/or marketing experience preferred. Launch experience is a plus. 
*Successful candidate must have excellent oral and written communication skills, as well as presentation skills. Candidate should also be knowledgeable in financial management systems and ad agency operations. The position requires excellent organizational skills and the ability to manage multiple projects with tight deadlines in a fast paced environment. Collaboration skills are a must. Fluency in multiple languages and demonstrated to work across cultures is a plus. 

For more information and to apply online, please visit us at: www.wyeth.com/careers 
Wyeth is an Equal Opportunity Employer, M/F/D/V. 
Search Firm Representatives: 
Please Read Carefully. 


Please refer to Job code 16482 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



