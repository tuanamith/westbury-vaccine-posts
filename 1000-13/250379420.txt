


Job Title:     Sci II Bio Job Location:  NY: Pearl River Pay Rate:      competitive Job Length:    full time Start Date:    2007-06-13 
Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 
*This individual would assist in experiments to evaluate vaccine candidates for bacterial vaccine project(s).  The incumbent would develop and optimize ELISA specific for the vaccine antigens.  In addition, the individual would perform other in vivo and in vitro assays designed to test antibodies and antisera for biological activity.  These assays include OPA, bactericidal assays, FACS analysis and animal models of infection. 
JOB RESPONSIBILITIES :- 
*In general, assist with animal model studies designed to evaluate the efficacy of new vaccine candidates for bacterial project(s).  Develop and optimize ELISA and OPA assay for different vaccine antigens.  In addition, help in testing different glycoconjugates for immunogenicity in animal models.  Determine immune serum functionalities by measuring bactericidal and/or opsonophagocytic activities. 
*Present experimental results at group meetings, project meetings and department meetings.  Write monthly progress report. 
*Order lab supplies and other miscellaneous duties pertaining to the lab. 
*Satisfactorily completes all cGMP/GLP and safety training in conformance with Departmental requirements. 
*Where applicable, performs job responsibilities in compliance with cGMP/GLP and all other regulatory agency requirements. 
*All other job duties as assigned. 
BASIC QUALIFICATIONS :- 
*Bachelors degree and 4 years relevant experience, or a Masters degree and 2 years relevant experience. 
*Laboratory experience in a relevant field of science.  The incumbent will be trained on required Policies, Practices, and Procedures needed to support her work responsibilities. 
*Proficiency with personal computers including word processing, spreadsheets, PowerPoint and relevant scientific software is desirable.  Good verbal and written communication skills are required.  2 -- 4 years experience depending on education level. 
For more information and to apply online, please visit us at: www.wyeth.com/careers 
Wyeth is an Equal Opportunity Employer, M/F/D/V. 
Search Firm Representatives: 
Please Read Carefully. 


Please refer to Job code 16785 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



