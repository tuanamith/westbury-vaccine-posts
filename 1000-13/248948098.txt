


"A diet to ease the trauma of epilepsy", Telegraph, June 4, 2007, Link:  <URL> = /hepilepsy104.xml 
Sara and David Garland lost their daughter, Daisy, to epilepsy. They tell Cassandra Jardine how their tragedy grew into a charity helping some of the UK's 59,000 epileptic children 
Daisy Garland was 18 months old when her mother, Sara, started her on a strange new diet. 
The first meal consisted of mackerel in olive oil, a little kiwi fruit and goat's cream mixed with water and vegetable oil. " 'This is awful,' I thought as I gave it to her," says Sara. 
Three times a day, Daisy ate meals that would make even Atkins dieters feel queasy: omelettes with extra mayonnaise, lots of salmon and avocado, fatty frankfurters, cream, butter and olive oil galore, but very little carbohydrate and protein. 
The diet was so precise that even her toothpaste and sunscreen were sugar-free. 
"It was half scary, half exciting," says Sara, 43, who remembers vividly those early days when she would spend six hours calculating menus and weighing everything to the nearest gram. 
It was worth it. Within a week of starting the diet, she and her husband David noticed a change in Daisy, who suffered from epilepsy: "It felt like a veil was lifting. Previously she had had as many as 100 seizures, day and night, but her fits became fewer, shorter and less violent." 
The seizures had begun in October 1998 when Daisy was five months old, a couple of days after her second polio, diptheria and tetanus vaccination. 
Indeed a study published last year in Australia on vaccine links to Daisy's form of epilepsy - severe myoclonic epilepsy in infancy - has found an underlying genetic cause, with the first seizure possibly triggered by the rise in temperature following vaccination. 
Most of Daisy's seizures involved jerky movements, but she had many sorts: screaming seizures, tongue-biting incidents and others where she would stare into space. 
"They would come out of the blue - just when you were ready to go out," her mother remembers. 
"You never knew what state she would be in afterwards. Sometimes she was fine, but after longer ones - her longest was six and a half hours - she had headaches and had to rest. Epilepsy was stealing her childhood; it left her little time to play or learn, and each seizure may have inflicted some brain damage." 
Penny Fallon, consultant paediatric neurologist at St George's Hospital, South London, tried various anti-convulsant medications and a steroid on Daisy. 
The drugs only made her worse. "She was like a little zombie: drowsy and with little appetite. She also became aggressive and would stare into space and dribble," says Sara. 
It was then she decided to try the ketonic diet she had heard about. 
"Most doctors don't offer it because it is not a drug," she says. "No one knows exactly how it works and doctors like to understand what they are prescribing." 
"The NICE [National Institute for Clinical Excellence] guidelines are not to try the ketonic diet until two drugs have proved ineffective," says Fallon, who describes it as "not so much a diet as a treatment". 
The idea behind the ketonic diet - which Sara wants to make available to other children with epilepsy - is to mimic starvation. 
When the body is denied carbohydrates as fuel, it instead burns fat - either body or dietary. 
This produces ketones, by-products of fat metabolism that can affect the brain. 
While the exact biochemistry is not yet known, the effect of fasting on seizures was recognised by Hippocrates in the 5th century BC. 
It is safe only if prescribed by a dietician, who will devise a diet to put the body into ketosis while providing enough calories in the form of fat, as well as the protein, vitamins and mineral supplements needed for growth. 
It must be started in hospital, in case the child becomes hypoglycaemic, and under the supervision of a neurologist who can adjust medication and perform EEG (electroencephalogram) tests. 
Although Fallon pointed to possible side-effects, including anaemia, high cholesterol and kidney stones, she eventually gave in to the Garlands' pleas and Daisy was put under a dietician who impressed upon them that there could be no cheating. 
If Daisy had so much as a couple of raisins that weren't on the day's diet sheet, Sara found, the result would be seizures a couple of hours later. 
It was worth it, however, because over the next few months she became seizure-free, was able to stop taking medication and developed much faster. 
Just before she turned four, Daisy had the walking and talking skills of a two-year-old. Sara and David were beginning to have a life again, having formerly had to watch Daisy 24 hours a day. 
Then they had a tragic setback. After a routine test, Daisy was given antibiotics in hospital because she was thought to have E-coli. 
She didn't, but by the time that was known, Daisy had had a massive seizure and was so ill that she was not expected to last the weekend. 
She recovered, but it took a long time for her to become seizure-free again and she never regained either her balance or her speech. 
Two years later, she went to bed one evening and never woke up. Sudden death is a feature of severe myoclonic epilepsy, but the Garlands fear her system had been weakened by the antibiotics. 
They might have dwelt bitterly on the past but they wanted to create a memorial to their daughter, so, a few months after Daisy's death, they set up a charity bearing her name. In just two years - during which time their daughter Grace was born - they have raised =A390,000. 
As well as creating a website, a support group for parents of children with epilepsy, and a handbook on the ketonic diet, The Daisy Garland charity is funding specialists to make the diet more widely available to some of the more severe cases among the 59,000 children with epilepsy in the UK. 
Lee-Anne McHarry, a dietician at St George's, was their first part- time appointment in March 2006. 
She now helps 14 children, which is as many as she can manage because each one requires constant adjustments to their diet to reflect their age, stage and food preferences. 
"It's marvellous for their brains to be given a chance to develop," she says. 
"The diet can produce great changes in them - not just seizure reduction but behavioural changes. One child who was having screaming tantrums is now calmer and more alert." 
Younger children tend to do better on the diet than older ones, she finds, because parents have more control over what they eat. 
Some prefer the "classic" ketonic diet, others prefer one that substitutes medium-chain triglycerides (a pharmaceutical alternative) for naturally occurring fats. 
Both seem to work equally well, according to studies by the Institute of Child Health attached to Great Ormond Street Hospital. But, despite the growing evidence of their effectiveness, ketonic diets are not generally available to children with epilepsy because dieticians are already overstretched. 
"Unfortunately," says Dr Helen Cross, who is overseeing the research at the ICH, "there are only a handful of departments in the UK with the expertise and resources to deliver it." 
The Daisy Garland charity hopes to change that. The family moved to Totnes in Devon earlier this year, so the Garlands are hoping to place a second ketonic dietitian in a hospital somewhere in the West Country. 
To their great relief, Grace, who is now 18 months old, shows no sign of epilepsy, but Sara Garland's advice to parents who are going through what she experienced with Daisy is this: "Keep nagging your neurologist to be put in touch with a dietitian. Although the ketogenic diet is hard work, the results are amazing - and it soon becomes a piece of (sugar-free) cake." 
www.thedaisygarland.org.uk 


