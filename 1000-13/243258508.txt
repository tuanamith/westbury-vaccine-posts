


Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 
Description:   Company Information: At Wyeth, we discover, develop, manufacture and market innovative medicines that are leading the way to a healthier world. In a career at Wyeth, you will be an important part of a leading research and manufacturing organization. We currently have an opening in our Glassware/Media Department for a(n) Manufacturing Supervisor, at our Pearl River, NY facility (located on the Rockland County, NY/Bergen County, NJ border). 
As a Manufacturing Supervisor, you will provide supervision to hourly employees in the cleaning, set up, and sterilization of glassware and equipment for all manufacturing, Quality Control and development areas within WV to meet production demand for vaccine manufacture. You will be accountable for ensuring that the work performed by the hourly employees is accurate, timely, complete, and compliant with specifications, corporate polices, Standard Operating Policies (SOPs), current Good Manufacturing Practices (cGMPs) and FDA regulations. You will schedule work to meet varying customer demands on time. You will also perform investigations into deviations, exceptions, and errors through the use of the MIR system. 
You must have a BS degree in Sciences with 7-10 years of pharmaceutical experience in a manufacturing environment. You must also have knowledge of cGMPs and have good verbal and written communication skills. Additionally, you must have supervisory skills. 
Please send to attention of Wyeth. 

Please refer to Job code 13454 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



