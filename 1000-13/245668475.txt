


Job Title:     Senior Project Manager I Job Location:  MA: Andover Pay Rate:      competitive Job Length:    full time Start Date:    2007-05-21 
Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 
JOB SUMMARY: 
The Supply Partnership Site (SPS) is headquartered in Wilmington, MA, with Operations and Quality personnel in Collegeville, Pearl River-Blue Hill, Bloomington, IN, McPherson, KS, Stockholm Sweden, and Ravensburg, Germany.  Within SPS the Drug Product PPU has responsibility for the contract manufacturing of biotech products (biopharma and vaccines).  SPS-DP is organized into product cells.  This position is in the Oncology Cell.  The cell is responsible for two commercial products (Mylotarg and Neumega) and one development product (CMC-544). Duties and Responsibilities: Manage relationship with contract manufacturers.  The objective is to assure a productive relationship resulting in effective and responsive communications and efficient operations for the contractor and Wyeth as it relates to our drug products. 
Ensure successful and timely manufacturing operations for the product(s) and/or contractor(s) assigned. This will include the tracking and follow-up of all key production related activities. Manage the overall project(s) or process(es). This requires, but is not limited to a clear understanding, communication, and agreement of the project/product requirements and deliverables. Tracking and managing of all tasks, completion dates, and Wyeth project team members. 
Manage open production issues that impact the project/process. Take the lead role in assessing and resolving Contractor process deviations. Coordinate, support, and where assigned, manage, the Quarterly Meetings. Establish and implement Joint Service Procedures. Manage change control. 
Support product and process technology transfer to Contract sites, along with completion of associated qualification, validation, and certification activities. Assist in specifying and qualifying, and responsible for ordering new product/process equipment for Contract sites. 
Assists with formal audits of Contract sites as part of the audit team. 
Other responsibilities will include day to day support of manufacturing operations using Wyeth business systems (e.g., SAP, LabWare, TrackWise, etc.) 
Requirements: 
BS degree in a Life Science discipline or equivalent experience, along with a minimum of 10 years experience in a drug product manufacturing environment, preferably aseptic fill/finish and lyophilization.  Strong analytical and computer skills (Windows, MS Word, Excel, Project). Excellent communication, organizational, and interpersonal skills are required. 
Travel (approx. 25 -- 30%) is required. Quality Assurance/Quality Control background is a plus. 
For more information and to apply online, please visit us at: www.wyeth.com/careers 
Wyeth is an Equal Opportunity Employer, M/F/D/V. 
Search Firm Representatives: 
Please Read Carefully. 


Please refer to Job code 16172 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



