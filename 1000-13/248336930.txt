


Earl Ubell, 80, Veteran Science Reporter BY ADAM BERNSTEIN - The Washington Post May 31, 2007 URL:  <URL>  


Earl Ubell, who died yesterday at 80, covered the leading  health and science breakthroughs of the post-war age with a  lively and effective style. Ubell, who had a physics degree, worked as science editor at  the old New York Herald Tribune from 1953 until the paper  folded in 1966. While there, he won an Albert Lasker medical  journalism award for his series of articles about heart  attacks. 
He later became a science reporter and news director at  broadcast network affiliates in New York and spent many  years simultaneously as health editor at Parade magazine. He  also wrote several books for juveniles and one aimed at  adults called "How to Save Your Life" (1973). 
His stories often tended to feel like features instead of  hard news. He began his front-page account of the first  Sputnik flight in October 1957 this way: "Our planet has a  new moon tonight." This is often cited as one of his best  opening lines, which he considered amusing because the story  itself was cobbled together at the last minute. 
When the news broke, Ubell was at a conference at the Soviet  embassy in Washington honoring the International Geophysical  Year. He immediately went about the conference asking Soviet  scientists and bureaucrats about the space launch. 
The official Soviet newspaper Izvestia later attacked as "a  demented savage" when Ubell wrote that the satellite was  invading American territory. 
While at the Herald Tribune, Ubell interviewed physicist  Albert Einstein and wrote about Jonas Salk's work on polio  vaccine and Watson and Crick's discovery of the structure of  DNA. He was one of the early chroniclers of sex researcher  Alfred Kinsey. 
He detested many conventional journalism practices,  including the "inverted pyramid" style of news writing that  places the key information up high and becomes increasingly  less urgent. 
"It says, 'The more you read me, the less interesting I  get,' " he told an interviewer. 
Stuart H. Loory, a former Herald Tribune colleague who later  became a CNN executive, called Ubell "an enfant terrible who  did not like being pushed around." 
Earl Ubell was born June 21, 1926, in Brooklyn, N.Y., to  Russian-Jewish immigrants. He spoke Yiddish until he went to  school. 
He joined the Herald Tribune as a messenger in 1943, then  rejoined the staff as a reporter after returning from Navy  service during World War II. He received a bachelor's degree  in physics from City College of New York in 1948. 
As a newspaperman, he became an authority on X-ray  crystallography, a technique to view atomic and molecular  structures and was invited to study at Nobel laureate Linus  Pauling's lab at the California Institute of Technology. 
Besides a stint in the mid-1970s as news director at  WNBC-TV, he spent the remainder of his career as science  editor at WCBS-TV. He retired in 1995, having completed a  two-part series about his struggle with Parkinson's disease. 
With his wife, he co-founded the Center for Modern Dance  Education, a nonprofit community arts school near his home  in Hackensack, N.J. 
Survivors include his wife of 58 years, Shirley Leitman  Ubell of Hackensack; two children, Lori Ubell and Michael  Ubell of Oakland, Calif.; three grandsons; and four  great-grandchildren. 



