


-----BEGIN PGP SIGNED MESSAGE----- Hash: SHA1 
New Admission: Sarin Nerve Gas Caused Some Gulf War Illness 
Via NY Transfer News Collective  *  All the News that Doesn't Fit   [The numbers in the studies quoted seem incredibly small, given the large numbers of vets with Gulf War illness. As AHRP notes, it's been 15 years. And yes, dioxin caused illness in Vietnam vets.]  
ALLIANCE FOR HUMAN RESEARCH PROTECTION (AHRP) - May 17, 2007 Promoting Openness, Full Disclosure, and Accountability www.ahrp.org and   <URL>    

After a decade and a half of dismissive denials by government officials and scientists who are beholden to government and industry for funding, a new study and other research provide "converging evidence that some gulf war veterans experienced nervous system damage as a result of service, and this is an important development in explaining gulf war illnesses." 
The new study, to be published in the journal, NeuroToxicology, found "evidence that a low-level exposure to sarin nerve gas--the kind experienced by more than 100,000 American troops in the Persian Gulf war of 1991--could have caused lasting brain deficits" furthermore, the study found a dose relationship between degree of brain changes and higher levels of exposure. financed by the Department of Veterans Affairs and the Centers for Disease Controls. 
The lead scientist of the new study, Dr. Roberta White (Boston University School of Public Health), examined the brain scans of 26 Gulf War veterans--half of whom were exposed to the gases. The researchers found that troops with greater potential exposure had less white matter.  
Furthermore, "In a companion study, the researchers also tested 140 troops believed to have experienced differing degrees of exposure to the chemical agents to check their fine motor coordination and found a direct relation between performance level and the level of potential exposure. Individuals who were potentially more exposed to the gases had a deterioration in fine motor skills, performing such tests at a level similar to people 20 years older." 
Yet, even as the converging evidence confirms veterans' claims linking their disabilities to nerve gas, the New York Times reports that "The Pentagon has not decided whether to inform veterans about the possibility of a link between exposure and brain damage."  Dr. Michael Kilpatrick, deputy director of the Force Health Protection and Readiness of the Defense Department said he "did not believe that his department would send letters to potentially exposed veterans alerting them of it." 
Why do citizens--in this case military troops who served their country--be denied care for incapacitating injuries incurred during their service? This case is an illustration validating critics' well placed mistrust in pronouncements by "authoritative" government officials and stakeholders in the medico-health care industry. More often than not, those who claim they have been harmed by a pathogen, a defective drug, vaccine, or medical device will eventually be proven right--but often it's too late. 
Contact: Vera Hassner Sharav 212-595-8974  <EMAILADDRESS>  
                           *** 
The New York Times - May 17, 2007   <URL>   
Gas May Have Harmed Troops, Scientists Say  
By IAN URBINA 
WASHINGTON, May 16 - Scientists working with the Defense Department have found evidence that a low-level exposure to sarin nerve gas - the kind experienced by more than 100,000 American troops in the Persian Gulf war of 1991 - could have caused lasting brain deficits in former service members. 
The report, to be published in the June issue of the journal NeuroToxicology, found apparent changes in the brain's connective tissue - its so-called white matter - in soldiers exposed to the gas. The extent of the brain changes - less white matter and slightly larger brain cavities - corresponded to the extent of exposure, the study found.  
White matter volume varies by individual, but studies have shown that significant shrinkage in adulthood can be a sign of damage.  
The study was led by Roberta F. White, chairman of the department of environmental health at the Boston University School of Public Health. Dr. White and other researchers studied 26 gulf war veterans, half of whom were exposed to the gases, according to a Defense Department modeling of the likely chemical makeup and location of the plume. The researchers found that troops with greater potential exposure had less white matter.  
Phil Budahn, a spokesman for the Department of Veterans Affairs, said the research required further examination. 
Dr. White said she did not describe her study as inconclusive, though she said it would be accurate to call it preliminary.  
But the new research, Dr. Steele said, used previously nonexistent brain scanning technology to, essentially, "look into the brain to evaluate the difficult-to-characterize problems affecting gulf war veterans."  
Thus, she said, it is "the first to demonstrate objective indicators of pathology in association with possible low-level sarin-cyclosarin exposures."  
On May 2, after learning about the research, Senators Patty Murray, Democrat of Washington, and Christopher S. Bond, Republican of Missouri, wrote the Defense and Veterans Affairs Departments, asking about their plans for outreach and expanded benefits for exposed troops.  
The Pentagon has not decided whether to inform veterans about the possibility of a link between exposure and brain damage. 
The impact of the study was limited, Dr. Kilpatrick said, because it did not establish a direct causal connection between sarin exposure and gulf war illnesses, and it depended on Defense Department data that was at best an estimate and at worst a guesstimate of exposure levels by troops.  
Copyright 2007 The New York Times Company                                          * ================================================================  NY Transfer News Collective    *    A Service of Blythe Systems            Since 1985 - Information for the Rest of Us            Search Archives:  <URL>   List Archives:    <URL> /  Subscribe:  <URL>  ================================================================ 
-----BEGIN PGP SIGNATURE----- Version: GnuPG v1.4.7 (FreeBSD) 
iD8DBQFGTXkNiz2i76ou9wQRAkThAKDB4cyMNvgdT0+taNBM0u4+cbV51QCaAvVn tgWbfWtJKvCUWJQy6flo5xk= =izIP -----END PGP SIGNATURE----- 


