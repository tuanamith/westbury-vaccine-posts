


Job Title:     Director Job Location:  NY: Pearl River Pay Rate:      competitive Job Length:    full time Start Date:    2007-05-16 
Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 
*The primary purpose of this job is to direct activities in the Vaccine Research Early Phase Programs area headed by Dr. K. Jansen related to new vaccine programs. Emphasis will be on bacterial vaccine programs. Job requires strong background in bacterial research and early development. Candidate needs to provide leadership and guidance in research and early vaccine program developmental activities. Job requires experience and established track record in working with multifunctional teams. 
JOB RESPONSIBILITIES :- 
*Provide scientific leadership and oversight to bacterial programs with an emphasis on ï¢-hemolytic strep program. 
*Serve as scientific advisor to early vaccine project teams and functional teams. 
*Conduct scientific research on defined early vaccine programs and provide guidance in the effective development of early vaccine programs. 
*Evaluate and recommend new vaccine programs using both external collaborations and/or internal resources 
*Evaluate new technologies and novel approaches to vaccine development; play an active role in the evaluation of potentially new vaccine programs. 
*Facilitate collaborations both within group as well as between all functional groups involved in the early development of vaccines. 
*Play an active role in the design of epidemiology studies to support vaccine programs through internal and external collaborations. 
BASIC QUALIFICATIONS :- 
*Ph.D. in Biological Sciences with emphasis on bacterial research; experience in anti-infective field required. 
*At least 15-18 years postgraduate experience; vaccine research and development experience a must; obvious scientific excellence through sound publication record.  Proven track record of track record in working with multifunctional teams. 
*Postdoctoral as well as &#62; 5 year experience in industrial setting required; management experience within and across functions.  Clear and concise communication skills (written & verbal).  Additionally, candidate must possess a broad perspective on the operations surrounding early phase developmental vaccine programs and be focused. 
For more information and to apply online, please visit us at: www.wyeth.com/careers 
Wyeth is an Equal Opportunity Employer, M/F/D/V. 
Search Firm Representatives: 
Please Read Carefully. 
Please send to attention of Wyeth. 

Please refer to Job code 16187 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



