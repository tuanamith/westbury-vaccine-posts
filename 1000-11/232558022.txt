


"However, Bush administration officials cautioned against drawing sweeping conclusions from the study." 

Of course they cautioned that.  They have a vested political interest is pushing this particular issue.  The Republicans are the party of moralizers, family values, etc, this is inherent with the bourgeoisis.  It is inherent that they lie generally, both to others and themselves. 
****  
Study: Abstinence programs no guarantee 
POSTED: 0636 GMT (1436 HKT), April 14, 2007 
 Students who took part in programs found to be just as likely to have sex   Findings give boost to advocates of comprehensive sex education   Congress to mull renewing the block grant program Title V this summer   Administration officials: Don't draw sweeping conclusions from the study Adjust font size: 
WASHINGTON (AP) -- Students who took part in sexual abstinence programs were just as likely to have sex as those who did not, according to a study ordered by Congress. 
Also, those who attended one of the four abstinence classes that were reviewed reported having similar numbers of sexual partners as those who did not attend the classes. And they first had sex at about the same age as other students -- 14.9 years, according to Mathematica Policy Research Inc. 
The federal government now spends about $176 million annually on abstinence-until-marriage education. Critics have repeatedly said they don't believe the programs are working, and the study will give them reinforcement. 
However, Bush administration officials cautioned against drawing sweeping conclusions from the study. They said the four programs reviewed -- among several hundred across the nation -- were some of the very first established after Congress overhauled the nation's welfare laws in 1996. 
Officials said one lesson they learned from the study is that the abstinence message should be reinforced in subsequent years to truly affect behavior. 
"This report confirms that these interventions are not like vaccines. You can't expect one dose in middle school, or a small dose, to be protective all throughout the youth's high school career," said Harry Wilson, the commissioner of the Family and Youth Services Bureau at the Administration for Children and Families. 
For its study, Mathematica looked at students in four abstinence programs around the country as well as students from the same communities who did not participate in the abstinence programs. The 2,057 youths came from big cities -- Miami, Florida, and Milwaukee, Wisconsin -- as well as rural communities -- Powhatan, Virginia, and Clarksdale, Mississippi. 
The students who participated in abstinence education did so for one to three years. Their average age was 11 to 12 when they entered the programs back in 1999. 
'Two-part story' 
Mathematica then did a follow up survey in late 2005 and early 2006. By that time, the average age for participants was about 16.5. Mathematica found that about half of the abstinence students and about half from the control group reported that they remained abstinent. 
"I really do think it's a two-part story. First, there is no evidence that the programs increased the rate of sexual abstinence," said Chris Trenholm, a senior researcher at Mathematica who oversaw the study. 
"However, the second part of the story that I think is equally important is that we find no evidence that the programs increased the rate of unprotected sex." 
Trenholm said his second point of emphasis was important because some critics of abstinence programs have contended that they lead to less frequent use of condoms. 
Mathematica's study could have serious implications as Congress considers renewing this summer the block grant program for abstinence education known as Title V. 
The federal government has authorized up to $50 million annually for the program. Participating states then provide $3 for every $4 they get from the federal government. Eight states decline to take part in the grant program. 
'Clear signal' 
Some lawmakers and advocacy groups believe the federal government should use that money for comprehensive sex education, which would include abstinence as a piece of the curriculum. 
"Members of Congress need to listen to what the evidence tells us," said William Smith, vice president for public policy at the Sexuality Information and Education Council of the United States, which promotes comprehensive sex education. 
"This report should give a clear signal to members of Congress that the program should be changed to support programs that work, or it should end when it expires at the end of June," Smith said. 
Smith also said he didn't have trouble making broader generalizations about abstinence programs based on the four reviewed because "this was supposed to be their all-star lineup." 
But a trade association for abstinence educators emphasized that the findings represent less than 1 percent of all Title V abstinence projects across the nation. 
"This study began when [the programs] were still in their infancy," said Valerie Huber, executive director of the National Abstinence Education Association. "The field of abstinence has significantly grown and evolved since that time, and the results demonstrated in the Mathematica study are not representative of the abstinence education community as a whole." 
The four programs differed in many respects. One was voluntary and took place after school. Three had mandatory attendance and served youth during the school day. All offered more than 50 hours of classes. Two were particularly intensive. The young people met every day of the school year. 
Common topics included human anatomy and sexually transmitted diseases. Also, classes focused on helping students set personal goals and build self-esteem. The young people were taught to improve communication skills and manage peer pressure. 


