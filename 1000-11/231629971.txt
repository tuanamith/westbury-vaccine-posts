



"Jeff" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
And congenital rubella syndrome, a huge tragedy, has become almost unheard  of, thanks to vaccination: 
The number of reported cases of CRS in the United States has declined 96% 
from 77 cases in 1970 to 3 cases in 2001.1-3 Two of the three infants with  CRS 
reported in 2001 had foreign-born mothers. Between 1990 and 2001, 121 cases 
of confirmed CRS were reported to the National Congenital Rubella Syndrome 
Registry. Of the 118 cases with known import status between 1990 and 2001, 
33 (28%) were imported. 
Despite routine rubella vaccination among children, some rubella outbreaks 
continue in the U.S. These outbreaks are primarily confined to groups who 
traditionally refuse vaccinations and to adults from countries without a  history of 
or with recently established routine rubella vaccination programs.  Throughout 
the 1990s, the majority of infants with CRS were infants of mothers who fall  into 
these categories. 
Though rubella cases are at record-low levels in the United States, rubella  and 
CRS continue to be global burdens. It is estimated that there are more than 
110,000 cases of CRS annually throughout the world. With the increased use  of 
rubella vaccine; however, the burden of rubella infection should decrease in  the 
future. As of April, 2000, 52% of countries use rubella vaccine in their  national 
programs. 
 <URL>  



