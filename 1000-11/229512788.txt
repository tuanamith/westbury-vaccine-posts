


In article < <EMAILADDRESS> >,  Omelet < <EMAILADDRESS> > wrote: 

Ps: 
WHY DOGS ARE BETTER THAN MEN 
Dogs do not have problems expressing affection in public. Dogs miss you when you're gone. Dogs feel guilt when they've done something wrong. Dogs don't criticize your friends. Dogs admit when they're jealous. Dogs are very direct about wanting to go out. Dogs do not play games with you--except Frisbee (and they never laugh at  how you throw). Dogs don't feel threatened by your intelligence. You can train a dog. Dogs are easy to buy for. You are never suspicious of your dog's dreams. The worst social disease you can get from dogs is fleas. (OK. The *really* worst disease you can get from them is rabies, but  there's a vaccine for it, and you get to kill the one that gives it to  you.) Dogs understand what no means. Dogs understand if some of their friends cannot come inside. Middle-aged dogs don't feel the need to abandon you for a younger owner. Dogs admit it when they're lost. Dogs aren't threatened if you earn more than they do. Dogs mean it when they kiss you. 
  
HOW DOGS AND MEN ARE ALIKE 
Both take up too much space on the bed. Both have irrational fears about vacuum cleaning. Both are threatened by their own kind. Both mark their territory. Both are bad at asking you questions. Both have an inordinate fascination with women's crotches. Neither does any dishes. Both pass gas shamelessly. Neither of them notice when you get your hair cut. Both like dominance games. Both are suspicious of the postman. Neither knows how to talk on the telephone. Neither understands what you see in cats. 
  
HOW MEN ARE BETTER THAN DOGS 
Men only have two feet that track in mud. Men can buy you presents. Men don't have to play with every man they see when you take them around  the block. Men are a little bit more subtle. Dogs have dog breath all the time. Men don't shed as much, and if they do, they hide it. And the number one reason dogs fall short... It's fun to dry off a wet man. --  Peace, Om 
Remove _ to validate e-mails. 
"My mother never saw the irony in calling me a Son of a bitch" -- Jack Nicholson 

