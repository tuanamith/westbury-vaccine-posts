


David Friedman 
James A. Donald 
Jonathan L Cunningham 
Evil is usually irrational, at least when it is done  floridly enough to be entertaining.   None of the  really enormous real life crimes committed by  spectacular real life evil overlords, Pol Pot, Tojo,  Stalin, Hitler, Mao, etc, were committed out of rational  self interest.  A good pick pocket might rationally pick  pockets out of self interest, and a despot might squeeze  the poor in order to build large palaces to contain his  numerous concubines out of rational self interest, but  serial killers and mass murderers generally have the  finest of reasons for their activities. 
It seems likely that Genghis Kahn acted out of rational  self interest, but we have not seen his like for a long  time.  Rational evil, though theoretically possible,  seems fairly uncommon in practice. 
To do evil on a really large scale, one needs to work in  concert with others, but it is generally unwise to work  in concert with evil people.  Thus even if the evil  overlord is acting rationally, (and he usually is not)  his minions are seldom acting rationally.  Thus for  example Stalin and Ho Chi Minh predictably eradicated their former comrades in their communist parties, and Pol Pot was arguably in the process of doing the same when he fell. 
James A. Donald 
Jonathan L Cunningham 
Evil as defined to small children in usual stories, or  as defined to slightly older children in comic books.  Someone is evil if, like Snow White's stepmother, they  have a propensity to harm others for frivolous reasons.  Such conduct is usually irrational, for it eventually  gets others serious pissed at you, thus the evil  stepmother derangedly attends the wedding, and the  prince executes her by forcing her to dance in red hot  shoes. 
If someone acts out of rational self interest, there is  not a lot of room for them to be impressively evil, for  the weakest man can kill the strongest. 

This distinction is fairly meaningless:  When communists  hold a peasant's child in the fire to force the mother  to reveal where the seed corn was buried, in one sense  their intentions were extremely good.  Remarkably good.  Insanely good.  Their intentions were indeed *insanely*  good.  They showed an extraordinary willingness to not  only sacrifice the peasant for the greater good, but to  sacrifice *themselves* for the greater good, a  willingness I fail to observe amongst today's  Christians, despite the fact that Christians supposedly  believe such willingness will be rewarded in heaven.  In  one sense communists don't really want to harm the  peasant, just re-educate her out of her false  consciousness about property rights, but these peasants  are just so strangely stubborn.  In another sense, they  want grain to finance the revolution, for war costs  money, and they don't care who they hurt in order to get  it. 

This hypothetical is uninteresting:.  Supposing a  reasonable man believes that the vaccine is safe, then  the usual arguments about herd immunity apply, and so  the usual arguments for compulsion apply.  I don't agree  with those arguments, and think the conclusions  dangerous, and might well lead to the scenario you  envisage, and therefore people should not be so easily  persuaded by such arguments for compulsion, or even free  vaccination, but the kind of evil we have seen during  the twentieth century is based on errors more  transparently maniacal and acts of coercion more  blatantly evil. 
The archetype of evil in the twentieth century is not a  vaccination program that goes wrong, but a communist  holding a child in the fire in order to coerce the  mother for the noblest and highest of reasons. 

This is mere reasonable error - error plus power does  indeed result in evil, but the evil gets considerably  more evil when the errors are not  reasonable, but  maniacal.  
 <URL> /      James A. Donald 

