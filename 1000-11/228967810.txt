


This one just infuriated me.  As an autistic tax-paying citizen since the age of 13, I find this to be a huge insult. 
How many other disabilities do you hear whined about as being such a financial burden as autism? 
ok...here's the article. 
 <URL> = tory/specialScienceandHealth/home 
Autism a lifelong burden, study shows Because few adults with the disorder can work, the economic costs continue By: ANDR=C9 PICARD 

Each child diagnosed with autism will accrue about $3.2-million (U.S.) in medical and non-medical costs over his or her lifetime, according to a new study. 
The most expensive components are lost productivity and adult care, not medical costs and behavioural therapies that so many parents crave for their children, the research shows. 
"Although autism is typically thought of as a disorder of childhood, its costs can be felt well into adulthood," said Michael Ganz, an adjunct professor in the department of society, human development and health at Harvard University in Cambridge, Mass., and the lead author. 
Earlier research estimated that autism costs the U.S. economy about $35-billion annually, and the Canadian economy $3.5-billion. 
The new study, published in today's edition of the Archives of Pediatric & Adolescent Medicine, takes the analysis further, examining how these costs are incurred throughout the lifetime of a person with autism. 
The study showed that direct medical costs were high in the first five years of life, at about $35,000 annually. That is when most behavioural therapies are offered. The indirect costs to parents, in large part due to lost income, are also quite high in the childhood and adolescent years, averaging $43,000 annually. 
But, as a person with autism ages, the costs of non-medical care soars, and so do productivity losses. 
Because there are programs for autistic adults, these are paid out-of- pocket, with costs averaging $27,500 a year. Many people with the disorder do not work and their parents still need to provide care, factors that translate into economic losses averaging $52,000 annually, according to the research. 
Susan Bryson, who holds the Jack and Joan Craig Chair in Autism Research at Dalhousie University in Halifax, said the research is important because it draws attention to adults with autism. 
"The data we have tell us that only about 5 per cent of adults with autism are self-supporting," she said. Yet there is a paucity of adult programs, and this creates a life-long burden for families." 
Dr. Bryson said it is not clear if investing in behavioural therapy like ABA/IBI in childhood will ultimately result in adult autistics who are more able to be independent and to work. 
"But we need to ask the question: Does investment early on have significant economic benefits later?" Dr. Bryson said. 
Dr. Ganz said recognizing that a child diagnosed with autism today may become an adult who is unable to work and who requires specialized adult care should awaken parents to the need for financial planning. 
"Parents of children with autism should seek financial counselling to help plan the transition into adulthood," he said. 
To conduct the research, Dr. Ganz looked across the lifetime of a hypothetical group of individuals born in 2000 and diagnosed with autism in 2003. 
Only costs directly related to autism were included, with no medical or non-medical costs incurred by individuals with or without autism included. 
Autism and related conditions are development disabilities known under the catch-all term autism spectrum disorders. 
They have become increasingly common in recent years. 
The soaring numbers are due to a combination of factors, including greater awareness, changes in diagnostic criteria and, perhaps, more children being born with the disorder. 
The Autism Society of Canada estimates the incidence rate in this country is one in every 286 births. The condition is about four times more likely in boys than girls. 
It is not clear what causes autism spectrum disorders, but there are many theories, including exposure to environmental toxins, diet, a malfunctioning immune system and paternal age. 
(There are those who also believe that autism can be caused by a mercury derivative that used to be found in childhood vaccines, but that theory has repeatedly been debunked.) 


