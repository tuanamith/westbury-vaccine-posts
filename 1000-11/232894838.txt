


On Apr 12, 2:28 am, Dale Kelly < <EMAILADDRESS> > wrote: 
We think we do. 

I know otherwise. Now what? 

Quadraplegics will be happy to hear this. 

The external world is more trustworthy than the internal fantasy life we all harbor to some degree. Those who think themselves free of error are almost certain to be wallowing in self-deception. 

Chaotic? Random? It doesn't bother me that there may be a small degree of random noise in my brain, but I don't really fell freer because of it. 

Sure we are. 

No we're not. 
See, anybody can make assertions. Repeating them does not make them more compelling. I refer you to 30 years of brain science which has determined (heh) that different kinds of data processing are done by discernable subsystems, that different kinds of stimulation of the brain (magnetic fields; tiny, pinpoint direct application of electrical current; injuries; drugs, etc.), differnt animal species show similar brain activity when doing the same sort of processing (e.g. mirror cells). And there has been no persuasive evidence of minds outside of working brains. Nor any proposed mechanism. 

Lots of stuff. Go to the closet university library and browse. But not in the New Age section, please. 

This will disappoint logicians everywhere. Why would deduction require "knowledge of the whole", or induction "knowledge of all the parts"? Please justify this. 
Show your work. 

This is not coherent. 

No, we don't. And hypothoses are not "leaps of faith", they are reasonable models proposed to explain a set of facts. They are testable. 

How would you do any analysis of variance on a T. Rex fossil? 
Abiogenesis is still a group of hypothoses; we'll let you know when they come up with a good theory. 

True, true. If science actually *worked, we'd have functioning computers, airplanes that fly, influenza vaccines, probes photographing other planets, genetic engineering, artificial diamonds, treatments for high LDL cholesterol... 
Oh, wait. 

Kermit, who gave up pipe dreams decades ago in order to get stuff done. 


