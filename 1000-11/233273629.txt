


-----BEGIN PGP SIGNED MESSAGE----- Hash: SHA1 
Michael Moore takes 9/11 Workers to Cuba for Health Care 
Via NY Transfer News Collective  *  All the News that Doesn't Fit   [Michael Moore is, of course, known for a number of films before "Bowling for Columbine," especially "Roger & Me," a labor film about General Motors in Flint, Michigan. He has also written a number of books and has long been a vocal opponent of the US economic and  political war against the Cuban Revolution. -NY Transfer] 
Michael Moore's website and mailing list are available at:  <URL> / 

Herald Sun (Australia) - Apr 17, 2007  <URL>  
Michael Moore takes 9/11 Workers to Cuba for Health Care 

FILMMAKER Michael Moore has caused a furore by taking ailing Ground Zero emergency workers to Cuba to show the US health-care system is inferior to Fidel Castro's. 
The trip by his production company was reportedly filmed as part of the director's latest documentary, Sicko. 
The film is an attack on US drug companies and private health insurance firms, which Moore hopes to show at Cannes Film Festival next month. 
Two years in the making, the flick also takes aim at the shortfall in medical care being provided to people who worked on the toxic World Trade Centre debris pile. 
The movie, which critics say uses ill 9/11 workers as pawns, has angered many in the emergency services community. 
"He's using people that are in a bad situation and that's morally wrong," said Jeff Endean, a former SWAT commander who spent a month at Ground Zero and suffers from respiratory problems. 
Moore made his name with the controversial social documentaries Bowling for Columbine and Fahrenheit 9/11. 
A spokeswoman for Weinstein Co, the film's distributor, would not say when it would hit cinemas or provide details on the film or the trip. 
Emergency workers were told Cuban doctors had developed new techniques for treating lung cancer and other respiratory illness, and health care in the communist country was free, according to those offered the two-week February trip. 
Cuba has made recent advancements in biotechnology and exports its cancer treatments to 40 countries around the world, raking in an estimated $120 million a year. 
In 2004 the US Government granted an exception to its embargo against Cuba and allowed a California drug company to test three cancer vaccines developed in Havana. 
Some ill 9/11 workers baulked at Moore's idea. 
"I would rather die in America than go to Cuba," said Joe Picurro, a New Jersey ironworker approached by the filmmaker via an e-mail that read, "Joe and Mike in Cuba". 
After helping remove debris from Ground Zero, Mr Picurro has a long list of respiratory and other ailments so bad that he relies on fundraisers to help pay his expenses. "I just laughed. I couldn't do it," he said. 
Another ill worker who said he was willing to take the trip ended up being stood up. 
Michael McCormack, 48, a disabled medic who found an American flag at Ground Zero that once flew atop the Twin Towers, was all set to go. 
"What he (Moore) wanted to do is shove it up George W's rear end that 9/11 heroes had to go to a communist country to get adequate health care," said Mr McCormack, who also suffers from chronic respiratory illness. 
But Mr McCormack said he was abandoned by Moore. 
At a March fundraiser for another 9/11 responder in New Jersey, Mr McCormack learned Moore had gone to Cuba without him. 
"It's the ultimate betrayal," he said. "You're promised that you're going to be taken care of then you find out you're not. 
"He's trying to profiteer off of our suffering." 
Not everyone was unhappy. Some who went reportedly said they "got the Elvis treatment". Moore's publicist did not return calls. 
© Herald and Weekly Times. 
                            *** 
VivirLatino - Apr 16, 2007  <URL>  
Michael Moore Goes to Cuba 
Michael Moore of Fahrenheit 9/11 fame (or infamy) is at it again. His latest film brought him and some 9-11-01 emergency workers to Cuba, but not to play where's Fidel. Moore took those workers to get medical treatment in Cuba. But it's all not big heartedness on Moore's part. It's all part of his soon to be released documentary Sicko. 
Two years in the making, the flick also takes aim at the shortfall in medical care being provided to people who worked on the toxic World Trade Centre debris pile.  
Not surprisingly the film has garnered controversy already with some saying Moore is using the 9-11-01 workers as pawns in his own anti-Bush political game. Others say he is making a string point and proving it by showing how inferior the U.S. health care system is to that of say oh a third world Latin American socialist country. 
The actual reaction from the 9-11-01 rescue workers was mixed. Some said they were treated well, like Elvis. Other rescue workers who didn't go have responses ranging from feeling abandoned by Moore to feeling used. 
Personally I have known people who have gone to Cuba to seek medical care because it was actually cheaper to make the trip out to Cuba to get medical treatment so Moore's premise isn't as far fetched as it seems. 
Via / The Herald Sun                                         * ================================================================  NY Transfer News Collective    *    A Service of Blythe Systems            Since 1985 - Information for the Rest of Us            Search Archives:  <URL>   List Archives:    <URL> /  Subscribe:  <URL>  ================================================================ 
-----BEGIN PGP SIGNATURE----- Version: GnuPG v1.4.7 (FreeBSD) 
iD8DBQFGJAoJiz2i76ou9wQRArL2AJ4gotvqKkvaihia1mkKc4fdICGdBACfTc8I Kbrw1FG/8EYqEfARcMe+LFs= =VAnd -----END PGP SIGNATURE----- 


