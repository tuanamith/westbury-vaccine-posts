


PeterB wrote: 
What you snipped, which is quite meaningful: 
This is a good common-sense article about chelation and autism by Dr  Michael Fitzpatrick, author of MMR and Autism: What Parents Need to Know. 
 <URL>  

I expecially liked: 
"Parent activists challenge mainstream scientific expertise with the evidence of their own experience and with the results of their own painstaking researches. But both these sources of knowledge may be misleading and relying on them may have damaging consequences for children with autism and their families. The experience of having a child with autism - as I have - qualifies you to speak authoritatively on your experience as a parent of a child with autism: it does not give you any particular insights into the science of autism. Indeed, one of the problems of being the parent of a child with autism is that it gives you little time or energy to study the wider aspects of the subject. In recent years, however, some parents have devoted much time to reading scientific  papers on autism. But, when such parents demand to be heard - and are heard - in scientific controversies it is important that the limitations of parental experience and study are recognised. 
"Modern scientific knowledge in any discipline is complex and highly specialised. The professional understanding of research scientists and clinicians is the product of a long process of study, training and experience. Such knowledge and expertise cannot be acquired through reading journals, downloading information from the internet and attending  occasional conferences. At best, parents can acquire a 'narrow-band competence' that may allow them to select information supporting some pre-conceived conviction, and presenting this may be effective for campaigning purposes. But a narrow and selective approach can lead to the sort of dogmatic  outlook expressed by the anti-vaccine campaigners, which is inimical to scientific inquiry and discussion." 
This applies not only to parents, but to adults with diseases that  frustrate them. A "narrow and selective approach" to published knowledge of a disease process, combined with a distrust of science and medicine can lead  people to support some false and dangerous notions of the causes and appropriate treatments. 

Yes, the best you can do is an ad hominem. 




