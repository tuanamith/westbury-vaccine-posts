


Robert wrote: 
I argued no such thing.  I asked for data showing that non-specific immunity is less common than specific immune response in cases of viral exposure, as you claim.  I presume you would if you could, therefore you obviously can't. 

It's consistent with the fact there are six billion people in the world, most of them never vaccinated.  It's hard to argue with success. 

Most people don't get disease.  Those who do are usually immuno-supressed, nutritionally deprived, starving, living in squalor, or genetically vulnerable.  You're right, of course, that most people exposed to a virus develop antibodies, but that isn't the point.  The point is that most people don't experience catastrophic illness, with or without vaccine. 

And that happens without vaccine, which is how you and everyone else got here. 

Which only shows that evolved immunity works; the fact it doesn't save everyone isn't because they lack vaccine, it's because they lack the beneficial metabolic effects of optimal nutrition and a clean environment.  The jest about the "surgery being a success but the patient being a failure" applies here.  You can't reduce human health to an anti-body titre. 

Like I said, it's only a guess. 

I'm sure it's fun saying that.  At some point, perhaps you will actually address the points I'm raising. 

When did I say that?  The point is that non-specific immunity effectively defends the majority of people by reducing the viral insult sufficiently that natural antibody production, when needed, is also effective.  Vaccine has nothing to do with it. 


I didn't say it was. 

Correct.  A normal functioning immune system does this with or without vaccine. 

Evolution is a remarkable thing. 

This is my argument about vaccine. 
<snipped for focus> 

Natural antibody production is crucial when the assault is substantial.  I never said it wasn't.  The point is that host immunity adequately protects us, otherwise we wouldn't be having this conversation. 

Nice anecdote, but irrelevant. 

Is that why 100,000 people in the US are dying each year as a direct result of prescription medication?  Have you looked at the vaccine mortality and morbidity data in VAERS?  Have you considered CDC's statement regarding 80-90% under-reporting of such ADRs? 

How nice. 

You should read more outside the newsgroups.  <URL>  

If the patient survives, immunity was successful regardless.  If the patient dies, that's an immune response failure.  What I'm saying is that vaccine is not the key. 

As I said at the beginning, natural immunity is FAR more complex than vaccine-stimulated antibody production.  Each layer of host immunity is essential for our survival. 

That's why all layers of evolved immunity are superior to artifically stimulated antibody production from vaccine. 


It's easy to find associations between rain and dancing, all you need is a tamborine. 


By "successful antibody response," I am referring to real-world resistance to disease, not antibody titres.  Adjunctive labwork is nice, but it isn't a foundation for the promotion of vaccine. Correction -- it *shouldn't* be the foundation for promotion of vaccine.  In the absence of double-blind, controlled, and long-term studies, we just don't know how effective vaccine is.  There is evidence to suggest it may even alter disease epidemiology in such a way that it make viral illness more dangerous to other demographics. 

Saying it doesn't make it so. 

Good science isn't easy.  If you prefer wearing blinders, be my guest. 

An ipso facto.  Tally-stroking health surveys don't prove vaccines are the magical potions you like to think. 

Immunity has been around for billions of years.  Vaccines are a little more recent than that. 

As I've said, host immunity is both non-specific and specific.  The point is that not all layers are needed in every instance. We need all layers depending on the assault, of course. 

A non sequitur, since most people don't drive Masseratis either.  Is that why they get sick? 

Nutrient deprivation was a serious health issue for pre-modern europeans.  It continues to be the world's most serious health risk today.  When people are nutritionally deprived, they get sick. Medicine anesthesizes our pain, but it doesn't cure disease.   
PeterB 


