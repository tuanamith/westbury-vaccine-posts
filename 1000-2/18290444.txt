


WASHINGTON (CNN) -- U.S. President George W. Bush will unveil a national strategy for bird flu on Tuesday, in a bid to reduce the chance that an outbreak among people could become widespread, White House Press Secretary Scott McClellan said. 
The strategy -- to be unveiled at the National Institutes of Health in Bethesda, Maryland -- will include plans to identify an outbreak as soon as it appears, work to contain it and treat it "to the best extent possible," McClellan told reporters Monday. 
The announcement by Bush comes as disaster coordinators from Pacific rim countries discuss ways to head off such a crisis. (Full story) 
One way to contain the virus would be to devise a cell-based vaccine against it and to stockpile antiviral medicines, two efforts that are currently under way, McClellan said. 
By the end of the year, more than 4 million courses of antiviral treatment should be on hand, though that number is far lower than what the World Health Organization has urged. 
In addition, scientists are redoubling their efforts to create the next generation of vaccine, one that would be cell-based rather than the current egg-based versions, McClellan said. Cell-based vaccines could be mass-produced quickly, which could prove to be a critical advantage in the event of a pandemic. 
"It's something we need to take seriously," he said. "That's why the president has been leading the way." 
The bird flu virus H5N1 has circulated largely among flocks in Asia, though it has spread to birds in 16 countries, including Russia and parts of Europe. In all, more than 140 million birds have been killed to stem its spread, which scientists blame on migratory birds. 
Thailand on Monday reported its 20th human victim of the disease since 2003, and the third this year. (Full story) 
On Monday, Canada said it had discovered a strain of H5 avian flu in wild birds, but was still testing to see if it was the N subtype. 
So far, the disease does not appear to infect people easily, with the WHO having tallied just 121 human cases. But, unlike other forms of influenza, it has a high mortality rate -- 62 of those cases proved fatal. 
Health experts fear that it could mutate and acquire the ability to infect large numbers of people. Should that happen, without immediate and effective interventions to contain it, the global impact could be incalculable. 
There have been three pandemics in the past century, and global health experts say the world is overdue for another. 
Garden-variety influenza causes about 36,000 deaths per year in the United States and is responsible for an annual total cost of more than $12 billion, according to a draft of the strategy, obtained by CNN. 
A pandemic of bird flu "could dwarf this impact by overwhelming our health and medical capabilities, potentially resulting in hundreds of thousands of deaths, millions of hospitalizations, and hundreds of billions of dollars in direct and indirect costs," said the draft, dated October 24. 
The Bush administration has sought about $70 million in next year's budget for mobile hospitals that could be set up in affected areas to handle large numbers of patients, McClellan said. 
On April 1, Bush added influenza viruses with pandemic potential to the list of diseases against which a quarantine can be imposed. 
More than 80 nations have joined an international partnership based at the United Nations to fight the disease, should it emerge as a human threat. 
Bush discussed the issue Monday in his luncheon meeting at the White House with Italian Prime Minister Silvio Berlusconi, McClellan said. 
Stockpiling drugs and vaccines is just one component of the U.S. plan, The Associated Press reports. 
"Understand that a lot of the things we need to do to prepare are not related to magic bullets," said Michael Osterholm of the University of Minnesota, AP reported. 
Osterholm is an infectious disease specialist who has advised the government on preparations for the next worldwide flu outbreak but has not seen the final version of the plan. 
How to provide food supplies, everyday medical care for people who don't have the super-flu, basic utilities and even security must be part of the plan, Osterholm and others have counseled the Bush administration. 
The U.S. government already is buying $162.5 million worth of vaccine against the H5N1 bird flu strain from two companies -- Sanofi-Aventis and Chiron Corp. It also is ordering millions of doses of Tamiflu and Relenza, two antiflu drugs believed to offer some protection against the bird flu. 
Lawmakers angry at months of delay have already given Bush money to begin those preparations: $8 billion in emergency funding that the Senate, pushed by Democrats, passed on Thursday. 
The money is to be spent at the president's discretion, but senators said it should be used both for medications and vaccine and for beefing up hospitals and other systems to detect and contain a super-flu. 


