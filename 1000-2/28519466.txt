


 <EMAILADDRESS>  wrote: 
Therein lies the reason why Lamarck's theory was not taken seriously - it  posited a miracle. And this explains why Darwin's was - it didn't. The  internal impetus required by Lamarck involved something like water finding its  level, except there was no equivalent notion analogous to density to explain why. 
It can - you can read Lamarck and say "he must be referring to the ramping up  effect of selection on molecular variation". It sort of works (but isn't  consistent with the species-wide change he required), but it's anachronistic  to do so. Not that this has stopped a lot of Lamarck boosters, particularly in  francophone nations at the turn of the 20thC. 
If the feu ethere was of a uniform nature in all individuals, then it would  result in the same outcome. He wrote (part III, chapter V) 
"I have said that the subtle fluids which move in the interior of these living  bodies cut out special routes which they continually follow, and thus begin to  establish definite movements which become habits. Now if we reflect that  organisation develops as life goes on, me shall understand how new routes must  have been cut out, increased in number,: and progressively varied for the  furtherance of the movements of contraction; and how the habits to which these  movements give rise then become fixed and irresistible to a corresponding extent." 
For "imperfect animals" he thought this force was external to them, but in  "perfect animals" it was also interior, and so they directed their own  evolution somewhat. 
No. He was a pure materialist, and did not require that there was any  oversight. Understand that at this time the notion of there being a *natural*  hierarchy of types was widely held. You need to put yourself into the mindset  of the time to see how this might have applied. 
You are importing notions like mutation, heredity, selection and so on that  Lamarck was simply unaware or unconcerned with. But this line applies to the  neo-Lamarckians like Cope, Packard, Hyatt and Ryder in the US at the end of  the 19thC. 
Go here:  <URL>  
Well that is about words, not the world. And I'm a philosopher. We don' need  no steeking evidence. 
Nice point. Although it's an illicit inference, it's good sophistry :-) 
*sob* So I'm the research assistant? A glorified librarian? 
It's basically Dawkins' primary claim to fame - if we view the evolution of  organisms as the travails of genes, their "strategies" and "interests", he  says, then we understand evolution. The opponents of that view, such as Gould,  treat genes merely as bookmarkers. 
No, the metaphor is not wrong. It happens that game theory mathematics (which  while cast in terms of rational players is entirely independent of that  metaphor) applies very nicely to evolutionary processes. Since it is the math  not the metaphor that counts here, we can safely use the metaphor to get the  ideas across - such as Prisoners Dilemma games, Stag Hunt games, and so on. 
One thing that the math does is force us to think clearly about what the  "interest-bearers" are in evolution - Dawkins said genes, of course, but a  good many others think this is wrong, and as a result Multi-Level Selection  Theory has developed. 
Yes, but you haven't escaped the metaphor - "strategies"? I quote (from ailing  memory) Michael Ghiselin - "The only strategists in evolution are evolutionary  biologists. Carrots do not ponder their reproductive fitness" :-) 
Nevertheless it is still a useful way to conceptualise the matter. 
Indeed. 
No, because "rational" here has a technical meaning in game theory - a player  that seeks only its maximum benefit. An irrational player is one who commits  the gambler's fallacy, for instance. A rational player follows only the  probabilities. It has no intentional connotations. 
Not at all. We *know* that the virus will evolve into less virulent forms if  we reduce the likelihood of it being transmitted. We *know* that if we keep  the birds in lower number this will result. We *know* that if we keep the  birds isolated from humans and other livestock (NB: Humans are livestock for  this exercise) that the likelihood of cross infection reduces. In short, we  can do this without technological solutions. Whether we can do it economically  is another matter. 
Sure. But zoonooses usually cross over via closely contacting species, not via  wild populations as such. And we can control both the presence of wild  scavengers like rats through hygienic conditions, and the proximity of  domestic birds. 
I said and implied no such thing. Vaccines are a good thing, but they are  strain-specific and easily outevolved by a pendemic disease. Likewise  antivirals - it would take less than a few thousand generations to evade these  unless they were (improbably) 100% effective on the first go. Hygiene,  however, is always effective because it deforms the adaptive landscape of the  pathogen. How many strains are likely to evolve in any influenza virus? It will be on  the order of trillions. Each vaccine or GM solution will cover at best a few  thousand strains. 
So your solution is to eradicate wild populations, with unforeseen effects on  ecology, rather than try to minimse the contact had between likely reservoirs  of diseases and humans? I don't think this is any improvement. It might work  in the case of something like malaria, although it turns out not to have so  far despite millions of tons of DDT being used even today. But it won't work  for most possible zoonoses. We have to figure out a way to sidestep them. 
No. We should try all kinds of solutions, and use those which are overall the  best. But there's no magic bullet, and there never will be, in virtue of the  nature of life. Your solution, of eradicating the sources, would leave us in a  depauperate environment that would be so fragile it would kill us all anyway. Hygiene. See - you're getting it. 

Pigenons aren't the source of bird flu - chickens are. But if they became the  source I'd happily recommend eradicatinng them where they are a foreign species. 
It takes millions of generation *of the virus*, but this can happen in  decades. It's already happening with HIV. You're getting there, grasshopper ;-) 


