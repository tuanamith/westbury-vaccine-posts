


Nanotechnology May Help Treat Cancer By EMMA ROSS, AP Medical Writer Tue Nov 1, 5:18 PM ET 
Experiments on mice have shown promise for the future of nanotechnology in treating cancer - research that could bring doctors a step closer to using the technology to release cancer-killing drugs inside tumors while leaving the rest of the body unscathed. 
After seeing how some mice were cured of human prostate cancer with the technology, cancer specialists at the European Cancer Conference in Paris said Tuesday they had high hopes for its future application. 
"There are a lot of candidates for intelligent carriers and these nanoparticles are among them," said Dr. Gordon McVie, a professor at the European Institute of Oncology in Milan, Italy, who was not involved with the research. 
"This is a new system, and the more systems we have, the better, because we'll probably be lucky if we get one system to work out of 10," he said. "It looks as if it could be quite good." 
Dr. David Kerr, a professor of clinical pharmacology and cancer therapeutics at Oxford University in England who also was not connected with the research, said the approach may have the edge on others. Previous designs of nanoparticles have used antibodies to zone in on cancer cells. 
"The body's immunodefense system can create antibodies to the therapeutic antibodies, deactivate them and prevent the antibody binding to the right cancer cells. This looks like a step forward," Kerr said. 
Nanotechnology is the science of manipulating matter smaller than 100 nanometers and taking advantage of properties that are present only at that level, such as conductivity. A nanometer is one-billionth of a meter, or about one-millionth the size of a pin head. The prefix comes from "nanos," the Greek word for dwarf. 
Nanotech has been around for several decades, but only now is its potential starting to be realized. Medicine is expected to be one of the fields to benefit most from the technology. In cancer, it is hoped the technology will allow for more precisely targeted drugs and surgery and less toxic chemotherapy. 
The study, done by scientists at Harvard Medical School and the Massachusetts Institute of Technology, involved engineering nanoparticles embedded with the cancer drug Taxotere. The particles were then injected into human tumors created from prostate cancer cell lines and implanted into the flanks of mice. The mice were watched for 100 days. 
The technology being tested involves a nanoparticle made of a hydrogen and carbon polymer with bits of drug bound up in its fabric and attached to a substance that homes in on cancer cells. The polymer gradually dissolves, exposing the nuggets of drug little by little. 
The mice were divided into five groups, including one that had their tumors injected with ineffective saltwater. A second group died after injections of a nanoparticle containing no drugs. 
Another group was given one shot of the drug, experienced an initial decrease in tumor size and then suffered a strong rebound. They also died. 
Other mice were injected with a nanoparticle-encased drug, but one that was not designed to specifically target cancer cells. 
"What happens here is the lymphatic system of the tumor can take it up and wash it away, because the nanoparticle is not targeted to the cancer cells," said the study's presenter, Dr. Omid Farokhzad of Harvard Medical School. The tumor initially shrank to half its original size, but then rebounded. 
In a final group of mice, scientists injected the targeted nanoparticles containing the drug. 
"The tumor completely disappeared," Farokhzad said. 
Injecting targeted nanoparticles into the bloodstream and having them seek out tumors and get inside on their own is the ultimate goal, but direct injection is also promising for cancers where the tumor is accessible and hasn't spread, such as early prostate cancer, Farokhzad said. He said his group hopes to test the approach in prostate cancer patients within two years. 
Kerr said he doubted that direct injection of tumors would turn out to be a useful treatment in itself. "Cancer tends, almost from the outset, to be a systemic disease," he said. "This is only one design step toward what ultimately must be a systemic treatment." 
He said there may be a use for the direct injection of nanoparticles to deliver vaccines to tumors. The idea is that the cancer can be vaccinated against itself, meaning the immune system would then destroy the cancer in other parts of the body. 


