


******************************************************** 
 <URL>  Discerning good sweeteners from bad ones [aspartame, etc], Rebecca Wood, Mail Tribune, Medford, Oregon: Murray 2005.11.10 
 <URL>  
November 9, 2005 
Discerning good sweeteners from bad ones BY REBECCA WOOD    For the Mail Tribune  Medford, Oregon 
If you're one of the many people who suffer from sugar cravings (as did I for decades), you can free yourself. I have, and so have many of my students. Here's what we've discovered. 
When a meal satisfies, you don't nibble on carbohydrates between meals. When, however, you skip a meal, or you eat sweets that are empty of other nutrients, then you'll feel empty and reach for something to fill the hole.   It's predictable. 
So your first step in freeing yourself from the sugar teeter-totter (and to prevent diabetes) is to, as your mother admonished, eat three good meals a day. 
This takes some doing as packaged foods and restaurant meals often fall far short. Because their denatured ingredients lack oomph, they can't deeply satisfy. Furthermore, most are laced with cheap sugars or -- even worse -- with faux sweeteners that actually goad your cravings for more sweets. 
Let's examine the quality sweeteners so that, as part of a good diet, you can enjoy a great cookie -- and stop there. In order to skillfully navigate through marketing hype to discern the good sweeteners, I'll also mention the ones to avoid. 
But first, it's useful to reflect that no sweetener is a whole food; each has had something removed to concentrate its sugars. Nectar is refined by bees into honey, maple sap is concentrated into maple  syrup, and cane's fiber and nutrients are removed to make table sugar. 
Your opportunity is to avoid the empty ones and to delight in the minimally refined sweeteners that retain their trace minerals and rich flavor. 
CANE SUGAR: Worldwide, the most common sweetener is table sugar (sucrose), which is typically extracted from sugar cane. The most wholesome cane products have their trace nutrients intact. Two such domestically available cane sugars, Rapadura and Sucanat, are 90 percent crystalline sucrose enveloped in 10 percent mineral-rich cooked cane juice. Of the two, I find Rapadura more flavorful and satisfyingly sweet. 
Rapadura and Sucanat are available in quality food stores and on line. (In import stores they're available as jaggery, gur, piloncillo or panela.) Use them cup for cup as you would sugar in any recipe. Whole sugars imbue a buff to tan color and wonderful flavor to your finished dish. 
In comparison, white sugar is 99.85 percent sucrose and tastes harsh, cloyingly sweet and one-dimensional. Chemically pure sucrose, like any drug, plays havoc in your system. Also, avoid the numerous highly refined cane products that attempt to convey a healthful image but are as drained of nutrients as is white sugar. (An exception is blackstrap molasses, the mineral-rich by-product of sugar cane production.) 
CANE SWEETENERS MASQUERADING AS "HEALTHFUL": Typically, the following cane products are refined to pure sucrose and then "painted" with a little molasses to lightly color and flavor the sugar. 
They are NOT recommended because they lack trace nutrients. Unfortunately, "natural," "whole" and "unrefined" are open terms without legal protection. 
Brown Sugar Demrerara Evaporated Cane Juice Florida Crystals Muscovado Naturally Milled Organic Cane Juice Organic Plantation Milled Sugar Organic Whole Cane Sugar Raw sugar Sugar-in-the-Raw Turbinado Unrefined Cane Juice Whole Cane Yellow-D Honey 
Take away the water from honey and it's about as sugary as white sugar. Honey does, however, retain nearly all of the flower nectars' original nutrients. In comparison to table sugar, it is "minimally" refined. 
Favor local, unpasteurized, wild flower honey. Buying locally supports the local economy and, if you have pollen allergies, may decrease your allergic response to pollen. Unlike pasteurized honey, raw honey is not mucus-forming and it retains its medicinal properties (it helps ease constipation and fluid retention, and, according to Oriental medicine, tones the pancreas). 
Because of wild flowers' genetic diversity, honey gathered from wild blossoms is superior in flavor and essence to cultivated crops, like clover or orange. At the Rogue Valley Grower's Market, I buy honey from Wild Bee Honey Farm in Eagle Point. You can reach them at 826.7621. 
Their blackberry honey has a wonderful fruity aroma and taste. Whereas, their poison oak honey is deeply sweet but leaves an almost persimmon-like tingle on my tongue. Given my druthers of a poison oak rash or its antidote, I say, "Yes!" to the tingle in a spoonful of honey. 
Other Sweeteners 
As with sugar, your guideline for judging quality sugar alternatives is to favor those which are minimally refined and higher in trace nutrients. Because the following are higher in fructose and/or maltose, some people regard them as more healthful than white sugar. 
With all purchases, read labels carefully and bypass any product that contains cheap additives like corn syrup, high fructose corn syrup, dextrose or artificial sweeteners. 
Agave Syrup The sap of a cactus-like desert plant, agave, is a remarkably abundant source of fructose (70%). This ranks it low on the glycemic index and makes it a healthy sweetener for non-insulin dependent diabetics. Thanks to agave syrup's pleasant sweetness, versatility and moderate price, it is quickly becoming a popular food and beverage sweetener. 
Maple and Birch Syrup Two excellent and delicious sweeteners are concentrated sap from maple and birch trees. The sap is collected and its water is reduced (historically by evaporation, today by reverse osmosis). While maple syrup comes from northeastern United States, birch syrup is produced in Scandinavia and Alaska. Both are energy intensive and therefore pricey --  it takes 40 gallons of maple sap, or 80 gallons of birch sap, to make one gallon of pure syrup. 
Grain Sweeteners Any grain can be malted into sweet, maltose-rich syrup. Enzymes digest the grains' complex carbohydrates into a more simple sugar.   Barley malt and rice or sorghum syrups are the most common. 
(Corn syrup and high fructose corn syrup are pure hydrolyzed products, devoid of trace nutrients and are not recommended. They're cited by some nutritionists as leading causes of obesity.) 
Artificial Sweeteners - Not Recommended 
The non-caloric sweeteners are a chemical rather than a food -- please avoid them. Please note: When doing on-line research about their toxicity, you'll find conflicting data. The manufacturers' web pages claim their safety; numerous alternative publications report otherwise. 
Of the artificial sweeteners, xylitol has been highly marketed as a "healthy" sweetener. Xylitol is dangerous -- even life-threatening --  for pets, according to the American Society for the Prevention of Cruelty to Animals. A byproduct of the plywood industry, xylitol is a pure crystalline chemical, or hydrogenated polyol. 
Yes, data correlates xylitol with the reduction of dental caries, however there are more wholesome ways of preventing tooth decay. 
Neither do I recommend Acesulfame-K (Sunette, Sweet & Safe, Sweet One), Aspartame (Equal, Canderel and NutraSweet), cyclamates, saccharine or sucralose (Splenda). 
Rebecca Wood is an award-wining cookbook author and local cooking teacher. To ask a question, to find a recipe or, or to post your comment about this article, visit: www.RWood.com 
Recipe: Gluten-Free Shortbread Cookies 1 cup white or brown, short-grain rice (or 11/3 cup rice flour) 1 teaspoon baking powder ¼ teaspoon sea salt 8 tablespoons (1 stick) unsalted butter ½ cup Rapadura or Sucanat (or other quality sugar) 2 large egg yolks 1 teaspoon vanilla extract Heat oven to 350 F. Grease cookie sheet. Grind rice in a flour or nut or seed mill (or use 11/3 cup rice flour). Stir in baking powder and salt. Set aside. Cream butter and Rapadura until light and fluffy. Add yolks and vanilla and beat to incorporate. Fold in dry ingredients until blended. Pinch off pieces of dough and roll between palms of hands to form ¾ -inch balls. Set the balls 2 inches apart on a prepared cookie sheet. Flatten the cookies by pressing into each with the tines of a fork or with two fingers. Bake for 10 to 12 minutes or until the bottom is lightly browned and the cookies are lightly colored. The cookies are fragile when hot and so carefully transfer to a cooling rack (or allow to cool on the cookie sheet). VARIATION: For raspberry tartlets, spread a cookie with raspberry jam (warm the jam if necessary to facilitate spreading). Arrange fresh raspberries on cookie. Makes 2 dozen cookies. NOTE: When I want an exceptionally tender sugar cookie, I make this shortbread using rice flour and whole cane sugar. You may purchase rice flour or grind your own in a flour or nut or seed mill. When making your own rice flour, use only short-grain brown or white rice (curiously enough, long-grain rice flour yields a soggy, coarse crumb). 
Copyright © 1997-2005 Mail Tribune, Inc. All rights reserved. ******************************************************** 
 <URL>  
Mail Tribune  P.O. Box 1108  111 N. Fir St.  Medford, OR 97501 541-776-4411   1-800-366-2527 
Newsroom   (541) 776-4477  Fax (541) 776-4376 Do you have a story idea?   <EMAILADDRESS>  
Feedback We appreciate your comments, regarding stories...   <EMAILADDRESS>  ******************************************************** 
 <URL>     <EMAILADDRESS>  
Biography Rebecca Wood, who learned to garden and forage from her grandparents, studied with leading experts in traditional Oriental medicine. She has taught and written about healing with a sustainable diet since 1970. 
Her book, The Splendid Grain, won both a James Beard Award and a Julia Child/IACP Award. 
Her most recent book, The New Whole Foods Encyclopedia, was a One Spirit Book Club (Quality Paperback Book division) main selection. 
An Educational Consultant to numerous organizations in the Natural Foods Industry, Rebecca co-founded and directed the East-West Center in Boulder, Colorado. 
She has established two cooking schools. 
Rebecca's food columns have appeared in Whole Foods Magazine, Health Foods Retailer, and other publications, and she was food editor for East-West Journal and Whole Foods Digest. 
Her articles appear in various publications including Ladies' Home Journal, Yoga Journal, Veggie Life, Men's Fitness, American Health, Utne Reader and Delicious! 
Rebecca is currently writing her next book, Be Nourished: A Six-Week Healthy Eating Program. It enables busy people to implement a diet that addresses their specific health and energy needs. 
Rebecca resides in Ashland, Oregon. 

A Personal Note 
I come from a lineage of cooks. At our clan gatherings, grandma prepared the selection of game, other meats and trimmings; each daughter brought her signature dish. It was Aunt Mick's pickles, Aunt Barbara's hand- dipped chocolates, Aunt Roselyn's huckleberry pies, Aunt Anna's home- brewed root beer and my mom's tender crescent rolls. 
Water and food were respected for their intrinsic worth. They were never wasted. Food was richly shared with friends and people in need. And a focal point of our many celebrations and gatherings was feasting. 
At home, it was a given that my siblings and I would help mom and dad with the gardening and canning. We set aside hundreds of quart jars of tomatoes, peaches, apricots, pears, raspberries, tomatoes and cherries plus grape juice and assorted jams and jellies -- a respectably appointed fruit larder for our Mormon family of six. 
These days, I appreciate my precious visits home during canning season to help my now 84-year old parents. I wish you could see the litany of their hands as they prep, scald, peel, pit, jar and then share the bottled peaches with their family. Such nurture in their kitchen duet! 
Shortly after college in 1969, I relocated to Boston and the home of macrobiotic teachers Aveline and Michio Kushi. We were a school of 25 living in two large "study houses" and eating a simple, whole foods diet. As a community, we observed and discussed how diet affected physical, emotional and mental health and we explored the effects of diet on world cultures. We learned how changing our eating changed our consciousness. It was a fascinating education. 
Before long I was cooking at a study house, teaching my first five-week cooking program and writing food columns. For me, macrobiotics' most valuable gift was engendering the kind of respect for food that I'd experienced at home. 
After twenty years of carefully following macrobiotics, I was shocked to have developed invasive third class cervical cancer. Fortunately, I was able to reverse the cancer by using natural methods which included adding meat and dairy back into my diet. Despite macrobiotic theory, some people do not thrive on a vegetarian diet. In all, it was an effective  lesson in moderation. 
As a food writer, I've delved deeply into our food sources. A big pleasure has been helping introduce whole and new grains to our market. All of us eating organic whole grains back in the 1960s were considered, at best, "health nuts". So eight years ago it was personally satisfying when my book, The Splendid Grain, received the two highest cookbook awards. Today, whole grains are actually in vogue! What a delight to have so many joining us at table. 
After three decades of writing in the alternative media about health advantages of organic and whole foods, it's a great pleasure to see the message reaching critical mass. It's been my privilege for my articles to help expose fraudulent marketing claims about sweeteners, processed soy and refined oils. 
On several continents I've walked the fields with farmers and visited cottage-crafted food producers. My studies of traditional food ways and Five-Element Chinese Medicine have formed my understanding of healing with food. Reinforcing this are my dietary counseling practice and being a mother and grandmother. 
May the information on these many pages serve you well and may all be well nourished. ********************************************************* 

Thursday November 10, 2005 


Folic acid, from fruits and vegetables, plays a role by powerfully protecting against methanol (formaldehyde) toxicity. 
Many common drugs, such as aspirin, interfere with folic acid, as do some mutations in relevant enzymes. 
The majority of aspartame reactors are female. 
In mutual service, Rich Murray ******************************************************** 
Rich Murray, MA  Room For All   <EMAILADDRESS>  505-501-2298  1943 Otowi Road   Santa Fe, New Mexico 87505 
 <URL>  group with 146 members, 1,241 posts in a public, searchable archive  <URL>   <URL>  
 <URL>  rationale to ban thimerosal (mercury) in vaccines and to ban aspartame (methanol, formaldehyde), speech from Kenneth P Stoller, MD to New Mexico Board of Pharmacy: Murray 2005.11.10 ******************************************************** 



