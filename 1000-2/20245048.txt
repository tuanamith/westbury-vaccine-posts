


Can states afford Bush's flu plan? 
 Nov, 04 2005 - WASHINGTON (AP) -- The nation's response to a flu pandemic could not succeed without a strong effort by state and local governments because the battle might have to be fought on "5,000 fronts," Health and Human Services Secretary Mike Leavitt says. 
Democrats in the House and Senate, however, question whether the states have the financial resources to engage in such a fight. 
In particular, lawmakers take issue with the Bush administration's plans for the purchase of certain medicines. The plan says states would pay about $510 million for enough anti-flu drugs such as Tamiflu and Relenza, which can reduce the severity of the illness, to treat 31 million people. 
The federal government would give states an incentive to make those purchases by providing a 25 percent match, or $170 million. 
Rep. Nita Lowey, D-New York, said the proposal amounted to an unfunded mandate on the states and might mean that some states would not be able to buy enough drugs. 
"This is a national emergency. I believe very strongly it should not depend upon where you live as to what sort of protection you get," Lowey told Leavitt at a House hearing Wednesday. 
Leavitt unveiled the administration's pandemic preparedness plan during two separate hearings before congressional appropriators. Sen. Patty Murray, D-Washington, broached the issue of state funding, too. 
Leavitt said that when it came to anti-flu drug purchases the federal government would be spending most of the money. The Bush plan calls for adding enough antivirals to the federal stockpile to treat 24 million people. All of that money would come from the federal government. 
The program involving state funding would supplement the stockpile with an additional 31 million courses of treatment. 
Leavitt also downplayed the role of anti-flu drugs during a pandemic, saying people should not equate the stockpiling of the drugs to pandemic preparedness. Rather, the foundation of the Bush plan relies on the development of vaccines that could prevent somebody from getting the disease altogether. 
Leavitt said funding and liability protections were critical to ramping up the production of a pandemic flu vaccine. 
Lawmakers said they agreed that drug manufacturers would need some protection from civil lawsuits, but Republicans and Democrats alike expressed concern that some of the legislation proposed so far gives consumers basically no recourse if harmed by a pandemic flu vaccine. 
Overall, President Bush proposes to spend $7.1 billion to prepare for a flu pandemic, three of which have occurred during the past century. The plan itself was released Wednesday, and it stressed major steps that state and local authorities must begin taking now: 
 Update quarantine laws.  Work with utilities to keep the phones working and grocers to keep supplying food amid the certain panic.  Determine when to close schools and limit public gatherings such as movies or religious services. 
"Every community is different and requires a different approach," Leavitt said. 
Also Wednesday, the government for the first time told Americans not to hoard Tamiflu, because doing so will hurt federal efforts to stockpile enough to treat the sick who really need it. Tamiflu's maker recently suspended shipments of the drug to U.S. pharmacies because of concern about hoarding. 
Lawmakers also grilled Leavitt on why it took the administration more than a year to issue its plan. 
"Could we have acted sooner to avoid the situation we are in now, in effect running for cover?" said Sen. Arlen Specter, R-Pennsylvania. 
But other lawmakers described the Bush plan as sound. Rep. Ralph Regula, R-Ohio, congratulated Leavitt for his "proven leadership" on the issue. 
Pandemics strike when the easy-to-mutate influenza virus shifts to a strain that people have never experienced before. It's impossible to predict when the next pandemic will strike, or its toll. But concern is rising that the Asian bird flu, called the H5N1 strain, might trigger one if it eventually starts spreading easily from person-to-person. 



