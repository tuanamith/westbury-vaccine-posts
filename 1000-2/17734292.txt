


Bush seeks $7.1 billion for avian-flu defense Reuters WASHINGTON - President George W. Bush asked Congress for $7.1 billion in emergency funding on Tuesday to prepare the United States for a feared avian-influenza pandemic by building stockpiles of drugs and vaccines and encouraging vaccine makers to modernize. 
Bush's plan would also bolster efforts to globally monitor the disease. 
"To respond to a pandemic we must have emergency plans in place in all 50 states, in every local community. We must ensure that all levels of government are ready to act to contain an outbreak," Bush said in a speech at the National Institutes of Health. 
Congress did not immediately respond, but the Senate was scraping together $3.9 billion in an amendment to a spending bill. 
The White House request includes $1.2 billion to make 20 million more doses of the current experimental vaccine against H5N1 avian influenza, $2.8 billion to accelerate new flu-vaccine technology and $1 billion to stockpile more antiviral drugs. 
The H5N1 avian influenza has so far only infected 122 people and killed 62, but it has spread to poultry flocks across many parts of Asia and into Europe. 
It is making steady mutations that scientists say could allow it to spread easily from person to person and cause a catastrophic global pandemic. 
"We really view this as a threat that cannot be addressed just within our borders. We see this as an international threat," Dr. Rajeev Venkayya, Special Assistant to the President for Biological Defense Policy, told reporters in a telephone briefing. 
Venkayya said the U.S. plan was meant as an example to state and local governments and other countries. 
Several groups said the requests were nowhere near enough, but praised Bush for making a start. 
The plan, to be spelled out in detail on Wednesday by Health and Human Services Secretary Michael Leavitt, includes $251 million to help detect and contain outbreaks before they spread around the world.  
GETTING BETTER VACCINES 
Bush's request will add to the U.S. supply of H5N1 vaccine, made experimentally by Chiron Corp , Sanofi-Aventis and others. 
The vaccine would only provide partial protection against a pandemic strain of flu and would have to be reformulated to match whatever mutation eventually emerges -- a process that would take months. 
Scientists say a better way of making flu vaccines is years off, but they have pressed the government to make a start. The vaccine against annual flu does not protect against avian flu. 
Only a few companies make flu vaccines and only two -- Sanofi-Pasteur and MedImmune -- make them in plants within the United States. 
Recent shortfalls in influenza vaccine production -- last year Chiron lost its license and cost the United States half its anticipated supply -- have illustrated how tenuous the industry is. 
Bush asked Congress to pass legislation reducing the liability of companies making flu vaccine. 
"We rely on the private sector to do a lot of things in this country. The private sector has not responded in this case. We have had decimations of the vaccine industry in this country," Venkayya said. 
He said the White House had persuaded Sanofi to drop one project and instead produce some extra batches of its experimental H5N1 vaccine.  
STOCKPILE 
Bush also requested $1.03 billion to add to the 2.3 million treatment courses of influenza drugs the United States already has stockpiled. Two drugs -- Roche and Gilead Sciences Inc's Tamiflu, and GlaxoSmithKline's Relenza, can help relieve the severest symptoms of avian influenza. 
The request will bring the total U.S. supply up to 50 million treatment courses by 2009, Venkayya said. While about 40 other countries are competing for a limited supply of the drugs, Venkayya said he was confident the United States could fill its own orders. 
Roche said it was willing to help, and had won a license for a new U.S.-based production facility. "By mid-2006, global production capacity for Tamiflu will have increased eight to ten-fold over 2003," it said in a statement. 
Experts have warned against over-reliance on antiviral drugs in the event of a pandemic, because it is not clear how well they will work or whether there will be anywhere near enough. They say it will take years to build a sufficient stockpile. 
Any U.S. plan also needs to address issues such as security and availability of food and other supplies, said Mike Osterholm, an infectious disease expert at the University of Minnesota and an adviser to the U.S. government. 
The federal government set up a Web site on avian flu preparedness at  <URL> . 
 <URL>  



