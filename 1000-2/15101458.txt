


Male circumcision reduces HIV risk by 60%, says study  
· Researchers believe virus attaches to foreskin cells · Men urged to keep practising safe sex  
Ian Sample, science correspondent Tuesday October 25, 2005 The Guardian  

A study into the spread of HIV, the virus that leads to Aids, has found  that male circumcision significantly protects men from picking up the  infection. The finding, though cautiously welcomed, presents a headache for health  officials who fear that communities where male circumcision is common  might neglect more effective protective measures such as using condoms  and reducing their number of sexual partners. 
The study, which followed infection rates in more than 3,000 heterosexual  men over nearly two years, found that circumcision reduced a man's risk  of acquiring HIV by 60%. 
Scientists had suspected circumcision might offer some protection against  the virus after noticing differences in HIV infection between groups  where circumcision was a cultural right of passage and others where few  were circumcised. Until now, no large-scale studies had been carried out  to investigate the effect. 
Adrian Puren at the National Institute for Communicable Diseases in  Johannesburg and a team of researchers in Paris recruited 3,274  uncircumcised volunteers from South Africa aged 18 to 24, who were  considering circumcision. Half underwent the operation. The researchers  then monitored both groups for HIV infection over the next 21 months. 
So marked was the difference in infection between the groups that the  study was halted on ethical grounds. Of those who had been circumcised,  20 tested positive for HIV while 49 of the uncircumcised group had  contracted the virus. 
Writing in the journal Public Library of Science Medicine today, the  authors say that circumcision appears to reduce the risk of acquiring HIV  by 61%, "equivalent to what a vaccine of high efficacy would have  achieved". 
Other scientists sought to clarify that while circumcision appeared to  offer a protective effect over the duration of the study, a high-quality  vaccine would be more effective in the longer term. 
Peter Cleaton-Jones, the chairman of the human research ethics committee  at the University of Witwatersrand in Johannesburg, said: "Circumcision  is not going to prevent HIV infection in the long run. If circumcised men  think they're protected against HIV, they're fooling themselves. If they  don't practise safe sex, they'll still be at risk, it's just a lower  risk." 
Why circumcision should offer some protection is not well understood, but  researchers know that the part of the foreskin that is removed in the  operation is rich in Langerhans cells that the virus strongly attaches  to. "HIV has to gain access to the body and to do that it binds to  particular cell types," Dr Puren said. "By removing the skin that  contains those cells, you remove the tissue the virus would normally bind  to." Similar trials are ongoing in Kenya and Uganda and are expected to  end within the next year. 
The World Health Organisation said: "If male circumcision is confirmed to  be an effective intervention to reduce the risk of acquiring HIV, this  will not mean that men will be prevented from becoming infected with HIV  during sexual intercourse through circumcision alone. Nor does male  circumcision provide protection for sexual partners against HIV  infection." 
Will Nutland of the Terrence Higgins Trust said circumcision would not  have much effect in the UK, where most new infections were in homosexual  men who had receptive anal sex. 
British scientists yesterday announced a separate trial to test a  microbicide developed to stem the spread of HIV. Thousands of women in  Uganda and South Africa are being enrolled to test Pro 2000, a gel  applied to the vagina, that has been shown to block the entry of the  virus. The study will be coordinated by the MRC Clinical Trials Unit and  Imperial College London.   

--  Big Lies of anti-Israel Propaganda  <URL>  



