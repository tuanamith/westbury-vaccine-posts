



"RetroProphet" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
This is for Jong and Art and others who like to argue about whether  evolution is occurring or whether the Biblical flood myth is true.  I found  this on the 'net some time ago and the original author is not known as it  was not signed when I found it on an obscure website of some university  somewhere; however, read through this and think about how many times we have  been subjected to faulty logic in these discussions.  Of course, this is  only a partial list, but it's pretty good for a start. 
Here goes: 
Logical Fallacies 





Logical fallacies are errors in reasoning that are used, either deliberately  or subconsciously, to support arguments or points of view.  Detecting and  evaluating logical fallacies is a critical skill that can be used to  determine whether to accept or reject an argument. 
Fallacies are often used when somebody wants to convince you of something  that has little basis in fact. They are also commonly used to distract  attention away from uncomfortable answers. Needless to say, a good knowledge  of fallacies is a useful tool in the armory of any good salesperson,  politician, lawyer or con-man. 
While fallacies will not prove a claim, neither will they disprove one.  Just because an author uses fallacies to support his case doesn't mean that  his claim is wrong. However if all the arguments used are fallacies then his  argument remains unproven and his audience is entitled to reject his  argument until he can provide a stronger basis for it. 


Hasty Generalization 

This is where someone pronounces that something is always the case, when in  fact only a small amount of evidence is available. Often this evidence may  simply be anecdotal. Just as one swallow does not make a summer, most  generalizations require a lot evidence to become commonly accepted. 


"Air temperatures in Antarctica have been decreasing for the past 10 years,  therefore the world is not warming." 


What about studies in other parts of the world? Would 10 years be considered  a long enough time to come to a conclusion such as this? 


"You don't care about me. Just yesterday you didn't reply to my phone call." 


Can we have more evidence that this is the case? 


Slippery Slope 

This fallacy is employed to convince someone that much greater consequence  will follow if an initial claim is accepted. The implication is that the  claim will be a first step in an unstoppable chain of events. While  sometimes large consequences can occur from an initial event (such as  lighting the fuse on a stick of dynamite), this is not always the case, and  the mechanism by which greater consequences can transpire must be  investigated further. 


"If you go to the night club tonight, before you know it you will meet up  with some undesirable young man and he will make you pregnant." 


Is this a certainty? Does free will not have anything to do with it? 


"By filling in this form, you will be buying into a business enterprise that  will make you a millionaire in 5 years." 


This may be true, but it might also require a lot of hard work, a ton of  luck, and a market eager to buy your product. 


Single Cause 

Life is complex, and people have a tendency to over-simplify, so when  someone is attributing a single cause to something that has many causes,  they may be committing the Single Cause fallacy. 


"The cause of the traffic problems in this city is old people who drive too  slowly." 


There are many factors in traffic congestion, including the quality of the  road infrastructure, the number of people hitting the roads at any time or  the likelihood of accidents happening. To attribute it to just one cause is  simplistic. 


False Cause 

Sometimes people assert that one event caused another event when in fact the  events are not related to each other at all, or they are both effects from  an unstated cause. 


"There was a huge aurora borealis in the sky last month, and then three days  later the terrorist attack took place. This was an omen." 


Such events are unrelated, however there is always a tendency for people  look back in the past to rationalize a situation in the present. 


Begging the Question 

Begging the question is a type of circular reasoning.  It can occur in many  forms, the most trivial being a restatement of the original argument. 


"Nudity should be banned on television." 


Why? 


"Society needs laws to prevent the flagrant display of naked flesh on our TV  sets." 


Translation: Nudity should be banned on TV because nudity should be banned  on TV. 


The fallacy can be much more complex than this. When someone has a strong  belief that they have insufficient basis for, they may use the argument  itself as a basis for the argument. It's also called assuming the answer,  and it's very common in prejudicial thinking. 


"The opposition party wants to trigger a war." 


Why? 


"Well, they reject everything the government says." 


Why? 


(They want to trigger a war) 




False Dilemma 

A dilemma is a choice between two different, contradictory options, and a  false dilemma is where multiple options are open to you, but only two  options are presented to you. One option is often hugely undesirable,  leading you to accept the claimant's conclusion. 


"You are either with us or with the terrorists." 


What if you support neither? 


"Would you like to buy the blue kettle or the red kettle?" 


What if you don't want to buy any kettle? 


Ad Populum 

If the basis of a claim is merely that it is accepted by many other people,  this does not on its own make that claim true. Lots of people thought that  the Sun, the planets, and the stars traveled around the Earth many centuries  ago, and the whole lot of them were wrong. Just because something is popular  doesn't mean that it is right. 


"A recent opinion poll found that 90% of respondents believed John to be  guilty, therefore he must be guilty." 


Right-o, we don't need any evidence then! 


"Loads of people went to see the film 'Titanic'. It must be a great film." 


Appeal to Ignorance 

This is the last refuge of somebody who hasn't a leg to stand on regarding  their viewpoint, so they might question whether an alternative could  possibly exist. Just because they can't think of another solution doesn't  mean there isn't one. 
Another refuge of last resort is an appeal to an untestable authority (e.g.  God) for explanations, or to assert that it's all part of some grand  conspiracy.  These are called 'Ad Hoc Rationalizations' - the provision of  special reasons which are impossible to prove or disprove one way or  another. 


"If UFO's are not making contact with us, then how can you explain all the  lights in the sky?" 


Well, it could be satellites, meteors, planets or planes? 


"It's impossible to imagine evolution being workable in any way, and this  should be proof alone that it's not true." 


It might be possible to imagine it working if you studied biology? 


Appeal to Dubious Authorities 

Just because someone powerful says that something is true, or just because  it is written in a book, doesn't make it true, unless the source of the  claim is creditable. If the claimant and his authority share the same  incorrect assumptions, then it proves nothing. 


"The Pope is against contraception, therefore contraception must be bad for  you." 


The Pope certainly has his viewpoint and he is entitled to it, but it is one  of many. 


"Well I bought this book and I liked it, so you will like it too." 


You might well hate it. 


Appeal to Pity 

An Appeal to Pity is used when the claimant tries to convince people by  making them feel sorry for him, when his circumstances are irrelevant to the  argument. The use of pity is just one way people can be manipulated into  believing something untrue. They can also be threatened, induced, seduced,  bullied or harangued. 


"Are you telling me that I have just spent 30 years of my life knocking on  doors telling people about The Divine Truth, for nothing?" 


"If you don't buy this product from me, I will lose my job!" 


If the product is not right for you, you should not be compelled to buy it. 



Emotive Words 

While not necessarily fallacious, the choice of language is often a powerful  tool in convincing people of your argument. There are many words and phrases  with powerful negative and positive connotations, which are used to rouse  emotions without adding substance to the case. 


"Our great leader is wise and gentle. He chose to lead our volunteers, who  love freedom and justice into this fight for truth, so that the enemy could  be combated and our children could be safe from oppression." 


"This terrorist madman has brainwashed impressionable teenagers to murder  with impunity. This recent act of wholesale terrorism is a last-gasp effort  to consolidate his grip on power, irrespective of the consequences." 


Ad Hominem 

An ad hominem attack is an attack on someone's character. A very powerful  way to convince others of the merits of your argument is to ridicule your  detractors or competitors, even though this, of itself, does nothing to  support your position. It's also used as a tool to distract people from you  having to better substantiate your argument. 


"Well this law was obviously constructed because of the demands of that  sanctimonious mob of weepy politically correct sops who always stick their  heads above the parapet when anything even slightly critical of immigration  is mentioned." 


Whatever the character of the people who introduced it, what about the  merits of the law itself? 


"Here you go spouting off about your rights to go out at night, when you are  the first person to tell everyone else to shut up when they are talking to  you. You're a complete pig!" 


Nice way of shutting somebody up, don't you think, irrespective of the  arguments being proposed by the other person? 


Another example of Ad Hominem is to point out hypocrisy on the part of the  claimant. Just because there might be an element of personal hypocrisy  involved, it doesn't automatically make their claim invalid. 


"Mr. Jennings is berating us about the evils of unregulated handgun  ownership. What he didn't tell you however is that in the 1980s he had a  handgun himself, and used to go out shooting on a regular basis" 


The Red Herring 

A Red Herring is a deliberate attempt to distract people away from a  position by attempting to focus attention elsewhere. The issue might be  generalized to a philosophical debate, or the phrase 'but what about?' might  be inserted to try to wrest the discussion away to a safer topic. 


"Well you are giving me a hard time about our objections to your new  vaccine, but what about the time that your company released a vaccine into  the market that had to be recalled 3 weeks later?" 


The topic has been deliberately switched from the current objections to  something quite different. 


"You are claiming that we caused this crisis, but there has been a long  history of you guys claiming all sorts of calumnies against us which turned  out not to be true ? why not talk about that for a while?" 


This doesn't necessarily mean that they didn't cause the current crisis. 


Straw Man 

A common approach is to switch the focus of your attack to an easier target,  by misrepresenting the views of the opponent. So instead of acknowledging  that your opponents are saying X, you pretend they are saying Y, and you  attack this position with gusto. 


"The minister wants funding to be withdrawn from child services, but what he  is really saying is that he wants more teenage violence, more teenage  pregnancies, and more drug abuse." 


Is this actually what the minister wants? 




JB 





