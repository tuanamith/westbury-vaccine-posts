



"fresh~horses" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... Bush Requests $7.1 Billion To Prepare for Flu Pandemic 
Associated Press November 1, 2005 1:02 p.m. 
The president, in a speech at the National Institutes of Health, said the U.S. must be prepared to detect outbreaks anywhere in the world, stockpile vaccines and antiviral drugs and be ready to respond at the federal, state and local levels in the event a pandemic reaches the U.S. 
Mr. Bush outlined a strategy that would cost $7.1 billion including: 
· $1.2 billion for the government to buy enough doses of the vaccine against the current strain of bird flu to protect 20 million Americans; 
· $1 billion to stockpile more antiviral drugs that lessen the severity of the flu symptoms; 
· $2.8 billion to speed the development of vaccines as new strains emerge, a process that now takes months; 
· $583 million for states and local governments to prepare emergency plans to respond to an outbreak. 

China Pledges Openness 
China was heavily criticized during the 2003 outbreak of severe acute respiratory syndrome for initially covering up the illness. Now Beijing says it is committed to quickly investigating and reporting possible bird-flu cases to the public and world-health groups. "From SARS, we see that no ... information can be hidden," China's disease-control director Qi Xiaoqiu said Monday through a translator while visiting the U.S. "We have policies to encourage farmers to report possible outbreaks." Mr. Qi said China has provided subsidies for farmers who quickly report sick birds to authorities. 
Chinese health officials have also signed a memo of understanding on U.S.-China cooperation with U.S. Health chief Mike Leavitt. China's biggest drug maker, Shanghai Pharmaceutical Group, has also contacted Roche Holding AG about sublicensing the right to make Tamiflu. 
China has reported three bird-flu outbreaks in poultry over the past month. No human cases have been reported. 
Fighting Bird Flu Could Cost $102 Million 
Meanwhile, Pacific Rim health officials met in Australia for a second day Tuesday to discuss ways to fight the virus. 
Copyright © 2005 Associated Press 

Maybe Bush should raise the Homescam Security Alert.  You never know when the Avian Flu may use a WMD and hate the US for their 'freedom'. 



