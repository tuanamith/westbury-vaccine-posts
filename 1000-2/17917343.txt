



PocoLoco wrote: 

Oh crap, John. Very sorry to hear about your daughter. (I had no idea you were a widower). 
In what state does your daughter reside? I would think each state insurance commissioner and statutes will vary just a bit on what her patient's rights are going to be. My sister-in-law, at age 56, has consumed a few million dollars worth of medical care (seriously) due to a heart condition. She has reached the "lifetime maximum" with a couple of companies- but just her good luck, each time she does the organization she works for changes hands and she gets a "fresh start". 
Odds are that her current insurance company cannot just dump her, but if she ever loses coverage due to firing, layoff, company bankruptcy, etc she might, in some cases, have some difficulty getting a private policy on her own. Several years ago we passed alaw in Washington that said people who lost group medical insurance had the right to buy private insurance (at a market rate) and could not be turned down for a pre-existing condition. Several insurance companies left the state as a result. (Good riddance). 
Best wishes for your daughters successful treatment and a full recovery. They are treating cancer far more effectively than ever before- but even so it's a rigorous and demanding course of treatment and it won't be easy for her. 
Tell your younger daughter to get an annual mammogram, (if she isn't doing so already), and to hope that some of the very latest news about a promising "vaccine" to prevent breast cancer might bear fruit. 


