



"Wow, I always knew Cartmans' mom was a slut..." < <EMAILADDRESS> > wrote in message > > 

HIV Could Launch Bird Flu Pandemic 

  Scientists are concerned that HIV/AIDS carriers who contract Bird Flu could mutate the Flu virus into a more easily transferable form, warning that this might trigger the feared international pandemic. 
Noted US virologist Dr Robert Webster warns that people with compromised immune systems will not be able to fight off influenza, and could carry it for much longer than healthy people, allowing it to mutate in their bodies to a form which more easily spreads in humans. Scientists have already found that Avian Flu will mutate in such a way, and that a long period in a person with a compromised immune system will allow the mutation to occur more quickly. 
The greatest concern is currently for people living in South Africa, where there is the world's highest number of people living with HIV/AIDS. Bird Flu has presently infected about 125 people in South East Asia, and has spread to Asia, home to the second largest number of people living with HIV/AIDS in the world. Should Avian Flu be contracted by someone with HIV/AIDS in one of these two regions, scientists warn that the influenza virus could quickly spread into a pandemic. 
Aids may help spread of bird flu By Roland Pease BBC science correspondent 
It is feared bird flu will jump from human to human Bird flu could readily mutate into a pandemic form if it infects people with Aids, a flu expert has warned. 
Dr Robert Webster said it was possible people with Aids, who have depressed immune systems, could harbour the deadly H5N1 strain of bird flu. 
This would potentially give it the opportunity to become better adapted - and more dangerous - to humans. 
Dr Webster was speaking at a conference organised by the Council on Foreign Relations in New York. 
At present, H5N1 cannot pass easily from human to human. It has so far infected around 125 people in South East Asia, but most of these have had close contact with infected birds. 
Experts fear that the widespread infection of birds in this region, coupled with the close mixing of birds and people, could lead to the virus evolving to pose a more deadly threat. 
But Dr Webster, of St Jude Children's Research Hospital im Memphis, said the key could be when H5N1 reaches East Africa, where HIV/Aids is rife. 
Cannot clear virus 
He said experience with immune-compromised cancer patients at his hospital had showed they are unable to clear normal flu virus from their systems, and can shed copies of the virus for weeks. 
The same could be expected of AIDS patients coming down with H5N1, he said. 
"We're all very worried by the prospect," he told the BBC. 
Reproducing over a long period inside a human would be the ideal conditions for more infectious forms of the virus to develop. 
H5N1 has not reached East Africa yet, but it is the final destination for many birds currently migrating from infected areas. 
Officials at the UN Food and Agriculture Organisation expect to arrive there soon. 
They believe that because the social conditions are close to those in Asia, and farming practices are similar, the virus could take a grip among poultry as it has in Vietnam, Thailand, Indonesia and China. 
Health expert Laurie Garrett adds that with malaria, tuberculosis and HIV already widespread in Africa, it will be difficult to single out the symptoms of bird flu in new victims - high fever and nausea. 
The situation is compounded by the parlous state of the health systems across the continent. 
The direct effect of H5N1 on people with Aids is hard to predict. 
The H5N1 virus overstimulates the immune system, and many of its powerful effects are caused by what medical expert call a "cytokine storm", after the immune molecules excited by the disease. 
It was the cytokine storm that overwhelmed so many victims of the 1918 flu pandemic. Aids patients may be spared that fate. 
But equally possible, with their immune defences down, they could succumb easily to the disease. 
"In that situation," said Laurie Garrett, "vast populations of HIV positive people could be obliterated by the pandemic flu." 
Can China cope with bird flu? By Stephen Cviic BBC News 
China has announced its first human death from bird flu, raising renewed fears about the possible spread of the disease among birds and humans. 
Up until now, all of the human fatalities from the H5N1 strain have been in East Asia, most of them in Vietnam and Thailand. 
But when the disease affects people in China, the world's most populous country, it is bound to attract even more attention. 
The woman who is known to have died of it was a poultry worker in the eastern province of Anhui. 
The other confirmed case was a boy in Hunan province who has since recovered; his sister died, but because she was cremated immediately afterwards, it is impossible to know what the cause of death was. 
"The virus is entrenched... we expect more outbreaks" Henk Bedekam World Health Organization 
China has been grappling with eleven separate outbreaks of bird flu, also known as avian influenza, among fowl, and some human cases were expected. 
Henk Bedekam, the World Health Organization's representative in China, says the time of year is also favouring the continuation of the disease. 
"We're moving towards winter and we know that [the virus] can longer survive in the winter when it's cold, so we're not surprised that we have now here and there some outbreaks. 
"We know that in this part of the world - of course not for the whole of China as well, but for parts of China - that the virus is entrenched, and that means especially in the cold months that we expect some more outbreaks." 
Human infection 'rare' 
Bird flu has already spread from Asia into Europe. Outbreaks have been reported in Turkey, Romania and Croatia. 
However, Europe has had no human cases, and all the ones in Asia seem to have been the result of direct contact with animals. 
Dick Thompson, of the WHO in Geneva, says there is no evidence that that is changing. 
"The one thing that we are worried about of course and we've been worried about for some time is that this virus will change in a way that will allow it to move easily from one human to another. 
"That hasn't happened. It continues to be an extremely rare event for a human to become infected with this disease." 
But of course the threat of avian influenza is a worry for farmers everywhere. 
In China, millions of birds have been culled, and now the authorities have come up with an even more ambitious plan: to vaccinate every single one of the 14 billion farm birds in the country. 
But is this actually possible? 
Enough vaccine? 
"I think logistically that's an enormous nightmare, particularly given that as far as European scientists are concerned, any vaccine would have to be done twice, with one week between both vaccines," says Roger Wait of the agricultural newsletter, Agra Facts. 
"You also have other problems, such as the vaccine itself - do you have enough vaccine in China? One could imagine there could be a problem with bogus vaccines or imitation vaccines getting into the system somehow. 
"And then of course you've got other issues such as trade and a question mark over whether the vaccine actually kills the virus itself." 
So, there is not a global consensus that vaccination of birds represents the best answer. 
Experts do believe, however, that trying to control the movement of domestic fowl is important. 
Ultimately, though, once the uncertainties surrounding wild bird migration and virus mutation are factored in, it is fair to say that nobody really knows how much effect avian influenza will eventually have on the world's economy and its population. 
TESTIMONY BEFORE COUNCIL ON FOREIGN RELATIONS 
Would you please speculate on what you think would happen when an individual who is HIV positive becomes exposed to a bird or in some other way acquires an infection of H5N1? 
WEBSTER: Well, thank you for the crown. I'm not sure that I wear it comfortably at the moment -- not at all. 
My great concern I think I'm sharing with you is that if this virus and when this virus gets into Africa into the HIV-positive people, who are immunosuppressed, what happens in an immunosuppressed person we know with influenza in cancer patients, the virus is shed for an extended period of time, and it gives the virus the chance to accumulate the mutations of adaptation to humans. 
And so this -- you put your finger on the great worry that we all have for this virus getting into Africa along with HIV. 
There are at this moment unconfirmed reports of H5N1 die-offs among bird populations in Iran and Iraq. If true, these could foretell spread of the virus to the African flyway, which would include a spectacular range of species migrating from Ethiopia to South Africa. We do not know how H5N1 will behave in the body of an HIV+ human being. There are two theories, scientific rationales for which are a bit too complicated to detail here. Nevertheless, in one scenario the HIV-weakened immune systems of infected individuals create permissive environments for H5N1, allowing the flu virus to thrive, mutate and adapt to human beings. In such a scenario, the HIV+ person is, in a sense, an ambulatory Petri dish, incubating, and possibly spreading, new forms of the virus. 
In a second scenario, however, the HIV+ individual, unable to mount a protective immune response against H5N1 is easily infected and swiftly devastated. In that situation vast populations of HIV+ people could be obliterated by the pandemic flu. This is a horrible notion, and ominous given the extraordinary HIV infection rates in many African countries. 
Regardless of which HIV/H5N1 scenario is correct, spotting any movement of the flu virus from African birds to the continent's peoples will be exceedingly difficult. As weak as the public health infrastructures and surveillance systems are in much of Asia, such capacities are far worse in sub-SaharanAfrica. Further, spotting symptoms such as the emergence of clusters of people with high fevers and nausea might be impossible against a background of malaria, tuberculosis and HIV. 
When there is a human-to-human confirmed case, will that tell us something about the underlying health status of the humans involved, or will it tell us something that's changed fundamentally about the virus itself? 
WEBSTER: One human-to-human won't tell us very much. And unfortunately, we don't get enough information from humans in Asia on what has happened to humans. The number of postmortems done is extremely small. The number of viruses we get to examine is extraordinarily small. And we will not know from one or two passages the molecular basis. We have to take this back into the laboratory to really understand this, and we will know when we have sustained transmissibility; that's the breakpoint. 
SUAREZ: Dr. Wolinsky, is there a situation that's created by human activity or a situation that's created by bird activity, which is something sort of beyond our reach, that can make this a greater or lesser danger? 
DR. STEVEN WOLINSKY: Well, certainly when, during the course of human events when we're put in greater proximity to animals that have other viruses, they do have a propensity to leap from their animal host to humans. This has gone on in many situations. HIV/AIDS is a perfect example of a virus that existed within a primate host for a long period of time until it actually was successful in its jump to humans. But that leap was preceded by many sort of tentative steps, as we're seeing now where it sort of makes a little dance forward; maybe there's a one or two person spread. It doesn't quite make it. And again, I would emphasize that we really do need to know what is going on at the molecular level. What really characterizes the virus that makes that leap? What are the underlying genetic factors that both the virus, the host and its immune response that makes this happen? 
SUAREZ: Are there changes that happen in the virus that blunt any attempts to defend against it? 
WOLINSKY: Well, in respect that the virus can have enhanced pathogenicity -- its ability to actually do greater damage to the host -- there may be aspects of the virus, per se. Witness the 1918 influenza virus. There's properties that we've seen in the animal models where it really has shown an enhanced ability to kill in a situation where other viruses, other influenza viruses did not -- its ability to incite an immune response that far outweighs what is needed for the host to be protected, and in fact, ravages the host, ravages the animal model. So there are some genetic variants that exist in particular and have been transmitted. What are the actual particular pathogenic factors that mediate this are still really not known. 
SUAREZ: We tend to talk about mutation as if it's a discrete event which gets you from one place to a single new place. Could five or six or 10 or 100 variants be cooking right now in animal host bloodstreams and some just end up being mutational dead ends because they kill too quickly or don't kill at all or don't spread easily, and, in fact, it's only the one that's successful that catches our attention? 
WOLINSKY: Well, certainly -- we can just go back to Darwin on this -- (inaudible) -- we're looking at dissent with modification. 
SUAREZ: You can do that because we're in New York State. Okay? (Laughter.) 
WOLINSKY: That's right. We're not in Kansas anymore. (Laughter.) And like other RNA viruses, flu is highly mutable, highly adaptable and capable of rapid evolution. And within the host, it's not a single virus that you have but actually a swarm of viruses. And they're all genetically related but yet distinctive. The virus is constantly mutating. And it's on a cycle that's akin to other RNA viruses that every time it replicates, it makes a mistake. And so basically you're left with this viral swarm. And how the swarm actually adapts to the host, how the immune system actually manages that is really up to an individual basis as well. And there are individual differences in terms of how people respond to infectious diseases, and there are differences because of their genetic makeup, because of their immune response, their underlying difficulties with immune response, how the virus is able to exploit these and the fact that certain ones become adapted, better able to grow in a certain environment. 






    www.natap.org 










