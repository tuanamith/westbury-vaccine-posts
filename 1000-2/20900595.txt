



 <EMAILADDRESS>  wrote: 

Perhaps you are thinking of Madsen et al? Hviid et al. [ <URL>  (behind registration, but registration is free)] 
"To evaluate whether the incidence of autistic-spectrum disorders was increasing in Denmark in the study period, we calculated time period trends from our cohort. We found statistically significant increases in age-adjusted RR per calendar year for both autism and other autistic-spectrum disorders during the study period (RR, 1.24 [95% CI, 1.17-1.31] for autism; RR, 1.21 [95% CI, 1.16-1.27] for other autistic-spectrum disorders). In the period from January 1, 1995, to December 31, 2000, a period where outpatients were included, we found similar trends (RR, 1.24 [95% CI, 1.16-1.32] for autism; RR, 1.20 [95% CI, 1.13-1.26] for other autistic-spectrum disorders)." 
Which is to say, the incidence of autism was still increasing 3--5 years after the removal of thimerosal, without reference to the numbers only including in-patients [although even then, in-patients included those attending hospital for daily treatment at some point, and outnumbered the actual in-patients by 4 to 6 times. Madsen et al. Pediatrics, 2003)] 
Take a look; this was a good paper; they considered many confounding factors, and used a Madsen et al. (NEJM 2002) assessment of the accuracy of diagnosis. 
Their conclusion: 
"We found no evidence of an association between thimerosal-containing vaccine and autism in children who received thimerosal-containing vaccine compared with children who received the same vaccine formulated without thimerosal. Furthermore, there was no indication of a dose-response association between autism and the amount of ethylmercury received through thimerosal." 
Cathy 




