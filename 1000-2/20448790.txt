


Lansing Authorities Dig Deeper Into Planten's Past In Michigan 
POSTED: 9:47 pm EST November 9, 2005 UPDATED: 10:04 pm EST November 9, 2005 
LANSING, Mich. -- From where he lived to where Drew Planten worked, investigators in Lansing, Mich., are continuing their investigation into a possible connection between Stephanie Bennett's accused killer and an unsolved death in Michigan. 
Even though they have no evidence to confirm any type of relationship between Planten and 22-year-old Rebecca Huismann, investigators will work over the next few weeks to develop a timeline of Planten's life in Michigan. 

Lansing police first began looking at Planten when Raleigh police found documents related to Huismann in his apartment, as well as a gun that sources tell WRAL is tied to Huismann's death. 
Planten moved to the Lansing area his senior year of high school. In 1998, he graduated East Lansing High School and attended Michigan State University, where he graduated in 1995 with a degree in zoology. 
After graduation, Planten's mother told WRAL that her son worked for a company called Bioport Corporation, the sole producer of the Anthrax vaccine. In 1997, Planten worked as a lab technician for nearly a year at Neogen Corporation, which manufactures food and animal safety products. 
Just 10 months before Planten took a job with the North Carolina Department of Agriculture, on an early morning in 1999, Lansing authorities say Huismann was shot in the head while returning from her job where she worked as a dancer at a club called Dream Girls. Her body was found sprawled on the ground with the car door still ajar. 
At one point, Planten had lived just two miles from that home, but was not living there at the time of Huismann's death. 
Huismann's former roommate, who spoke under the condition that her identity not be released, told WRAL that her roommate had told her about a frequent client at the club who was getting too close. 
Michigan investigators are also looking at Planten in two other unsolved murder cases. To date, they have found nothing to link him to those two crimes. 
 <URL>  



