



"fresh~horses" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... Bush Requests $7.1 Billion To Prepare for Flu Pandemic 
Associated Press November 1, 2005 1:02 p.m. 
Mr. Bush outlined a strategy that would cost $7.1 billion including: 
· $1 billion to stockpile more antiviral drugs that lessen the severity of the flu symptoms; 
· $2.8 billion to speed the development of vaccines as new strains emerge, a process that now takes months; 
· $583 million for states and local governments to prepare emergency plans to respond to an outbreak. 

China Pledges Openness 
China has reported three bird-flu outbreaks in poultry over the past month. No human cases have been reported. 
Fighting Bird Flu Could Cost $102 Million 
Meanwhile, Pacific Rim health officials met in Australia for a second day Tuesday to discuss ways to fight the virus. 
Copyright © 2005 Associated Press 




