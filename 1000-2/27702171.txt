


Return-Path: < <EMAILADDRESS> > Received: from smtp5k.poczta.onet.pl ([213.180.130.50]:18621 "EHLO 	smtp5k.poczta.onet.pl") by ps11.test.onet.pl with ESMTP 	id <S1053170AbVKSKpS>; Sat, 19 Nov 2005 11:45:18 +0100 Received: from smtp3.poczta.onet.pl ([213.180.130.29]:28321 "EHLO 	smtp3.poczta.onet.pl") by kps5.test.onet.pl with ESMTP 	id <S4874515AbVKSKoJ>; Sat, 19 Nov 2005 11:44:09 +0100 Received: from omc1-s35.bay6.hotmail.com ([65.54.248.237]:11913 "EHLO 	omc1-s35.bay6.hotmail.com") by ps3.test.onet.pl with ESMTP 	id <S3412162AbVKSKnr>; Sat, 19 Nov 2005 11:43:47 +0100 Received: from hotmail.com ([65.54.173.14]) by omc1-s35.bay6.hotmail.com with Microsoft SMTPSVC(6.0.3790.211); 	 Sat, 19 Nov 2005 02:43:45 -0800 Received: from mail pickup service by hotmail.com with Microsoft SMTPSVC; 	 Sat, 19 Nov 2005 02:43:45 -0800 Message-ID: < <EMAILADDRESS> > Received: from 195.166.237.40 by by5fd.bay5.hotmail.msn.com with HTTP; 	Sat, 19 Nov 2005 10:43:44 GMT X-Originating-IP: [195.166.237.40] X-Originating-Email: [ <EMAILADDRESS> ] X-Sender:  <EMAILADDRESS>  Reply-To:  <EMAILADDRESS>  From:	"JAMES ERIC" < <EMAILADDRESS> > Subject: UN-USA JOBS VACANCY Date:	Sat, 19 Nov 2005 10:43:44 +0000 Mime-Version: 1.0 Content-Type: text/plain; format=flowed X-OriginalArrivalTime: 19 Nov 2005 10:43:45.0224 (UTC) FILETIME=[1D937480:01C5ECF6] To:	unlisted-recipients:; (no To-header on input) X-OnetAntySpam: NIE, to nie jest SPAM X-OrigFrom:  <EMAILADDRESS>  X-ZA0: unknown (-1,0) 


US COMMITTE FOR THE U.N  New Entry Professional Program 
The NEW ENTRY PROFESSIONAL (NEP) PROGRAM is the Agency's program for bringing well qualified 
applicants into the Agency's Foreign Service. UN-USA's Foreign Service provides successful 
applicants with a career-long system of rotational assignments in Washington D.C. and overseas. 
Promotion is based upon merit with selections being made for promotion at various established 
points in an employee's career. Unlike the competitive U.S. Civil Service and most private 
industry, pay is set based upon a person's grade level regardless of duty assignments. Like the 
U.S. Department of State and other agencies employing Foreign Service personnel, successful 
applicants are offered a clear path for planning their career from the intake level through the 
most senior executive positions. 
I. SELECTION 
Selection for the NEP program is based upon a highly competitive screening process. Consequently, 
applicants meeting the basic requirements may not go through the entire 
screening/interview/selection process. Candidates are evaluated on the basis of academic 
credentials, related overseas and/or domestic development professional experience, and other 
relevant factors. 
Applications are  screened for basic eligibility, such as:  education, and experience which 
come to Washington at their own expense for training although all Expenses made are reimbursed 
along with salary. 

III. SPECIALIZED REQUIREMENTS 
Applicants are strongly encouraged to submit a supplemental statement with their application 
addressing the specific knowledge, skills and abilities listed in the announcement. 
Computer Skills: Computer skills is also used as selection criteria. 

IV. PROFESSIONAL TARGET DISCIPLINES 

A. Democracy and Governance Officers    Salary:$152,678.00 USD 
Responsible for development, oversight, management (staff, financial, and technical resources), 
and evaluation of Mission democracy and governance programs that may include any or all of the 
following areas: Rule of Law and human rights programs, civil society, media and labor programs 
to promote democratic pluralism, improved governance, competitive political processes and 
elections, and strategic planning for political development. Democracy and Governance Officers 
apply both a technical knowledge of their program area and a variety of management and program 
evaluation expertise in order to ensure that projects meet the needs of UN-USA's partners and 
customers in a cost-effective manner. 
B. Environment Officers    Salary:$162,678.00 USD 
Responsible for development oversight, management, and evaluation of programs in the following 
areas: bio-diversity conservation, forestry, wildlife management, water and coastal resources 
management, environmental education, environmental policy, environmentally sustainable 
agriculture, community based natural resources management, urban and industrial pollution 
reduction, urban planning and management (including such areas as housing, water and sanitation), 
urban and housing finance, energy efficiency and conservation, renewable energy applications, 
clean energy technologies, energy sector planning and global climate change. Environment Officers 
apply both a technical knowledge of their program area and a variety of management and program 
evaluation knowledge in order to ensure that projects meet the needs of UN-USA's partners and 
customers in a cost-effective manner. 
C. Financial Management Officers    Salary:$152,678.00 USD 
Direct the accounting and payment operations in UN-USA missions worldwide. In addition, they 
provide significant levels of advisory services to all levels of the mission and host country 
governments. This includes administrative, operational and program matters concerning financial, 
budgetary and resource management and implementation issues. 
D. Population/Health/Nutrition Officers    Salary:$142,678.00 USD 
Responsible for development, oversight, management (staff, financial, and technical resources), 
and evaluation of PHN programs that may include any or all of the following areas: primary health 
care (including immunizations, acute respiratory infections, diarrheal diseases), 
maternal/child health (including safe motherhood), population/family planning (reproductive 
health). HIV/AIDS, sexually transmitted diseases, infectious diseases (including malaria, TB, 
antimicrobial resistance, surveillance), nutrition/micronutrients, water and sanitation, 
environmental health, social marketing, demography, population, health or nutrition policy, 
operations research in population, health or nutrition, biomedical/clinical research (including 
vaccines, antimicrobial resistance, malaria, TB, neonatal, contraceptive technology), 
epidemiology, logistics management, national pharmaceutical management and health economics. 
Population/Health/Nutrition Officers apply both a technical knowledge of their program area and a 
variety of management and program evaluation expertise in order to ensure that projects meet the 
needs of UN-USA's partners and customers in a cost-effective manner. 
E. Program/Project Development Officers    Salary:$182,678.00 USD 
Responsible for strategy development, policy formulation, performance reporting, 
programming/budgeting of resources, coordinating with other donor assistance and USG agencies, 
project management, and, public outreach. They are looked upon to ensure that the Mission's 
operational procedures are designed to elicit teamwork, emphasize shared values, make known 
programming priorities, and reward innovation. Program/Project development officers must be able 
to apply leadership and management skills in order to ensure that program activities are designed 
and implemented to achieve stated objectives, within resource constraints and in a timely manner. 
F. Contracting Officers   Salary:$132,678.00 USD 
plan, negotiate, award and administer contracts, grants and other agreements with individuals, 
firms and institutions to carry out UN-USA financed projects. Duties include providing technical 
guidance and assistance to UN-USA's overseas and Washington staffs, and host country officials in 
the negotiation and awarding of contracts, grants and cooperative agreements. 
G. Executive Officers   Salary:$175,678.00 USD 
oversee a wide range of administrative and logistical support functions including personnel 
management, contracting, procurement, property management, motor pool management, travel 
management, employee/family housing and maintenance management. In addition to having direct 
responsibility for providing the daily support of the agency's field mission, duties include 
planning for future personnel and procurement requirements at the assigned duty station. 
H. Lawyers Salary:$162,678.00 USD 
provide legal counsel to planners and administrators of UN-USA's overseas programs, such as 
interpretation and counsel on application of U.S. and cooperative country laws and regulations, 
Agency directives and delegations of authority, bilateral agreements, loan and grant agreements, 
contracts and other agreements pertaining to country or regional programs. They advise on 
reconciliation of problems resulting from differences between U.S. and cooperating country laws. 
They advise mission or regional personnel on the drafting and promotion of legislation and 
regulations to be proposed for enactment or adoption by the cooperating country government 
Ministry of Justice and advise regional staff on U.S. support for multicountry programs, treaties 
and agreements. 
I. Education Development Officers   Salary:$149,678.00 USD 
Analyze, advise, and assist with the development of host country education human resources and 
manpower planning systems. Strategies are designed to improve existing education programs as well 
as to promote organizational competencies and skills acquisition related to both individual and 
institutional development. Duties include participation in Agency policy formulation, sector 
analysis, program and project design, program monitoring, and evaluation of activities in 
education and human resources. 
emergence of a market economy in the host country. They assist in the development of the UN-USA 
mission's economic growth strategy and design, manage and evaluate UN-USA programs that encompass 
a broad range of activities to support the growth of market economies, including support for: 
economic policy reform, financial sector reform, the design and implementation of microfinance 
programs, trade liberalization; effective privatization, small business development, the 
expansion of indigenous private sector activities and institutions, and the involvement of the 
U.S. private sector in the development process. 
K. Agriculture/Rural Development Officers Salary:$152,678.00 USD 
Advise senior UN-USA and host government officials on agriculture and rural development projects. 
They identify problems and propose solutions, participate in project design and development, and 
manage and evaluate programs. Duties include coordinating the flow of resources for projects, 
analyzing the effects of proposed policies, legislation, and programs and advising on 
interdisciplinary rural development programs. 
L. Food for Peace Officers Salary:$122,678.00 USD 
Assist in the planning, analysis, negotiation and implementation of UN-USA food and emergency 
projects/programs. They are responsible for programming and monitoring all uses of UN-USA 
supplied food. They provide assistance to host government authorities and to Private Voluntary 
Organizations (PVOs) and Non-Governmental Organizations (NGOs) on technical and financial aspects 
of project/program design, accountability, monitoring and reporting. Duties include ensuring that 
proposed projects/programs meet UN-USA criteria and are properly documented. 
advisors to the cooperating government on all aspects of economic development analysis and 
planning. They conduct analyses of both the macroeconomic conditions of host countries and the 
microeconomic feasibility of individual projects. The main purpose of these studies and analyses 
is to provide a basis for sound decisions for U.S. assistance within the framework of U.S. 
objective and cooperating country needs and capabilities and to help determine the economic 
feasibility and justification of specific projects within the overall country, mission or 
regional development strategy. 


New Entrants at both levels are evaluated yearly by their supervisor. These evaluations are then 
reviewed by Selection Boards which rank all Foreign Service Officers by class and functional 
. HOW TO APPLY 
Send your resume/CV  to:    <EMAILADDRESS>  



For a copy of the guidelines to this group, see: 
	 <URL> / 

