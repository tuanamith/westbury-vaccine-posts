



"Phil P." < <EMAILADDRESS> > wrote in message  <NEWSURL> ... 
"joeblow" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... 
I'm not saying your cats will definitely become infected and symptomatic. I just wanted to you know what the risks are and the precautions you can take to minimize the risks. 
Herpesvirus infection isn't as serious for an adult as it is for a kitten. Primary FHV is usually self-limiting inside of 2-3 weeks.  However, following recovery, they could also become asymptomatic carriers and the infection could reactivate if they're stressed or develop another disease that stresses the immune system.  OR- they could be just fine.  Almost every cat in my shelter has been exposed to FHV- but only a few develop symptoms. The problem is you don't know which cats will resist infection and which will become symptomatic.   As I said, vaccination will prevent your cats from developing serious disease, but it won't prevent infection. 
Keeping the cats separated might be more stressful for all of them and it probably wouldn't be practical if the other cat remains there for any length of time.  If you decide to let them mix, at least make sure you have your cats inoculated with the UltraNasal vaccine.  L-lysine would be a good idea, too. 
Phil 



Our Koko has herpes,and  Kady came to live with us as a stray..... Keeping the kitten (kady) separate from Koko was next to impossible..... Koko was had respiratory problems  since day one ( a rescue from spca ) and an eye with constant discharge.  Even tho Kady was tiny, and not so well off herself... Kady has shown no problems with the herpes. Koko has been getting llysine every day for a year........ yes she still has outbreaks.  She was diagnosed 'officially' in July of this year , and is on interferon  orally once/ day and another antiviral eyedrop - Idoxuridine - 2  to 3 times a day , and this will be for life. At our website, www.richandlaurie.com   I have put up some pics of Koko's eyes. 
After this more 'major' outbreak,  the opthamologist did say that she had a scar on her cornea, which is the most 'definite' diagnosis of having the virus, other than the very expensive dna testing. 
Since she has been on the antiviral meds, she still has had one or two less serious outbreaks. She is in the midst of one now, actually.   I guess I just figured that if Kady got  the herpes also, since I already know how to deal with the disease, and since I already have to get the med, dosing one cat or two cats really didn't make a huge difference, other than the pocketbook, of course. 
And you are right, it costs about $300 bucks for 3 months of meds.... however, she will not need another course of  some of the original meds, unless she  has another more severe outbreaks.  If there is green gunk in the eye discharge, I add an antibiotic drop, but one that does NOT contain any steroidal meds, even tho they often  help eyes to feel better quickly.  It makes  the anti viral meds not able to work as well. 
I guess we are in a whole different category than 'occasional' stress related outbreaks of the virus.  It seems no matter the 'stress'  in Koko's life, she just almost constantly has  some eye irritation and discharge, and also 'snotiness' in her nose.  I can lay in bed at night and hear her breathe. 
laurie www.richandlaurie.com 




