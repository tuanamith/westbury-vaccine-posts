


------------------------------ 
Subject: CONTENTS OF THIS FILE 
Part 5: MEDICAL OVERVIEW 
11. *** Common health problems *** 
12. *** General medical information *** 
13. *** Medical reference material *** 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
11. *** Common health problems *** 
------------------------------ 
Subject: (11.1) Common diseases in ferrets 
------------------------------ 
Subject: (11.2) Overview of common health problems 
All of this section was written by Susan A. Brown, DVM. 
Most common health problems of the pet ferret 
------------------------------ 
Subject: (11.2.1) Noninfectious 
by Dr. Susan Brown, DVM 
A. GI Foreign Bodies [11.1] 
B. Aplastic Anemia 
C. Anal Gland Impaction 
D. Cataracts 
E. Cardiomyopathy There is a separate FAQ devoted to cardiomyopathy; see section [1.1]. 
F. Urolithiasis (Bladder Stones) 
------------------------------ 
Subject: (11.2.2) Parasitic health problems 
by Dr. Susan Brown, DVM 
A. Ear Mites [10.10] 
B. Fleas [10.9] 
------------------------------ 
Subject: (11.2.3) Infectious diseases 
by Dr. Susan Brown, DVM 
A. Influenza virus 
B. Canine Distemper 
------------------------------ 
Subject: (11.2.4) Neoplasia (Cancer) 
by Dr. Susan Brown, DVM 
Each of these four cancers has its own FAQ; see section [1.1]. 
A. Lymphosarcoma 
B. Insulinoma 
       This is a tumor of the pancreas leading to a high insulin    production and a low blood sugar. 
C. Adrenal Adenoma or Adenocarcinoma 
       This is a tumor of the adrenal gland. 
D. Skin tumors 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
12. *** General medical information *** 
------------------------------ 
Subject: (12.1) Do I need to worry about toxoplasmosis? 
Dr. Bruce Williams, DVM, says: 
------------------------------ 
Subject: (12.2) How can I get my ferret to take this medication? 
The method Rick Beveridge has used, with pictures, can be found at < <URL> >. 
------------------------------ 
Subject: (12.3) Where can I get medications at a discount? 
------------------------------ 
Subject: (12.4) Can ferrets have transfusions? 
------------------------------ 
Subject: (12.5) What anesthetic should my vet be using? 
Isoflurane, an inhalant.  Dr. Bruce Williams, DVM, says: 
------------------------------ 
Subject: (12.6) How do I care for my sick or recovering ferret? 
------------------------------ 
Subject: (12.7) My ferret won't eat.  What should I do? 
------------------------------ 
Subject: (12.8) What's Duck Soup?  Anyone have a recipe? 
The following comes from Ann Davis: 
ACME Ferret Company --- The Original  DUCK SOUP 
DUCK SOUP 
------------------------------ 
Subject: (12.9) What are normal body temperature, blood test results, etc.? 
------------------------------ 
Subject: (12.10) What tests might my vet want to run, and why? 
Dr. Michael Dutton, DVM, writes: 
    TESTS THAT ARE SPECIFIC FOR ONE DISEASE 
    TESTS THAT HELP DETERMINE A PARTICULAR ORGAN FUNCTION     (may not be specific to cause, prognosis, etc.) 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
13. *** Medical reference material *** 
------------------------------ 
Subject: (13.1) Who makes this product or medication? 
(This list was provided by Dr. Susan Brown.) 
Alkeran - Burroughs-Wellcome Co. 
Cytoxan - Bristol Meyers 
Fervac D vaccine - United Vaccines Madison, Wisc. 53713 (608) 277-3030 
Fromm D vaccine - Solvay Animal Health, Inc. Mendota Heights, Minn. 55120 
Keflex Pediatric Suspension 100 mg/cc - Dista Products Co. Division of Eli Lilly, Inc. Indianapolis, Ind. 
Lasix - Taylor Pharmacal Co. Decatur, Illinois 62525 
Lysodren - Bristol Meyers 
Nutrical - EVSCO Pharmaceuticals Buena, N.J. 08310 
PDS II - Ethicon, Inc. Somerville, N.J. 08876-0151 
Proglycem - Baker Cummins 800-347-4774 
------------------------------ 
Subject: (13.2) What books can I get or recommend to my vet? 
One excellent medical reference is 
Ferrets, Rabbits and Rodents - Clinical Medicine and Surgery, by    Elizabeth Hillyer and Katherine Quesenberry (1997) 
Another good reference work, a bit outdated but still worthwhile for both vets and others, is 
Biology and Diseases of the Ferret, by James G. Fox.  Lea and Febiger,    Philadelphia (1988).  ISBN 0-8121-1139-7. 
There is also a series out by the 
------------------------------ 
Subject: (13.3) Are there any other useful references? 
Dr. Bruce Williams, DVM, recommends these references on cancers: 
Marini, RP et al.  Functional islet cell tumor in six ferrets.  JAVMA      202(3):430-434, 1 February 1993. 
Dr. Susan Brown recommends these, on a variety of subjects: 
Daoust PY, Hunter DB. Spontaneous aleutian disease in ferrets. Can Vet      J 1978; 19: 133-135. 
Kawasaki, T. Retinal Atrophy in the ferret. J of Small Exotic Animal      Medicine 1992; 3: 137. 
Kociba GJ, Caputo CA. Aplastic anemia associated with estrus in pet      ferrets. J Am Vet Med Assoc 1981; 178: 1293-1294. 
Luttgen PJ, Storts RW, Rogers KS, Morton LD. Insulinoma in a ferret. J      Am VetMed Assoc 1986; 189: 920-921. 
Nguyen HT, Moreland AF, Shields RP. Urolithasis in ferrets (Mustela      putorius).  Lab An Sci 1979; 29: 243-245. 
== End of Part 5 == 


