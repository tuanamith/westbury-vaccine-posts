


      Vaccine may target obesity in the future: researchers       Oct 18 2:34 PM US/Eastern 

When babies receive shots against diseases like polio and measles, their vaccinations may in the future include protection against getting fat, according to researchers. 
Infection by certain pathogens triggers rapid increases in fatty tissue in animals, Nikhil Dhurnadha told the annual meeting of NAASO, the Obesity Society, in this western Canadian city. 


At the same time, the discovery that many more obese people than normal-weight people have been exposed to a certain virus suggests a link between obesity and viral infection. 
"Not all obesity can be explained by infection," said Dhurandhar, of the Pennington Biomedial Research Center at Louisiana State University in Baton Rouge. "Infections can be one of the causes." 
Researchers are reporting at the conference on other fat triggers that include a genetic tendency to store fat among groups whose ancestors survived famines, medications such as treatments for psychotic mental disorders, toxins in the environment like organochlorines, and infectious agents like bacteria, viruses and prions. 
"Obesity is multifactoral," Dhurandhar told scientists at the conference. 
In an interview with AFP, he said there is proof that at least 10 different pathogens cause obesity in animals. They include canine distemper virus, RAV7 and MAM1 avian viruses, the Borna virus in rats -- which is also linked with depression in humans, types of scrapie, three adeno viruses including AD5, AD36 and AD37 which cause fat gain in several species, and chlamydia pneumonae bacteria. 
Dhurandhar became interested in viral causes of obesity while working as a family physician in Bombay in the 1980s, during a severe outbreak of SMAM1, an adeno virus that kills chickens. 
Dhurandhar wondered how the virus affected people. He tested his own patients, and found 20 per cent of his obese patients had been exposed to SMAM1, and that those people were significantly heavier with lower cholesterol levels. 
He moved to the United States to conduct more research, and started working with Richard Atkinson at the University of Wisconsin. Because US authorities refused permission to import the Indian avian virus, the pair decided to work with adeno virus AD36. 
First, they infected laboratory chickens, mice and monkeys, all of which grew significantly fatter and had lower cholesterol. 
Then, because they could not test the virus on humans, they examined stored blood from 500 people in Wisconsin, Florida and New York. They found antibodies for AD36 in 30 per cent of the obese people, but only in 11 per cent of people with normal body weight. 
And, just as Dhurandhar earlier discovered among his Indian patients, the obese who had been exposed to the virus were 20 per cent heavier than other overweight people. 
Further tests on tissue from lab monkeys taken over a nine-year period showed that healthy monkeys newly infected by AD36 "gained 15 per cent body weight in six months, and dropped their cholesterol by 30 per cent." 
The scientists also studied 26 pairs of twins, and found that in cases where one twin had been exposed to AD36, in all cases their weight was significantly greater. 
"In 10 years, people may be able to walk into a clinic and be told that their obesity is due to X cause, such as genes, the endocrine system, or pathogens. That may have a more productive outcome than a blanket treatment right now, (which) is not very successful," said Dhurandhar. 
And because viruses are hard or impossible to treat, he said, prevention through vaccines will be key. 




