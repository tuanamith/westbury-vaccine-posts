


 <URL>  
The Bush administration's long-awaited plan on how to fight the next super-flu will likely include beefed-up attempts to spot human infections early, both here and abroad.  
Expect recommendations on how to isolate the sick. Governors and mayors are on notice to figure out who will actually inject stockpiled vaccines into the arms of panicked people.  
Bush on Tuesday is visiting the National Institutes of Health to announce his administration's strategy on how to prepare for the next flu pandemic, whether it's caused by the bird flu in Asia or some other super strain of influenza. Federal health officials have spent the last year updating a national plan on how to do that.  
Stockpiling drugs and vaccines is just one component.  
"Understand that a lot of the things we need to do to prepare are not related to magic bullets," said Michael Osterholm of the University of Minnesota, an infectious disease specialist who has advised the government on preparations for the next worldwide flu outbreak but has not seen the final version of the plan.  
How to provide food supplies, everyday medical care for people who don't have the super-flu, basic utilities and even security must be part of the plan, Osterholm and others have counseled the Bush administration.  
"In this day and age of a global economy, with just-in-time delivery and no surge capacity and international supply chains _ those things are very difficult to do for a week, let alone for 12 to 18 months of what will be a very tough time," he said.  
While it is impossible to say when the next super-flu will strike, there have been three pandemics in the last century and influenza experts say the world is overdue. Concern is growing that the bird flu could trigger one if it mutates to start spreading easily among people _ something that hasn't yet happened.  
Already the government is buying $162.5 million worth of vaccine against that bird flu strain, called H5N1, from two companies _ Sanofi-Aventis and Chiron Corp. _ in case that happens. It also is ordering millions of doses of Tamiflu and Relenza, two antiflu drugs believed to offer some protection against the bird flu, stockpiles that the pandemic plan is expected to order be augmented.  
Lawmakers angry at months of delay have already given Bush money to begin those preparations: $8 billion in emergency funding that the Senate, pushed by Democrats, passed on Thursday _ and an amount considered close to what federal health officials will need.  
The money is to be spent at the president's discretion, but senators said it should be used both for medications and vaccine and for beefing up hospitals and other systems to detect and contain a super- flu.  
Sen. Edward Kennedy, D-Mass., called the funding "a victory for common sense."  
But amid growing public fear about the bird flu, federal health officials are beginning to wonder about a backlash if the worrisome strain in fact fizzles out _ or is contained in birds, as specialists are struggling to do _ and never threatens Americans' health.  
"Will critics say, 'We have been crying wolf,' and lose the sense of urgency we feel about this issue?" Health and Human Services Secretary Mike Leavitt asked last week.  
They shouldn't, he stressed _ because pandemic preparations to improve how vaccines are made and diseases are detected will improve public health overall.  
"If it isn't the current H5N1 cirus that leads to an influenza pandemic, at some point in our nation's future another virus will," Leavitt said.  



