


    The World Today - US doctor comes up with solution to drug shortage 
[This is the print version of story  <URL> ] 
    The World Today - Wednesday, 2 November , 2005  12:34:00     Reporter: Karen Percy 
ELEANOR HALL: As health authorities around the world try to work out how to deal with a possible flu pandemic, an American doctor has come up with a potential solution to any drug shortage, and it's a solution that goes back more than six decades. 
The Oregon-based Dr Joseph Howton from the Adventist Medical Centre says using the antiviral treatment Tamiflu, together with a drug called Probenecid, makes the flu treatment last twice as long. 
It's a technique similar to that used during World War II to preserve supplies of penicillin. 
Karen Percy has been talking to Dr Howton about how he made the connection. 
JOSEPH HOWTON: Recently, I was reviewing the studies and also Tamiflu, and I found that they talk about being careful when you use Probenecid with also Tamiflu, because it raises the drug level. And I thought about it from the opposite perspective  well this is great, this a wonderful side effect, why don't we consider using Probenecid to, exactly for that reason, we have this tremendous shortage of the drug. 
And there was one study in 2002 that documented the profound effect of Probenecid on Tamiflu, it, actually it increased the level by two and a half times, and at that time it wasn't really being looked at as an issue of, you know, something to use during a shortage. But if you look at Tamiflu the same way we looked at penicillin during World War II, and penicillin was in very short supply, and as a result we gave Probenecid with penicillin to enhance the effect and extend the supplies, why not use that same idea for Tamiflu. 
KAREN PERCY: As I understand it, Probenecid is used primarily to treat gout. Just explain, I guess, a little more to those of us who don't have a medical or science mind, about how it actually might work in cooperation with Tamiflu. 
JOSEPH HOWTON: Tamiflu is a drug that's, it's a weak organic acid, and as such Probenecid, it blocks organic acid from being excreted by the kidneys, so other drugs like penicillin and cephalosporins or other antibiotics all by the same mechanism, the Probenecid blocks the enzymes that allows the excretion of the drug via the kidneys, and therefore you have much less of the intact drug excreted. It stays around and recirculates and you have better virus-killing effect. 
KAREN PERCY: Tell us then the significance of this discovery of yours. 
JOSEPH HOWTON: I don't want to let it get to my head too much. It just means that since the supplies are so limited, that we can use it one of two ways. We can give a half-dose of, or even less than half of a dose of Tamiflu, along with Probenecid, and we should get roughly the same levels as if we had just given a full dose, so therefore we can treat twice as many people with the same amount of drug. 
Another approach might be to use a standard dose of Tamiflu, along with Probenecid, and thereby getting a higher than usual tissue level of Tamiflu, which might actually be necessary, depending on how things turn out with this virus. 
There are some mouse studies that indicated that we will need higher levels of Tamiflu in the system to effectively combat the virus. 
KAREN PERCY: There is some criticism that Probenecid won't be enough in and of itself, that the world still needs to be producing a lot more of the flu vaccines, and that this is only sort of one element to a broader plan. Do you agree with that? 
JOSEPH HOWTON: Oh 100 per cent. Yeah, really the only hope to the only way we're going to prevent just massive numbers of fatalities in the event or a major pandemic is by a vaccine. So the whole idea of a drug like Tamiflu is to buy us time, so that the vaccine can be developed. 
And if you kind of look at it in the analogy of a forest full of dry kindling and a spark gets set off, you want to stamp out that spark as quickly as possible before the whole forest goes up, and the same kind of thing with a pandemic, you know, it's possible, the virus is just mutating like crazy, so it's possible that it'll eventually mutate to a point that it can spread easily among humans, and when and if that happens you want to move in quickly and quarantine the area, give Tamiflu to everyone who's exposed and treat people with that, with Tamiflu, and but try to stamp out that spark and then hopefully buy time that you can take that strain of virus that's causing the problem, and develop a vaccine that'll be effective. 
KAREN PERCY: One of the issues with Tamiflu is that a) there are patents around the world, as well there's just not the ability to produce it quickly enough. Are there similar issues that might relate to Probenecid? 
JOSEPH HOWTON: Fortunately, Probenecid is fairly easy to produce, and it's very widely available. So it's been around since 1951, and it's, I don't think there should be a short supply of that. 
ELEANOR HALL: And that's Oregon doctor Joseph Howton speaking to Karen Percy. 
 2005 Australian Broadcasting Corporation 

