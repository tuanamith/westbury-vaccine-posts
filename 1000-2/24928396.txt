


 <EMAILADDRESS>  wrote: 
Good afternoon, and no apologies necessary. I'm cutting this down to  just a few points. You can find whatever resources you need to clarify  the rest of the issue on your own. 
<...> 
I should've moved the point at the bottom of my previous post with the  asterisk up and capitalized the whole thing. I think it's relevant when  comparing 1955 to 2005. Here it is again:      *- There's one point I didn't address above. As more people are      vaccinated, there are fewer vectors (in this case, other      children) carrying and transmitting the disease. Accordingly,      one vaccinated today should have a concomitantly reduced risk      than one did in 1955 (which was half). That doesn't mean      children should no longer be vaccinated. As I've pointed out,      unvaccinated children still contract polio and can transmit it      to other unvaccinated children. 	 
One of the goals of public health officials is to vaccinate the largest  possible number of people in the fastest, safest manner for that reason  -- to reduce the number of vectors and thereby exponentially reduce the  risks of contracting the disease in question. 
<...> 
Wrong -- you're suggesting a universality in communicable diseases when  there's actually great variation in persistence and virulence. HIV/AIDS  certainly hasn't tapered off. Neither have diseases for which we now  have vaccinations like measles -- and we still have periodic outbreaks  because some people avoid vaccinating their children for bogus reasons  (I personally don't consider religious reasons to be respectable). 
<...> 
I did address it at least tangentially in the asterisked part at the end  of the previous post, which I repasted above and I believe it was  addressed in the link about polio eradication efforts in Senegal from a  couple posts back. 
<...> 
That depends on your criteria. I've already told you that I don't have  an automatic objection when one vaccine doesn't pan out -- that's just a  fork in the road. Since multiple efforts to cure or prevent a disease  seldom duplicate each other (i.e., one research group focuses on one  facet of a disease and other groups focus on its other facets), it's  sort of a shotgun approach. That works faster in terms of finding a  vaccine or cure than a rifle approach -- working on one facet of a  disease at a time -- could hope to outside of the random chance of  hitting paydirt early in such efforts (possible, but less likely). 

I might if the decisions were unilaterally made by one person or by the  group of people who stood to profit. Those decisions, though, are made  by committee. Members with conflicts of interest ordinarily recuse  themselves in matters pertaining to their work. I find such a process,  even if it includes some who profit, fair and reasonable. 
<...> 
More precisely, The Salk vaccine trial in *1955* showed that vaccinated  children were over half as likely to contract polio AND those who did  contract it had cases which were non-fatal and less severe than  unvaccinated children who contracted it. 
That was with a population starting with 0% vaccinations against polio.  Since that time, there are fewer vectors -- real or potential -- in the  general population, so vaccination further reduces the risks of polio. 

You're welcome. 
<...> 
Prions, though, aren't connected to heart disease, cancer, diabetes,  etc., which is what you raised. Stick to one subject at a time. We can  discuss TSEs if you want; they're rare and far less common, especially  in humans, than the diseases to which you referred. 
<...> 
You're welcome. 

All of that would be accounted for through statistical analysis. 

This happens and it's very rare. People shouldn't make decisions on the  basis of anomalies or rare occurrences, but on the norm. In the normal  scheme of things, most vaccines are properly constituted and safe. 

Also rare. 

Hasn't happened yet. Your risks of being in an auto crash are much  higher than any of these three. Do you drive? Will you ever again ride  in a car? 

Very rare. 

You can get a vaccination in any location you desire, if you're willing  to pay for it. The risks of contracting an infection at a hospital are  still worth the benefits of vaccination, imo. 

The cost of vaccines is often absorbed by local health departments -- so  taxpayers foot the bill so every child can be vaccinated. It costs a lot  less to vaccinate a group of children than to treat one case of polio,  measles, or whooping cough. My city has programs like Shots for Tots  that don't charge fees at all, as well as other programs for other  diseases (like flu) without charge for those on Medicaid/Medicare or who  have either no or not enough insurance. 
I also found the following while ago in a related search. It has  something to do with the issue. It's a reprint from our local paper  about a teen mother whose unvaccinated baby died from whooping cough.  The baby was too young for vaccination, but it highlights the fact that  these diseases which are preventable by vaccination still exist and they  still kill.  <URL>  
(There's a link at the bottom of the page for other stories. I encourage  you to at least read the ones in the General section at the top of that  page, and to at least skim through the rest to see the potential effects  of not getting proper vaccinations for yourself or children.) 
<...> 
Many people share that opinion. 

