


WASHINGTON (AP) -- People injured by a vaccine against bird flu or anthrax would have to prove willful misconduct to bring a claim for damages against drug manufacturers or distributors, according to legislation being drafted behind the scenes by Republicans. 
A 10-page draft of the legislation obtained by The Associated Press says it would be up to the Health and Human Services secretary to declare that such misconduct occurred. If that declaration is made, the case must be heard in federal court. 
The measure, which would be included in a spending bill, would bar any punitive damages and limit awards for physical and emotional pain and suffering and other noneconomic damages to a maximum of $250,000. 
The draft legislation was provided to the AP separately by two parties opposed to its provisions, who did not want to be identified. 
An aide to Sen. Bill Frist, R-Tennessee, confirmed the majority leader was looking to add the liability protections to a spending bill. 
Amy Call said the legislation is important because "it would be a pity to appropriate $7.1 billion to purchase vaccines and antivirals but have no capacity to produce them." 
. . . . .  
   It's a toughie really ... why would any company agree to rush    a quasi-experimental vaccine into production if it's only going    to be sued into oblivion for any side-effects ? If they WON'T    make it however, MILLIONS could die.  
   Lawsuit shields can allow a "greater good" drug to get to    the market quickly - but can also encourage manufacturers    to be lazy about quality and silent about side-effects. 
   Kinda a chicken/egg situation.  
   The only alternative would be for the government itself to    try and produce 100+ million doses of vaccine in a short time.    While certain military labs CAN do the work, they're not    set-up for high-volume production. $7.1 gigabucks probably    isn't enough to actually BUILD adequate facilities for the    government - but it IS enough to fund existing commercial    vaccine factories.  
   Third option - the government TAKES OVER an existing vaccine    factory (or two) - and uses the facilities, perhaps even    'drafting' the staff. That way the company didn't make the    stuff, didn't profit by it and thus isn't responsible for    any side-effects or ineffectiveness. Of course option 3 is    kinda harsh, really throwing the governments weight around. 


