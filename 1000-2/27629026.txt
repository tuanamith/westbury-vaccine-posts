


THOMAS MAIER, "A shot in the dark?", Newsday, November 20, 2005, Link:  <URL> ?= coll=3Dny-health-headlines 
America's homeland defense program is spending more than $1 billion on anthrax vaccines earmarked for wide civilian use despite uncertainty about their effectiveness and an ongoing debate about potential health problems, Newsday has found. 
The vaccine stockpiling is a key element of the federal Project BioShield program, which was awarded $5.6 billion in funding in 2004 to develop drugs and vaccines to protect Americans against biological and chemical attacks. It constitutes the largest federal effort ever to protect civilians from an anthrax attack. 
In May, BioPort Corp., the only manufacturer currently licensed in the United States to produce an anthrax vaccine, won a $123-million contract to make 5 million new doses for the public. And earlier this month, federal officials doubled their request, saying they wanted to buy another 5 million doses for approximately the same amount. 
Last November, another firm, California-based VaxGen, received an $877-million contract, plus up to $69 million in other potential fees, to manufacture 75 million doses of an updated vaccine. The product, which still lacks approval by the U.S. Food and Drug Administration, will not be available until 2007, company officials say. 
Federal officials say an airborne anthrax attack could kill thousands of people in an urban setting like New York and tout the vaccines as key parts of the civilian defense program. 
But while a body of scientific research shows that the current vaccine is effective if administered before skin exposure to anthrax - and the rate of serious side effects is comparable to other common vaccines - several public health experts have raised questions about the vaccine's safety and whether it would work following an airborne attack. 
David Ozonoff, a professor at Boston University's School of Public Health, said there was "scant" evidence the vaccine will work to treat people who inhale the airborne spores. He said studies show antibiotics as the most effective treatment, and that the vaccine could cause potentially serious health problems among civilians. 
"The number of doses they are amassing is wildly out of proportion to any possible threat from anthrax," Ozonoff said. "What the benefits are is very unclear and there are always [health] risks ... when you vaccinate a whole lot of people." 
Hillel W. Cohen, an epidemiologist at the Albert Einstein College of Medicine in the Bronx, agreed. 
"The only possible benefit of a vaccine is if there's a danger of exposure and that danger is small because of the technological hurdles of weaponizing anthrax," Cohen said. " ... It's not something you can do in your basement." 
If an anthrax attack were to occur today, the nation would rely on stocks of the BioPort vaccine, which, like the VaxGen product, would be provided in combination with antibiotics. 
The only major study of the use of the BioPort vaccine following inhalation exposure found it ineffective on laboratory animals unless used in conjunction with antibiotics. 
VaxGen also is conducting animal studies of its vaccine, but company officials say they are not yet certain it will work safely and effectively on humans exposed to airborne anthrax attacks. 
"We'd hopefully achieve a high level of protection, and the alternative is severe disease," said Harry Keyserling, a pediatrics professor at Emory University School of Medicine in Atlanta and a key researcher in early VaxGen trials. 
Still, several prominent members of Congress are skeptical of the amount of federal money going to VaxGen. 
"I do question the BioShield acquisition strategy being pursued that bets 800 million dollars on an untested vaccine ... ," said Rep. Christopher Shays (R-Conn.), chairman of the House Subcommittee on National Security, Emerging Threats and International Relations. 
"In the event of an attack, we need to know the vaccines and medicines in the national stockpile are the best modern science can produce," Shays said. 
The issue of whether the vaccines themselves may cause health problems, and even death, also remains in dispute. 
In documents of the FDA, Newsday found reports of more deaths and serious health problems among anthrax vaccine takers than previously reported. 
Until late last year, the FDA had listed reports of six deaths and 1,850 "adverse" reactions since 1990, ranging from minor redness at the inoculation site to severe cardiovascular and respiratory system problems, that "possibly" were caused by the BioPort vaccine. The government's monitoring system collects voluntary reports of illness, but does not determine exact causes. 
But in a little-noticed report issued in December, the FDA said 16 deaths were possibly linked to the BioPort vaccine. After Newsday asked about other fatalities cited in FDA filings, the agency upped the total number of fatalities possibly linked to the vaccine to 21 - including one the agency said had been "incorrectly coded" in its database. 
The same report tallied more than 4,100 illnesses, including 347 it characterized as "serious," as possibly associated with the vaccine. 
Government officials say the rate of serious illness associated with anthrax injections is lower than that for other common vaccines such as influenza, smallpox, tetanus, diphtheria and hepatitis. 
About 9 percent of all health problems tied to the BioPort vaccine are considered "serious," compared to 14 percent for the other vaccines combined, said the FDA. 
"This vaccine is as safe as any," said Kim Brennen Root, a spokeswoman for BioPort, of Lansing, Mich. 
Although humans can contract anthrax poisoning through breaks in the skin or the gastrointestinal system, the civilian vaccine program is focused on post-exposure treatment of the deadliest form, inhaled anthrax. Initial symptoms of inhalation anthrax include mild fever and muscle aches, but shock, severe breathing problems and often meningitis then develop, according to the federal Centers for Disease Control and Prevention. 
The BioPort vaccine contains proteins from the anthrax bacteria called "protective antigen," and once in the bloodstream the vaccine makes the body produce antibodies to the antigen, so the bacteria can't produce the anthrax disease. The VaxGen product would work in a similar manner but with technology utilizing new combinations of genetic material. 
Under current plans, contaminated civilians, along with others who suspect they were exposed, would be offered a regimen of antibiotics followed by several injections of vaccine. 
But while BioPort's FDA license authorizes the vaccine as a preventative against anthrax contracted through skin exposure, federal officials have begun efforts to expand that authorization to cover inhalation anthrax suffered by civilians. 
Under the expanded powers in Project BioShield, the BioPort vaccine could be used following an airborne anthrax attack, even without a federal license for that use. The VaxGen product also could be administered to civilians, even if it were still unlicensed. 
Federal officials say the catastrophic potential of an anthrax attack would justify any available medical weapon. 
"We know that the consequences of such use could be very grave," the Department of Homeland Security said of the possibility of an anthrax attack. But data about whether either of the vaccines would be effective in treating inhalation anthrax victims are scarce. 
As the primary evidence that its vaccine would work for post-exposure, BioPort cited a 1993 study in the Journal of Infectious Diseases that was sparked by concerns about anthrax attacks during the Persian Gulf War. 
Tested on monkeys 
The researchers, led by Arthur Friedlander of the Army Medical Research Institute of Infectious Diseases at Fort Detrick in Frederick, Md., exposed six groups of 10 Rhesus monkeys each to anthrax contained in a spray. Subsequent treatment included the BioPort vaccine by itself, the vaccine in combination with antibiotics or antibiotics alone. Members of a control group received only saline. 
The untreated monkeys had a death rate of 90 percent, while 80 percent of the monkeys given only the anthrax vaccine died. The groups treated with the antibiotics Ciprofloxacin or Doxycycline showed death rates of 11 percent and 10 percent, respectively. Another group of animals took a combination of Doxycycline and the vaccine. All survived. 
"This suggests that antibiotic treatment, begun early after exposure, prevented the infection from fully developing," the study said, adding that the vaccine "may provide an additional degree of protection against relapse" by killing spores remaining in the body after antibiotic treatment. 
"We know that antibiotics treat the symptoms of anthrax, but antibiotics don't kill the spores," said Root, the BioPort spokeswoman. 
Beyond that, Friedlander said, the vaccine's effectiveness in treating humans already exposed to anthrax spores remains uncertain. 
The vaccine was "not meant to be given after exposure," Friedlander said in an interview. "The vaccine alone doesn't protect and we wouldn't expect it to protect" those contaminated with anthrax. 
In touting their vaccine, VaxGen officials also note the limits of antibiotics. 
Lance Ignon, VaxGen's vice president of corporate affairs, noted CDC recommendations that anthrax patients take antibiotics for only up to 60 days. He said patients often don't follow the prescribed schedule, while others develop resistance to antibiotics over time or cannot tolerate them long-term. 
Beyond the unanswered questions about efficacy, there is debate about whether the vaccines themselves are dangerous. Some activists and military personnel argue that the government is moving too quickly with plans for a civilian vaccination program, without assurances that the BioPort and VaxGen vaccines won't cause serious health problems. 
"There's been a tremendous amount of spin by the government," said Meryl Nass, a physician in Maine and director of the Alliance for Human Research Protection, an advocacy group that has been a long-time critic of the anthrax vaccine program. 
Several deaths reported 
The BioPort vaccine was first licensed in 1970, primarily for use by agricultural workers in danger of skin exposure to anthrax from animals. The vaccine's health effects have been studied extensively in recent years, beginning with the 1991 Gulf War when thousands of U.S. troops rolled up their sleeves for injections. 
Most studies have found low levels of serious illness, although allegations of severe health problems, including several deaths, are detailed in at least one federal report and in several lawsuits. 
A 2002 study by the National Academies' Institute of Medicine examined the cases of people who took a total of nearly 2 million doses of the vaccine, primarily in the late 1990s. 
Researchers pored over illness reports in examining possible patterns of long-term health problems and gender differences in reactions to the vaccine. They said "limited scientific data" suggested that the vaccine with antibiotics "could provide post-exposure protection" from inhaled anthrax spores. The vaccine, they said, was "sufficiently safe and effective." 
In 2003, a study by academics from universities including George Washington in Washington, D.C., and Johns Hopkins in Baltimore reviewed health problems reported by some of the 500,000 military personnel who took the vaccine between 1998 and 2001. 
The lead researcher, John Sever of George Washington, said in an interview that the inquiry could find no "unexpectedly high rate" of serious adverse reaction to the vaccine. The study found six known deaths at that time were either "unrelated" to the vaccine or "unclassifiable." 
In 2002, however, a U.S. General Accounting Office study of Air National Guard and Air Force Reserve members who took the vaccine revealed that 84 percent reported some adverse reaction - more than double the approximately 30 percent rate reported to the FDA and included in BioPort's packaging at the time. The preponderance were minor, but almost 20 percent were considered serious - chills, fever, nausea and dizziness, with some symptoms lasting more than seven days, the GAO said. 
"The implications were that the vaccine was part of the problem of getting sick, and we recommended that they should be following up," said Nancy Kingsbury, who oversaw the GAO study. 
The Army rejected the GAO's call for more active surveillance, saying it already kept track of and studied health problems linked to anthrax vaccinations. 
Defense Department records show that Army Reservist Spc. Rachel Lacy took vaccinations for anthrax and smallpox at Fort McCoy in Wisconsin, while preparing in March 2003 for overseas deployment. Subsequently, Lacy developed pulmonary and neurological problems that led to inflammation of her lungs. She died the following month. 
Lacy's father, Moses, said he believes the combination of vaccinations overwhelmed his daughter. "I do think the anthrax vaccine contributed to my daughter's death," he said. 
After reviewing Lacy's case, two federal panels said the evidence "favored a causal relationship" between her death and the vaccines, although they could not establish a conclusive link. 
Root, the BioPort spokeswoman, denied any link between Lacy's death and the vaccine. 
Other U.S. service members have cited the vaccine's alleged ill effects in lawsuits challenging the military's compulsory use of the BioPort vaccine. Hundreds of troops have refused to undergo the vaccine regimen, and some have faced court-martial. 
Three advocacy groups have filed court papers contending that the FDA improperly granted "emergency authorization" for the military to use the vaccine against possible airborne anthrax attacks, and ignored evidence it was unsafe. 
Last year, U.S. District Judge Emmet Sullivan, of Washington, D.C., temporarily halted the military program, questioning the FDA's approval of the vaccine for inhalation anthrax cases. Sullivan later allowed the revival of the inoculation program, after the military made it voluntary. 
Some health problems, including headaches and fatigue, also have cropped up in early trials of the VaxGen vaccine, company officials said. However, VaxGen chief executive Lance Gordon called the reported health problems "not significant" because of the small initial testing sample. 
But BioPort's vice president of medical affairs, Tom Waytes, emphasized VaxGen's early difficulties and said it was time for them "to go back to the drawing board." 
Despite the safety debate, BioPort and VaxGen continue to vie for the $5.6 billion in Project BioShield money. 
This year, BioPort said it has used Jerome Hauer, former acting assistant secretary of the HHS Office of Public Health and Emergency Preparedness, as a lobbyist in Washington. 
Last year, BioPort hired Louis Sullivan, the former HHS secretary under President George H.W. Bush, as a consultant to help land a new federal contract for its anthrax vaccine. 
Sullivan said he set up a meeting for the company with government scientists in October 2004. The following month, BioPort announced it would be manufacturing 5 million anthrax vaccine doses for the civilian stockpile. 
How the vaccine works: In general, the anthrax vaccine works the same way as tetanus, rabies and other inoculations. 
(A) A vaccine is made from an antigen isolated or produced from the disease-causing organism. In the case of anthrax, the existing vaccine is culled from proteins in the bacteria and (B) injected into the bloodstream. (C) Once it recognizes the antigen, T cells in the immune system trigger B cells to neutralize it and another type of T cells to kill it. 
The process produces memory cells that remain ready to mount a quick response against subsequent infection from the same agent. That's why, for example, a childhood vaccination generally protects against a disease for a lifetime. 
How it's taken 
As a preventative, it is taken in six does of 0.5 milliliters each over an 18-month period. For those already exposed, treatment is antibiotics with three doses of vaccine. 
Side effects 
Mild: Soreness, muscle and joint aches, headaches, chills, fever, fatigue and nausea. 
Severe: May range from serious allergic reaction to rarely, death. 
Anthrax, in brief 
Anthrax spores exist all over the world and become dangerous only when they make contact with human blood, organs and tissues. There are three types of anthrax exposure: 
1: CUTANEOUS 
=B7 Bacteria enter through a break in the skin. 
=B7 Handling contaminated animal products, such as meat, wool or hides. 
Death is rare if antibiotic therapy is given. 
2: GASTROINTESTINAL 
=B7 Eating raw, undercooked, contaminated meat.* 
Death in 25% to 60% of cases. 
3: INHALATIONAL 
=B7 Inhaling at least a deep breath of anthrax spores. 
If left untreated, death rate is almost 100%; even when treated, 45% to 80% of patient's die. 
1=2E3 million Number of military personnel who have taken vaccines for anthrax since 1990 
5 million+ Number of individual doses supplied since 1990 
$123 million Amount BioPort Corp. of Lansing, Mich., is being paid to make 5 million new doses of its vaccine. 
$877 million+ Amount VaxGen, Inc., of Brisbane, Calif., is being paid to develop 75 million doses of an updated vaccine. 
SOURCES: National Health Museum, Department of Defense, Centers for Disease Control and Prevention, Food and Drug Administration, Anthrax Vaccine Immunization Program 
Researched by J. Stephen Smith 


