



cathyb wrote: 
If cathy consulted the Oxford dictionary, she would see the term doesn't satisfy the definition.  First, it isn't a personal characterization.  Second, it isn't being used to avoid debating mha topics, it's being used to explain why others defend mainstream medicine in a newsgroup devoted to mha topics.  The real reason that "pharma bloggers" attribute the ad hominem to other posters is to draw attention from their own fanatical use of them.  cathy, meanwhile, uses ad hominems in a majority of her posts (to me, at least.)  She would like to persuade others that "pharma blogger" is an ad hominem only because she is one. 

Expressing one's opinion is another matter.  Cathy, by contast, obsessively attacks those who criticize mainstream healthcare, ridicules those who express confidence in natural medicine, and promotes vaccine without medical training.  The term "pharma blogger" was created to describe the behaviour, not the person. 

There is, actually, a common thread between industry blogging in a web blog and industry participation in a newsgroup:  both are done under the pretense that the poster is not professionally affiliated.   
PeterB 


