



DougL wrote: 
Do you know exactly how the money is to be spent? All reports are vague, but the lions share seems to be about 2bn for "experimental anti-virals" and vaccines which will probably have no effect. There will also be some money set aside for state and local governments to "prepare". 
Note that this 2bn is for 20 million doses. Am I the only one who can do the math? That's $100/dose, and I have to wonder how much of that is profit (since, after all, it's my money). If these drugs really did work, then why not spend money to build factories to produce a lot more? 
There's was lots and lots of money assigned for "Homeland Security" and look what good that did for Katrina. So forgive me if I am a bit skeptical as to what good this 7bn will do. 

Yes, especially since once the medical industry catches wind of increased demand, they will send the price of respirators through the roof, and justify it as the effect of free-market. 

Don't get me wrong - my criticism is not partisan. 

Dems suck even worse than Republicans. They are spineless blowhards who, once this plan was revealed, sputtered that it was their idea first. 
It bothers me that when the US faces a problem the solution always involves throwing money at it. Especially in a time when the Federal deficit is enormous, and we have two wars going on (remember Afghanistan?) and Katrina (another problem "solved" by throwing huge Federal sums at it). 
It also bothers me that our government seems to have the "lets use the guy down the hall" syndrom, to wit, Harriet Miers. Who's the guy down the hall with Dick Cheney in the White House? 
In the list below (from Forbes, which also didn't have an actual breakdown), where's the money for expanded research? Let me translate the breakdown for you: 
    * Manufacturing a 20-million-course vaccine stockpile that would be directed at the current strain of H5N1, and developing the capacity to produce 300 million courses of the vaccine within a six-month period. This latter point would involve expanding existing egg-based production facilities while adding cell-based capabilities and adjuvants, or ways to make the vaccine more efficient. This is not a plan to actually give the vaccine to 300 million Americans, but simply to have the capacity to make the vaccine, Leavitt stressed. 
(Federally funded expansion of Sanofi Pastuer, Chiron, GlaxoSmithKline, MedImmune Vaccines Inc, facilities - without committing to "giving away" vaccine these companies can still charge what they want. Distribution deals will be struck with other large corporations. Note that even these companies themselves admit that any vaccine will almost certainly be inneffective against a mutated bird flu.) 
    * Stockpiling 20 million courses of several different antivirals, such as Tamiflu, by the end of 2006, and up to 81 million courses by the summer of 2007. 
(Huge money to Roche, more distribution and storage deals with Haliburton. These drugs are unproven and unlikely to work from what I've read.) 
    * Establishing international and domestic surveillance networks of labs, epidemic investigators and rapid-response teams. 
(What does this entail? What value does it bring? Buying internet connections and web cams for blood labs? If it involves equipment, huge money to contractors. I'm open minded here but I have to see specifics.) 
    * Improving communication abilities. 
(more equipement, more training - more money for contractors. Communication is good, but there's no indication of how this fits into a response plan in general.) 
    * Ensuring state and local preparation. 
(What does this entail? An audit? If so, that's a small army of $200/hr consultants to "ensure" that various local governments are prepared. This is something I least like.) 
I am a moron to even bother laying this out for you, since you've obviously decided that if the federal government decides to spend money, then the problem is solved. 
When 50 million Americans die of the bird flu, I'm sure you'll be saying "well, it could have been 100 million and boy that $7bn of pork really saved our asses!" 


