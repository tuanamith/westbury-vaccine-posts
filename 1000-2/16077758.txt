



CONGRESS SET TO PASS LAW ELIMINATING LIABILITY FOR VACCINE INJURIES 
Janwillem van de Wetering: "Greed is a fat demon with a small mouth and whatever you feed it is never enough." 
[This is the next-to-last step before complete and total tyranny over your health. 
While there has long been a Vaccine Injury Compensation Fund (VICP) to protect pharmaceutical companies from law suits when their vaccines kill or injure children, the Biodefense and Pandemic Vaccine and Drug Development Act of 2005 would make it impossible for anyone to seek compensation or pursue any legal action whatsoever if a drug or vaccine is categorized as a "countermeasure." 
The Homeland Security Act of 2002 provided liability bail-outs for Eli Lilly's vaccine mercury preservative thimerosal which is contained in every flu vaccine, but this new act gives Big-Pharma the kitchen sink. This unprecedented legislation would establish the Biomedical Advanced Research and Development Agency (BARDA) which will operate secretly, exempt from the Freedom of Information Act. BARDA would decide which vaccines and drugs are considered "countermeasures." 
The legislation goes so far as to say (in legalese) if death occurs because the "countermeasure" (drug or vaccine) administered was mislabeled you still cannot seek compensation. The Model Emergency Health Powers Act (MEHPA) failed to implement similar legislation on the state level back in 2002, but many of its important aspects were rolled into the Homeland Security Act. 
The only step left for total health tyranny would be to implement a state of emergency. Under the Homeland Security Act, this would give the Secretary of Health and Human Services the power to implement forced vaccinations and drugging with experimental, untested "countermeasures." Those who refuse forced administration would be subject to incarceration. 
Meanwhile everyone waits for explosive indictments, which serve as the perfect smokescreen for the end-game in health tyranny legislation. - MK] 
 <URL>  
For immediate release Wednesday, October 19, 2005 
 <URL>  
Issues and Allegations: Medical Malpractice Background Conservative Perspective Liberal Perspective Notes and Sources  Email this page 
WHY WE USE "CONSERVATIVE" AND "LIBERAL" 
Background Americans injured while receiving health care can sue providers for medical malpractice under governing state tort (injury) law. If the suit is successful, the victim can be compensated for the cost of medical treatments and lost wages. The victim can also be awarded financial compensation for pain and suffering, mental anguish and other less tangible but painful symptoms of distress. Punitive damages can also be awarded to punish the defendant for anti-social actions. 
Health care providers buy medical malpractice insurance to protect themselves against potentially devastating claims. The insurer agrees to investigate claims, provide legal representation and accept financial responsibility for up to the amount stipulated in the policy. A typical policy provides $1 million of coverage per incident and $3 million in total coverage per year.[1] 
Insurance rates (premiums) vary dramatically by medical specialty. High risk specialties, such as obstetrics, usually pay higher premiums. Rates also vary based on the amount of competition among insurers within a geographic and specialty market. 
The problem Medical malpractice premiums are rising rapidly with potentially negative effects on health care costs and doctor access. 
The CONSERVATIVE VIEW: 
"One of the major cost drivers in the delivery of health care are these junk and frivolous lawsuits." 
                  Supreme Court Appointee President                     George W.Bush 
Medical malpractice lawsuits are a major factor driving health care costs higher. The magnitude of malpractice awards is increasing rapidly. Excessive litigation is the key force behind soaring insurance premium rates. Rising insurance rates are driving doctors out of practice and reducing access to health care. Doctor walkouts are visible evidence of this. Fear of lawsuits encourages doctors to practice "defensive medicine" which increases health care costs. Capping jury awards is effective in lowering insurance premium rates as is most clearly verified by the California experience. Limiting the size of contingency fees trial lawyers may receive from jury awards reduces frivolous lawsuits. Medical malpractice suits hurt women most. 

The LIBERAL VIEW: SEE:Notes and Sources: 
Liberals note that the data of Jury Verdict Research, which shows awards rising 100 percent between 1997 and 2000 is misleading because the company collects only jury verdict information.[38] According to the PIAA 96 percent of malpractice awards are a result of out-of-court settlements. [39] A better database is contained at the National Practitioner Data Bank (NPDB), a government service that tracks malpractice claims, verdicts and settlements. The NPDB says the median payment for medical malpractice rose from $100,000 in 1997 to $135,000 in 2001.[40] The recent surge in malpractice insurance rates is a direct result of the equally dramatic drop in investment income during the late 1990s. 
In the 1990s insurance companies under priced their policies to attract capital because investment returns were favorable. Several large insurance companies left the market, reducing competition and contributing to the run-up in rates. Their departure was not a result increased claims but rather financial and sometimes illegal mismanagement. Medical mistakes not medical lawsuits are the key problem. A small number of doctors makes the majority of the mistakes. State medical boards are reluctant to discipline incompetent doctors. The overall number of medical malpractice claims is not increasing. Only a small portion of medical mistakes result in a malpractice claim. 
Jury awards are reasonable and proportional to the severity of the injury. No significant evidence exists that doctors are driving up health costs by practicing defensive medicine. State jury award caps have not resulted in lower premium increases. Insurance company regulation not caps is a key solution as the California experience demonstrated. Reducing the workweek for medical residents and increasing nursing staff levels are key steps to reducing medical mistakes 
 <URL>  
Marian Wright Edelman: "It's time for greatness -- not for greed. It's a time for idealism -- not ideology. It is a time not just for compassionate words, but compassionate action" 
 <URL>  
 <URL>  
Let us prey. Raymond/Liberal 


