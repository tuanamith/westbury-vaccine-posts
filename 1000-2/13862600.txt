


When the Plaintiff is now asked if Lyme was a Plum Island accidental release or a deliberate release of a bioweapon, the Plaintiff has to say, "They made a 'vaccine' that shuts off the immune system, OspA.  They claimed it was 'immunogenic.'  That is, they claimed it made antibodies against OspA from the Lyme spirochete.  Based on this same vaccine, which is now off the market due to the unreported adverse events related to the immune suppression it caused (as the Plaintiff explained to the FDA in 2001), Corixa and SmithKline are now making allergy vaccines.  That is, LymeRIX shut off the immune system so well, that this very type of immunogen may be helpful to people with asthma, mold allergies, and the like.  So, how could that have been a vaccine, when vaccines are based on the principle of MAKING antibodies?  This should answer the question as to how competent the CDC s to either making bioweapons or detecting them.  There is one set of people for whom this vaccine is dangerous because these people who are genetically susceptible to a hypersensitivity reaction to it, and there is another set who are genetically susceptible to immune suppression to this Pam3Cys mycobacterial lipopeptide.  HAD Yale looked into the latter matter, as would have been appropriate and dutiful not only to the FDA, but to the NIH and the public, we would not be here today."  <URL>  



courant.com  <URL>  MEDICAL FRONTIERS 
Cats' Asthma Linked To Home Irritants 


By WILLIAM HATHAWAY Courant Staff Writer 
October 25 2005 
Scottish researchers believe they have discovered a cause of asthma in cats - their owners. 
Human irritants such as cigarette smoke, household dust and dandruff along with pollen and some types of cat litters can create inflammation in the airways of cats, according to the University of Edinburgh veterinarians. 
Feline asthma afflicts about 1 in 200 cats, and vets often see an improvement in animals' condition when they are removed from the home for treatment, said Nick Reed, a clinical scholar at the Royal (Dick) School of Veterinary Studies. 
The common denominator may be the presence of the bacterium Mycoplasma, which has been implicated as a trigger of human asthmas. University of Edinburgh researchers plan further research on the incidence of the bacterial infection in cats with asthma, they announced last week. 
Circumcising HIV 
Circumcision reduced rates of HIV infection in heterosexual men in South Africa by 60 percent, according to a new study released Monday. 
Doctors had noted that groups that practice adult circumcision had lower rates of HIV transmission, but the large preventive effects of circumcision reported Monday in the online journal Public Library of Science surprised scientists. 
French and South African researchers tracked more than 3,200 men who lived in an area outside of Johannesburg, where circumcision in adulthood is common but not universal. The group tracked both circumcised and uncircumcised men and found that after 18 months, 49 uncircumcised men had contracted HIV, compared with 20 circumcised men. 
The protective effect was so pronounced that the researchers canceled the study and offered to circumcise the rest of the study's participants. 
Researchers said more research needs to be done to confirm the value of adult circumcision and noted that similar studies were underway in Uganda and Kenya. 
A Sweet Heart Study 
Just in time for Halloween, the Hershey Co. has announced that two servings of dark chocolate seem to improve blood flow and lower blood pressure. 
The study of 45 people, conducted by Dr. David Katz, director of the Prevention Research Center at Yale University, was funded by Hershey and has not been reviewed by other scientists or published in a peer-reviewed journal. The findings tend to reinforce earlier research that suggests dark chocolate may decrease some of the risk factors for heart disease such as cholesterol and high blood pressure. 
Dark chocolate is rich in anti-oxidants called flavonoids. 
Children, however, might not want to tell their parents that antioxidants can be found in many fruits and vegetables, which also have the added benefit of containing fewer calories. And no study has found a health benefit for sugary milk and white chocolates - traditional Halloween fare. 
Breast-Cancer Bust 
Using cholesterol-lowering drugs does not offer women protection against breast cancer, according to a study published Monday in the Archives of Internal Medicine. 
Some laboratory studies had suggested that statins might inhibit development of tumors, but researchers at Brigham and Women's Hospital in Boston found the use of the drugs was not associated with breast-cancer risk in an analysis of health data from nearly 80,000 women who participated in the Nurse's Health Study. 
"We also found no associations of general lipid-lowering drugs and serum cholesterol levels with breast cancer risk," the authors said. 
Copyright 2005, Hartford Courant 


