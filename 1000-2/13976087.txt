


 Yes to Gossamer 
 Embraced  by Siberian Skys   <EMAILADDRESS>     Part 1 Please see part 0 (template) for story information. 
Title: Embraced Author: Siberian Skys Rating: NC17 Classification: M/K slash Spoilers: The entire series is fair game.  Archive: Yes, as long as the header remains attached and you let me know where.  Summary: Alex tries to convince Mulder that death is not an option.  Disclaimers: No copyright infringement intended.  Notes: This is the fifth story in the Stalled in Fargo series. The first four stories are: Stalled in Fargo, Snow Day, Ditched and Breakdown. My thanks to Xscribe for her beta and continual support.  Feedback: Is appreciated  by  <EMAILADDRESS>  
*** 
Alex rested his cheek against Mulder's, his hand restlessly stroking his lover's chest.  
"I'm sorry, Alex, you don't...don't have to. I can...I can do it," Mulder said,  his breath coming in little gasps as he spoke.  
"Why are you so hell bent on dying and leaving me alone?" Alex exploded shoving  Mulder away. The force and lack of limb on his left side caused him to lose his  balance and he ended up sprawled on the bed. Embarrassed, he struggled to a sitting position and cradled his stump protectively in his hand. He refused to look up, even when he felt Mulder's fingers tangle in his hair.  
"I don't want to die. Is that what you think? That I...that I'm suicidal. Jesus, Alex, I--I'm finally--I don't know what else to do. You can't trust me to watch your back and you can't leave me alone, obviously. You know I'm useless to you now. I'm so tired. It's just too hard to pretend everything's okay anymore. I've been in a mental hospital...hospitals. I won't get locked up  like that again." 
Alex looked up into Mulder's face. Slowly standing, he wrapped his fingers around Mulder's neck and drew him in for a deep kiss. "I have your word; you won't do anything...drastic." 
"I promise," Mulder whispered.  
Alex slid his hand around Mulder's waist and steered him toward the door. "Come  back to bed. We'll deal with this in the morning." 
"You're not worried about Tunisia?" Mulder asked.  
"I'm worried about you." 
"I'm a distraction. A dangerous one," Mulder said.  
"I'm only going to say this once. You're my first priority. We'll take care of Tunisia when we take care of Tunisia. It sure as hell isn't going anywhere. Let 'em sweat it out a little. They'll get complacent." 
"I'm one person--" 
"You're the one person I love. Deal with it, Mulder; I don't care if the planet  goes to hell in a handbasket." 
Mulder blinked.  
"What?" Alex asked.  
"I...You love me?" 
"Give it a rest," Alex grumbled.  
"No." 
"God, don't turn this into some girly Jerry Maguire moment," Alex groaned. 
"You had me--" Mulder laughed.  
"Don't go there," Alex warned.  
Mulder only laughed harder.  
"What now?" 
"Big bad Alex Krycek has seen Jerry Maguire. Scully dragged me, what's your excuse?" Mulder asked trying to catch is breath.  
"It's not too late, I can still get my gun," Alex said.  
Mulder's mirth vanished. "When did you decide that fighting them was worth the effort?" 
"It doesn't matter." 
"I think it does. This is all for me isn't it?" Mulder asked.  
"Somewhere along the way I became your proselyte," Alex said.  
"So you decided to bring down the collaborators single handedly," Mulder said, grimacing at his choice of words.  
"I decided if I did something noble, maybe you wouldn't hate me so much," Alex confessed.  
Mulder chewed on his lower lip as he studied he studied his lover. "When?" 
The little crinkle above Alex's nose appeared, as it always did when he was thinking or confused.  
"When did you decide to fight instead of collaborate?" Mulder asked.  
"At first it was just about exacting revenge on Spender, but after the silo--" 
Mulder wrapped his arms around Alex. 
He accepted the attempt at comfort without comment.  
"After the silo," Mulder prompted.  
"Let's just say that Sir Antony made me an offer I couldn't refuse." 
"The English gentleman?" Mulder asked.  
Alex nodded.  
"It's cold. Let's take this conversation to bed," Mulder said. 
*** 
Alex lay pillowed on Mulder's chest, his lover combing through his hair.  
"Tell me about the offer," Mulder coaxed.  
"Sir Antony said I could die in the silo or I could come to work for him." 
"What was the job?" Mulder asked.  
"The Consortium already had a source for a piece of the rock, but the source refused to provide him a sample of the vaccine. He left the particulars up to me." 
"So you decided to take me along for the ride." 
"I stumbled across the militia and it gave me a --" 
"Singular opportunity," Mulder finished.  
"I thought if I could get you to trust me again, things might be different between us."  
"So you set them up." Mulder said.  
"Not really, I just helped their plans along and sent you the evidence...kill two birds with one stone." 
Alex could feel Mulder's mental wheels turning.  
"I still don't understand why you wanted me in Russia with you." Mulder said.  
"It started out as a chance to be close to you again, but then I got an idea. If I could get you into the tests, then you'd be immune. I'm sorry, Mulder, I know it was terrible, but all I could think was that I had the chance to save you." Alex said, lifting his head so he could see Mulder's eyes.  
"You really weren't going to leave me there." 
"Of course not," Alex said. 
"God, Alex," Mulder said rubbing the remains of his lover's arm. "This didn't have to happen. Why didn't you tell me? Why did you run off?" 
"I couldn't blow my cover. I thought you'd kill me before I had the chance to explain. You wouldn't have believed me anyway. I don't know. It was damn stupid," Alex said.  
"I'm sorry. I didn't mean for this to happen," Mulder said, kissing Alex's shoulder.  
"You didn't do this to me. You can't take responsibility for everything that goes wrong in the world. This was my cluster fuck not yours." 
"I feel responsible." 
"You feel responsible if it rains on game day," Alex teased, offering his mouth  for a kiss.  
Mulder returned the kiss hungrily and let his hand trail down Alex's abdomen to  cup his cock and balls. "Wanna fuck?" he asked. 
"Do you ever not want sex?" Alex asked.  
"We don't have to..." 
"Did I say no?" Alex asked, reaching across Mulder to open the nightstand to pull out condoms, lube and a hand towel. Dropping the supplies next to Mulder, he rolled over onto his stomach, scrunching his pillow beneath his upper body and resting his head on his arm. 
"Do you ever want to--would you like to fuck me? Mulder asked. 
"You've never shown much interest," Alex said. 
"Not from lack of...It's not that I don't..." Mulder said, dropping his eyes.  
Alex remained silent for several minutes, searching for the words that wouldn't  make his lover any more uncomfortable than he already was. "I always thought the issue was that you didn't like giving up control--especially to me."  
"I trust you," Mulder said, his nervousness evident in the constant tap of his fingers against his thighs.  
Alex let go of his pillow and sat up as the memory foam bounced back into shape. "Who hurt you?" he asked, trying to not let anger creep into his voice.  
"What makes you think--" 
Alex cocked his head and stared at his lover.  
"Don't make me," Mulder whispered. 
"I'll hunt him down and bring you his head," Alex snarled. 
Mulder let out a harsh laugh that deteriorated into something slightly insane. "What makes you think it was a he?" 
Alex ran down the list of possibilities in his head and didn't get past the first name. "Phoebe..." 
"...liked any game that was painful and humiliating," Mulder finished. 
"And you never quite got past it," Alex said." And yet you still offered." 
"You aren't her, Alex. I'm not scared of what you might do. There're just some things that have lost their eroticism." 
Alex smiled sadly, as he tugged Mulder against his chest. "You could have told me, you know."  
"You didn't sign on to be my nursemaid," Mulder said.  
"I signed on for whatever comes," Alex replied, tilting Mulder's face up and kissing him deeply on the mouth.  
*** 
Mulder finished putting the last of his things in the back of the Range Rover. "Are you going to tell where we're going?" 
"It's not some big secret; I thought we could just drive." 
Mulder held out his hand for the keys and looked hopeful.  
Alex smiled and dropped them into his hand. "If you're driving then I should qualify that response. We should drive anywhere that doesn't include UFO hotspots, sea monsters, haunted houses--let's just be safe. We're going wherever the paranormal isn't." 
"Kill joy," Mulder said, patting Alex's ass.  
*** 
The End 
***      


### The End ### 



