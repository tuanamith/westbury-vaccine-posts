


In article < <EMAILADDRESS> >,  "Igor The Terrible" < <EMAILADDRESS> > wrote: 

An increase of 0.009% over 3 centuries is hardly affecting the cycle at  all. 
Double that by 2100, and you get 0.02%. 
A room doesn't get stuffy with an increase of 0.02% CO2. You wouldn't  even feel like opening a window. 
Methane is measured in parts per billion. CO2 is measured in parts per  million. 
H2O is the primary greenhouse agent on earth. 
But, yes, large increases (in the 0.02% range) in tropospheric  concentrations of CO2 and CH4 may RESULT from global warming. 
One of the many dangers we face every day; every century; every  millennium. 
We were supposed to be out of topsoil in the midwest by 2005, if you'll  recall. 15 years ago when we were warned the topsoil was being harshly  depleted, you could easily walk between rows of corn in a cornfield.  Now, farmers plant corn so densely that you'd need a machete to walk  between the rows. Back in the 1800's they had to plant it so far apart  that the leaves of fully mature plants wouldn't even touch. 
Look at the curve and tell me we're not surrounded by Chicken Littles. 
Have you ever studied tropical storm cycles? In 1982, when Global  Warming was well advanced, and the Ozone Hole was expanding  ("scientists" were claiming that it was permanent and deteriorating, if  you'll recall) there were only 2 named storms. 
Did you know there were only 2 cyclones in 1930? Did you know there were  whopping 21 in 1933? 
Reliable records only go back to 1898 or so, but the 2005 cycle, while a  spike, is not so abnormal as to be remarkable or indicative of anything  in particular. 
Well, even if true, there's no way to stop it. Canned foods,  refrigerated foods, medicine, vaccines, you name it--it's contributing  to CO2, amen. 
Hybrid cars ain't even a band-aid. 
Smokey the Bear could do a lot towards reducing a large and unnecessary  contributor to the levels of CO2 in the troposphere. Perhaps as much as  1/4 could be eliminated by preventing the nearly continuous worldwide  grassland and forest fires. Especially in Asia. And Africa. And South  America. And the American Southwest. 
However, the Kyoto Protocol doesn't even mention forest fires. 
Why? Because this wouldn't take wealth from the industrial West and give  it to Pogo and Chad. No, it would actually doubly benefit mankind while  ACTUALLY reducing aerosol emissions--which is NOT what Kyoto is all  about. 
Kyoto isn't even about reducing emissions in the first place. 
Which should tell you how seriously those who commissioned IPCC take  those flawed findings. 
Heh. 
--  NeoLibertarian 
Global Warming: It ain't the heat, it's the stupidity. 

