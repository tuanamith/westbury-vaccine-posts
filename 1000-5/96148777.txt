


Calls for urgent action as TB soars 
08:24am 3rd August 2004 


What is RSS? 
An expert is calling on the Government to stop the relentless climb of TB. 
London is now a TB "hotspot" with nearly half of all UK cases occurring in  the capital - around 3,000 each year - double the total of 15 years ago. 
TB cases have jumped by nearly 20% in England and Wales over the last two  years, but London in particular is under threat from the bacterial disease,  which is more commonly found in developing countries. 


Look here too... 
Skip gossip links to more articles 
Guide: Tuberculosis: the facts 
News: Deadly TB superstrains arrive in Britain 
Story: Britain's tuberculosis shame 
Article: 30 schoolchildren exposed to tuberculosis 
Immigration, overcrowding and the spread of HIV infection have all  contributed to the resurgence of TB in the UK. 
Multi-drug resistance 
Dr Vas Novelli, head consultant on infectious diseases at Great Ormond  Street Hospital, London, which is running a new monthly TB clinic in  response to the steep rise in cases, called on the Government to pump more  cash into improving TB services. 
Two of the main problems, he said, are that certain strains of TB emerging  are becoming resistant to many drugs, and secondly, that the number of TB  specialists falls far short of current recommendations. 
Dr Novelli said: "Whilst screening for TB in high-risk groups has always  been a priority, TB is not always picked up immediately, and we are now  facing multi-drug resistant strains of TB in the capital. 
"The current numbers of TB clinical nurse specialists also fall dramatically  short of British Thoracic Society (BTS) Joint Tuberculosis Committee  recommendations that there should be one nurse for every 40 TB patients. 
"Unless the numbers of TB nurses are increased we will continue to see an  inexorable rise in TB cases in London." 
Areas worst-hit by TB such as Newham and Tower Hamlets should be a top  priority for recruiting more specialist nurses in order to better screen and  keep track of TB patients and ensure they are taking their medication. 
Insufficient staff 
A national roll-out of DOT-S programmes (Directly Observed Therapy - Short  Course) would also be key. 
An audit carried out by the BTS of high incidence TB areas in 43 districts  across England and Wales last year found that nearly 90% of them had  insufficient staff to cope with such high levels of the disease as were  being experienced. 
But calls for the Government to take action on TB have been met by a wall of  silence from the Department of Health, according to the BTS, the British  Lung Foundation and TB Alert. 
The groups are warning that lives are being put at risk by the government's  failure to publish the long-promised TB Action Plan, which was expected to  outline better prevention, diagnosis and treatment of TB. 
Professor Peter Ormerod, member of the BTS Joint TB Committee said: "It is  amazing that on one hand the Government makes grandiose statements about  building a modern, hi-tech NHS, but when it comes to deciding how we are  going to manage a Victorian disease like TB we appear to be still living in  the dark ages. 
"It seems to me that when it comes to TB, John Reid is all talk and no  action." 
The BTS reported a rise in TB cases in England and Wales from 5,798 in 1992  to 6,891 in 2002, in figures released in March of this year. No other EU  country has suffered an increase in TB over the past 10 years. 
Figures from the Health Protection Agency showed London accounted for 43% of  all cases reported - followed by the West Midlands (15.6%), East Midlands  (12.2%), and Yorkshire and Humberside (10.6%). 
Action plan 
The 15-44 year-old age group accounted for 56% of all cases while the  highest rate of 28.8 cases per 100,000 was found in people aged 25-29. 
TB is a contagious airborne bacterial disease of the respiratory system  which causes severe lung symptoms but can often be present without causing  any symptoms until the patient becomes run-down or the immune system is  damaged. 
The disease can usually be treated by a six-month course of drugs but in  many developing nations it can prove to be fatal due to a lack of medical  care and a combination of other diseases. 
Sir John Crofton, President of TB Alert, said: "Despite the seriousness of  the situation, there is an apparent lack of urgency on the Government's part  to take action on TB. 
"It is now two-and-a-half years since the Chief Medical Officer for England  identified TB as an area where intensified action is needed, but there has  been no movement since then." 
A Department of Health spokeswoman said: "We are aware that there are a  small number of areas of the country where numbers of TB cases are rising. 
"Primary Care Trusts are responsible for delivering TB services appropriate  for their population and a huge amount of action is already under way across  the country. 
"The Chief Medical Officer identified the need for more intense action and  that is why we have been preparing a TB Action Plan for England which will  be published in the Autumn. 
"In the meantime there is no reason why the ongoing diagnosis, treatment,  care and control of TB should be in any way compromised." 
Tuberculosis (TB), a potentially fatal disease, had been almost wiped out in  Britain by the 1970s. 
But there has been a dramatic rise in cases recently, especially in and  around London, where 40 per cent of all TB cases are detected. 
In a parliamentary briefing last week, doctors warned that London has a  higher rate of TB than some Third World countries, including China and  Brazil, with cases in the capital rising from 1,500 in 1987 to 3,000 last  year. 




Top of Form 1 
Bottom of Form 1 
in the debate » Over, 7,300 cases were identified across Britain last year,  which is an 80 per cent rise over the past decade. 
There is fear that the disease may spread further, due to a combination of  foreign immigrants infected with TB coming to the UK, increased travel  abroad, and hospital staff who are not trained to spot the disease from  early symptoms. 
Tuberculosis is a communicable disease caused by infection with the tubercle  bacillus (also known as Mycobacterium tuberculosis or M. tuberculosis), most  frequently affecting the lungs. 
These tiny bacteria are usually passed on through mucus in the same way as  colds are passed on, such as when people sneeze, talk or cough. 
The bacillus can remain active in the air for several hours in confined warm  spaces such as planes and the London Underground 
A cure for TB was developed more than 50 years ago, but it is still one of  the top three killer diseases in the world, along with malaria and HIV. An  untreated person with TB can spread the disease to ten to 15 people each  year. 
WHO GETS IT? 
Anyone can catch TB, although people who are at most risk are those in close  contact with an infectious case, those who have lived in places where TB is  still common, such as Third World countries with a high rate of poverty,  those whose immune system is weakened by HIV or other medical conditions,  young children and the elderly. 
The TB bacilli have a thick, waxy coat, are slow growing and can survive in  the body for many years in a dormant or inactive state. This means people  can be infected but show no signs of the disease. 
It is when the bacilli are dividing that people are said to have 'active  TB'. 
The disease can affect any part of the body, but is most common in the lungs  and lymph glands. However, it is only TB in the lungs and respiratory system  that is infectious. 
Studies have demonstrated that only about 30 per cent of healthy people  exposed to the disease will be infected, and, of those, only 5 per cent to  10 per cent will develop TB. 
What happens to the TB bacilli once in the body is largely determined by the  person's individual immune response. The immune systems of 70 per cent of  healthy people will completely eradicate the bacilli; the remainder will  become infected and have a positive reaction to a skin test. 
For the 5 per cent to 10 per cent who go on to develop TB, the risk is  greatest within the first five years following infection. 
A small number of people who become infected develop what is called 'primary  disease', usually within eight weeks of exposure. This can pass unnoticed  and usually resolves without treatment, leaving a small scar on the lung and  surrounding lymph nodes that can be seen by a chest X-ray. 
Children are more likely to develop primary disease than adults. If the  immune system cannot kill or contain the bacilli, they multiply, resulting  in damage to the surrounding tissue of the lung. 
WHAT ARE THE SYMPTOMS? 
The most common symptoms include: . Cough lasting for more than two weeks, and sometimes with blood-streaked  sputum. . shortness of breath. . loss of appetite and weight loss. . fever and sweating. . extreme fatigue. 
HOW IS THE INFECTION RISK ASSESSED? 
When someone is diagnosed with TB, a team of specialist health professionals  will first make an assessment of the infection risk posed to others. 
If TB bacilli are found in the sputum of the sufferer, then their contacts  will be investigated to identify others who may have been infected. 
Contacts are defined as 'close' - meaning household and immediate family -  and 'casual' - meaning friends, work colleagues, schoolmates, etc. Casual  contacts are investigated only if the TB sufferer is assessed to be a  serious infection risk. 
If you are identified as a contact at risk from TB, then you will be  routinely invited for screening. Screening will consist of a skin test to  determine if your immune system recognises TB. 
The skin test will be done in one of two ways. The most common method is the  Heaf test, which will take a week to come back. 
The other method is the Mantoux test, which can be interpreted after three  days. Both types of test involve a small injection into the skin of the  forearm. 
You may also be asked to have a chest X-ray, especially if the skin test is  strongly positive. 
In the UK, the majority of people have had the BCG vaccine and so their skin  tests will often be mildly positive. This does not mean that they have TB:  it just means that their immune systems recognise the disease. 
Because TB can develop some time after exposure, contacts are advised to  look out for symptoms and may be followed for up to one year, with further  appointments for screening. 
WHAT IS THE TREATMENT? 
People who have a strongly positive skin test and/or evidence of TB  infection found on a chest X-ray, or who are unwell, will be investigated  further by a specialist doctor and may be given a course of anti-TB  medication. 
TB is treated with antibiotics that must be taken for at least six months.  Modern anti-TB drugs are extremely effective and, in nearly all cases,  sufferers feel much better after the first two weeks of medication. 
It is important to note that within two weeks of taking the antibiotics, TB  sufferers are no longer infectious and can return to life as normal. 
Anti-TB drugs are always prescribed in combination to reduce the risk of the  TB bacilli becoming resistant to one or more strains of them. Patients will  usually be started on three or four drugs. 
It is vital, as with all antibiotics, that the medication is taken as  prescribed. Taking anti-TB medication for too short a time can lead to the  development of drugresistance TB, which is much harder to treat and  significantly increases the sufferers' risk of longterm complications, or  even death. 
HOW IS IT PREVENTED? 
The BCG vaccine increases a person's immunity to TB and protects against the  most severe forms of disease, such as TB meningitis. However, people who  have had the BCG vaccine can still develop TB. 
BCG immunisation programmes vary across the UK according to the local risk  of TB infection. It is particularly recommended for newborn babies in  families where there has been a case of TB, or whose members may be at  increased risk of TB infection, but it is not compulsory. 
A new, more effective and longer lasting BCG vaccine, called the BCG SSI  vaccine, was passed by the Medicines Control Agency in September and will be  administered to schoolchildren throughout Britain this month. 
Although there is no preventive treatment for TB, scientists are in the  early stages of developing a new vaccine that would protect adults  completely from the disease. 
A team of U.S. scientists at the Tuberculosis Research Unit has identified  the particular molecules in the body's immune system - class II major  histocompatibility complex (MHC-II) molecules - that the TB bacteria blocks  from working, so disarming the body's natural immune response. 
This has allowed scientists to begin development on an effective vaccine by  making these molecules resistant to TB. 
For more information, contact TB Alert, 020 8969 4830, or visit  www.tbalert.org.uk 
CASE HISTORY 
Lillian Snowdon, 55, is a retired secretary from Acton, West London. She 


Lillian Snowdon 
is divorced and has three children, aged 28, 27 and 23. 
Lillian says: Last year, I began losing weight for no reason. I was vomiting  and lost about a stone-and-a-half in six months. I thought I had cancer. 
I knew about the symptoms of TB because my son had suffered from it in 1999,  but I still didn't realise that I had contracted the disease. 
The doctors didn't think that the two cases were connected and said it was  likely I'd caught it on the Tube. However, my theory is that I had it lying  dormant in me for years, and that my son caught it from me. 
I was put into an isolation unit as I was extremely infectious. I had to  stay there for eight weeks while I was treated with antibiotics. They should  have worked within two weeks but I didn't respond to them because I was  still being sick. 
I was on antibiotics for a total of six months. Everyone who had been in  close contact with me had to be screened for TB, and my daughter and other  son were put on a precautionary course of antibiotics. 
Now I feel back to normal and have no long-lasting symptoms from the  infection, though I still have to go for chest X-rays every six months.  Luckily, I don't have any scar tissue on my lungs. 
I was shocked at how easy it is to catch TB. Now I'm very cautious about my  health as the disease can lie dormant and re-emerge. Doctors should make  people more aware of the disease and its symptoms. It is treatable - more  so, if caught early. 
Have your Daily Mail and Mail on Sunday delivered to your door. To find out  more click here. 



