


-----BEGIN PGP SIGNED MESSAGE----- Hash: SHA1 
Cuban Radar Newsbriefs - May 11, 2006 
Via NY Transfer News Collective  *  All the News that Doesn't Fit   Progreso Weekly - May 11, 2006  <URL>  
Cuban Radar Newsbriefs - May 11, 2006 A Service of the Radio Progreso Alternativa Havana Bureau 

* Social workers finish task and oil production grows 

* Military weaponry upgraded 
Two plants in the eastern province of Holguín hosted the visit and were praised by Gen. Castro. 

* Cuba announces trials of vaccine against prostate cancer 
Havana -- The National Information Agency announced that Cuban scientists are working on a prostate cancer vaccine. 
Eulogio Pimentel, vice director of CIGB said that the project is "one of our most important." 
Prostate cancer is one of the main causes of death among Cuban males. 

* Villa Clara crime statistics: More than 1,000 offenses  
The newspaper also said that there are companies that tally as expenses, shortages both in cash figures and in products. 

* New oil well in Varadero 
-----BEGIN PGP SIGNATURE----- Version: GnuPG v1.4.2 (GNU/Linux) 
iD8DBQFEZlbkHwEfpL2U00kRApF1AJkBkb97AYJsJxhD9uZtacrG2qp/jQCgsaNB MuQp1SU8abezW+LLZx+38lA= =mxNj -----END PGP SIGNATURE----- 


