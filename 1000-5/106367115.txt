


ALERT OVER NEW 5-IN-1 BABY JABS 
SUNDAY EXPRESS  MAY 14 2006 
FRONT PAGE STORY 
EXCLUSIVE By Lucy Johnston 
HEALTH EDITOR 
Brain damage fears after trial results show that two-thirds of patients  experience bad reaction 
Babies given the new five-in-one jab face a risk of convulsions, brain  damage or even death. 
Results of medical trials by the firm which makes the vaccine have revealed  that the "superdose" vaccination, which was introduced 18 months ago, can  have serious side-effects. 
The five-in-one is designed to protect children against diphtheria, tetanus,  whooping cough, Hib influenza and polio in a single shot. 
Evidence from the vaccine's manufacturers, Sanofi Pasteur, shows that in  clinical trials 64 per cent of 451 babies given the Pediacel jab experienced  bad reactions. Ten per cent of these were "moderate to severe". 
These included convulsions, loss of consciousness and high-pitched or  persistent inconsolable crying. 
Other studies showed that components of the vaccine can cause breathing  difficulties, blue discolouration of the skin due to lack of oxygen,  swelling of the brain, low blood pressure and extreme allergic shock. 
The document prepared for the Medicines Healthcare Regulatory Authority and  publicised by What Doctors Don't Tell You, a monthly newsletter that  questions conventional medicine, has angered doctors and MPs. Professor  Steve Webb, Liberal Democrat health spokesman, criticised the Government  saying: "I am very concerned that these apparently wide spread side-effects  have not been disclosed. The Government should investigate these concerns  urgently". 
Dr Richard Halvorsen, a London GP who specialises in innoculations said:  "I'm concerned that this five-in-one is overloading a child's immune system. 
"The risks of vaccinations have been played down by the Government and  parents have been fobbed off with bland reassurances". 
And Dr Peter Mansfield, who won a case against the doctor's governing body,  the General Medical Council, for refusing to give the combined controversial  measles, mumps and rubella to infants, said: "This is scandalous. We're  vaccinating babies when their immune systems are not ready". 
Jackie Fletcher from the vaccine damage support group JABS, is also  concerned. She said: "The Department of Health is failing to acknowledge the  dangers of the vaccines and the damage they can cause. It claims the  five-in-one is perfectly safe, but clearly it is not. 
"The only person taking a risk here is the baby and parents are not being  given correct information about it". 
Health officials claim it is safer than the four-in-one-jab it replaced  which contained the controversial preservative mercury, which has been  linked with autism. The new jab is also deemed to be safer because it  contains inactivated polio to remove the risk that children could contract  polio paralysis from the old oral vaccine that contained a live sample of  the virus. 
However, the revelations will ignite debate about the over-use of  inoculations and could hinder Government plans to bring in an even more  intensive vaccination programme that will see children receiving 25 vaccines  in 12 injections by the time they are two. 
The new proposed schedule contains the controversial MMR jab, the  five-in-one jab, a new pneumococcal vaccine and the meningitis C vaccine. 
When the five-in-one was launched the Government vaccine chief urged  parents: " Please do not delay having your child vaccinated. Our vaccines  are extremely safe." 
Nicholas Kitchin, medical director of Sanofi Pasteur, insisted: "There are  fewer side-effects with the five-in-one than were reported with the older  four-in-one vaccine. 
"All drugs and vaccines have potential side-effects, which parents should be  aware of and these are listed in the information leaflet included with every  dose of the vaccine. Severe reactions are rare and the benefits of  vaccination far outweigh the risks." 
A spokeswoman for the Department of Health added: "The Joint Committee on  Vaccination and Immunisation thoroughly assesses the evidence on the safety  and efficacy of all new vaccines before they are added to the childhood  immunisation programme. Pediacel has an excellent record. 
"Every parent is informed about the potential side effects of every vaccine  in a guide which is given to parents by health visitors." 

SUNDAY EXPRESS MAY 14 2006 
OPINION 
Health chiefs must act now on five-in-one vaccine fears 
The Government must move fast to prove that its new super-drug is completely  safe for British babies. The five-in-one vaccination, introduced 18 months  ago, was claimed to be a major breakthrough in the fight against childhood  killer diseases. The drug, which is intended to provide protection against  polio, diphtheria, whooping cough, tetanus and meningitis, is given to  babies as young as two months and was claimed to be risk-free. 
But evidence collected by the drug's manufacturer tells a different story. 
The Sunday Express has revealed that the maker's clinical trials showed that  nearly two out of three infants given an injection of its super jab  experienced significant side effects. 
The trials, when linked with other data, revealed a danger that children  could suffer convulsions and even run the risk of brain damage. 
This is devastating news for parents. Some health campaigners warn that the  combination of five powerful vaccines adminstered in this way overwhelms a  baby's immune system. 
It is vital that the Department of Health intervenes now to restore public  confidence. Every parent knows that no threat to a baby's health is ever  tolerable. 



