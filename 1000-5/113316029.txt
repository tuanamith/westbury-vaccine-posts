


AIDS' relentless march leaves legacy of misery After 25 years, grim prognosis for hard-hit nations, skyrocketing deaths The Associated Press 
Updated: 6:03 a.m. MT June 5, 2006 
JOHANNESBURG, South Africa - It began innocuously, when a statistical anomaly pointed to a mysterious syndrome that attacked the immune systems of gay men in California. No one imagined 25 years ago that AIDS would become the deadliest epidemic in history. 
Since June 5, 1981, HIV, the virus that causes AIDS, has killed more than 25 million people, infected 40 million others and left a legacy of unspeakable loss, hardship, fear and despair. 
Its spread was hastened by ignorance, prejudice, denial and the freedoms of the sexual revolution. Along the way from oddity to pandemic, AIDS changed the way people live and love. 
Slowed but unchecked, the epidemics relentless march has established footholds in the worlds most populous countries. Advances in medicine and prevention that have made the disease manageable in the developed world havent reach the rest. 
In the worst case, sub-Saharan Africa, the epidemic has been devastating. And the next 25 years of AIDS promise to be deadlier than the first. 
AIDS could kill 31 million people in India and 18 million in China by 2025, according to projections by U.N. population researchers. By then in Africa, where AIDS likely began and where the virus has wrought the most devastation, researchers said the toll could reach 100 million. 
It is the worst and deadliest epidemic that humankind has ever experienced, said Mark Stirling, the director of East and Southern Africa for UNAIDS. 
More effective medicines, better access to treatment and improved prevention in the last few years have started to lower the grim projections. But even if new infections stopped immediately, additional African deaths alone would exceed 40 million, Stirling said. 
We will be grappling with AIDS for the next 10, 20, 30, 50 years, he said. 
Search for a vaccine Efforts to find an effective vaccine have failed dismally, so far. The International AIDS Vaccine Initiative says 30 are being tested in small-scale trials. More money and more efforts are being poured into prevention campaigns but the efforts are uneven. Success varies widely from region to region, country to country. 
Still, science offers some promise. In highly developed countries, cocktails of powerful antiretroviral drugs have largely altered the AIDS prognosis from certain death to a manageable chronic illness. 
There is great hope that current AIDS drugs might prevent high-risk people from becoming infected. One of these, tenofovir, is being tested in several countries. Plans are to test it as well with a second drug, emtricitabine or FTC. 
But nothing can be stated with certainty until clinical trials are complete, said Anthony Fauci, a leading AIDS researcher and infectious diseases chief at the U.S. National Institutes of Health. 
And then there is the risk that treatment will create a resistant strain or, as some critics claim, cause people to lower their guard and have more unprotected sex. 
Only 1 in 5 receive drugs Medicine offers less hope in the developing world where most victims are desperately poor with little or no access to the medical care needed to administer and monitor AIDS drugs. Globally, just 1 in 5 HIV patients get the drugs they need, according to a recent report by UNAIDS, the body leading the worldwide battle against the disease. 
Stirling said that despite the advances, the toll over the next 25 years will go far beyond the 34 million thought to have died from the Black Death in 14th century Europe or the 20 to 40 million who perished in the 1918 Spanish flu epidemic. 
Almost two-thirds of those infected with HIV live in sub-Saharan Africa where poverty, ignorance and negligent political leadership extended the epidemics reach and hindered efforts to contain it. In South Africa, the president once questioned the link between HIV and AIDS and the health minister urged use of garlic and the African potato to fight AIDS, instead of effective treatments. 
AIDS is the leading cause of death in Africa, which has accounted for nearly half of all global AIDS deaths. The epidemic is still growing and its peak could be a decade or more away. 
Many nations going backwards In at least seven countries, the U.N. estimates that AIDS has reduced life expectancy to 40 years or less. In Botswana, which has the worlds highest infection rate, a child born today can expect to live less than 30 years. 
Particularly in southern Africa, we may have to apply a new notion, and that is of underdeveloping nations. These are nations which, because of the AIDS epidemic, are going backwards, Peter Piot, the director of UNAIDS, said in a speech in Washington in March. 
Later, at a meeting in Abuja, Nigeria, last month, Piot cited encouraging news including a sharp fall in new infections in some African countries. There also has been an eightfold increase in the number of Africans benefiting from antiretroviral treatment, he said. 
But, he warned, the crisis of AIDS continues and is getting worse and any slackening of our efforts would jeopardize the hard-won gains of each and every one of us. 
Besides the personal suffering of the infected and their families, the epidemic already has had devastating consequences for African education systems, industry, agriculture and economies in general. The impact is magnified because AIDS weakens and kills many young adults, people in their most productive years. 
So many farmers and farmworkers have died of AIDS that the U.N. has invented the term new variant famine. It means that because of AIDS, the continent will experience persistent famine for generations instead of the usual cycles of hunger tied to variable weather. 
Africas misery hangs like a sword over Asia, Eastern Europe and the Caribbean. 
Researchers dont expect the infection rates to rival those in Africa. But Asias population is so big that even low infection rates could easily translate into tens of millions of deaths. 
Although fewer than 1 percent of its people are infected, India has topped South Africa as the country with the most infections, 5.7 million to 5.5 million, according to UNAIDS. 
Patient Zero: A hunter in 1959? The astonishing numbers have grown from a humble beginning. 
Nobody knows for sure when or where, but the AIDS epidemic is thought to have begun in the primeval forests of West Africa when a virus lurking in the blood of a monkey or a chimpanzee made the leap from one species to another, infecting a hunter. 
Researchers have found HIV in a blood sample collected in 1959 from a man in Kinshasa, Congo. Genetic analysis of his blood suggested the HIV infection stemmed from a single virus in the late 1940s or early 1950s. 
For decades at least, the early human infections went unnoticed on a continent where life routinely is harsh, short and cheap. 
Then, on June 5, 1981, the Centers for Disease Control and Prevention in Atlanta reported that five young, actively homosexual men in Los Angeles had a new, mysterious and as yet unnamed illness that attacked the immune system and caused a type of pneumonia. A month later, it reported an odd surge among homosexual men in the number of cases of Karposi Sarcoma, a rare cancer now linked to AIDS. 
In the early days of the epidemic, just the mention of AIDS elicited snickers and jokes. Few saw it as a major threat. It was the Gay Plague, and for some, divine retribution for a lifestyle Christian fundamentalists and other conservatives consider deviant and sinful. 
When heterosexuals began to contract the disease through blood transfusions and other medical procedures, they were often portrayed as innocent victims of a disease spread by the immoral and licentious behavior of others. 
Slow response rooted in prejudice The initial reactions and prejudices associated with AIDS slowed the early response to the epidemic and limited the funding. Too much time, money and effort was spent on the wrong priorities, Stirling aid. 
Over the last 25 years, the one real weakness was the search for the magic bullet. There is no quick and simple fix, he said. But with the recent successes we are starting to see the end of epidemic. 
There is evidence to suggest we are at the tipping point, said Stirling. 
The pace of change over the last couple of years suggests the number of new infections can be reduced by 50 to 60 percent by 2020  if the momentum continues. 
It is surely possible, it is doable, Stirling said. 
© 2006 The Associated Press 
URL:  <URL>  


