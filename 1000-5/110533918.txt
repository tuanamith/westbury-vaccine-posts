


HIV origin 'found in wild chimps'  
 <URL>  
"The origin of HIV has been found in wild chimpanzees living in southern Cameroon, researchers report.  
A virus called SIVcpz (Simian Immunodeficiency Virus from chimps) was thought to be the source, but had only been found in a few captive animals.  
Now, an international team of scientists has identified a natural reservoir of SIVcpz in animals living in the wild.  
The findings are to be published in Science magazine. 
It is thought that people hunting chimpanzees first contracted the virus - and that cases were first seen in Kinshasa, in the Democratic Republic of Congo - the nearest urban area - in 1930.  
Scientists believe the rareness of cases - and the fact that symptoms of Aids differ significantly between individuals - explains why it was another 50 years before the virus was named.  
This team of researchers, including experts from the universities of Nottingham, Montpellier and Alabama, have been working for a decade to identify the source of HIV.  
While SIVcpz was only identified in captive animals, the possibility remained that yet another species could be the natural reservoir of both HIV and SIVcpz.  
Gene tests  
It had only been possible to detect SIVcpz using blood test - which meant that only captive animals could be studied.  
This study, carried out alongside experts from the Project Prevention du Sida au Cameroun (PRESICA) in Cameroon, involved analysing chimpanzee faeces, collected from the forest floor in remote jungle areas.  
This was useful because University of Alabama researchers had been able to determine the genetic sequences of the chimpanzee viruses - which could then be searched for in the faecal samples.  
Lab tests detected SIVcpz specific antibodies and genetic information linked to the virus in up to 35% of chimpanzees in some groups.  
All of the data was then sent to the University of Nottingham for analysis, which revealed the extremely close genetic relationship between some of the samples and strains of HIV.  
Chimpanzees in south-east Cameroon were found to have the viruses most similar to the form of HIV that has spread throughout the world.  
The researchers say that, as well as solving the mystery about the origin of the virus, the findings open up avenues for future research. 
But SIVcpz has not been found to cause any Aids-like illnesses in chimpanzees, so researchers are investigating why the animals do not suffer any symptoms, when humans - who are so genetically similar - do.  
Close relation  
Paul Sharp, professor of genetics at the University of Nottingham said: "It is likely that the jump between chimps and humans occurred in south-east Cameroon - and that virus then spread across the world.  
"When you consider that HIV probably originated more than 75 years ago, it is most unlikely that there are any viruses out there that will prove to be more closely related to the human virus."  
He said the team were currently working to understand the genetic differences between SIVcpz and HIV evolved as a response to the species jump.  
Keith Alcorn of Aidsmap said: "The researchers have pinned down a very specific location where they believe the precursor of HIV came from.  
"But there are vast areas of west Africa where other forms of SIVcpz lineages exist, and the possibility remains for human infection.  
Yusef Azad, policy director of the National Aids Trust said: "This research is interesting as all discoveries which relate to the history and origins of HIV could be of value to the vital work being carried out by scientists in developing a HIV vaccine."  
Story from BBC NEWS:  <URL>  
Published: 2006/05/25 18:01:51 GMT 
© BBC MMVI" 

-- Michael Gray. Founding Member and Doorman, Earthquack's 666 Club. EAC Rack Monitor, Chamber 5B 

