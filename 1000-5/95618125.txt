


"Ned Ludd" < <EMAILADDRESS> > wrote in   <NEWSURL> : 

I am standing waiting, waiting at your door, one of hunger's children from a billion poor, though you cannot see me, though I am so small - listen to my crying, crying for us all. 
I stand at your table, asking to be fed, holding up my rice bowl, begging for you bread, I stand at your schoolroom, longing just to learn, hoping that you'll teach me ways to live and earn. 
I stand at your clinic begging for vaccine, I stand at your wash place where the water's clean, I stand at office, beg the Heads of State, I am just a child, so I must hope and wait. 
I stand in your churches, listen to your prayers, long to know a God who understands and cares. If there is a God, a God who loves the poor, I'm still standing waiting, waiting at your door. 
-- Shirley Erena Murray 


  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   <EMAILADDRESS>                      <EMAILADDRESS>  
if you and the universe are going in the same direction you will find that the whole world conspires for your  benefit. 

