



"Squark" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... 
Single organisms have been around longer than man has. They have adapted and have learned to evade host immune systems. There is specific and non-specific immunity. What ever the end result is depends on the host and the bug interactions. They don't know the specifics of why or how the immune system produces certain antibodies or cells or cytokines and under what circumstances those might be. If they did we would have an AIDS vaccine by now or a malaria vaccine etc. With influenza we have a virus that changes and thus evades immunity. With parasites you have the worm wraping itself with host tissue and becomes stealth. With TB you have the bug living in a cell, macrophage, actually living and growing in a cell intended to kill bugs. You have bacteria secreting coagluase enzyme that walls off itself from host immune cells. The body has sensors that detect an invader and generate a wide array of cells that go to work. It is broad and as well as specific in the hopes of destroying the bug. 

Yes. It's called passive immunity. The CDC went into the field against Ebola virus with plasma from people who survived the infection. The limiting factor is the amount of antibodies and the quality of protection, both of which are unknown. Immune globulin is injected all the time with hepatitis A exposure and with other virus exposures such as hepatitis B. Synthetic antibodies ie monoclonal antibodies from clonal tissue cells hybridomas are used clinically for testing and used as carriers for drug delivery in cancer. If they had a monoclonal antibody already then that would mean they already had a vaccine as that is what a vaccine is or at least have a neutralizing antibody. 



