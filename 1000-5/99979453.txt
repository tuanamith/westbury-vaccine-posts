


good ol' chickenpox "used to kill hundreds of children a year!"  Just because there happens to be a vaccination for it doesn't necessarily mean it's life-threatening. < 
First of all, chicken pox IS life threatening, just not to kids.  What is a standard issue  illness in children is quite dangerous and sometimes fatal in adults.  I'm not gonna provide a citation for you - Google is your friend. 
Second, is the Center for Disease authoritative enough for you?  Or are they considered "a vaccination site" (whatever that is) because they don't support your beliefs?  And I use the word "beliefs" advisedly. 
here's the url: 
 <URL>  
And here's a quote: 
"In the 20th century, pertussis was one of the most common childhood diseases and a major cause of childhood mortality in the United States.  Before the availability of pertussis vaccine in the 1940s, more than 200,000 cases of pertussis were reported annually.  Since widespread use of the vaccine began, incidence has decreased more than 80% compared with the prevaccine era. 
Pertussis remains a major health problem among children in developing countries with an estimated 285,000 deaths resulting from the disease in 2001." 
As to your personal experience, will you ever learn that your personal experiences are NOT applicable to everyone on earth in every set of circumstances?  Pertussis comes in several strains and apparently you and your children were fortunate in contracting one of the milder ones.  MY personal experience was in direct contrast, as follows. 
1.  Contracted it in early September 
2.  Got diagnosed in late October (did you know that adults with pertussis don't typically have the characteristic "whoop" that kids do?  Neither did my doctor - grrrr.) 
3.  Coughed uncontrollably (not able to stop) for *at least* 45 minutes per night.  Much  vomiting (I toyed with the idea of making a map detailing all the places I threw up - the parking garage at the Museum of Fine Arts, pulled over on the side of Route 2 (there went the nice dinner my Dad cooked!), in middle of the street in front of my friend's house...)   Also, as my baby got larger and my bladder capacity got smaller, and I was shaken with non-stop paroxysms - ugh, you get the picture.  Sorry if TMI. 
4.  Here was an unexpected wrinkle - just before Thanksgiving I completely lost my voice.  I was unable to make *any* sounds for 13 days and my doctor, thinking it was laryngitis, kept putting off sending me to an ENT.  When I finally did go, I was diagnosed with severe dysphonia.  The way it was explained to me, my vocal cords were "stuck" in the position they're in when you cough - wide apart, unable to vibrate against one another, and therefore incapable of producing sounds. 
5.  Had speech therapy and was finally able to vocalize the "mmmm" sound.  After that I practiced and practiced and my voice came back verrrry gradually.  By the time my baby was born in April, there was still a raspy huskiness, but I could talk!  BTW, my lack of a voice for all those months made me unable to perform my regular job.  My boss was very understanding and temporarily assigned me to duties that didn't require me to speak. 
6.  I don't know if I will be believed or not, but I am NOT the kind of person who goes running to the doctor with every little thing, and I am not making this sound worse than it is.  Pertussis is the sickest I've ever been (and yes, I do know how lucky I am to be able to say that.) Including the complications, it was eight months I would not wish on my worst enemy, never mind a small child. 
Oh, one more thing: about allowing children to "ride the disease out" - that's what EVERYONE does because that's the only option - there is no treatment!  The best you can do, if you get diagnosed VERY early (like within 2 weeks of being infected, which practically never happens since symptoms don't even show up until later than that) is to get on an antibiotic in the hope of reducing the chance that you will infect others.  The antibiotic will not help the already infected person, but it may prevent others from contracting the disease.  It is very infectious, but only for the first couple of weeks. 


