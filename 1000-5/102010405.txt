


 <URL>  

Russians Dying Of Aids, Drugs And Despair 
by Graeme Smith 
 April 26, 2006 


Irkutsk, Russia - Five Russian teenagers climbed onto the roof of an apartment block one summer day in Irkutsk. It was unusually warm and sunny in this Siberian city, about 5,000 kilometres east of Moscow, but the boys huddled around a woodstove because they were cooking their first batch of hanka, a homemade type of opium. The dark paste bubbled in dirty metal cups until it was liquid enough to inject. 
A decade later, only one of those young men remains alive. Vlad Pohabov, now 26, remembers that sunny day when he first tried hard drugs as a sweet moment of friendship. But it was also the beginning of a disaster: Nikolai, Andrei and two friends named Alexei have all died of AIDS or overdoses. 
"It killed all of them," Mr. Pohabov said, "and it almost killed me." 
Mr. Pohabov is a survivor in a generation of Russians who are dying in shocking numbers. Russia is among the few countries in the world where life expectancy is falling, and Russians aged 15 to 24 have the highest mortality rate in Europe. 
Demographic experts say it's hard to understand why so many Russians are dying. 
The causes that usually get the blame -- alcohol, drugs, violence, suicide, disease, accidents and a long list of others -- are problems that plague many other countries, but somehow Russia suffers the worst. 
Nothing except the ravages of war has caused any country's life expectancy to drop so dramatically in modern times. Russian and Canadian men could expect to live almost exactly the same number of years in the mid-1960s, as Russian male life expectancy peaked at 65. 
Russian men now live only 58 years on average, while the life expectancy of a Canadian male has jumped to more than 77. Female life expectancy in Russia is 13 years higher, but also declining. Part of the problem seems to be linked with Russian economic troubles after the collapse of the Soviet Union, as life expectancy dropped after the 1991 transition to a market economy and the 1998 financial crisis. But for at least five years since then, the Russian economy has shown healthy growth while the Russian people continue to die out. 
The World Bank estimates that a 15-year-old boy in Russia has a 40-per-cent chance of dying before his 60th birthday, which is double the odds in Turkey and four times the chances in Britain. 
If these trends persist, Russia could lose a third of its population in the next half-century. 
The problem is at least partly psychological, said Boris Tsvetkov, director of the AIDS treatment centre in Irkutsk. His patients know they're risking their lives when they inject drugs or have unprotected sex, he says, but sometimes they just don't care. 
"To persuade a person to have the right behaviour, that person must be raised with a good understanding of how to live correctly," Mr. Tsvetkov said. "It's easier for the young people to break everything, including themselves. We broke our country for the last 10 years and now we're trying to put it back together." 
resembles a factory, Mr. Tsvetkov stands at the forefront of Russia's fight to stop the bleeding. The Irkutsk region has the highest drug-addiction rate in the country, and the highest AIDS infection rate. 
Among the many problems driving down Russian life expectancy, the government appears to have chosen AIDS as one of its most urgent battles. 
The Kremlin plans to spend $126-million (Canadian) on AIDS programs this year -- a 30-fold increase from the previous year. The budget will grow again next year, to $324-million. 
That makes Russia's program fairly small by international standards; Canada's federal AIDS strategy provides at least three times as much funding for each infected person. But observers say the new money shows that Russia takes the problem seriously, as the country's overall infection rate is believed to have climbed past the 1-per-cent threshold that separates low-level outbreaks from broad epidemics. 
In Irkutsk alone, with a population of about 600,000, 18,000 people have tested positive for HIV, and the numbers are growing by roughly 1,000 a year. 
"The federal support will help a lot," Mr. Tsvetkov said. "But we can't control this disease until a vaccine is found." 
On a farm outside the city, Mr. Pohabov considers himself blessed. He escaped the death that surrounded him for the past decade, even as he shared needles, went to prison and spent a year squatting in a dirt-floored cellar with no running water and only a pile of clothes for a bed. 
He eventually checked into a Christian rehabilitation centre, a farm where he learned to raise cattle and love Jesus. Social workers say that about 60 per cent of people enrolled in the program manage to stop using drugs. 
But finding God isn't enough for many Russians. A recent afternoon in the office of Tatiana Burdanova, an infectious-disease specialist at the Irkutsk AIDS centre, showed how a person who appears to be dying of AIDS or alcoholism can, in fact, be dying of despair. 
Ms. Burdanova was trying to persuade one of her HIV-positive patients, a 35-year-old carpenter with work-scarred hands, that his heavy drinking was sabotaging his drug regimen. He admitted a penchant for Russkii Razmer, a vodka whose brand name translates as "Russian-sized," saying he needed the drink to ease the daily pressures of life in a wooden cottage where the water taps work only when the pipes thaw in summertime. 
"I still drink a little vodka," the man said. 
"Every day?" Ms. Burdanova said. 
"Yeah." 
"After work?" 
"Yeah. I drink to relax. Sometimes 200 grams, sometimes half a litre." 
Ms. Burdanova paused, as if holding her tongue. She put down her pen and looked at him seriously. 
"You really shouldn't," she said. 
"But I can't live without drink," he said. "What else is there?" 


