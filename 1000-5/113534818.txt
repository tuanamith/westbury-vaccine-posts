


 <EMAILADDRESS>  wrote: 
<snips> 



This is what you would learn, perhaps: 
 <URL>  
<quote in-part> 
[ ... ] 
Deliberate infection? 
One of the most contentious issues relating to disease and depopulation  in the Americas concerns the question of whether or not American  indigenous peoples were intentionally infected with diseases such as  smallpox. Despite some legends to the contrary, there seems to be no  evidence that the Spanish ever attempted to deliberately infect the  American natives.[8] 
However, there is at least one documented incident in which British  soldiers in North America attempted to intentionally infect native  people. During Pontiac's Rebellion in 1763, a number of Native Americans  launched a widespread war against British soldiers and settlers in an  attempt to drive the British out of the Great Lakes region. In what is  now western Pennsylvania, Native Americans (primarily Delawares) laid  siege to Fort Pitt on June 22, 1763. Surrounded and isolated, and with  over 200 women and children in the fort, the commander of Fort Pitt gave  representatives of the besieging Delawares two blankets that had been  exposed to smallpox in an attempt to infect the natives and end the siege. 
British General Jeffrey Amherst is usually associated with this  incident, and although he suggested this tactic in a letter to a  subordinate, by that time the commander at Fort Pitt had already made  the attempt. While it is certain that these officers attempted to  intentionally infect American Indians with smallpox, it is uncertain  whether or not the attempt was successful. Because many natives in the  area died from smallpox in 1763, some writers have concluded that the  attempt was indeed a success. A number of recent scholars, however, have  noted that evidence for connecting the blanket incident with the  smallpox outbreak is doubtful, and that the disease was more likely  spread by native warriors returning from attacks on infected white  settlements.[9] 
A second alleged incident is disputed. Colorado professor Ward Churchill  has written that in 1837 the United States Army deliberately infected  Mandan Indians by distributing blankets that had been exposed to  smallpox, resulting in at least 125,000 deaths. While it is not disputed  that the Mandans suffered greatly from smallpox in 1837, sociology  professor Thomas Brown and others have argued that the sources Churchill  cites do not support his claims of deliberate infection. Three of  Churchill's own sources have said that Churchill has misrepresented  their work. The Cherokee demographer and UCLA Professor Russell Thornton  said: "The history is bad enoughthere's no need to embellish it."[10] 
Historian Guenter Lewy agrees that there is no evidence that the United  States ever attempted to deliberately infect Native Americans. In fact,  he says, the opposite was taking place: the U.S. government had  implemented a program of smallpox vaccination for American Indians at  the time of the alleged Mandan incident. Vaccination for Native  Americans had been suggested in 1801 by President Thomas Jefferson, who  sent smallpox vaccine on the Lewis and Clark expedition for distribution  to western tribes (the vaccine was spoiled in transit). An official U.S.  vaccination program was first funded in 1832, with an act passed "for  the purpose of arresting the progress of smallpox among the several  tribes by vaccination." One study concludes that in some areas of the  United States, American Indians were eventually more thoroughly  vaccinated against smallpox than their white neighbors.[11] 
[ ... ] 
Ref: 11. Lewy's article is at  <URL>  ;  vaccination policy mentioned in Lewy, more detailed in Stearn and  Stearn, p. 62-3. Vaccine sent by Jefferson to Lewis & Clark apparently  spoiled in transit (Stearn & Stearn, p. 57); the American Indian  vaccination program was poorly funded until 1900 (Stearn & Stearn, p.  70); Indians eventually better vaccinated than whites (Stearn & Stearn  p. 59). </quote> 

--  It is better to remain silent and be thought a fool than to speak foolishness and remove all doubt. 				--Aesop 



