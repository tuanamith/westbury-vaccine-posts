


HIV Escalating: 20% Increase in Canada; Doubles to 40 Million World-Wide 
By Terry Vanderheyden and Steve Jalsevac 
OTTAWA, November 25, 2005 (LifeSiteNews.com) ? There are now almost 58,000 people with HIV in Canada, up 20% from 2000, according to recently released figures from the United Nations and World Health Organization. 
Homosexual sex accounted for the majority of new cases, at 45%, while 25% of new cases were in women, the report said. Ten years ago only 10% of new cases were in women. Heterosexual sex accounted for 30% of new cases, with intravenous drug use responsible for the remainder. Young women aged 15 to 29 were particularly affected, accounting for 42% of the new HIV cases in women, up from 13 percent 20 years ago. 
A growing phenomena is the spread of HIV to women by so-called bi-sexual men who pass on infections acquired by homosexual sex to women sexual partners. In the United States this is said to be an especially serious problem among black Americans. 
An April 4, 2004 New York Times article stated, ?Recent studies suggest that 30 percent of all black bisexual men may be infected with HIV, and up to 90 percent of those men do not know. CDC researchers have referred to these men as a ?bridge? to infection from gay men to heterosexual women.? 
The Times article went on to report, ?In government studies of 29 states, a black woman was 23 times more likely to be infected with HIV than a white woman, and black women accounted for 71.8 percent of new HIV cases in women from 1999 to 2002.? 
World-wide, the number of cases has doubled to 40.3 million, compared to 1995. An added 5 million new cases were recorded in the last year, the majority from sub-Saharan Africa, at 3.2 million. At least half a million of the 3 million AIDS-related deaths were in children. 
?It?s increasing everywhere,? said World Health Organization director of HIV/AIDS, Dr. Jim Yong Kim at a news conference Monday. ?The report illustrates very clearly that we really are failing in attempting to prevent the epidemic in the rest of the world.? 
Additionally, in Eastern Europe and Central Asia, the number of people living with HIV has increased twenty-fold in less than 10 years, according to UNAIDS Executive Director Dr Peter Piot. Uganda and Tanzania are the only sub-Saharan countries that have experienced continued declines. 
?In the two African countries, the declines in HIV rates have been due to changes in behaviour, including increased use of condoms, people delaying the first time they have sexual intercourse, and people having fewer sexual partners,? Dr. Piot said at a launch of the 2005 AIDS Epidemic Update, in Delhi. 
Dr. Piot includes condoms as a measure that has helped slow HIV transmission, whereas the real success in diminishing transmission rates has been an ongoing abstinence campaign by Ugandan President Museveni and his wife, First Lady Janet Museveni, whose success has been likened to a highly effective vaccine. 
Reasoning that HIV is spread mainly through sexual contact, the Ugandan government decided in the early 1990?s that the best way to prevent that spread would be to convince its citizens to adopt sexual abstinence and fidelity within marriage. This inexpensive, indigenous program required little outside funding or support and the results are acknowledged to be astonishing. The Ugandan HIV/AIDS rate fell by half, from 15% in 1991 to 4% by 2004. Uganda?s program was starting to be emulated by other countries such as Kenya and Senegal. 
See a related report, The Haunting Story of Africa's Ruth, by Calgary Bishop Fred Henry:  <URL>  
See related LifeSiteNews.com coverage: Uganda AIDS Prevention Success Being Undermined by Infuriated UN Condom-Pushers  <URL>  United Nations Report says Condoms Fail to Protect against AIDS 10% of the Time  <URL>  Ugandan Abstinence AIDS Prevention Program Equivalent to a Highly Effective Vaccine, Researchers Find  <URL>  Uganda's First Lady Warns Teens against Condom Use  <URL>  


 -- 


