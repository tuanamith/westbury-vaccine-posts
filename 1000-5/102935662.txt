


SyVyN11 wrote: 

When Republicans couldn't prevent executive action, President Clinton: 
-- Developed the nation's first anti-terrorism policy, and appointed  first national coordinator. 
-- Stopped cold the planned attack to blow up 12 U.S. jetliners  simultaneously. 
-- Stopped cold the planned attack to blow up UN Headquarters. 
-- Stopped cold the planned attack to blow up FBI Headquarters. 
-- Stopped cold the planned attack to blow up the Israeli Embassy in  Washington. 
--Stopped cold the planned attack to blow up Boston airport. 
-- Stopped cold the planned attack to blow up Lincoln and Holland  Tunnels in NY. 
-- Stopped cold the planned attack to blow up the George Washington Bridge. 
-- Stopped cold the planned attack to blow up the US Embassy in Albania. 
-- Tried to kill Osama bin Laden and disrupt Al Qaeda through preemptive  strikes (efforts denounced by the G.O.P.). 
-- Brought perpetrators of first World Trade Center bombing and CIA  killings to justice. 
-- Tripled the budget of the FBI for counterterrorism and doubled  overall funding for counterterrorism. 
-- Detected and destroyed cells of Al Qaeda in over 20 countries 
-- Created a national stockpile of drugs and vaccines including 40  million doses of smallpox vaccine. 
Here, in stark contrast, is part of the Bush-Cheney anti-terrorism  record before September 11, 2001: 
-- Backed off Clinton administration's anti-terrorism efforts. 
-- Shelved the Hart-Rudman report. 
-- Appointed new anti-terrorism task force under Dick Cheney. Group did  not even meet before 9/11. 
-- Called for cuts in anti-terrorism efforts by the Department of Defense. 
-- Gave no priority to anti-terrorism efforts by Justice Department. 
-- Halted Predator drone tracking of Osama bin Laden. 
-- Blamed President Clinton for 9/11. 

