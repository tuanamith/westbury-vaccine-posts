


Quote: ------------ Dell Inc., the world's biggest maker of personal computers, will sell PCs installed with Google Inc. software, dealing a setback to Microsoft Corp.'s long-time control of the desktop, according to a person briefed on the deal. 
Under the three-year agreement, Google will pay Dell to install its PCs with Google programs for searching hard drives and e-mail as well as its Web browser tool bar, said the person, who asked not to be identified because the agreement is confidential. The deal may be announced today, the person said. 
The pact is a victory for Google, the world's most-used search engine, putting its software before 100 million new PC owners over the life of the deal. Until now, Microsoft's Internet browser and services have been the default settings on Dell PCs and the agreement may derail Microsoft's attempts to draw users to its search services with its new Windows operating system. 
``The gloves have come off,'' said Jordan Rohan, an analyst at RBC Capital Markets in New York. He rates Google ``outperform'' and said he doesn't own it. ``This is a bare-knuckles distribution move for Google.'' 
Mountain View, California-based Google's software will be on sale in Dell PCs in the next few weeks, said the person. The Wall St. Journal reported news of the deal earlier today. Google spokesman Jon Murchinson declined to comment. Bob Pearson, a spokesman for Dell, declined to comment. 
Shares of Google rose $1.74 cents to $382.99 in Nasdaq Stock Market composite trading and have dropped 7.7 percent this year. Round Rock, Texas-based Dell, down 19 percent, added 12 cents to $24.30, and Redmond, Washington-based Microsoft gained 24 cents to $23.74.... 
The deal curbs Microsoft's ability to close the gap with Google. Microsoft is putting search in the new Vista version of Windows, scheduled to hit stores in January, in an effort to encourage more customers to use it. 
Microsoft vs Google 
While Microsoft dominates desktop software, Google has four times more users for its Internet search and Google leads in advertising revenue. Microsoft CEO Steve Ballmer escalated spending on the company's MSN Internet unit to challenge Google and reclaim share of Internet search. 
Microsoft handled just 11 percent of U.S. searches in March, compared with Google's 49 percent, Nielsen//NetRatings data show. 
In the new Windows, Microsoft has placed search panes on the start menu, the Internet browser, as well as the program that helps users organize files and folders. The new version of Office also has search integrated into its programs. 
Google's ability to buy a place on Dell desktops was made possible by a settlement between Microsoft and U.S. government regulators. Microsoft, in its settlement after the U.S. antitrust case, agreed to allow PC makers to promote and highlight rival software on their machines. 
The company was found to have violated antitrust law by restricting computer makers from changing the way Windows was displayed when users turned on their machines in a bid to shut out rival Netscape Communications Corp.'s browser. 
Google complained to the European Commission about Microsoft's new browser, which directs Web surfers to Microsoft's MSN and other search sites. 
``The Dell deal and similar deals like it are the Vista vaccine,'' Rohan said. ``They help to prevent against a loss of share between a tight integration between Vista and MSN search.'' ------------------- End quote 
 <URL> # 


