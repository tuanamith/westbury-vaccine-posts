


 <URL>  
   Home > Lifestyle > Health           E-mail 
   Cervical Cancer Vaccine Gives Long-Term Protection    04.06.06, 12:00 AM ET 
   THURSDAY, April 6 (HealthDay News) -- An experimental    cervical cancer vaccine remains effective for nearly    five years, and may protect women against more types of    cancer-causing viruses than originally thought,    researchers report. 
   "At 4.5 years, the vaccine was still protecting    everyone," said study author Dr. Diane M. Harper, a    professor of community and family medicine and    obstetrics and gynecology at Dartmouth Medical School. 
   The study, reported in the April 6 online issue of The    Lancet, is a follow-up on some of the more than 1,100    women who participated in a previous study gauging the    effectiveness of the vaccine. Called Cervarix, the    vaccine is under development by drug giant    GlaxoSmithKline. 
   In this latest effort, Harper's team tracked 776 of    those women, following them for up to 53 months. The    women had received either three doses of the vaccine or    three doses of placebo. 
   A specific type of infectious pathogen, called the    human papilloma virus (HPV), is credited with causing    most cervical cancers. Cervarix is designed to protect    against HPV types 16 and 18, Harper said, which account    for about 70 percent of cervical malignancies. 
   However, Harper's team found the vaccine also conferred    protection against two other HPV types, 45 and 31,    which can also trigger cervical cancer. 
   At the end of the 4.5 years, all the women still tested    positive for having been immunized with HPV 16 and 18,    the researchers found. And Harper noted that "their    antibody levels [which develop after a vaccine is    administered] on average did not decrease," meaning the    vaccine seems to have conferred ongoing protection    against these cancer-causing viruses. 
   Cervical cancer is the second most common malignant    disease in women globally, the authors note, and it    often is diagnosed at a younger age than other cancers.    The main cause of cervical cancer is continuous    infection with human papilloma viruses, especially HPV    16 and 18, which are spread by sexual contact. 
   Harper said women who got Cervarix didn't report any    more long-term problems (such as thyroid problems)    compared to those who received a placebo injection. 
   The vaccine's apparent long-term effectiveness is    promising, said Rachel Winer, a University of    Washington, Seattle, research scientist and HPV    researcher. Once a cervical cancer vaccine hits the    market, young women in their late adolescence or early    20s would be the most likely candidates to receive it,    she said. 
   The multi-strain cross-protection Cervarix offers is a    bonus, Winer added, and "worth investigating further"    to see what is behind it. 
   Another vaccine for cervical cancer, called Gardasil,    is also awaiting approval from the U.S. Food and Drug    Administration, noted Janet Skidmore, a spokeswoman for    the vaccine's developer, Merck. She believes it could    be on the market by later this year. 
   Gardasil protects against HPV 16 and 18, and also    against HPV 6 and 11, which Skidmore said accounts for    about 90 percent of genital warts. 
   More information 
   To learn more about cervical cancer, visit the    National Cancer Institute.  <URL>  
   More On This Topic 
   E-mail  <URL>  

