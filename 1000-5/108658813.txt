


Washington Post Health Section Interactions 
Tuesday, May 9, 2006; HE02 






More to Know About Breast Implants 
Quick Study is a great resource, but sometimes ["Silicone does not seem to  raise the risk of cancer in women," May 2] important points are missed. 
After a successful class action breast implant lawsuit against them, Dow  Corning has funded more than 40 research articles on breast implants. All  include one or more authors from the new study and all conclude that breast  implants are safe. In contrast, researchers who do not have financial  conflicts consistently report implant problems. 
Cancer is not the only concern. The National Cancer Institute found a more  than doubling of suicides among women with breast implants compared to other  plastic surgery patients. And a new study in a peer-reviewed chemistry  journal, Analytical Chemistry, found toxic levels of platinum in the breast  milk, urine, hair and nails of women with silicone gel breast implants.  Platinum is used in the manufacture of breast implants. 
Diana Zuckerman, PhD 

President, 
National Research Center for Women &amp; Families 

Washington 





Editor's Note: The Quick Study report should have noted that the institute  that funded the study has received support from Dow Corning. 
------------------------ 
Los Angeles Times, 
To the editor: 
As a former congressional investigator who initiated hearings on FDA issues,  I have seen FDA morale rise and fall through the years, but it has certainly  been at an all-time low in recent months. Lack of safeguards and political  interference are not just issues for prescription drugs, however. 
As baby boomers near retirement age, their increased reliance on implanted  medical devices also necessitates a permanent commissioner and a focus on  science. 
Whatever the FDA is reviewing, long-term safety data are essential.  Unfortunately, such data are rarely required by today's FDA. 
In addition, fears about the avian flu should remind us that the FDA needs  to focus its resources on safe and effective vaccines as well. The Plan B  controversy is the tip of the iceberg. All FDA decisions need to be based on  solid science, not ideology or industry pressure. That is true for medical  devices and vaccines as much as for prescription drugs. 
DIANA ZUCKERMAN 
President 
National Research Center 
for Women & Families 
Washington 


www.BreastImplantInfo.org Dr.Zuckerman's website 
www.BreastImplantAwareness.org 


*** Posted via a free Usenet account from  <URL>  *** 

