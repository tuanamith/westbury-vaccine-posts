


By Carey Goldberg  <URL>      boston.com News your connection to The Boston Globe  
   Home > News > Boston Globe > Health / Science 
   The Boston Globe  
A fresh shot 
Soon-to-be approved vaccine can prevent cervical cancer; the question is, are people ready to accept it? 
   By Carey Goldberg, Globe Staff  |  May 29, 2006 
   Dr. David Watson has already begun thinking about his    pitch for a new vaccine to block the sexually    transmitted virus that causes cervical cancer. 
   ``What I will probably do is point out that last year    alone, more people died of cervical cancer, which was    pretty much directly produced by Human Papillomavirus,    than were killed in 9/11," said Watson, president of    Pediatrics West, a private practice with offices in    Concord, Westwood, and Groton. ``People appreciate    those sorts of comparisons." 
   Parents will soon start hearing similar pitches from    their children's doctors, supplementing a television    and magazine ad campaign already begun by Merck & Co.,    the manufacturer of the vaccine, which is expected to    receive federal approval next week. 
   While most pediatricians support the new vaccine,    called Gardasil, they also recognize that it may not be    quickly or universally adopted. 
   Early studies show that the vaccine is largely    effective at preventing the cervical cancer and genital    warts caused by four predominant strains of the    sexually transmitted Human Papillomavirus, or HPV. But    there are other strains the vaccine doesn't address, so    women will still need to get annual Pap smears to check    for cervical cancer, which kills 4,000 Americans    annually and nearly half a million worldwide. 
   The vaccine is also inconvenient -- three separate    shots must be given over six months -- and expensive,    costing $300 to $500 for the required doses. Insurance    companies generally cover the cost of vaccinations that    receive federal approval. 
   Some parents are skeptical about vaccines in general,    or are uncomfortable facing the fact that their cute    preteen might someday be sexually active. Gardasil is    likely to be recommended for girls as young as 9 to 11. 
   ``Most of us are a bit leery about going into the gory    details about different sorts of sexual transmission,"    Watson said. 
   But that might not be necessary, he and other doctors    said. 
   ``I'll probably focus on the fact that the vaccine    prevents cervical cancer in my sell, and not on the    fact that this prevents a sexually transmitted    disease," said Dr. Joseph Hagan , a Burlington, Vt.,    pediatrician active in the American Academy of    Pediatrics. ``You know why? Because my Dad sold    insurance, and I know what to emphasize and what not to    emphasize." 
   Parents, too, say they might not get into awkward    questions of sexuality. In general, said Lynn Randall    of Concord, whose daughters are 12 and 15, when one of    her girls gets a shot, ``We're not telling her what    she's being vaccinated against; usually it's just a    shot to her -- one of many shots she gets in her    pediatrician's visits." 
   Irene Freidel , of Littleton said that if her    11-year-old daughter, Claire, were vaccinated tomorrow,    she would most likely tell the girl only that ``it was    something that was going to protect her, like any other    vaccination, and is good for her health." 
   ``She just turned 11," Freidel said. ``We have talked    about the facts of life, but I don't know that she's    ready at this point to get into more than we've already    discussed." They will talk more in the future about the    hazards of sexuality, she said, but right now, ``I    cling to her innocence." 
   Overall, Freidel said, she welcomes the vaccine and    does not worry that, as some advocates of abstinence    have warned, it might encourage promiscuity. 
   ``I adore my daughter more than anything in the world,"    she said, ``and I can't predict what she's going to do    as a teenager, what risks she's going to be exposed to.    To me, this is just extra protection." 
   Such pro-vaccine parents are sure to be the majority,    said Dr. Jessica Kahn , of Cincinnati Children's    Hospital Medical Center, who has researched the    expected attitudes. 
   A national survey of 513 pediatricians published in    March, found that about three-quarters of pediatricians    would recommend the vaccine, 40 percent thought parents    would be reluctant to have their children vaccinated    against a sexually transmitted disease, and 70 percent    expected parents to worry about the safety of a new    vaccine. In a separate paper published earlier this    month in the journal Pediatrics, a survey mailed to    1,600 parents found that 65 percent would lean toward    permitting their preteen child to get the vaccine. 
   ``Most parents will be eager to have their daughters    vaccinated against HPV as long as they know they're    sending the right preventive messages: that they still    need to practice safe sexual behaviors, postpone sexual    initiation as long as possible," and the like, Kahn    said. 
   The logistics may pose even more problems than parental    attitudes, several doctors said. 
   Initially, the vaccine will likely not be covered by    insurance or paid for by the state, and it could cost    as much as $500. ``That's a dealbreaker right there,"    said Dr. Victoria McEvoy , medical director and chief    of pediatrics of the Massachusetts General West Medical    Group. 
   Massachusetts has not made a decision yet on whether to    fund the vaccine. 
   ``We are several months away from making a decision,"    said Donna Rheaume, spokeswoman for the state    Department of Public Health. 
   The three-visit regime that the vaccine will require is    also a ``huge problem," McEvoy said. Without getting    the full three-dose regimen, teens won't get the full    benefit of the vaccine. 
   ``Adolescents, unless they're dragged by their parents,    don't come in unless they think they're pregnant, they    have acne, or they need a note for soccer or work." 
   Nonwhites and those with poor access to healthcare    stand to benefit the most from the vaccine because they    are the least likely to get annual Pap smears and    follow-up care after a problematic test, said Dr.    Elizabeth Garner, a gynecological oncologist at Brigham    and Women's Hospital.Yet they are precisely the people    who will have the hardest time getting access to the    vaccine and getting the full dosage, she said. 
   McEvoy said she'll likely wait a year or so before    offering the vaccine in her own practice, waiting for    it to ``sort itself out a little bit," get covered by    insurance, and paid for by the state. She'll also be    watching for reports of unexpected side effects; after    28 years of practice, she likes medical innovations to    accumulate some history of safety before she recommends    them. 
   In contrast, Hagan of Vermont plans to offer the    vaccine to his patients as soon as it's available,    confident that the state Health Department will    subsidize it. 
   ``If we have a safe and effective way to prevent a    particularly prevalent form of cancer, then why    wouldn't we do that?" 
   Carey Goldberg is reachable at    goldberg at globe.com 
     * PRINTER FRIENDLY Printer friendly      * E-MAIL E-mail to a friend  <URL>  By Carey Goldberg 

