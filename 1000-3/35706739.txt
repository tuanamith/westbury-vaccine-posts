



<RamRod Sword of Baal> wrote in message hahahaha nothing, black and hot. Shine is short this year, farmers are burning corn in heaters to cut back because of high gas bills. 
Something for you to read with your coffee. 

   Hepatitis B Prevention for Men who have Sex with Men 

What is hepatitis B? Hepatitis B is a vaccine preventable liver disease caused by infection with hepatitis B virus (HBV). In 2001, approximately 78,000 people in the United States were newly infected with HBV. Chronic (life-long) infection occurs in 2%-6% of adults with newly acquired HBV infection and is more likely to occur in HIV-infected people. The most serious forms of liver disease that can result from chronic HBV infection are cirrhosis (scarring) of the liver and liver cancer. Each year, about 5,000 people die as a result of liver disease caused by HBV. About 1.25 million people in the United States have chronic HBV infection and can infect others. How is hepatitis B virus spread? Hepatitis B virus is spread when blood from an infected person enters the body of a person who is susceptible to HBV infection.  HBV is known to be 100 times more infectious than HIV and is easily spread by sex. Risk factors for infection among men who have sex with men (MSM) include having multiple sex partners, practicing unprotected receptive anal intercourse, and having a history of other sexually transmitted diseases. Injection drug use is another major risk factor for HBV infection. How do you know if you are infected with HBV? Only a blood test can tell for sure. Not all people who are infected with HBV look or feel sick, so you might be infected and unknowingly spreading the virus. Some people get symptoms when they first get infected. These people might: have yellow eyes or yellow skin (jaundice) lose their appetite have nausea, vomiting, fever, stomach or joint pain feel extremely tired and not be able to work for weeks or months. People with chronic HBV infection might not feel sick for many years, but will have symptoms if they develop the most serious forms of hepatitis B, like cirrhosis or liver cancer. 
Is there a treatment for hepatitis B? There are no medicines for the treatment of newly acquired HBV infection. Medicines are available to treat people with chronic hepatitis B. In 4 out of 10 people treated with these medicines, the chance of developing severe disease is decreased, but the medicines do not eliminate the virus. Are MSM at increased risk for hepatitis B? Yes. Hepatitis B disproportionately affects MSM. About 15%-20% of all new HBV infections in the United States are among MSM. Hepatitis B vaccine can prevent these infections. Although hepatitis B vaccine has been available since 1982, hepatitis B vaccination rates are very low among young MSM. How effective is hepatitis B vaccine? Vaccination is the best way to prevent HBV infection and its chronic consequences. Hepatitis B vaccine is the first anti-cancer vaccine, as it prevents liver cancer caused by HBV. Three doses are usually needed for complete protection. The second dose is usually given 1-2 months after the first dose and the third dose usually given 4-6 months after the first dose. Immune compromised people (e.g., people with HIV/AIDS) might not respond as well to hepatitis B vaccine. They should be tested 1-2 months after the third dose of vaccine to see if they responded. Those who do not respond to the first three-dose vaccine series should be revaccinated with another full hepatitis B vaccine series. If the person doesn't respond to the second series, more vaccine doses are not recommended. Non-responders to the vaccine are at risk for HBV infection and should take appropriate precautions to protect themselves. Is the vaccine safe? Yes, hepatitis B vaccine has been shown to be very safe. More than 30 million adolescents and adults have been vaccinated in the United States. The vaccine is well tolerated by most recipients. The risk of the vaccine causing a serious allergic reaction is extremely small. Soreness at the site of injection is the most common reported side effect; some people might have a fever after vaccination. The use of hepatitis B vaccine is strongly endorsed by the medical, scientific and public health communities as a safe and effective way to prevent disease and death from hepatitis B What should MSM do to protect themselves? Talk to your health care professional about getting vaccinated. Hepatitis B vaccine is safe and effective, and is your best protection against HBV infection. The most reliable way to avoid transmission of STDs is to abstain from sexual intercourse or to be in a long-term, mutually monogamous relationship with an uninfected partner. If you choose to have sexual intercourse with a partner whose infection status is unknown or who has an STD, you should use latex condoms correctly and consistently every time. Whether or not the use of latex condoms prevents HBV infection is not known, but their proper use might reduce the spread of HBV, as well as protect against other sexually transmitted diseases. Do not shoot drugs; if you shoot drugs, stop and get into a treatment program; if you can't stop, never share needles, syringes, water, or "works". Do not share personal care items that might have blood on them (e.g., razors, toothbrushes). Talk to your health professional about getting tested for other STDs (e.g., syphilis, gonorrhea, chlamydia, HIV). Ask your health professional about other vaccines that you might need (e.g., hepatitis A vaccine). What if I want more information? Alter MJ, Hadler SC, Margolis HS, et al. The changing epidemiology of hepatitis B in the United States: Need for alternative vaccination strategies. JAMA 1990;263:1218-22. Goldstein ST, Alter MJ, Williams IT, et al. Incidence and Risk factors for acute hepatitis B in the United States, 1982-1998: Implications for vaccination programs. JID 2002;185:713-19. Margolis HS, Alter MJ, Hadler, SC. Hepatitis B: Evolving epidemiology and implications for control. Sem Liv Dis 1991;11:84-92. Centers for Disease Control and Prevention. Under vaccination for Hepatitis B among young men who have sex with men - San Francisco and Berkeley, California, 1992-1993. MMWR 1996;45(10):215-7. MacKellar DA, Valleroy LA, Secura GM, et al. Two Decades After Vaccine License: Hepatitis B Immunization and Infection Among Young Men Who Have Sex With Men. AJPH 2001;91:965-71. Centers for Disease Control and Prevention. Sexually Transmitted Diseases Treatment Guidelines. MMWR 2002;51(6). 

MSM Quick Links 
Dear Colleague Letter FAQs HAV MSM Fact Sheet HBV MSM Fact Sheet Educational Materials Dear Colleague letter NASTAD Press Statement CDC Action Memo (PDF) Order free materials online Contact Us 







Online Resources 
CDC Websites: 
Division of STDs HIV/AIDS Prevention Immunization Program 
Other online resources for information about MSM health and information about hepatitis A and B immunization: 
www.immunize.org www.hepprograms.org www.hepclinics.com www.stophep.com www.glma.org www.methresources.gov 



Division of Viral Hepatitis 
DVH, NCID, CDC Mail-Stop G-37 1600 Clifton Road Atlanta, GA 30333 24-hour toll-free hotline: 1-888-4HEPCDC (1-888-443-7232) 







-------------------------------------------------------------------------------- 

This page last reviewed December 1, 2005 
All information presented in these pages and all items available for download are for public use. 
Division of Viral Hepatitis Centers for Disease Control and Prevention National Center for Infectious Diseases 


US Department of Health and Human Services 
Privacy Policy | Accessibility 

posted in toto, naturally 





