


Job Title:     Project / Acct Manager - Life Science MFG Job Location:  PA: Philadelphia Pay Rate:      DOE Job Length:    full time Start Date:    2006-02-02 
Company Name:  Computer Methods Corporation Contact:       Recruiter Phone:         No calls Fax:           No faxes 
Position Location: Metro Philadelphia area 
Successful candidates will possess some or all of the following experience and qualifications: 
* Five (5) years or more years of experience in the Life Sciences industry (pharmaceutical, vaccines, biotech) 
* Demonstrated knowledge and understanding of life science shop floor manufacturing processes 
* Demonstrated leadership abilities to direct manufacturing or supply chain system implementation and integration teams 
* Experience in the design, configuration, implementation, and integration of manufacturing or supply chain systems 
* Experience with SAP centric manufacturing modules (PP, QM, MM, WM, PM, PP-PI, SCM, PLM, etc.) 
* Experience with various manufacturing, supply chain, and quality systems products and tools 
* Experience in applying industry standards (such as SCOR, ISA S88, and ISA S95) 
* Exposure to manufacturing operations improvement initiatives (such as 6 sigma, 5S, Visual 
Performance Measures, Throughput Optimization, Set-Up Time Reduction, Focused Improvement, etc.) 
* Strong verbal and written communication skills 
* Bachelor*s degree or higher 
* PMI and/or APICS Certification 
Please include the job number in each subject line of your email. We do not accept faxed resumes! 
Please email resume to:  <EMAILADDRESS>  
Please refer to Job code 843J when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



