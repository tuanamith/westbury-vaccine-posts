


In article < <EMAILADDRESS> >,  Brooks Moses < <EMAILADDRESS> > wrote: 


Yes. But not on what is covered by contract, secrecy, and the like. 

If sufficiently curious, you can find an old article of mine on the  subject webbed at: 
 <URL>  fense_of_pvt_orderings.htm 

The problem is that copyright holders can't enforce the rights granted  to them by copyright law, given current technology. One could argue that  nobody should have a lock on his door, since trespass is illegal. But if  there is no practical way of enforcing the legal rule against someone  else trespassing on my property, I have a good reason to want a lock.  

I have no objection to requiring a notice of the existence of limits,  and some pointer to additional information. 

It is, however, an example of my point--that fair use doesn't give you a  right to use things, it just limits my right to use copyright law for  stopping you. 

And DRM can't prevent your from talking about it. 

Unless you tell them that it doesn't. Hertz and Avis don't give you the  right to use their cars as if they were pieces of physical property that  you now own. There are lots of familiar examples of selling people some  level of control over property short of complete ownership.  
... 


So you object to a restaurant with a sign over the door saying "guests  are invited to discuss music and art, but anyone arguing politics will  be removed?" Why? Why shouldn't people be free to create spaces with  particular rules of behavior, and invite those who approve of those  rules to come into them? Do you object, on the same basis, to all  moderated listserves and Usenet groups--which are the cyberspace  equivalent? 

I want to separate the two questions. I agree that there are real issues  of notice, although I'm a little worried that the more stringently one  insists on them, the easier it is for courts to use them as an excuse  for voiding contracts the court disapproves of. But the point I feel  strongly about, and so am interested in arguing, is that freedom of  contract is the right model--and permits either party to set any terms  he likes, where if the terms are not acceptable to the other party the  transaction doesn't happen. 
... 

As it happens, I have no way of doing the latter. But I could sell the  book to people who agree not to quote from it online, use Google to  identify some who violate that agreement, and sue them for breech of  contract. It wouldn't be a very sensible thing for me to do, but why  would it be wrong? 

It sounds as though you want to replace freedom of contract with  tradition--all transactions are defined as having whatever meaning they  are "culturally treated" as having. Would you extend that general  principle to marriage? Employment? 
Doesn't it make more sense to treat the traditional meaning as the  default--the terms implied if you hand me money, I hand you the goods,  and nothing else is said--and permit the parties to modify them if they  so wish? 

One of the things that bothers me about this  line of  argument--generally in other contexts ("commodification")--is that it  goes directly against the principle of freedom of speech. The usual view  of freedom of speech is that the fact that an act is also speech--flag  burning, say--should make it harder to prohibit it. But you are arguing  that an act should be prohibited because it is speech--because it tends  to change how people think about things, which is what persuasive speech  does. It's as if you argued that people should be prohibited from  burning flags they own because doing so might encourage disrespect for  the flag. Does that strike you as a legitimate argument?  
Should I be banned from trying to persuade people that it is legitimate  to sell an ebook without permission to print out? If not, how can you  argue that I should be banned from actually selling an ebook on those  terms for fear my doing it might persuade people that it is legitimate? 

It seems to me that the lack of legal machinery is a feature, not a bug,  given how clumsy the legal system is.  

But note that, once DRM becomes common, the problem you are raising goes  away, since people will take it for granted that the materials might be  protected--and if they care will ask. I have no objection to requiring  notice, provided that the requirement is clear enough so that it doesn't  give courts a blank check to cancel any contracts they don't like. I'm  thinking specifically of a polio case which is one of the reasons there  is a problem getting vaccines--the court held that the vaccine wasn't  defective, since the very small chance that vaccination would cause  polio was unavoidable at that stage of the technology, recognized that  the provider had included information on the risk with the vaccine, but  held that the provider was liable when the risk eventuated because it  had not made sure that every patient was individually informed of the  risk by the people administering the vaccine, who of course were not  under the provider's control. 
Suppose we have some standard symbol, large and brightly colored, that  means "this CD has some sort of DRM software on it," and require it on  any CD with such software. Do you consider that that deals with your  objection? 

Or in other words, you were starting to make a circular argument.  Something to which the public has been given unfettered access is  something to which the public has been given unfettered access--but a CD  with DRM is not such a thing. 

And again, I want to separate the issue of fraud, which you keep  bringing in, from the issue of DRM. Practically any transaction could be  done fraudulently--but that doesn't mean that all transactions are  "somewhat morally repulsive." What is there at all morally repulsive  about selling someone access to IP short of complete and unfettered  access? 
--  Remove NOPSAM to email www.daviddfriedman.com 

