


30 Dec 2005 12:16:10 -0800 in article < <EMAILADDRESS> > " <EMAILADDRESS>  (Larry Weisenthal)" < <EMAILADDRESS> > wrote: 
I found this article from the web: 
Townsend Letter for Doctors and Patients: Nathan Pritikin and atheroma < <URL> > 
I don't know anything about its reliability, but because Pritikin's person and diet may interest some people I include the content of the article here for comments and verification: 
    "Nathan Pritikin at age 43 was told by his cardiologist     that he was at great risk of death from myocardial     infarction (MI). He began to live on a diet patterned     after the black population in Uganda. In 1959 the black     population of Uganda was shown to live on a vegetarian     diet made up mostly of the grains corn, barley and     millet. (1) It was a very low fat diet with less than 15%     fat calories. (1) In 1960 it was shown that this black     population was essentially free from death from MI. (2)     In time, Pritikin felt that this diet had led to his     salvation and he founded a clinic to treat cardiac     patients with the Pritikin diet. Over a 17-year period,     the Pritikin diet was widely publicized in the news     media. Pritikin developed leukemia and was treated with     chemotherapy for over 10 years and then, at age 60, he     terminated his life in suicide. An examination of his     arteries showed them to be free from atheroma. 
    Background for a suggestion 
    The black population of Uganda who lived on a grain     vegetarian diet in 1960 was found to be nearly free from     death from MI but was found to have about as much     atheroma as the black population of St Louis, USA. (2) 
    In 1973, E.P. Benditt and J.M. Benditt, using an     electron-microscope, presented evidence that the vast     bulk of human atheroma is composed of proliferating     smooth muscle cells that are monoclonal in origin and     that human atheroma is for the most part made up of     benign tumors. (3) 
    In 1978 there was an editorial in The Lancet on "Virus     Infections and Atheroma." In it the work of Benditt and     Benditt was reviewed and the suggestion was made that     human atheroma may indeed be mostly benign tumor and that     a virus infection may be the cause of the neoplastic     conversion of smooth muscle cells. The editorial ends     with "The overwhelming emphasis placed on cholesterol and     lipoproteins in atherogenesis has diverted effort away     from other lines of research for nearly two decades.     Lipoproteins are only one component in an extremely     complex system of cellular and molecular interaction."     (4) 
    In 1983, Melnick et al. examined plaques of atheroma     removed in blood vessel surgery and in 25% of their     samples they found antigen to the cytomegalovirus (CMV).     They suggested that the CMV may be causing smooth muscle     cells to become neo plastic, to proliferate and to     migrate from the media into the intima of arteries. They     suggest that if it can be demonstrated that a virus is     the cause of the neoplastic transformation of smooth     muscle cells, then a vaccine against the virus may be a     way to prevent the formation of human atheroma. (5) 
    Conclusion 
    It could have been in the case of Pritikin, that many     years of chemotherapy did not cure his leukemia, but did     abolish his benign tumors which were his atheroma. It is     suggested that pathologists may care to begin to do     examinations of arteries of cancer patients who have died     after treatment with chemotherapy. If indeed most such     patients are found to be free from atheroma, as was the     case with Pritikin, this would throw a new light on the     cause of the formation of human atheroma. Such a finding     may lead to the sug gestion that everyone at about age 45     have a course of chemotherapy to abolish the atheroma     formed at that age. 
    References 
    1. Shaper AC, Jones KW. Serum cholesterol, diet, and     coronary heart disease in Africans and Asians in Uganda.     Lancet ii:534-7, 1959. 
    2. Thomas Wilber et al. Incidence of myocardial     infarction correlated with venous and pulmonary     thrombosis and embolism. The American Journal of     Cardiology: 41-47, 1960. 
    3. Benditt EP The origin of atherosclerosis. Scientific     American, Feb, 381-3, 1977. 
    4. Editorial. Virus infections and atherosclerosis.     Lancet 11:821-2, 1978. 
    5. Malnick JL el al. Cytomegalovirus antigen within     smooth muscle cells. Lancet ii: 644-6. 1983." 

--  Matti Narkia 

