


"Alan Kennedy" < <EMAILADDRESS> > wrote in  <NEWSURL> :  

However, if it is a genetic susceptibility, then no matter what a person takes, they are still susceptible.  Maybe the body will develope permanent antibodies, and all sign of the disease will be gone and not reappear.  But if there is some other underlying cause, such as genetics, then it is still possible to be triggered, by another agent.  In which case, it will not have been cured.   
Also, my fear would be that if it is decided that somehow MAP is the trigger, there will be less desire to fund further studies since "the cure has been found" and people who are not suffering due to MAP will still suffer.   
And what of a reinfection from another agent?  Will another agent be looked for or will it be assumed that the anti-MAP treatment failed.   
I still think CURE is too strong of a word.  It is a treatment.  And, as yet, and unproven one.      
And the potential for the bacteria to mutate into a form which is not stopped by the vaccine?  The possibility that the vaccine will not work 100%, but will instead kill off enough to stop outward symptoms yet leave enough live bacteria to continue doing damage.  

Why wrong?  If only 10-90 percent of cases are MAP, then further research is clearly indicated.  If it turns out to be the 10% factor, then the statements that this will eradicate the disease are patently false.  Even at 90% of cases, the remaining 10% won't get any benefit from this treatment.  How is that eradication?   
Until we know what causes it, there can't be a cure.  Only a treatment.  

I do not know much about the H pylori.  I have not looked into it as much as I have the Crohn's.   
Does the MAP vaccine take into account the remission factor?  That this disease can go into remission for no apparent reason?   
I am sure people who go into remission feel they are cured.  But the disease can still return.  

Yes, the MAP vaccine treatment has the potential to be a permanent cure.  But at the moment, it is just a potential treatment.  Until long term studies are done, there is no way of knowing.  I seem to recall the early thoughts on Remicade suggested a long term effect, something we now see isn't accurate.  There was a lot of hope pinned on Remicade.  And now on variations of that drug.  And on many other treatments.   
I do hope the MAP vaccine is a permanent cure, for the people who actually have MAP induced Crohn's.   
But as a cure for Crohn's itself, I doubt it.  Yes, I am being picky with wording, but it is an important difference.  To say this will eradicate Crohn's (which the article says) suggests it will cure everyone... yet as even you point out, only 10-90% of the people have MAP induced Crohn's.  This isn't eradication.  This isn't a Cure.   
I take offense not to the treatment, but the sensational way it is being announced.  Sensationalism doesn't help.   

