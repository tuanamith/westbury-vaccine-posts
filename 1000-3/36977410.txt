



  Dec 12 2:31 AM US/Eastern 
China's human trials of an AIDS vaccine were proceeding "smoothly," state press reported, nine months after the program was launched. The last batch of 15 Chinese volunteers received the vaccine over the weekend and reported no side-effects in the crucial first 24 hours, Xinhua news agency quoted medical officials involved in the trial as saying. 

"The first 24 hours are a vital period for observation," the deputy director of the Guangxi regional disease prevention and control centre in the nation's south, Chen Jie, said, according to Xinhua. 
"So far, no volunteers have reported ill reactions. They have entered a relatively stable period for observation." 
The latest inoculations bring to 49 the number of Chinese volunteers to have received the potential AIDS vaccine since the centre began trials on March 12 in Nanning, the capital of Guangxi province. 
"So far the tests have been going on smoothly," Chen said. 
With all the 49 volunteers inoculated, the first phase of the three-phase trials has come to an end. 
The initial gathering of clinical data from the volunteers will be completed in June next year, after which a decision will be made on whether the centre can go ahead with phase two, Chen said. 
The second phase of the trials will test the immune nature and safety of the vaccine, according to Chen. 
There have been about 35 AIDS vaccine trials on humans throughout the world, most of which are still in the first phase, according to Xinhua. 
In the 24-year history of AIDS, only one vaccine has completed the full three-phase trial process -- AIDSVAX, which was found to be a disappointing failure. 
In the latest trial to make headlines, Swedish researchers announced on December 1 that their trial for a so-called DNA vaccine against the AIDS virus was going better than expected. 
The vaccine had successfully completed the first phase of tests among 40 Swedish HIV-negative volunteers, the Karolinska Institute in Stockholm said. 
"It has been more effective than we thought it would be," the professor and head of clinical testing at the institute, Eric Sandstroem, told AFP. 
"We have also failed to find any vaccine-related side effects at all." 
Karolinska professor Britta Wahren, who developed the vaccine, also expressed optimism. 
"There is every reason to be hopeful, even though the study is not finished," Wahren said. 




