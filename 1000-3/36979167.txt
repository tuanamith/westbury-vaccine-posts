


"But thousands of children cared for by Homefirst Health Services in metropolitan Chicago have at least two things in common with thousands of Amish children in rural Lancaster: They have never been vaccinated. And 
they don't have autism. "We have a fairly large practice. We have about 
30,000 or 35,000 children that we've taken care of over the years, and I don't think we have a single case of autism in children delivered by us who never received vaccines," said Dr. Mayer Eisenstein, Homefirst's medical director who founded the practice in 1973." 
 <URL>  
The Age of Autism: 'A pretty big secret' 
By Dan Olmsted UPI Senior Editor Dec. 7, 2005 at 2:08PM 
It's a far piece from the horse-and-buggies of Lancaster County, Pa., to the cars and freeways of Cook County, Ill. 
But thousands of children cared for by Homefirst Health Services in metropolitan Chicago have at least two things in common with thousands of Amish children in rural Lancaster: They have never been vaccinated. And 
they don't have autism. 
"We have a fairly large practice. We have about 30,000 or 35,000 children that we've taken care of over the years, and I don't think we have a single case of autism in children delivered by us who never received vaccines," said Dr. Mayer Eisenstein, Homefirst's medical director who founded the practice in 1973. Homefirst doctors have delivered more than 15,000 babies at home, and thousands of them have never been vaccinated. 
The few autistic children Homefirst sees were vaccinated before their families became patients, Eisenstein said. "I can think of two or three 
autistic children who we've delivered their mother's next baby, and we aren't really totally taking care of that child -- they have special care needs. But they bring the younger children to us. I don't have a single 
case that I can think of that wasn't vaccinated." 
"We do have enough of a sample," Eisenstein said. "The numbers are too large to not see it. We would absolutely know. We're all family doctors. If I have a child with autism come in, there's no communication. It's frightening. You can't touch them. It's not something that anyone would miss." 
This column has been looking for autism in never-vaccinated U.S. children in an effort to shed light on the issue. We went to Chicago to 
meet with Eisenstein at the suggestion of a reader, and we also visited 
Homefirst's office in northwest suburban Rolling Meadows. Homefirst has 
four other offices in the Chicago area and a total of six doctors. Eisenstein stresses his observations are not scientific. "The trouble is this is just anecdotal in a sense, because what if every autistic child goes somewhere else and (their family) never calls us or they moved out of state?" 
Homefirst follows state immunization mandates, but Illinois allows religious exemptions if parents object based either on tenets of their faith or specific personal religious views. Homefirst does not exclude or discourage such families. Eisenstein, in fact, is author of the book "Don't Vaccinate Before You Educate!" and is critical of the CDC's vaccination 
policy in the 1990s, when several new immunizations were added to the schedule, including Hepatitis B as early as the day of birth. Several of the vaccines -- HepB included -- contained a mercury-based preservative 
that has since been phased out of most childhood vaccines in the United 
States. 
Medical practices with Homefirst's approach to immunizations are rare. "Because of that, we tend to attract families that have questions 
about that issue," said Dr. Paul Schattauer, who has been with Homefirst for 20 years and treats "at least" 100 children a week. 
Earlier this year we reported the same phenomenon in the mostly unvaccinated Amish. CDC Director Dr. Julie Gerberding told us the Amish 
"have genetic connectivity that would make them different from populations that are in other sectors of the United States." Gerberding said, however, studies "could and should be done" in more representative unvaccinated groups -- if they could be found and their autism rate documented. 
Chicago is America's prototypical "City of Big Shoulders," to quote Carl Sandburg, and Homefirst's mostly middle-class families seem fairly 
representative. A substantial number are conservative Christians who home-school their children. They are mostly white, but the Homefirst practice also includes black and Hispanic families and non-home-schooling Jews, Catholics and Muslims. 
Schattauer, interviewed at the Rolling Meadows office, said his caseload is too limited to draw conclusions about a possible link between vaccines and autism. "With these numbers you'd have a hard time proving or disproving anything," he said. "You can only get a feeling about it. 
"Sometimes you feel frustrated because you feel like you've got a pretty big secret," Schattauer said. He argues for more research on all 
those disorders, independent of political or business pressures. 
"In the alternative-medicine network which Homefirst is part of, there are virtually no cases of childhood asthma, in contrast to the overall Blue Cross rate of childhood asthma which is approximately 10 percent," he said. "At first I thought it was because they (Homefirst's 
children) were breast-fed, but even among the breast-fed we've had asthma. We have virtually no asthma if you're breast-fed and not vaccinated." 
Several studies have found a risk of asthma from vaccination; others have not. Studies that include never-vaccinated children generally find 
little or no asthma in that group. Earlier this year Florida pediatrician Dr. Jeff Bradstreet said there is virtually no autism in home-schooling families who decline to vaccinate for religious reasons -- lending credence to Eisenstein's observations. 

"It's largely non-existent," said Bradstreet, who treats children with autism from around the country. "It's an extremely rare event." 
Bradstreet has a son whose autism he attributes to a vaccine reaction at 15 months. His daughter has been home-schooled, he describes himself as a "Christian family physician," and he knows many of the leaders in the 
home-school movement. 
Federal health authorities and mainstream medical groups emphatically dismiss any link between autism and vaccines, including the mercury-based preservative thimerosal. Last year a panel of the Institute of Medicine, part of the National Academies, said there is no evidence of such a link, and funding should henceforth go to "promising" research. Thimerosal, which is 49.6 percent ethyl mercury by weight, was phased out of most U.S. childhood immunizations beginning in 1999, but the CDC 
recommends flu shots for pregnant women and last year began recommending them for children 6 to 23 months old. Most of those shots contain thimerosal. 
But where is the simple, straightforward study of autism in never-vaccinated U.S. children? Based on our admittedly anecdotal and limited reporting among the Amish, the home-schooled and now Chicago's Homefirst, that may prove to be a significant omission. -- This ongoing series on the roots and rise of autism welcomes comment. E-mail:  <EMAILADDRESS>  


