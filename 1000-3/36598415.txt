


Another example of pharmo-capitalists using their skewed "research" to sell drugs: 

Are U.S. Health Experts Inflating Flu Statistics? 
By Ed Edelson HealthDay Reporter 
FRIDAY, Dec. 9 (HealthDay News) -- A Harvard grad student is charging that the U.S. government is hyping the threat of the annual (non-avian) strains of influenza. Specifically, Peter Doishi says, the estimate of 36,000 flu-related deaths a year by the Centers for Disease Control and Prevention is unsupported by the available data. 
And, he suspects, the numbers may be inflated to help drug companies sell more flu vaccine. 
It's a familiar charge -- a quick scan of the Internet turns up several Web sites claiming much the same thing -- and like many others who make the claim, Doshi is not a medical expert. He's a student in Harvard's department of East Asian studies. 
But he presents his charges with one notable difference: They appear in the form of an article published in this week's issue of the prestigious British Medical Journal. 
In his one-page article, Doshi lauded the BMJ's "system of open discussion and open debate through their on-line bulletin board ... a very democratic form of scientific discourse." 
His criticism centers on a 2003 paper in the Journal of the American Medical Association in which CDC experts increased their estimate of flu-related deaths from 20,000 a year to 36,000 a year. The reasons the agency used to justify that rise are dubious at best, Doshi said. 
For one thing, the National Center for Health Statistics lists only a few hundred deaths a year as directly caused by influenza, Doshi said. And the major explanation for the increased estimate -- the aging of the American population that puts more people in the highly vulnerable over-65 group -- doesn't hold water, he maintained. 
"The 65-plus population grew just 12 percent between 1990 and 2000," Doshi wrote. How can the CDC justify an estimate of 36,000 U.S. deaths a year now when there were just 34,000 deaths recorded in the 1968-1969 "Hong Kong flu" epidemic? he asked. 
  Doshi's article talks of a "public relations approach" linked to drug company profits. "CDC is already working in the manufacturers' interest by conducting campaigns to increase vaccinations," he wrote. 


