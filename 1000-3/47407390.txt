


-----BEGIN PGP SIGNED MESSAGE----- Hash: SHA1 
Weekend News Summary from RHC - Jan 7, 2006 
Via NY Transfer News Collective  *  All the News that Doesn't Fit   Weekend News Summary from Radio Havana Cuba - January 7, 2006 
* Cuba and Caribbean Countries Strengthen Cooperation * High-Ranking Russian Military Official Praises Cuba Visit * Cuba's Vaccination Program Targets 13 Preventable Diseases * Cuba Expands Medical Rehabilitation Services 
* New Study Reveals Multi-Billion Dollar Cost of Iraq Occupation * United Nations Warns that Millions Could Starve in Africa * Preparations Underway in Venezuela for Upcoming World Social Forum * German Chancellor Says Guantánamo Prison Camp Should be Closed * UN Issues Ultimatum for Haitian Elections 

Cuba and Caribbean Countries Strengthen Cooperation 
Havana, January 7 (RHC) - Cuba's Deputy Foreign Minister Rafael Daussa described the recent visit to Cuba by the General Secretary of the Association of Caribbean States (ACS), Ruben Silie, as very positive, noting it will help strengthen regional cooperation. 
Daussa told Havana-based Prensa Latina news agency that, during his recent visit, Silie received updated information on Cuba's development and was able to once again confirm Havana's unconditional support of the ACS. Cuba's deputy foreign minister added that the visit of the ACS official also helped identify new fields in which Cuba can cooperate with the Caribbean, such as in the energy sector, experience in natural disasters, and science and technology. 
Shortly before leaving Cuba, Silie affirmed Cuba-ACS relations will increase, and the Island will completely support the organization he heads, mainly in the fields of transportation and tourism, which have been given top priority. 
As he concluded his visit to Cuba this week, the head of the Association of Caribbean States said: "I regard my visit as very positive. I am very impressed and pleased." 

High-Ranking Russian Military Official Praises Cuba Visit 
Havana, January 7 (RHC) - The head of the Russian Armed Forces General Staff, General Yuri Nicolaevich Baluevski, has described his recent visit to Cuba as excellent. Upon his arrival in Moscow on Saturday, Baluevski, who is also the Russian First Deputy Defense Minister, told reporters that as a result of his visit to the island, relations between the Russian and Cuban armies were strengthened. 
"It was my honor to speak with the head of the Cuban Revolutionary Armed Forces, Army General Raúl Castro Ruz, about several topics of interest for our defense ministries. We also held working-meetings with Cuban Army Corps General Alvaro Lopez Miera," he noted. 
After touring several military bases, the high-ranking Russian military official praised the technical preparation of Cuba's military personnel. During his visit to Cuba, Baluiyevski and his delegation also paid tribute to Antonio Maceo, hero of the Cuban Independence War and toured historical and cultural sites in Havana. 

Cuba's Vaccination Program Targets 13 Preventable Diseases 
Havana, January 7 (RHC) - A recent report from Cuba's Public Health Ministry on the progress of the National Immunization Program in 2005 reports a 25 percent decline in cases of meningitis and zero cases of tetanus. 
The comprehensive plan includes immunization against 13 preventable diseases by administering ten vaccines - seven of them developed by Cuban scientists. 
Miguel Angel Galindo, head of the program, said there has been an important decline in the number of victims of Hemophilic Influenza type B (Hib) that causes meningitis, otitis and pneumonia. 
Galindo said that in 2005, there was not a single case of diphtheria, whooping cough, measles, German measles and TB meningitis in children under one year of age. The head of the National Immunization Program pointed out that another achievement of Cuban biotechnology is the development of a vaccine able to protect children from hepatitis B, tetanus, diphtheria and whooping cough. 
Cuba is working to improve its program to fight and reduce any possible epidemic should bird flu mutate to human-to-human infection and the 20 year old National Zoonosis Program (for human diseases from vertebrae animals) is setting up a detailed bird watch. 
The country also consolidated its International Health Control Program for global emergence of new diseases and recurrence of old ones with new regulations. 

Cuba Expands Medical Rehabilitation Services 
Santiago de Cuba, January 7 (RHC) - The eastern Cuban province of Santiago de Cuba has seen a significant increase of physiotherapy and rehabilitation services, particularly in isolated mountainous towns and communities. 
The region now has 18 physiotherapy and rehabilitation hospital wards in full operation, with an additional 308 such facilities in rural areas being fitted with solar energy panels. The treatment offered at these centers is aimed at patients suffering from orthopedic and cerebral-vascular diseases. 
The medical initiative allowed the setting up of 452 specialized hospital wards throughout the island in 2005. Santiago de Cuba is now improving upon this important medical service, where more than 149,000 patients are treated annually in its 18 facilities. 
Medical specialists have been providing at-home assistance to patients since last year, while nearly 1,000 professionals have been trained in rehabilitation techniques to work in the new facilities. There, they will also employ alternative or natural medicine methods, massage, nutrition-oriented techniques, and others. 
As part of the first stage of the program in 2006, a range of services will be provided at the rehabilitation wards, including electro-therapy and infra-red radiation. 

New Study Reveals Multi-Billion Dollar Cost of Iraq Occupation 
Boston, January 7 (RHC)-- A new study by two leading academic experts suggests that the costs of the invasion and occupation of Iraq will be substantially higher than previously estimated. In a paper presented to this week's Allied Social Sciences Association annual meeting in Boston, Massachusetts, Harvard budget expert Linda Bilmes and Columbia University professor and Nobel Laureate Joseph E. Stiglitz calculate that the war is likely to cost the United States a minimum of nearly one trillion dollars and potentially over two trillion. 
The study expands on traditional budgetary estimates by including costs such as lifetime disability and health care for the over 16,000 injured -- at least one-fifth of whom have serious brain or spinal injuries. It then goes on to analyze the costs to the economy, including the economic value of lives lost and the impact of factors such as higher oil prices that can be partly attributed to the conflict in Iraq. The paper also calculates the impact on the economy if a proportion of the money spent on the Iraq occupation were spent in other ways, including on investments in the United States 
Joseph Stiglitz told reporters in Boston that shortly before the invasion of Iraq, when administration economist Larry Lindsey suggested that the costs might range between $100 and $200 billion, White House spokesmen "quickly distanced themselves from those numbers." The 2001 Nobel prizewinner in Economics said that "in retrospect, it appears that Lindsey's numbers represented a gross underestimate of the actual costs." 

United Nations Warns that Millions Could Starve in Africa 
Rome, January 7 (RHC)-- Millions of people are on the brink of starvation in the Horn of Africa due to severe droughts and conflicts, according to the United Nations Food and Agriculture Organization (FAO). The FAO "special alert" -- issued in Rome on Friday -- reports that more than 11 million people in Somalia, Kenya, Djibouti and Ethiopia are estimated to be in need of assistance. 
Earlier this week, the World Food Program (WFP), a UN agency responsible for distributing food aid, issued a similar alert, saying the situation in Kenya, Somalia and Ethiopia was critical. The FAO said food shortages were "particularly grave" in Somalia, where about two million people needed humanitarian aid. 
Severe drought had produced widespread crop failures across the south and the new crop about to be harvested "could be the lowest in a decade." The Food and Agriculture Organization urged international donors to respond immediately to the WFP's appeal for 64,000 tons of food aid to avert hunger-related deaths in southern Somalia -- noting that only 16,700 tons are now available. 
The FAO said the situation was also "very serious" in pastoral areas of Ethiopia, Djibouti and Kenya, where people were already dying from famine. In northern and eastern Kenya, prolonged drought was killing crops and the livestock essential to the survival of the largely rural population. 
The Kenyan government had requested about 150 million dollars to provide food for 2.5 million people -- almost 10 percent of the population -- over the next six months, according to the FAO report. But more international aid was needed to provide people and animals with water, restock herds and supply seeds to farmers. 
At least three foreign relief organizations -- the International Federation of Red Cross and Red Crescent Societies (IFRC), Action Against Hunger (AAH) and World Vision -- said Thursday immediate emergency assistance was needed. They predicted the death rate among livestock could soar from 30 to 70 percent. 
The Red Cross has reported deaths from malnutrition in northern Kenya since early December, calling the situation "disastrous." Kenyan President Mwai Kibaki has declared a "national disaster" and ordered the military to help distribute food and water. 
The FAO said a fifth of the population of Djibouti, or about 150,000 people, were facing food shortages as a result of severe drought. And over 40 million dollars were "urgently required to stave off starvation" in the rural areas of eastern and southern Ethiopia. 

Preparations Underway in Venezuela for Upcoming World Social Forum 
Caracas, January 7 (RHC)-- The building of socialism in Latin America is one of the main topics to be addressed at the 6th World Social Forum to be held in Caracas, Venezuela from January 24th through the 29th. 
Jacobo Torres, a member of the organizing committee, told Prensa Latina News Agency that following recent electoral events in the region, the topic of building socialism is gaining ground among the expressed interests of the participating organizations. Torres said a key element is to define the participation of popular movements in this new, ongoing political process. 
With the winds of change blowing through Latin America, nearly 100,000 are expected in Caracas later this month for the 6th World Social Forum, which will feature numerous cultural presentations and workshops. 
The member of the organizing committee for the upcoming World Social Forum said that other topics on the agenda include increasing militarization and a comparison between the Bolivarian Alternative for the Americas (ALBA) and the U.S.-sponsored Free Trade Area of the Americas (FTAA). 

German Chancellor Says Guantánamo Prison Camp Should be Closed 
Berlin, January 7 (RHC)-- German Chancellor Angela Merkel, in an interview published just days before her first visit to the United States, says Washington should close its Guantánamo Bay prison camp and find other ways of dealing with terror suspects. 
In an interview published on Saturday in the weekly magazine Der Spiegel, Merkel said that "an institution like Guantánamo can and should not exist in the longer term." She noted that different ways and means must be found for dealing with these prisoners." 
Merkel -- who is not known for anti-U.S. ideas -- says she will repair Germany's ties with Washington, which were strained over the U.S.-led invasion of Iraq. Her predecessor, Gerhard Schroeder, strongly Washington's invasion and occupation of the Arab country. 
According to media reports from Berlin, there is widespread skepticism in Germany about the way the United States is fighting its so-called "war on terror" -- compounded by the recent scandal over the CIA's abduction and detention of German citizen Khaled el-Masri. 
Guantánamo Bay, the U.S. detention center located in Cuba and illegally occupied by the United States, has been denounced by human rights activists and is deeply unpopular in Germany. 
Angela Merkel travels to Washington next week for her first visit since becoming chancellor in November, and will meet U.S. President George W. Bush on Friday. Merkel told Der Spiegel she expected to speak to Bush about the fight against terrorism. But she added that Germany's relationship with the U.S. will not be reduced to talking about fighting terrorism and the Iraq war." 

UN Issues Ultimatum for Haitian Elections 
Port au Prince, January 7 (RHC)-- Provisional authorities in Haiti will have one month starting Saturday to convene new presidential and legislative elections, which were to have been held Sunday. 
A declaration by the UN Security Council over the issue was regarded in Port au Prince as an ultimatum for the acting executive. The statement says that "the UN Council encourages the temporary Haitian government and Provisional Electoral Council to promptly announce new election dates, with the first round to be held February 7th at the latest." 
With this statement by the UN Security Council, the United Nations has joined the pressure by different sectors on the interim Haitian government, which has now suspended the elections four times due to what they call "logistical problems." 
-----BEGIN PGP SIGNATURE----- Version: GnuPG v1.4.2 (GNU/Linux) 
iD8DBQFDwZIjgVqEKMbi+yQRAoNZAJ91XfG43cu72WmrhEGpcBwAblAtUgCfWkb3 wzC2FEjviILsLgGtG6hCzOM= =VPLs -----END PGP SIGNATURE----- 


