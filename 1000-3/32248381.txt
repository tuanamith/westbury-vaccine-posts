


Pray for the World: 6 December 2005 Update From HCJB World Radio 

Today's Headlines: 
MILITANT HINDU GROUPS IN INDIA CONTINUE ATTACKS AGAINST CHRISTIANS 
MEDICAL TEAM TO PROVIDE VACCINES FOR INDIA'S ORPHANS 
INTERNATIONAL CALLERS JOIN IN PRAYER FOR PERSECUTED CHURCH 
Today's Top Stories: 
MILITANT HINDU GROUPS IN INDIA CONTINUE ATTACKS AGAINST CHRISTIANS 
Two militant Hindu groups struck churches in central India's Chattisgarh and Madhya Pradesh states on Sunday, Dec. 4. At least 25 members of the Hindu extremist group Dharma Sena attacked a church in Raipur, Chattisgarh, severely beating five Christians. After beating four Christians in the church, the attackers took them and a pastor from another area church into a Hindu temple where they tried to force them to bow down to idols. Also on Sunday, a group of 15 extremists from the Rashtriya Swayamsevak Sangh attacked a pastor in the Jhabua district of Madhya Pradesh state. Police declined to arrest any of the militant Hindus. Instead, they detained Pastor Anil Mehra of the Indian Evangelical Team for more than 10 hours for "disrupting public peace." (Compass) 
MEDICAL TEAM TO PROVIDE VACCINES FOR INDIA'S ORPHANS 
Hopegivers International is sponsoring a special medical team to travel to India to vaccinate orphans against the deadly virus Hepatitis A. Led by retired registered nurse Bobbye Golden, five volunteers will leave for India in January. "In everything we do, we want to reflect the love of Christ," said Hopegivers President Dr. Samuel Thomas. "I'm thankful for people like Bobbye Golden who make it their business to reflect Christ through helping others." The India Vaccination Campaign first began when Golden went to India in 1997 with Hopegivers to provide typhoid vaccine for hundreds of orphans at the Raipura orphanage in Kota, Rajasthan. Along with her team of volunteers, Golden has been back to India every year since then to provide life saving shots for abandoned and orphaned children. Along with the vaccines, the children are taught proper hygiene techniques and are given toothbrushes, vitamins and antibiotics. (Assist News Service) 
INTERNATIONAL CALLERS JOIN IN PRAYER FOR PERSECUTED CHURCH 
International callers from 15 countries and 22 states in the U.S. joined a 12-hour conference call to unite in prayer for the International Day of Prayer for the Persecuted Church on Sunday, Nov. 13. Greater Calling, a nonprofit Christian teleconferencing prayer ministry, announced that several callers from countries where persecution of Christians is severe were able to use their telephones to unite with Christians in the U.S. for prayer. "We were so excited to hear the voice of callers that actually live in the persecuted church regions," said Dana Simons, co-founder of Greater Calling Ministry. "Even though they could not stay on the line for more than a few minutes, through the power of the Internet, they were able to find out about the call and get the number to join us in order to remind us about their plight. We had callers from Myanmar, Somalia and Algeria, all of whom were literally putting their lives at risk to make the phone call," she added. (Assist News Service) 
================= --  *Peace of Christ*  <URL>  To send e-mail, remove "youhat" from address 

