


Former science chief: 'MMR fears coming true' By SUE CORRIGAN, Mail on Sunday 
A former Government medical officer responsible for deciding whether  medicines are safe has accused the Government of "utterly inexplicable  complacency" over the MMR triple vaccine for children. 
Dr Peter Fletcher, who was Chief Scientific Officer at the Department of  Health, said if it is proven that the jab causes autism, "the refusal by  governments to evaluate the risks properly will make this one of the  greatest scandals in medical history". 
He added that after agreeing to be an expert witness on drug-safety trials  for parents' lawyers, he had received and studied thousands of documents  relating to the case which he believed the public had a right to see. 
He said he has seen a "steady accumulation of evidence" from scientists  worldwide that the measles, mumps and rubella jab is causing brain damage in  certain children. 
But he added: "There are very powerful people in positions of great  authority in Britain and elsewhere who have staked their reputations and  careers on the safety of MMR and they are willing to do almost anything to  protect themselves." 
His warning follows reports that the Government is this week planning to  announce the addition of a jab against pneumococcal meningitis for babies,  probably from next April. It is also considering flu jabs for under-twos -  not to protect the children, but adults they may infect. 
In the late Seventies, Dr Fletcher served as Chief Scientific Officer at the  DoH and Medical Assessor to the Committee on Safety of Medicines, meaning he  was responsible for deciding if new vaccines were safe. 
He first expressed concerns about MMR in 2001, saying safety trials before  the vaccine's introduction in Britain were inadequate. 
Now he says the theoretical fears he raised appear to be becoming reality. 
He said the rising tide of autism cases and growing scientific understanding  of autism-related bowel disease have convinced him the MMR vaccine may be to  blame. 
"Clinical and scientific data is steadily accumulating that the live measles  virus in MMR can cause brain, gut and immune system damage in a subset of  vulnerable children," he said. "There's no one conclusive piece of  scientific evidence, no 'smoking gun', because there very rarely is when  adverse drug reactions are first suspected. When vaccine damage in very  young children is involved, it is harder to prove the links. 
"But it is the steady accumulation of evidence, from a number of respected  universities, teaching hospitals and laboratories around the world, that  matters here. There's far too much to ignore. Yet government health  authorities are, it seems, more than happy to do so." 
'Why isn't the Government taking this massive public health problem more  seriously?' 
Dr Fletcher said he found "this official complacency utterly inexplicable"  in the light of an explosive worldwide increase in regressive autism and  inflammatory bowel disease in children, which was first linked to the live  measles virus in the MMR jab by clinical researcher Dr Andrew Wakefield in  1998. 
"When scientists first raised fears of a possible link between mad cow  disease and an apparently new, variant form of CJD they had detected in just  20 or 30 patients, everybody panicked and millions of cows were  slaughtered," said Dr Fletcher. 
"Yet there has been a tenfold increase in autism and related forms of brain  damage over the past 15 years, roughly coinciding with MMR's introduction,  and an extremely worrying increase in childhood inflammatory bowel diseases  and immune disorders such as diabetes, and no one in authority will even  admit it's happening, let alone try to investigate the causes." 
He said there was "no way" the tenfold leap in autistic children could be  the result of better recognition and definitional changes, as claimed by  health authorities. 
"It is highly likely that at least part of this increase is a vaccinerelated  problem." he said. "But whatever it is, why isn't the Government taking this  massive public health problem more seriously?" 
His outspokenness will infuriate health authorities, who have spent millions  of pounds shoring up confidence in MMR since Dr Wakefield's 1998 statement. 
But Dr Fletcher said the Government is undermining public confidence in  vaccine safety by refusing to do in-depth clinical research to rule out  fears of MMR damage to children. 
He added that the risks of brain and gut damage from MMR injections seem to  be much higher in children where a brother or sister has diabetes, an immune  disorder. 
"That is a very strong clinical signal that some children are  immunologically at risk from MMR," he said. "Why is the Government not  investigating it further - diverting some of the millions of pounds spent on  advertising and PR campaigns to promote MMR uptake into detailed clinical  research instead?" 
Now retired after a distinguished 40-year career in science and medicine in  Britain, Europe and the US, Dr Fletcher said that without such research,  health authorities could not possibly rule out fears about MMR. 
He said: "It is entirely possible that the immune systems of a small  minority simply cannot cope with the challenge of the three live viruses in  the MMR jab, and the ever-increasing vaccine load in general." 
He said he had decided to speak out because of his deep concern at the lack  of treatment for autistic children with bowel disease, as revealed in The  Mail on Sunday two weeks ago. 
He called the sudden termination of legal aid to parents of allegedly  vaccine-damaged children in late 2003 "a monstrous injustice". After  agreeing to be a witness for the parents, he received thousands of documents  relating to the case. 
"Now, it seems, unless the parents force the Government to restore legal  aid, much of this revealing evidence may never come out," he said. 
The Department of Health said: "MMR remains the best protection against  measles, mumps and rubella. It is recognised by the World Health  Organisation as having an outstanding safety record and there is a wealth of  evidence showing children who receive the MMR vaccine are no more at risk of  autism than those who don't." 
Find this story at   <URL>  ©2006 Associated New Media  



