


(Yet another panic-mongering "We're all gonna die tomorrow" story. Like the rest of the terrifying bird flu stories that have found their way into the mainstream media, there is no mention of Donald Rumsfeld owning about a zillion shares of the one company that makes bird flu vaccine. The more fear they can spread, the more Rummy stands to make. Those dumb hacks at Reuters get more like Fox every day. Rumsfeld probably has people at Reuters on the Armstrong Williams Rewards Program.) 

Preparing for pandemic: know how to bury your dead Wednesday February 15, 09:50 PM 


MINNEAPOLIS (Reuters) - When burying a body in the backyard, don't put it too close to the septic system. That was one piece of advice offered on Wednesday to a business conference on preparing for a potentially lethal bird flu pandemic. Preparations for a global flu pandemic, which many experts believe is overdue, have begun but the grisly details are horrific and the number of sick could quickly overwhelm the health care system. Needed supplies of even common medical supplies such as surgical masks and gloves are in doubt, not to mention the syringes needed for an as-yet undeveloped vaccine and costly mechanical ventilators. 
The H5N1 avian flu virus that has infected flocks on at least three continents and killed 91 people could be the virus that experts fear will mutate into a highly pathogenic form that kills hundreds of millions of people in a matter of weeks or months. 
In Seattle, public health officials are weighing the ramifications of hospitals overwhelmed by hundreds of thousands of sick people and the need for thousands of body bags. 
"We talk about how people should bury their dead in their backyards, how far from the septic systems," said Dorothy Teeter, director of the King County public health department in Seattle. "In case you're wondering, it's $20 (11.50 pounds) apiece for high-quality body bags. In New Orleans (after Hurricane Katrina) they had to double-bag bodies." 
Refrigerated trucks will be needed to ship and store food and medicines and will not be available for corpses, a mistake made by federal authorities who commandeered trucks after Katrina, said James Caverly of the U.S. Department of Homeland Security. 
Communicating the truth will be important to deter civil unrest, several experts told the conference. Up until six months ago, the Department of Health and Human Services was planning privately for a pandemic but saying little publicly, said communications consultant Peter Sandman. 
The shift may be due to President George W. Bush reading John Barry's "The Great Influenza," an account of the 1918 influenza pandemic during which government assured the public that it was just another seasonal flu outbreak, Sandman said. At the time, Barry said many communities were brought to a near standstill, with people afraid to talk to each other or care for the sick. 
"When you mislead people, when you over-reassure people ... they feel less trusting, and they behave much worse," Sandman said. 
Conference participants from corporations, interest groups, and government met to discuss revising just-in-time supply concepts in order to stockpile, shutting down and restarting facilities in pandemic-hit countries, and cross-training employees and bringing back retirees to replace a significant portion of their employees that might become sick, decide not to come to work, or die. 
If companies stockpile masks and antiviral drugs such as Tamiflu, conference participants wondered if tax breaks would be enacted to defray the cost of preparing. 
The dilemma for producers of syringes and the like is that increasing factory capacity for a one-time sale for a pandemic is bad business practice. 
Meanwhile, government will have to address public scepticism about its ability to prepare for a pandemic, the official in charge of emergency preparedness at the Department of Health and Human Services said. 


