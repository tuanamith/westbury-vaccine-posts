



 <URL>  Sheet=/news/2006/02/12/ixhome.html 

Secret report reveals 18 child deaths following vaccinations Beezy Marsh, Health Correspondent (Filed: 12/02/2006) 
Eighteen babies and toddlers have died following childhood vaccinations in just four years, a secret Government report reveals. 
Four deaths have been linked to suspected adverse reactions to the measles, mumps and rubella (MMR) triple jab, according to documents prepared for the Government's expert advisers on immunisation. 
The controversial jab has been beset by fears of a link to autism and bowel disease, although since its introduction in 1998, yearly deaths from measles have fallen from 16 to zero, while the jab against meningitis C is thought to save 50 lives a year. 
The report, covering the period between 2001 and 2004, details how one baby suffered a cot death following MMR vaccination in 2003. Two more infants were reported to have died after having the MMR jab in 2001, but the cause of death in both cases was unknown. 
After the death of a child who developed meningitis and swelling of the brain three weeks after an MMR jab in 2004, a claim for compensation was made by the child's parents. It is not known if this was successful. 
Six fatalities followed meningitis C vaccinations between 2001 and 2003. The deaths of seven other babies were linked to combined vaccines against diphtheria, tetanus and whooping cough and reported to the Medicines and Healthcare products Regulatory Agency (MHRA). They include a baby who died from a heart attack. Another died after a polio jab. 
Almost 800 other reports of suspected complications of childhood vaccination - including convulsions and hyptonia, in which the baby becomes floppy like a "rag doll" - were also made, including 160 for MMR. 
Medics raised the alarm under the MHRA "yellow card" warning system, set up to monitor suspected adverse drug reactions. Although making such a report does not prove that vaccination caused death or injury, it means that doctors fear it may have played a part. 
Their reports were considered by the Joint Committee on Vaccination and Immunisation, which concluded that no significant safety issues were identified. 
Details of the document, which emerged after a request under the Freedom of Information Act, come amid fears of "vaccine overload" due to a rise in the number of jabs given to infants. 
The Department of Health last week announced the introduction of a new jab against the pneumococcal bug, which causes a deadly strain of meningitis, in addition to a booster for Hib disease, which can cause meningitis. This means that by the time a child is two he will have had 25 vaccinations, although some will be given in five-in-one or three-in-one combinations. 
Dr Richard Halvorsen, a general practitioner who runs a private single jabs clinic at Holborn Medical Centre in London, said: "We know vaccines have potential side effects, but this does not mean that children died as a result of MMR or other jabs. However, it is not insignificant because these are reports from health professionals who suspect an adverse vaccination reaction." 
Experts last night said that the true figures for suspected fatalities and serious side effects could be much greater. Dr John Griffin, the former editor of the medical journal Adverse Drug Reactions, said: "For fatalities, it is probably only one in two which gets reported and for other side effects one in 10." 
This means that almost 40 baby deaths could have occurred following jabs between 2001 and 2004, and 8,000 serious adverse reactions. 
Prof Peter Openshaw, a leading immunologist from Imperial College London, said parents should not be alarmed by the report's findings. 
"A lot of vaccine reactions are just inexplicable," he said. "It may be that someone had an infection before they got a jab, it may be something in their genetic make-up or sometimes there are allergic reactions. But vaccines are extraordinarily safe compared to the diseases they prevent." 
Overall, thousands of lives have been saved by childhood immunisation. Smallpox and polio have been eradicated. 
A spokesman from the Department of Health said: "Immunisation programmes are regularly reviewed to ensure that all children have the best possible protection." 
TO VACCINATE IS YOURS AND YOURS ALONE. ****** "Just look at us. Everything is backwards; everything is upside down. Doctors destroy health, lawyers destroy justice, universities destroy knowledge, governments destroy freedom, the major media destroy information and religions destroy spirituality" ....Michael Ellner 



--  No virus found in this incoming message. Checked by AVG Free Edition. Version: 7.1.375 / Virus Database: 267.15.6/257 - Release Date: 10/02/2006 




