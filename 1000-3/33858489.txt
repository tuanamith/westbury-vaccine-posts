


Republicans want 'secret' drug agency Testing in private could speed development of super-flu, bio-warfare treatments 
) 2005 WorldNetDaily.com  
Some Republican lawmakers believe Americans would benefit from the creation of a secretive federal agency responsible for development and testing of medications designed to combat mega-flu outbreaks as well as chemical, biological and radiological terror attacks. 
"We must ensure the federal government acts as a partner with the private sector, providing the incentives and protections necessary to bring more and better drugs and vaccines to market faster," Sen. Richard Burr, R-N.C., said as the Committee on Health, Education, Labor and Pensions approved legislation this week that would create the Biomedical Advanced Research and Development Agency, or BARDA. 
The new agency would be protected from traditional public scrutiny such as open-meetings laws and other provisions that allow inspection of an agency's records. 
The reduced scrutiny, it is believed, is necessary both for the more rapid development of treatments and to protect drug companies from liability. Also, supporters say the legislation would enhance national security and a drug maker's proprietary formula. 
Critics, however, worry about provisions in the bill that would exempt the new agency from Freedom of Information and Federal Advisory Committee acts. 
"There is no other agency that I am aware of where the agency is totally exempt either from FOIA or FACA," said Pete Weitzel, coordinator of the Coalition of Journalists for Open Government. 
The coalition is part of a journalistic alliance seeking to amend provisions of the legislation that exempt the new agency from scrutiny. 
"That is a cause for major concern and should raise major policy concerns," Weitzel said. 
Barbara Loe Fisher, president of the National Vaccine Information Center, said the proposed legislation was dangerous. Without scrutiny and the right to sue negligent companies, "these drugs are going to be developed in secret, and if there are deaths and injuries, they're going to be covered up," she told the San Jose Mercury News. 
Robert Kadlec, staff director for Burr's committee, said such fears weren't justified. 
"We're not trying to create a super-secret agency," he told the Mercury News. 
Kadlec conceded parts of the legislation may have to be reworded so that it was clear what information could and could not be kept secret. 
Some Democrats oppose the planned agency. 
"[The GOP] plan will protect companies that make ineffective or harmful medicines, and because it does not include compensation for those injured by a vaccine or drug, it will discourage first responders and patients from taking medicines to counter a biological attack or disease outbreak," said Sen. Chris Dodd, D-Conn. 
Reports said although the bill does provide for limited compensation, drug companies would have had to demonstrate "willful misconduct" before their immunity is withdrawn. 
Others also criticized the secrecy provisions. 
"These provisions are extremely dangerous," Dr. Sidney Wolfe, director of Public Citizen's Health Research Group, told the Associated Press. "The fact that they are being proposed, really exploiting people's fears about pandemics and epidemics, is outrageous and goes backward on the progress on the use of the Freedom of Information Act and Federal Advisory Committee Act to increase public scrutiny and increase the correctness of decisions that are made." 
Amy Call, a spokeswoman for Senate Majority Leader Bill Frist, R-Tenn., said, however, that a level of protection for the drug makers involved in such work was an important protective element. 
"There's really no financial incentive for them to get into the market, sell to the government at a reduced rate and then open themselves up to losses that could potentially bankrupt them," she said. 
And Gerald Epstein, a senior fellow for science and security at the Center for Strategic and International Studies, a non-profit Washington think tank, said the privacy provisions are "not as scary as people make it out to be." He said the alternative would be to set up cumbersome access regulations, the Mercury News reported. 
The paper said Burr, Frist, Sen. Elizabeth Dole, R-N.C., and three other senators introduced the legislation. 
 <URL>  

