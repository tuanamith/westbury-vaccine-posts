


Roger Johansson wrote: 
You are naive in the extreme.  Perhaps you are fooled by your relatively  homogeneous society into thinking that communism (which is essentially  what you describe) is a desirable form of government. 
Conditions of inequality arise naturally when some people work harder  than others.  People work harder than others to gain an advantage, and  if that advantage is not there, they will not work harder.  They may not  work at all.  People who work hard will expect reward, and will resent  the expectation by lazy people that they deserve their "fair share"  without working.  They will, rightfully and with vigor, defend with  force the fruits of their labor.  They may be fighting for the welfare  of themselves, of their own family, or for others they love, but they  will fight.  They will make moral judgements:  They will love whatever  they regard as good, and protect it from those they regard as bad, and  if forced to, they will fight to the death. 
This is human nature, and if you disregard the essential nature of man,  you cannot be a humanist. 
Let me make a brash statement:  The free market system has NO problems.    All that you see as problems are in fact problems with details of law  or problems intrinsic in human nature, not problems with capitalism.  If  you see good people starving and dying, that is in fact a consequence of  man's indomitable will to procreate.  His expansive nature will  ultimately overturn ANY attempt at control or form of government, so  people will starve uner ANY system.  Under true capitalism, the  survivors will at least roughly correlate to the more intelligent and  the harder working.  Under communism, the survivors are those who  brandish the party doctrine, who are part of the enforcement arm of  government, or whoever ultimately wields physical force. 
Mankind did not evolve in the kind of society that you promote, and he  is not adapted to it.  To think that there is ANY governmental form that  will totally banish injustice is absurd.  If the best we can do is  minimize it, then the existence of injustice is not proof of a flaw in  the system. 
Capitalism IS a moral system.  It is the ONLY moral system.  People are  free:  To apply their OWN morality by allocating resources they have  saved through free trade.  You cannot accuse capitalism by pointing to  modern societies, because there are no purely capitalistic societies. 
Governments that attempt to allocate resources are inherently amoral:  They cannot make good moral judgements because 1) they cannot ascertain  the truth of an individual's condition, and 2) they cannot efficiently  allocate resources based on economic need and individual needs.  Government, to the extent that it attempts to be "fair", acts in a  ham-handed way, enforcing a lowest-common-denominator "morality" that is  blind to individual differences, and individual goodness. 
You naively say abolish money, but money is an indispensable tool.  As  well as vastly promoting economic efficiency, it provides a common  measurement tool for judging the worth of EVERYTHING.  Is extending the  life of an old woman one year worth $1,000,000?  Perhaps not, if that  $1,000,000 can buy vaccines to immunize 100,000 children, many of whom  would die without vaccine.  I have now put a dollar price on a human  life.  Should I sacrifice the education of my children to save the life  of the impudent jerk next door?  No, not in a million years.  Since the  cost of saving his life can be measured in dollars, and the cost of my  children's education can be measured in dollars, money can help me apply  resources consistent with my moral judgments. 
Resources are limited.  Mankind will find some way of exhausting his  resources by breaking the bonds that have previously constrained him.  His choices will then be of a kind that bring life to one and death to  another.  To the extent that capitalism enforces restraint on the lives  of some now, it cannot be judged bad.  It is better to have attrition  and restraint based on an individual's exercise of his moral sense, than  the cataclysmic destruction of good AND bad that inevitably follows from  naive attempts, such as yours, to avoid any measure of pain now. 

--  Philosophy is a stage in intellectual development, and is not compatible  with mental maturity. -- Bertrand Russell 







