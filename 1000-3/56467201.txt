


Thanks for the input everyone. I guess the consenus is "let nature take her course or get a different cat." We enjoy both cats -- individually, they are very nice, friendly cats -- and don't necessarily want more (unless we come across more strays), so I guess we'll just try to ignore the goings on,  keep them separated as much as possible, and if there is a confrontation give a squirt of water or let things take their course as long as it doesn't turn into clawing and biting.  
As far as the replies about me being irresponisble by not having cats fixed, vaccinated, etc. -- EVERY cat we have had to date, even ephemerally, has been fixed and vaccinated at the local humane society. The only cat we lost was one who either had FeLV before we took him in or had some adverse reaction to the vaccines - about 2 weeks after he got vaccinated he got extremely weak and had to be put down. It was sad but probably preventable, as we got him from a person who was feeding him and pseudo-taking-care of him but never got him vaccinated. 
It's been fun helping out the feral population of the city, but animal behavior can be somewhat frustrating at times... 
On Thu, 26 Jan 2006 00:21:19 -0800, Desert Tripper < <EMAILADDRESS> > wrote: 

-------------------------------------------------------------------------------------- Doug the Desert Tripper - Exploring Southern Cal deserts and the Net since '94                        www.geocities.com/destrip                          destrip at sonic dot net -------------------------------------------------------------------------------------- . 

