


 <URL>  
by Evelyn Pringle 
 <URL>  
Pharma's Poisoned Generation 
A growing number of professionals in the health care field are reporting that a  relationship exists between the epidemic in neurodevelopmental disorders of  autism, attention deficit hyperactive disorder, and speech or language delay all  across the country, and the use of thimerosal, the mercury-based preservative  used in childhood vaccines. 
Vaccines are the only medicines that Americans are mandated to receive as a  condition for attendance in school and day care, and for some types of  employment. Parents who receive federal assistance are also required to show  proof that their children have been vaccinated. 
While the mandate for which vaccines must be included on the vaccine schedule is  a state mandate, it is the Centers for Disease Control and Prevention (CDC) and  its Advisory Committee that make the recommendations to which the majority of  states adhere when determining mandates. The current epidemic actually began in  the late 1980s when a large number of new vaccines were added to the schedule. 
The blame is at least partially attributable to the failure of government  officials to keep track of the cumulative amounts of mercury as they added  triple-dose-vaccines to the schedule and the amount of thimerosal was multiplied  by three. 
Each new vaccine contained 25 micrograms of mercury and according to Professor  Lynn Adams, of Radford University, who specializes in autism, by 1999, a study  determined that the average child received 33 doses of 10 different vaccines by  the age 5. 
Elected lawmakers first became aware of the problem in 1999, when the House  Committee on Government Reform initiated an investigation into the dangers of  mercury exposure. An alarm rang early about the exposure of children to  thimerosal. 
By October 25, 2000, Committee Chairman, Dan Burton (R-IN), was trying to get  the substance out of vaccines as quickly as possible and sent a letter to the  Department of Health and Human Services, asking the director to get the FDA to  recall all vaccines with thimerosal. 
We all know and accept that mercury is a neurotoxin, and yet the FDA has failed  to recall the 50 vaccines that contain Thimerosal, Burton wrote. Every day  that mercury-containing vaccines remain on the market is another day HHS is  putting 8,000 children at risk, he said. 
I implore you to conduct a full recall of these products, he wrote. If the  only action ... is a gradual phase out, children will continue to be put at risk  every day, Burton warned. These vaccines will continue to be injected in  children for years to come - putting our nation's most vulnerable population ...  at risk for mercury poisoning," he added. 
The Reform Committee soon discovered that regulatory agencies were still  allowing thimerosal to be used decades after the recognition that it was  harmful. The Committee was told that the Food and Drug Administration, (FDA),  uses a subjective barometer in determining when a product that has known risks  can remain on the market. According to the agency: 
"at the heart of all FDAs product evaluation decisions is a judgment about  whether a new products benefits to users will outweigh its risks. No regulated  product is totally risk-free, so these judgments are important. The FDA will  allow a product to present more of a risk when its potential benefit is  greatespecially for products used to treat serious, life-threatening  conditions." 
The argument that the known risks of infectious diseases outweighs any potential  risk of neurological damage is one that has continuously been used by officials.  The FDA claims that any risk from thimerosal is theoretical because no proof of  harm exists. 
However, after its review of scientific literature and listening to the  testimony of witnesses, the Committee found plenty of evidence to support the  fact that thimerosal posed a grave risk. "The possible risk for harm from either  low dose chronic or one time high level (bolus dose) exposure to thimerosal is  not "theoretical," but very real and documented in the medical literature," the  Committee said. 
The Committee also discovered that regulatory agencies have never required drug  companies to conduct studies on the use of thimerosal. During a June 20, 2002,  hearing, Burton questioned officials from the FDA and CDC and said, "You mean to  tell me that since 1929, we've been using Thimerosal and the only test that you  know of is from 1929, and every one of those people had mennigitis, and they all  died?" 
In his opening statement at a July 18, 2000, hearing, Burton said: 
"We assume that the FDA would protect our children from exposure to any level of  mercury through drugs. But that hasnt been the case. Thimerosal was first  marketed in 1930 and has become the most widely used preservative in vaccines.  It is present in over 50 licensed vaccines." 
"The FDA recently acknowledged that in the first six months of life, children  get more mercury than is considered safe by the EPA," Burton noted. "The truth  is that sometimes kids go to their doctors office and get four or five vaccines  at the same time," he added. 
"My grandson received vaccines for nine different diseases in one day," Burton  said. "He may have been exposed to 62.5 micrograms of mercury in one day through  his vaccines." 
"According to his weight, the maximum safe level of mercury he should be exposed  to in one day is 1.51 micrograms," Burton advised. "This is forty-one times the  amount at which harm can be caused," he added. 
In his opening remarks at a June 19, 2002, hearing, Burton described the  devastation of witnessing the correlation between vaccines and autism. 
"My only grandson became autistic right before my eyes  shortly after receiving  his federally recommended and state-mandated vaccines. Without a full  explanation of what was in the shots being given, my talkative, playful,  outgoing, healthy, grandson Christian was subjected to very high levels of  mercury through his vaccines. He also received the MMR vaccine. Within a few  days he was showing signs of autism." 
People often wonder why regulatory officials would protect drug makers. In large  part, because the CDC and FDA policy decisions are made through advisory panels  whose members have financial relationships with the same companies they are  charged to regulate. 
The decisions of the 300 experts who sit on the FDA's 18 advisory committees  affect billions of dollars in sales. The panel members play a crucial role in  determining what drugs will be approved and participate in just about every  major decision related to industry regulation. 
When it comes to vaccines, the large population in the USA transforms into a  lucrative customer base when our government makes vaccines mandatory and keeps  adding more and more to the list. Between 2003 and 2006, it was predicted that  the annual global market for vaccines would rise from $6 billion to $10 billion,  by Mark Benjamin for United Press International on July 21, 2003. 
Investors follow the decisions made by the advisory panels closely. A favorable  vote by a committee can add hundreds of millions of dollars to a company's stock  value which also means the potential for corruption in the panels is enormous. 
In a July 18, 2000 hearing, Burton mentioned the problem. "We have a lot of  doctors who serve on Federal advisory committees who have serious  conflicts-of-interest problems. Theyre allowed to vote on vaccines made by  companies that they get money from." 
An analysis conducted by USA Today, of 159 FDA advisory committee meetings that  took place between January 1, 1998, and June 30, 2000, revealed conflicts of  interest were wide-spread: 
At 92% of the meetings, at least one member had a financial conflict of  interest. 
At least one committee member had a financial stake in the topic under review at  146 of 159 advisory committee meetings. 
At 55% of meetings, half or more of the FDA advisers had conflicts of interest. 
At the 102 meetings dealing with the fate of a specific drug, 33% of the experts  had a financial conflict. 
Many parents are now refusing to vaccinate their children because they believe  there is an on-going conspiracy by government officials and the pharmaceutical  industry to boost profits by mandating unnecessary vaccines, while at the same  time, denying their potential for harm. Congressman Burton addressed this issue  in a June 19, 2002 hearing: 
"Parents are increasingly concerned that the Department may be inherently  conflicted in its multiple roles of promoting immunization, regulating  manufacturers, looking for adverse events, managing the vaccine injury  compensation program, and developing new vaccines. Families share my concern  that vaccine manufacturers have too much influence as well." 
Burton also noted the need to get honest about the current epidemic. "As  representatives of the people, we have a responsibility to ensure that our  public health officials are adequately and honestly addressing this epidemic and  its possible links to vaccine injury," he said. 
In May 2003, the Reform Committee, released a report that said the "FDA and the  CDC failed in their duty to be vigilant as new vaccines containing thimerosal  were approved and added to the immunization schedule." 
As an example the report cited the Hepatitis B vaccine. "When the Hepatitis B  and Haemophilus Influenzae Type b vaccines were added to the recommended  schedule of childhood immunizations, the cumulative amount of ethylmercury to  which children were exposed nearly tripled." The report identified thimerosal as  the cause of the autism and chastised the FDA: 
Thimerosal used as a preservative in vaccines is directly related to the autism  epidemic. This epidemic in all probability may have been prevented or curtailed  had the FDA not been asleep at the switch regarding a lack of safety data  regarding injected thimerosal and the sharper eyes of infant exposure to this  known neurotoxin. The public health agencies failure to act is indicative of  institutional malfeasance for self protection and misplaced protectionism of the  pharmaceutical industry. 
Many people have not yet recognized the seriousness of the epidemic, largely  because the majority of people have not seen many autistic children due to the  fact that parents seldom take their affected children out in public because of  the difficulty in trying to control them in a strange environment. 
However, the rising numbers in special education classes in the nation's public  school system provides a clear measurement of how wide-spread the epidemic has  become. State by state statistics for students with autism from the Department  of Education for the 12-year period between 1992-93 and 2003-04, are almost  unbelievable. For instance, in Ohio in 1992-93, there were only 22 cases of  autism, by 2003-04 there were 5,146. In Illinois, there were only 5 cases twelve  years ago and 6,005 in 2003-04. Wisconsin had 18 cases of autism in 1992-93 and  the numbers rose to 3,259 in 2003-04. 
The true reality of these statistics will register in the not too distant  future. "With eighty percent of autistic Americans under the age of 18, the  dramatic impact of this crisis will be felt by taxpayers in the coming years  when these autistic children become adults," says Anne McElroy Dachel, Media  Relations Coordinator for the National Autism Association. 
Most vaccines on the immunization schedule now are said to be thimerosal-free  but some still do contain trace amounts. "An exception is the flu shot, which  the Centers for Disease Control and Prevention recommends for pregnant women and  for infants 6 to 23 months old, advises Don Olmsted in United Press  International on November 19, 2005. 
For 6 to 23-month-old infants, the schedule calls for two flu shots that contain  12.5 micrograms of thimerosal each to be given a month apart. That total of is  the same amount that was in vaccines some parents believe triggered their  child's autism," Olmsted advises. "And some of them believe in utero exposure to  mercury via the pregnant mother might be the most dangerous exposure of all, he  said. 
When deciding whether mercury-laced flu vaccines are safe for children, parents  had better think long and hard before rolling the dice. 
Evelyn Pringle  <EMAILADDRESS>  
 <URL>  

Alan 
Nemesis Peace Centre 
 <URL>  
Abuse of Women and Children 
 <URL> / 
Nemesis News 
 <URL> / 
Absolute Anarchy 
 <URL> / 
 <URL> / 


