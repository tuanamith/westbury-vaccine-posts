


Family sues over alleged MMR link to autism  <URL>  
Tue 24 Jan 2006 TANYA THOMPSON HOME AFFAIRS CORRESPONDENT 
Legal experts said that if the challenge is successful, each child could receive £5 million in compensation. 
A spokeswoman for the Department of Health insisted MMR was safe and urged parents to have their children vaccinated. 
Related topics  * MMR vaccine  <URL>  * Autism  <URL>  
This article:  <URL>  
Last updated: 24-Jan-06 00:31 GMT 





