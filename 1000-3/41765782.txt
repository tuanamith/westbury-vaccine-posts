


FAS (Federation of American Scientists) is appalled by the first reduction  in health research funding in 36 years 
December 22, 2005 

Complete sequence of the human genome, 
clinical trials for pandemic flu vaccine, 
immunotherapy for melanoma, identification of a gene associated with the  most common form of blindness, 
a rapid test for inherited immune deficiency, 
development of a vaccine to prevent bacterial pneumonia in children, and 
a test that predicts the risk of breast cancer recurrence. 
Future achievements like this will be blocked or slowed by the short sighted  cuts in the 2006 research budget. 
In his 2005 State of the Union address, President Bush stated his view of  science funding: 
 <URL>  
"The Splendid Blond Beast", Christopher Simpson 

--  The fair use of a copyrighted work: 



