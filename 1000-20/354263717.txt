


Job Title:     Staff Biologist / Research Biologist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-03-28 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Staff Biologist / Research Biologist &#150; BIO001733 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Responsibilities would include developing, optimizing, and running a variety of biological and biochemical assays using a range of automated equipment. Overall, the assays would be mainly in support of counterscreen activities, but could also include assisting with supporting other lead optimization efforts. 
Qualifications Required- Aptitudes in operating automated laboratory equipment are sought. Careful attention to detail, ability to follow procedures independently, and computer skills are paramount in this position. Successful candidate would have demonstrated their ability to effectively work as part of team. Desired- Prior experience with CYP or ion channel assays is desirable. Education Requirement- A Bachelor&#8217;s or Masters degree in biochemistry, biology, or related disciplines is required plus 1-10 years of relevant laboratory experience. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # BIO001733. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm RepresentativesPlease Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  03/26/2008, 10:20 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/25/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-386696 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



