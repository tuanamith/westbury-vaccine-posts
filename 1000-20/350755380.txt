


Job Title:     Senior Compensation Consultant--Merck &amp; Co.,Inc. Job Location:  NJ: Whitehouse Station Pay Rate:      Open Job Length:    full time Start Date:    2008-03-22 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Compensation Consultant--Merck &amp; Co.,Inc. &#150; HUM001235 
Job Description  
Submit 
Working with the Global Rewards Leader, supports the design, development and implementation of global compensation programs for Merck &amp; Co., Inc. Performs detailed analyses and provides consulting support to Regional Compensation (RBTM) team members, including but not limited to base salary management, variable pay, job evaluations, annual salary planning, short and long-term incentives. Primary responsibilities include, but are not limited to: .Responsible for monitoring market trends and conducting analysis and recommendations on design, development and deployment of compensation programs. .Works interdependently with Regional Reward and Talent Management Team and other Center of Expertise (COE&#8217;s) like HR Communications, Talent Management, Staffing, Organizational Development etc. to assist with facilitating communication, implementation and administration of compensation strategy, policy and practice. .Provides support to ensure governance and appropriately consistent implementation and administration of programs and practices across divisions and regions. .Contributes to compensation projects and may be required to lead projects .Interfaces with external consultants, vendors and/or compensation regional survey providers to manage projects and/or problem solve complex service and data issues. .Responsible for performing detailed compensation analyses and manage projects in an autonomous manner. 
Qualifications 


Bachelor&#8217;s degree required; master&#8217;s degree preferred.  .At least 7 years of relevant experience required. 5 years of relevant experience required if in possession of a graduate degree. .Technically proficient in all areas of compensation required. .Prior experience/background in domestic or international compensation and compensation administration systems is desirable. .Experience in implementing and administrating specific compensation programs, in partnership with HRBPs and business clients required. .Experience in managing working relationships with external vendors desired. .Flexibility in working hours and team work to support regions operating in a different time zone is a key requirement along with a demonstrated ability to work within a diverse cultural environment. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #HUM001235 . Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means.  
Profile   Locations  US-NJ-Whitehouse Station   Employee Status  Regular   Travel  Yes, 25 % of the Time  
Additional Information   Posting Date  03/20/2008, 02:49 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/03/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-387316 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



