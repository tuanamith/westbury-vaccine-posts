


On Feb 24, 5:43 pm, the 3rd Man < <EMAILADDRESS> > wrote: 
First I am NOT trying to apply this to an individual case, what individual case are we talking about anyway? Sheesh. 
If I am, I am breaking some law? 
I am saying NO MORE THAN THIS: 
 <URL>  

EXCERPT: 

LYME IgG/IgM ANTIBODY SEROLOGY 



 <URL>  Journal of Spirochetal and Tick-borne Diseases--Volume 5, Spring/ Summer 1998 

EXCERPT: 






 <URL>  
 <URL>  
In 1990, more than 20 commercially prepared serologic test kits for Lyme disease were being sold in the United States, but no nationally standardized reference test was available. A collaborative evaluation of a selected sample of the commercial test kits by the Centers for Disease Control and Prevention (CDC) and the Association of State and Territorial Public Health Laboratory Directors (ASTPHLD) demonstrated poor concordance of results among these test kits and among a selected group of state health department laboratories (11). Because of the lack of a rigorously defined reference serum panel, conclusions could not be drawn about the sensitivity and specificity of the test kits evaluated. An unexpected finding in this study was the low concordance in test results between CDC and two consulting academic reference center laboratories. A number of other studies also have demonstrated low concordance of Lyme disease serologic test results obtained by a variety of laboratories (4-10). 
4. Bakken LL, Case KL, Callister SM, Bourdeau NJ, Schell RF. Performance of 45 laboratories participating in a proficiency testing program for Lyme disease serology. JAMA 1992;268:891-5. 
5. Hedberg CW, Osterholm MT, MacDonald KL, White KE. An interlaboratory study of antibody to Borrelia burgdorferi. J Infect Dis 1987;155:1325-7. 6. Hedberg CW, Osterholm MT. Serologic tests for antibody to Borrelia burgdorferi--another Pandora's box for medicine? Arch Intern Med 1990;150:732-3 7. Jones JM. Serodiagnosis of Lyme disease. Ann Intern Med 1991;114:1064. 8. Lane RS, Lennette ET, Madigan JE. Interlaboratory and intralaboratory comparisons of indirect immunofluorescence assays for serodiagnosis of Lyme disease. J Clin Microbiol 1990;28:1774-9. 9. Luger SW, Krauss E. Serologic tests for Lyme disease: interlaboratory variability. Arch Intern Med 1990;15:761-3. 10. Schwartz BS, Goldstein MD, Ribeiro JMC, Schulze TL, Shahied SI. Antibody testing in Lyme disease: a comparison of results in four laboratories. JAMA 1989;262:3431-4. 

Why? The averages are pretty much identical when using relatively smaller and relatively larger sample groups. Findings in the Lyme vaccine trials were similar to findings in other studies. 


And so what is the problem with that? 
That is what I was talking about anyway, the general reliability of the ELISA test and the fact that negatives aren't a reliable way to rule out Lyme disease. 

Why? 

Which would be horrible because..............????? 
And that is what I was talking about not a specific individual case. Where are we talking about an individual case? 
You thought for some strange reason there was too little information about my friend's case. I'm not treating him anyway. I don't know what his test results are even he doesn't yet. I think it was stupid to even do the test honestly, why not just treat him w a couple of weeks of abx? They're treating possible HSV1 that way, and the rationale for both is the same: common cause of bells palsy, additional arguments for Lyme treatment: he lives in an endemic area and does a lot of work outdoors in his yard where he has gotten bitten by ticks before, ticks that carried Lyme disease. He had a flu like illness followed by the bells palsy. No history of HSV1 or cold sores even, no positive test for HSV1. 
That's the only individual case I was talking about. 

Where? 

It is a theoretical discussion. I'm not giving medical advice. If you look the whole discussion started talking about igenex and babesia testing and I made a general comment about testing which you have been beating to death ever since. I admitted that I made a mistake in discounting positives while focused on the theoretical statistical argument. Whatever. Mea Culpa. 
But in the first place here is what I said and all that I was saying: 
"A "good lab" isn't necessarily one that gives people positives because that confirms what they think they have. The testing for Lyme is so bad wb, elisa that you might as well flip a coin. PCR in commercial labs isn't that well established as valid either. Bottom line it is a clinical diagnosis. To me the most reliable thing is ruling out other things on the differential through testing we know is more reliable and if Lyme is left after ruling out other things then it is a fair provisional hypothesis. Empiric treatment on that basis seems entirely rational and evaluation of symptom response can help validate a clinical diagnosis. Positive tests for Lyme from Igenex or wherever might help support the diagnosis, I wouldn't rely on negative tests to rule it out. Again, testing for other things on the differential and ruling them out with testing we KNOW is more reliable than any Lyme testing seems a reasonable approach." 

Well please sue me for whatever crime I'm committing making a general statement about the sensitivity of ELISA testing. 

Whatever. 

It was a general theoretical discussion dude. 
Don't get so excited about it. 
No I wasn't talking about an individual case. I was making a general statement. And specifically there in a theoretical context. 
YOU were talking about YOUR case. Again, I'm not commenting on your case. And YOU apparently had positives serially not negatives. 
 > > > THAT is what this entire discussion has been about. All hundred some 
Beyond tedious. 
Your welcome. 


Didn't I say thank you and good night? 

