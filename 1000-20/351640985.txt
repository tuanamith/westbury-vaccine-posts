


Anger Over Culling of Yellowstone's Bison By JIM ROBBINS Published: March 23, 2008 GARDINER, Mont. - This was not the Yellowstone National Park that tourists  see. 
At first light on Tuesday, at the end of a closed road, past a boneyard of  junk cars, trailers and old cabins, more than 60 of the park's wild bison  were being loaded on a semi-trailer to be shipped to a slaughterhouse. 
With heavy snow still covering the park's vast grasslands, hundreds of bison  have been leaving Yellowstone in search of food at lower elevations. A  record number of the migrating animals - 1,195, or about a quarter of the  park's population - have been killed by hunters or rounded up and sent to  slaughterhouses by park employees. The bison are being killed because they  have ventured outside the park into Montana and some might carry a disease  called brucellosis, which can be passed along to cattle. 
The large-scale culling, which is expected to continue through April, has  outraged groups working to preserve the park's bison herds, considered by  scientists to be the largest genetically pure population in the country. It  has also led to an angry exchange between Montana state officials and the  federal government over a stalled agreement to create a haven for the bison  that has not received the needed federal financing. 
"When they leave the park they have nowhere to go," said Gov. Brian  Schweitzer of Montana, a Democrat. "This agreement would have given them a  place to go." 
Al Nash, a spokesman for Yellowstone National Park, said park employees  tried to haze the bison into returning to the park but often met with  limited success. Last week, two employees on horseback drove a large herd  across a snow-flecked mountain from the north entrance back into the park. 
"They come right back out again," Mr. Schweitzer said. "They just rebel.  What would you do if you were a starving buffalo?" 
The culling of bison at Yellowstone, while legal, has been a briar patch of  controversy for more than two decades. In 1996, the count reached a peak -  until this year - when 1,084 animals were killed. 
In 2000, the State of Montana, the National Park Service, the United States  Forest Service and the Animal Plant Health Inspection Service, which  oversees disease issues for the Department of Agriculture, signed an  agreement to manage the population. It had two main objectives: to stop the  spread of brucellosis, which can also be transmitted from elk, and to allow  some bison to leave Yellowstone unmolested. 
Conservationists, Montana state officials and other critics say the first  part of the agreement has been honored, but the second part has been ignored  by the federal government. 
"The public should be outraged," said Amy McNamara, national parks program  director for the Greater Yellowstone Coalition in Bozeman, Mont., which has  worked to allow bison to leave the park. "An American icon is being taken to  slaughter." 
Ms. McNamara added, "By next week they'll be in somebody's freezer." 
Federal officials say the money needed to make the agreement work - to  obtain land along the Yellowstone River that would allow the bison to cross  from the park to a publicly owned forest north of the park - has not been  allocated by Congress. 
Bruce Knight, under secretary for marketing and regulatory programs for the  Department of Agriculture, said his department did not manage land or pay  for the acquisition of habitat. "I've never received a directed  appropriation for that," Mr. Knight said. 
At issue is a corridor of land on the Royal Teton Ranch, owned by a  religious group called the Church Universal and Triumphant. Last fall, a  final stumbling block was removed when church leaders agreed to move their  cattle off 2,500 acres of the land so the bison could cross to the forest,  about 10,000 acres farther downstream. Any movement from there is blocked by  a narrow canyon and the river. 
With the cattle removed from the land, there would be no risk of  transmission of brucellosis from infected bison. The plan would allow 25  bison who had tested negative for exposure to the disease to be allowed out  of the park. If that went well, 50 or more would be allowed to leave, and so  on. 
The State of Montana and conservationists committed to raising $1.3 million  toward the $3 million or so it would cost to lease the church group's land  for 30 years. They expected the federal government, through the Animal Plant  Health Inspection Service, to provide the balance. 
Mr. Schweitzer blamed Representative Denny Rehberg, Republican of Montana,  for leading the opposition last summer to a $1.5-million Congressional  appropriation that would have fulfilled the federal obligation. "He killed  it," Mr. Schweitzer said. 
A spokesman for Mr. Rehberg, Bridger Pierce, said Mr. Rehberg wanted the  spread of brucellosis dealt with inside the park before any bison were  allowed to migrate outside. 
The standoff has been made all the worse by the detection last year of  brucellosis in several cattle elsewhere in Montana. Though experts believe  the disease was transmitted by elk, not bison, the case has stirred passions  among ranchers. Brucellosis is a bacterial infection that can cause  spontaneous abortion in cattle, and when detected, requires that the cattle  be destroyed. 
If another incidence of brucellosis appears in Montana, the state would lose  its brucellosis-free status, which would mean each cow exported would need  to be tested, an expensive proposition for ranchers. Wyoming and Idaho only  recently regained their status as brucellosis free after cases were detected  in those states in 2004 and 2005. 
"Our interest is having a brucellosis-free United States," said Mr. Knight,  the agriculture official. "The sole remaining reservoir is in the Greater  Yellowstone. That makes it an exceptionally high priority for us." 
Mr. Knight says the best solution would be a vaccine for bison, which he  said could be a year away. Park officials, however, say it is not known when  a vaccine, which they are researching, will be available. 
In the meantime, conservationists and researchers who care about the bison  worry that serious damage is being inflicted on the population here. 
In the last few years biologists have discovered that Yellowstone's bison  are one of only two genetically pure herds owned by the federal government. 
James Derr, a professor of genetics at Texas A&M who is studying the  Yellowstone bison, said he feared that some behaviors or traits, including  the propensity to migrate, could be lost with the killed bison. "The  great-grandmother, grandmother, mother and daughter often travel together,"  he said. Killing them "is like going to a family reunion and killing off all  of the Smiths. You are affecting the genetic architecture of the herd." 
In the next few weeks, so-called green-up - when the snow melts and new  grass sprouts - is expected to begin in the park. At that time, some  captured bison being held at a facility here who test negative for exposure  to brucellosis will be released and allowed to head back into the park.  Those that test positive, however, will be slaughtered. 
"It's a very difficult thing," said Mr. Nash, the park spokesman, as he  watched park employees load the bison for slaughter on Tuesday. "They do the  job they have to do, but that doesn't mean they enjoy doing it." 

--  
 <URL>  
got love? Remember to spay or neuter your pet 




