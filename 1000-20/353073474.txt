


FREE UNIVERSITY Transferring Prime Human Attention from Objects to States of Mind     Via < <EMAILADDRESS> >     March 4, 2008 
                                                   Breakthrough Energy - 19 
From: President, USA Exile Govt [ <EMAILADDRESS> ] Sent: Friday, February 29, 2008 11:05 PM To: Wade Frazier Subject: For Wade     Dear Wade,   Here's another one I'd love comment on--if you have time.   Highest regards,   Keith 
--------------------------------------- 
From: Ecological Options Network < <EMAILADDRESS> > Date: February 29, 2008 6:59:23 PM EST Subject: GMO Eats Climate Crisis: New Life Form to Turn CO2 to Fuel 
 <URL> / Famed_geneticist_creating_life_form_02282008.html 
  
Famed geneticist creating life form that turns CO2 to fuel   AFP   Published: Thursday February 28, 2008 
  A scientist who mapped his genome and the genetic diversity of the oceans said Thursday he is creating a life form that feeds on climate-ruining carbon dioxide to produce fuel. 
  Geneticist Craig Venter disclosed his potentially world-changing "fourth-generation fuel" project at an elite Technology, Entertainment and Design conference in Monterey, California. 
  "We have modest goals of replacing the whole petrochemical industry and becoming a major source of energy," Venter told an audience that included global warming fighter Al Gore and Google co-founder Larry Page. 
  "We think we will have fourth-generation fuels in about 18 months, with CO2 as the fuel stock." 
  Simple organisms can be genetically re-engineered to produce vaccines or octane-based fuels as waste, according to Venter. 
  Biofuel alternatives to oil are third-generation. The next step is life forms that feed on CO2 and give off fuel such as methane gas as waste, according to Venter. 
  "We have 20 million genes which I call the design components of the future," Venter said. "We are limited here only by our imagination." 
  His team is using synthetic chromosomes to modify organisms that already exist, not making new life, he said. Organisms already exist that produce octane, but not in amounts needed to be a fuel supply. 
  "If they could produce things on the scale we need, this would be a methane planet," Venter said. "The scale is what is critical; which is why we need to genetically design them." 
  The genetics of octane-producing organisms can be tinkered with to increase the amount of CO2 they eat and octane they excrete, according to Venter. 
  The limiting part of the equation isn't designing an organism, it's the difficulty of extracting high concentrations of CO2 from the air to feed the organisms, the scientist said in answer to a question from Page. 
  Scientists put "suicide genes" into their living creations so that if they escape the lab, they can be triggered to kill themselves. 
  Venter said he is also working on organisms that make vaccines for the flu and other illnesses. 
  "We will see an exponential change in the pace of the sophistication of organisms and what they can do," Venter said. 
  "We are a ways away from designing people. Our goal is just to make sure they survive long enough to do that." 
____________________________________ EON   The Ecological Options Network "Documenting Solutions"  www.eon3.net 
======================================================================== ================== 
From: "Wade Frazier Comcast" Date: March 1, 2008 11:09:43 AM EST To: "'President, USA Exile Govt'" < <EMAILADDRESS> > Subject: RE: For Wade 
Hi Keith:   The most common reaction that I get when talking about FE are the magic of capitalism, laws of physics, conspiracy theory and other denials that FE is possible or that it could be suppressed. 
If somebody gets past those denial reactions, then open fear is the next most common reaction.  I just received one of those reactions this week.  People immediately imagine all the bad things that can happen with FE, and never even acknowledge the upside.  Those kinds of reactions helped lead to my hooked on scarcity essay:    <URL>    Genetic engineering is another Pandoras Box, similar to how FE is seen, but instead of fear reactions, there is this enthusiasm for tinkering with DNA to solve the problems that we have created.  To read about how vaccines are a side-benefit only adds to my skepticism for that solution.    <URL>    The chemistry and thermodynamics of this solution escape me at this time.  I believe that carbon dioxide has about the lowest potential energy of carbon compounds, because it is fully oxidized, so the part of making fuel from it seems odd.  Also, if they make octane out of carbon dioxide, and they call it a fuel, then burning it will simply reintroduce it to the atmosphere.  Is the goal carbon sequestration or using carbon dioxide as an energy source?  I think they are mutually exclusive goals.  Again, I doubt there is any available chemical energy in carbon dioxide.  Plants can only use carbon dioxide because the energy they derive from photosynthesis powers the process.  Unless those organisms are getting their energy from someplace else, I do not see how they can turn carbon dioxide into fuel.  A college professor recently calculated that the energy produced by burning hydrocarbon fuels is hundreds of times the net biological production of earth.  Even if that estimate is a little high, it shows how putting all of earths life forms in the yoke of serving humanity will not even come close to supporting the energy needs of the United States, much less the world.    So, all these, lets exploit life forms to meet our industrialized energy needs plans are not solutions, IMO, and certainly are not enlightened.  As Brian O says, the answer to all of those conventional solutions is none of the above:    <URL>    It is time to get beyond all these small-ball answers and think outside the box.  The big, permanent, harmless solutions await, if enough of us can wake up in time.   Best,   Wade 
======================================================================== ================================================ 

