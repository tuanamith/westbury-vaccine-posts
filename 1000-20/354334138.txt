


Job Title:     Senior Research Biologist / Research Fellow - Ophthalmic... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-03-30 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Research Biologist / Research Fellow - Ophthalmic Research &#150; BIO001740 
Job Description  
Submit 
Description 
The Department of Ophthalmology Research at the Merck Research Laboratories in West Point, PA is seeking a highly motivated research scientist with extensive experience in the study of ocular biology, diabetic complications and/or neuroscience. The successful candidate will lead a team of investigators focusing on the discovery of novel therapeutics for the treatment of ocular diseases including AMD and diabetic retinopathy. Job responsibilities include designing and interpretingin vitroandin vivostudies, communicating results to multi-disciplinary teams, and developing a strategic vision for their group.  
Qualifications Qualified candidates should have a Ph.D. or MD/Ph.D. in Ocular Biology, Vascular Biology, Metabolic Diseases, Neuroscience, Biochemistry, Pharmacology, or a related discipline. A minimum of two (2) years post-doctoral experience in a related field or two (2) years pharmaceutical / biotechnology industry experience in drug development is required. Demonstrated leadership skills and a track record of quality publications and presentations at national scientific meetings is necessary. Previous involvement in discovery research and target validation including study design and statistical analysis is desirable. Preferred candidates will have experience with people management, a command of relevant ocular, diabetes and neuroscience literature, and a strong understanding ofin vitroandin vivopharmacology. The individual should demonstrate outstanding analytical skills and initiative; in addition, excellent interpersonal and communication skills are essential. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #BIO001740. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  03/28/2008, 03:32 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  04/27/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-388436 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



