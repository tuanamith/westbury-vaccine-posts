


 <URL>  
Deal in an Autism Case Fuels Debate on Vaccine 
WASHINGTON - Study after study has failed to show any link between vaccines  and autism, but many parents of autistic children remain unconvinced. For  the skeptics, the case of 9-year-old Hannah Poling shows that they have been  right along. 
The government has conceded that vaccines may have hurt Hannah, and it has  agreed to pay her family for her care. Advocates say the settlement -  reached last fall in a federal compensation court for people injured by  vaccines, but disclosed only in recent days - is a long-overdue government  recognition that vaccinations can cause autism. 
"This decision gives people significant reason to be cautious about  vaccinating their children," John Gilmore, executive director of the group  Autism United, said Friday. 
Mr. Gilmore has filed his own claim that his son became autistic as a result  of vaccinations. 
Government officials say they have made no such concession. 
"Let me be very clear that the government has made absolutely no statement  indicating that vaccines are a cause of autism," Dr. Julie L. Gerberding,  director of the Centers for Disease Control and Prevention, said Thursday.  "That is a complete mischaracterization of the findings of the case and a  complete mischaracterization of any of the science that we have at our  disposal today." 
Hannah, of Athens, Ga., was 19 months old and developing normally in 2000  when she received five shots against nine infectious diseases. Two days  later, she developed a fever, cried inconsolably and refused to walk. Over  the next seven months she spiraled downward, and in 2001 she was given a  diagnosis of autism. 
Hannah's father, Dr. Jon Poling, was a neurology resident at Johns Hopkins  Hospital at the time, and she underwent an intensive series of tests that  found a disorder in her mitochondria, the energy factories of the cells. 
Such disorders are uncommon, their effects can be significant but varied,  and the problems associated with them can show up immediately or lie dormant  for years. 
There are two theories about what happened to Hannah, said her mother, Terry  Poling. The first is that she had an underlying mitochondrial disorder that  vaccinations aggravated. The second is that vaccinations caused this  disorder. 
"The government chose to believe the first theory," Ms. Poling said, but  added, "We don't know that she had an underlying disorder." 
Mr. Gilmore has filed his own claim that his son became autistic as a result  of vaccinations. 
Government officials say they have made no such concession. 
In a news conference on Thursday, Dr. Edwin Trevathan, director of the  National Center for Birth Defects and Development Disabilities at the  disease control agency, said, "I don't think we have any science that would  lead us to believe that mitochondrial disorders are caused by vaccines." 
Dr. Trevathan explained that children with mitochondrial disorders often  develop normally until they come down with an infection. Then their  mitochondria are unable to manufacture the energy needed to nourish the  brain. As a result, they regress. 
The Poling case has become a flashpoint in the long-running controversy over  thimerosal, a vaccine preservative containing mercury. Some people believe  that thimerosal is behind the rising number of autism diagnoses. Among them  is Lyn Redwood, director of the Coalition for SafeMinds. 
Many of the vaccines Hannah received contained thimerosal, and to Ms.  Redwood, she is more proof of thimerosal's dangers. 
The disease control centers, the Food and Drug Administration, the Institute  of Medicine, the World Health Organization and the American Academy of  Pediatrics have all largely dismissed the notion that thimerosal causes or  contributes to autism. 
Five major studies have found no link, and since thimerosal's removal from  all routinely administered childhood vaccines in 2001, there has been no  apparent effect on autism rates. 
Many of those who believe in an autism-vaccine link dismiss all this  evidence, and Hannah's case fuels their cause. 
"Her story is very important because it echoes so many others, and it's  clear that thimerosal played a role," said Rita Shreffler, executive  director of the National Autism Association. 
Dr. and Mrs. Poling said Hannah did not prove the case against thimerosal,  but Dr. Poling noted that there was no debate that vaccines had risks. 
"They're not safe for everybody," he said, "and one person for whom they  proved unsafe happened to be my daughter." 





