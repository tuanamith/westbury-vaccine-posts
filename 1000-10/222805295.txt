


On Mar 17, 9:59 pm, "Rupert" < <EMAILADDRESS> > wrote: 

OK. I still disagree, though, and you know why. 

Dammit, you caught me, Rupert ;) This actually is more favorable to my argument, as it doubles the risk in men. 

This is an anti-circ propaganda site. The article contains nonsense like the following: 
- Citing all the silly reasons used in the past to jusify circumcision, as if this had anything to do with the argument today. 
- Claiming that the death rate from circumcision is several per thousand. In fact it's at most several per million, utterly negligible. 
- Implying that the new HPV vaccine protects against all HPV-induced cancers. In fact the manufacturer admits this is at most 70%, and it will drop with time as new strains take over as the most common. 

Even with your figures, it is non-negligible. 

Yes, but all of them have significant costs of one sort or another. Circumcision doesn't, as there is no proof that circumcised men are worse off in any way whatever. 

It was once thought so, but that was disproved. Of course, this would never have been conjectured were it not for the obvious correlation with lack of circumcision. 

They do (with regard to HPV) just as much as they justify requiring the HPV vaccine. You never answered my question: do you think routine infant circumcision should be illegal? 
Andrew Usher 


