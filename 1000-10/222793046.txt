


Why the Rush? 
Related Audio/Video Downloads 
      <URL>  
 The Case Against Gardasil 
Note: This commentary was delivered by Prison Fellowship President Mark Earley. 
So why not vaccinate girls against HPV? The reasons are both practical and moral. 
So why the rush to make Gardasil mandatory for students? 
By Mark Earley 3/19/2007 
   --------------------------------------------------------------------- 
For Further Reading and Information 

Michael Fumento, "A Merck-y Business," Weekly Standard, 12 March 2007. 
Gina Dalfonzo, "Oops!" The Point, 14 March 2007. (Comment here on BreakPoint's blog about the HPV vaccine.) 
Motte Brown, "Assumptions about Daughters, Casual Sex, and HPV," The Line, 14 March 2007. 
"New Studies and Your Health," USA Today, 2 March 2007. 
Melissa Hendricks, "HPV Vaccine: Who Chooses?" Los Angeles Times, 5 February 2007. 
Arthur Allen, "Underage: Why It's Too Soon for a Mandatory HPV Vaccine," New Republic, 2 March 2007. 
Steven Groopman, "Blind Faith: Conservative Christians and HPV," New Republic, 10 March 2007. 
Janet Elliott, "House Passes Bill to Overturn HPV Vaccine Order," Houston Chronicle, 14 March 2007. 
"N.M. Governor to Sign Bill Requiring Girls to Get HPV Vaccine," USA Today, 12 March 2007. 
Tim Craig, "Kaine Says He'll Sign Bill Making Shots Mandatory," Washington Post, 3 March 2007, B10. 
"Calif. Bill to Require Cancer Vaccine for Girls," CBS13.com, 12 March 2007. 
"D.C. Lawmakers Consider Mandatory HPV Vaccine," NBC4.com, 8 March 2007. 
Kate Clements, "HPV Vaccine Bill Coasts through House Panel," News- Gazette (Ill.), 15 March 2007. 





