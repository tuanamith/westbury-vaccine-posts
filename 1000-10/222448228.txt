





1] you'd like to believe in the concept of personal responsibility, but it cost you too many freedoms 
2] You give more money to the ACLU than you spend on your therapist 
3] you see no difference whatsoever between a cold-blooded murder and a State-ordered execution 
4] you can't imagine why a convicted sex offender wouldn't obey the law and register when relocating to a new area 
5] you firmly believe that the only evil thing in the world is a conservative armed with all the facts 
6] you believe that as long as you don't admit to the truth, no one will see that you're a liar 
7] you see the Military as a totally unnecessary expenditure that should be liquidated, with the revenue going to help the poor 
8] you draw no parallel between the number of people in prison and the crimes that sent them there 
9] you see 'No' as a bad word and an obscene concept. Especially where children are concerned 
10] you see men like Vermont's Judge Cashman, who doesn't believe in the concept of punishment, as righteous and fair 
11] you can't understand why President Carter and President Clinton's hopes and dreams for North Korea didn't come off as planned 
12] you see the events of 911 as common ordinary everyday crimes 
13] you see President Bush as a ruthless dictator like Adolph Hitler and Osama bin Laden as a heroic freedom fighter similar to George Washington 
14] you see the Guantanamo Bay detainees as the real victims of 911 
15] you can't understand why more people don't feel sorry for the hurricane Katrina victims 
16] you'd rather make love and not war. Even with an enemy who has made it clear that he wants to kill you 
17] you believe that while the victim can't be helped, the killer wants to be reached 
18] you can't understand why the Manson girls haven't been pardoned yet 
19]  you secretly feel that John Lennon's killer should rot in hell. Even though you don't believe in the place 
20] you see nothing abnormal with two men screwing each other. In fact, you see nothing abnormal period 
21] you see the religious right in this country as a major threat, but think that Islam should be mandatory curriculum in the public schools 
22] you see capital punishment as just another wrong and still think that it's supporters are trying to make a right. 
23] you see people protesting the construction of a porno theater next to their church and you wonder what all the fuss is about 
24] you don't believe in any god other than yourself 
25] you see nothing wrong with a defense attorney marrying her death- row client 
26] you believe that the bum on the corner is really going to use that dollar to buy food 
27] you think that blacks that loot and burn during race riots are 'just getting even' 
28] you believe in forgiving the negroe and turning away from the mess he makes... No matter what 
29] you still harbor a grudge against your parents because you discovered they weren't perfect 
30] you see yourself as being.. 'a member one race.. the human race', but actively encourage minorities to be proud of their racial heritage 
31] you view the removal of a baby elephant from the rest of the herd as an act of animal cruelty, but you see nothing wrong with raising a child without a father 
32] you don't believe in animal testing but you complain that they're not working fast enough on a vaccine for AIDS 
33] you actually think that yelling 'fire' in a crowed theater should be covered by the first amendment 
34] you're able to hold the concepts of 'No War' and 'Free Tibet' in your mind simultaneously 
35] you don't have a problem with minorities living wherever they want. Just so long as it's not next door to you 
36] you don't believe in bad people. Only bad choices 
37] you see bombs dropped by Democrats as good and bombs dropped by Republicans as bad 
38] you think the dictionary should be revised to exclude oral stimulation from the definition of 'sex' because your favorite President said so 
39] you don't complain against the way the democrat President bungled the war on terror, but you gripe your head off when the eight year-old bill comes due at the beginning of the next guy's shift 
40] you think that it's a fine thing for Blacks and Hispanics to serve in politics, just as long as it's not on the Supreme Court 
41] you see AIDS as a racist, anti-gay disease and you'd like to say so, but you know the world isn't ready for that much of a stretch. 
42] you believe in 'reasoning' with children instead of just telling the immature, fickle little brats what you want them to do 
43] you believe that women should be allowed into combat roles in the Army, if for no other reason than to show they can get away with it 
44] you don't agree with low scores turned in by African Americans on IQ tests because you feel the tests were 'geared towards whites'. When someone reminds you that spear-chucking and nose-piercing were not part of the curriculum, you call them a racist 
45] you think that in an election, they should keep counting votes until your candidate wins 
46] you believe that criminals will be good citizens and turn in their firearms if the 2nd amendment is ever repealed 
47] you dangle 'the children' in front of any debate that would control welfare spending, yet view abortion as just another form of birth control 
48] you view the gun used in the commission of a crime as the primary responsible party instead of the actual assailant who chose to misuse it 
49] you see nothing abnormal with two lesbians playing with a double- headed dildo, but you haven't let your own husband near you in over a year 
50] you keep trying to con the public into believing that a huge meat- eating fish with a mouthful of razor-sharp teeth is really a misunderstood creature 
51] you find the practice of 'white flight' basically abhorrent, yet you'll never in a million years move into an all-black neighborhood 
52] you think that the white race is by far the most wicked group of people on the face of the earth....... and you're white yourself 
53] you have a sneaking suspicion that your parents genuinely love your sister's all-white children while only pretending to love yours. 
54] you secretly approved of the proposal to change the actual image of the three white firefighters raising the flag at ground zero to a multi-racial one 
55] despite all the school shootings and teen suicides, you still believe that a child needs a village, not a home 
56] you basically agree with the premise that a tornado can careen thru a junkyard and assemble a 747. You call this process 'evolution'. 
57] your typical reaction when someone hands you a gun is to recoil from it as if it were a snake 
58] you think there's a fairy-tale land called ' The Government' where all your tax-and-spend money comes from 
59] you're all for people working hard, sacrificing and enjoying the success that comes with it, just as long as it doesn't make those who didn't work 'feel bad' 
60] you believe that the Founding Fathers were including Blacks when they wrote "All men are created equal". Even thought most of them owned slaves at the time 
61] you label the TV media 'conservative-biased' because one of them doesn't march to the bang of your drum 
62] you stand behind the idea of giving law enforcement the power to do their job, as long as that power is limited to harsh language only 
63] you know in your heart that there's really nothing wrong with loving Uncle Remus, but the Blacks hate him, so that's one of those things that...... "we just don't talk about' 
64] you wholeheartedly support women's rights, so long as the umbilical cord is not attached 
65] getting down on your knees and begging for mercy is your primary means of self-defense 
66] you tell yourself that if people will buy gay marriage, "we can get them to swallow anything" 
67] the Principal at your son's school informs you that during a random search of his locker, they found guns, bomb making material and a plan to blow up the entire building. Your immediate response is to accuse them of an illegal search and seizure 
68] the last time you had an orgasm was when O.J. Simpson was found not-guilty 
69] you secretly feel good inside when you learn that a criminal has been released on a technicality 
70] you believe that homeless people would rather work and criminals would like to be rehabilitated 
71] you don't have a problem providing endless appeals to death-row inmates, but argue that the death penalty is no good because it cost too much 
72] your basic motto in life is "No rules, just right" because it works good for you 
73] you have no problem with Mexicans slipping across the border so they can have their babies here at the taxpayer's expense because... "freedom is such a fine thing, man" 
74] you raise cane getting the voting ballots in your state to contain 16 different languages, even though you know that the first thing your own immigrant forefathers did when they got here was to learn to speak English 
75] you speak in glowing terms of American society being a 'salad bowl', but then say how proud you are that you're 'colour-blind' 
76] you strongly support freedom of speech... as long as the person who's speaking thinks exactly as you do 




from the mind of: 

 IBen Getiner.. 


