


Jim07D7 wrote:  > An 'approach' that seems to have been soundly demolished by Edmond  Gettier. Maybe Karl Popper too. 
As Edmond Gettier demonstrated <see:  <URL> >, belief has not been found  to be a factor in demonstrating that a given statement is a fact. 
What demonstrates a fact is NEVER merely the stating of a personal  subjective conviction (a belief), like for example, "I believe this  vaccine will prevent disease D," but rather an explanation of how it is  known that the statement in question is in accord with the actual state  of affairs so that anyone can check the observations. 

 <URL>  
See also Karl Popper, _The Logic of Scientific Discovery_, Chapter 1,  Section 8, 
"Scientific Objectivity and Subjective Conviction" 
[Personal subjective conviction with no basis in fact has no bearing on  demonstrating scientific discovery.] 


