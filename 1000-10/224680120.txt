




1] you'd like to believe in the concept of personal responsibility, but it cost you too many freedoms 
2] You give more money to the ACLU than you send on your therapist 
3] you see not difference whatsoever between a cold-blooded murder and a State-ordered execution 
4] you can't imagine why a convicted sex offender wouldn't obey the law and register when relocating to a new area 
5] you firmly believe that the only evil thing in the world is a conservative armed with the facts 
6] you believe that as long as you don't admit to the truth, no one will see that you're a liar 
8] you draw no parallel between the number of people in prison and the crimes that sent them there 
9] you see 'No' as a bad word and basically an obscene concept. Especially where children are concerned 
10] you see men like Vermont's Judge Cashman, who doesn't believe in the concept of punishment, as righteous and fair 
11] you can't understand why President Carter and President Clinton's hopes and dreams for North Korea didn't come off as planed 
12] you see the events of 911 as common ordinary everyday crimes 
14] you see the Guantanamo Bay detainees as the real victims of 911 
15] you can't understand why more people don't feel sorry for the hurricane Katrina victims 
16] you'd rather make love and not war. Even with an enemy who has made it clear that he wants to kill you 
17] you believe that while the victim can't be helped, the killer wants to be reached 
18] you can't understand why the Manson girls haven't been pardoned yet 
19]  you secretly feel that John Lennon's killer should rot in hell. Even though you don't believe in the place 
20] you see nothing abnormal with two men screwing each other 
22] you see capital punishment as just another wrong and still think that its supporter are try and make a right. 
24] you don't believe in any god other than yourself 
25] you see nothing wrong with a defense attorney marrying her death- row client 
26] you believe that the bum on the corner is really going to use that dollar to buy food 
27] you think that blacks that loot and burn during race riots are 'just getting even' 
28] you believe in forgiving the negroe and turning  away from the mess he makes... No matter what 
29] you still harbor a grudge against your parents because you discovered they weren't perfect 
32] you don't believe in animal testing but you complain that they're not working fast enough on a vaccine for AIDS 
33] you actually think that yelling 'fire' in a crowed theater should be covered by the first amendment 
34] you're able to hold the concepts of 'No War' and 'Free Tibet' in your mind simultaneously 
35] you don't have a problem with minorities living wherever they want. Just so long as it's not next door to you 
36] you don't believe in bad people. Only bad choices 
37] you see bombs dropped by Democrats as good and bombs dropped by Republicans as bad 
39] you don't complain against the way the democrat President bungled the war on terror, but you gripe your head off with the eight year-old bill comes due at the beginning of the next guy's shift 
41] you see AIDS as a racist, anti-gay disease and you'd like to say so, but you know the world isn't ready for that much of a stretch. Yet 
45] you think that in an election, they should keep counting votes until your candidate wins 
46] you believe that criminals will be good citizens and turn in their firearms if the 2nd amendment is ever repealed 
53] you have a sneaking suspicion that your parents really love your sister's all-white children while only pretending to love yours. 
55] despite all the school shootings and teen suicides, you still believe that a child needs a village, not a home 
57] your typical reaction when someone hands you a gun is to recoil from it as if it were a snake 
58] you think there's a fairy-tale land called ' The Government' where all your tax-and-spend money comes from 
59] you're all for people working hard, sacrificing and enjoying the success that comes with it, just as long as it doesn't make those who didn't work as hard 'feel bad' 
61] you label the TV media 'conservative-biased' because one of them doesn't march to the bang of your drum 
64] you wholeheartedly support women's rights, so long as the umbilical cord is not attached 
65] getting down on your knees and begging for mercy is your primary means of self-defense 
66] you tell yourself that if people will buy gay marriage, "we can get them to swallow anything" 
67] the Principal at your son's school informs you that during a random search of his locker, they found guns, bomb making material and a plan to blow up the entire building. You first response is to accuse them of an illegal search and seizure 
68] the last time you had an orgasm was when O.J. Simpson was found not-guilty 
69] you secretly feel good inside when you learn that a criminal has been released on a technicality 
70] you believe that homeless people would rather work and that criminals would like to be rehabilitated 
72] your basic motto in life is "No rules, just right" because it works good for you 
73] you have no problem with Mexicans slipping across the border so they can have their baby here at the taxpayer's expense because... "freedom is such a fine thing, man" 
74] you raise cane getting the voting ballots in your state to contain 16 different languages, even though the first thing your own immigrant forefathers did when they got here was to learn to speak English 
76] you strongly support freedom of speech... as long as the person who's speaking thinks exactly like you 

IBen Getiner 


