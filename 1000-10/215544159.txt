



Pro-life Doctor Reveals Dark Side of Pro-abortion Pressure in Nicaragua 
Managua, Feb 16, 2007 (CNA)- One of Nicaragua's top pro-life leaders and the president of the Nicaraguan Association for Life, Dr. Rafael Cabrera, spoke up this week about the harassment and pressure the people and government are receiving from various international organizations and authorities to reverse its decision to outlaw abortion. 
Cabrera stressed the decision to outlaw therapeutic abortion was not fueled by the election battle, as some have suggested, but rather it was the culmination of a "10 year-long struggle." He said he was "offended by the intrusion from ambassadors and international organizations such as the UN, which are invading our sovereignty and threatening us with the suspension of economic aid if we do not give in to their whims." 
In response to EU official Marc Litvine's comment that it was "unforgivable" that Nicaragua did not debate the issue outside the context of the elections, Cabrera said his statement was one of "absolute ignorance and disrespect for the will of the Nicaraguan people," and he decried the attitude that "we should have to copy what other countries do. I cannot accept the statements of somebody who ignores this situation and ignores history." 
He went on to note that the attempt to make a distinction between abortion and "therapeutic" abortion is a sign of ignorance, as "abortion is the elimination of a living baby from the womb of the mother, period."  
Abortion is a "multi-million dollar industry," Cabrera stressed, related to "the issue of genetics, stem cells, the manufacturing of vaccines that are made from the tissue of aborted fetuses. And since Europe has begun imposing a few restrictions on genetic research, what better place to collect tissue for their research than here in the poor countries." 
Cabrera underscored that abortion providers are concerned about the law because "they are losing business," and he noted that "the example of Nicaragua is having repercussions in Latin America.  
Ecuador has already seen strong demonstrations, as well as the Dominican Republic. And so these international organizations say: we must sink Nicaragua so that the other countries do not rebel, so that the poor countries do not rise up and we can continue to dominate." 
Cabrera also denounced a "deceptive and manipulative report" by the Pan American Health Organization, which counted "non-therapeutic abortions in a study on therapeutic abortions," and thus violating "the Constitution, which says that Nicaraguans have the right to be truthfully informed." 
He warned that the PAHO "is dependent upon the World Health Organization and the United Nations, which are two organizations that promote abortion in the world. An organization that is supposed to be meant for the benefit of humanity is seeking how to prevent children from being born," Cabrera said. 


