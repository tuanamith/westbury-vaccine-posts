


 <URL>  
Texas lawmakers vote on cancer vaccine By APRIL CASTRO, Associated Press Writer 2 hours, 5 minutes ago 
AUSTIN, Texas - Texas lawmakers are fighting to block the governor's order requiring that sixth-grade girls be vaccinated against the virus that causes cervical cancer, with the House giving key approval to a bill to make the shots strictly voluntary. 
Gov. Rick Perry's executive order has inflamed conservatives who say it contradicts Texas' abstinence-only sexual education policies and intrudes into family lives. Some critics also have questioned whether the vaccine has been proven safe. 
The House voted 119-21 on Tuesday to approve a bill that would keep the vaccine off the list of required shots for school attendance. The measure was likely to get a final House vote Wednesday to send it on to the state Senate. 
The 119 votes for the bill Tuesday would be more than enough to override a veto by the governor. 
The vaccine protects girls against some strains of human papillomavirus, or HPV, a sexually transmitted virus that causes most cases of cervical cancer. A February report by the federal Centers for Disease Control and Prevention estimated that one in four U.S. women ages 14 to 59 is infected with the virus. 
Perry's order directed Health and Human Services Executive Commissioner Albert Hawkins to adopt rules to vaccinate all girls entering the sixth grade as of September 2008. Parents could have refused the shots for their daughters. Lawmakers said the governor circumvented the legislative process. 
The bill adopted Tuesday "will not take away the option for a single girl or a single family in this state to choose to vaccinate a child," said Republican Rep. Dennis Bonnen of Angleton, the lead author of the bill. "It simply says a family must make that choice, not a state government." 
The governor's office has estimated that only 25 percent of young women in Texas would get the vaccine if it is not mandatory. 
Critics also have argued that the vaccine, called Gardasil, was too new and its effects needed to be further studied before mandating it for Texas schoolgirls. The Food and Drug Administration approved Gardasil last year. 
Elsewhere, a New Mexico bill that requiring the shots for sixth-grade girls is expected to be signed by the end of this week by Gov. Bill Richardson, spokesman Gilbert Gallegos said. And Virginia Gov. Timothy M. Kaine has said he would sign a similar bill passed by his state's Legislature. 
Although the Wyoming Legislature recently rejected a request for $4 million specifically to fund HPV vaccination, the state's Department of Health intends to continue offering the vaccine to eligible girls with existing funding until the money run out. 
In other states, Massachusetts Gov. Deval Patrick's budget proposal, unveiled in February, proposed offering free shots in a voluntary program to all girls ages 9 to 18. A California Assembly committee on Tuesday put off voting on a bill that would require girls entering the seventh grade to be vaccinated against HPV. 



