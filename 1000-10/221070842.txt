



"sherry" < <EMAILADDRESS> > wrote in message  <NEWSURL> ...  <URL>  
From the above link: 
"Since meditation usually involves sitting quietly for a period of time and breathing deeply, anyone who cannot sit comfortably ........... may have difficulty practicing meditation." 
This is nonsense. I have been meditating for 40 years and I do it while flat on my back. 
Below is an interesting article I read recently. 
Enjoy 
RF Subject: The Anti-Aging Effects of Meditation 
Originally published in MORE magazine, May 2005. 
It's so unfair: Just at the age when you start figuring things out -- who 
you are, what you want out of life -- you begin noticing that you're losing 
your mind. Already, at 44, I not only forget where I left my keys, I get in 
the car and have no idea where I'm going. I'm worried that what's left of my 
poor brain is starting to petrify. Finally sure of my priorities and values, 
I'm cross with others who don't see things my way. I'm anxious about getting 
older, and impatient with my little aches and pains. When I look in the 
mirror, I see not just superficial wrinkles, but something deeper and more 
troublesome -- the nascent beginnings of a forgetful, cranky, inflexible old 
bat. 
Forget about anti-aging creams. I need something to reset my mental clock. 
Is there anything like exercise for the brain to keep it in shape? Studies 
have shown that doing crossword puzzles helps keep the mind sharp, but I 
doubt that struggling with 37-down, "Palenque king," can relieve the stress, 
anxieties, and mental rigidity that can accompany aging. Recently, alarmed 
at my brain's seemingly swift degeneration (not to mention my impatience, 
distractedness, and maddening forgetfulness), I decided to try a different 
kind of mental exercise: meditation. 
It seemed unlikely that simply sitting, closing my eyes, and focusing on my 
breathing could help. But after only a couple of weeks -- results are quick 
-- I was starting to believe that the best thing to keep my mind calm, 
cheerful, flexible, and focused is to do nothing, for 15 minutes a day. 
Meditation made me feel both relaxed and more energetic. I developed a bit 
of distance between events and my reactions. Someone cut me off in the car? 
Maybe he's having a bad day. A promising date didn't blossom into a romance? 
Perhaps it's his problem, not mine. Even at this early stage, I've noticed 
I'm much more able to let go of judgments of myself and others. I wondered: 
Can meditation really keep your mind young? And if your mind stays young, 
will your body follow? 
When I attended a daylong meditation retreat to strengthen my practice, it 
certainly appeared that way. The participants, mostly a decade older than I, 
radiated the kind of clear-eyed luminosity one associates with the bloom of 
youth. "I don't know whether meditation actually makes you younger," said 
the teacher, who, in his 60s, had no trouble sitting cross-legged for hours 
on end. "But it sure as hell makes you feel younger." 
It turns out that how you feel -- stressed or relaxed, anxious or calm --  
does affect the aging process. Recent research suggests that meditation and 
other forms of mindful relaxation may help slow down the biological clock, 
so you're better able to heal and to withstand disease. "There's a reason 
why experienced meditators live so long and look so young," says Eva Selhub, 
MD, medical director of the Mind/Body Medical Institute. That reason has 
mainly to do with reducing stress. Though there is little direct research on 
meditation and aging, one 1989 study of residents in nursing homes showed 
that those who practiced transcendental meditation had better mental 
flexibility and lower blood pressure, and lived longer. 
Stress = Aging 
Why? Researchers suspect that meditation slows down aging because aging is, 
in many ways, an accumulation of stress. The new thinking is that our cells, 
under stress, may stop regenerating as quickly, and become more prone to 
disease and early cell death. Meditation and other forms of deliberate 
relaxation also change the way you perceive stress, which actually lightens 
the physiological load. To some extent, age really is a state of mind: If 
you feel young, you're apt to be physiologically younger and healthier than 
your cranky peers. 
"If we can affect the stress response, we can affect the aging process," 
says Selhub. The longer we live, the more stress we're under, because 
stressful events are stored in our brains, Selhub continues, like icons on a 
computer, and each new anxiety triggers a lifetime's worth of anxiety, like 
double-clicking on that icon. The average woman over 40, who deals with 
work, kids, relationships, and her changing life and body, has about 50 of 
these stress responses a day. Selhub adds, "Without a lot of rest and 
recovery time between stress responses, we can age quickly." 
The good news is that several studies have shown that deliberate relaxation, 
or meditation, has exactly the opposite effect as the stress response, 
slowing and calming all those wiggy, whacked-out physiological changes. And 
while the stress response may be automatic and uncontrollable, the 
"relaxation response" can be called up at will, by just sitting and 
literally doing nothing. Meditation also teaches you to separate events from 
your reactions -- simply observing situations, without judging them, and 
then letting them go -- so that a stressful event might not automatically 
cause your brain to click on that icon of stored memories of stress. 
Other studies have shown that meditation can help strengthen the immune 
system and promote healing of illnesses that crop up as we age. Jon 
Kabat-Zinn, MD, founder of the Stress Reduction Program at the University of 
Massachusetts Medical School, whose most recent book on meditation is Coming 
to Our Senses: Healing Ourselves and the World Through Mindfulness, has 
shown that when patients with psoriasis listened to meditation tapes during 
light therapy, they healed four times faster than those who didn't relax. 
"The mind can affect the healing process right down to the level of cell 
division and cell replication," he says. Psoriasis is an uncontrolled cell 
growth -- not unlike cancer, so there is some potential that meditation can 
help control cancer, too, he theorizes. 
Rewire Your Brain 
In another study, Kabat-Zinn and his colleagues found that meditation can 
change the way the brain works. After an eight-week program, employees at a 
biotech firm showed increased activity in the left prefrontal cortex -- the 
side associated with feelings of happiness and well-being (Buddhist monks 
show the same kind of brain activity). The subjects who meditated developed 
more antibodies more quickly in response to a flu vaccine, signaling a 
stronger immune system. 
A calm, focused mind also improves memory and concentration. So much of 
forgetfulness has to do with multitasking, with your mind scattered in a 
million directions. "When you can't find your keys or remember a name, you 
have to ask yourself, how many things are you trying to pay attention to at 
once?" says Leslee Kagan, of the Mind/Body Medical Institute. Meditation 
teaches you to be mindful in the present moment, letting go of all those 
spiraling thoughts about the past, future, office politics, and the grocery 
list, and giving your mind and memory an opportunity to come into focus. 
"There's nothing as effective as some form of meditation for cultivating 
concentration," Kabat-Zinn says. "If exercise takes care of the body, 
meditation is what takes care of the mind." 
Meditation may also help us cope with the aging process. Studies at the 
Mind/Body Medical Institute have shown that women who did 15-20 minutes a 
day of some kind of meditative activity that produced a relaxation response 
reported a 58 percent reduction in premenstrual symptoms and significant 
decreases in hot-flash intensity, and 90 percent were able to reduce or 
eliminate use of sleep medications. Beyond reducing physical symptoms, the 
meditators had a more positive attitude about their body's changes, with 
fewer anxieties and negative thoughts. "With regular practice, relaxation 
techniques can substantially diminish one of the most problematic aspects of 
menopause -- our negative attitudes about aging," says Kagan, director of 
the Institute's menopause program. 
Over-the-Hill Illusion 
Meditation, says Kabat-Zinn, involves accepting things as they are, without 
judgment. "Acceptance doesn't mean passive resignation, like, 'Oh, well, I'm 
over the hill,'" he says. Instead you realize that over the hill is an 
illusory thought -- one which, if you identify with it, can affect how you 
feel about yourself. "If you think you're old, you can look in the mirror 
and find 100 different ways to confirm it," he says. "It becomes a 
self-fulfilling prophecy." Instead, meditation can take you out of the 
self-absorption with aging that can actually age you. "What we're talking 
about is attitude," he says. "Age is not so much chronological, but how 
you inhabit your body and your life in relationship to the world -- and that can be worked on. Meditation is really about reclaiming your life as if it were worth living now." 
In a society that often tells us otherwise, meditation can help us realize 
that aging is not such a bad thing. "There's a certain reflectiveness, 
balance, and perspective about the sorrows and the joys of life, which 
meditation can enhance," says my first meditation teacher, Sharon Salzberg, 
author of Lovingkindness and Faith. Salzberg, who looks much younger than 
her 52 years, says meditation keeps your mind young by keeping you 
constantly open to the present, curious and interested in the world around 
us. "That attitude, that sense of wonder and interest -- that daring -- is 
a product of meditation," she says. "Instead of feeling stuck in a self-image 
of being old, I feel like I'm getting younger all the time, in a sense of 
playfulness. What's important is not aging, but continuing to learn and 
grow." 
I'm a beginner, but after a few months of meditating, I feel less impatient, 
more relaxed, and able to concentrate more easily. I can meditate about a 
friend who is facing a serious illness and not spend the rest of the day 
panicked about her. Things on my desk get cleared away one by one, instead 
of haphazardly, between cups of coffee. I'm less likely to blurt out 
something in a fit of anger, or press "send" on a fuming e-mail. I'm also 
more apt to let go of grievances and disappointments. My mind may feel 
younger and in better shape -- but I also feel older, wiser, and more 
content. The research on meditation and aging is still in its infancy, but 
I'm convinced that meditation will indeed help keep my mind fit, flexible, 
and acute for the coming decades. 
Now that I've found the key, I'm in the driver's seat... 
Meditation 101 
There is more than one way to quiet the mind and elicit the relaxation 
response. Take your pick from among deep breathing, meditation, 
visualization, yoga, repetitive prayer and mindfulness (which includes 
deliberately focusing on the sensations of a small activity, like washing 
your hands or drinking a cup of tea). What's important is to set aside at 
least 15 minutes a day to engage in one of these activities. 
Even though it sounds straightforward enough, it can be difficult to begin 
meditating without some guidance (not to mention keeping up the practice). 
There are many books, classes, and tapes on meditation practice available, 
and you need to find one that's right for your temperament. 
Classes: The semi-annual journal Inquiring Mind (www.inquiringmind.com) has 
listings of retreats and sitting groups (it's meditation's equivalent of the 
book club). Or just Google "meditation sitting groups" for local 
information. 
Tapes and DVDs: Sharon Salzberg's tapes can be found at 
www.loving-kindness.org. Jon Kabat-Zinn has a series of guided meditations 
available at www.mindfulness tapes.com. Dharma Seed Tape Library 
(www.dharmaseed.org) sells live recordings from top teachers. This Web site 
links to Dharmastream (www.dharmastream.org) where you can listen to samples 
of meditation talks for free, so you can try before you buy. 
Books: Jon Kabat-Zinn's Coming to Our Senses: Healing Ourselves and the 
World Through Mindfulness explores meditation via the five senses. Also try: 
8 Minute Meditation by Victor Davich and Mindfulness in Plain English by 
Bhante Henepola Gunaratana. 
Originally published in MOREmagazine, May 2005. 





