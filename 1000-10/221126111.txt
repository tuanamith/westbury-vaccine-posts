


On Mar 4, 11:50 am, "pinknebulous" < <EMAILADDRESS> > wrote: 
Supported by research is not conjecture. 
So what do you suggest? Conduct a larger study -- but how large, considering the small percentage of people who suffer from the disease -- and wait another ten to fifteen years? 
How large a study? In order to get a decent number of positives, it would clearly have to involve hundreds of thousands or millions of more, in order to be able to get, within a human population (as opposed to animal models) a large enough sample to get you a statistically valid result. 
And within that population, you'd have to give half of them the real stuff and half of them a placebo, wait fifteen years -- and see if, say, out of a million, how many got the cancer in the half million control population and how many got the cancer in the half million vaccinated population. 
If you're going to do that (and I don't know that it would be remotely practical), you might as well just institute the vaccination and follow up and check on the cancer rates fifteen years later and see where you are. 
All of these things have up-sides and down-sides. 
If you have a reasonable basis for thinking that the vaccine works -- and you hold off for fifteen years for a wide-scale study, it means that thousands of people who might have lived will die. 
Clearly, there's also a downside -- there might be some unexpected and unanticipated effect that the vaccine has that nobody now can anticipate, and maybe it will crop up five years from now, tens years from now -- fifteen years from now -- or even fifty years from now. 
But that is true for every drug of every kind. 
Should we, in that case, have given the polio vaccine to a handful of people and literally waited until they all lived through their entire lives and died of old age, confirming that there were no unexpected effects right up until they all expired of natural causes? 
If not their whole lives -- how long, since we are talking about unkown and unanticipatable effects? 
Is ten years enough for an effect that you can't predict and have no way of anticipating? Twenty years? Thirty years? 
And all the while, kids are still suffering from polio. 
You have to weigh, on some level, amorphous concerns of unknown effects against the real concerns of the real effects of real diseases. 
Clearly, those possibilities are non-existent. You do take a chance. You can't be completely cavalier about these things. 
But somebody -- hopefully some disinterested party -- not the drug companies and not a state legislature that the drug company has in its pocket -- has to be able to meaningfully weigh those risks and benefits and come out with some reasonable balance between the two. 

Personally, I can't imagine why anybody would authorize a program that would immunize girls who'll suffer the consequences of the disease but not boys who'll be carrying and spreading it. It's bad medicine. 
Could anybody imagine the reverse, if men suffered the disease and women carried it and somebody said -- oh, we'll just let the boys get the shot and leave the girls who are going to be spreading it alone? 
NMS 


