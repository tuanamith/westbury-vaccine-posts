


Here's a two-punch health combo: Fasting on juices and cleansing 
The other day, a co-worker of mine boldly stated that she just wouldn't use pills anymore for an illness if she ever got one. "I'll probably allow vaccines, but that's it..." she added emphatically. 
This caught my attention considering that we share similar beliefs and in the course of our discussion it became apparent that now, most people are becoming more and more aware that fasting on juices and cleansing are two processes that go hand in hand in perhaps a more superior way than any other choice out there for not only improving and maintaining good health, but for dealing with just about any illness or malady. 
Now, it is quite obvious that both processes are closely linked and if used as an alternative or holistic method of cleansing the body, one could reap immense benefits including but not limited to a slimmer figure, brighter eyes, clearer skin, delayed aging and many more. 
There are several types of fasts that one could look into and they are each more or less effective and they include: 
1. The Water fast: This, admittedly, should be conducted with extreme caution or in a specialized sanitarium if undertaken more than 3 days. 
2. A Juice Fast or "The Lemonade Master Cleanser Diet" 
3. Fasting on Vegetable Broth 
4. A Restricted Mono-Meal of Seasonal Fruits Fast (more of a camouflaged fast) 
Now as far as juice fasting, from my experiences, you could try this recipe out for yourself as a means of cleansing: 
-Grade B Maple Syrup (Organic preferably) -Organic Valencia Oranges 
-Chilled Water (distilled, reverse osmosis or filtered) 
The basic recipe is this 
For the Sedentary 1.5 to 2 quarts of this juice (which is 3-4 oranges and 8-10 ounces of Grade B genuine maple syrup) should suffice as an excellent juice fast. For the more active, a maximum of 6 oranges and the Maple Syrup would prove to be more than sufficient. 
Also ensure that you: 
1. Get as Much Fresh Air as possible. 
2. Get as Much Rest as Possible 
3. DON'T use microwaves...at all. 
4. Avoid negative mass media and suggestions. 
5. Don't overexert yourself, at least not till you feel up to physical activities 
6. Don't break a fast with just any food...fruits are your best bet. 
So, if you are intrigued with this 2-punch health combo, suffice it to say that with the information provided herewith, you should be well on the path to attaining improved health, vigor and vitality with the tips above. 

Here's to health mates. 


Aje 


