


On Sat, 17 Feb 2007 11:51:27 +0000 (UTC), Bruce Barnett < <EMAILADDRESS> > wrote: 

Now let's see, you've admitted that the EPA newsletter is not a peer-reviewed journal and yet you're on about how they haven't published results of research and are using the lack of that newsletter, which is not the proper venue for reporting the results of research, as evidence that they are not reporting such results. 
This is called "circular reasoning" and is a logical fallacy. 
One would expect research results to be reported in peer-reviewed journals, not government newsletters. 

All of that sounds very dire however how do you know that the results in question supported global warming and were not related to, say, mercury in vaccines, or o-rings in solid rocket boosters? 

If you work for the government then you do what your boss says.  Same in industry.  That is the nature of the employer/employee relationship. 

Did your buddy Grifo mention the EPA specifically as one of those "multiple agencies" or do you have another source or are you just jumping to conclusions not supported by the evidence that you have presented? 
By the way, you're starting to sound like a broken record.  It used to be that if you repeated something often enough people would believe it.  Now they wonder what line of bullshit you're trying to sell them. You might want to consider revising your tactics. 

