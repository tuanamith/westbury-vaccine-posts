


In article < <EMAILADDRESS> >,   <EMAILADDRESS>  says... 
Yes, fighting OTHER DOGS. 

Instinct is part of it, but to say a dog's behavior is nearly all  instinct is showing you don't really know a lot about dogs. they have  personalities, just like we do, and if you don't think they think, you  would learn something if you met one of my dogs. The male is very  dominant, he sleeps more than any other dog I have ever had, and isn't  interested much at all with toys and even most kinds of food. He is  interested in other dogs, and cats. He plays very rough though, and  other dogs get scared a lot of the time. My other dog, a litter mate of  the first one, is totally opposite. She's food crazy, and the ball  hypnotizes her. She is scared of strange dogs and rules the back yard,  repelling other dogs, and usually killing any other 4 legged invaders.  When she gets into it with something like a Possum, she does 99% of the "work", the other one just sits back and watches.   
By your thinking any medium sized dog is dangerous. Even though there  are some bad ones, I'm not the slightest bit afraid of the neighbor's  normal sized, and giant Labrador Retrievers. I am leery around some  Rotts, if I don't know them. Same goes for Shepherds and the related  breeds. 

Exaggerating the danger quite a bit, IMHO. 


Again, exaggeration. It happens sometimes, sure, but there are thousands  of people out there who may get bitten by accident, and it's minor. Pits  and any other dog can and do control the intensity of their bites. When  one of the other dogs I had attacked the Pit Mix, he tried to get away  at first, but during the third attack (the attacker had suffered a  number of strokes and didn't seem to know who the other dog was), he  shoved the other dog, much older than him, up against the wall, and then  shook him slightly, and then just held him until the older dog stopped  struggling. The only damage to the old dog was a few bruises on his  neck, the skin wasn't even broken. He was totally depressed over losing  though. 




I saw it long ago, very interesting. 

Funny how Pits are the most likely to pass behavioral tests, isn't it? 
My friend has two Pits, one 9 years old, and the other 2, that sleep  with his 8 year old daughter. The older dog used to sneak into her room  at night when she was a newborn, and climb up on a table and get into  her crib without my friend and his wife knowing it. The dog got  "caught" one night when she didn't wake up and my friend found her  sleeping with the baby. "Everyone" told them that the dog would kill the  baby, either by suffocating it, or going crazy and chewing it up. After  a few nights of the dog whining, and the kid, who's entire vocabulary  consisted of mama, dada, and the dog's name, yelling it all night, they  cracked and let him back in, that was almost 8 years ago.  
My friend never has to worry about his daughter being kidnapped, both  those dogs worship her, and have proven that they will defend her  against strangers. 
Another friend had a huge Rottweiller who was great with kids old enough  to not fall down all the time. His wiggle used to make them "tip over"  when they were starting to walk. One kid was walking and the dog stepped  on his foot. He screamed, punched the dog in the face. The dog licked  him on the face, and started wagging the little stump tail.  

You probably couldn't, but I would take my chances with a Pit over a  Rottweiler or one of the bigger guard type breeds. My 75 pound dog was a  real handful at the vet, if he wasn't in a cooperative mood. Most of the  time, he was a very co-operative patient. Some things happened to him at  the vet that were pretty unpleasant. His reaction 99% of the time was to  just try to "bail out", to get away. 
One time the vet dripped a vaccine down his nose, and kept doing it,  even though my dog showed severe panic, and gave his usual 3 warning  growls that told anyone paying the slightest attention he was getting  pretty angry. Myself, the receptionist, and another person were holding  my dog down, and it was difficult. Just before the vet finished, my dog  did a fourth, teeth showing, very loud snarl. The vet finished, took a  step back, and we let loose of my dog, who lunged, and made a single,  probably full power snap at his face, right below his left eye. The vet  crashed into the counter, and my dog hit the floor.  
After the vet got his breath back, he said, "I shouldn't have ignored  the warning and kept going!" He said it was his fault, and he was right. "I could feel his lips and front teeth scrape my cheek!!" There was no grudge, and my dog got along great with him until the end.  
You have to have respect for something that can literally amputate  fingers, and can saw through 3/8" thick rawhide like you or I could cut  french bread with a knife. I used to show him to people eating that  rawhide, then give them a piece of it, and a pair of pruning shears, and  let them try to but it in half. Only a few of the stronger men could,  and it took both hands, and usually you would hurt your hands doing it. 

My dog never intentionally hurt me. He gave me head butts, piledrivers  to my elbow, and would slam into the back of my arm while holding onto  the steering wheel while driving, causing my wrist to bend way past the  usual amount. It hurt like hell. 
That's not to mention the bruises, cuts and scrapes he gave me over the  years. He used to have a talent for running into my thumb with his eye.  I would be in terrible pain, he would blink a couple of times, and wag  his tail. When he was older, you could see scars all over his eyes in  the right light. 
BDK  


