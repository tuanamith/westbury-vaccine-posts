


JoeSpareBedroom wrote: Evidently, (and sadly) it IS debatable.  The 9th Circuit Court (the most  liberal and overturned court in the land) has ruled that neither parents  nor students have rights when school officials decide what is right for  "their" children. 
The city of Palmdale, CA administered a sex quiz to students as young as  seven years old.  The students were asked to certain activities. Here  are some of the questions: 
8. Touching my private parts too much 17. Thinking about having sex 22. Thinking about touching other peoples private parts 23. Thinking about sex when I dont want to 26. Washing myself because I feel dirty on the inside 34. Not trusting people because they might want sex 40. Getting scared or upset when I think about sex 44. Having sex feelings in my body 47. Cant stop thinking about sex 54. Getting upset when people talk about sex 

The school refused to release students from activities such as these and  the parents sued.  Here is the opinion of the court: 
We agree, and hold that there is no fundamental right of parents to be  the exclusive provider of information regarding sexual matters to their  children, either independent of their right to direct the upbringing and  education of their children or encompassed by it. We also hold that  parents have no due process or privacy right to override the  determinations of public schools as to the information to which their  children will be exposed while enrolled as students. 

I won't speak for others, but this is another thing that I find  disheartening. 
Some states (not California yet, although it is being discussed) have  already ruled that as a condition for PS attendance female students must  take the HPV vaccine.  I would need to double check, but I believe Texas  is one of them.  The New York Times had written editorials stating how  it would be a good idea for NY.  HPV is ONLY transmitted through sexual  contact, and I personally find it appalling that any state could mandate  the taking of this new and relatively untested vaccine to my daughter.  Big Nanny government is, in my never so humble opinion, encroaching  deeply into my paternal rights. 
Glen 

