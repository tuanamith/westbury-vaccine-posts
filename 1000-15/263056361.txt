


Net surfers save 800 cats 
By Benjamin Kang Lim in Beijing July 09, 2007 09:59pm Article from: Reuters 
THE power of the internet has saved more than 800 cats from being skinned and served up on Chinese dinner tables. 
About 30 animal lovers rushed to a parking lot in Shanghai after reading an internet posting sparked by animal rights activist Huo Puyang that said two trucks carrying cats in wooden boxes had been intercepted, Ms Huo said today. 
Ms Huo's daughter-in-law had been looking for their missing pets and stumbled into the trucks, one of which sped away. The daughter-in-law called Ms Huo, whose animal-loving friends then sent out an Internet alert last Friday. 
The felines were on their way to the booming southern province of Guangdong, where some residents pride themselves as gourmets who will eat anything that flies, crawls or swims. 
"It was a cruel sight ... Pregnant cats and kittens were packed into the boxes," Ms Huo said. 
"Many cats had died and smelled," she said. "Some were trampled to death. Others bit each other." 
Ms Huo also telephoned police, who took the driver and the truck to a police station. 
Police said Ms Huo had lacked evidence to prove the 42 boxes held stolen pets and told the animal lovers to buy the cats. 
The driver demanded 14 yuan each. 
After hours of haggling, the animal lovers paid more than 10,000 yuan ($1509) for 840 cats. 
"We have a difficult task. The cost of feeding them pales compared to medical fees, vaccines and sterilisation," Ms Huo said. 
She called for donations and for other animal lovers to adopt the cats, which were initially being cared for at her shelter. 
Ms Huo's expensive act was another small victory of sorts in Chinese citizens' efforts to harness internet and cell phone technologies to mobilise around often long-stifled grievances. 
A slavery saga at brick kilns in the northern province of Shanxi came to light partly as a result of an Internet campaign conducted by the fathers of missing children. 
The Maglev train project between Shanghai and nearby Hangzhou was put under review after petitions by thousands of residents. 
Construction of a chemical plant in the southeastern port city of Xiamen was shelved after thousands of protesters received cell phone text messages that warned the plant would be the equivalent of an "atomic bomb" and threaten seaside environment. 
Internet censorship is common in China, where the Government employs an elaborate system of filters and tens of thousands of human monitors to survey its 140 million internet users' surfing habits, surgically clipping sensitive content.  

