


On Mon, 30 Jul 2007 16:43:30 +0100, "JOHN" < <EMAILADDRESS> > wrote: 

When a group attacks only the messenger rather than the message it is usually a pretty fair indication of the paucity of their case. Whatever you think of Deer had it not been for his widely acclaimed investigative work Wakefields activities would have remained hidden for longer. 

The case had been going since early 2005 and Wakefield had been stretching it out as much as he could to use as a threat against anyone else trying to research his activities (as was commented upon by the Judge).   
" (The) Claimant is seeking to take full advantage of the fact that he has issued libel proceedings while avoiding any detailed public scrutiny of the underlying merits. In other words, (it is argued), he is seeking to adopt a strategy comparable to that generally characterised by the phrase a gagging writ. It is necessary to consider these allegations in a little further detail... 
Matters do not rest there. It is suggested that there was a consistent pattern of using the existence of libel proceedings, albeit stayed, as a tool for stifling further criticism or debate... 
Again, one sees the same pattern. The Claimant wishes to use the proceedings for tactical or public relations advantage without revealing that they have been put on the back burner... 
 There was even an attempt on the Claimants behalf to restrict the Department of Health from supplying the public with such information as it thought appropriate. ... 
 I am quite satisfied, therefore, that the Claimant wished to extract whatever advantage he could from the existence of the proceedings while not wishing to progress them or to give the Defendants an opportunity of meeting the claims. It seems to me that these are inconsistent positions to adopt. This conduct is a powerful factor to be weighed in the exercise of the courts discretion in circumstances which are clearly unique. 
38. I have come to the conclusion, bearing all these considerations in mind, that the interests of the administration of justice require that the Channel 4 proceedings should not be stayed pending the outcome of the GMC proceedings. " 
It is worth reading the claims made against Wakefield and agreed between claimant and defendant which formed the basis of his libel case:- 
"    i) had dishonestly and irresponsibly spread fear that the MMR vaccine might cause autism in some children, even though he knew that his own laboratory's tests dramatically contradicted his claims and he knew or ought to have known that there was absolutely no scientific basis at all for his belief that MMR should be broken up into single vaccines; 
    ii) in spreading such fear, also acted dishonestly and irresponsibly, by repeatedly failing to disclose conflicts of interest and/or material information, including his association with contemplated litigation against the manufacturers of MMR and his application for a patent for a vaccine for measles which, if effective, and if the MMR vaccine had been undermined and/or withdrawn on safety grounds, would have been commercially very valuable; 
    iii) caused medical colleagues serious unease by carrying out research tests on vulnerable children outside the terms or in breach of the permission given by an ethics committee, in particular by subjecting those children to highly invasive and sometimes distressing clinical procedures and thereby abusing them; 
    iv) has been unremittingly evasive and dishonest in an effort to cover up his wrong-doing; 
    v) has improperly lent his reputation to the International Child Development Resource Centre which exploited very vulnerable parents by promoting to them expensive products the efficacy of which (as he knew or should have known) had no scientific basis." 
These are fatally damning criticisms of a researcher and not ones which could usually go unchallenged if the claimant felt there was any chance of success in pursuing them. 

Wellcome isn't a big drugs company? 

No, they had imagined it.   

Wakefield gave the impression to his fellow researchers that the subjects of his study were not pre selected.  Had he declared both his own substantial pecuniary interest and the fact that the small number of subjects were carefully selected for their symptoms and were already involved in litigation then his fellow researchers would have approached the analysis differently.  The bias he both created and hid skewed the results fatally. 

I understand you find truth to be inconvenient but the MMR case was refused funding by the Legal Services Commission in 2003 when it became abundantly clear that the claimants had no  worthwhile evidence to support their case and no possibility of winning it.  
"After taking expert advice, the LSC acknowledged that, given the failure of research to establish a link between MMR and autism, the litigation was 'very likely to fail' . Having already made the 'significant investment' of £15million in the claimants' unsuccessful attempt to demonstrate this alleged link, the LSC judged that it would be wrong 'to spend a further £10million of public money' in pursuing the case." 
That decision was subject to independent review by the funding review committee which supported it. The Funding Review Committee is an independent body, made up of solicitors and barristers from private practice. The Funding Review Committee that heard the MMR appeal consisted of a Queens Counsel barrister and solicitors who are experts in this area. 
An attempt was made to seek a judicial review and this was refused as there was nothing to review. 
An appeal was made to the High Court and this was also rejected. 
So far from one judge withdrawing the funding it was because of the clear evidence there was no chance of success.  

It's perfectly OK. A variety of complete harebrains such as Fudenberg received funding via Wakefield from the LSC, all in all over £15M was wasted and almost every anti-vaccine kook on the planet had their snout in the trough at some stage.  What isn't OK though is for a researcher, taking such money to prove a point, hiding that fact from colleagues and publishers and purporting to have no competing interests. 

No one needs to bother doing that - he has done it himself and appears not to want to stop:- 
 <URL>  

You do realise that St Wakefield said it quite obviously wasn't Thiomersal which was the cause (it wasn't used in MMR in the UK)? 
Which is it - Measles or Thiomersal?   --  Peter Parry.     <URL> /  

