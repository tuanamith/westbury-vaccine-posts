



 <URL> / 
  The Worst Health Money Can Buy By Kim Ridley, Ode.  July 7, 2007. 

 One of the biggest myths about health care is that more is always better. Perhaps nowhere is this more evident than in the United States, which offers a cautionary tale for other countries seeking health care reform -- including those in Europe looking across the Atlantic for inspiration. The American people spent nearly $2.1 trillion on health care in 2006 -- more than was spent on food -- yet Americans aren't exceptionally healthy or long-lived as a result. They have shorter life expectancies than people in Western Europe, Canada and Japan and are no less hindered by disease than their counterparts in other developed countries. 
In spite of all this spending, nearly 47 million Americans have no health insurance, according to the U.S. Census Bureau. And here's another irony: Although insured people often feel they receive too little medical attention, many are actually getting too much in the form of unneeded tests and treatments. This "overtreatment" is at the root of America's health care woes, according to medical journalist Shannon Brownlee, author of the upcoming book Overtreated: Why Too Much Medicine is Making Us Sicker and Poorer. Brownlee contends that up to a third of health care dollars in the U.S are wasted on unnecessary care that doesn't improve people's health -- and may even endanger it. 
A study of nearly a million Medicare patients (older Americans who receive government-funded health insurance) provides a compelling example of how too much care can cause harm. Medicare patients treated at hospitals that did the most tests and treatment and spent the most money were up to 6 percent more likely to die than patients at hospitals spending the least. In short, more spending, more hospitalization, more technology and more drugs do not necessarily equal better health care. 
Why do doctors and hospitals provide too much care in the first place? They are stuck in a dysfunctional system driven by money. Doctors get paid for how much care they deliver -- not how well they take care of their patients. Meanwhile, hospitals are pressured to recoup the expensive investments they've made in pricey technologies and specialists. This means the more care doctors and hospitals provide, the more money they make. 
Much of what doctors do is prescribe medications, some of which help save lives, like insulin for diabetics and cyclosporine for organ-transplant patients. But when it comes to medications, doctors are increasingly under the sway of drug companies. Brownlee observes that the pharmaceutical industry now foots the bill for at least 80 percent of clinical research (formerly funded by the federal government) and underwrites 90 percent of continuing medical education, wielding unprecedented influence over the content of medical journals and what doctors do and don't know about drugs. 
When money drives every aspect of health care, from doctors to hospitals to the pushing of dangerous -- and often inadequately tested -- drugs, what can be done? Brownlee finds inspiration and solutions in one of the most unlikely places: the Veteran's Health Administration (VHA). A horrific shambles in the mid-1990s, the VHA has been transformed over the past decade into a model of effective, affordable and humane care. Today the agency, which cares for military veterans -- including many of America's oldest, poorest and sickest patients -- outperforms most other U.S. health-care institutions at a little more than half the cost per person. The VHA's prescription-accuracy rate is 99.9 percent and it has a lower rate of hospital-acquired infections than most other health-care institutions in the U.S. 
In 1994, Kenneth W. Kizer took over the VHA as undersecretary for health. He introduced new information technology that helped lower rates of drug error and infections, while reducing unnecessary care. VHA doctors are encouraged to choose drugs carefully, based on scientific evidence rather than slick marketing, a step that has reduced costs and unneeded prescriptions. And patients report a high rate of satisfaction. 
The perfect system may not exist, but the VHA story suggests that effective, affordable and compassionate health care -- publicly funded, no less -- isn't a pipe dream, but an achievable reality. Patients, policymakers and anyone concerned about the future of health care around the world would do well to read Brownlee's book. 
Shannon Brownlee's book, Overtreated: Why Too Much Medicine is Making Us Sicker and Poorer (Bloomsbury), will be out in September. 


Kim Ridley is co-editor of "Signs of Hope: In Praise of Ordinary Heroes."  She writes about people creating positive social change for Ode Magazine. 


------------------------------------------------------- real problem with usa health care  [Report this comment] Posted by: allyourbasearebelongtous on Jul 7, 2007 5:49 AM     Current rating: 4    [1 = poor; 5 = excellent] 
As a mental health provider who has worked for the insurance companies and been in private practice, I can say that much of the problem with the cost of health care in this country is the large amounts of money being raked in as profits and extremely high levels of executive compensation by the insurance companies and by the pharmaceutical industries. That's where most of your excesssive costs originate from. It's not going to the providers of care.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
True, but not all the truth.  [Report this comment] Posted by: heid on Jul 7, 2007 6:30 AM     Current rating: 4    [1 = poor; 5 = excellent] 
The author has been forthright to a point, but is missing part of it. It's true that doctors make more by prescribing more and doing more procedures, but that certainly doesn't let them off the hook of responsibility for their actions. How can it ever be right to do harmful procedures and prescribe harmful drugs? 
But there's more to this story than that. The pharmaceutical firms and chemical firms and agribusiness and other corporate entities make money by making people sick, not by making them well. There is something obviously wrong with the free market system in healthcare. It simply doesn't belong there, as it benefits for making us sick, not for making us well. If a drug has side effects, then the drug companies make more money by selling drugs to treat the side effects. When they have long term bad effects, as in the neurological damage done by vaccines (or their preservatives), again the pharmaceutical firms and the hospitals and the doctors stand to make even more money for the ongoing treatment required. 
The bottom line is that the bottom line is the wrong way to run a healthcare system.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: True, but not all the truth. Posted by: Lincoln fan  
» Just scratches the surface............. Posted by: ekipnrut  
Delegalize medicine  [Report this comment] Posted by: edith on Jul 7, 2007 7:48 AM     Current rating: 5    [1 = poor; 5 = excellent] 
One reason for the overtreatment of patients is the fear of lawsuit. Certainly negligence happens, and under our current system of justice, civil lawsuit is often the only way a patient can be compensated for poor decisions by a doctor. On the other hand, the current system and indeed a public "universal" system will feature overtreatment and unnecessary costs as long as megamillion judgements against doctoras are possible and publicized. Already, it's hard to find a good doctor to deliver a baby, as gynecologists drop the OB part of their practice because of innumerable lawsuits when a birth isn't perfect. 
Some kind of arbitration panel is needed to compensate truly injured patients for actual lost earnings or capacity to earn. Beyond that, damages equal vengeance. Where the damages are under today's system "pain and suffering", there should be no monetary reward for the plaintiff. If a doctor continually makes unacceptable mistakes, the remedy should be ouster from the profession, rather than awards of "punitive" (deterrence) damages.  
We must have as a goal for universal health insurance accountalbility of doctors without the presence and goading of seedy lawyers and their solicitation ads.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: Delegalize medicine Posted by: Lincoln fan  
» RE: Delegalize medicine Posted by: wolfdaughter  
» RE: Delegalize medicine Posted by: JERSEYDAN  
The US needs Universal helathcare but not a single payer system  [Report this comment] Posted by: Swedish liberal on Jul 7, 2007 9:56 AM     Current rating: 1    [1 = poor; 5 = excellent] 
Sweden has a single payer system and it is now as the US system starting to malfunction. It is cheaper than the US system but is unable to give the care that Swedes want. Swedes have a greater demand for health services that the system can supply. Further the services is deteriorating because of rising costs that is unable to fund because Sweden can no longer afford tax increases, Sweden has the highest taxes in the world.  
The UK national health system is also crumbling because of its single payer status.  
The only viable system for the US, UK and Sweden is a mixed system after the example of France. France combines both private and public providers as well as combines private insurance with public funding. It is an extremely efficient system and the patients get good care in time but they have the option to pay for better care. However everybody has good care but some can afford better care without the least well off getting shafted. 
But the craze for a single payer system in the US is a fallacy but so is the system if today. However we will probably not see a change. Democrats want socialised medicine i.e. single payer and the Republicans do not so they oppose any changes in the system.  
I hope that the American people are wise enough to elect a leader without ideological blinkers that can create a system that combines the best of a single payer system and a private system. France is an as good a model as any, a start anyway.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: The US needs Universal helathcare but not a single payer system Posted by: Lincoln fan  
» RE: The US needs Universal healthcare but not a single payer system Posted by: Swedish liberal  
» The UK has a two-tier system now. Almost all countries do. So would the US. Posted by: janvdb  
» RE: The UK has a two-tier system now. Almost all countries do. So would the US. Posted by: Lincoln fan  
» RE: The UK has a two-tier system now. Almost all countries do. So would the US. Posted by: Swedish liberal  
» RE: The US needs Universal helathcare but not a single payer system Posted by: bob t  
» The Single Payer Aspect is the Appeal. It is the $400 billion saved that can cover the uninsured. Posted by: yellow  
» Only $400 billion? So if we get out of Irag now, repeal the tax give aways to the rich....... Posted by: johngary66  
Organized Medicine Doesn't Have Much to Do with Health  [Report this comment] Posted by: drricklippin on Jul 7, 2007 11:03 AM     Current rating: 5    [1 = poor; 5 = excellent] 
Finally an article in AlterNet on the myth that "more in medicine is always better". Believe me it is not! But it is typically American to think so. 
And even more egregious is that much of what US organized medicine professes to do to help patients has never been proven to be effective- or even worse in many cases causes harm.  
In short we have been duped and infantalized by a paternalistic so called "profession" who in recent decades, mostly out of greed and scientific arrogance, has become much more capable of diagnostic and therapeutic, often dangerous, misadventures. THANKS KIM RIDLEY! 
While you can wait till September for Shannon Brownlee's book, Overtreated: Why Too Much Medicine is Making Us Sicker and Poorer, you can now purchase and read two books written @ 30 years apart that changed my view as a physician of organized medicine forever. 
They are Medical Nemesis (1976) by Ivan Illich and The Last well Person (2004) by Dr. Nortin Hadler 
Hadler also writes a periodic health column for ABC NEWS on line 
Dr. Rick Lippin Southampton, Pa  <URL>   
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: Organized Medicine Doesn't Have Much to Do with Health Posted by: heid  
» RE: Organized Medicine Doesn't Have Much to Do with Health Posted by: drricklippin  
» RE: Organized Medicine Doesn't Have Much to Do with Health Posted by: JERSEYDAN  
» RE: Organized Medicine Doesn't Have Much to Do with Health Posted by: drricklippin  
The Unmentioned Elephant In The Room  [Report this comment] Posted by: NoPCZone on Jul 7, 2007 11:59 AM     Current rating: 4    [1 = poor; 5 = excellent] 
I have worked in healthcare for 20 years in public, military and private civilian hospitals and clinics and have seen more than I ever cared to of the waste and inefficiency the system has built in. I am for a single payer universal coverage system, but that alone will not contain the incredible costs of what passes for healthcare in the US. 
One the rarely mentioned facts is that the bulk of the money expended upon anyone's healthcare is used in the last 6 months-1 year of your life. Simply put, the majority of our resources are wasted propping up dying people who simply haven't stopped breathing yet. Does it really make sense to more money during the last 6 months of a 75 year olds life than was spend during the previous 74 1/2 years? 
Many of the 'miracle drugs' used commonly today do little more than buy a limited amount of time for people already on the glide-path to the mortuary, come with serious side effects and tremendous costs. Many of their remaining 'good days' are short-changed by the side effects of drugs prescribed to artificially buy them a couple of months or a few years of a heavily marginalized life. 
A huge re-think on what constitutes healthcare is needed or we will never manage costs effectively. It's not just defensive medicine, administrative overhead and greed that is making pay such a high price for such poor care.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» Sounds good in theory... Posted by: CatDad  
» RE: Sounds good in theory... Posted by: kellysgarden  
» RE: Sounds good in theory... Posted by: badkitty  
» RE: Sounds good in theory... Posted by: CatDad  
» RE: Sounds good in theory... Posted by: Lincoln fan  
» RE: Sounds good in theory... Posted by: CatDad  
» RE: Sounds good in theory... Posted by: NoPCZone  
» My Mother is 95. What if somebody had decided not to treat her 20yrs. ago? Posted by: johngary66  
» RE: The Unmentioned Elephant In The Room Posted by: JSquercia  
» RE: The Unmentioned Elephant In The Room Posted by: JERSEYDAN  
» And you get more "bang for your buck" with prevention Posted by: lb  
It would be really nice if more writers understood the scientific method  [Report this comment] Posted by: eridani on Jul 7, 2007 12:49 PM     Current rating: 4    [1 = poor; 5 = excellent] 
Medicare patients treated at hospitals that did the most tests and treatment and spent the most money were up to 6 percent more likely to die than patients at hospitals spending the least. 
Maybe they got more treatment because they were more seriously ill in the first place? 
This reminds me of the nonsense published about how sunscreens cause skin cancer. Sure, there is a correlation between sunscreen use and cancer, but could that possibly be because people who use it have far more actual sun exposure than those who don't use it (and whatever protective effect that sunscreen has is cancelled out by too much exposure to the sun)? 
Here's a clue for you--in a society with an adequate food supply, good plumbing, and antibiotics that cure most childhood illnesses. reasonably good health is the norm. You are somewhat more likely to get expensively sick than to have your house catch fire, but not by much. 85% of all health care expenses are incurred by 15% of the population for exactly that reason. 
We all pay taxes to pay for the fire department even though we are likely to never use their services because anybody might need them. That's exactly why we need single payer--shared risk.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: It would be really nice if more writers understood the scientific method Posted by: heid  
» RE: It would be really nice if more writers understood the scientific method Posted by: JERSEYDAN  
» RE: It would be really nice if more writers understood the scientific method Posted by: PirateJesus  
» It most assuredly IS the norm Posted by: eridani  
Book review articles should be subtitled accordingly  [Report this comment] Posted by: DaBear on Jul 7, 2007 3:50 PM     Current rating: 1    [1 = poor; 5 = excellent] 
AlterNet, get a clue! This is a book review not an informative article positing solutions! Your choice of title and subtitle is misleading.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
2 sides of the same coin.  [Report this comment] Posted by: ABetterFuture on Jul 7, 2007 7:19 PM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
The U.S. spends too much on healthcare because folks with health coverage look more towards maximizing their health coverage than they do to taking an objective, reasonable approach to their healthcare. 
15% of Americans are not covered by any health coverage whatsoever, partly because HMO's, insurance companies, etc are getting rich off of ignorant people over medicating themselves because they can. 
Health savings accounts aren't a panacea, but they do put people in charge of managing their health dollars, and thus they put people in the position of determining what is best for them, in their particular circumstance. They also offer the promise of bringing down health care costs in bulk by making providers compete for the services of informed, invested patients. Therefore, HSA's are one avenue to approach the two credible problems this author presents us with. 
HSA's aren't perfect, but no health care "solution" is--health problems do not invite "inherent" solutions. Hello?  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» Edit, with regard to VA healthcare: Posted by: ABetterFuture  
How do/can Corportist's attack common sense?  [Report this comment] Posted by: Bearzerker on Jul 8, 2007 12:55 AM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
Michael Moore isn't a "Brain" and I doubt thats hes much of a real live Genius either ... "he" is "just" a common man with loud ideals, simple common sense and [a hard fought & self built] medium to get his message out with, but everybody must see the logic behind [t]his latest argument... 
"Are we" actually afraid that people wont see through the planned/staged "BS" to divert the debate in this day and age? Come on people, we ain't reliving the dirty 30's... yet... 
"Their" brown shirts maybe out and about beating up little old ladies and preventing them from casting their votes but can they really expect to stop the landslide that they must know is coming their way? 
"Do they" really think its gonna make a difference this time around when their number one meal ticket, [The Rethugnicans] are most certainly gonna be wiped out of the house, senate and executive branches of power? 
"Whats there" plan when the Current crimminal organization in power starts boxn their DC offices up and moving to Canada to enjoy the good life while "the rest of team select" is left holding the bag. 
"The Corporatist manipulators" must have a plan...perhaps they've a pre-planted a "Smart LOBBY Bomb" in the DNC HQ that they're betting "Dubya's" ranch on?... while hoping that you'll bet "your" ranch on "there" bent vision [smoke screen] just in order to manipulate the debate... 
How "do they" expect any tactic of theirs to succeed especially now? ...with the current sway of public moods and perceptions... 18 months isn't much breathing room to manipulate when the cards currently being played are "bitches & spades"! 
"They are" as you probably already know, the most powerful lobby group/coaltion in Washington DC... the Medical/Pharma/Insurance coalition that has $$bought$$ every Rethugnican ticket since Nixon! 
FYI to all not currently tuned into the new reality... DO NOT, in anyway possible let them "TRY AND CONTROL THIS DEBATE" 
Take up our quarrel with the foe: To you from failing hands we throw The torch; be yours to hold it high. If ye break faith with us who die We shall not sleep, though poppies grow In Flanders fields. By John McCrae 
I'm just wondering if this generation is ready cause its their turn, the torch is passed? 
It smells like 1969 all over again... except without the music... wheres the music?... [oh yeah they've been taken over too] 
Keep up the great work Micheal ...as you know, the fights only JUST BEGUN... you've lit this torch that "we" the rest must carry and pass on... future generations are counting on it!  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: How do/can Corportist's attack common sense? Posted by: Lincoln fan  
» There is much to criticize about big pharma in America today. Posted by: yellow  
» RE: How do/can Corportist's attack common sense? Posted by: Appeal_to_Reason 

Free fall and the See-Saw Medical System  [Report this comment] Posted by: logansafi on Jul 8, 2007 11:48 AM     Current rating: 5    [1 = poor; 5 = excellent] 
As a nurse, that has worked at many a medical institution over 1/4 century time, it is obvious that the US has a see-saw 'system' of care, depending on payment source or lack there of. A patient can go from under tx to over tx and vice versa, in just a matter of seconds depending on payment source. At times, it's like watching 2 rambunctious kids on a see saw bump each other up and down. Have payment? Don't have payment? 
Both can be deadly, and many folk just try to stay away from the whole nasty mess as long as they can. That is the 3rd 'tx plan' in place. Over all, there is about as much 'health' in our US med system as there is 'security' in our military industrial complex. Over treated one sec, and onto the discard pile the next. Usually just ignored. 
Then you add to these 2 items our disintegrating educational system, and our burlesque political and legal systems, and we have an entire society in free fall. The med system cannot be reformed without all these issues being tackled together.  
Big is everywhere producing the rot that is destroying the nation. Monopoly corporations have become like a bunch of termites destroying the foundation of all society. They are the rot, not the solution to much anything. They certainly are not players in promoting public health.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» why I see you've mentioned two (more) of mah favorite diabetic 'extry sweet' chocolates.... Posted by: ekipnrut  
» RE: why I see you've mentioned two (more) of mah favorite diabetic 'extry sweet' chocolates.... Posted by: tally  
Safe Doctors  [Report this comment] Posted by: mike_burns on Jul 8, 2007 11:57 AM     Current rating: 3    [1 = poor; 5 = excellent] 
A few years ago, down in south Texas, the malpractice insurance became extremely high. The Doctors went on strike in protest. The death rate in that time period went down. Legalize Marijuana, and raw Opium. Much more than 50% of patients will stop showing up.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
New initiatives  [Report this comment] Posted by: Ambrose Pare on Jul 8, 2007 6:22 PM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
I think we need to make it profitable to cure diseases. 
Large sum cash payments, like bounty hunting, for cures. Whoever cures cancer gets $50 Billion dollars. Ect. 
It would spawn a massive effort of scientists and researchers. Pharmaceutical companies wouldn't have to do any of the nasty things they do now to maintain there market share. 
Human greed trumps all, we must exploit this for the greater good.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» RE: New initiatives Posted by: Appeal_to_Reason  
'Single Payer BS"  [Report this comment] Posted by: gellero on Jul 8, 2007 6:27 PM     Current rating: 1    [1 = poor; 5 = excellent] 
Perhaps you all should read a bit more about this 'Single Payer' (aka socialized medical ) system 
HERE'S THE SINGLE PAYER TRUTH  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
» Gallero, it's not socialized medicine. Just socialized insurence. Motherfuck insurence companies. Posted by: yellow  
» Socialized insurance?? Posted by: gellero  
» RE: 'Single Payer BS" Posted by: Appeal_to_Reason  
» The "document" you provided is a self-interested misleading document full of errors... Posted by: yellow  
Sign up and sign a pledge  [Report this comment] Posted by: Mamarianne on Jul 8, 2007 6:48 PM     Current rating: 1    [1 = poor; 5 = excellent] 
I have suggested in other posts that one way to insure more of our uninsured would be to lower the age of medicare to 62 since that systems--for all its flaws--works better none and is a real salvation for hardworking people after a lifetime of holding down the real jobs that hold this country together. Most of the people who sign up for medicare at 65 are still of sound mind (they'd better be considering the paperwork involved!). Most of the people who reach that age have begun to think of end of life issues, and I would venture to say that few want to be kept alive when there is no hope of regaining even a minimal quality of life. I have spent time with elderly people who were bed-ridden and blind, but could still enjoy a treat and laugh at the antics of grandchildren. There is still quality left to a life like that and lessons to be learned from these folks. But there are also terminal situations that come with age, and people are kept alive due to a lack of an advanced directive. Getting medicare should not require signing such a directive, but getting medicare could come with education about such directives and provision of a directive for each enrollee who desires one.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
End of Life Issues  [Report this comment] Posted by: gellero on Jul 8, 2007 7:42 PM     Current rating: 1    [1 = poor; 5 = excellent] 
They already have what you're talking about....it's called a 'living will'. But why should it be a quid pro quo for getting medicare?  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
United Health gave it's CEO Bill Mcquire over $1.6 Billion in stock options.  [Report this comment] Posted by: johngary66 on Jul 8, 2007 9:08 PM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
This is from the AFL-CIO Corp pay watch. McGuire was recently forced out in an option backdating scandal. He might lose a little when the SEC is finished. Not much though, it is George Bush's SEC. Estimated Annual Retirement Benefit: $5,092,000* 
*Calculated by The Corporate Library for the AFL-CIO Executive PayWatch 


UnitedHealth Group CEO William W. McGuire will receive an annual supplemental retirement benefit for his lifetime. If he retires at age 65, his pension benefit will equal 65 percent of his average cash compensation over his past 36 months of employment. This special pension benefit is part of McGuires employment agreement.[1]  


Ironically, this special pension guarantee probably will not be necessary for McGuire to have a secure retirement. On paper, McGuire is a stock option billionaire with $1,776,547,635 in unexercised stock options as of Dec. 31, 2005.[2]  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
On a more Philosophical Note...  [Report this comment] Posted by: yellow on Jul 9, 2007 1:11 AM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
I think that one of the reasons that Mr. Moore chose the health care crisis in the US today to be the topic of his new documentary is some of the philosophical implications that undergird all of his movies and general outlook. It could be termed what philosopher Norman Geras has termed the "basic solidarity of mankind" which consists of a kind of altruism which surfaces at critical moments when people have gotten fed up. They seem to act out spontaneously in utter revulsion against the outrages and irrationalities caused by social systems consisting of severe inequality. There is a deep, suppressed altruism that conservatives deny exists. It is repressed by the ideological hegemony of the unequal social systems. At crucial moments this altruism emerges out of a sudden moral compulsion that is the exception that proves the rule.  
People's cross culture support for universal health care that socializes the burden of caring for one another and rejects rugged individualism is something that is an ideological threat to conservatism. In so many countries people embrace this idea over the belief that healthcare is a basic universal right. If people can learn that principle with regard to health care then they can embrace it with regard to all else. Therein lies the real threat to the system. The basic solidarity of mankind is as learnable as individualistic cynacism.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
impeachment  [Report this comment] Posted by: gsaephanh on Jul 13, 2007 1:06 PM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
Call in your vote TODAY for impeaching Bush and Cheney at this number: 202-225-0100 
House Speaker Nancy Pelosis office is taking calls voting for Impeachment of Bush/Cheney at 202-225-0100. PLEASE CALL TODAY. At the toll free capitol switchboard #s below, you can also call your particular districts congressional representative to insist that they support impeachment for Cheney. E.g., for Rep. Dennis Kucinichs H Res 333 for Cheney; please say: 
In addition to supporting Kucinichs bill H Res 333, I would also support a similar Impeachment Resolution against Bush, especially after the disgraceful Scooter Libby sentence commuting and the following issues: wiretapping, torture, numerous 9/11 intelligence misrepresentations, the continued occupation of Iraq, gross negligence during Hurrican Katrina, the Valerie Plame CIA leak, [list your other grounds] ..[see resolutions on tab #2 for other grounds for impeachment]). 
LANIC requests that Americans call todayNot tomorrow or next week. Every call adds to the extraordinary grasswoots and nationwide movements pressures on House Speaker Pelosi to act now .before further innocent lives are lost in Iraq and elsewhere. Last week 28 Americans lost their lives. Over the July 4, 2007 weekend over 400 Iraqis lost their lives 
SEND MAIL TO HOUSE SPEAKER NANCY PELOSI: Attn: Nancy Pelosi, House Representative/Speaker of the House, 235 Cannon H.O.B., Washington, DC 20515 ; Pelosis Fax # 202 225-8259 
Pelosis e-mail address : 
 <EMAILADDRESS>  
CC her at:  <EMAILADDRESS>  
Please send her a pro-impeachment email and a specific call to endorse H Res 333. Note: On Saturdays/Sundays, Pelosis office has a comment line at which you can leave a voicemail. Your message will be transcribed and relayed to her. Please do encourage your family/friends to contact the same number. Refer them to www.bcimpeach.com for the actual telephone #s & contact info. 
Find out who your Congressional representative is and call that person. For toll free numbers to your Congress rep: (800) 828  0498; (800) 459  1887; or (866) 340  9281. You will be connected once you name your congress person. The staff aid should take detailed notes and provided to the Congressional representative. 
Final Note: Please say I support Impeachment based on ____. Id like to know where [representative name] stands on this issue. Lets strike while the Libby fury keeps the iron hot! Please call and Act Now! 
PLEASE ALSO CONTACT THESE KEY CONGRESSIONAL REPS RE IMPEACHMENT: Representative Capitol Phone Capitol Fax Howard Berman 202-225-4695 202-225-3196 & 818-944-7200 818-994-1050 
MAILING ADDRESS FOR BERMAN Congressman Howard L. Berman 14546 Hamlin Street, Suite 202 Van Nuys, CA 91411 
Henry Waxman 202-225-3976 202-225-4099 Loreta Sanchez 202 225-2965 202-225-5859 D. Watson 202 225-7084 202-225-2422 LindaSanchez 202 225-6676 202-226-1012 L. Solis 202 225-5464 202-225-5467 A. G. Eshoo 202 225-8104 202-225-8890 L. Roybal/Allard 202 225-1766 202-225-0350 
 <URL> /  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  
I Don't Think So  [Report this comment] Posted by: PatientX on Jul 15, 2007 2:22 PM     Current rating: Not yet rated    [1 = poor; 5 = excellent] 
I've always been treated much more humanely in the Dutch and British health care systems that I have in any system the US had to offer, private, insured, or governmental. VA health care? I'm a 100% service-connected disabled Vietnam veteran and I've been under it, and I do mean under, for over three decades. Several years ago a doctor at the Mather VAMC near Sacramento described me as crazy and delusional because I believed that I was in contact with scientists and had invented a fuel device that increased gas mileage. The book I co-authored with others, including scientists, involved in aerospace can be bought from any US book vendor, and my car with my "delusional" device on it just went 561 miles on a 12- gallon tank of regular gas in normal driving I do every day. The character assassinations and other abuses I've received (and believe me when I say that I am far from alone) at the hands of the VA health care employees have been incredibly damaging to both my health and well being for years. Please don't use the VHA as a model - you have no idea what you're talking about. Let's talk about real care - not money savings, because that's what the bottom line is, and the VA has a very long way to go to give authentic CARE to people - trust me on this.  
[« Reply to this comment] [Post a new comment »] [Rate this comment: 1 - 2 - 3 - 4 - 5]  

  2007 Independent Media Institute.  



