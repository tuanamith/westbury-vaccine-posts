


WHY IS CHILDHOOD ARTHRITIS ON THE RISE? 

Dr. Sherri Tenpenny, DO 
July 6, 2007 
 <URL>  

More children are suffering from arthritis than ever before. Unpublished  data released June, 2007 from a study conducted jointly by the American  College of Rheumatology and the American Academy of Pediatrics at the behest  of the Centers for Disease Control and Prevention (CDC) states nearly  300,000 children in the United States have significant arthritis. Dr. Brian  Feldman, chief of the arthritis program at the Bloorview MacMillan  Children's Center in Toronto states that this number is "probably an  underestimate" since it only takes into account those children who have  actually been diagnosed with arthritis. An Australian study confirms that  the rate of childhood arthritis is four to six times higher than rates  typically quoted.[1] 

Physicians are taught about 20 percent of children who are diagnosed with  arthritis go on to develop chronic disease. However, new research suggests  that the percentage is much greater than previously thought. Even when the  acute painful episode subsides or goes into remission, relapses often occur.  In a study of children who had arthritis isolated to one joint, called  oligoarticular arthritis, 60 percent went into remission. Of those, nearly  40 percent had reoccurrences.[2] 

Despite assurances by the Institute of Medicine and the FDA that vaccines  are safe and have no association with arthritis, a review of medical  literature suggests a different conclusion. 

Arthritis and the Hepatitis b Vaccine 

Reports of the association between the hepatitis b vaccine and arthritis  have been documented since 1990. At least two mechanisms of action have been  described. Inflammatory conditions, including joint pain, vasculitis (blood  vessel inflammation) or erythema nodosum (tender nodules), are thought to be  caused by immune complexes in the tissues. Complexes are a combination of  the viral antigen from the shot and the antibodies induced by the antigen.  If the particles reside in the tissues transiently, the condition eventually  resolves. Long term attachment of the complexes can lead to progressive  deterioration and residual disease.[3] 

Another mechanism proposes that severe arthritis, such as rheumatoid  arthritis, may be triggered by the vaccine if the person has a genetic  tendency toward an autoimmune disease and then is vaccinated.[4] Genetic  predisposition cannot be determined in advance of the vaccine. In essence,  this mechanism blames the bad outcome from the vaccine on the defective  genetics of the recipient. 

The hepatitis b vaccine has been recommended for newborns since 1991 and is  generally administered within the first 48 hours of life. The value of this  vaccination of should certainly be questioned. For example, an Italian study  touted a 46 percent reduction in the number of cases of hepatitis b in  adults, attributed to the vaccination of children. This seems like a  significant contribution to health until the study is closely inspected.  Then the significance of that report withers away. The study concludes that  the number of acute hepatitis b infections decreased from 5.4 per 1 million  persons to 2.9 per one million persons between 1990 and 1998. This was the  researchers arrived at a 46 percent reduction in disease. A similar study  from France boasted that the wide spread vaccination of hundreds of thousand  11 year-old adolescents could prevent 30 from contracting liver cancer as  adults.[5] This is how experts to promote universal vaccination of newborns. 

The World Health Organization denies the association between hepatitis b  vaccination and arthritis, claiming, "the medical literature comprises  mainly case reports, case series and a few case-control studies," reports  considered to be marginally scientific. Unless the numbers of individuals  are found to be statistically significant in proportion to the number of  vaccines that are given worldwide, clinically observed correlations are  dismissed. The Global Advisory Committee on Vaccine Safety (GACVS) concluded  in 2006 that there was "no convincing evidence to support an association  between hepatitis B vaccination and rheumatoid arthritis."[6] Making this  conclusion by Committee is an easy way to negate all clinical evidence to  the contrary, an easy way to dismiss the illness of an individual. 

Arthritis and the Rubella Vaccine in the MMR 

Acute arthritis following rubella vaccinations have been reported since  1972, the earliest use of the vaccine. All of the symptoms are lumped and  called an arthropathy, defined as any abnormality of a joint. The term  encompasses joint stiffness, arthralgia (subjective joint pain), and  arthritis (joint pain that is accompanied by swelling, redness, heat, pain,  and/or decreased range of motion.) Arthropathy after a rubella shot usually  occurs within 10 to 28 days and tends to appear suddenly. The joints  involved, in order of decreasing frequency, are fingers, knees, wrists,  elbows, ankles, hips, and toes.[7] The rubella strain used in today's  rubella vaccine, strain RA27/3, reportedly causes post-vaccination joint  symptoms in approximately 15% of recipients.[8] 

While most reports of arthropathy after rubella vaccination have most  commonly occurred in adult females, extremely painful joints have occurred  in children, reported as two different syndromes. The "arm syndrome" causes  severe pain in the arm and the hand, and tingling that is worse at night.  The "catcher's crouch" syndrome causes severe knee pain upon arising in the  morning. Both can occur within two months after rubella vaccination. As an  example, The Journal of Arthritis and Rheumatism published a report in  November, 2005 about eleven children who suffered recurrent episodes of  catcher's crouch after receiving a rubella vaccine.[9] 

Another type of painful condition was reported in 2004 involving an 11  year-old girl who was diagnosed with "complex regional pain syndrome type I"  (CRPS-I) after receiving a rubella vaccination.[10] CRPS-I, referred to as  reflex sympathetic dystrophy prior to 1994, is characterized by severe,  deep, burning, unrelenting pain without discernible nerve injury. Treatment  involves physical therapy, intensive psychological care to cope with the  debilitating pain, nerve blocks, pain medication infusion pumps. The  long-term prognosis for recovery is poor, meaning, the damage can be  permanent. 

As stated by Dr. Feldman, "The whole idea that 80% of children [with  arthritis] will have permanent remission is just wrong. Most of our patients  will have arthritis well into adulthood." Those with post-vaccination  arthritis are more likely to require substantial drugs to control the  progression of their disease, one that has no cure. While not every child  has vaccine-induced arthritis, there is a strong possibility that many  children can attribute their condition to the hepatitis b or rubella  vaccine. Those children have become customers of the pharmaceutical industry  for life as a result of vaccination. 

Footnotes: 

1, Childhood Arthritis Prevalence, Prognosis Eyed. Pediatric News. Volume  41, Issue 6, Page 36 (June 2007) 
2, Ibid. Pediatric News. June, 2007. 
3, J Sibilia, J F Maillefert. Vaccination and rheumatoid arthritis. Ann  Rheum Dis 2002;61:575-576 
4, Ibid. J Sibilia 
5, Ibid. J Sibilia 
6, Global Advisory Committee on Vaccine Safety. "Hepatitis B vaccination and  rheumatoid arthritis." 
7, Adverse Effects of Pertussis and Rubella Vaccines. The National Academies  Press. (1991) p. 187. 
8, Viral arthritis 
9, Spruance, Spotswood, M.D Chronic arthropathy associated with rubella  vaccination. Arthritis & Rheumatism. Volume 20, Issue 2, Pages 741 - 747.  (Nov. 2005) 
10, Hakan Genc, et al. Complex regional pain syndrome type-I after rubella  vaccine.European Journal of Pain. Volume 9, Issue 5, October 2005, Pages  517-520. 

© 2007 Sherri Tenpenny - All Rights Reserved 



