


bigvince wrote: 

Actually, its hugely important.  The form in which mercury takes plays a  huge role in its toxicity.  You cannot take the toxicity (mechanism, or  LD50) of one form, and assume its the same as other forms.  For example,  this is why your hero Boyd is considered a quack - he claims methyl and  ethylmercury are the same - in the face of overwhelming evidence that  the toxicity, retention, secretion, and mechanism of toxicity are  completely different for the two compounds. 



No, it was not.  The experiment used high levels of mercuric ions (100nM  to be exact).  To put this into perspective, assuming mercury spread  evenly throughout your body (which it doesn't, but this keeps the math  simple) to get to this dose would require that the average adult male  inject ~2 LD50's worth of mercury. 
That is not a small dose, that is a huge dose which would kill the large  majority of people it is injected into. 
As I stated before, what they did was exactly what you do when you're  starting to work out mechanisms of toxicity for any material - you hit  cells with high doses of the material and see what happens.  It gives  you an idea of potential mechanisms, and gives you an idea of what you  should look for as you move into more realistic concentrations. 



Which right away should act as a warning; alzheimers-like lesions are  not a symptom of mercury toxicity.  Which suggests one of two things: 
1) Snail neurons (which are what they used) respond to mercury  differently then humans, or 
2) The concentration used is not relevant to conventional mercury toxicity 
Given that no additional publications have been brought forth in this  area I can't even begin to comment on which of those possibilities are true. 



And, had you actually read the paper, you'd know that they were not  looking at low levels.  Permanent neurological damage occurs when brain  levels hit the range of 1-10 nM (i.e. 1/100th to 1/10th the dose used in  this study).  Lethal toxicity occurs at about the same dose used in this  study.  I'm unsure of how exactly you'd consider lethal toxicity to be a  "low dose". 
 <URL>   <URL>  



Actually, you specifically stated that thiomersal causes cancer, hence  why I replied to the statement.  Specifically, the reference was made in  your little cut-and-paste from wikipedia.  Do you not read the things  you post?  Here, I'll refresh your memory: 
Message ID:  <EMAILADDRESS>  "Thiomersal is harmful by inhalation and ingestion, lethal between 50  and 1000 times[citation needed][1] the usual intake (hazard symbol  T+).[2] It is a neoplastigen and a teratogen. Thiomersal is also  dangerous for the environment" 
Neoplastigen = cancer causing; hence, according to what you wrote,  thiomersal causes cancer. 
If you disagree with that statement, then why did you post it? 



The answer to which we've known for a very, very long time - a  resounding 'no'.  Thomersal breaks town into ethylmercury, which is both  highly stable, and readily excreted from our bodies.  And unlike the  more toxic forms of mercury (i.e. ionic, metallic and methylmercury) it  doesn't build up in neural tissues.  A small number of studies have  suggested that some Hg2+ may be released.  However, it doesn't act like  "pure" ions (i.e. its not retained like Hg2+ normally is), suggesting  that they're not seeing ions, but rather another (unidentified)  breakdown product. 
Numerous citations have been posted here in support of these  conclusions.  Myself, Mark Probert (sp?) and other frequently post them.    I've included one below. 



Hardly.  At the concentration tested, mercury causes damage to isolated  neurons.  No tests were done using animals, isolated pieces of brain, or  any other form of experiment which would have allowed neurological  damage to be assessed. 
Once again, a petri dish is not your brain.  You're taking discoveries  found in a dish, and extending them to the brain.  This is wrong - the  authors of the study did not do this; the closest they came was to say  "this *may* be the mechanism".  So if the authors are only that  confident in their findings, what special insight do you have that  extends their observations out of the petri dish? 



So?  That still doesn't guarantee that the mechanisms of toxicity are  the same.  Nor does it prove that growth cone loss is the mechanism by  which mercury is toxic within our brains. 
For that matter, "stable" neurons - i.e. ones which are not in the  process of growing (as in the bulk of our neurons), do not have growth  cones.  Which would lead me to hypothesize that growth cone toxicity (if  it occurs in our body) is only one (and probably minor) mechanism by  which mercury causes neurodegradation. 
It's also worth pointing out that other groups disagree with the results  of his studies, and instead believe that it is oxidative damage, not  cytoskeletal changes, which cause toxicity: 
 <URL>  
You'll also notice that like the above study, this paper used a high  dose of mercury... 


If you're going to quote someone, you could at least find someone who's  reputable.  All of Haley's theories in regards to thiomersal and  ethylmercury have been disproven - again, and again, and again.  Lets  not forget that he has ZERO RESEARCH PUBLICATIONS in regards to  thiomerisal toxicity.  Hell, you didn't even quote a scientific study,  but rather a court briefing WHICH WAS THROWN OUT BY THE VERY COURT IT  WAS PRESENTED TO. 
Apparently, you missed what the court said, after receiving Boyd's  testimony: 
"The courtfinds that Dr. Haleys report does not state an expert  opinion that thimerosal causes autism, rather just that he has a theory  about how such a thing could happen. At best, he expressed strong  belief that the cause of neurodevelopmental disorders in infants is  exposure to an organic-mercury compound such as thimerosal.  Additionally, Plaintiffs proffered the report of Dr. Lucier, who is an  expert in methylmercury and not ethylmercury, which is the substance in  RhoGAM. Dr. Lucier does not offer an opinion that methylmercury causes  autism, but rather that it may cause developmental disorders.  Significantly, the Court notes that neither Dr. Haley nor Dr. Lucier  asserts that he is an expert on autism nor are they offered as such. In  any event, the Court finds that neither of the proffered reports of Dr.  Haley nor Dr. Lucier are sufficiently reliable under Daubert on the  general causation issue because neither is relevant to the task at  hand. It would be an unacceptable scientific leap to suggest that they  serve as proof, by a preponderance of the evidence, of Plaintiffs claim  that the thimerosal in RhoGAM can cause autism." 
From:  <URL>  
So he self-admits that he is not an expert, the court itself  specifically stated that he does not qualify as an expert witness.  Gee,  great source for your information. 
One of my favorite bits from that particular court case was what was  said about Boyd's bestest-buddy, and greatest supporter, Mark Geier: 
"The Court has taken into account..the fact that Dr. Geier has  testified as an expert witness in about one hundred cases before the  National Vaccine Injury Compensation Program of the United States Court  of Federal Claims. It is noteworthy that in more than ten of these  cases, particularly in some of the more recent cases, Dr. Geiers  opinion testimony has either been excluded or accorded little or no  weight based upon a determination that he was testifying beyond his  expertise.In this case, subject to the Courts Daubert analysis" 
Ouch! 
So instead of quoting a known liar - an individual who's claims have  been throughly disproven by real scientists, and an individual with no  direct experience in the area - why don't you try quoting some  legitimate medical researchers who actually work in the field? 
<snippage> 

How nothing to do with zeal, but rather integrity.  I'm not hiding the  fact that thiomersal is toxic; you'd be hard pressed to find any  substance which isn't toxic at some level.  This paper also doesn't say  anything about thiomersal breaking down into ionic mercury,  methylmercury, or other more toxic forms.  So it hardly disproves  anything I claimed. 
Ironically, it was also a bit of a trap I set for you - too see if you  actually read and understand these things.  I'll give you credit for  reading the paper, unfortunately (thorugh no fault of your own; its  pretty technical stuff) you missed out on some of the more important  details. 
Lets look at what this study did: 
Cell type used: glioblastoma (brain cancer) Apopototic dose of thiomersal identified: 2.5-10uM 
So we have cells isolated from a brain tumor, and they saw apoptotic  effects at doses of 2.5-10uM.  Below that dose nothing happened.  What  does that translate into?  Well, it translates into about 2000x the  concentration of what is seen in the brain of a child after multiple  vaccinations given over a short period of time: 
 <URL>  
Now, don't forget that these people are looking at cancer cells.  What  happens if we look at normal neurons?  Well, someone's done that too: 
 <URL>  
As it turns out you get apoptosis and other signs of toxicity as well,  but you need a much higher dose of thiomersal (250uM, rather then  2.5-10uM); as in 100x the dose required to get toxological effects in  the tumor; hence, why thiomersal has been considered a potential cancer  therapeutic.  And hence why I posted this article as evidence that  thiomersal may be a potential cancer therapeutic. 
 <URL>  
<sarcasm> Strange, tumor cells are different then normal cells.  How  odd. </sarcasm> 
It all comes back to what I was talking about when I posted the  reference.  Upon the discovery by studies such as this one, that  thiomersal can kill cancer cells at levels which don't damage normal  cells, it was considered a potential cancer therapeutic.  Unfortunately,  the dose required is above the levels considered safe for use.  But  those studies do come back to the main point - at the doses used in  vaccines thiomersal is safe. 



I'll try not to sound too rude here, as this is one of those technical  things that people outside of the field would be unaware of.  That said,  you just completely undermined your own argument.  A little advice, if  you don't know what something is, don't try and use it in an argument. 
TAM67 is a gene which prevents cancer cells from dividing, or in other  words, is a gene we can add back into cancerous cells that makes those  cells non-cancerous.  It is also a commonly mutated gene in cancers -  i.e. loss of this genes normal function will cause a normal cell to  become cancerous.  The effect of putting TAM67 into a cancerous cell is  commonly used as a "goal post" of what an anti-cancer drug must achieve  to be effective. 

Here's an example:  <URL>  

So the study showed that adding TAM67 prevents thiomersal toxicity - or  in other words, if they made the cancer cells non-cancerous, thiomersal  was no longer toxic. 
Now what exactly does that say about thiomersal toxicity in  non-cancerous cells?  What does it say about the potential use of  thiomersal as a cancer therapeutic?  What does it say about thiomersal  toxicity in those of us who don't have brain tumors? 
And keep in mind that the levels of thiomersal they are using is very  high, far, far higher then what is seen in brains after vaccination -  even in young children. 



Yes, obviously you didn't, or if you did, you didn't understand it.  The  results of the paper were obvious - thiomersal kills cancer cells, but  not non-cancerous cells, at the concentration of thiomersal they used. 
What part of that don't you get? 
Strangely enough, this very paper is the most-cited paper by those  people who are testing thiomersal for the treatment of cancer.  For the  very reason that this paper successfully showed that the toxicity of  thiomersal is much, much higher in cancerous neurons then in  non-cancerous neurons.  It isn't the paper that sparked the interest in  thiomersal as a potential anti-cancer drug, but it represents the best  evidence that thiomersal may be useful as an anti-cancer drug. 


It's "Bryan", with a 'y'. 



Hardly, but I'm sure you'll go on believing so, as to actually think  about the data would show that the paper demonstrates exactly what I  claimed it does. 



Hey, you brought it up.  If you didn't want to talk about cancer then  maybe you shouldn't have typed the word "neoplastigen".  Although the  irony that you're complaining about me disproving one of your points is  duly noted. 



Really?  So kids have cancer in their brains?  I'd argue that this  paper, which used neurons from infant mice, would be a much more  accurate model as it is using real neurons and not cancer cells: 
 <URL>  
In case you're confused, this is the paper I referred to above, the one  which showed that thiomersal-induced apoptosis occurred at thiomersal  levels 100x those used in this study. 
And you were pretty selective in what you posted, what about these parts? 
"studies will be needed to determine if either of these events is  occurring in response to thimerosal since SK-N-SH cells treated with the  mercury-containing compound appear unable to successfully process the  toxic insult and thus proceed down an apoptotic pathway" 
Or, in other words, the cancer cells they used don't respond the way the  rest of our cells do in response to mercury.  Strangely enough, that  throws into doubt any conclusions you make about non-cancerous cells, as  the cancer cells they used as a model for neurons don't behave the same  as real neurons exposed to mercury. 
Or how about: "Our studies are the first to show that proapoptotic Bim levels are  increased in response to thimerosal treatment and are decreased when  cells were pretreated with SP600125 but not when transfected with TAM67" 
Yep, the evidence shows that if you make the cell non-cancerous,  thiomersal becomes non-toxic at the dose used.  Why does it become  non-toxic?  Simple - the very apoptotic pathway that caused the cancer  cells to apoptose doesn't work in non-cancerous cells. 



Bad news for you buddy.  Not only have I read this paper, but it makes  up the foundation of a research project we are starting here in our lab.    Not only am I intimately familiar with this paper, their methods, and  their readouts, but I've actually repeated some of their experiments  (using different types of cells, mind you), and we've asked this group  for some of their gene constructs. 
If you're wondering, we're not looking at thiomersal; but rather the  signaling pathway they've discovered.  Regardless, the pathway they have  identified is very interesting - if it exists in non-cancerous cells. To  date we've only been able to replicate their results in a few  cancer-cell lines, so this may be a cancer-specific signaling pathway. 
Now, you want to try going down this road again?  Perhaps you'd like to  pull up a paper where they show toxicity in a cancer cell derived from a  monkeys kidney, and claim that its proof that thiomersal is neurotoxic  (that's one of Johns favorite tricks)?  Or perhaps you want to pull up a  few papers on methylmercury toxicity, and try to argue that there is  some relevance to thiomersal (Boyds favorite trick?  Or perhaps you'd  like to degrade things all the way back to the old arguments about  molecular mass and toxicity? 
Or perhaps you'd like to continue to argue that cancer cells and neurons  are the same thing? 
Or perhaps you'd like to delve even deeper into my area of expertise; my  entire PhD thesis is on MAP-Kinase signaling (JNK is a MAPK).  I'd be  more then happy to blow holes in your claims vis-a-vis JNK-mediated  thiomersal toxicity, JNK mediated apoptosis, and all other things JNK... 
Bryan 

