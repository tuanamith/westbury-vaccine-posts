


Sorry this may show twice.... 
 <URL>  
	 Science lab suspected in foot and mouth outbreak 

· Research plant is near infected farm · Ban on export of British livestock 
Jo Revill, Juliette Jowit and Anushka Asthana Sunday August 5, 2007 The Observer 
An accidental leak of an experimental vaccine from a private research  site was being investigated urgently last night as the likely source of  Britain's new foot and mouth disease outbreak. The news came as the  government attempted to avert a full-scale crisis in farming and the  tourism industry. 
Movement of all livestock has been banned, exports to Europe stopped and  country fairs cancelled to minimise the risk of the country suffering a  disastrous rerun of the 2001 foot and mouth epidemic which cost the  nation £8.5bn. 


Scientists made a breakthrough last night as they identified the strain  of the virus as one which is not naturally occurring, but is a vaccine  strain, and has never been seen before in Europe. This enabled  investigators to link the outbreak to a company which lies less than  three miles down the road from the source of the outbreak. 
Merial Animal Health, a private pharmaceutical firm shares facilities  with a government laboratory in Pirbright, and is commissioned by the  European Union to formulate new vaccines for animal diseases. Both  companies are expected to meet tight regulatory standards for biosecurity. 
Investigators are now focusing on whether there was a lapse which meant  that a batch of the vaccine, made last month, escaped the site. The  company is believed to test its vaccines on animals, which may have been  able to graze on the land. The virus may have been carried by the wind,  or by people or vehicles down the road from the site to a rented field  in the village of Normandy, near Guildford, where the outbreak happened. 
Bobbie.. 

