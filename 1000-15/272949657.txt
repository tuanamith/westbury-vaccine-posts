


AJH wrote: I'll not argue against that. F&M is about as serious in cattle, once  they've recovered from/ survived the initial infection, as chickenpox is  in people, from what I've read. It allegedly results in a slight to  moderate loss in rate of growth in livestock, making it a commercially  sensitive disease. It's just a lot more contagious than chickenpox. 

As I understand it, the loss of status is permanent. It's a bit like  being a virgin, if I can use that analogy here. Last time, the decision  to vaccinate would probably have done exactly as you said, but after  vaccination is done, there's no easy  way to prove that the antibody  reaction in the livestock is against the vaccine, not a real infection,  so other countries (Or, rather, the meat importers in other countries)  no longer accept our meat as F&M free, no matter what the rules say. Not only that, but how do you decide when to stop vaccination? 
Am I allowed to rant a little bit about Ramblers here?;-) As for commercially acceptable, would my memory of Irish meat exports  using ferry routes that avoided England, Scotland & Wales during the  last outbreak be right? 
You make a couple of very good points that I agree with totally. 
Tciao for Now! 
John. 

