


"But the problem with using immunology to fight cancer is that the immune system works by recognizing and repelling foreign invaders such as viruses and bacteria. Cancer is the body's own cells gone haywire." 
 <URL>  Friday, February 24, 2006 
40 years ago, this Swedish couple pioneered cancer immunology 
By TOM PAULSON 
Did you hear the one about the two Swedes who walked into a Seattle lab about 40 years ago and helped launch the field of modern cancer immunology? 
Drs. Karl Erik and Ingegerd Hellstrom, now in their 70s and still active in research, have bounced around since they first came here in 1966. Part of this was due to circumstances beyond their control, but it also may have been due to a Scandinavian stubborn streak that kept them focused on trying to answer scientific questions few thought worth pursuing. 
"Lots of famous people wrote big articles saying tumors are not recognized by the immune system," said Hellstrom in his sing-song, Swedish accent ideally suited to ironic emphasis. "We were not so famous, but we thought maybe they were wrong." 
His wife smiled, noting that most experts now take it for granted that the immune system is involved in cancer. But it wasn't that long ago, she said, that this was considered heresy. 
The Hellstroms were recruited to the University of Washington from the Karolinska Institute, which awards the Nobel Prizes, when many scientists were looking at the interaction of the immune system and cancer. The couple published some of the seminal discoveries in the field. 
They were one of the first scientific teams to produce monoclonal antibodies, specialized immune system cells that can be designed to attack cancer by recognizing a specific tumor protein known as an antigen (or "anti-yen," as Hellstrom tends to pronounce it). 
The Hellstroms developed new analytical tools and methods in immunology that have advanced the science. They helped start the Fred Hutchinson Cancer Research Center and continued to publish findings on immunology and cancer. 
But the problem with using immunology to fight cancer is that the immune system works by recognizing and repelling foreign invaders such as viruses and bacteria. Cancer is the body's own cells gone haywire. Most scientists eventually decided to abandon the concept of harnessing the immune system to fight cancer. 
"The bottom just fell out of it," said Ingegerd Hellstrom. The couple believed in what they were doing, but couldn't get funding or institutional support for their research. In 1983, they left academia to work in industry with a biotechnology firm (now long gone) called Oncogen. 
When that company was bought out and moved from Seattle, in 1997, the couple found refuge with a non-profit organization, the Pacific Northwest Research Institute. Last year, the institute decided to focus on diabetes and the Hellstroms again had to move. 
"We didn't know what to do," said Hellstrom. "We felt kind of obsolete." 
But a like-minded colleague at the University of Washington who has long recognized the value of the Hellstroms' contributions came to the rescue. Dr. Mary "Nora" Disis, a leader in the search for cancer vaccines, helped arrange for them to find a new home in the research building at Harborview Medical Center. 
"They're still quite productive," said Disis. So the Hellstroms are still at it. The couple met, married and had children while in medical school and seem just as excited today about their work as they must have been while young medical students. They refuse to reveal how old they are. 
"We live in the present," said Karl Erik. "In doing science, one can be timeless." 


