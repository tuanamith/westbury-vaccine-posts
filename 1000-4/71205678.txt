


Bird flu likely in US flocks soon: Health Secretary Wed Mar 1, 2006 4:17 PM ET 
By Richard Cowan 
WASHINGTON (Reuters) - The lethal avian flu that is spreading rapidly  around the world could soon infect wild birds and domesticated flocks in  the United States, U.S. Health and Human Services Secretary Michael  Leavitt said on Wednesday. 
In testimony to a congressional panel on his agency's budget for  combating a possible avian flu outbreak among humans, Leavitt told  senators that no one knows when or if the virus will pose a threat to  people. But, he said, "it's just a matter of time -- it may be very  soon" when wild birds and possibly poultry flocks contract the disease. 
Leavitt said that infection of birds alone in the United States with the  H5N1 virus would not create a public health emergency. Such an emergency  would occur if the disease mutated so that it became easily transferred  from human to human. 
The H5N1 disease so far has killed 94 people in seven countries. 
Nevertheless, Democrats on the Senate Budget Committee criticized the  Bush administration's preparedness, saying not enough federal funds were  being allocated for vaccine production, stockpiling other medical  supplies, disease detection and community readiness. 
"It could be the disaster of our time. Two billion dollars is not  enough," North Dakota Sen. Kent Conrad, the senior Democrat on the  committee, told Leavitt. 
Conrad was referring the $2.3 billion in additional emergency funds the  Bush administration has requested from Congress. Late last year, Congress  approved a first injection of more than $3 billion in emergency money. 
While U.S. poultry flocks have suffered from isolated cases of highly  contagious avian flu, they have not yet been hit by the virulent H5N1  strain that has killed or led to the culling of about 200 million birds,  mostly in Asia, since late 2003. 
Recently, the animal disease has been found in western Europe and worries  escalated this week when German authorities said avian flu may have  jumped species and killed a cat. 
CONTACT WITH SICK BIRDS 
So far, human fatalities have largely been limited to people who have had  close contact with sick birds. 
Leavitt told the committee that by the end of this year, the United  States will have about 20 million doses of anti-viral drugs, mostly  Tamiflu, stockpiled. 
But the development of a vaccine is three to five years away, Leavitt  said. He downplayed chances that this timetable could be accelerated  significantly and added that even with vaccine technology, it would take  drug companies six months after the start of a pandemic to produce an  effective one. 
"In the first six months of a pandemic we are dependent on basic public  health, social distancing; every business, every school, every church,  every county to have a plan," Leavitt said, adding, "We are overdue (for  a pandemic) and under-protected, but we are moving with dispatch." 
Leavitt also was skeptical that the federal government could provide all  localities with the full arsenal of basic medical equipment, such as  ventilators, masks, gauze and gloves, needed during a pandemic. That  surprised Senate Budget Committee Chairman Judd Gregg, a New Hampshire  Republican, who said he had thought the billions of dollars being spent  would cover such stockpiles. 
Instead, Leavitt put the responsibility of local preparedness mostly with  local officials. 
--  
Risk taking leads to greatness if well managed. 
Me        
Jonathan Swift 

