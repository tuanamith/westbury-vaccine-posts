




.... In Nigeria polio cases exploded [Nigeria now accounts for more than half the world's polio victims] and 18 countries previously declared polio free were re-infected, all with virus originating from Nigeria. Now that vaccinations have restarted the fall-out is still being felt. 
.... In parts of northern Nigeria more than 50% of the children have never been vaccinated against polio, ... 
For more than a year from mid-2003 the northern states stopped the vaccination programme altogether after rumours swept the country that the polio vaccine caused AIDS and that it was all part of a western plot to sterilise Muslim girls. 
Many still believe these scare stories and its one of the main reasons why people still refuse to vaccinate their children 
.....Nafiu Baba Ahmed, Secretary General of the Supreme Council for Sharia in Nigeria, has 16 children and none of them have been immunised against polio. 
"There are greater risks than polio," he says. "I think either this is an imaginary thing created in the west or it is a ploy to get us to submit to this evil agenda." 
The suspension of the immunisation programme was a disaster for those like the World Health Organization trying to eradicate polio..... 


*********** ********** ********* ********* FULL TEXT ****** ***** **** ****** ******* 
 <URL>  

Nigeria's struggle to beat polio By Andrew Bomford BBC, Nigeria 

In a small house in Nasarawa, a district of the northern Nigerian city of Kano, a mother mops the brow of her 18-month old boy called Osman. The child has a fever and is crying. 


Two days earlier he'd been diagnosed with Acute Flaccid Paralysis after he lost the use of one of his legs. Tests are expected to confirm that he too has polio. His mother Jamilah says he's never been vaccinated because her husband refused to allow it. 
"Please," she says, "don't let him become a cripple." 
But it is probably too late. There is no cure for polio. 
Outside the house it is easy to see how Osman contracted polio. Like many parts of Nigeria, there are open sewers running along the streets. 

Children are everywhere playing games in the streets and running through the sewers. Polio is passed on through faeces. For every paralysed child there are 200 carriers. 
On the rise 
In most parts of the world three or four doses of polio vaccine, administered as a small baby, are enough to provide protection. 
But in Nigeria there is so much polio virus around that children under the age of five have to be immunised over and over again. 

Even seven or eight times is not enough. This is very difficult to achieve. 
Rates of non-compliance are high, and even though vaccination rounds are done several times a year children keep getting missed. 
There have been some successes though. 
In Nigeria polio is now largely confined to just eight states in the north of the country. But there the numbers of victims are rising, not falling. 
Confirmed cases grew from 781 in 2004 to 801 in 2005. So far this year there have been 31 new polio victims and tests are awaited on 55 other suspected cases. 
Nigeria now accounts for more than half the world's polio victims. 
Training 
In the classroom of a dilapidated school in Kano in the north, mothers sit at the desks, being trained in how to be a vaccinator. 
A whole army is needed for the vaccination rounds, as teams of women go from door to door immunising children. They appear to know their stuff. 


"It doesn't harm the child - it has no overdose," says one mother. Another chips in: "It's a booster, to boost the immunity. Even if they take it a thousand times it won't harm them." 
The rest of the class applaud. 
In parts of Muslim northern Nigeria, only women can enter households if the husband is not present, so all the vaccinators are women, and they are paid a small amount of money for their time. 
In parts of northern Nigeria more than 50% of the children have never been vaccinated against polio, and often their parents refuse to cooperate because of mistrust and suspicion. 
For more than a year from mid-2003 the northern states stopped the vaccination programme altogether after rumours swept the country that the polio vaccine caused Aids and that it was all part of a western plot to sterilise Muslim girls. 
Many still believe these scare stories and its one of the main reasons why people still refuse to vaccinate their children 
Doors marked 
Nafiu Baba Ahmed, Secretary General of the Supreme Council for Sharia in Nigeria, has 16 children and none of them have been immunised against polio. 
"There are greater risks than polio," he says. 

"I think either this is an imaginary thing created in the west or it is a ploy to get us to submit to this evil agenda." The suspension if the immunisation programme was a disaster for those like the World Health Organization trying to eradicate polio. 
In Nigeria polio cases exploded and 18 countries previously declared polio free were re-infected, all with virus originating from Nigeria. Now that vaccinations have restarted the fall-out is still being felt. 
At six in the morning the vaccination teams start to assemble. They grab cold boxes and vials of vaccine are passed from battered old refrigerators to the boxes. 
Slowly, amid the noise and the chaos, they move off heading for their designated areas. 
On the streets they move from door to door, checking everywhere, and putting chalk marks on the walls to indicate where children have been vaccinated and where parents have refused. 
They'll be revisited later in an attempt to win them over. Children who are immunised get a black mark on their little finger. 
Sweeps 
Most parents seemed to welcome the visitors and understand the need to vaccinate over and over again. But many were tired and harassed by the constant visitations. These sweeps take place several times a year. 
One mother who had taken her children to the local health centre to be immunised refused to cooperate this time. 

"I saw a poster in the health centre which said children need to be vaccinated four times," she said. "My children have been vaccinated four times. Why do you people keep coming round month after month?" 
Other parents ask why the concentration on polio? 
"What about malaria?" they ask. "Malaria kills far more people than polio." 
In other places there were problems with the vaccinators themselves. Many are not properly trained and some can't write. 
Hardest battle 
They are supposed to record immunisations and refusals on paper, but often when the paperwork is returned a 100% success rate is recorded, which experts say is impossible. 
In one area there were large numbers of children out on the streets and almost all of them had not been vaccinated. 
Later when the paperwork for the same area was checked, the team had claimed they'd successfully immunised almost all the children. 
But WHO is confident that Nigeria will soon turn the corner and that the numbers of polio cases will start to decline later this year. 
In world history only one disease, smallpox, has ever been completely eradicated. 
Eighteen years ago, a deadline to get rid of polio was set for the year 2000. 
We are now six years too late and the experts are finding that the last few skirmishes of the battle are the hardest ones to fight. 
Story from BBC NEWS:  <URL>  
Published: 2006/03/31 07:12:02 GMT 


