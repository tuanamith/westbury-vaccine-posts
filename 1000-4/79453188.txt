


March 20, 2006 E. Russell Alexander, 77, Public Health Expert, Dies By Jeremy Pearce, NY Times 
Dr. E. Russell Alexander, an epidemiologist and public  health official who helped curb deadly disease outbreaks and  raised concerns about federal plans to thwart a potential  swine flu epidemic in the 1970's, when a vaccine was linked  to a neurological disorder, died on Feb. 26 at his home in  Seattle. He was 77. 
The cause was islet cell carcinoma of the pancreas, his  family said. 
Dr. Alexander was a member of a committee on immunization  practices that was advising the Centers for Disease Control  and Prevention when swine flu was first reported at Fort  Dix, N.J., in 1976. 
The outbreak caused public health officials to fear a  national epidemic. More than 200 soldiers were mildly  infected with the influenza A virus, and 13 were  hospitalized. Dr. Alexander advised caution and argued for  the stockpiling of flu vaccine, without embarking on  vaccinations until it could be shown that the flu had spread  beyond Fort Dix. But the committee decided to move quickly  to cut off potential contagion, and about 40 million  Americans received the vaccine. 
A small percentage of those treated developed Guillain-Barré  syndrome, a poliolike disorder that causes paralysis, and  some patient deaths were linked to the vaccinations. 
The devastating consequences of a flu epidemic did not  occur, and the effort to use the vaccine nationwide was  halted by the end of 1976. 
Dr. Harvey V. Fineberg, president of the Institute of  Medicine at the National Academy of Sciences, said that Dr.  Alexander "posed very important questions about how to  proceed with swine flu, and they turned out to be the right  questions." 
With Richard E. Neustadt, Dr. Fineberg wrote a book, "The  Epidemic That Never Was: Policy Making in the Swine Flu  Scare" (1983). As a result of his prudence, Dr. Alexander  received "wide respect by knowledgeable leaders in public  health," Dr. Fineberg added. 
In 1993, while serving as chief of epidemiology for the  Seattle-King County Department of Public Health, Dr.  Alexander and others headed off a food-borne epidemic that  killed four children. Within three days, investigators  traced E. coli bacteria back to improperly cooked beef  served by a fast-food chain. 
The tainted food was withdrawn from the market, in an  "example of timely action that prevented many more cases of  illness," said James L. Gale, an emeritus professor of  epidemiology at the University of Washington. 
Dr. Alexander, who trained as a pediatrician, was interested  in preventing the spread of sexually transmitted diseases.  He studied the association between genital herpes and  cervical cancer in women, and the neonatal infection of  infants by mothers carrying the trachoma organism, a leading  cause of blindness in children. 
Edward Russell Alexander was born in Chicago. He received  his undergraduate and medical degrees from the University of  Chicago. 
He joined the federal Epidemic Intelligence Service in 1955,  which is part of the Communicable Disease Center, and served  there until 1960, ultimately as chief of its surveillance  section. The center would eventually become the Centers for  Disease Control and Prevention. 
From 1961 to 1969, Dr. Alexander was an assistant professor  of preventive medicine and pediatrics, and then an associate  professor, at the University of Washington. He was named a  professor and chairman of Washington's division of  epidemiology and international health in 1970 and remained  until 1979. 
From 1980 to 1983, he was a professor of pediatrics at the  University of Arizona. After returning to the Centers for  Disease Control, from 1983 to 1989, Dr. Alexander moved to  the Seattle-King County Department of Public Health, and  also held a professorship at the University of Washington.  He retired in 1998. 
Dr. Alexander was president of the American Epidemiological  Society in 1986, and was named president of the American  Venereal Disease Association in the same year. 
Dr. Alexander is survived by his wife, the Rev. Mary Jane  Francis, an Episcopal priest. Two previous marriages ended  in divorce. 
He is also survived by four daughters, Ann Alexander of  Seattle, Kay Alexander of Lake Forest Park, Wash., Bess  Carter of Fall City, Wash., and Eva Alexander Rice of  Denver; a stepson, Bill Levitch of Renton, Wash.; and by  five grandchildren. 






