


 <URL>  
 Laid low by Lyme vaccine 
The rotavirus recommendation is not the only controversial call made by the CDC. Another involves a vaccine to fight Lyme disease, a tick-borne illness that can cause profound fatigue, headache, fever and severe muscle pain. 
"It was after the booster shot that I absolutely collapsed," said Lewis Bull, a farmer from East Lyme, Conn. Bull, now 49, volunteered in 1996 to take shots during a clinical study for a new vaccine to prevent Lyme disease developed by SmithKline Beecham, now GlaxoSmithKline. Clinical studies are tests on humans to make sure vaccines are safe and work before going on the market. 
In the study, Bull first received placebo shots containing no vaccine and felt fine. 
But soon after his second shot of the real vaccine he began to suffer from debilitating arthritis, memory loss and fatigue. Some doctors believe the Lyme vaccine side effects mirror the disease itself. 
"For the first six months I could not get out of bed. The memory loss was incredible. I've played guitar all my life and I could not remember how to play guitar. I could not find the town hall and I used to go there four times a week," he said in a recent telephone interview. 
Bull said his fatigue was so severe he would sleep for stretches of 22 hours or more. Without medical insurance, Bull was forced to sell his farm. 
On Feb. 18, 1999, the CDC endorsed Lyme disease vaccine for people age 15-70 who work or recreate in possible tick-infested areas. 
By October of 2000, more than 1.4 million people had received the vaccine, according to the CDC. 
But 19 months later, in February 2002, SmithKline Beecham pulled the vaccine off the market because "sales of LYMERIX are insufficient to justify the continued investment." 
The company also faced hundreds lawsuits by people who said they suffered side effects, many similar to Lewis Bull's. 
Although he never sued, Bull said he complained to the CDC to report what he says were obvious side effects from the vaccine, called LYMERIX. 
The government's database of possible side effects for LYMERIX lists 640 emergency room visits, 34 life-threatening reactions, 77 hospitalizations, 198 disabilities and six deaths after people took the shots since the CDC endorsed it. 
According to CDC meeting transcripts where the advisory committee weighed its recommendation, five of 10 committee members disclosed their financial conflicts of interest with vaccine manufactures. Three of the five had conflicts of interest with SmithKlineBeecham. 
The committee ignored a plea from a consumer advocate to delay a recommendation on LYMERIX because it might not be safe, according to a February 1999 transcript. 
"We are just saying there is a wealth of information out there that is different than the information you have been provided. I think the honorable thing to do would be to wait," said Karen Vanderhoof-Forschner, founder of the Lyme Disease Foundation, a patient's advocacy group that eventually opposed the vaccine. 
UPI found that the CDC and SmithKline Beecham worked together on a Lyme vaccine. A 1992 CDC activity report obtained by UPI says the agency had an agreement "with SmithKline Beecham that currently funds three positions at (the CDC) for the purpose of providing information of use in developing advanced test methods and vaccine candidates." 
In June 2001, the General Accounting Office delivered a report to Sen. Chris Dodd, D-Conn., on this issue. It says that CDC employees "are listed on two Lyme-disease related patents" including "a 1993 joint patent between CDC and SmithKline Beecham Corporation." The report also said that six of 12 consultants working for the CDC on Lyme vaccines "reported at least one interest related to a vaccine firm." 


