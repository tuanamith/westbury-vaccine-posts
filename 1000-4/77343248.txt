



Rich wrote: 
It did not jump species. The 1918 Swine Flu was just that. 
1: Philos Trans R Soc Lond B Biol Sci. 2001 Dec 29;356(1416):1845-55. 	Related Articles, Links     Click here to read     The haemagglutinin gene, but not the neuraminidase gene, of 'Spanish flu' was a recombinant. 
    Gibbs MJ, Armstrong JS, Gibbs AJ. 
    School of Botany and Zoology, Faculty of Science, Australian National University, Australian Capital Territory 0200, Australia.  <EMAILADDRESS>  
    Published analyses of the sequences of three genes from the 1918 Spanish influenza virus have cast doubt on the theory that it came from birds immediately before the pandemic. They showed that the virus was of the H1N1 subtype lineage but more closely related to mammal-infecting strains than any known bird-infecting strain. They provided no evidence that the virus originated by gene reassortment nor that the virus was the direct ancestor of the two lineages of H1N1 viruses currently found in mammals; one that mostly infects human beings, the other pigs. The unusual virulence of the virus and why it produced a pandemic have remained unsolved. We have reanalysed the sequences of the three 1918 genes and found conflicting patterns of relatedness in all three. Various tests showed that the patterns in its haemagglutinin (HA) gene were produced by true recombination between two different parental HA H1 subtype genes, but that the conflicting patterns in its neuraminidase and non-structural-nuclear export proteins genes resulted from selection. The recombination event that produced the 1918 HA gene probably coincided with the start of the pandemic, and may have triggered it. 
    PMID: 11779383 [PubMed - indexed for MEDLINE] 
    Gibbs MJ, Armstrong JS, Gibbs AJ. 
    PMID: 11779383 [PubMed - indexed for MEDLINE] 
1: Rev Med Virol. 2000 Mar-Apr;10(2):119-33. 	Related Articles, Links     Click here to read     Influenza A pandemics of the 20th century with special reference to 1918: virology, pathology and epidemiology. 
    Oxford JS. 
    Academic and Retroscreen Virology (www.retroscreen.com), Department of Medical Microbiology, St. Bartholomew's and The Royal London School of Medicine & Dentistry, London, UK. 
    Influenza A virus initiated worldwide epidemics (pandemics) in 1918, 1957, 1968 and 1977. A revised calculation of the 1918-1919 pandemic estimates that 40 million persons died and 500 million were infected. The mortalities in 1957 and 1968 were nearly 6 million. Biological and genetic characteristics of the causative agents of the more recent pandemics, have been well studied but little is known about the causative agent of the Great Pandemic in 1918. Genetic characterisation of the 1918 virus has been achieved by sourcing virus RNA from formalin fixed lung samples or by exhuming frozen victims of the outbreak from Arctic regions. Initial analysis of the HA gene from two USA sources indicates a virus related to swine and human influenza with no base insertion at the HA1-HA2 cleavage junction which, at least in avian influenza A, characterises high virulence. Important unanswered questions are whether the 1918 virus spread pantropically perhaps to include the brain and hence cause encephalitis including the later lethargic forms, or whether infection was confined to the respiratory tract. Re-examination of reports of respiratory disease in England and France in 1916-1917 may indicate a non-Spanish origin of the pandemic and a period of 2 years for the virus to be seeded worldwide. In contrast the other two pandemic viruses in 1957 and 1968 appeared to originate in Asia. New anti-neuraminidase drugs in conjunction with amantadine and novel developments with influenza vaccines would be expected to ameliorate the disease in a future pandemic. Copyright 2000 John Wiley & Sons, Ltd. 
    Publication Types: 
        * Historical Article 
1: Virus Res. 2004 Jul;103(1-2):107-13. 	Related Articles, Links     Click here to read     The NS1 gene of H5N1 influenza viruses circumvents the host anti-viral cytokine responses. 
    Seo SH, Hoffmann E, Webster RG. 
    Division of Virology, Department of Infectious Diseases, St. Jude Children's Research Hospital, 332 North Lauderdale, Memphis, TN 38105-2794, USA. 
    The H5N1 influenza viruses transmitted to humans in 1997 were highly virulent, but the mechanism of their virulence in humans is largely unknown. Here we show that lethal H5N1 influenza viruses, unlike other human, avian, and swine influenza viruses, are resistant to the anti-viral effects of interferons and tumor necrosis factor alpha The nonstructural (NS) gene of H5N1 viruses is associated with this resistance. Pigs infected with recombinant human H1N1 influenza virus that carried the H5N1 NS gene experienced significantly greater and more prolonged viremia, fever, and weight loss than did pigs infected with wild-type human H1N1 influenza virus. These effects required the presence of glutamic acid at position 92 of the NS1 molecule. These findings may explain the mechanism of the high virulence of H5N1 influenza viruses in humans and provide insight into the virulence of 1918 Spanish influenza. 
    PMID: 15163498 [PubMed - indexed for MEDLINE] 
 Display 



