


 <URL>  Agriculture Minister Boim admits tainted poultry was mishandled 
By Amiram Cohen, Haaretz Correspondent, and Haaretz Service 
Agriculture Minister Ze'ev Boim admitted Tuesday that allowing fowl to rummage in the carcasses of dead poultry suspected of having contracted the disease was a mistake, which has since been remedied. The fear arose that the fowl would then transmit the disease to other, remote areas. 
Meanwhile, the Palestinian Authority on Tuesday declared a state of emergency due to the avian flu outbreak. 
"A mistake occurred, and from now on we are systematically culling and burying the birds," Boim said at a meeting of the Knesset's Economic Committee. He assured members of the committee that the flu is not spreading, but has been confined to six known locations. 
The Agriculture Ministry's general manager, Yossi Ishai, said that the culling of poultry in known affected areas will be completed Tuesday. Culling in surrounding areas will be done with by the end of the week. 
The shortage of manpower needed for this operation was solved through the retaining of 8 Defense Ministry contractors, each with a staff of 20. The government allocated NIS 5 million for the additional manpower. 
The Agriculture Ministry stated Tuesday morning that the initial mishandling of the bird flu occurred because of a difficulty in submitting the culling orders, which must be handed personally to henhouse owners. 
"Our farmers' database is lacking and since we could not destroy property without first contacting the farmer, it was impossible to begin the culling on time," said a ministry official, adding that this obstacle has since been overcome. 
Dr. Avi Israeli, general manager of the health ministry, said that he would not be surprised if one or two people contracted the disease, although none have so far. He stated that even though this is not currently a human epidemic, there are enough vaccines for 25% of the population. 
Police cars to be stationed at entrances to affected areas Police cars are to be stationed at the entrances of all moshavim and kibbutzim in which poultry are being destroyed as part of efforts to check the spread of avian flu, authorities announced Tuesday. 
Two squad cars are to be deployed at all such farming communities to aid Agriculture Ministry officials in enforcing the quarentine over these areas, a government task force decided late Monday. 
Agriculture Minister Ze'ev Boim told the Knesset Tuesday that officials are finding it difficult to find people willing to work as part of the containment campaign. 
"There are people that are unwilling to enter poultry houses, even if they are offered large sums. They are apprehensive" that they might fall ill, Boim said. 
"The atmosphere that has been created, one of panic, further deters prospective employees, he said. 
Ministry officials confirmed that their inspectors have caught two trucks trying to smuggle chickens from Moshav Sde Moshe near Kiryat Gat to a slaughterhouse. Sde Moshe was one of the sites at which all poultry has been ordered destroyed. 
The officials said they could not rule out the possibility that other attempts at smuggling had succeeded. 
PA declares emergency over bird flu The PA on Tuesday declared a state of emergency in hope of preventing the spread of the fatal H5N1 bird flu virus, which struck Israel last week. 
"The government has declared the state of emergency," outgoing Palestinian Prime Minister Ahmed Qurie told reporters ahead of a cabinet meeting. 
Azzam Tbaileh, a top Palestinian agriculture official, told Reuters that there have been no cases of H5N1 bird flu so far in Palestinian areas. 
"The state of emergency means we will use all our resources and all our abilities to prevent bird flu from reaching our areas and to deal with (it)," Tbaileh said. 

--  
regards Jill Bowis 




