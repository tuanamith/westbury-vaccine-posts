


      Bangladesh Vigilence against entry of indian hindu poultry 
      Bird Flu       Strict border vigilance to prevent entry of Indian poultry       Pinaki Roy 
      The government has asked the Bangladesh Rifles and police to  strengthen vigilance on the borders to ensure that no poultry products and  pet birds enter into the country from India, where lethal avian flu virus  H5N1 has been found in poultry. 
      Following the bird flu virus outbreak in Maharashtra state of India,  the government yesterday called an emergency inter-ministerial meeting where  poultry industry leaders were also present. 
      The meeting asked concerned officials to increase surveillance and  decided not to import any poultry chicks from France where virus H5N1 was  found recently. 
      Earlier, the government decided not to import chicks from some  European and Asian countries including India where low and high pathogenic  avian influenza virus was found. 
      Since early 2003, the H5N1 strain has killed at least 91 people,  mostly in Southeast Asia. The virus can infect humans in close contact with  birds. 
      There is, however, no evidence still now that it can pass from human  to human. 
      The recent bird flu virus outbreak in India poses a direct threat to  the country's poultry industry as neither the government nor any private  organisation has bird flu vaccines. 
      "We do not have any flu vaccination facilities in our country. We do  not know what will happen if the flu starts to spread here," said eminent  poultry vaccine expert Md Monzur Murshid. 
      "The government should have taken the issue seriously and arranged  vaccines," he added. 
      Pharmaceutical company Roche has imported only 3,000 doses of Tamiflu  vaccine, which is used as a precaution against bird flu among humans. 

      As suggestion to the people, Dr Monjur Murshid said, "It needs at  least 70 degree Celsius temperature to kill the influenza virus. If the eggs  and poultry meat are properly cooked, there will not be any problem for  human body." 
      There are some 150,000 poultry farms in Bangladesh, one of the world's  most densely populated countries. 

      "Bird flu outbreak in India is a threat to us. But we are careful  enough so that the virus does not spread in our country's poultry sector,"  said Md Abu Siddique, Secretary General of Bangladesh Poultry Owners  Association. 
  



