


Title: This Fragile Universe chapter Seven (WIP) Author: Neoxphile Rating: R. Keywords: MSR; alternate universe (literally and figuratively) 
* trouble finding the prologue or other chapters? Get them here: www.geocities.com/mulderscreek/thisfragileuniverse.html 
** ** 
Chapter Seven - Fading Fast 
Mulder was in a pretty good mood when he returned to Scully's from visiting the other side. Scully looked happy to see him, but faintly nervous too. Eventually she disappeared into her room, and when she came back she wasn't empty handed. 
"Mulder, I have something for you." Scully held out a small white box, tied with a yellow bow. When he took it, she urged, "Open it." 
"My birthday isn't until October." He reminded her. 
"Mulder! Open it!" 
"Okay, okay." Deciding to tease her a little, he slowly worked the knot to release the bow, then carefully pried the tape off the paper with his fingernails. She was beginning to look impatient, so he didn't fool around with the tissue paper. 
Once the gift was revealed, he stared at her, dumbfounded. It was a shirt that would have fit on Samantha's dolls, and emblazoned on it was the sentence, "If you think I'm cute, you should see my Daddy." 
She smiled at him. "I found out that I should have gotten a second opinion a few weeks ago." 
"The IVF worked?" 
"Apparently." 
"Oh my God, Scully!" He threw his arms around her. "We're going to be great parents." 
"I hope so." 
"Do you know if it's a boy or girl yet?" He asked eagerly before shaking his head at his own question. "No, of course not. It's too soon." 
"My new OB said we can find out in another eight weeks or so." 
"This is so great. I really wanted this for you..." He trailed off and gave her a loving look. "I wanted it so badly for me too, but I didn't want you to know if we never were successful. But we were, I can't believe it." 
"Yeah." She said, snuggling against his chest. "It's enough to make you believe in miracles, isn't it?" 
"Hey, I never claimed to be an atheist." Mulder objected. "I never said that miracles don't happen. You putting up with me as a partner this long is a miracle too." 
"Quit with the self-deprecating humor and just be happy with me." Scully commanded. 
"I am, I already am." 
** 
As he hugged her tight, she was glad that she'd nixed her initial idea of presenting him with two baby t-shirts. Although she was hopeful that the babies would both be brought to term, she knew as a doctor that twin pregnancies didn't always result in two babies at the end, especially when twins were detected this early. 
With luck her next ultrasound would show them both two babies more than a third of the way along in their development. He had a lot to worry about already, and she didn't mind shouldering the secret for a few more weeks. 
** 
Two Days Later 
Though James claimed not to be a king or even a man of much consequence, he still had a great deal of clout. Working round the clock, the vaccine was mass produced. Scully could scarcely believe that it was possible to create a supply that quickly, but her doubts had been laid to rest, at least to a degree, by the speed in which vaccine enough to test on Dana and Fox had been produced. And, Scully reflected, it wasn't as though they needed to vaccinate millions, just two or three thousand. 
Within days people were being warned to go to visit their local hospitals for an inoculation. Of course, people thought they were being protected against something particularly nasty and viral, like Small Pox, not a disease that would eat you alive then burst out of your body and attack people. 
Both Scully and Mulder helped out at the Chilmark hospital, mostly directing people where to go. After a few hours, Mulder tapped her on the shoulder. "You should go to Samantha's for a while. She said you're welcome at any time." 
"Why?" 
"Because you look dead on your feet. Is it-" His face creased in concern, and she noticed that his eyes flickered towards her waist. 
"No, everything's fine there. I'm just worn out, I guess." 
"Okay." Mulder kissed her quickly. "See you in a while. This clinic is only supposed to go another couple of hours." 
As she walked away from him, it occurred to her to wonder if part of the appeal of coming to this place was the fact that they didn't have to worry about what anyone thought of their public displays of affection. She couldn't say that she didn't like the faux Chilmark for that reason, too. 
** 
Later 
Somehow, Scully and Chip ended up in the spare room, chatting while the kids slept. Having her alone for the first time, Chip seemed every bit as curious as his father. At least the version of his father that'd she'd grown up with. 
"Tell me everything that's different between this world and yours." He invited, and two hours passed in a flash. 
"Yeah," Chip was saying. "Uncle Fox is an engineer, and Aunt Dana was a librarian, but she's been staying home with the babies-" 
Before Scully could make a comment about how different Fox and Dana's chosen paths were, Mulder burst into the room, looking pale and frantic. Without even seeming to look at them, he picked Ezra out of his crib and thrust him into Scully's startled arms before scooping up Mckenna. "Chip, you have five minutes to grab whatever you need. You and these two," he nodded at the toddlers with his chin, "are going through to our world right now. You can stay at my apartment." 
"Mulder, what's going on?" Scully asked as she tried to sooth the crying baby in her arms. 
"Chip, I'm sorry to be the one to give you this news, but... Fox is dead." Mulder said shortly. "I'm not willing to take any chances that the MMR will protect these three." 
Chip had been methodically gathering things he hadn't already packed looked up with a concerned face. "Poor uncle Fox. But what about my mom?" 
"We'll send her through when she gets home." Mulder assured him. "Bring your stuff out to the car." 
As soon as they were out of earshot, Scully turned to Mulder. "How is my family going to react to the kids?" 
"I have no idea." He admitted. "We can't lie to them about who they are, they look too much like us to be some random children we decided to look after." 
"We can't tell them the truth." Scully hissed. "They'd have us institutionalized." 
"We'll cross that bridge when we come to it." He told her. "And it's not like Dana might not get better, so we can give them back before your family ever sees them." 
"Mulder." Scully said slowly. "Dana's probably not going to get better either. She isn't in much better shape than Fox was." 
"We'll figure something out. Right now getting these kids away is the most important thing. Right?" 
She found herself nodding in agreement, but still she wondered if they were being selfish to think of the people they cared about first. Who didn't, though, in a situation in which you couldn't save everyone? No one. That's what she kept telling herself as she grabbed baby belongings and stuffed them into diaper bags so they could grab the kids and run. 
** 
Mulder's apartment was overfilled that night. The little ones crashed in a playpen Mulder hastily bought, but that still left three adults to deal with. Looking around at the inadequate accommodations, Mulder gave Chip and Samantha an apologetic smile. "I'm sorry. I don't host too many sleepovers..." He glanced at Scully. "At least not many that involve more than one person spending the night." 
"Good thing we washed the sleeping bags." Scully remarked brightly, which only made him feel worse. 
"It's okay, Mulder." Samantha assured him. "Besides, it's just for one night." 
He shook his head. "I don't think that's wise. I'd like for you guys to stay here until it's clear that the vaccine is going to work." 
"And what if it doesn't?" Chip asked innocently. 
"Then your mother and I will have to have long talks about that." Mulder told him after thinking the answer over. "If it doesn't work, your world isn't the type of place anyone is going to want to live in." 
Samantha began to object. "Disease is terrible, but-" 
"You've never seen a disease like this one." Scully said quietly. "Whatever you're thinking disease is, this disease will completely challenge your mental picture." 
"Scully, I don't know if this is the best time to be telling them what we might be up against." Mulder objected. 
"I think it is, Mulder. It took me a long time to come to terms with what we know, and Chip and Samantha probably don't have the luxury of time, not if this thing turns into a full-scale infection." 
"I guess." He said reluctantly. 
"The reason that Mulder doesn't want to explain this now is because you're probably going to think that we're insane when we tell you what might happen. But James sent you to us because he knew that we'd dealt with this before, so you're going to have to trust him that we know what we're talking about, even if you don't trust us." Scully told them, and then she and Mulder explained everything they knew about the virus in question. 
It was clear that neither Samantha or Chip wanted to believe that they were telling the truth, but by the end of the lecture, they seemed to fall into a grim acceptance. That was enough for Scully, so she was able to leave for the night without worrying that they'd try to get Mulder committed in her absence. 
** 
Although he tried to talk her out of it, Samantha insisted on returning to her world the next day to keep a vigil over her sister-in-law. Mulder had seemed ready to argue with her, but Scully had drawn him aside, taking him out of the room. 
"Mulder, you have to look at it from her point of view. Her brother has just died, and odds are fair to middling that her sister-in-law, whom she's known for half of her life, is going to die too. You can't keep her away from Dana, it would be cruel." 
"Keeping her safe would be cruel?" He grumbled. 
Scully gave an incredulous snort. "Are you implying that you'd never, or rather you haven't, put yourself at personal risk for someone else's sake? Think carefully about what I know before you lie to me." 
"Those times were different." 
"How?" 
"They just were." He insisted stubbornly. 
"If you keep her here, she'll hate you." Scully warned him. 
This elicited a huge sigh from him, and he turned and walked away, leaving her behind. Though the walls muffled the conversation in the other room a little, she still clearly heard him say, "If you feel like you have to be there, you're an adult, and I guess I can't stop you. I ask, however, that you go alone. At least for now, unless it becomes clear that good-byes need to be said..." 
Perhaps realizing that they could be overheard, they pitched their voices lower. A few minutes later Mulder returned to where she stood, and he still looked aggrieved. "Chip is going to stay here and baby-sit." 
"And what are we going to do?" 
He shrugged. "Go to work. Unless you think there's anything more we can do for Dana-" 
Scully shook her head in sad denial. 
"If we miss too much time we'll put our work in jeopardy too." He concluded. 
"Didn't I say that days ago?" She asked, raising her eyebrows in amusement. 
"Maybe I listened for once." 
** 
Two days later 
The heart monitor was still making its steady buzz when Chip put his arm around his mother's shoulder and led her out of the hospital room. Giving Mulder and Scully a sad look, a nurse turned it off and left them alone with the dead woman. 
Dana looked peaceful. Nothing had burst out of her, clawing its way into life, which was the only blessing in the sad situation. The vaccine hadn't cured her, so Scully thought perhaps they'd just been too late giving it to her. Or perhaps it worked like the treatment for heartworm in dogs: poisoning the parasite, but throwing pieces of the decaying creature into the blood stream to cause blockages. Nearly half of dogs given the last ditch cure didn't survive the treatment. 
What was worse, as far as Scully was concerned, was that the vaccine hadn't prevented new cases from showing up at the hospital. At least a handful of the new cases were people who'd been amongst the first vaccinated. 
As if reading her mind Mulder sighed heavily. "We're not going to be able to contain this. This world is doomed." 
"Then what do we do?" Her voice was laced with frustration. 
"We evacuate as many as we can. All those born after 1973 and anyone whose double isn't living. Then quarantine them to make sure they're not infected." 
"And the ones who can't go through because they are living in our world already?" 
His only reply was a bleak look. 
"This is going to break up families." 
"After being quarantined the orphans can be adopted." His look softened a little. "Except for Mckenna and Ezra." There was a note of challenge to his voice. "I intend to raise them." 
"And if I don't?" She blurted out before stopping to ask herself why. 
Mulder gave a nearly imperceptible shrug. "Then I'd do it alone. Isn't that what you had in mind for yourself when you asked me to be your donor months ago?" 
"Things are different now." 
"I hope that they are. But if they're not I'll hire help and do things on my own. I owe them that much." 
"Why do you owe it to them? It's not your fault that this dying world exists. So why are you the one who has to sacrifice?" 
"That's just the way things are. No one's actions exist in a vacuum and consequences have to be dealt with. As consequences go, there are worse. Murder, disease, poverty... two unexpected children are a joy in comparison." 
"You mean three. Three unexpected children." She put a hand to her belly. "But Mulder, what if Samantha wants them?" 
He looked stricken. "Do you think she does?" 
"She might. Mulder, she's their aunt, and she's been looking after them since their parents took ill." 
"Maybe she doesn't." 
Scully stared at him. She was beginning to warm to the idea of raising their duplicates' children, but now she was worried that her question was one born of unconscious insight rather than practicality. Just then, she had no idea how soon the issue would be resolved. 
** 
Samantha had promised to bring Chip back through as soon as they said goodbye to Dana, and she'd kept her word. She and Chip were red-eyed and antsy when Mulder and Scully met up with them a short time later. 
"We can't go back." Samantha said heavily. 
"No." Mulder agreed. "It wouldn't be safe for you. In fact, we're going to try to get as many people out as possible." 
"And those you can't get out?" She asked. 
"Pray for them." Scully replied bleakly. 
"We're immune to this." Mulder told Samantha. "So if you want to make a list of things you want, we can try to get them for you. We're going to have to go back and speak to James, anyway." 
Samantha nodded slowly. "Thank you. Do you know of a good hotel in the area that rents by the week? It's going to take some time to settle, and I don't think sleeping on your floor will be the best plan for everyone." 
"Take my apartment." Scully blurted out. "Until you find one. I can stay at Mulder's. I do half the time any way." 
"That's kind of you." Samantha told her. Then she sighed. "When Charlie died, the three of us discussed what we'd want done when we died. Fox and Dana wanted to be cremated. Can you get James to arrange that, please?" 
"Of course." Mulder said quickly. 
Chip looked like he was thinking hard about something. "We're not going to be able to bring their ashes here, are we?" 
"No. I'm sorry." Scully looked it. "We have no way of knowing if cremated remains still pose a risk of infection." 
"That's what I thought. It's okay." He said shakily. "Who needs urns to dust anyway. It won't bring them back any." 
With nothing left to say, they got into the car and prepared to move the displaced to Scully's apartment. 
** 
They left Mulder sitting in the car when they pulled up to Scully's apartment. She leaned over the front seat and kissed his cheek. "I'll just show them where everything is and get some clothes, okay?" 
"Sure. I'm going to be listening to the game on the radio, so don't hurry on my account. Do you want me to help carry the kids in?" 
"We've got it, Mulder. Thanks." Samantha answered for her. 
"Yeah, we'll be fine." Scully told him. "We've got Chip for the heavy lifting." 
Chip grumbled good-naturedly and they abandoned Mulder to his radio baseball game. 
Once inside the apartment, Scully played tour guide. "The master bedroom and the spare bedroom are those doors there-" She pointed, "and the bathroom is over here. You've seen the living room and kitchen by now." She turned to Chip. "Help yourself to anything in the fridge that doesn't try to eat you first." 
"Ha, thanks." 
"If you'll excuse me, I need to pack a bag to take to Mulder's." 
"This is very generous of you...Scully." Samantha told her before she was able to leave the room. 
"No problem. It's a only temporary, right?" 
"Yeah. I hope we find a place real soon." 
** 
In her room she debated a while about what to pack. On one hand she could always come back if she needed more clothes, but on the other she didn't want to come around any more than necessary, since she thought it would seem territorial, and less than welcoming to her guests. 
She was still putting things into a bag when a gentle hand closed on her shoulder, making her jump. When Scully looked up Samantha's dark blue eyes were filled with concern. "I'm sorry. I didn't mean to startle you." 
"It's okay." Scully said quickly. "I've just been a little on edge lately." 
"I don't blame you. It's been one shock after another the last couple weeks." 
Scully looked up at the taller woman, suddenly surprised that she'd only known her for two weeks. "More so for you and Chip than me." 
"That's up to debate." 
"What was it that you came in to asked me?" Scully asked, aware of how fire their conversation must have wandered from what was on Samantha's mind. 
"I... I wanted to talk to you about Ezra and Mckenna." Samantha said looking guilty. 
A sudden icy panic flooded Scully stomach, and she didn't think it was morning sickness. She realized that despite her recent conversation with Mulder, she already had convinced herself that the children belonged with them. 
"I love my niece and nephew dearly, but... I raised a child already, and I'm proud of the young adult my son has become. I don't think I could start over again, with diapers rather than college tuitions." Samantha looked away as if ashamed. "I know it's a lot to asked, considering how convoluted your relation to the kids is, and Mulder let it slip that you are already expecting baby, who'll be pretty near them in age... I'll understand if you're not interested in doing it..." 
Scully blinked in confusion. "Are you saying that you want Mulder and me to raise them?" 
"Like I said if you don't want to, there's always private adoption-" 
"No, no don't get that idea. We do want them, actually. Mulder already made that quite clear." 
"What about you?" 
"I want them. God do I. I was afraid you're going to say that you want them." 
"They're the sweetest things, but I don't think I could do it." A smile broke out on Samantha's face. "I'll help you and Mulder and I can." 
** 
Even over the frantic announcer, Mulder heard the door to Scully's building open. He looked up and was surprised to see Mckenna toddling out in front of Scully, whose arms were filled with Ezra and baby bags. 
It only took him a moment to launch himself out of the driver's seat and meet her on the walk. "What's going on?" 
Scully gave him a happy smile. "They're ours, Mulder. Samantha asked me if we'd raise them. She's had her fill of child-rearing." 
Bending down, he pulled Mckenna off her feet, which made her laugh with delight. "That's fantastic! I can't believe that we're parents already. I thought we had more than seven months left to prepare ourselves. Not that I'm complaining." 
"It's something else, isn't it?" Scully agreed. 
Before they knew it they were strapping the kids into the car seats that they hadn't got around to taking out of the car. 
** 
Having the kids alone for dinner proved to be an interesting experience. 
"What do they eat?" Mulder asked Scully once she broached the topic of food. 
"I'm not sure. Mckenna is two, so she probably eats what we do. Ezra I'm not so sure of. Bill told me that Matthew had some very strange eating habits at Ezra's age. Maybe we should call Samantha and ask." 
"No, I don't want her to think that we're completely incompetent at this." Mulder told her. "Well, we know he can eat french toast, anyway." 
"We can't feed him that three times a day, Mulder!" 
He held up his hand. "I meant for tonight." Suddenly, he looked down at Mckenna. "Hey, Kenna, do you know what your brother likes to eat?" The little redhead gave him a blank look. "Oh well." 
"I think they make food for toddlers at the grocery store." Scully suddenly remembered. We can pick up some meals tomorrow." 
"Good. Problem solved." He gave her a long look. "So, do you know how to make french toast?" 
Sighing, Scully handed him Ezra and went into his kitchen. 
The kids were enthusiastic, if messy, eaters, and they didn't require as much help as much help eating as either of their new "parents" anticipated. Mckenna used a fork with some skill, and Ezra was content to pick up the pieces Mulder cut up for him and stuff them in his mouth. 
Eventually, Mckenna pushed her plate away. "All done?" Scully asked. 
"Yup, Mommy." 
"Let me grab your plate, then." Mulder told the little girl. Everyone else had finished, so he took all the plates to the sink. "We're going to need a bigger place." Mulder called over his shoulder. 
"Yeah." Scully agreed faintly. If he'd looked back, he would have seen that Scully was staring at Mckenna. 
** Late That Night 
Mulder woke to rain on his fingertips. Brow furrowing over the wrongness of that, he struggled to gain access to the waking world. They were sleeping in bed, so there was no rain. Yet his fingers were wet. Eventually he realized that the wetness was tears, not raindrops. His fingers were pressed to Scully's wet cheek. 
Leaning over, Mulder saw that she was awake. "What's wrong?" 
After taking a hitching breath, she looked up at him with a crumpled face. "We're terrible people, Mulder." 
This pronouncement startled him. "What do you mean?" 
"Fox and Dana are dead." 
"Yes..." 
"And we don't even care! Parts of us have died-" 
"They weren't us, Scully." 
"In a way they were, and you know it. And we can't even summon up the least bit of grief for them. All we have is a little polite sympathy for Samantha and Chip. Where is our sense of empathy?" 
"We never even met Fox or Dana, so you're being a little hard on us-" 
She shook her head. "What's worse is that we've scavenged the dead." 
"We've what?!" 
"Picked up the fallen off pieces of their life that they can't hold on to any more." 
Sighing, he gathered her into his arms. "So this is about the kids." 
"And Samantha. And Chip." She agreed with a tiny nod. 
"I don't think they'd be mad. If you died, wouldn't you be happy if there was someone there to help your mother, Bill and Charlie carry on with life?" 
"Yes, but...Mulder, Mckenna calls me Mommy. She thinks I'm her real mother!" 
"Ezra probably does too. That's a good thing." He rubbed small circles on her back as he spoke. 
"Is it? Letting them believe that is lying to them." 
"Back when that surgery was done to me, the smoking bastard claimed to be my real father." Mulder confessed. "And as much as I hated to admit it to myself, I don't think he's lying. Do you think I would have been happier growing up knowing that Bill Mulder wasn't my father?" 
"I don't know." 
"I do. Knowing that the man who terrorized my mother and upset the man I thought was my father would have just made my life harder. Not knowing the truth meant life was pretty good until I was twelve...the lie was kinder." He said firmly. 
"But-" 
"When they're older, we can decide if it's better to tell them or not. Right now, let's just let them enjoy their childhood." 
He felt her give in, her body relaxing against his, before she replied. "Okay."  
TO BE CONTINUED 


