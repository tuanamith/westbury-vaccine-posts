


< <URL> > Mumps hits Iowa 
DES MOINES -- An outbreak of mumps is sweeping across Iowa, the first in nearly 20 years, and it's puzzling health officials and worrying parents. 
``We have probably, at this point, what we would call an epidemic of mumps,'' said state epidemiologist Dr. Patricia Quinlisk. 
As of Thursday, the latest report available, 245 confirmed, probable and suspected cases of mumps had been reported to the Iowa Department of Public Health this year. 
The first cases were reported in mid-January. 
``It started in eastern Iowa. It's spreading across the state. We have now seen possible cases in three of our bordering states,'' Quinlisk said. 
Illinois, Minnesota and Nebraska may have one or two cases of suspected mumps, but Iowa is the only state in the United States with so many cases of the virus, she said. 
Health officials have not identified how it started. 
...snip 
The virus is in 36 of Iowa's 99 counties. Dubuque, Johnson and Black Hawk counties in eastern Iowa are reporting the highest incidences. 
College-age students, those 18- to 22-years-old, have been infected the most, but other age groups are also seeing cases. 
...snip 
Both children had been given the measles, mumps and rubella vaccine, or MMR. So had their other son, 13-year-old Jimmy, who did not get the mumps. 
A mumps vaccine was introduced in 1967. People born before 1957 are believed to have been exposed to mumps during childhood and should be immune. 
``The vaccine is working,'' Quinlisk said. ``The vaccine certainly was made to cover this particular strain because it's a fairly common strain of mumps.'' 
Quinlisk said, however, the vaccine is about 95 percent effective. 
``What that means is out of 100 people who get vaccinated, 95 of them will have lifelong immunity and will never get mumps even if they're exposed,'' she said. ``Unfortunately, five percent or 5 out of every 100, the vaccine doesn't take.'' 
The last mumps outbreak in Iowa was in 1987 when 476 people were infected. Until this year, less than 60 cases were reported annually, with only one to three cases reported in the past five years. 
The U.S. Centers for Disease Control and Prevention in Atlanta, which was notified of the mumps outbreak in Iowa on Feb. 9, has identified the strain as genotype g. 
That genotype has been identified in recent outbreaks in Canada, in two New Jersey cases imported from Ireland and an ongoing mumps outbreak in the UK, CDC spokeswoman Lola Russell said. 
The mumps epidemic in the United Kingdom began in 2004 and peaked in 2005 when 56,390 cases were reported in England and Wales, according to the CDC. 
In 2004, New Jersey had eight cases of the mumps. For 2005, there were seven cases, but those numbers are not final, said Nathan Rudy, spokesman for the New Jersey Department of Health and Senior Services. 
New York reported a cluster of cases at a summer camp in 2005. An investigation by state health officials identified 31 cases of the mumps. According to the CDC, the outbreak was most likely introduced by a camp counselor who traveled from the UK and had not been vaccinated for mumps. 
``Eradication is only as effective as the population remains at high levels of being vaccinated,'' Russell said. ``That's the health message that we always emphasis over and over again.''  
-- WÂ§ mostly in m.s -  <URL>  

