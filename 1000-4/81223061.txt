



" Jill" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 

from  <URL>  
OIE says some China bird flu vaccines ineffective  21 Mar 2006 16:55:06 GMT PARIS, March 21 (Reuters) - China may be using some substandard poultry  vaccines to fight bird flu that could allow birds to keep spreading the  virus despite not showing symptoms, a top animal health expert said on  Tuesday. Christianne Bruschke, a member of the bird flu task force at the Paris-based  World Animal Health Organisation (OIE), said the vaccines made in China at  the Harbin Veterinary Institute conformed to international standards and  were fully effective. "But there are local companies in China that produce vaccines and it's  possible these don't have the efficacy of those made at Harbin," she told  Reuters. Chinese officials have said their vaccines are effective and that no  healthy-looking bird has yet been found carrying H5N1. But there have been outbreaks in areas where poultry were supposedly already  vaccinated and this has raised the possibility that birds are continuing to  spread the virus. "There are two possibilities," Bruschke said. "Either the vaccine is not  strong enough, or that it is a good vaccine but not administered properly.  Vaccines have to be given twice to be fully effective, maybe that is not  happening. "When a vaccine is not effective enough, it could prevent clinical signs  from showing but the bird could still spread the virus. That's certainly a  possibility," she said. And she said poultry that had not been properly vaccinated may also pose a  health risk to humans although it was difficult to quantify the danger. Research in Europe, which did not import vaccines from China, showed drugs  stopped the spread of the virus, she said. "The vaccines are, when administered properly according to manufacturers'  instructions, effective in preventing the spread in chickens and ducks," she  said, adding that more research was needed on other bird species. The Netherlands, France and Russia have announced plans to vaccinate some of  their poultry against bird flu. But Dutch farmers have mostly chosen to wait  before vaccinating their birds because they fear a negative impact on  exports. David King, the British government's chief scientific adviser, said the  often close contact between humans and chickens in China meant that vaccines  were particularly necessary there. "The Chinese are using a number of different vaccines and one of the  vaccines it is claimed has a high efficacy," he said. "But we couldn't possibly use a vaccine that hadn't been fully through the  European procedures of testing," he added. (Additional reporting by  Elizabeth Piper in London) 

--  
regards Jill Bowis 



