


Jo Schaper < <EMAILADDRESS> > wrote in   <NEWSURL> : 
      
   "H5N1 avian influenza leads to severe disease in both birds and     humans. Between January 2004 and March 11, 2005, there were 69     confirmed cases of and 46 deaths from H5N1 infection in humans     reported to the World Health Organization.  
 <URL>  
That's a 66% mortality rate. Assuming another 69 cases unreported  still leaves a 33% mortality rate. They've just now started working on a vaccine. H5N1 doesn't respond to conventional antivirals. 
We'll have to wait and see what actually emerges. 


