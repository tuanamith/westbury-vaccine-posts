


Hospital beds, medical equipment heading for Cuba The Mississauga News Mar 1, 2006 
Credit Valley Hospital (CVH) is donating much-needed beds and medical  equipment to Cuba this week. 
In doing so, the Mississauga health-care facility is continuing a  Canadian tradition of helping Cubans in need of medical items that began  more than a decade ago, thanks in large part to one of Canada's most  famous Prime Ministers, Pierre Trudeau. 
The shipment, which is being sent via Health Partners International of  Canada's (HPIC) Tomken Rd. distribution centre, is the latest load to  head to the Caribbean nation under a humanitarian medical aid  arrangement struck in 1995. 
The items, which are second-hand from CVH, are going directly to a  hospital in Cuba, HPIC officials say. 
HPIC, a Montreal-based humanitarian organization with a 26,000-sq.ft.  distribution centre in Mississauga, provides medicines, vaccines and  medical supplies donated by Canadian health-care groups -- such as CVH  -- to doctors, aid organizations and government agencies in the  developing world. 
"We partner with Canadian companies and individuals from across the  country to provide medical aid. Together, we are improving the health  and lives of children and adults in the developing world," said HPIC  spokesperson Margaret Buchanan. 
Since 1990, HPIC has delivered more than $180 million in medical aid to  111 countries. Of that, $40 million in goods has been sent to Cuba since  1995. 
HPIC also taps into a network of non-governmental organizations,  humanitarian agencies, medical professionals, faith-based organizations  and churches, community groups, and transportation service providers to  ensure vital medicines are sent quickly to aid workers for efficient  distribution. 
The arrangement with Cuba was struck in August of 1995. After an  official visit to the country in 1976 as Prime Minister, Trudeau  returned to the Caribbean island numerous times and it was his goal to  help people there get the medicine and supplies they needed. 
An arrangement was eventually worked out and today HPIC has various  projects in the country. 
In 2003, the Canadian International Development Agency (CIDA) began  working with HPIC to deliver $15 million in medical aid to Cuba over  five years. Cuba's Ministry of Health relates medical needs to HPIC and  officials here assemble the medical products required. 
Millions of dollars in donated medical products are received at HPIC's  facility throughout the year and subsequently shipped to locations  around the world. 
 <URL>  


