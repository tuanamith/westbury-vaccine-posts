


Experts Say AIDS Prevention Pill Close 
ATLANTA -- Twenty-five years after the first AIDS cases jolted the world, scientists think they soon may have a pill that people could take to keep from getting the virus that causes the global killer. 
Two drugs already used to treat HIV infection have shown such promise at preventing it in monkeys that officials last week said they would expand early tests in healthy high-risk men and women around the world. 
"This is the first thing I've seen at this point that I think really could have a prevention impact," said Thomas Folks, a federal scientist since the earliest days of AIDS. "If it works, it could be distributed quickly and could blunt the epidemic." 
Condoms and counseling alone have not been enough - HIV spreads to 10 people every minute, 5 million every year. A vaccine remains the best hope but none is in sight.    If larger tests show the drugs work, they could be given to people at highest risk of HIV - from gay men in American cities to women in Africa who catch the virus from their partners. 
People like Matthew Bell, a 32-year-old hotel manager in San Francisco who volunteered for a safety study of one of the drugs. 
"As much as I want to make the right choices all of the time, that's not the reality of it," he said of practicing safe sex. "If I thought there was a fallback parachute, a preventative, I would definitely want to add that." 
Some fear that this could make things worse. 
"I've had people make comments to me, 'Aren't you just making the world safer for unsafe sex?"' said Dr. Lynn Paxton, team leader for the project at the Centers for Disease Control and Prevention. 
The drugs would only be given to people along with counseling and condoms, and regular testing to make sure they haven't become infected. Health officials also think the strategy has potential for more people than just gay men, though they don't intend to give it "to housewives in Peoria," as Paxton puts it. 
Some uninfected gay men already are getting the drugs from friends with AIDS or doctors willing to prescribe them to patients who admit not using condoms. This kind of use could lead to drug resistance and is one reason officials are rushing to expand studies. 
"We need information about whether this approach is safe and effective" before recommending it, said Dr. Susan Buchbinder, who leads one study in San Francisco. 
The drugs are tenofovir (Viread) and emtricitabine, or FTC (Emtriva), sold in combination as Truvada by Gilead Sciences Inc., a California company best known for inventing Tamiflu, a drug showing promise against bird flu. 
Unlike vaccines, which work through the immune system - the very thing HIV destroys - AIDS drugs simply keep the virus from reproducing. They already are used to prevent infection in health care workers accidentally exposed to HIV, and in babies whose pregnant mothers receive them. 
Taking them daily or weekly before exposure to the virus - the time frame isn't known yet - may keep it from taking hold, just as taking malaria drugs in advance can prevent that disease when someone is bitten by an infected mosquito, scientists believe. 
Monkeys suggest they are right. 
Specifically, six macaques were given the drugs and then challenged with a deadly combination of monkey and human AIDS viruses, administered in rectal doses to imitate how the germ spreads in gay men. 
Despite 14 weekly blasts of the virus, none of the monkeys became infected. All but one of another group of monkeys that didn't get the drugs did, typically after two exposures. 
"Seeing complete protection is very promising," and something never before achieved in HIV prevention experiments, said Walid Heneine, a CDC scientist working on the study. 
What happened next, when scientists quit giving the drugs, was equally exciting. 
"We wanted to see, was the drug holding the virus down so we didn't detect it," or was it truly preventing infection, said Folks, head of the CDC's HIV research lab. It turned out to be the latter. "We're now four months following the animals with no drug, no virus. They're uninfected and healthy." 
Years of previous monkey studies using tenofovir alone had shown partial protection. The scientists thought to add the second drug, FTC, when Gilead's combination pill, Truvada, came on the market last year. 
The results, announced at a scientific meeting last month in Denver, so electrified the field that private and government funders alike have been looking at ways to expand human testing. 
"This is an approach we've considered for a long, long time," but didn't try sooner because AIDS drugs had side effects and risks unacceptable for uninfected people, said Dr. Mary Fanning, director of prevention research at the National Institute of Allergy and Infectious Diseases. 
Tenofovir changed that when it came on the market in 2001. It is potent, safe, stays in the bloodstream long enough that it can be taken just once a day, doesn't interact with other medicines or birth control pills, and spurs less drug resistance than other AIDS medications. 
The CDC last year launched $19 million worth of studies of it in drug users in Thailand, heterosexual men and women in Botswana, and gay men in Atlanta and San Francisco. A third U.S. city, not yet identified, will be added, CDC announced last week. 
Because of the exciting new monkey results, the Botswana study now will be switched to the drug combination; the others are well under way with tenofovir alone. 
Farthest along is a study of 400 heterosexual women in Ghana by Family Health Initiative. The Bill & Melinda Gates Foundation funded it and others in Cambodia, Nigeria, Cameroon and Malawi, but the rest were doomed by rumors, including fears that scientists wanted to deliberately expose people to HIV or that study participants who got infected might not have access to treatment. In other cases, activists demanded better health care or clean needles for drug users as a condition for allowing the studies to proceed. 
Such problems are "part of the HIV prevention landscape" in many foreign countries, said Dr. Helene Gayle, who formerly oversaw AIDS research for the Gates Foundation. 
Expense also could limit use of the drugs. Gilead donated them for the studies and sells them in poor countries at cost - 57 cents a pill for tenofovir and 87 cents for Truvada, the combination drug. That's more than the cost of condoms, available for pennies and donated by the truckload in Africa, but often unused. 
In the United States, wholesale costs are $417 for a month of tenofovir and $650 for Truvada. 
Still, health officials are hopeful the drugs could fill an important gap. 
The National Institutes of Health is starting a tenofovir study in 1,400 gay men in Peru. Private and government funders are considering others. Tenofovir also is being tested in microbicide gels that women could use vaginally to try to prevent catching HIV. 
"If you're in an area where there's a really high HIV incidence, something that's even 40 percent effective could have a huge impact," Paxton said. 
And in the Atlanta labs where Heneine, Folks and others are still minding the monkeys, "the level of enthusiasm is pretty high," Heneine said. "This is very promising. For us to be involved in a potential solution to the big HIV crisis and pandemic is very exciting.". 
Copyright 2006 by The Associated Press.  

"Civilizaton is the interval between Ice Ages." -- Will Durant. 
"War is God's way of teaching Americans geography" -- Ambrose Bierce  
"Progress is the increasing control of the environment by life.  --Will Durant 
Joseph R. Darancette  <EMAILADDRESS>  

