


Mumps cases spread in Nebraska, 8 other states Updated 4/17/2006 9:39 PM  <URL>  
Tina Rolfes gets a mumps, measles, and rubella immunization in Dubuque, Iowa. 
LINCOLN, Neb. (AP)  Nebraska, which is part of a nine-state mumps epidemic, is now reporting 110 cases of the disease in 22 counties, health officials said Monday. Thirty-two of those cases are confirmed. "Currently, most of our mumps cases are in southeastern Nebraska," said Anne O'Keefe, epidemiologist for the state Health and Human Services System. 
She said most of the cases are among people ages 10 to 18 and 35 to 45. 
"However, we're seeing cases in children as young as 2 and adults up to age 64," she said. 
The mumps epidemic is the nation's first in 20 years. 
Some 600 suspected cases have been reported in Iowa, according to the national Centers for Disease Control and Prevention. 
There are also cases reported in Kansas, Illinois, Indiana, Michigan, Missouri, Wisconsin and Minnesota. 
Mumps is a viral infection of the salivary glands. Symptoms include fever, headache, muscle aches and swelling of the glands close to the jaw. It can cause serious complications, including meningitis, damage to the testicles and deafness. 
Mumps is spread by coughing and sneezing. 
No deaths have been reported from the current epidemic. 
A two-dose mumps vaccine is recommended for all children and is considered highly  but not completely  effective against the illness. 
According to HHS, Nebraskans ages 30 to 65 years old are the most at risk to catch the disease because they probably never were vaccinated or had the disease. 
Nebraska law for years has required two doses of the mumps vaccine before a child can enter school or college. Thus Nebraskans under the age of 30 who followed the K-12 and college entry requirements probably have been vaccinated. 
Nebraskans over the age of 65 are likely to have natural immunity to the virus. Many in this age group had mumps as a child. 
Two infected airline passengers may have helped spread Iowa's mumps epidemic. 
Iowa health officials last week identified two people who were potentially infectious when they were traveling in late March and early April. 
The CDC said the present outbreak is the nation's biggest epidemic of mumps since 269 cases were reported in Douglas County, Kan., from October 1988 to April 1989. 
 <URL>  
The Truth About Slavery from the American Dissident Voices  <URL>  
LACK THE ABILITY TO THINK OR FEEL AS OTHER RACES. 
*** Posted via a free Usenet account from  <URL>  *** 

