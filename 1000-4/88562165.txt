


Job Title:     Clinical Trial Manager Job Location:  PA: Philadelphia Pay Rate:      Open Job Length:    7-12 months (contract) Start Date:    2006-04-10 
Description:   Title:  Clinical Trial Manager  
Company:   Pharmaceutical Company 
Location:   Philadelphia Metro Area 
Status:   Full-time consulting engagement, 40 hrs/week 
Length:  Long-term assignment in 8 month increments 
As a Clinical Trial Manager you will participate in the scientific aspects of phase IIIb and phase IV vaccine clinical trials sponsored by company.  You will participate in or lead scientifically-oriented activities including protocol writing, case report form design, writing clinical study reports, writing sections of various regulatory submissions such as periodic safety update reports and responses to regulatory agencies. You will lead or actively participate in various activities related to the writing and management of scientific publications. You will be responsible for the management of vaccine-related investigator-initiated research trials, including leadership of the contracting, procurement, coordination and shipment of clinical trial material to investigators, financial forecasting, monthly budget accounting, detailed tracking of study status, and timely processing of all investigator payments. When applicable, you will participate in the selection of contract research organizations. You will regularly interact with other functional areas including clinical pharmacy, legal, marketing, central contracting, worldwide regulatory affairs, clinical research and development, and global safety, surveillance, and epidemiology.  
Requirements: An RN or Bachelor's degree in scientific field is required; a Master's degree is preferred. With an RN, a minimum of 8 years of clinical research experience is required. With a BS/BA, 5 years of clinical research experience is required and with a MS/MA or R.Ph., 3 years relevant experience is required. Your experience should be in one or a combination of several of the following areas: design, implementation, monitoring (CRA), and/or management of phase II, III and/or Phase IV clinical trials, clinical (study) coordinator for clinical trials, safety surveillance of clinical trials, medical writing, or other related clinical experience.  This position requires strong scientific skills, including experience writing clinical protocols and clinical study reports. Current or prior experience in vaccine clinical trials is a plus. Prior industry/CRO/site experience in the conduct clinical trials is a must. 
-Three different Blue Cross Health Plans  
-Dental Insurance  
-401k  
-Life Insurance & AD&D  
-Long-term Disability  
-Paid Holidays  
-Up to 4 weeks of Vacation  
-Continuing Education Reimbursement up to $5,250 /yr  
-Section 529 Tax-Deferred College Savings Plan 
Please refer to Job code CB3301 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



