



"BAC" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... ... .... 
Sure, but there is some sense in the argument.  Presumably at present none  of those who disapprove of testing on animals would volunteer for human  trials because of the use of animals in the existing system.  Removing  animals from the process could create two benefits, it would increase the  number of people willing to volunteer, and increase the size of the market  for the resulting drugs. 
Assuming those who object to the use of animals is greater than the number  unwilling to participate if animal testing ends, that the vegan/ARA market  for drugs is significant, and that vegans/ARAs have either an interest in  the money to be made from volunteering and/or take an interest in the  benefits to others that result. 
It may be though that vegan/AR types also share a lack of belief in the use  of vaccines and other medicines that "strengthen the weak" preferring  nature to take its course.  Indeed it may even be that such beliefs are  what draw many to veganism, seeing it as somehow more natural. 
There is a precedent for assuming that opposition to testing on animals  indicates a willingness to test on (sub)humans - criminals, and even in a  lack of interest  in the use of medicines, favouring instead dietary  interventions, exercise, and "healthy living" -   <URL>  
Michael  

*** Free account sponsored by SecureIX.com *** *** Encrypt your Internet usage with a free VPN account from  <URL>  *** 

