


I saw the tower lit last night with the half and half look (bottom orange, top white), but didn't know what it was for.  Turns out that Texas history professor David M. Oshinsky won the Pulitzer Prize for History with his book Polio: An American Story.  Congrats to Oshinsky and this just continues the accolades at Texas.  Some are on the field of play, and some are in the classroom. 
RonB 
Press Release: 
History professor wins Pulitzer Prize for Polio: An American Story April 17, 2006 
AUSTIN, TexasThe University of Texas at Austins David M. Oshinsky has been awarded the Pulitzer Prize in the history category for his book Polio: An American Story. Oshinsky is the George Littlefield Professor of American History. 
This book is the latest in a long list of superb work that David Oshinsky has given us, said Richard Lariviere, dean of the College of Liberal Arts. He examines the historical record and shares it with the world in a way that illuminates human nature. I am pleased that the Pulitzer Committee shares our view that this is a wonderful and durable contribution to American history. 
Polio: An American Story details Americas obsession with the disease in the 1940s and 1950s. With no known cause and no available cure, polio was a frightening disease that held America in its grips until a vaccine was found. Oshinskys book examines the race between rival researchers Jonas Salk and Albert Sabin to find a cure. It notes that polio was actually a relatively uncommon disease, but was kept in the spotlight by an aggressive public relations campaign and unprecedented fund-raising efforts by the National Foundation for Infantile Paralysis, which founded the March of Dimes. 
This is a stunning achievement by a renowned member of our faculty, said William Powers Jr., president of The University of Texas at Austin. The Pulitzer Prize is well deserved recognition of another seminal contribution David Oshinsky has made to American history. 
Oshinsky is the second University of Texas at Austin professor to win a Pulitzer Prize. The other was William Goetzmann, who won the award in 1967 for his book Exploration and Empire. 
Oshinsky is a leading historian of modern American politics and society and has been at the university since 2001. Polio: An American Story has received accolades from National Public Radios Science Friday, the News Hour with Jim Lehrer, the New York Times and other media outlets. He is also the author of A Conspiracy So Immense: The World of Joe McCarthy and Worse Than Slavery: Parchman Farm and the Ordeal of Jim Crow Justice, both of which won major prizes and were New York Times Notable Books. 

