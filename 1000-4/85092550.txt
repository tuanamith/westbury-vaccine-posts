


Brian Carey wrote: 
Bush is a liar. 

 <URL>  
U.N. steps up Iraq inspections 
Tuesday, December 10, 2002 Posted: 10:20 AM EST (1520 GMT) 	U.N. vehicles enter the al-Tuweitha Nuclear Research Center U.N. vehicles enter the al-Tuweitha Nuclear Research Center     Story Tools Save a link to this article and return to it at www.savethis.comSave a  link to this article and return to it at www.savethis.com  Email a link  to this articleEmail a link to this article Printer-friendly version of this articlePrinter-friendly version of this  article  View a list of the most popular articles on our siteView a list  of the most popular articles on our site 	 
BAGHDAD, Iraq (CNN) -- U.N. arms inspection teams in Iraq doubled in  numbers on Tuesday from two to four -- and further inspectors are  heading for Iraq. 
The expanded teams were due to visit five sites on Tuesday including a  nuclear research centre storing uranium and a lab that previous  inspections teams believed was used for developing chemical and  biological weapons. 
Inspectors are carrying out their work as U.N. experts in New York pore  over Iraq's nearly 12,000-page declaration on its weapons programs. 
Additional weapons inspectors arrived last weekend to bolster the number  of U.N. inspection teams and a further 25 inspectors were flying to Iraq  later Tuesday. 
CNN's Nic Robertson said inspectors were again at the al-Tuweitha  Nuclear Research Center, where several tons of uranium have been under  seal since the previous round of inspections ended in 1998. 
Teams have now made four visits to Iraq's main nuclear facility --  composed of more than 100 buildings -- in order to put together a  comprehensive account of what is done there. 
It was at al-Tuweitha in the 1980s, The Associated Press reported, that  Iraqi scientists worked on developing technology for enriching uranium  to levels usable in nuclear bombs. 
In the other visits on Tuesday, inspectors were also at the Al-Hazen Ibn  Al Haithem Institute, a chemical and biological weapons site which  caused a dispute during inspections in the 1990s. 
At the time, then head of the U.N. weapons inspection program Richard  Butler said he did not believe Iraq had provided complete information on  activities at the complex near Baghdad. 
Near the capital, a third inspection team stopped at Abu Ghareeb, an  animal vaccine facility just west of Baghdad. 
The Amariyah Serum and Vaccine Institute there was the site of  biological weapons-related research in the 1980s, AP reports. 
That institute is reported to have expanded its storage capacity to an  extent that the U.S. government says exceeds Iraq's needs. Iraq contends  the facility only makes and stores human vaccines. 
The fourth inspection team made its longest journey outside the capital  Tuesday, traveling five-and-half hours and 420 kilometers (260 miles)  west of Baghdad to the Akashat Mine, a uranium ore and phosphate  production facility near the Syrian border. 
CNN's Rym Brahimi reported from the scene that it was a huge phosphate  complex that was known for producing natural uranium. 
As such it might have been part of an integrated network of facilities  for the production and conversion of natural uranium at the time that  Iraq seemed to have a nuclear program, she said. 
In the 1980s, phosphate deposits at Ashakat were exploited for their  uranium content as well as for fertilizer, producing some 100 tons of  uranium over six years. 
The inspectors convoy was waved in through the gate without difficulty,  Brahimi said, past a poster of a smiling Iraqi president, Saddam Hussein. 
The enormous complex surrounded by antenna posts, some broken, is in an  otherwise empty part of the desert. Reporters were unable to follow the  inspectors inside. 
The same team of inspectors was later expected to move 120 kilometers  (75 miles) northeast of Akashat to al Qaim where uranium was processed. 
Iraq's chief liaison to the U.N. teams, Lt. Gen. Hossam Mohammed Amin,  meanwhile told the Baghdad newspaper al-Rafidayn the Iraqis had found  the inspectors to be working in a "calm and professional" manner and  said Baghdad was "satisfied with it 

