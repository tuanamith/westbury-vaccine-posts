


Margaret Carlson: 'Bush's search for leakers leads to his mirror' 
Margaret Carlson, Bloomberg News 
(Bloomberg) -- I. Lewis "Scooter" Libby, former chief of staff and close confidante to the vice president, looked into the maw of a grand jury and disgorged words that few thought could be squeezed out of him. 
Libby told the grand jury, according to court documents obtained by the New York Sun, that he was authorized by President George W. Bush, through Vice President Dick Cheney, to spill classified information to New York Times reporter Judith Miller. This Libby did, on July 8, 2003, over drinks at the St. Regis Hotel, a few blocks north of the White House. 
At the time, the Bush administration was shaken by revelations undermining its carefully constructed justification for going to war with Iraq. The latest blow had come two days earlier from former Ambassador Joseph Wilson, who wrote an op-ed piece disputing the White House claim that Saddam Hussein tried to buy weapons-grade uranium in Niger. Wilson had been sent to Niger by the CIA to investigate the intelligence reports on which the claim was based. 
To strike back, the president wanted to reveal to the one reporter who had proved her willingness to serve as an administration scribe the section of the National Intelligence Estimate that said Saddam had "probably" tried to secure such fuel. 


No Denials 
By not denying Libby's bombshell, the White House last week more or less confirmed that the person the president swore would be punished for leaking classified information was himself, and that his oft-stated wish to find the leaker, in a bad parody of O.J. Simpson, had been achieved. 
Confronted with this revelation at a press briefing Friday, Bush spokesman Scott McClellan split hairs. The president didn't leak classified information, he "shared information;" ipso facto, information is no longer classified if the president leaks it. 
Secondly, McClellan said, by sharing information aimed at batting down "wild accusations" about weapons of mass destruction, the president is acting in the public interest, even if cynics might think he's really acting out of political interest. 
Secret Fiat 
Whether the president can leak anything classified at any moment by secret fiat while repeatedly vowing to track down and fire such leakers depends on what you mean by "declassify" and "leak." The president didn't publicly report this information. Through Cheney, he sent a top aide off to "share" it with a reporter, demanding that she cite a Capitol Hill staffer as the source. 
There's a process for declassifying information outlined in the Executive Order on the subject, which the president followed 10 days later when he released the whole National Intelligence Estimate. 
What he did on July 8 was something quite different, according to Jeffrey H. Smith, a former CIA general counsel. "Declassification is a formal act and the courts are clear on how it's done," Smith says. "Instead, here selective portions of the NIE were released to a single reporter. That's not declassification, that's a leak." 
Prosecute Them 
The president has lots of authority, but it's not unlimited or without hoops to jump through. For instance, the president has the authority to pardon anyone he wants for any reason. But imagine if he'd told the veep to tell Scooter to tell the warden at the Alderson, West Virginia, prison to spring Martha Stewart early. He could never get away with such a thing. 
The president wasn't unaware of the leaks; he was on top of them. And he isn't just a leaker, but a hypocritical one. He calls leakers of intelligence treasonous and vows to prosecute them. There's at least one official investigation going on now to track down and punish whoever leaked the existence of Bush's warrantless wiretaps. 
McClellan repeatedly insisted that Bush's power to declassify on a whim was absolute on Friday, without citing any authority or expressing any need to. 
Of a Piece 
It's all of a piece. Since Sept. 11, Bush has acted as if he can do anything he wants because we're at war. How extensive are his war powers and when do they end? When Osama's captured? When the Iraqi Army "stands up"? Before his term ends? He doesn't say. 
And what are those powers? As the president interprets them, he can round up anyone, hold and torture them as enemy combatants without benefit of counsel or trial. He can fudge the number of troops and amount of money needed in Iraq. 
He can wiretap anyone without a warrant, even though getting a warrant is swift, is nearly automatic, and can be sought retroactively. 
Domestically, he also has a free hand. He can ignore settled scientific judgments (and disband his science board, as he did last week) and make his own intuitive judgments about global warming, stem-cell research and the success of abstinence versus vaccines in reducing sexually transmitted diseases. He can get rid of any government employee who tells Congress or the public the true cost of the prescription-drug bill or his tax cuts. 
If there's justice in the world, the clip of Bush's promise to hold the leakers responsible will replace, or at least accompany, every replay of the tape of Bill Clinton's "I didn't have sexual relations with that woman" whopper. Bush joins Richard Nixon and O.J. Simpson and all the other Hall of Fame hypocrites who made such a to-do over searching far and wide for the miscreants they saw in the mirror each morning. 
©2006 Bloomberg L.P. 
Source: Bloomberg News  <URL>  d=aA0KoKrk5.Ec 






