



American Forces Information Service News Articles 
Tick. Tick. Tick. Lyme Disease Explosion Starts in Spring 
American Forces Press Service 
 WASHINGTON, March 16, 2001 -- Spring is here, and so is tick season across America and in many foreign countries. Being bitten by an infected tick can result in debilitating, sometimes deadly, Lyme disease, military and civilian experts warn. 
Left untreated, Lyme disease can advance from early flu- like symptoms to painful and permanent damage to the joints, according to the National Centers for Disease Control. The disease can also affect the nervous system, causing numbness, pain, stiff neck and severe headache or muscle weakness in the face or limbs. Occasionally, heart irregularities occur. 
The first stage of the disease begins three to 31 days after the tick bites. Symptoms can include fatigue, chills and fever, headache, muscle and joint pain or swollen lymph nodes. 
Another mark of Lyme disease, researchers said, is a peculiar expanding circular skin rash in the areas where the tick bite occurred. Patch shapes vary depending on location. The rash appears mostly on the thighs, groin, trunk and armpits, and on the faces of children. 
As the patch enlarges, the center may clear, giving a ring- like appearance. It may be warm, but isn't usually painful. However, researchers said, some people never develop a rash. 
People can pick up ticks during walks in parks or the woods, or while hiking and camping. Children are especially susceptible because they run around in tall grass, play in wooded areas and roll on the ground, researchers noted. The individual risk of getting Lyme disease is reasonably small. Only about 12 percent to 15 percent of ticks actually carry the bug. Experts said removing ticks from the body quickly may prevent a person from contracting Lyme disease. Ticks generally must feed on a person for 24 to 48 hours before the person becomes infected. 
Lyme disease experts warn field troops not to wear tick and flea collars meant for pets. Cats and dogs don't sweat, but people do, and harmful chemicals can get into the human body through sweat glands. 
Named after Lyme, Conn., where it surfaced in 1975, Lyme disease has become one of the fastest-growing vector-borne diseases in the United States. The highest incidence occurs in the Northeast from Massachusetts to Maryland and in Wisconsin, Minnesota, California and Oregon. A vector is a host -- the tick, in this case -- that passes the disease germ. 
Researchers at the Armed Forces Pest Management Board note that all military recruit training areas are infested with ticks. CDC officials said a number of service members have been infected in Germany over the years. 
The federal Food and Drug Administration approved a Lyme disease vaccine in December 1998 for persons ages 15 to 70. The vaccine's effectiveness depends on getting three doses in a year. The second dose is given a month after the first and the third, 11 months after that and just before the start of tick season. In other words, start now for protection next year. 
FDA officials emphasize the vaccine is not 100 percent effective and is not a substitute for other standard preventive measures. 
The best way to avoid Lyme disease is to stay away from places where ticks live -- tall grass and weeds, scrubby areas, woods and leaf litter. Another good idea: Check children and pets after they've played outside. 
Service members can use a two-part DoD chemical repellent system consisting of a permethrin-based spray for clothing and DEET-based lotion for exposed skin. The repellents should be coupled with proper wearing of the uniform. 
If you can't avoid tick-infested areas, CDC experts suggest you wear a long-sleeved shirt and long pants, tuck pant legs into socks or boots, tuck shirt into pants, tape area where pants and socks meet to keep ticks out, and wear light-colored clothing so ticks can be seen easily. 
After being outdoors: 
o Promptly remove and wash clothing; 
o Inspect your body carefully and remove attached ticks with tweezers, grasping as close to the head as possible and gently tugging the tick free without crushing its body. Squeezing the tick's body may force infected fluid into the wound; 
o Place tick in sealed container for examination by a local health department; and 
o Wash the wound and apply an antiseptic. 
DoD uses education to combat Lyme disease as well as other vector-borne diseases, said officials at the Army Center for Health Promotion and Preventive Medicine at Aberdeen Proving Ground, Md. The center Web site provides technical information, fact sheets and dozens of links to other pest- and disease-control agencies and activities. 
The center's address for comments, questions and requests for educational material is: 
U.S. Army CHPPM ATTN: Entomological Sciences Program Aberdeen Proving Ground, MD 21010-5403 or call DSN 584-3613 or (410)436-3613. or visit chppm-www.apgea.army.mil/ento on the Web. 
The Armed Forces Pest Management Board Web site offers an online version of Technical Information Memorandum 36, "Personal Protection Against Insects and Other Arthropods of Military Importance." The illustrated 113-page handbook is no longer available in print. 
You can obtain information from the Lyme disease electronic mail network called LymeNet. The service is available through the Internet at www.lymenet.org. 
The following hot lines are available for public use: 
Lyme Disease Foundation Inc. 1 Financial Plaza Hartford, CT 06103-2610 or call (800) 886-LYME or visit the Web site at www.Lyme.org. or send e-mail to  <EMAILADDRESS> . 
American Lyme Disease Foundation Inc. Mill Pond Offices 293 Route 100 Somers, NY 10589 or call (914) 277-6970 or visit the Web site at www.aldf.com. or send e-mail to  <EMAILADDRESS> . 
Also, the DoD pesticide hot line can answer all kinds of pest management questions. Call DSN 584-3773 or (410) 436- 3773. 


