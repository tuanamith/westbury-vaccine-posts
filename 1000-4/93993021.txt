


 <URL>  
Molecular Psychiatry Posted: June 9, 2004 
Thimerosal, Found In Childhood Vaccines, Can Increase The Risk Of Autism-like Damage In Mice 
A new study indicates that postnatal exposure to thimerosal, a mercury preservative commonly used in a number of childhood vaccines, can lead to the development of autism-like damage in autoimmune disease susceptible mice. This animal model, the first to show that the administration of low-dose ethylmercury can lead to behavioral and neurological changes in the developing brain, reinforces previous studies showing that a genetic predisposition affects risk in combination with certain environmental triggers. The study was conducted by researchers at the Jerome L. and Dawn Greene Infectious Disease Laboratory at the Mailman School of Public Health, Columbia University. 
Over the past 20 years, there has been a striking increase--at least ten-fold since 1985--in the number of children diagnosed with autism spectrum disorders. Genetic factors alone cannot account for this rise in prevalence. Researchers at the Mailman School, led by Dr. Mady Hornig, created an animal model to explore the relationship between thimerosal (ethylmercury) and autism, hypothesizing that the combination of genetic susceptibility and environmental exposure to mercury in childhood vaccines may cause neurotoxicity. Cumulative mercury burden through other sources, including in utero exposures to mercury in fish or vaccines, may also lead to damage in susceptible hosts. Timing and quantity of thimerosal dosing for the mouse model were developed using the U.S. immunization schedule for children, with doses calculated for mice based on 10th percentile weight of U.S. boys at age two, four, six, and twelve months. 
* Abnormal response to novel environments; 
* Behavioral impoverishment (limited range of behaviors and decreased exploration of environment); 
* Significant abnormalities in brain architecture, affecting areas subserving emotion and cognition; 
* Increased brain size. 
### 
Citation source: Molecular Psychiatry 2004 Volume 9, advance on line publication doi:10.1038/sj.mp.4001529 
ARTICLE: "Neurotoxic effects of postnatal thimerosal are mouse strain-dependent" 
M Hornig, D Chian, W. I. Lipkin 

