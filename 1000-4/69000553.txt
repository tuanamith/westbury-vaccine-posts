


Chris wrote: 
In case you actually have vaccines in mind, parasitic disease bacteria "need" to change - to retain any useful mutations that they get - because species that the bacteria use as hosts are constantly revising their immune system, by the same means, to recognise and resist the bacteria.  A vaccine is a device that stimulates your immune system further, to better recognise and resist the current version of the disease. 
If all of the human race lived in a very small Ark rather recently, then it's interesting that European colonists' commonplace diseases devastated native American populations.  Bacteria aren't the same as viruses, but the genetics of the H5N1 virus also are interesting.  It has been closely studied; I don't recall that it has been found to have lost its information.  Anyway, if that was going on all the time, how long would it take for the virus to run out of information completely? 
This, by the way, is not just made up.  A bacterium or a virus really has a finite store of genetic information and it can be defined in a consistent way, which may, however, not be useful.  Nevertheless, at one level, each "letter" in a gene, one from a set of four chemical groups which are repeated alternately in a long DNA molecule, contains an amount of information equivalent to 2 "bits" of data inside a computer.  However, data and information are not the same thing. 

Evolution proceeds from simple to complex by a slow process of many, many small steps.  If you read reviews or comments on Richard Dawkins' book _Climbing Mount Improbable_ then you'll probably get the idea. 


