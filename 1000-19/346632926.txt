


Job Title:     Scientist-Cell Culture Process Development Job Location:  MD: Rockville Pay Rate:      70-80K Job Length:    full time Start Date:    2008-02-26 
Company Name:  ETS INC Contact:       Recruiting Administrator Phone:         EMAIL ONLY Fax:           000-000-0000 
Description:   We are seeking a highly motivated individual with ample experiences to play a key role in the insect cell culture process development for our VLP vaccine and biologic product candidates, as well as supporting the manufacturing of pre-clinical and clinical study materials. The individual will be hands-on working on the development, scale-up and optimization of the cell culture production processes. This position will be located at our Rockville, Maryland facility and will report to the Manager of Upstream Development.  
Responsibilities Include but are not limited to: 	Develop robust, high titer, scalable and economical upstream cell culture processes for VLP vaccine and biologic product candidates.  	Prepare and characterize cell banks and virus seed stocks, formulate and improve media, develop fed-batch cell culture, scale up and optimize processes, and perform critical operations as needed in out cGMP manufacturing facility to deliver materials for pre-clinical and clinical studies.  	Collaborate and coordinate with colleagues from basic research team, cell culture team, analytical team, manufacturing team, and QA/QC team.  	Contribute to ensure that the laboratories/manufacturing facilities are in full compliance with all cGLP/cGMP requirements 
Minimum Requirements: 	Degree in chemical engineering 	PhD with 0-3 years experience, MS with 3-5 years or BS with 5 or more of experience.  	Multiple years of hands-on experience in bioprocess R&D environment performing some or all of the following; cell and virus banking medium formulation and optimization, fed-batch development, virus infection, Wave/stirred bioreactor operation. 	Knowledgeable of cGMP requirements regarding the manufacture of biologics.  	Knowledge of process optimization, scale-up and technical transfer.  	Demonstrated verbal and written skills in communicating regulatory and technical information. 

Additional Skills: 	Knowledge and experiences such as cell biology, centrifugation, tangential filtration, normal flow filtration, and assay development are a bonus but not required.  	Knowledge of process characterization and validation. 
Please refer to Job code DSR-SCI when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



