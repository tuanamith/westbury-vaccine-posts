


In article < <EMAILADDRESS> >,  Chris Malcolm < <EMAILADDRESS> > wrote: 

.................. A search for ada and Buse yielded this. 
Bill .................. 
Why diabetics don't want the flu 18:07 ET, Thu 10 Jan 2008 
by Terri Coles TORONTO (Reuters) - Flu season is unpleasant for everyone, but if you  have diabetes it can be even worse. If diabetics fail to manage their  disease while sick, the complications can be serious, the American  Diabetes Association warned in a statement this week. 
"Your average person will spend three or four days at home but they'll  do okay," said Dr. John Buse, president of medicine and science at the  ADA, about influenza. "In patients with diabetes, occasionally they even  have to be hospitalized." 
When a person has an infection, the physiologic stress on the body can  cause their blood sugar to rise, Buse said.  This is of particular concern to diabetics, who must monitor their blood  sugar because levels that are either too high or too low can be  dangerous. The body burns stored fat for energy when there is no food  available, and if insulin is not taken regularly, particularly in type 1  diabetics, a waste product called ketones can begin to build up, he  said. High ketone levels can lead to ketoacidosis, which can result in a  coma or death. 
As well, some drugstore medications commonly taken to alleviate cold and  flu symptoms may pose a risk to diabetics also dealing with blood  pressure, Buse said. Cough syrups containing decongestants can raise  blood sugar and blood pressure, and hypertension is an issue for about  half of diabetics. Though they may relieve some symptoms, these  medications haven't been demonstrated to actually reduce the duration of  colds or flu, he said. 
"It's not that nobody with diabetes can take these products," Buse said.  "As a rule of thumb, if people don't know that it's okay to take it,  it's probably better to err on the side of not taking it." It is  generally better to put something on the area that is bothering you  instead of ingesting something that has to work through your entire  body, he suggested -- for example, choose a nasal spray to relieve a  stuffy nose instead of an oral decongestant. 
Diabetics who become ill this winter can avoid more serious  complications by taking extra care to manage their condition, Buse said.  Unless a doctor tells them otherwise, diabetics should continue to take  their medication as usual, even if they are not eating normally, he said. 
While they are sick, patients should eat as much as is comfortable for  them, Buse said -- the ADA recommends 15 grams of carbohydrates per hour  -- but this may be difficult during some illnesses. Nevertheless,  liquids are key to preventing dehydration, and water, tea and broth are  better choices than sugar-laden juices, which can raise blood sugar. 
In addition to monitoring their blood sugar regularly for dangerous  rises or drops, the ADA also suggests monitoring urine for ketones. If  blood sugar is testing too low, drinking juice with about 15 grams of  carbohydrates once an hour, or a ? cup of apple juice or one cup of  milk, can help. 
The ADA advises diabetics to contact their doctor if ketones rise or are  present in the urine for more than 12 hours. The same is true if you  have vomiting or diarrhea for more than six hours, if you have a fever  that increases or lasts longer than a day, if you experience abdominal  pain or if you can't control your blood sugar. 
Because it can be hard to reach a health care professional quickly, it's  helpful for diabetics to have a plan in place with their doctors, Buse  said. "The thing to do is to know what to do before you get sick." 
It is also recommended that diabetics receive the influenza vaccine  every year, Buse said. Pneumonia isn't a winter-specific illness, he  said, but diabetics should be vaccinated against that as well -- once  before age 65, and once after. 
Hand-washing is the best known weapon for cold and flu prevention, Buse  said, but if you get sick, taking care to monitor your diabetes can help  you get over it quickly and without complication. 
"The bottom line is there's just more consequences to colds,  potentially, in patients with diabetes than in the general population." 
 <URL>  2F5D91.html 
--  Garden in shade zone 5 S Jersey USA ICAO = KMIV     Millville Weather Lat 39.5982  Long -75.0358 

