


Job Title:     Co-Op Assignment 2008; Biologics (B. Williams) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-02-23 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-Op Assignment 2008; Biologics (B. Williams) &#150; BIO001713 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The co-op student will work on a team in the Antibody Engineering Group in the Biologics Research Department on the discovery, development, and engineering of therapeutic antibodies and/or other types of protein therapeutics to specific disease-related targets. The co-op student will also become involved with the testing and analysis of antibody phage display libraries. A basic level of understanding of general laboratory procedures (making solutions, doing dilutions, lab calculations, etc), molecular biology, immunoassays, and cell culture are required. Experience with antibody/protein characterization and purification, phage display library manipulation, or DNA sequence analysis would be a plus. This is a 6 month co-op assignment targeted to start in June 2008 (end in December 2008).  
Qualifications - Pursuing degree in Biology, Biochemistry, Chemistry or related discipline - Grade Point Average (GPA) of 3.0 or higher preferred - Previous experience doing independent research in academia or industry preferred - Applicants must be available for full time employment for 6 months for this assignment targeted to begin in June 2008, and - Applicant must be currently enrolled in an academic program and returning to school following this assignment. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition# BIO001713or opportunities that meet your qualification.  Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  02/22/2008, 08:47 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/26/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-381236 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



