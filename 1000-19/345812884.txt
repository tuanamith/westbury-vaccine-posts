


< <URL> >: 
----- Begin Quote ----- 
One of the world's leading experts on the Aids virus said last night he was profoundly depressed about the prospects of a HIV vaccine and that scientists were clutching at straws in their attempts to develop one. 
David Baltimore, who won a Nobel Prize in 1975 for work that was crucial to the discovery of HIV in 1983, said that the past 25 years has been a story of abject failure in terms of developing a vaccine, with still no sign of a breakthrough. 
Dr Baltimore, professor of biology at the California Institute of Technology, said that some scientists are now talking openly about the prospect of never being able to produce a vaccine... 
[...] 
Millions of pounds of research funds have been spent on trying to develop a HIV vaccine and the best scientists around the world have devoted years to the problem, he said. "But all of those things sound like incremental improvements on zero, and if you improve on zero you generally get more zeros. So we do have to be thinking about this in a very different way," said Dr Baltimore. 
Referring to an American football term that describes the last, desperate attempt to score from a throw of the ball, Dr Baltimore said scientists now are "doing a Hail Mary" in terms of vaccine research. "I say that because I don't want to pretend that we have found the route to a new vaccine. What we're trying to do is to use a combination of gene therapy, immunotherapy and stem cell therapy, all which are trendy and difficult," he said. "HIV has found ways to totally fool the immune system. That is an established fact. So we've got to do one better than nature." 
One clinical trial of a vaccine made by the drug company Merck had to be stopped because it appeared to make people more vulnerable to infection than if they refrained from vaccination, he said. 
"We were hopeful that we would at least see a glimmer of efficacy," he added. "Well we saw a glimmer of its reverse, that is there was some evidence that people who were vaccinated got a higher rate of infection than the people who were not vaccinated.  
"The conclusion that was loud and clear was that the vaccine wasn't worth anything. [We] should be able to find a way around Mother Nature and produce a vaccine that is effective but so far we haven't done that and the failure last year of the Merck vaccine was a jolt." 
In 1986, Dr Baltimore wrote a report saying that an Aids vaccine was at least 10 years away and every year since then he has continued to say that a vaccine against HIV is at least 10 years away.  
"I still think that an Aids vaccine is at least 10 years away," he said. "You are quite within bounds to say, well if it's been 10 years away for the last 20 years does that really mean that it's never going to happen? And there are people saying now that it will never happen." 
"I'm not prepared to say that because I don't want to take a pessimistic stance. I want to take an optimistic stance and say this is too important to give up on." 
----- End Quote ----- --  < <URL> /> Moible: +447939991519 4,781 days and counting... 

