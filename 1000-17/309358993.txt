


Job Title:     Associate Automation Engineer Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-24 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Associate Automation Engineer &#150; ENG001467 
Job Description  
Submit 
Qualifications Desired:A working knowledge of the regulatory requirements for pharmaceutical applications is desirable. Highly desired are skills in PLCs (AB/Siemens), DCSs (DeltaV), Batch Operations (S88/S95), IT, and OEM equipment. Lean/Six Sigma experience desired. Education Requirements: B.S., B.A, or M.S. in Information Technology, Science, or Engineering, or related field. Strong leadership, interpersonal and communication skills (written and oral) are required. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #ENG001467. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  11/23/2007, 12:00 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/07/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-355136 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



