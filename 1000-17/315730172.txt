



 <URL>  
A Proposal to Require Flu Vaccines for Preschool 
By JILL P. CAPUZZO December 9, 2007 


The New Jersey Public Health Council is expected to vote tomorrow on a rule that would require flu vaccines for any child entering day care or preschool. If it is approved, New Jersey would become the first state in the country to impose that mandate. 
The flu vaccine is one of four that the council will consider. There is a vaccine that would be given to children entering day care or preschool to protect against pneumonia, and two others that would be given to those entering sixth grade: one to guard against meningococcal disease, a fast-killing strain of meningitis, and the other an additional booster of a three-part shot already administered at a younger age against tetanus, pertussis and diphtheria. 
If approved, the meningococcal vaccine requirement would also be a first in the nation for public school students. Currently, Massachusetts requires this vaccine for high school students at boarding schools. New Jersey and other states also mandate this vaccine for college students living in dormitories. 
The final decision on the new vaccines will be up to Fred Jacobs, the states health commissioner, who will take the recommendation of the council under advisement, according to a spokesman. Currently, New Jersey requires that students be given a series of vaccines against nine diseases. The new vaccines would be required as of next September. 
Since the new vaccines were first proposed last December, parents and doctors who oppose them have protested against the Department of Health and Senior Services. Thomas Slater, a spokesman for the department, noted that there had been a good amount of public comment from both sides, and a very extensive review period leading up to the meeting tomorrow. 
At a public hearing in January, about 30 people complained about the proposed vaccines, citing possible side effects and insufficient evidence that they help prevent disease. On Friday, a group opposing the new vaccines held a news conference at the State House in Trenton. And people will have a chance to speak at tomorrows meeting, at the health department building in Trenton. 
The decision to propose the additional vaccines, especially the one for the flu, was based on recommendations by the federal Centers for Disease Control and Prevention and the Food and Drug Administration, according to Dr. Eddy Bresnitz, New Jerseys deputy commissioner of health and the state epidemiologist. Each year, 108 of every 100,000 children 5 or younger are hospitalized with complications from the flu, and about 100 die, according to the C.D.C. 
If you have children and they go to day care, theyre constantly getting sick, said Dr. Bresnitz. He noted that children were also the best transmitters of influenza virus. 
We believe this will limit the disease, decrease hospitalization and prevent death, he said, not only for the children affected but also in the community at large. 
At present, 13 other states require pneumonia vaccines for preschool students, and 27 states have added the extra three-in-one booster for sixth graders. Two other states, Kansas and Vermont, recommend the meningococcal vaccine. 



