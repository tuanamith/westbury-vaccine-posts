


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Staffing Consultant, Scientific and Medical--Merck &amp; Co.,Inc. &#150; HUM001195 
Job Description  
Submit 






Qualifications 
.Bachelor&#8217;s degree required, master&#8217;s degree preferred. .At least 5 years of recruiting/staffing experience required, preferably in a large, matrixed organization (3 years exp required if in possession of a graduate degree) .Experience with direct sourcing and qualification of candidates, including use of the Internet, conducting telephone and on-site interviews utilizing behavioral-based interviewing techniques required. .Requires thorough knowledge of recruiting/staffing database systems (experience using Taleo a plus) .Requires intermediate knowledge of Microsoft Office Suite (Excel, PowerPoint, Word). .Demonstrated strength in written and verbal communication as well as strong relationship building / maintenance, facilitation and influencing skills required. .Pharmaceutical/Biotechnology industry recruiting experience is a plus. .Project management expertise in a deadline driven environment is a plus. .International Recruiting experience is also a plus. .Behavioral based interview certification is a plus. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # &amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;HUM001195. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestation. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  Yes, 15 % of the Time  



Please refer to Job code merck-360296 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



