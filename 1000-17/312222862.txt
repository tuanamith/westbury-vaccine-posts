


Job Title:     Secretary Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-30 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Secretary &#150; ADM003455 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We currently have an open position for a Secretary at our West Point, PA site. Please note, this is a Union position. The starting rate for this position is $22.925/hour. The incumbent will be working under the direct supervision of the Senior Director, Director, Associate Director, Veterinarian, Managers or other staff members. The duties for this position include, but are not limited, to the following: * Prepares memoranda and generates reports as required for the various groups within the department. * Maintains electronic databases and paper files as required for Regulatory/GLP/GMP requirements, personnel records, safety programs, training programs, medical data, SOPs, necropsy data, work schedules, overtime, attendance records, expense reports and other pertinent data. * Prepares and coordinates requisitions to purchase animals, materials and supplies via the various purchasing systems. * Prepares and maintains vacation schedules, schedules meetings, conference rooms and interviews. * Maintains all records in compliance with MRL Records Retention Schedule and State and Federal regulations. 
Qualifications 
NECESSARY EDUCATION: High School Diploma or equivalent NECESSARY EXPERIENCE:  3-5 years general secretarial experience with expert skills in Microsoft Office Software (Word, Excel, Access, and PowerPoint). 
Job requires activities related to the use of animals in a research environment. Applicant must be aware of and be comfortable with those activities.  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # ADM003455. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  11/28/2007, 03:10 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/05/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-354716 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



