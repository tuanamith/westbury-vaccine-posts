


Job Title:     Franchise Trainer - Neurology Job Location:  PA: Lansdale Pay Rate:      Open Job Length:    full time Start Date:    2007-12-02 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Franchise Trainer - Neurology &#150; TRA000515 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Neurology Franchise Trainer will perform the following functions: * Design, lead and implement Neurology training for new and incumbent representatives. * Take on a leadership role in identifying and developing ongoing training needs in support of the Neurology Franchise. * Work closely with key stakeholders within the Neurology Franchise. * Work and partner with Sales Training and Professional Development (ST &amp; PD) Brand Trainers and Professional Development Trainers to identify, integrate and help execute on department strategy. 
Qualifications Educational requirements: *  Four (4) year degree from an accredited college or university. Required: *  Minimum of two (2) years experience as a Neurology Specialty Representative (NSR) *  Strong interpersonal and communication skills *  Demonstrate qualitative skills *  Excellent project management skills Core Competencies: . * Project and Time Management - ability to personally oversee and drive project objectives in a timely manner * Facilitation Skills - ability to lead classroom activities for new and incumbent representatives * Innovation - ability to think creatively to address complex business issues * Leadership - ability to lead, partner, motivate and direct teams to achieve objectives * Influence - ability to position, advocate and present ideas and recommendations in order to obtain commitment and support of key stakeholders 
Desired:  * Proficiency with computers and Microsoft applications. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # TRA000515. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails.  All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-Lansdale   Employee Status  Regular   Travel  Yes, 15 % of the Time  
Additional Information   Posting Date  11/29/2007, 10:23 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/06/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-358316 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



