


Job Title:     Co-Operative Assignment-Pharmaceutical Pkg Tech &amp;... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-29 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-Operative Assignment-Pharmaceutical Pkg Tech &amp; Development &#150; CHE001241 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Responsibilities include execution of planned activities on the evaluation of container systems for supporting stability and marketing suitability studies for new pharmaceutical products in both solid and liquid dosage forms. Laboratory work includes determination of moisture sorption isotherms (MSI) for various products and packaging components, novel head-space analysis, installation of custom-made apparatus, design and implementation of various product characterization studies quantifying the interaction of moisture and oxygen with labile products, thermal characterization of materials, and use of non-destructive techniques to evaluate package integrity. Non- laboratory tasks include compilation and analysis of data, and mathematical modeling (Math-CAD) of packaging systems. This co-op program has been very rewarding since 2000 for both the students and our organization. Note that this is a paid co-operative assignment whereby a weekly stipend will be provded. Housing subsidy is not available as part of this program and if required by the student must be funded 100% by the student. 
Qualifications - Pursuing degree in Chemical Engineer (strongly recommended) or a related field - Grade Point Average (GPA) of at least 3.0 or higher; - Proficient with MS Office programs.  - Candidate must be available for full time employment (40 hours per week) for a period of 6 months targeted to begin in April 2008 - Currently enrolled in an academic program and returning to school following this assignment Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition # CHE001241.  Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular  
Additional Information   Posting Date  11/26/2007, 04:31 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/30/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  3 



Please refer to Job code merckcollege-350196 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



