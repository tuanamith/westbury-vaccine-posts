


Job Title:     National Medical Representative/National Professional... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-16 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   National Medical Representative/National Professional Representative &#150; PRI004208 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We currently have an opportunity in the National Business Group (NBG) for a National Medical Representative (NMR) to represent Merck and Merck products in a complex, dynamic business environment. This position is based at Merck&#8217;s office complex in West Point, Pennsylvania. Typical hours of operation of the NBG are 8 AM to 12 AM Monday through Thursday, 8 AM to 6 PM on Friday and 8 AM to 3 PM on Saturdays. Working hours are based upon business need and may vary during the operating hours of the NBG. The primary responsibility of the National Sales Representative is to effectively promote Merck products to physicians through the use of e-communication and video detailing computer technology. The NMR provides accurate information to physicians so Merck products will be prescribed appropriately. The NMR is responsible for achieving yearly objectives either individually or in a group. 
Qualifications Educational Requirements: * BS/BA degree, preferably in Science or Business. Requirements: * Ability to continuously learn, understand and convey complex information. * Flexibility/adaptability. * Must have ability to work flexible hours. * Must successfully complete required sales training * Excellent verbal communication skills. Desired: * 1-3 years outside sales experience is preferred. * Strong selling and computer skills. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # PRI004208. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails.  All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  Yes, 25 % of the Time  
Additional Information   Posting Date  11/14/2007, 11:52 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/21/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  2 



Please refer to Job code merck-354076 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



