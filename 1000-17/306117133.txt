


Job Title:     BS/MS-Biology/Biochemistry (Full Time 2007-2008) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   BS/MS-Biology/Biochemistry (Full Time 2007-2008) &#150; BIO001569 
Job Description  
Submit 
Opportunities exist in research and manufacturing areas and include opportunities in basic research, research &amp; development, process validation and manufacturing technical areas. Opportunities are available in analytical, biological, and vaccine development. Each of these opportunities provides a challenging environment to learn and contribute. The successful candidate will be expected to carry out research on assigned problems in the above listed areas, including applied development, problem solving, process / product improvement and laboratory testing in the above listed areas. Many analytical techniques are used, including both in vivo and in vitro methods, to characterize complex biological products and systems The candidate is expected to work in a team environment and provide all essential understanding toward addressing mechanisms responsible for product safety, efficacy and stability. Responsibilities will also include the analysis and interpretation of data for problem solving and predictive purposes. A company-wide program is available to facilitate individual development planning to prepare the individual for increasing responsibility according to either technical or management tracks support career path planning. This may include movement to other divisions of Merck &amp; Co., Inc. 
Qualifications BS or MS in Biology or Biochemistry is required. Degree completion no later than end of 2008 required. If you are the kind of individual who thrives on challenge and possess the technical, leadership and business skills that are of value to our business, we invite you to apply. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careers/university/to create a profile and submit your resume for requisition # BIO001569. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point     US-VA-Elkton     US-NJ-Rahway   Job Type  Standard   Employee Status  Regular  
Additional Information   Posting Date  08/16/2007, 08:39 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/15/2008, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00) 



Please refer to Job code merckcollege-325636 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



