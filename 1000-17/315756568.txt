


An ancient Middle-Eastern empire had already mastered the art of  biological warfare almost 3,500 years ago, according to an Italian  scientist. 
The Hittites of Anatolia, whose empire stretched from northern Turkey  into Iraq and Syria, were famed for their lethal chariots and skill with  horses. 
In the Biblical book of Genesis, the Hittites are named as the  descendants of Ham, through his son, Canaan. They are described as a  great power who dwell "in the mountains". 
Now Dr Siro Trevisanato, a molecular biochemist, has claimed that many  of the Hittites' glorious victories were down to their use of infected  sheep, which they would slyly introduce into cities they wanted to  conquer. 
"There is no doubt that these were the first weapons of mass  destruction," he said. "They were waging bioterrorism". 
The sheep carried tularemia, also known as rabbit fever, a devastating  bacterial disease that is a potential threat even today, since there is  no known vaccine. 
The disease can pass from animals to humans, causing enormous skin  ulcers and respiratory failure. 
Without antibiotic treatment, around 15 per cent of its victims die. 
Dr Trevisanato said he had spent years searching through ancient  accounts of Hittite conquests. In 1325 BC, when the Hittites sacked the  Phoenician city of Symra, on the borders of Lebanon and Syria, a  mysterious plague was recorded. 
"This is the first time we hear of the so-called Hittite Plague," he  said. "It appears in several documents. In my view, it is no accident  that it coincides with the first documented description of tularemia." 
The plague was described in letters to the Egyptian king Akhenaten. The  letter reports that donkeys, which also carry the disease, were banned  from the city, in an attempt to stop the illness. 
The method of attack was simple. The Hittites would leave the sheep  outside the targeted city. Locals would bring them in and either breed  them or eat them, spreading the disease. 
However, Dr Trevisanato noted, the Hittites paid a high price for their  tactics. An epidemic of the disease weakened their ranks a few years  after the Symra attack. 

--  Rob Cypher robcypher.livejournal.com Usenet vet '96 to infinity.... 

