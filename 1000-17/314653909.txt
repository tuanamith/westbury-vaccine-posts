




Stupid children generally sit at the front and hang on every word. These  form the "yes" men of the next generation. 


Psychiatry is about removing dissidents and spotting them early. 




Psychiatry was invented by two mass murderers who cut out peoples brains to  steal their money. 








RESEARCH 
at the Institute of Psychiatry 


Helping people like you 




Depression, stress, 
Alzheimer's Disease, 
bulimia, Attention Deficit 
Hyperactivity Disorder, 
schizophrenia... 
...mental health problems and 
diseases of the brain are not rare. 






Inside 
: 

Alcohol addiction Page: 18 
Alzheimer's Disease Page: 20 
Anorexia Page: 10 
Attention Deficit Hyperactivy Disorder (ADHD) Page: 6 
Autism Spectrum Disorders Pages: 4 | 6 
Bipolar Disorder Pages: 14 |15 
Bulimia Page: 11 
Chronic Fatigue Syndrome Page: 28 
Depression Pages: 3 |12 
Gulf War Syndrome Page: 26 
Heroin addiction Pages: 16 |17 




The answer is it's the way 
Every gene has a promoter 
capable of regulating how powerful its action is - like a dimmer switch on  an electric light. 
  Professors Caspi and Moffitt found that people who had at least one short  promoter in their genetic make-up 


and made people more susceptible to depression. 
chance of becoming depressed 
and anxious. Research teams 
around the world are getting the same results from different studies and  different tests. 




4 Social, Genetic and Developmental Psychiatry 






Why do different brains 
process information 
in different ways? 
  In addition, the research team is analysing information collected over the  internet from more than 600 sets 
of eight-year-old twins who do not have an ASD. 'This will help us assess  the 
roles of genes and environment in this psychological style,' she says. 


The results of her 
work showed 
'substantial 
genetic 
overlap' 
between 
maths 
and 


and intelligence and thus bring 
them a step nearer to 
understanding 
learning 
abilities 
and 


Social, Genetic and Developmental Psychiatry 5 






of Behavioural 
Genetics. 


school, it's much harder to fix.' 
An experiment is also planned 
can read it properly, we can predict 
problems 
and hopefully intervene to 
help.' 


Relationships with 
absent fathers count 
Children also emotionally 
  Similar findings came from a bigger study of 10,000 families, she says. 


ADHD diagnosis stops children being 'written off' 


child to a specialist clinic. 
Even though those clinics 
'If the disorder is diagnosed, 
we can control the symptoms with 


medication or behaviour therapy,' 
he says. 'But within current resources, not every child who would benefit  could be treated.' 
The research recommended 
have problems making friends and sustaining relationships with other  families because of the disorder. 
  'What's more, ADHD goes on into adulthood - people in their 40s attend the  specialist clinic at the Maudsley 


To follow the progress of 
symptoms and find out what 
happens as people grow older, 
IoP researchers interviewed a 
Brain pictures show ADHD differences 



Working together to solve the puzzle of autism 


Compulsive Disorder and motor tics? 
Scientists simply don't know 
Research has already shown that 
brains of young children with autism 
develop differently from those of their 
peers who do not have it. Their brains 
grow rapidly and become large early on. 


that make him or her more likely to develop an ASD would only do so if  exposed to 'environmental' 
or 'epigenetic' influences. 
  'The genes, though important, are not the complete picture,' 
says Professor Bolton. 'It is only 


Experimental 
'phone treatment 
The treatment offered to many 
But when the case notes 
any treatment at all - even though most, on average, had had the symptoms of  OCD for more 
than three years. 
Adoptive parents across England are  taking part in a trial to test methods 
that may help them support children 
with emotional problems and disturbed  behaviour who have previously been in  care. 


'Many children mentioned 
that they actually preferred having 
this did not seem to be a problem.' 
  Dr Heyman says she hopes treatment-bytelephone will 
now be tested on a larger scale. In future, it could be used by specialist  clinics throughout the UK. 


Supporting troubled 
foster children 
  'Multidimensional Treatment Foster Care in England' is based 
learn how to build decent relationships with their peers, adults and members  of their birth family. 
  The ultimate aim is to improve these children's chances in life. Many of  them have problems 
because of abuse or neglect. 


Channel 4 viewers got a taste of how  Cognitive Behaviour Therapy (CBT)  works 


In the UK, OCD is thought to 
  Medication in the shape of antidepressants that act in the serotonin  system can help temporarily - but 


once the tablets end, the OCD comes back with a vengeance. 
  'There is only one known effective treatment - and that's CBT,' says  Professor Salkovskis. CBT, however, 
So instead of offering CBT in 
they also want to tailor it 
to individual patient's needs. 
  'People with a mild problem can probably help themselves with guidance  from a web- 
after a year to see how successful the treatment has been. 


How does the brain 
process emotions? 
things.' Afterwards, everyone was asked to rate how anxious they had felt. 


  It's mostly daughters who develop anorexia nervosa, normally after an  emotional or life crisis - very few 
sons get this life-threatening illness. 
ROMs designed to 
help patients help 
themselves at home. 
The CD ROM for 
carers, developed with the support of the Eating Disorders Association,  includes information and 
advice about how best to talk about 
the illness, how to avoid conflict at meal times, how to encourage healthy  eating, how to manage crises and 
spot warning signs. 
'If our trial 
shows this is 
successful 
and useful, 
this could 
be made 
available 
throughout 
the NHS,' 
says Dr 
Schmidt. 
'The Carers' 
CD ROM could 
be made available 
through charities 
and through NHS 
Direct.' 
  The research team that developed this package were the 
2005 winners in the Innovative Service 


Delivery Category in the London NHS Innovation Competition 
are 
given advice 
and practical strategies 
from therapists - 
and the chance 
to share 
experiences. 
'Parents can sometimes feel it's "their fault" and it's good to meet other 
people who 
are in the 
same situation 
as you are,' says 
Dr Schmidt. 'We're 
trying to find out if 
working with groups of families is 
as effective - or more effective - than 
working with individual families.' 


Treating young women 
with bulimia on the net 
  People who have bulimia have often been fat or even obese, often 
have low self-esteem and are susceptible to the pressures of a culture that  values being thin. 


ordeal in their minds, get flashbacks, nightmares, frightening thoughts 
a general feeling of being unwell, stomach problems, dizziness and  discomfort. 


of thinking and behaving to help them cope with their feelings and symptoms. 
  The Omagh-based Northern Ireland Centre for Trauma and Transformation  (NICTT), set up 


Free workshops beat 
stress and depression 
  Dr Brown first started organising workshops for up to 25 people in  Birmingham, just over a decade ago. 
  'Studies have shown that only about 30 per cent of people with mental  health problems consult 
their GPs and I wanted to reach those people who don't talk to their doctor. 


Research led by the IoP's 
Avshalom Caspi, Professor of Personality Development, has 
shown that 25 per cent of the population has the type of genes that makes  them vulnerable. 
more likely to have symptoms 
their lives and lifestyle throughout their childhood and teenage years. 
on them, controlling them, for example - 
by the time 
they were 26. 


Only three per cent of people who had not smoked cannabis at 15 went on to  develop psychosis in early adulthood. 
We all have two copies of most 
genes and there are therefore three 
different possible combinations of 
COMT gene pairs - either two 'mets', 


two 'vals' or a pair consisting of one 'met' and one 'val'. Professor Caspi 


14 Psychosis 






Talking therapy works 
  Research funded by the Wellcome Trust is now being undertaken to try to  understand why CBT helps people with psychosis. 
Clinical Excellence: these guidelines recommend CBT for patients 
with psychosis. 




Psychosis 15 






  The information collected through AESOP will not only help the research  team learn more about the causes 
of psychosis, it will also help make recommendations for better psychiatric  services. 'At the moment, the 
'By finding out what's happened 
Helping young people 
before they become ill 
  'People with symptoms which may indicate the start of psychosis 


  A study of records of people in Camberwell, south London, 
  The information collected by Dr Noel Kennedy while he was working 
OASIS staff see young people at 


take their own lives, and 47 per cent attempt suicide at least once, 
says Clinical Researcher Dr Rina Dutta. 
risk of developing psychosis through contact with local GPs,' says Professor  McGuire. 
  'Surprisingly little is known about the period before the onset of  psychotic disorders,' says 
Professor McGuire. 


has recently transferred from the Home Office to the Department 
of Health, which in turn has asked Primary Health Care Trusts to deliver  effective services. 






'Managing' heroin habits 


Heroin addicts often get caught up 
  Methadone is an opiate cousinonce-removed from heroin, used 
  So what if their addiction were managed - if they injected heroin 
The experiment will run for 
for injection. 
  The aim is to find out the success rates of these different treatments. 


Addictions 17 






or has been bought on the streets, by analysis of a urine sample. 
  'If the trial works, the people coming to the clinics will succeed 
Allowing addicts to inject heroin 
in a safe, supervised environment isn't a new idea. Clinics where people can 


make it work properly.' 
  The research will include an analysis of the cost-effectiveness 


Life-saving 'antidote' 
home could keep people alive 
until the ambulance comes.' 
  'The views of family members are crucial, because they would often be the  first at the scene.' 


Quit smoking 
  A team of IoP researchers are trying to find out - with the help 
of 1,800 volunteers recruited through stop smoking clinics all over London  and in south-east Essex. 
'As well as looking at success 
a history of mental health problems? Does your genetic make-up count?' 
Risky sex and alcohol 



in schools to try out new, personality-tailored  ways of persuading them not  to drink too  much or take drugs. 
alcohol and 
drug abuse,' says Natalie Castellanos, Co-ordinator of Preventure, the name  of the school-based new programme which is 
funded by 
the charity Action on Addiction. 
The 
Preventure 
team, headed by Dr Patricia Conrod, is 


The initial survey of 4,000 
teenagers showed that a third of 
A study in Canada that used 


Neurodegeneration and Brain Injury 19 








symptoms and experiences of getting help - and then tested different ways of  making sure their voices are heard. 
The BUILD 
project (Building User InvoLvement in MND) found that most people  interviewed had never 
'The project found that people 
The BUILD team set up 
a users' group which met to 
discuss experiences and make recommendations - and also set up 
a network so people who didn't want to go to meetings could have their say. 
'But it is hard to run a users' 
  'The website works because people can access computers in 
their own time from home and it gives them 
the opportunity to share views and communicate with people 
in a similar situation 
around the world. Health professionals 
can listen to their views and contribute too.' 


Stem cell research 


20 Neurodegeneration and Brain Injury 






Trying to slow the progress of Alzheimer's Disease 


One in five people aged over 85 have  Alzheimer's Disease. 'There are not  many people who are not touched 
Alzheimer's Disease is caused 


dressed and cooking. The parts of our brains that govern vision and movement  are spared. 
  'There are already drugs available to treat the symptoms of the disease - 




Neurodegeneration and Brain Injury 21 






Finding out about different 
types of Parkinson's Disease 


  So the next step is to try to find a way of cutting down the amount 
them up again. 


  With the help of some 800 patients living in the UK, France and Germany,  IoP researchers and colleagues 


affects the movements of the eyes and results in 'eye palsy' - which means  patients can only look straight ahead. 
With this in mind, another team 
  The Centre has already embarked on work funded by the Multiple Sclerosis  Society to look at the needs of people with MS. 


22 Health Services Research 
Do you think it's 'bad' to have 
a mental health problem? 


change public perception? 
The results indicate that 
making education about mental health a routine part of the PSHE curriculum  did make a difference: 
the questionnaires completed after 
the second workshop revealed positive and significant changes in attitudes,  particularly amongst girls. 
 Many of the 200-plus Kent 
to assume that everyone is violent and to treat people with respect. 


Health Services Research 23 
...if the answer's no, 
why use these words? 


Abnormal Bananas Barmy Basketcase Bonkers Brain-damaged Brain-dead 
Cabbage Cracked up 
Crank Crazy Cripple Dangerous eDens 
Deranged Dim 
Disruptive Disturbed Div Do-Lally Dumb 
Faggot Fierce Flid 
Freak Fruitcake Funnyin-the-head Handicapped 
Hard-to-listen-to Hard- 
to-speak-to Hard-to- 
understand Headcase 
Hollow-in-the-head 
Insane Lights on but 
no-one at home Loony Lost their marbles 
Mad Maniac Mental 
Mong Mongrel Mute Not all there Not right upstairs Nut Nutcase Nutter Nutty  as a fruitcake 
Odd Off-their-head 
Off-their-rocker Off-their-trolley Out-of-your-head Psycho Queer 
e Deranged Dim 
Disruptive Disturbed Div Do-Lally Dumb 
Faggot Fierce Flid 
Freak Fruitcake Funnyin-the-head Handicapped 
Hard-to-listen-to Hard- 
to-speak-to Hard-to- 
understand Headcase 
Hollow-in-the-head 
Insane Lights on but 
no-one at home Loony Lost their marbles 
Mad Maniac Mental 
Mong Mongrel Mute 
Not all there Not right upstairs Nut Nutcase Nutter Nutty as a fruitcake 
Odd Off-their-head 
Off-their-rocker Off-their-trolley Out-of-your-head Psycho Queer 
Retard Sad Scary 
Unstable Veggies 
Violent Wacko Wacky 
Weird Wrong-inthe-head Abnormal Bananas Barmy Basketcase Bonkers 
Brain-damaged Brain-dead Cabbage Cracked up Crank Crazy 
Cripple Dangerous 
e DensDeranged Dim 
Disruptive Disturbed Div Do-Lally Dumb 
Faggot Fierce Flid 
Freak Fruitcake Funnyin-the-head Handicapped 
Hard-to-listen-to Hard- 
to-speak-to Hard-to- 
understand Headcase 
Hollow-in-the-head 
Insane Lights on but 
no-one at home Loony Lost their marbles 
Mad Maniac Mental 
Mong Mongrel Mute 
Not all there Not right upstairsNut Nutcase 
Nutter Nutty as a fruitcake 
Odd Off-their-head 
Off-their-rocker Off-their-trolley Out-of-your-head Psycho Queer 
Retard Sad Scary 
Unstable Veggies 
Violent Wacko Wacky 
Weird Wrong-inthe-head Abnormal Bananas Barmy Basketcase Bonkers 
Brain-damaged Brain-dead Cabbage Cracked up Crank 
Crazy Cripple Dangerous eDens 
Deranged Dim 
Disruptive Disturbed Div Do-Lally Dumb 
Faggot Fierce Flid 
Freak Fruitcake Funnyin-the-head Handicapped 
Hard-to-listen-to Hard- 
to-speak-to Hard-to- 
understand Headcase 
Hollow-in-the-head 


...you could be describing one of these people. 




24 Antisocial Behaviour 
Violent crime could be cut 


People with mental health problems 
She studied the medical 
if men with schizophrenia are likely to commit violent crimes in future when  they are first admitted to 
general psychiatric hospitals. 
  Professor Hodgins acknowledges that most general psychiatric hospitals do  not have the resources to do 
this. 'Most do not have enough 
staff and resources to carry out 
the assessments of patients that are needed, give specialist treatments or  arrange intensive treatments 
after discharge,' she says. 


Psychology and Medicine 25 


Patients may not 
understand treatment 
transfer to a nursing home, he adds. 


Cognitive Behaviour Therapy could help Irritable Bowel Syndrome 


First used to help people with 
depression and anxiety in the 1960s, 
CBT was developed by American psychiatrist and psychotherapist Aaron T Beck. 
  It involves trying out both new ways of thinking and new, practical ways  of dealing with problems. 
Dr Hutton says there aren't 


26 Psychology and Medicine 
What's happened to 
Gulf War veterans? 


Previous studies have shown 
the quality of their life. 
  In the UK, veteran groups estimate some 6,000 people who served in the  Gulf War have unexplained ill health - 


including depression, mood swings, aching joints, dizziness, headaches and  chronic fatigue. 
'We have shown that going to the 
Gulf in 1990/91 did affect their health,' 


The Iraq War: what impact? 
The physical and psychological health of troops sent to war in Iraq is  already being studied. 
  He says there are already concerns about the psychological health of Iraq  War veterans. 



in which these vaccines were given may have contributed to future ill  health. 
Another study in search of 
an explanation for the ill health experienced by Gulf veterans 
had been given different 'labels'. 
in the shape of chemical and biological weapons. Perhaps the stress of war 


Psychology and Medicine 27 








28 Psychology and Medicine 






Navy tests new way 
to cope with trauma 


The Royal Navy trial comes in the wake of an analysis of research 
  'But what the review of research shows is that there is no difference  between people who are given 


Psychological Debriefing and those who aren't - the same number go on to  develop psychiatric disorders. 


Family therapy works for teenagers with chronic fatigue 


'We know very little about 
   'We are now awaiting the results of research designed to find out what  part IQ and expectations play,' she says. 


One third of patients 
seen by GPs have a 
serious mental health 
problem. Our work 
helps these people. 




30 Information about the Institute of Psychiatry 
The Institute 
of Psychiatry 






Working with others 
The local partnership with the Maudsley Hospital and the rest 
  The IoP and the University of Manchester work together to manage the UK  Mental Health Research Network, responsible 
  People from the IoP team up with other researchers not only in the UK, but  across the world. 


Service users 
to improve the quality of care on offer. 
Funding 
that pays for research is won 
through competition. 
Sainsbury Centre 
for Mental Health 


Information about the Institute of Psychiatry 31 






State-of-the-art 
resources 
More than £50 million has 
been spent on upgrading the IoP campus over the past five years. This  investment has brought together staff and students 
working in the same specialist fields in purpose-designed 
centres with state-of-the-art resources. 


Top quality standards 
Ethics 
Sharing our results 
www.mentalhealthcare.org 
is designed especially for families, carers and friends of people with  mental health problems and was 


Published by the 
Institute of Psychiatry De Crespigny Park Denmark Hill 
London SE5 8AF 
www.iop.kcl.ac.uk 
Assistant Director of Research and Development Gill Dale 
 <EMAILADDRESS>  
Chairs of Research Groups Addictions 
Ian Stolerman 
Antisocial Behaviour 
Sheilagh Hodgins 
Disorders of Childhood Eric Taylor 
Emotional Disorders 
Paul Salkovskis* 
Health Services Research Sube Banerjee* 
Neurodegeneration and Brain Injury Brian Anderton 
Psychology and Medicine Simon Wessely 
Psychosis 
Robin Murray 
Social, Genetic and 
Developmental Psychiatry Peter McGuffin 
Written and designed 
by Jane Smith and Sophie Gibson Inside Out, 020-7655 0885 
Printed by Calverts 
July 2005 


For more information 
about research at 
the IoP, courses and 
research opportunities 
for postgraduate 
students 
visit 
www.iop.kcl.ac.uk 









































