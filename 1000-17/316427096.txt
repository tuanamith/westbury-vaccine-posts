


Hepatitis B Vaccine Is Not Linked To Multiple Sclerosis In Childhood 
04 Dec 2007=A0=A0=A0 
Vaccinating children against the hepatitis B virus does not seem to raise that child's risk of developing Multiple Sclerosis, according to an article published in Archives of Pediatrics & Adolescent Medicine (JAMA/Archives), December 2007 issue.  
Numerous studies have looked at the potential link between the hepatitis B vaccine and MS (multiple sclerosis) among adults, explain the researchers.  
The majority of them did not find a notable raised MS risk in the short or long term, except for one which indicated there might be a slight increased risk within 36 months of taking the vaccine (adults).  
The authors explain "Some of these epidemiologic studies have been criticized for methodological limitations. This controversy created public misgivings about hepatitis B vaccination. Hepatitis B vaccination in children remained low in several countries despite vaccination campaigns supporting early vaccination against hepatitis B in children as a means of inducing strong and long-lasting immunity and despite high levels of hepatitis B-related morbidity and mortality worldwide."  
Yann Mikaeloff, M.D., Ph.D., of H=C3=B4pital Bic=C3=AAtre, Le Kremlin Bic=C3=AAtre, France, and team analyzed data on 143 children who developed MS before reaching the age of 16 - they all had an episode of MS happen during 1994-2003. They matched each patient to eight control participants from the general French population - they were all of the same age, sex, and lived in the same area, but did not have MS.  
Vaccination records and data on family MS history (as well as other autoimmune diseases) were collected via telephone interviews.  
In the 36 months preceding the first MS episode, about 32% of both the 143 MS patients and the 1,122 had been vaccinated against hepatitis B.  
"Vaccination against hepatitis B within the three-year study period was not associated with an increased rate of a first episode of MS. The rate was also not increased for hepatitis B vaccination within six months of the index date or at any time since birth or as a function of the number of injections or the brand of hepatitis B vaccine," the researchers wrote.  
The writers concluded "Vaccination against hepatitis B does not seem to increase the risk of a first episode of MS in childhood."  
"Hepatitis B Vaccination and the Risk of Childhood-Onset Multiple Sclerosis" 
Yann Mikaeloff, MD, PhD; Guillaume Caridade, MSc; M=C3=A9lanie Rossier, MSc; Samy Suissa, PhD; Marc Tardieu, MD, PhD  Arch Pediatr Adolesc Med. 2007;161(12):1176-1182.  
Article URL:  <URL>  
*****Don't Cry Because It's Over...Smile Because It Happened.***** 


