


Job Title:     Sr. Research Chemist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2007-12-21 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Sr. Research Chemist &#150; CHE001276 
Job Description  
Submit 
In this role, you will support current medicinal chemistry efforts at MRL-Rahway under the supervision of a senior medicinal chemist. Working within a multi-disciplinary team of scientists, the candidate will be expected to contribute to the design and synthesis of new drug candidates. Responsibilities include: planning and conducting multi-step synthesis, interpreting spectroscopic data, solving synthetic problems and applying modern chromatographic techniques for the purification of targets. * Support on-going medicinal chemistry efforts on key projects. * Develop structure-activity relationships using biological and DMPK data. * Responsible for the design and synthesis of target molecules. * Participate in the growth and development of the Medicinal Chemistry Department. * Ensure accurate margin-BOTTOM: 0px"  * Ph.D. in organic chemistry with strong expertise in synthetic organic chemistry. * Strong record of productivity as evidenced by publications. * Post-doctoral and/or industrial experience is advantageous, but not required. * Must possess organization, analytical, and problem-solving skills. * Leadership qualities, excellent communication and interpersonal skills are necessary Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # CHE001276. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  12/18/2007, 08:31 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/25/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-361836 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



