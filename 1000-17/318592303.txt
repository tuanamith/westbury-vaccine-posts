



 <URL>  
NJ requires flu shots for preschoolers By LINDA A. JOHNSON, Associated Press Writer  Fri Dec 14, 2:09 PM ET 


TRENTON, N.J. - New Jersey on Friday became the first state to require flu shots for preschoolers, saying their developing immune systems and likelihood of spreading germs make them as vulnerable to complications as the elderly. 

State Health Commissioner Dr. Fred M. Jacobs approved the requirement and three other vaccines for school children starting Sept. 1, 2008, over the objections of some parent groups. 
The new requirements "will have a direct impact on reducing illnesses, hospitalizations and deaths in one of New Jersey's most vulnerable populations  our children," Jacobs said in a statement. 
A health advisory board Monday backed the new requirements on a 5-2 vote with one abstention after parents said they worried about the safety of giving young children dozens of vaccine doses. Some also say they don't want government making their medical decisions. 
Starting in September, all children attending preschool or licensed day care centers will have to get an annual flu shot, Jacobs said. That makes New Jersey the first state to require flu shots for preschoolers or older students, according to the American Academy of Pediatrics. 
New Jersey also will require preschoolers to get a pneumococcal vaccine and sixth-graders to get vaccines against meningitis, which New Jersey already requires for college dormitory residents, and a booster shot against whooping cough, which in recent years has seen a resurgence blamed on waning potency of shots given to infants and preschoolers. 
The four additional vaccines are recommended by the U.S. Centers for Disease Control and Prevention, the American Academy of Pediatrics and other medical groups. 
Some parents support proposed legislation that would give families a right to skip required immunizations by lodging a "philosophical objection," as some other states allow. The bill has been sitting in a committee without action for several years. 
New Jersey does grant an automatic exemption on religious grounds and allows exemptions for medical reasons. 
The new vaccines will be available for free for low-income families, and private insurers generally will cover the cost. 
___ 
NJ health department site:  <URL>  
New Jersey Alliance for Informed Choice in Vaccination:  <URL>  

2007 The Associated Press.  2007 Yahoo  


