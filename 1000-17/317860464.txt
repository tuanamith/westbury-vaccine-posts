


Job Title:     Merck Research Labs - Neuroscience co-op (12 month... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-12-15 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Merck Research Labs - Neuroscience co-op (12 month assignment) &#150; PHA000754 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. This co-op assignment will provide an opportunity for pharmacology, behavioral neuroscience, biochemistry or physiology students to work under the supervision and guidance of experienced Merck research scientists on either in-vitro or in-vivo research projects for a 12 month period, starting May/June 2008. The West Point, PA Neuroscience is a state-of-the-art organization involved in the discovery and development of novel therapeutic agents for the treatment of ophthalmic, degenerative, pain and sleep disorders, Alzheimers disease and schizophrenia. 
Qualifications Applicants must complete a minimum of 3 years of undergraduate science education by March 2008. Enrollment in a formal Pharmacology program is required. If you are the kind of individual who thrives on challenge and possess the technical, leadership and business skills that are of value to our business, we invite you to apply. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers/university/to create a profile and submit your resume for requisition # PHA000754. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular  
Additional Information   Posting Date  12/11/2007, 04:45 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/15/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  2 



Please refer to Job code merckcollege-360896 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



