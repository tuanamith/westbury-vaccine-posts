


Job Title:     Masters HR Leadership Program Intern Job Location:  NJ: Whitehouse Station Pay Rate:      Open Job Length:    full time Start Date:    2007-11-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Masters HR Leadership Program Intern &#150; HUM001136 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, associates have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding their career. 
Merck HRLP Interns are placed into organizations that match students&#8217; interests with assignments that will provide exposure to Merck&#8217;s business and to key initiatives in Human Resources. Interns will typically have one primary project, but will also have opportunities to work on additional projects in order to obtain a breadth of experiences. Each intern will receive a mentor and a buddy that will provide ongoing professional support and developmental feedback. During the summer, interns with have opportunities to meet with HR professionals from a variety of organizations including meeting and presenting to Merck&#8217;s HR Leadership Team. HRLP Interns can choose to work and develop skills in a variety of functions. Recent opportunities have included: Compensation, Decision Support, Diversity, HR Business Partner, Labor Relations, Organizational Development, Organizational Learning, Staffing, and Talent Management. 
Qualifications QUALIFICATIONS * Excellence in analytic skills (qualitative and quantitative), strategic thinking and problem-solving * Superior communication, presentation and interpersonal skills * Strong capacity for leadership and cross-functional teamwork in a diverse work environment * Ability to take initiative, responsibility, and demonstrate results-oriented project management * Aptitude for innovation and creativity * Demonstrate a high degree of ethics and values * Pharmaceutical, healthcare or related industry experience a plus Applicants must * Currently be enrolled in an M.B.A./M.A. in Business or an HR-related discipline plus several years work experience preferred * Be eligible to work in the US on a permanent basis (US citizen or Permanent Resident) without sponsorship * Have a minumum of one year work experience Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers/university/to create a profile and submit your resume for requisition # HUM001001. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-NJ-Whitehouse Station     US-NJ-Rahway     US-PA-West Point   Job Type  Internship   Employee Status  Regular   Travel  Yes, 25 % of the Time  
Additional Information   Posting Date  09/14/2007, 11:03 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-327276 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



