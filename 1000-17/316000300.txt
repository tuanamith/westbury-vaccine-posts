


x-no-archive: yes 
"They're sharing food, dancing, kissing - all those things they should be  doing at that age"  --Dr. Kristine Bertini, director of university health 
 <URL>  
"Rule on Shots Forces Some Students Off Maine Campuses" 
By Katie Zezima (published Dec. 10, 2007) 

The Maine Center for Disease Control and Prevention recently issued an  emergency order that full-time or resident students at the campuses - 5,996  people - must have had a second vaccination for mumps. The order came after  two cases of the virus were confirmed at the university; other cases are  suspected. 
"I shouldn't be here right now," said Caleb Field, a junior who has not had  two shots but was studying at the university's student center Friday  afternoon. "I'm waiting until they drag me down there. I don't like shots.  If I don't have to get it, I'm not going to get it." 
The university sent e-mail messages and letters to students saying they had  to provide proof of a second immunization by Wednesday. For those who did  not meet the deadline, the university deactivated their identification  cards, which grant access to dormitories and dining halls. 
It also issued lists of affected students to professors, directing them to  send the students home if they came to class. Officials said the students  could be escorted off campus by the university police. "The students are  essentially banned from campus until they show they have received the  immunizations," said Robert S. Caswell, the university's director of public  relations. 
Under Maine law, college students are required to have one mumps  immunization, usually done in childhood. Some pediatricians recommend  another dose a few years later. But after the two cases were confirmed,  officials wanted to ensure that the highly contagious virus did not spread  and ordered students to get a second vaccination. 
"Having a high level of immunity at a college will greatly reduce the  likelihood that it will spread," said Geoff Beckett, the assistant state  epidemiologist. 
The order sent students scrambling to get shots, to locate immunization  records, to beg to remain in class and, in some cases, to find a different  place to stay for a few days. 
"I grabbed some clothes, my laptop, my toothbrush and went to stay with a  friend," said David Clark, 22, who was not allowed into his dormitory room. 
Mr. Clark, a senior, said that he had received two vaccinations but that the  doctor's office where he got the shots as a child was closed until Monday  and could not fax a confirmation to the university until then. He said he  planned to stay away from campus and wait for his doctor's office to reopen  rather than to pay for a shot at the university clinic. 
"I don't have $50," Mr. Clark said. 
Health records indicate that nearly 300 students did not have the required  second immunization as of Friday afternoon, university officials said. Those  who are medically exempt, for reasons like being allergic to a shot, will be  allowed to attend class. About 20 students who objected to the vaccine for  religious or personal reasons have been asked to stay away from the  university for the incubation period of the virus, about 18 days, Mr.  Beckett said. 
The university has issued about 800 vaccinations in the last week, said  Kristine A. Bertini, director of university health and counseling. She said  college students are at particular risk for the virus, which is spread  through respiratory contact. 
"They're sharing food, dancing, kissing - all those things they should be  doing at that age," Dr. Bertini said. 
Some students said the university could have done a better job informing  them of the situation. 
Inga Bozsik, 26, a graduate student, said that she was kicked out of a class  Thursday night and that she went to the health center midday Friday to get a  shot so she could return to class as soon as possible. 
Ms. Bozsik said she ignored e-mail messages and letters because she had  received two shots. The problem, she said, was that she got one of them as a  child in Belarus and her papers need to be translated. 
"I don't think they're informing people properly," she said, adding that  "nobody is going to respond" to general e-mail messages. 
University officials said they gave students as much notice as possible. "We're  doing our best," said Judie A. O'Malley, a spokeswoman. "This is  unprecedented here." 
Twelve mumps cases have been confirmed in Maine since September, Mr. Beckett  said, the largest outbreak here in 25 years. The strain of mumps has not  been identified, he said. 
Last year, more than 1,000 cases of mumps were reported in an outbreak in  eight Midwestern states. Most of those affected were college students. 
Mumps is an acute viral illness. Its symptoms include headache, swelling of  the glands, fever and sore throat. 
The virus is rare and was near eradication before the outbreak last year,  said Jim Alexander, a medical epidemiologist at the federal Centers for  Disease Control and Prevention in Atlanta. 



