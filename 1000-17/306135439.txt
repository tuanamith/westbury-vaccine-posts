


Job Title:     Senior Research Chemist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2007-11-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Research Chemist &#150; CHE001231 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We currently have a position for a Senior Research Chemist within our Process Research department at our Rahway, NJ location of Merck Research Laboratories. Under the direction of a senior department member, the Sr. Research Chemist  will be responsible for designing, discovering  and developing synthetic approaches to novel drug candidates. The Senior Research Chemist will have access to state of the art facilities and will conduct independent research as a member of a project team with the objective to develop highly efficient processes for the synthesis of challenging target molecules.    
Qualifications Ph.D. in organic chemistry, or equivalent in experience is required. Post-doctoral training is highly desirable. Prefer a candidate who has specialized in synthetic methods development, reaction mechanisms, and/or physical organic chemistry. In addition, the candidate should demonstrate good communication skills and show potential for leadership responsibility. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # CHE001047. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  10/17/2007, 02:07 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/16/2007, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-346376 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



