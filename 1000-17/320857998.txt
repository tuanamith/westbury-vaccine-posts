


Job Title:     Engineer II -(Associate) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-12-21 
Company Name:  Acro Service Corporation Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Engineer II -(Associate)   
Location: West Point PA Classification: Technical Required Education: Bachelor / Diploma Required       Travel:  Client:  Position Type: Contract Start Date: Immediate Duration: 6 months 
Description  Required Skills Exp (Mos.) Comments Technical Support 12 Minimal 12 monts experience in pharmaceutical/chemical industry or related expirience 
Degree Type Major Required Preferred College B.S. / M.S. engineering or scientific field 
Qualifications: B.S./M.S. degree in appropriate engineering/scientific field. Minimum 3/4-years post-Bachelors degree experience in Production, Development, Process Engineering, Technical Services or acceptable related experience. Demonstrated ability to work both independently and as a part of a team. Proven analytical abilities. Demonstrated written and verbal communication skills. Responsibilities: The function of this position is to provide technical support to aseptic manufacturing operations. This role will focus on manufactured sterile pharmaceuticals and liquid and lyophilized vaccines. The principle functions of the incumbent are in four main areas. The areas are: 1) issuance and maintenance of batch manufacturing documentation, 2) investigation, resolution, and documentation of atypical processing events, 3) technical projects focused on profit improvements, corrective action close outs, process analysis and improvements, process changes, process troubleshooting and component issues, and 4) compliance activities. Individuals will develop working relationships with counterparts in other areas supporting aseptic manufacturing, testing, and release. Summarily, the individual will be required to utilize sound scientific and engineering principles to investigate process deviations and execute continuous process improvements within the context 
Job             Order: 24634 Contact             Phone: 505-797-2774                 Ext.   Contact       Name: Brenda Bevington Contact             Fax: 734-591-1217 Contact             E-Mail:  <EMAILADDRESS>  a style="cursor: hand;color=blue" 



Please refer to Job code acrocorp-24634 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



