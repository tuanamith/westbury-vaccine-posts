


Job Title:     BS/MS/PhD - Bio/Chemical Engineering Summer Internship 2008 Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   BS/MS/PhD - Bio/Chemical Engineering Summer Internship 2008 &#150; CHE001193 
Job Description  
Submit 
The successful candidate will apply engineering, science, and business skills to help Merck carry out its core functions. A number of different entry points are possible for an internship, in either our research or our manufacturing division during the summer of 2008. Chemical and Biochemical Engineering at Merck has the vital role of bridging the gap between new pharmaceutical and vaccine discoveries and commercial products. Chemical processes produce in bulk form, the high quality complex chemicals used in Mercks most valuable human health, animal health and agricultural products. We then convert these basic chemicals into safe effective pharmaceuticals through formulation and packaging processes. Biologically derived products follow a similar path. 
Qualifications Working towards completion of BS, MS or PhD degree in Chemical Engineering, BioEngineering, Biochemical Engineering or Biomedical Engineering. Completion of at least 1 full year of college prior to start of internship is required. This position is for a summer internship; therefore, applicants must be available for a 10-12 week duration during the months of May and August and a current student pursuing a degree listed above. If you are the kind of individual who thrives on challenge and possess the technical, leadership and business skills that are of value to our business, we invite you to apply. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers/university/to create a profile and submit your resume for requisition # CHE001193. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point     US-NJ-Whitehouse Station     US-VA-Elkton     US-NJ-Rahway     PR-West-Mayaguez   Job Type  Internship   Employee Status  Regular  



Please refer to Job code merckcollege-325657 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



