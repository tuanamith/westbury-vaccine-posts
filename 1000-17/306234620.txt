


Job Title:     Recruiting Technology Lead -- Merck &amp; Co.,Inc. Job Location:  NJ: Whitehouse Station Pay Rate:      Open Job Length:    full time Start Date:    2007-11-21 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Recruiting Technology Lead -- Merck &amp; Co.,Inc. &#150; HUM001185 
Job Description  
Submit 




Reporting to the Director, Recruitment Technology &amp; Process, the Recruiting Technology Lead has responsibility for Taleo system management and integration as well as primary vendor management for various recruitment vendors. 
Act as primary business owner of the Taleo system within Merck .Lead the successful implementation and/or completion of various Taleo related projects including version and service pack upgrades, global deployments, system enhancements, user management, system configuration changes, etc. .Partner with Merck IT to ensure that the Taleo system and its users are adequately supported .Consult on all recruiting compliance issues and implement technology enhancements desirable to facilitate compliance with all pertinent government regulations as necessary. .Work with Merck recruitment ad agency to develop and implement corporate-wide annual internet sourcing strategies. .Provide training and information to the R&amp;amp;S team on various recruiting technology tools included in the annual internet sourcing strategy .Manage various internet sourcing strategy vendors. .Assist in the generation and analysis of Staffing reports and metrics 
Qualifications 
Bachelors degree required; master&#8217;s degree or MBA preferred. .At least 5 years of direct recruitment technology implementation and management experience or recruiting related experience required. At least 3 years of direct recruitment technology implementation and management experience or recruiting related experience required if in possession of a graduate degree. .Experience with selection, design, implementation and management of technology-based sourcing and direct sourcing tools and ATS systems required .Working knowledge of compliance issues relative to OFCCP, EEOC and Sarbanes Oxley required .Project management expertise in a deadline driven environment desired. .Demonstrated competencies in relationship building / maintenance, facilitation and influencing skills preferred. .Global recruiting process/technology experience is highly desired .Experience with Taleo ATS preferred. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # &amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;HUM001185. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestation. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
 Profile   Locations  US-NJ-Whitehouse Station   Employee Status  Regular   Travel  Yes, 15 % of the Time  
Additional Information   Posting Date  11/19/2007, 01:01 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/19/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-356876 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



