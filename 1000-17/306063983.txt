


Job Title:     Senior Technical Services Engineer - SORC Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-15 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Technical Services Engineer - SORC &#150; ENG001459 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Responsible for providing administrative and technical utility support to ensure cGMP compliance and ongoing efficient operations in production and research facilities and the site. This includes major utility and operating systems such as BAS, HVAC, specialty water, steam, compressed air and chilled water generation and distribution. Responsible for assuring reliable system generation, implementation of compliance initiatives and optimization of system operability in compliance with quality, safety and environmental regulations while operating within profit plan. Daily assignments require full application of sound engineering and cGMP principles, theories, concepts, and techniques. Responsible for the supervision, motivation, training and interviewing for utilities technical services team members. Incumbent is responsible for the development of objectives and projects. 
Qualifications * B.S., M.S. in Engineering, Science or equivalent experience. * Minimum of five years of cGMP, utilities and/or plant engineering experience or equivalent. * Demonstrated technical proficiency and leadership skills necessary to supervise a high-performance group of technical professionals. * Demonstrated interpersonal, oral presentation and communication skills including flexibility and ability to work in a team environment. * Demonstrated hourly and salaried supervision experience desired. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # ENG001459 Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Search Firm Representatives, Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means.  
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  11/12/2007, 11:35 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/12/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-353956 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



