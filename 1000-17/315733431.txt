



 <URL>  
Virus Starts Like a Cold But Can Turn Into a Killer 
By Rob Stein Washington Post Staff Writer Tuesday, December 11, 2007; A01 


Infectious-disease expert David N. Gilbert was making rounds at the Providence Portland Medical Center in Oregon in April when he realized that an unusual number of patients, including young, vigorous adults, were being hit by a frightening pneumonia. 
"What was so striking was to see patients who were otherwise healthy be just devastated," Gilbert said. Within a day or two of developing a cough and high fever, some were so sick they would arrive at the emergency room gasping for air. 
"They couldn't breathe," Gilbert said. "They were going to die if we didn't get more oxygen into them." 
Gilbert alerted state health officials, a decision that led investigators to realize that a new, apparently more virulent form of a virus that usually causes nothing worse than a nasty cold was circulating around the United States. At least 1,035 Americans in four states have been infected so far this year by the virus, known as an adenovirus. Dozens have been hospitalized, many requiring intensive care, and at least 10 have died. 
Health officials say the virus does not seem to be causing life-threatening illness on a wide scale, and most people who develop colds or flulike symptoms are at little or no risk. Likewise, most people infected by the suspect adenovirus do not appear to become seriously ill. But the germ appears to be spreading, and investigators are unsure how much of a threat it poses. 
"This virus has the capability of causing severe respiratory illness in people of all ages, regardless of their medical condition," said John Su, a disease investigator for the Centers for Disease Control and Prevention based in Texas, where the largest outbreak is tapering off at an Air Force base after 10 months. Other outbreaks have been reported in Washington state and South Carolina, along with a single case in an infant in New York City. 
"What people need to understand is that there is a virus out there that can make you very, very sick," Su said. "If you have a bad cold and your symptoms keep getting worse, go see your doctor. This is nothing to be necessarily alarmed about. But it is important to be aware that this bug is out there." 
The emergence of the virus is the latest example of how new, potentially dangerous pathogens can suddenly appear. 
"Infectious agents have the capacity to mutate and change form, and from time to time, either genuinely new agents appear or old agents appear in new guises," said William Schaffner, an infectious-disease expert at Vanderbilt University. "This appears to be another one of those emerging infections that has taken on genetic material or mutated so that it is now more virulent than it used to be." 
The virus, which spreads like those that cause flu or colds, raises many questions: Why has it suddenly become more common? Why is it apparently more dangerous? How often does it make people seriously ill? Who is most vulnerable? Is the threat growing or fading? 
"We don't know why it's associated with these severe cases," said Dean D. Erdman, who is studying the virus at CDC headquarters in Atlanta. "We don't know whether it's going to become a bigger problem in the future or whether we'll see more outbreaks of severe disease. These are all questions we're trying to answer." 
There are 51 known strains of adenovirus, ubiquitous germs that cause many illnesses, including colds, pinkeye, bronchitis, stomach flu and a respiratory infection called boot camp flu that has long plagued soldiers. But adenovirus infections rarely have been life-threatening, especially for healthy young adults. 
The new adenovirus is a variant of a strain known as adenovirus 14. First identified in Holland in 1955, it has caused sporadic outbreaks in Europe and Asia. No outbreaks, however, had ever been documented in the Western Hemisphere. 
But then Gilbert started seeing patients like Joseph Spencer, 18, a high school varsity swimmer who was suddenly racked by fever, chills and vomiting. 
"At first I thought it was just the flu," Spencer said. "But then it was the worst feeling I ever had. I felt so miserable. I really felt like I was dying." 
Spencer's mother took him to the emergency room, where he was placed in intensive care, sedated and put on a respirator. "Even then, we told the family we didn't think he was going to survive," Gilbert said. 
The teen spent 18 days in the hospital and was able to return home. But after weeks of bed rest and physical therapy, he remains short of breath and weak, and he is having memory problems. 
"I don't know if I'll ever be fully recovered," Spencer said. "I never imagined anything like this would ever happen to me." 
Spencer was not even the sickest. Of the 30 patients Oregon officials identified as having the virus, seven died. "That's an incredibly high mortality rate," Gilbert said. 
At about the same time, health officials learned of another outbreak affecting four residents of a nursing home in Washington state, including one person who died, as well as a far larger outbreak at Lackland Air Force Base in Texas. At least 579 recruits have been infected since February at the base, including at least 24 who had to be hospitalized. One recruit, Paige Villers, 19, of Norton, Ohio, died after getting mononucleosis and the virus. 
"All of a sudden out of nowhere she just got sick," said Villers's mother, Michelle. "She thought it was just something she needed to fight off. But instead of getting better, she just got worse and worse." 
Another 220 cases later turned up at other Texas military bases, along with about 200 more cases at the Marine Corps' Parris Island installation in South Carolina. 
Investigators also determined that an otherwise healthy 12-day-old girl who died in Manhattan in May 2006 had been infected with the same strain. 
A genetic analysis of the microbe at the CDC revealed that the currently circulating version of the virus is slightly different from the original 1955 strain, suggesting the microbe had mutated in some way to make it more virulent. 
"There are some suspicious changes in certain genes," Erdman said. "What we're trying to do now is link those changes to behavioral changes in the virus." 
Because doctors do not routinely test for adenovirus, investigators are uncertain how common it is. But recent surveys, including testing at military bases around the country, indicate that the virus suddenly appeared widely across the United States in 2006, showing up in significant numbers at military bases in San Diego, near Chicago and in Georgia. 
"It had been looked for but never identified prior to that," said David Metzgar of the Naval Health Research Center in San Diego, who has been tracking the virus among military recruits. "It was a very widespread emergence." 
The CDC reported the emergence of the virus and 362 cases on Nov. 16, but additional infections have since occurred in Texas and the report did not include the South Carolina patients. 
In Oregon, further testing has shown that the virus now accounts for more than half of all adenovirus infections. "That's shocking," said Paul Lewis, a state health investigator. "It went from being imperceptible to being the majority." 
Officials emphasize that the virus, even if it is widespread, may be only rarely causing serious illness. 
"It's like the blind person touching different parts of the elephant. We're touching the part of the elephant that is the sickest," said Ann Thomas of the Oregon Department of Human Services. 
The outbreak in Texas, which appears to be tapering off, supports that theory. 
"Even though it was more common to get more serious illness than is usual for adenovirus, most of the people who got infected had just a cold, and a small percentage had the more serious complications," said Larry J. Anderson, director of the CDC's division of viral diseases. "Why some who were infected got more serious illness we do not know." 
Some people may be genetically prone to the infection or have weaker immune systems, he said. Or it could just take time for people to build up immunity. But other experts say they believe the virus is inherently more dangerous. 
"My gestalt is that it's more virulent than average," said Gregory C. Gray, director of the Center for Emerging Infectious Diseases at the University of Iowa. "The consensus among people who look at adenovirus is this is a particularly virulent strain." 
In the meantime, researchers are trying to determine whether any antiviral drugs are effective against the bug and whether vaccines that protect against other strains offer any defense. They are on the lookout for the virus. 
"Are we going to have another huge outbreak, or will it disappear?" Gilbert said. "We just don't know." 
Michelle Villers said she hoped her daughter's death might at least alert others. 
"After my daughter's funeral in August, my son got sick in September with very similar symptoms," she said. "It turned out he just had strep throat. But parents need to press their doctor for tests, ask for tests, and keep pressing until you get results. My daughter, she had symptoms that just looked like the cold or like the flu. You hear of people dying of pneumonia, but it's usually older people. Not a 19-year-old in the prime of her life." 




2007 The Washington Post Company 


