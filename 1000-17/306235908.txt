


Job Title:     Project Scientist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2007-11-21 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Project Scientist &#150; CHE001259 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Develop and execute research projects within the Center for Materials Science and Engineering to characterize the physicochemical and functional properties of pharmaceutical solids in support of API process development (synthesis and isolation), API physical form definition, formulation and pharmaceutical process development, and chemical and pharmaceutical process optimization and troubleshooting. Elucidate the structure and phase behavior of APIs and their intermediates using a variety of physical and chemical characterization techniques, such as thermal analysis techniques, particle size measurements, solubility measurements, optical, infrared, and scanning electron microscopy, vibrational spectroscopy, and X-ray powder diffraction. Represent the Center on development teams in Early Development and Commercialization.  
Qualifications Required: Ph.D. in Physical Chemistry, Chemical Engineering, or Materials Science with expertise in characterization of the solid-state behavior of complex solids using a combination of physical characterization techniques. Demonstrated record of problem solving, strong communication skills, the ability to thrive in a team environment, and the capability to learn and apply all of the techniques in use in the laboratory and introduce new technologies to solve the complex, multifaceted problems encountered in drug development.  Desired: Expertise in solid-state NMR spectroscopy or molecular modeling. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # CHE001259. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  11/18/2007, 02:38 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  12/01/2007, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-354756 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



