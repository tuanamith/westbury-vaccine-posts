


Job Title:     PhD-Bio/Chemical Engineering (Full Time 2007-2008) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   PhD-Bio/Chemical Engineering (Full Time 2007-2008) &#150; CHE001190 
Job Description  
Submit 
Organize group and manage day-to-day operations. Monitor performance against established goals, taking corrective action as needed. Responsible for assisting in meeting established department budgets and plans. The successful candidate will apply engineering, science, and business skills to help Merck carry out its core functions. A number of different entry points are possible for a new hire, in either our research or our manufacturing division. Chemical and Biochemical Engineering at Merck has the vital role of bridging the gap between new pharmaceutical and vaccine discoveries and commercial products. Chemical processes produce in bulk form, the high quality complex chemicals used in Mercks most valuable human health, animal health and agricultural products. We then convert these basic chemicals into safe effective pharmaceuticals through formulation and packaging processes. Biologically derived products follow a similar path. 
At Merck we believe you can develop faster if we give you real challenges right from the onset. MRL offers a wide variety of both technical and leadership opportunities within each department. In MMD, we encourage you to move between functional groups so that you get hands-on experience in many aspects of our operations. 
Qualifications 
PhD in Chemical Engineering, BioEngineering, Biochemical Engineering or Biomedical Engineering is required. Degree completion no later than end of 2008 required. Some relevant experience via a co-op, internship, or independent research is preferred for consideration. If you are the kind of individual who thrives on challenge and possess the technical, leadership and business skills that are of value to our business, we invite you to apply. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careers/university/to create a profile and submit your resume for requisition # CHE001190. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point     US-NC-Durham-RTP     US-NJ-Lebanon     US-VA-Elkton     US-NJ-Rahway   Job Type  Standard   Employee Status  Regular  
Additional Information   Posting Date  08/16/2007, 08:43 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/15/2008, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00) 



Please refer to Job code merckcollege-325536 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



