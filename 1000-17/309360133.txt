


Job Title:     Staffing Consultant, Manufacturing--Merck &amp; Co.,Inc. Job Location:  NJ: Whitehouse Station Pay Rate:      Open Job Length:    full time Start Date:    2007-11-24 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Staffing Consultant, Manufacturing--Merck &amp; Co.,Inc. &#150; HUM001158 
Job Description  
Submit 








The Staffing Consultant (Manufacturing) will be responsible for the development and delivery of creative, efficient, and effective sourcing and recruitment campaigns in response to clients needs. Act as a resource to clients by providing creative methods of building prospect pools, pre-screening and interviewing candidates, and facilitating the staffing process. Responsibilities include but are not limited to:  


Qualifications 
.Bachelor&#8217;s degree required, master&#8217;s degree preferred. 
.At least 3 years of recruiting/staffing experience required, preferably in a large, matrixed organization. (At least 2 years of experience is required if in possession of a graduate degree). .Experience with direct sourcing and qualification of candidates (up to Manager/Director level), including use of the Internet, conducting telephone and on-site interviews utilizing behavioral-based interviewing techniques required. .Requires thorough knowledge of recruiting/staffing database systems (experience using Taleo a plus) .Requires working knowledge of Microsoft Office Suite (Excel, PowerPoint, Word). .Demonstrated strength in written and verbal communication as well as strong relationship building / maintenance, facilitation and influencing skills required. .DDI Targeted Selection Certification is also a plus. .International recruiting experience and project management expertise is preferred.  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # &amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;&amp;shy;HUM001158. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestation. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
 Profile   Locations  US-NJ-Whitehouse Station   Employee Status  Regular   Travel  Yes, 15 % of the Time  
Additional Information   Posting Date  10/25/2007, 02:27 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/24/2007, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-348338 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



