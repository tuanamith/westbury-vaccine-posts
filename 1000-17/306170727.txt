


Job Title:     Research Associate - Sr Research Biologist - In Vivo PET... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-18 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Research Associate - Sr Research Biologist - In Vivo PET Imaging &#150; PHA000745 
Job Description  
Submit 
Description At Merck, our mission is to discover, develop and deliver breakthrough medicines and vaccines to people around the world. Our mission also entails something more - a commitment to the health, safety and well-being of the people who take our medicines, and also to our employees, neighbors and others in the global communities where we live and work.  By joining Merck, you will share in this commitment that embraces our core values. We currently have an exciting opportunity available for a talented Research Associate / Sr. Research Associate, Senior Research Pharmacologist or Research Fellow to join our Imaging group at Merck Research Laboratories in West Point, Pennsylvania.  The successful candidate will be responsible for in vivo experimental and multi-modality imaging support and study supervision for a variety of drug discovery and development programs. The ideal candidate will have expertise in CT and/or PET imaging as well as radiotracer handling and administration. Experience in techniques across species such as blood sampling, intracath insertion, hemodynamic monitoring and administration and maintenance of anesthesia and recovery from anesthesia would be preferable. Record keeping, data analysis, graphics and study report preparation in a timely manner is essential. Experimental procedures must be continually upgraded to stay consistent with state-of-the art research to support top quality drug development.  
Qualifications 
If you have a BS / MS or PhD in Biology, Physiology, Pharmacology or related field with demonstrated hands-on experience, we want to hear from you. You will be involved in any or all of the ongoing efforts with in vivo studies to support neuroscience, metabolic disease, cardiovascular and oncology projects including Alzheimer&#8217;s Disease, Diabetes, Cancer and Atherosclerosis. You should have knowledge of relevant anatomy and physiology. Training will be provided for specific animal related techniques and imaging procedures. With your excellent interpersonal and communication skills, you will interact and collaborate with other scientists across Merck Research Laboratories. 
Our commitment to our employees resonates in the benefits we offer including competitive compensation, tuition reimbursement, work life balance initiatives, onsite child care at many of our locations and opportunities for personal and professional enrichment. Join us and become a part of our commitment and our legacy which continues to deliver novel medicines to the people that need them the most.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your CV for PHA000745 . Discovery of GreatDrugs demands continual discovery of Great People...  Merck is an equal opportunity employer proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  10/25/2007, 07:34 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  11/24/2007, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-348716 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



