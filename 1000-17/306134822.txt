


Job Title:     SORC - Engineer / Staff Engineer / Senior Engineer Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2007-11-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   SORC - Engineer / Staff Engineer / Senior Engineer &#150; ENG001440 
Job Description  
Submit 
Qualifications Candidates should have a BS or MS in Chemical Engineering, the biological sciences or a related discipline with a solid academic record and/or relevant work experience. Strong teamwork, communication and leadership skills are desired. Determination of appropriate grade level will be based on relevant skills and work experience. Strong preference will be given to candidates possessing previous cell culture, aseptic or purification experience. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #ENG001440. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  10/12/2007, 04:48 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  01/10/2008, 10:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-345177 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



