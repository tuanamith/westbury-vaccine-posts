


 <URL>  
In the United States, the CDC said cases had been confirmed in 11 states; 20 others have suspected cases. 
New York has the most confirmed cases, with 50, followed by Texas, with 26. California has 14 cases. 
The CDC added the 11th state Thursday -- South Carolina, with 10 cases. 
"There are many more states that have suspected cases and we will be getting additional results," Besser said. 
As the virus spread to more people, officials took precautions. 
Nowhere in the world is the crisis more severe than in Mexico, where the first cases were detected. 
The Mexican federal government will close all nonessential government offices and businesses starting Friday. 
But the 2009 H1N1 virus is a hybrid of swine, avian and human strains, and no vaccine has been developed for it. 
Amidst the anxiety, health officials tried to tamp down concerns. 

