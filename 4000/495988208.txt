


Mike Ash < <EMAILADDRESS> > wrote in  <NEWSURL> :  

They don't need to know *why* to decide not to buy one, when their  friend's is broken. 

The most common way for this to turn up right now is when someone  wants to "loan" some music to a friend. The device won't do it.  They call the help line, and are informed they are criminal  masterminds who should be put in a dungeon and tortured to death  for crimes against humanity. All their friends now know it's broken  _be design_. This is not the kind of word of mouth you want. They did do a fair amount of work. Not when the announcement was  made, but when the device and software was designed. 
This is not, however, as commonly desired a feature as the ability  to burn a music CD for a buddy. 
And while a lot of music fans really don't care about that, a lot  of book readers *do* loan out books to friends, and *do* care about  it. 
--  Terry Austin 
"Terry Austin: like the polio vaccine, only with more asshole."     -- David Bilek 
Jesus forgives sinners, not criminals. 

