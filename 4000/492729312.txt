


 <URL>  
Marijuana Chemical May Fight Brain Cancer Active Component In Marijuana Targets Aggressive Brain Cancer Cells, Study Says By Kelli Miller Stacy WebMD Health News Reviewed by Louise Chang, MD 
April 1, 2009 -- The active chemical in marijuana promotes the death of brain cancer cells by essentially helping them feed upon themselves, researchers in Spain report. 
Guillermo Velasco and colleagues at Complutense University in Spain have found that the active ingredient in marijuana, THC, causes brain cancer cells to undergo a process called autophagy. Autophagy is the breakdown of a cell that occurs when the cell essentially self-digests. 
The team discovered that cannabinoids such as THC had anticancer effects in mice with human brain cancer cells and people with brain tumors. When mice with the human brain cancer cells received the THC, the tumor growth shrank. 
Two patients enrolled in a clinical trial received THC directly to the brain as an experimental treatment for recurrent glioblastoma multiforme, a highly aggressive brain tumor.  Biopsies taken before and after treatment helped track their progress.  After receiving the THC, there was evidence of increased autophagy activity. 
The findings appear in the April 1 issue of the Journal of Clinical Investigation. 
The patients did not have any toxic effects from the treatment. Previous studies of THC for the treatment of cancer have also found the therapy to be well tolerated, according to background information in journal article. 
Study authors say their findings could lead to new strategies for preventing tumor growth. -------------------------------------------------------------------------------- 
click to show or hide video description  Brain Tumor Vaccine When cancer strikes the brain, it's deadly. But an experimental vaccine may help patients live longer. 
Watch Video click to show or hide video description  Life After a Brain Tumor woman gardening Medical advances make the chances of surviving a brain tumor better than ever. 
Watch Video click to show or hide video description  New Generation PET Scanner A high tech device that detects if tumors are benign or cancerous, the PET Scanner is fast becoming the diagnostic tool of choice. 
Watch Video click to show or hide video description  Cancer Pain Scale Doctors can not tell by looking at a patient how much pain they're in. Here's a new approach. 
Watch Video click to show or hide video description  Chemotherapy and the Brain 


