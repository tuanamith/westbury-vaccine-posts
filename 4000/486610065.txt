


Asian countries pledge common fight against flu  08 May 2009 04:51:57 GMT Source: Reuters  * Asian countries to increase flu medicine stockpiles 
* No restrictions on travel; seen as futile, disruptive 
* Asian states want access to vaccine production technology 
* China wants stockpile covering 1 percent of its people (For full coverage of the flu outbreak, click [nFLU]) 
By Kittipong Soonprasert 
BANGKOK, May 8 (Reuters) - Asian countries will increase stockpiles of medicine to fight the H1N1 flu virus and look at ways to share essential supplies in the event of an emergency, according to a statement drafted for a meeting on Friday. 
Health ministers from the 10-member Association of South East Asian Nations (ASEAN) plus China, Japan and South Korea will intensify cross- border cooperation and establish joint response teams to fight the spread of the virus, also known as swine flu. 
According to the statement, the ministers were concerned that most of the production capacity for vaccines was located in North America and Europe and it was inadequate for a global pandemic. 
Asia has no capacity to produce vaccines at the moment. 
"Despite other regions having begun to acquire the technology to produce influenza vaccines, access to effective pandemic vaccines is a major problem in the region," the statement said, calling for the transfer of technology to make vaccines and antiviral medicine. 
Chinese Health Minister Chen Zhu told reporters Beijing was pressing drug companies in China to increase the existing "quite small" national antiviral stockpile, but admitted it was a tall order to provide enough in a country of 1.3 billion people. 
"Our objective is a stockpile for eventually one percent of the population. One percent is already quite huge," he said. 
He defended the quarantine of passengers on a flight from Mexico, lifted on Thursday after one week. 
"At this time, we think this kind of precaution and measures are still necessary, but things may change according to the analysis of the WHO and our experts," Zhu said. 
NO TRAVEL BANS 
The 13 countries will look at screening people leaving affected areas but are not planning travel bans. 
Evidence showed that "imposing travel restrictions would have very little effect on stopping the virus from spreading, but would be highly disruptive to the global and regional communities and pose major negative impacts on the current global economic downturn", the statement said. 
Margaret Chan of the World Health Organization (WHO) told the meeting Asian governments had to stay vigilant, but urged them to "refrain from introducing economically and socially destructive measures that lack solid scientific backing and bring no clear benefit". 
"The rational use of travel- and trade-related measures is always wise at a time of severe economic downturn," she added. 
Asia has seen far fewer confirmed cases of the H1N1 virus, which has killed 44 people in Mexico and two in the United States, and spread across Europe. [ID:nFLUTOLL] 
However, after the damage wrought by SARS and bird flu in recent years, Asian countries are taking no chances this time. 
The statement said they would "assess the potential need and increase national stockpiling of antivirals and essential medicines, medical supplies and personal protective equipment to the level necessary for effective responses" if the flu spreads. 
They would also "consider the establishment in ASEAN+3 countries of a system to facilitate the sharing of essential supplies in the region in case of emergency needs". 
ASEAN comprises Indonesia, Thailand, Singapore, Malaysia, Myanmar, Vietnam, Cambodia, Laos, Brunei and the Philippines. (Additional reporting by Darren Schuettler; Writing by Alan Raybould; Editing by Bill Tarrant) 

