


Thoughtful House Center for Children on Court Ruling: MMR Causes Autistic Disorder  Court Rules MMR Vaccine Causes Autistic Disorder Decision Contradicts Findings in Controversial Cedillo Case 
(Austin, Texas) - The controversial Feb. 12 decision by the US vaccine court that there was no link between MMR (measles-mumps-rubella) vaccines and autism contradicts a ruling issued by the same court in June of 2007. In addition, just days after its announcement denying the vaccine-autism link in the Cedillo case, the court awarded an estimated $3 million dollars to the family of 10 year-old Bailey Banks  on Friday, February 20, 2009, and confirmed that the child's acute brain damage was a result of the MMR vaccine, which led to his autism spectrum disorder (ASD). 

Although the US vaccine court continues official denials to the public of any connection between the MMR shots and autism, it quietly settled the case with the Banks family.  Special Master Richard Abell wrote that the family had successfully demonstrated "the MMR vaccine at issue actually caused the conditions from which Bailey suffered and continues to suffer." Awards have been granted to three families because the vaccine court has decided there was in fact a causative connection between the MMR 3-in-1 shot and brain damage in these children. That damage resulted in an autism spectrum disorder.  Vaccines have also been found to be causally related to autism spectrum disorders in seven other known cases by the same court.  In the Banks decision, the court relied on a report based on a complete neurological investigation, including an MRI scan sixteen days after his MMR shot.  He was diagnosed with acute disseminated encephalomyelitis (ADEM), a neurological disorder characterized by inflammation of the brain and spinal cord, which is known to follow immunization.  "I am personally aware of many, many parents who report these exact symptoms in their children following MMR immunization," said Dr. Andrew Wakefield, Executive Director of Thoughtful House, an autism treatment center in Austin, Texas. "Very few children with autistic regression receive the proper work-up that Bailey had during the early stage of the disease, so a possible ADEM diagnosis may well have been missed in the other children. The MRI findings often disappear after the damage has been done." Signs of ADEM usually appear within a few days or a few weeks after immunization or infection, often beginning with gastrointestinal or respiratory symptoms. The disease progresses to neurological deterioration including loss of eye contact, ataxia (poor coordination), changes in mental status, delirium, lethargy, and seizures. 

"The contradictory rulings from the vaccine court regarding vaccines and autism demonstrate that we still don't have a definitive answer," said Dr. Bryan Jepson, an autism specialist at Thoughtful House. "We need to realize that the question of the MMR's possible contribution to autism remains a matter of scientific debate. Ultimately, the correct answer will come through honest, transparent, and rigorous scientific study, not from a court bench." 

About Thoughtful House: Thoughtful House takes a multi-disciplinary approach to treating autism and supports a 'safety-first' vaccination policy that gives parents the option of choosing a stand-alone measles vaccine for their children.  The research program at Thoughtful House is dedicated to understanding the biological origins of childhood developmental disorders and establishing best practices in treating children affected by these disorders. www.thoughtfulhouse.org 

 <URL> ... 



  


