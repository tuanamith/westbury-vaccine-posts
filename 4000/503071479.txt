



Panel 9 
========== TABLE OF CONTENTS of this chain: 

# --enm: At this time, there is a report of a giardia "vaccine" in test, # but no word on methods 

Here's an interesting recent publication.  Don't know if it's in the panel but should be. 



Backcountry Water Drinking 
 --Why Not Just Drink the Water? 
--Notes About This Introduction 
 ===== 
OCR'ed memo from the Centers from Disease Control:  
GIARDIASIS 
Transmission and Control 
Introduction 
Giardiasis: Prevalence and Symptoms 
Medical Treatment 
(*) Use of trade names is for purposes of identification only.  
Etiology and Epidemiology 
Parasite Biology 
Transmission 
Removal from Municipal Water Supplies 
Water Disinfection in the Out-of-Doors 
Boiling 
Chemical Disinfection 
IODINE 
Tincture of Iodine Drops* to be Added per Quart or Liter Clear Water Cold or Cloudy Water** 
    2% 5 10 
* 1 drop = 0.05ml 
** Very turbid or very cold water may require prolonged contact time; let stand up to several hours or even overnight.  
CHLORINE 
                           Drops* to be Added per Quart or Liter Available Chlorine 			Clear Water Cold or Cloudy Water** 
     1% 10 20 4% to 6% 2 4 7% to lO% 1 2 Unknown 10 20 
* 1 drop = 0.05ml 
** Very turbid or very cold water may require prolonged contact time; let stand up to several hours or even overnight.  
Filters 
Conclusion 
TABLE I Percentage Number of Patients 
Symptoms 
Physical finding 
   Abdomen tender to palpitation 66 92 
Laboratory findings Blood Anemia 15 124 Leukocytosis 9 32 
   Stool Increased mucus 56 32 Increased neutral fats 50 32 Blood 13 156 
* Index symptom; may be biased (upward)  


References 
2.  Weller, Peter F. Intestinal Protozoa: Giardiasis. Scientific American Medicine, 1985 
3.  Id. 2.  
4.  Davidson, R.A. Issues in Clinical Parasitology: The treatment of Giardiasis.  Am J. Gastroenterol. 79:256-261, 2984 
5.  Id. 2.  
6.  Intestinal Parasite Surveillance, Annual Summary 1978, Atlanta, Centers for Disease Control, 1979.  
11. Jokipii, L., Jokipii, A.M.M. Giardiasis in Travelers: A prospective Study.  J. Infect. Dis., 130:295-299, 1974.  
14. Sealy, D.P., Schuman, S.H. Endemic Giardiasis and Day Care. Pediatrics 72:154-158, 1983. 
19. Water-related Disease Outbreaks Surveillance, Annual Summary 1983. Atlanta, Centers for Disease Control, 1984.  
21. Frost, F. Plan, B., Liechty, B. Giardia Prevalence in Commercially Trapped Mammals. J. Environ. Health 42:245-249.  
22. Id. 21.  
23. Id. 18.  
=============== 
 CRYPTOSPORIDIOSIS 
                             (Fact Sheet)  
What is cryptosporidiosis?  
How is cryptosporidiosis spread?  
What are the symptoms of cryptosporidiosis?  
What should you do if you suspect that you have cryptosporidiosis?  
 What is the treatment for cryptosporidiosis?  
No safe and effective cure is available for cryptosporidiosis.  
People who have normal immune systems improve without taking antibiotic or antiparasitic medications.  
Who is at risk?  
How can you prevent cryptosporidiosis?  
If you have cryptosporidiosis, wash your hands often to prevent spreading the disease to other members of your household. 
 For more information on cryptosporidiosis, see the following sources:  
Dubey JP, Speer CA, Fayer R. Cryptosporidiosis of man and animals. Boston: CRC Press, 1990. 
Smith PD, Quinn TC, Strober W, Janoff EN, Masur H. Gastrointestinal infections in AIDS. Ann Intern Med 1992;116:63-77.  
=============== 
American Journal of Public Health December 1989, Vol 79, No 12, pp 1633-1637.  
Copyright 1989 AJPH 0090-0036/89$1.50 [used without permission] 
Abstract 
-------------------------------------------------------------------------------- 
Introduction 
-------------------------------------------------------------------------------- 
Methods 
Filter testing 
Contact Disinfection Testing 
Disinfectant Testing 
	Polar Pure Water Disinfectant (Polar Pure), 	Polar Equipment, Saratoga, CA; 
	Coghlan's Emergency Germicidal Drinking Water Tablets (CEGDWT). 	Coghlan's Ltd, Winnipeg. Canada; 
	Potable Aqua Drinking Water Germicidal Tablets (Potable Aqua), 	Wisconsin Pharmacal Inc., Jackson, WI; and 
	2 percent iodine prepared from I2 reagent grade 	(Baker, Phillipsburg, NJ). 
The remaining three products were chlorine-based: 
	Sierra Water Purifier (Sierra), 4 in 1 Water Co., Santa Fe, NM; 
	Halazone, Abbott Laboratories, North Chicago, IL; and 
	commercial liquid bleach (5.25 percent sodium hypochlorite). 
Cysticidal properties of the chemical treatments were determined as follows: 
1) Water was put in 50-ml disposable plastic centrifuge tubes and placed in a 10C incubator.  
2) G. lamblia cysts were added to each test sample at time zero.  
3) Tubes were vortex-mixed, sampled, and returned to the incubator.  
Heat Inactivation 
-------------------------------------------------------------------------------- 
Results 
Filter Device Tests 
Cyst Inactivation 
-------------------------------------------------------------------------------- 
Discussion 
-------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------- 
Acknowledgements 
-------------------------------------------------------------------------------- 
References 
(2) Kahn FH, Visscher BR: Water disinfection in the wilderness.  West J Med 1975; 122:450-453.  
-------------------------------------------------------------------------------- 
Appendix A: Water Filter characteristics Listed by Manufacturers on Packaging or Instruction Insert 
[Manufacturer column omitted.  See text for this information.] 
Name Filter Type Operating Operating Useful Restrictions Mode Rate Life /Limitations 
First Need 0.4 um microscreen hand pump 1 pt/min up to 800 A plus absorber pints 
{H2OK 6 um mesh, 3 in.  gravity 1 qt/min 2000 gal A, B activated carbon w/Ag} 
Katadyn 0.2 um ceramic, hand pump 1 qt/min many years A Pocket Ag-impregnated Filter 
Pocket 10 um (nominal), halo- mouth - - A Purifier genated resin (38% I), suction Ag-impregnated carbon 
Water Pur- Polystyrene resin bed gravity - 100 gal A, C ifier (a)  (46% I2 as I5)  
-------------------------------------------------------------------------------- 
Appendix B: Characteristics of Disinfectant Preparations 
[Manufacturer column omitted.  See text for this information.] 
Name Active Chemical Recommended Application Total Halogen pH Concentration (b)  (a), (mg/liter)  
Polar Pure Crystalline iodine, 1-7 capfuls per quart 2.4 (1 6.1 99.5% depending on temperature cap/quart)  
CEGDWT Tetraglycine hydro- 1 tablet per liter or 4.5 (1 5.6 periodate 16.7% (6.68% quart tab/quart)  titrable iodine)  
2% Iodine Iodine 0.4 ml per liter 4.5 6.5 
Sierra Calcium hypochlorite & 100 crystals (50 mg)  11.6 6.7 hydrogen peroxide Ca(OCl)2 + 6 drops H2O2 per gallon 
Halazone p-dichloro-sulfamoyl 5 tablets per quart 7.5 6.7 benzoic acid, 2.87% 
Chlorine sodium hypo-chlorite, 5 ml per gallon 3.9 7.1 bleach 5.25% 
(a) As prepared according to package instructions. (b) In water treated according to package instructions.  
-------------------------------------------------------------------------------- 
Appendix C: Characteristics of Disinfectant Test Water 
Source pH Turbidity (NTU)  Chlorine Demand (a)  (mg.liter)  
Spring-fed 6.8 0.09 0.3 
Lake Washington 7.1 0.75 - 0.80 0.7 
(a) 30 minutes, free chlorine demand (5).  
-------------------------------------------------------------------------------- 
The authors 
=============== 
AD Reprint: OHIO STATE UNIV,DEPT VET PREVENT MED,1900 COFFEY RD/COLUMBUS//OH/43210.  


=============== Pointer to CDC Web page on Giardia:  	 <URL>  
=============== =============== Pointer to FDA Web page on Giardia:  	 <URL>  
=============== 
===== 
REI Water Filter Chart (also available at  <URL> /)  



CHANGES IN PROGRESS 
Lower prices can be found elsewhere than REI. REI charges list mostly.  
=============== 
From:  <EMAILADDRESS>  (Alan Malkiel) Subject: Review-Katadyn Mini Filter 
Equipment Review - Katadyn Minifilter 
Reviewer - Alan Malkiel 
=============== 
*A warning about iodine toxicity* (some individuals appear to be susceptible to this, while many are not).  
%J Animal Health Newsletter %I University College of Veterinary Medicine %D July 1995 %K Canine Giardia, 



No effect. 




Eugene, I've made it easy for you. 

A) What is the effect of ordinary dish detergent on Giardia? 
It has no effect.  To kill Giardia cysts, you must rinse dishes in an iodine solution, or in hot water above 80 C (176 F). 
C) How long does Giardia survive on dry plates and utensils rinsed in    water that might contain Giardia? 



Risk of giardiasis in Aucklanders: a case-control study. Int J Infect Dis. 2002 Sep;6(3):191. 
Faecal contamination of greywater and associated microbial risks. Water Res. 2003 Feb;37(3):645-55. 

Microbial agents associated with waterborne diseases. Crit Rev Microbiol. 2002;28(4):371-409. Review. 
Giardia in beaver (Castor canadensis) and nutria (Myocastor coypus) from east Texas. J Parasitol. 2002 Dec;88(6):1254-8. 

ARSENIC references 
Chwirka, J. D. Removing Arsenic from groundwater Journal of the American Water Works Association v92, no3, pp. 79-88 

 <URL>  
 <URL>  
 <URL>  
 <URL>  
--  
Looking for an H-912 (container). 


