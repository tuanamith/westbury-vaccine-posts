


On May 1, 2:35=A0pm, Ed Rasimus < <EMAILADDRESS> > wrote:  <EMAILADDRESS> : any, efore 
Did you see this? 

Alberta pigs likely infected with flu from worker: CFIA official Number of confirmed Canadian cases now 85, all believed mild Last Updated: Saturday, May 2, 2009 | 7:56 PM ET Comments242Recommend108 CBC News 
In what would be the first reported case of its kind, a farm worker with the swine flu virus is believed to have infected about 200 pigs in Alberta, a top official with the Canadian Food Inspection Agency said Saturday. 
Senior research scientist Dr. Karuna Karunakaran works in the vaccine research lab at the British Columbia Centre for Disease Control during a demonstration for media in Vancouver on Thursday. Twenty-two cases of swine flu are now confirmed in B.C.Senior research scientist Dr. Karuna Karunakaran works in the vaccine research lab at the British Columbia Centre for Disease Control during a demonstration for media in Vancouver on Thursday. Twenty-two cases of swine flu are now confirmed in B.C. (Darryl Dyck/Canadian Press)Dr. Brian Evans, executive vice-president of the CFIA, said at a news conference in Ottawa that the pigs were apparently infected by a farm worker who had recently been in Mexico and fell ill upon his return. 
The worker returned from Mexico on April 12 and worked at the Alberta farm two days later. He "may have exposed pigs there to the illness," Evans told reporters. 
The man has since recovered. The pigs are also recovering and the herd in question has been quarantined, he said. Samples from the infected pigs are being analyzed. 
"We have found the virus is the one being tracked in the human population," Evans said. About 10 per cent of the 2,200 pigs at the farm exhibited flu-like symptoms such as loss of appetite or fever, he said. 
"I want to be clear =97 there is no food safety concern related to this finding," said Evans. 
It is common for pigs to contract influenza, he said. But this is the first known case of the H1N1 virus being transmitted from humans to pigs. 
Normally, detecting influenza in pigs wouldn't generate a response from food safety officials, but with an international flu outbreak the current circumstances are different, Evans said. 
"The chance that these pigs could transfer virus to a person is remote," said Evans. 
The outbreak among pigs, he said, was confined to the herd in question as none of the pigs have been moved outside the farm or sold elsewhere. Province 	Confirmed cases of swine flu in Canadians Alberta 	15 British Columbia 	22 Nova Scotia 	31 New Brunswick 	1 Ontario 	14 Quebec 	2 Total 	85 Spate of new confirmed cases 
Earlier in the day, health officials in Nova Scotia, Alberta, British Columbia, Ontario and Quebec confirmed a spate of new swine flu cases in humans, boosting the national count to 85. 
The Nova Scotia Department of Health said lab testing had confirmed 17 new cases in the province. 
Eleven of the cases are related to the King's-Edgehill school community in Windsor, but the breakdown between students and staff is not known, according to the Nova Scotia Department of Health. Six other cases are outside the school population and were confirmed by testing in a Halifax hospital. 
The virus, identified as a new strain of the H1N1 subtype of type A influenza, is believed to have originated in Mexico and has since appeared in Canada, the U.S., Europe and elsewhere. 
Canada's chief public health officer David Butler-Jones said Friday the more public health authorities look, the more cases they are likely to find. This assertion was echoed by Dr. Robert Strang, chief public health officer for Nova Scotia. 
"We have been expecting this and are prepared," Strang said Saturday. "Right now we are working to gather more information about the individual confirmed cases outside of King's-Edgehill." 
The cases are all mild, says the health department. 
Nova Scotia's first cases of the H1N1 virus were confirmed at the private school last weekend. Officials believe students who went on a school trip to Mexico carried the infection back home. 
None of the Canadian cases have been as severe as those in the Mexican outbreak. Cases considered mild 
The Alberta Department of Health confirmed seven new cases Saturday, four of them in the Edmonton area, two in the north and one in the Calgary area. 
Five of those infected are female, including one child. Another male child has also been infected. 
None of the cases require hospitalization, Alberta's Department of Health said in a release posted on its website. There was no further information. 
West Vernon Children's Centre employee Sharyn Stokes cleans plastic toys in the preschool classrooms in Vernon, B.C. The centre runs the after-school children's programs for Beairsto Elementary School, which was recently closed because of a case of swine flu.West Vernon Children's Centre employee Sharyn Stokes cleans plastic toys in the preschool classrooms in Vernon, B.C. The centre runs the after-school children's programs for Beairsto Elementary School, which was recently closed because of a case of swine flu. (Jeff Bassett/Canadian Press) Health officials in Quebec confirmed another case of swine flu in the province Saturday. There are now two confirmed cases in the province. 
Dr. Alain Poirier, Quebec's director of public health, said the virus had been found in a child who had travelled to Mexico with his parents. 
He said he would not name the child or the school he attended until all teachers and parents associated with the school had been contacted. 
Dr. David Williams, Ontario's acting chief medical officer of health, confirmed two new cases of the virus Saturday. One of the two newly confirmed cases is in the York region, while the other is in the Toronto area. 
In a release, Williams said one of the cases resulted from person-to- person transmission, as the infected individual picked up the illness from a roommate who travelled to Mexico. All the Ontario cases are believed to be mild. 
The B.C. Centre for Disease Control confirmed three more cases Saturday, raising its total to 22. Details of the new cases were not immediately clear, but a release on the department's website said all of the cases are "relatively mild." 
So far, Saskatchewan, Manitoba, Newfoundland and Labrador, P.E.I. and the territories have not confirmed any cases of this particular flu strain. With files from The Canadian Press 

