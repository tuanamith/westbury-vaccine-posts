


Doctors to leave our baby boys intact Stephen Cauchi June 7, 2009  <URL> = 90606-bz8c.html 
THE Royal Australasian College of Physicians has flagged it will not change its policy against circumcision despite evidence the procedure can prevent the spread of HIV and other sexual diseases. 
The college, which represents physicians and pediatricians in Australia and New Zealand, has adopted the position that "there is no medical indication for routine neonatal circumcision" since 2004. 
But it has been reviewing this stance in part following recent scientific research suggesting that the risk of HIV infection could be dramatically reduced by the practice. 
Three trials conducted in South Africa, Kenya and Uganda between 2005 and 2007 showed conclusively, according to the World Health Organisation, "that male circumcision reduces the risk of heterosexually acquired HIV infection in men by approximately 60 per cent". 
Further research, published this year in the New England Journal of Medicine, has found that circumcision can reduce the transfer of human papillomavirus =97 the chief cause of cervical cancer in women =97 by 35 per cent, and herpes simplex virus =97 the chief cause of herpes =97 by 25 per cent. 
The journal said the findings underscored "the potential public health benefits of the procedure". 
The college, which began its review in 2006 and was supposed to come out with a revised policy at the end of 2007, will not do so until the end of this year, as it considers the new evidence. 
It is estimated that 10 to 20 per cent of male infants are circumcised in Australia. 
The chairman of the college's panel considering the issue, David Forbes, of the University of Western Australia's school of pediatrics, said the African studies had delayed the new recommendation, but so too had the fact "it is a contentious and non-clear-cut issue". 
"It's quite clear that there's evidence that circumcision in adult males helps prevent HIV in Africa," said Professor Forbes. "It's not so clear that circumcision in Australia in infants is of benefit to the infants or the community. 
"Policy is about getting the right message to health planners so that we have safe but cost-effective expenditure." 
New concerns about the practice of circumcision in Australia were raised last week after the Tasmanian Law Reform Institute found that parental consent might not be enough to protect the circumcisers of baby boys from later legal action. 
No specific laws regulate the removal of the foreskin. 
Professor Forbes said he wanted to have the college's position "finalised by the end of this year, hopefully before". 
He said the benefit of reducing human papillomavirus "appears not to be such an issue in Australia" because a vaccine was being produced. And there were important differences between the HIV situation in Africa and Australia: HIV rates in Australia were far lower and condom use was much higher. 
He said while there was clear evidence that circumcision reduced the rate of urinary tract infection, this alone did not justify routine use of the procedure as the infection rate was so small. 
He said the 10-strong panel consisted of pediatricians, surgeons and policy and public health experts. "There are extremely strong views at both ends of the spectrum for those who promote circumcision and those who oppose it," he said. 
"Undertaking elective surgery of minors who are not able to consent is for many pediatricians an even bigger issue, especially when there are examples of (Muslim) societies who elect to have it at puberty when people can choose." 
Dr Forbes declined to preview what the college's recommendation would be. "Policy change tends to be evolutionary, not revolutionary, and given that there are no trials of neonatal circumcision for prevention of HIV, I wouldn't expect revolutionary change. I would expect evolutionary change." 
Roger Short, from Melbourne University's department of obstetrics and gynaecology, hopes the college will view circumcision in a more favourable light. 
"It would be exciting to see the Royal College come forward with a slightly more progressive attitude than its previous pronouncements," he said. "The evidence is coming in, and it is irrefutable, that there are major benefits." 
Professor Short said there were no grounds for making circumcision mandatory. Instead, the college should change its recommendation. 
"I think we should go from saying 'when in doubt, don't circumcise' to 'when in doubt, do'." 


