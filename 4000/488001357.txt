



"Roy" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... On May 16, 6:27 pm, "Gerrald Arnasen" < <EMAILADDRESS> > wrote: " <EMAILADDRESS>  gov> wrote in message 

So now according to you the The Canadian Press is a kook site...Too funny Roy...So anything that you can't comperhend just can't be facts...Go back to sleep Roy and bury your head in the sand again...you dumb ass socialist... 
WHO, flu experts looking into claim H1N1 swine flu evolved in lab, not nature 2 days ago 
TORONTO - The World Health Organization and leading influenza research groups are investigating unpublished claims that the new H1N1 swine flu virus may have evolved in a laboratory, not in nature. 
The Geneva-based agency was informed of the pending publication over the weekend by the author, a retired Australian plant virologist named Adrian Gibbs. 
Gibbs, who studies the evolution of viruses, said the genes of the novel H1N1 look like they evolved at a faster rate than would have been expected if the virus had just emerged from pigs. 
While that could mean the virus spent some time evolving in another, intermediary host - Gibbs listed birds or marine mammals as possibilities - the unusual evolutionary speed could also have been the product of the virus was being grown in eggs in a laboratory, he said. 
That raises the spectre of human error resulting in the accidental release of the virus, Gibbs suggested Tuesday from his home in Canberra. 
"Because I couldn't find any evidence that the authorities were seriously considering a lab escape, I thought it would be a good idea to come out in public and say: 'Well, the data looks to me like that. And that should be checked,"' he said. 
But leading flu experts don't see the signs of sped up evolution that Gibbs reports observing in the H1N1 virus genes. 
"We don't see evidence that there's accelerated evolution indicating that there would have been a new host that the virus was introduced into, be it eggs or any other host," said Dr. Nancy Cox, head of the influenza division at the U.S. Centers for Disease Control in Atlanta. 
When flu viruses are first introduced into eggs - or any new host - the viruses have to adapt quickly to survive. That can produce what Cox called accelerated evolution, with viruses mutating at rates one would not see in viruses living in hosts to which they were well adapted. 
Furthermore, when CDC scientists try to grow these viruses in eggs, they see the kinds of mutations that one would expect to arise when non-avian flu viruses are first forced to adapt to eggs, Cox said, adding these changes are seen in about 20 per cent of the viruses. That suggests the new H1N1s haven't grown in eggs before. 
Gibbs, who has yet to submit his findings to a scientific journal, notified the WHO of his hypothesis over the weekend. 
Gavin Smith, a researcher trying to trace the origins of the new virus, criticized Gibbs for publicizing his claim before publishing his analyses in a peer-reviewed journal. 
Peer review would have allowed people knowledgeable about flu virus evolution to kick the tires, so to speak, of this theory and then decide whether alarms needed to be sounded. 
"I think it's irresponsible to be making those conclusions before anything has been sent for peer-review," said Smith, a virologist and evolutionary biologist at the University of Hong Kong. 
"Obviously they are claims that would need to be investigated. And I just worry that it's going to divert attention away from more pressing issues." 
It already has. After hearing of Gibbs' theory, the WHO scrambled to draw in researchers from leading human and animal influenza laboratories around the world in a bid to determine if the claim has merit and if it does, whether that changes the advice WHO gives member countries on the threat posed by the new H1N1 swine flu virus. 
The agency's leading flu scientist said the consultation is still ongoing and a conclusion hasn't been reached, but the weight of evidence so far does not support Gibbs' theory. 
"I think the preliminary analyses certainly suggest there are other explanations and that the explanation suggested by the author is not the best one," Dr. Keiji Fukuda, acting assistant director general for health security and environment, said in an interview from Geneva. 
Fukuda said extensive analyses done by the CDC are bolstered by the work of a research group at Cambridge University which specializes in the evolution of influenza viruses. "They also feel that there are alternate explanations for this." 
Eggs are used in laboratories and in vaccine manufacturing to grow up quantities of influenza viruses. 
If the virus had spent time evolving in eggs, it would suggest it was not solely the creation of nature but may have been accidentally or deliberately engineered in a lab. And that would raise questions about how something constructed in a lab ended up transmitting among people in at least 30 countries around the globe. 
"If there was evidence that it was an egg-derived isolate, then that means it's been handled in a laboratory. And if it's been handled in a laboratory, then there are different possibilities that you have to think of," Fukuda said. 
"Was this developed in part as a vaccine virus? Was this developed as some sort of research project? And in those instances, was it released on purpose? Was it an accidental release? What would be the circumstances?" 
Gibbs said he doesn't subscribe to the notion the virus might have been deliberately engineered. 
"I'm not a great believer in the fact that anybody could wittingly bioterrorize with a virus. I'm a believer in Darwinian things . . . rather than - What's the right word for it? - intelligent design," he said. 
Italian influenza researcher Dr. Ilaria Capua said there is too little known about swine influenza virus evolution to make the claim in the first place. 
Capua, who runs an international reference laboratory for avian influenza in Padua, said Gibbs argues that the virus had accumulated mutations known to be seen in laboratory manipulation of viruses, specifically growth in eggs. 
But she said the same changes have been noted in a swine flu virus isolated from a pig in Italy. "So it can occur spontaneously in nature." 
"There is not enough scientific evidence to build this reasoning," Capua said. "This virus could have generated itself by many ways: In swine or in another host or God or laboratory manipulation or whatever. But we just cannot say." 
"This is not how science works. Science works by building a case. And in this case, at least from what I've read, there isn't enough information to build that case." 

Follow Canadian Press Medical Writer Helen Branswell's flu updates on Twitter at CP-Branswell 
The Canadian Press. All rights reserved. 



