


Engineered antibodies fight AIDS virus in monkeys By Maggie Fox, Health and Science Editor Maggie Fox, Health And Science Editor Sun May 17, 1:17 pm ET  <URL>  
WASHINGTON (Reuters) =96 Researchers may have discovered a technique that will eventually lead to a way to vaccinate against the AIDS virus, by creating an artificial antibody carried into the body by a virus. 
This synthetic immune system molecule protected monkeys against an animal version of HIV called SIV, the researchers reported in the journal Nature Medicine. 
While it will be years before the concept could be tested in humans, it opens up the possibility of protecting people against the fatal and incurable virus. 
"Six of nine immunized monkeys were protected against infection by the SIV challenge, and all nine were protected from AIDS," Philip Johnson of Children's Hospital of Philadelphia and colleagues wrote. 
Several attempts to create a vaccine against the human immunodeficiency virus that causes AIDS have failed. 
AIDS not only attacks the immune cells that usually defend against viruses, but it quickly hides out in an as-yet undiscovered "reservoir" so the immune system must be primed to capture virtually every single virus. 
In addition, people do not usually make antibodies against the virus. Antibodies are immune system particles that latch on to invaders so killer cells can destroy them. 
Johnson's team engineered an artificial piece of DNA that would make artificial antibodies, called antibody-like proteins or immunoadhesins. They made three different versions. 
This stretch of DNA was spliced into a virus, called an adeno-associated virus or AAV, that infects people and monkeys with little effect. 
PROTECTED MONKEYS 
They injected the monkeys with their lab-engineered AAV, which started cranking out antibodies in the blood of the monkeys. Then they injected the monkeys with SIV. 
One injection protected the monkeys -- six never became infected at all -- and the three that did never developed AIDS, the immune system destruction caused by HIV, they reported. 
One of the three appeared to work better than the other two but more testing is needed. "To ultimately succeed, more and better molecules that work against HIV, including human monoclonal antibodies, will be needed," they wrote. 
"As a concept, I think this is very promising," Dr. Peggy Johnston, head of the HIV Vaccine Research Branch at the National Institute of Allergy and Infectious Diseases, which helped pay for the study, said in a telephone interview. 
She said the monkeys had an immune response to the AAV virus and the approach would have to be carefully tested to ensure it was safe. In addition, the monkeys were infected by injection and tests would be needed to show the vaccine protected against HIV acquired sexually. 
"We need to make the genes as humanized as possible so that the human body doesn't react to that," she added. 
"I don't see this going into humans for years." 
Most AIDS experts agree the only hope of controlling the pandemic of HIV is to develop a vaccine. The virus has killed 25 million people since the early 1980s and infects 33 million people now. 
Drugs can control infection but often are expensive, have side-effects and often stop working after a time, forcing patients to switch to new drugs. 
--=20 Bob. 


