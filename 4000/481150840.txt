


60 Mexico flu deaths raise global epidemic fears 
 <URL> = s_raise_global_epidemic_fears_.html?page=3D0 
The Associated Press 
Friday, April 24th 2009, 2:06 PM 
MEXICO CITY =97 Mexican authorities said 60 people may have died from a swine flu virus in Mexico, and world health officials worry it could unleash a global flu epidemic. Mexico City closed schools across the metropolis Friday in hopes of containing the outbreak that has sickened more than 900. 
Scientists were trying to determine if the deaths involved the same new strain of swine flu that sickened seven people in Texas and California =97 a disturbing virus that combines genetic material from pigs, birds and humans in a way researchers have not seen before. 
The World Health Organization was looking closely at the 60 deaths =97 most of them in or near Mexico's capital. It wasn't yet clear what flu they died from, but spokesman Thomas Abraham said "We are very, very concerned." 
"We have what appears to be a novel virus and it has spread from human to human," he said. "It's all hands on deck at the moment." 
WHO raised its internal alert system Friday, preparing to divert more money and personnel to dealing with the outbreak. 
President Felipe Calderon cancelled a trip and met with his Cabinet to coordinate Mexico's response. The government has 500,000 flu vaccines and planned to administer them to health workers, the highest risk group. 
There are no vaccines available for the general public in Mexico, and authorities urged people to avoid hospitals unless they had a medical emergency, since hospitals are centers of infection. 
They also said Mexicans should refrain from customary greetings such as shaking hands or kissing cheeks, and authorities at Mexico City's international airport were questioning passengers to try to prevent anybody with possible influenza from boarding airplanes and spreading the disease. 
But the U.S. Centers for Disease Control and Prevention said Americans need not avoid traveling to Mexico, as long as they take the usual precautions, such as frequent handwashing. 
Mexico's Health Secretary, Jose Cordova, said only 16 of the deaths have been confirmed as the new swine flu strain, and that government laboratories were testing samples from 44 other people who died. At least 943 nationwide were sick from the suspected flu, the health department said. 
"We certainly have 60 deaths that we can't be sure are from the same virus, but it is probable," Cordova said, adding that samples were sent to the CDC to look for matches with the virus that infected seven people in Texas and California. 
Cordova called it a "new, different strain ... that originally came from pigs." 

