


On Mon, 13 Jul 2009 10:35:41 +0100, charles < <EMAILADDRESS> > wrote in < <EMAILADDRESS> >: 

Those who have studied the 1918 pandemic in detail are less optimistic. See < <URL> >: 
=== Even with modern antiviral and antibacterial drugs, vaccines, and prevention knowledge, the return of a pandemic virus equivalent in pathogenicity to the virus of 1918 would likely kill >100 million people worldwide. A pandemic virus with the (alleged) pathogenic potential of some recent H5N1 outbreaks could cause substantially more deaths. === 
The H5N1 "bird flu" they were concerned about at the time never achieved significant human-to-human transmission to make it a problem. 
"Swine flu" is being transmitted human-to-human. 

Perhaps the concern is that the current "swine flu" is H1N1 like the 1918 virus, spreading in humans like the 1918 virus, affecting young and healthy people like the 1918 virus, we have a mild wave in June/July like the 1918 virus. 
Actually, it is not so mild - according to WHO figures there have been 429 deaths from 94512 cases in total (just under 0.5%) but in the latest reporting period 47 deaths from 4591 cases (over 1%). Sources I have found class Asian flu and Hong Kong flu as category 2 (up to 0.5% deaths per reported case) so we are already facing something that appears to be more deadly. If, as seems likely, it spreads at least as far as Asian flu or Hong Kong flu current figures suggest it will kill more people than those two put together. If, as in 1918, it is followed by a second wave that peaks at a death rate five times the rate of the first wave then it could be worse than 1918. 

Those who get their health information from TV entertainment programmes may see all this as conspiracy of course. 
--  Owen Rees [one of] my preferred email address[es] and more stuff can be found at < <URL> >  

