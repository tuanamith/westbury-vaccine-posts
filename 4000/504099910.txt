


 <URL> - siv,0,6459170.story 
A team of scientists, including two from Lincoln Park Zoo,  studying the wild chimpanzee population in Tanzania's Gombe  National Park, has discovered chimps falling ill and dying of an  AIDS-like disease, a surprising finding that researchers hope  could lead to new insights into the disease process and  ultimately to a vaccine. 
The study's results are being published in Thursday's edition of  the British research journal Nature. The article reveals that  Gombe chimps infected by certain strains of SIV [Simian  Immunodeficiency Virus] can contract an AIDS-like disease.  Infected Gombe chimps died 10 to 16 times more frequently during  the study period than uninfected chimps. 
The results overturn previous evidence that suggested that  chimpanzees were immune from AIDS and that SIV virus infections  in them were harmless. Researchers already knew that some wild  chimpanzee populations had individuals infected with SIV from  eating infected monkeys. 
A strain of SIV at some point mutated to HIV [Human  Immunodeficiency Virus] in wild chimps in Cameroon in West  Africa, where it jumped into the human population when infected  chimp meat was eaten by humans. 
Over several years of surveillance, the study showed that 10 to  20 percent of the Gombe chimps were SIV-infected. 
The knowledge that some SIV strains can cause AIDS-like disease  in chimps is a unique opportunity, AIDS researchers say, to  compare and contrast how the two closely-related viruses become  disease-causing agents in two closely-related species -- chimps  and humans. Being able to compare and contrast, they say, should  speed up development of more effective AIDS therapies and  vaccines. 
"It is a pretty momentous study," Danny Douek, the chief of the  human immunology section of the National Institutes of Health  vaccine research center in Bethesda, Md. Douek is not a part of  the Gombe project but he is looking for possible AIDS vaccines. 
"You can regard the study as one that provides a missing link in  the history of [the] HIV pandemic. If we identify the  evolutionary adaptations, that opens us therapeutic avenues for  HIV disease," he said. 
Two Lincoln Park Zoo scientists, Elizabeth Lonsdorf and Dominic  Travis, set up a survey to assess the general health problems of  the Gombe chimps. They trained Gombe park biologists to  systematically but non-invasively gather data by following  individual animals a day at a time, noting those that seemed ill  or slowed by injury, collecting fecal and urine samples they  left behind. 
The zoo also trained Gombe park veterinarians how to conduct  thorough necropsies ? animal autopsies ? in the field when a  chimp died. They took relevant tissue and fluid samples, put  them in preservatives and sent them to Chicago for analysis by  veterinary pathologists at the University of Illinois Veterinary  School of Medicine. 
Londorf's and Travis' chimpanzee health survey dove-tailed  perfectly with a project already up and running at Gombe by  University of Alabama at Birmingham AIDS researcher Beatrice  Hahn, the principle author of the Nature article. She had been  studying SIV-infected Gombe chimps for several years. 
"Their roles were critical" in reaching the findings, said Hahn.  "This long-term health monitoring system was all their idea.  There is no other chimpanzee field site that has such a system." 
The necropsies provided were also crucial, said Hahn. Without  it, the study would not have been able to show how the SIV  strain in the Gombe chimps caused the health declines, "so their  contribution was essential" to the project. 


