


 <URL> = ml 
Wolf hybrids scrutinized after pet takes baby from crib By Amy Wilson, Greg Kocher and Emily Ulber 
McClatchy Newspapers 
LEXINGTON, Ky. =97 Dakota, the animal that badly injured 3-day-old Alexander James Smith on Monday, is at the center of a now national discussion that isn't likely to end soon. 
Is she a bad dog who should be destroyed? Or is she a dog acting on a dog's instinct and unfairly taking the blame for what was, essentially, human fault? And, now because of her complicated breed and background, has the matter become so incomprehensibly muddled that she is no longer considered a backyard dog at all? 
When Michael Smith, Alexander's father, spoke to the media Tuesday, he said Dakota was "a Native American Indian" breed and said the breeder told him the dog's grandparentage as "90 percent wolf." 
Critics argue that hybrids are unpredictable and dangerous, that they make poor pets and that there is no rabies vaccine available for wolves or their hybrids. Proponents claim the hybrid wolf is a good companion and is useful in educating the public about wolves. Many claim "once you have had a wolf hybrid, you will never own a dog again." 
Mary Ann Zeigenfuse, a Lexington dog trainer and owner of Best Friends Obedience, said if Dakota is part wolf, she is no expert. 
"If this is a wolf-hybrid, this is not a dog," she said. "It is still partly undomesticated. It may, in some cases, have no fear of humans." 
When asked if she and loving pet owners could domesticate a wolf, she responded, "if I had 10,000 years." 
There have been numerous reports of wolf hybrids injuring people, sometimes fatally. In 2002 in Ballard County, Ky., a wolf hybrid killed a 5-year-old boy; the animal's owner pleaded guilty to a charge of reckless homicide. 
Janece Rollet, a certified canine behaviorist in Georgetown, Ky., is among the people who say that wolf hybrids are potential trouble. She said they are essentially wild animals. 
A wolf hybrid "is not an animal you want living in your house," Rollet said. "Because you're dealing with the basic tenets, the basic behavior of a wolf, not the basic behavior of a dog." 
David Wise of Frankfort, Ky., is among those who defend wolf hybrids. 
"It's probably one of the most loyal animals I've ever had," Wise said. "I had one for 15 years, and it was the most docile animal you ever had. He never once snapped at a child." 
So how does Wise respond to reports of wolf hybrids biting and injuring people? 
"It's all in how they're raised," Wise said. "When they're pups, pups are going to be rough. But as they get older, you show 'em more affection than the roughhousing, and they're gentle as a lamb. They're no different than any other breed of dog in that respect." 
A matter of controversy 
Michael Smith says they have had Dakota and Nikita, her sister, for four years =97 since they were pups. They got the dogs from a Michigan breeder. 
But are Dakota, and her sister who still remains in the Smith home, wolf hybrids? 
The dog breed itself is a matter of some controversy. Is it the same as a Carolina Dog or an American dingo? 
Depends on who you ask. 
Spokesmen for the American Kennel Club and United Kennel Club said Wednesday that those registry organizations do not recognize the Native American Indian dog as a breed. 
However, do an Internet search and you will find sites that refer to Native American Indian dogs. According to Web sites devoted to the dog, there are only five breeders in the United States, and there's much discussion about their position and whether they have any verifiable claim to a dog with a specific native American origin with wolf ancestry. 
Apparently, even the legal status of the dog has been challenged by some Native Americans. 
Sherman Jett, supervisor of Jessamine County, Ky., Animal Control, said he has never heard of a Native American Indian dog, nor has his predecessor. Beckey Reiter, director of Boone County, Ky., Animal Control, also said "that's not a breed I'm aware of." 
Jett said he could not say with certainty what type of animal Dakota is. "I honestly don't know," he said. 
There are animals known as Native American dogs, "but they do not contain wolf," said Rollet. Native American dogs, she said, "are a combination of multiple, larger dogs: husky, German shepherd, malamute and so on." 
A classic mistake 
Dr. Andrew "Butch" Schroyer from the Animal Care Clinic in Lexington said that in 25 years of practice, he has never seen nor treated this breed of dog. 
That means it's hard to talk about its predilections. Still, he added, if it is a wolf hybrid, it is unpredictable, which is not good in a pet. And, he cautioned, like any exotic pet, "just because you can have one, doesn't mean you should have one." 
The disposition of the Native American Indian dog is being hotly defended this week. 
Karen Markel, a breeder at Majestic View Kennels in Lowell, Mich., said that in general, Native American Indian dogs are social and get along well with children. "Any dog trainer will tell you it's negligence" to leave a small child alone with any kind of canine, she said. "Of course the dog will suffer as a consequence." 
Ray Coppinger, a biology professor at Hampshire College who studies canine behavior, said the dog's history =97 where it came from, what it has experienced =97 is exponentially more important than what breed it is. He's seen similar incidents happen with dachshunds, he said. 
Coppinger said that although what happened to the Smiths is tragic, it's a relatively common problem. 
The Smiths made a "classic mistake, out of ignorance, and now they're suffering badly for it," he said. Dogs like Dakota don't recognize infants as people, Coppinger said. "It's no more of an act of violence on the dog's part," he said, "than you eating a steak." 
Copyright =A9 2009 The Seattle Times Company 

