


On Jun 3, 7:50 pm, Ed Stasiak < <EMAILADDRESS> > wrote: 
What U.S. military forces? The U.S. army is not the enormous poised-for-instant action force of today, but a rather somnolent garrison force, most of which is in the South and West, or in the Philippines. 
There is no chance whatever of the Army rushing in to surround LaGuardia and the plane before a million curious on-lookers and every hot reporter in New York gets there. Even the NYC police will be late arriving. 
To begin with, nobody will realize what is happening for a good long time. A lot of people will see the plane, but the vast majority won't understand what's unusual about it. Is it an advertising stunt? Some kind of bizarre hoax? (remember Welles' "War of the Worlds") A new model of airplane? etc. 
It won't be until the plane has grounded, and the passengers and crew have started talking with people on the ground that anyone realizes there has been a time warp. 
Which is not going to be believed immediately by most, and not accepted by anyone in authority very quickly. 
By the time it is, it's quite possible that many of the passengers will have dispersed. They're not injured, but the semi-crash landing shook them up, and the shock of learning they're in 1939 will stun them - and various people may take them away  - reporters, good samaritans, whatever. Some may have realized what has happened, and rushed off to Do Something. 
Odds are that nearly all of the passengers are alive in 1939; and that several of them either lived in NYC or had friends or immediate family there. 
For a comparable data point: after the HINDENBURG crashed and burned in 1938, many of the surviving passengers (about 2/3 survived) were taken away from the landing field in taxicabs. Some literally walked away from the still burning wreck, got right in a cab, and left. 
No, there is absolutely no way this can be hushed up or controlled. 
And once it breaks, there will be a stampede to get in on it. Reporters, souvenir hunters, and agents of every conceivable political and business interest. 
First there will be headline stories in all the NY papers, lavishly illustrated with photos of the plane. 
Then a second wave of stories announcing the time warp element - with interviews of passengers, photos of artifacts, and so on. 
Here's one aspect. Many of the passengers will wander away - and spend some of the post-1939 money in their pockets. Or try to - several will be arrested for trying to pass bizarro counterfeits. A "Roosevelt Dime"? Waddaya sumkinda nut? 

He'll have to. See above. 

Everyone goes into complete shock and paralysis for a while. 
Eventually (after a few days), the U.S. authorities recognize what has happened, and begin to think in terms of securing the plane, the passengers, and the material. 
Bear in mind that this is 1939 - there is certainly concern about the power of technological advantage, but it is not yet seen as the absolute trump card it became after WW II and the atomic bomb. 
A number of the passengers will disperse to association with various factions. I wouldn 't be shocked to learn that there was a true- believing Communist on the plane, who would probably decide to make contact with the Communist organization in NYC and through them with the USSR. 
There might be a German veteran/survivor of WW II. A secret retro-Nazi (i.e. someone who never really repudiated the Nazi creed, even though after defeat he shut up about it)? Unlikely IMO, but not impossible. He'd head for the German consulate or a Bundist office. 
Or someone who having survived the war, and feeling the disgrace as many postwar Germans did, wants to warn Germans and try to get rid of Hitler. 
One key factor here. The benefits of this occurrence are going to be very spotty. Some of the tech embodied in the plane and the passengers' possessions can be reverse engineered. But most, not. Sure, there will be transistor radios aboard, but not a clue on how to make transistors. 
Historiical knowledge will be dependent on the passengers' personal knowledge. There will be a few books and magazines on board, but very little that is _authoritative_. How likely is it that anyone would have, say, a copy of Churchill's history of WW II with him? Or _Crusade in Europe_? 
So it will come down to what the various passengers remember - and that will be, in many cases, highly disputed. 
The Reds tried to take over the U.S. after the war!! No, that's a damn lie by that despicable drunk McCarthy!!! etc. 
It will get even weirder WRT to the USSR. The Khrushchev thaw was in progress - but the details of Stalin's crimes were still very obscure to all but a handful of westerners in 1961 - and the 1939 USSR will immediately move all its considerable forces to discredit any damaging claims. 
Nazi Germany will try to do the same thing - but this will fail, due to the much greater general knowledge of the Holocaust, and Germany's weaker influence. 
However, a basic narrative of OTL 1939-1961 will be arrived at: 
Outbreak of the War in September, after the Stalin-Hitler pact. 
Quick defeat of Poland. 
Phony war. 
Nomination of Willkie. 
Quick defeat of France. Vichy regime; De Gaulle. 
Churchill in power. 
Battle of Britain 
Re-election of FDR. 
The Blitz, sea war, fighting in Africa. 
Invasion of USSR. 
Pearl Harbor; Japanese expansion - Singapore, Bataan. 
German defeat at Moscow. 
Renewed German offensive; Stalingrad. 
Midway, Guadalcanal. 
TORCH, El Alamein. Dewey elected governor of NY. 
"Island-hopping"; Sicily and Italy; Soviet drive west; "Rosie the Riveter". 
D-Day; continued Soviet drive; air war; V-weapons. 
Liberation of France; battle of the Bulge. FDR re-elected over Dewey 
Philippines liberated. Okinawa, Iwo Jima. 
Final defeat of Germany. 
Labor elected in Britain; De Gaulle discarded in France. 
FDR dies, succeeded by Truman. GOP resurgence. 
Hiroshima. 
United Nations. Nurermburg. Revelation of Nazi crimes. 
Breakdown of war-time alliance. Division of Germany. Marshall Plan, Berlin Airlift. NATO. 
Founding of Israel. Communst win in China. 
Indian independence and partition; murder of Gandhi. 
Re-election of Truman over Dewey. GI Bill. 
"Red Scare", McCarthy, Hiss case. 
Soviet A-bomb. 
Korean War; election of Eisenhower. Re-election of Churchill. 
Post-war prosperity in the U.S. and Britain. Polio vaccine. 
Death of Stalin and "de-Stalinization" (with much vagueness and "interpretation"); rise of Khrushchev. 
Re-election of Eisenhower. Civil-rights struggle starts. _Brown_ decision. 
Algerian insurrection. Independence for many colonies. Suez War. 
De Gaulle returns; Ike re-elected. Sputnik etc. 
Cuban revolution. Kennedy (Joe's son!) elected over Nixon (who?). 
"Then it was now, and history stopped." 
This is not comprehensive; it is an approximate outline of what most of the passengers would know about and agree on. "Headline" news, that would be familiar to typical Americans. 
Note that there is no mention of Vietnam, for instance, or of Peron. Eventually these areas would be investigated, but they were not on the radar for most people. Also note that non- American areas would be very sketchily covered. 
British/Commonwealth areas would be somewhere in between, as there would probably 20-30 Brits on the plane, as compared to ~100 Americans, and ~25 other nationalities. 
International airline passengers, in 1961, would IMHO be somewhat more prosperous and better educated than the _average_ American. I.e., they'd nearly all be high-school graduates, and many college graduates, whereas the general population was much less so. 
At a guess, about half the passengers might be regular readers of TIME or Newsweek. 
More obscure details will be very hit-or-miss. Out of 150 passengers, I would estimate at least 15 and possibly 30 veterans of the war; their recollections will provide real detail about whatever they were involved in. Similarly, with various areas of the world or technology fields. 
Reactions by nationality: 
U.S.: great confusion. Shock at the idea of the U.S. intervening decisively in WW II, being the Great Power after it. Some (FDR) might like it, but most Americans were still isolationist. The reported end of the Depression very welcome (but how to get there?). 
Britain: War? Well, of course we win! Churchill?? I suppose. India? To be expected. We must take immediate steps to curb Germany. 
France: What? The mighty French army crushed like a bug? _De Gaulle_??? Extreme thrashing; the army's hidebound leaders will either resist the New Wave or be swept away by it. As with Britain, immediate steps re Germany - but what? 
Germany: Two reactions. 1) Hmm. We _almost_ won. Great! All we have to do is... 2) We lost. Bad. And we did Bad Things. Best to get rid of the Nazis _now_. Both: build that atom bomb _now_. In any case the war move is delayed while this new information is assimilated. 
Italy: Brown trousers. Musso is going to be very very cautious from here on out. 
Japan: We lost. Big time. It sure looks like we never stood a chance, Better pull back for a while. This war that happened - it was all _Tojo_'s fault, Americans and Britons! (Make a deal with Chiang ASAP.) (Atomic bomb - start building it _now_.) 
USSR: Disbelief, followed by a rather twisted effort to gather information. Alliance with Germany? Obviously that was Molotov's fault - he has been liquidated. Stalin a mass murderer - capitalist lies! Khrushchev may get early "retirement". However - _we_ defeated the Nazis! And liberated half Europe, China, and even Cuba! Red Star in orbit! Not bad. (Atomic bomb - start building it _now_.) 
China: Uh-oh. All-out effort to exterminate the Communists _now_. The Japanese may actually be willing to deal. 
Norway, Denmark, Netherlands - we got invaded??? 
Finland, Greece - we got invaded and held out! 
Poland - we got crushed, and eventually "liberated" by the Soviets. Ugh. Better let Danzig go. 
Industrialists and financiers will be all over this. TV? Penicillin? Jet planes? "Computers"? Rocket ships?? More prosaically - helicopters, FM radio, ??? 

