


Deadly new flu strain erupts in Mexico, U.S. 

By Alistair Bell and Noel Randewich Alistair Bell And Noel Randewich - 2 hrs  18 mins ago MEXICO CITY (Reuters) - A strain of flu never seen before has killed up to  60 people in Mexico and has also appeared in the United States, where eight  people were infected but recovered, health officials said on Friday. 
Mexico's government said at least 20 people have died of the disease in  central Mexico and that it may also have been responsible for 40 other  deaths. 
Mexico reported more than 1,000 suspected cases and four possible cases were  also seen in Mexicali, right on the border with California. 
The World Health Organization said tests showed the virus from 12 of the  Mexican patients was the same genetically as a new strain of swine flu,  designated H1N1, seen in eight people in California and Texas. 
"Our concern has grown as of yesterday," U.S. Centers for Disease Control  and Prevention acting director Dr. Richard Besser told reporters in a  telephone briefing. 
Global health officials were not ready to declare a pandemic -- a global  epidemic of a new and deadly disease such as flu. "So far there has not been  any change in the pandemic threat level," Besser said. 
But the human-to-human spread of the new virus raised fears of a major  outbreak and Mexico's government canceled classes for millions of children  in its sprawling capital city and surrounding areas. All large public events  like concerts were suspended in Mexico City. 
Close analysis showed the disease is a never-before-seen mixture of swine,  human and avian viruses, according to the CDC. 
Most of the Mexican dead were aged between 25 and 45, a Mexican health  official said, in a worrying sign. Seasonal flu can be more deadly among the  very young and the very old but a hallmark of pandemics is that they affect  healthy young adults. 
Mexico has enough antiviral drugs to combat the outbreak for the moment,  Health Minister Jose Angel Cordova said. 
The WHO said the virus appears to be susceptible to Roche AG's flu drug  Tamiflu, also known as oseltamivir, but not to older flu drugs such as  amantadine. 
"In the last 20 hours, fewer serious cases of this disease and fewer deaths  have been reported," Cordova told reporters. 
Humans can occasionally catch swine flu from pigs but rarely have they been  known to pass it on to other people. 
NO CONTAINMENT 
The CDC's Besser said it was probably too late to contain this outbreak.  "There are things that we see that suggest that containment is not very  likely," he said. Once it has spread beyond a limited geographical area it  would be difficult to control. 
But there is no reason to avoid Mexico, CDC and the WHO said. "CDC is not  recommending any additional recommendations for travelers to California,  Texas and Mexico," Besser said. 
Worldwide, seasonal flu kills between 250,000 and 500,000 people in an  average year, but the flu season for North America should have been winding  down. 
The U.S. government said it was closely following the new cases. "The White  House is taking the situation seriously and monitoring for any new  developments. The president has been fully briefed," an administration  official said. 
Mexico's government cautioned people not to shake hands or kiss when  greeting or to share food, glasses or cutlery for fear of infection. Flu  virus can be spread on the hands, and handwashing is one of the most  important ways to prevent its spread. 
The outbreak jolted residents of the Mexican capital, one of the world's  biggest cities with 20 million residents. 
One pharmacy ran out of surgical face masks after selling 300 in a day. 
"We're frightened because they say it's not exactly flu, it's another kind  of virus and we're not vaccinated," said Angeles Rivera, 34, a federal  government worker who fetched her son from a public kindergarten that was  closing. 
The virus is an influenza A virus, carrying the designation H1N1. It  contains DNA from avian, swine and human viruses, including elements from  European and Asian swine viruses, the CDC has said. 
The CDC is already working on a vaccine. 
Scientists were working to understand why there are so many deaths in Mexico  when the infections in the United States seem mild, Besser said. 
The CDC said it will issue daily updates at   <URL> . 
The last flu pandemic was in 1968 when "Hong Kong" flu killed about a  million people globally. 
(Additional reporting by Stephanie Nebehay in Geneva and Maggie Fox in  Washington) 
-- Reuters 


This outbreak was probably implanted there by Israeli Mossad terrorists.  What better way to start an epidemic of global proportion -- in a backward  country where sanitation is poor, and right next to the largest mobile  industrialized population on the planet. 

Jason P -- 
Jews have been masterminds at talking from both sides of their mouths. In  the U.S. they are always perverting the facts with new "Zionist" and Jewish  Chauvinism in their choice of language, and political vernacular. And they  insist others must follow suit or they cry "anti-Semitism" like screeching  hyenas. 




