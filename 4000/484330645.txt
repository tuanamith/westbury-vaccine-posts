


cg, 
A large part of the concern is that the one other H1N1 outbreak (until now,  that is) exhibited similar behavoirs to 1918. It appeared in a mild form  early in the year at the end of a season, and reemerged at the beginning of  the next. 
Some theorize that the 1918 Spanish Flu moved into the southern hemisphere's  season, mutated, and moved back in the northern later. Called the Spanish  Flu simply because that was where it was first noted, it could easily have  migrated from northern Africa to southern Spain. There is no way to know, as  subtyping didn't exist, but recoverved intact virus from a frozen soldier  confirmed H1N1. 
Influenza A is subtyped (the 'strain') by two critical proteins on its  surface, haemaggluten and neuraminidase (That is the H in H1, and the N in  N1). There are 16 known H's and 9 known N's, producing a possible 144  possible strains of Influenza A. Only H1, H2, H3, and H5, and N1 and N2,  regularly infect humans on any scale. 
Asian flu was H2N2, Hong Kong was H3N2, Bird Flu was H5N1, and the Russian  Flu (1890) was later subtyped H2, probable N2. 
The only pandemic of H1N1 was 1918, with another non-pandemic outbreak in  1976 (the original Swine Flu). The 1976 outbreak was quickly detected as the  first case was on a military base, and thus the population was relatively  easily isolated, and it subsequently faded, but reemerged later in the year.  It was well-controlled before it reemerged by fearmongering and mass  vaccination with a hastily engineered and produced vaccine that killed 30  people, and left many times more than that with varying degrees of  Guillian-Barre-induced permanent disabilites (the vaccine did what H1N1 did,  overstimulated the immune system leading to a autoimmune attack, and/or  cytokine storm). 
There are other options today, as effective antiviral medications offer a  treatment alternative to mass-vaccination. The key word there is effective.  But the most effective control is individual behavior - wash your hands and  stay away from other people if you or they are sick. 
The difference between the mortality of H1N1 and other strains can be seen  here:  <URL>  The current H1N1 shows the same elevation in the age 20 to 45 range. 
H1N1 also changes faster than other strains. There is mutation (antigenic  drift), and recombination (antigenic shift). H1N1 is a fast-replicator (thus  exceptionally low incubation period of 18 to 48 hours), and its RNA  replication/transcription process makes an error in every 10,000 protein  sequences or so, so antigenic drift occurs constantly. In addition, this  H1N1 is itself already a unique (novel) recombinant of several types of flu,  with a structure never seen before. 
That recombinant novelty, its H1N1 classification, the fact it has killed  healthy young adults, the fact that it has appeared late-season, and has  escaped containment, is highly and continuously mutable, are all of  significant concern, and call for the exceedingly simple individual measures  of washing your hands and staying away from people if you or they are sick. 
That's a "duh", in my book, not a panic. 
If you'd like to know more about influenza, take a half-hour and digest  <URL>  
Allan 
"°cg°" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 



