


 <URL> / US_expects_more_cases_of_widespread_0425.html 
US expects more cases of 'widespread' swine flu Agence France-Presse Published: Saturday April 25, 2009 
WASHINGTON (AFP) â A new multi-strain swine flu virus is widespread and  cannot be contained, US health authorities said Saturday, warning that  they expect to identify more cases. 
"With infections in many different communities as we're seeing, we don't  think that containment is feasible," said Anne Schuchat of the US Centers  for Disease Control and Prevention (CDC). 
"We are not at a point where we can keep this virus in one place ... Now  that we are looking more widely, I really expect us to find more." 
She told reporters that the CDC was focusing on the transmissibility of  the virus, noting that influenza is generally quite transmissible. 
Her comments came after the head of the World Health Organization (WHO)  said the outbreak of the new virus transmitted between humans that has  killed up to 68 people, infected hundreds in Mexico and infected eight in  the United States is a "serious situation" with a "pandemic potential." 
"We are worried and because we are worried, we are working aggressively  on a number of fronts," said Schuchat, the interim deputy director for  the CDC's science and public health program. 
"It is clear that this is widespread. And that is why we have let you  know that we cannot contain the spread of this virus." 
CDC officials were assisting public health authorities in Mexico to test  additional specimens and providing epidemiologic support as part of a WHO  team, she noted. The centers have also dispatched teams in southern  California, where six cases were reported. 
Health leaders in the US, Mexico, Canada and at the WHO were  communicating frequently, and state and local US health authorities were  conducting investigations. 
In order to tackle the transmissibility of the virus, which Schuchat said  was clearly "widespread," the United States and Mexico were using an  already existing an infectious disease surveillance system along the  border to test individuals with respiratory illness for influenza. 
She said that two separate cases had already been detected through that  system. 
Mexican authorities on Friday launched a massive campaign to prevent the  spread of the virus, urging people to avoid contact in public. 
The CDC said tests show some of the Mexican victims died from the same  new strain of swine flu that affected eight people in Texas and  California, who later recovered. 
There is no vaccine to protect humans from swine flu, only to protect  pigs, according to the CDC. 
Schuchat said measures were being taken to produce a vaccine against the  virus if necessary, but cautioned that it usually takes "months" to  produce a vaccine. 
"We're not going to have large amounts of vaccine tomorrow," she warned. 


