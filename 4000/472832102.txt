


Spain Withdraws Gardasil After Illnesses  <URL> = ws-Gardasil-After-Illnesses.aspx 
Spanish health authorities have withdrawn tens of thousands of doses of Gardasil, a vaccine against the human papillomavirus vaccine (HPV), after two teenagers who received the shots were hospitalized. 
The two girls were vaccinated last week as part of a vast government program targeting adolescents. A batch of nearly 76,000 doses of the vaccine has been withdrawn from the market. 
The vaccine has been available since 2006. 


Sources: 
  AFP February 10, 2009 
---------------------------------------------------------------------------= ----------------- 
  Dr. Mercola's Comments: 

Since its launch in 2006, 40 million doses of the Gardasil vaccine have been distributed worldwide, amidst a rash of side effects and complaints. At least Spanish health authorities were quick to act this time, when two girls became seriously ill just hours after receiving the shot. 
In response, close to 76,000 doses of Gardasil were withdrawn from the market, all of them part of batch NH52670. Unfortunately, this is not a move to pull the vaccine from the market entirely; only use of shots from this particular batch has been suspended. 
I don=92t know what more health officials from all over the world could be waiting for to let them know how potentially dangerous this vaccine actually is. 
Over 10,000 adverse reactions, including 29 deaths, have been reported to the Vaccine Adverse Event Reporting System (VAERS) in relation to Gardasil -- and that is just from the United States. 
Although Merck, which manufactures Gardasil, has reported a 16 percent decline in quarterly sales of the vaccine, and said it expects sales this year to be lower than expected, a massive campaign is still underway urging young girls to get vaccinated and become =93one less=94 victim to cervical cancer. 
This is going on in many parts of the world, including in Sweden, which recently announced all primary school girls will be able to be vaccinated with Gardasil for free starting in 2010. 
Merck has also been pushing for an expansion of uses for Gardasil. In the U.S., the vaccine can also be promoted to prevent two rare vaginal and vulvar cancers, and Merck just recently filed for FDA approval to use Gardasil for boys! 
What Types of Risks Does Gardasil Pose? 
Side effects including paralysis and death have been reported in relation to Gardasil, among many, many others. Yet, on October 21, 2008 the Centers for Disease Control (CDC) in association with the FDA released a report alleging that the vast majority, or even ALL, of the 10,000+ adverse reactions reported are not related to the vaccine. Therefore, they say, Gardasil is safe. 
How did they come to this conclusion? Only the investigators know, and the information is not being made public so independent researchers can make their own decisions. 
Well, the National Vaccine Information Center (NVIC), which was co- founded by Barbara Loe Fisher, one of the top vaccine experts in the world, is now calling for the CDC and FDA to publicly release the study design, data, and names of principal investigators involved. 
To not properly evaluate the risks of Gardasil, they say, is =93a callous disregard for human life.=94 As Fisher said: 

 =93Parents of young girls and women cut down in their prime -- some of them paralyzed or dead within hours or days of getting Gardasil vaccine -- deserve better answers than a whitewashing of this vaccine=92s very serious side effects.=94 

On NVIC=92s Web site, you can read several stories of women and girls who have been seriously injured, and in some cases died, shortly after receiving this vaccine, including: 

=95 Christina Tarsell, a 21-year-old college student majoring in studio arts at Bard College, who died suddenly and without explanation shortly after receiving the third Gardasil shot in June 2008. 
=95 Gabrielle, a 15-year-old former gymnast and cheerleader who can no longer attend school and is suffering from severe headaches, heart problems and seizures since getting the vaccine. She has been diagnosed with Inflammation of the Central Nervous System as a result of a Gardasil vaccine reaction, and her condition continues to deteriorate. 
=95 Megan, a 20-year-old college student who died suddenly, without explanation, about one month after receiving her third Gardasil shot. No cause of death was found. 
=95 Ashley, a 16-year-old who became chronically ill after receiving Gardasil, and now suffers regular life-threatening episodes of seizure- like activity, difficulty breathing, back spasms, paralysis, dehydration, memory loss and tremors. 

Sadly, Merck only studied the Gardasil vaccine in fewer than 1,200 girls under 16 prior to it being released to the market, and most of the serious side effects that occurred during the pre-licensure clinical trials were merely called a =93coincidence.=94 
It is beyond me how that explanation can hold water, considering all the bad press coming out about this vaccine. NVIC has been following the risks of Gardasil closely, and just released a new analysis comparing the vaccine to another for meningitis (Menactra). They found, compared to Menactra, Gardasil was associated with: 

=95 At least twice as many emergency room visit reports (5,021) =95 Four times as many death reports (29) =95 Seven times as many disabled reports (261) =95 Three to six times more fainting reports 

Further, there have been a dizzying array of reactions reported among girls who received Gardasil alone, without any other vaccines, such as: 

=95 34 reports of thrombosis =95 27 reports of lupus =95 23 reports of blood clots =95 16 reports of stroke =95 11 reports of vasculitis =95 544 reports of seizures 

On top of this, in the VAERS database there are 467 =93rechallenge=94 reports, which involve cases where there was a worsening of symptoms after a repeated vaccination -- and nearly 60 percent of them are for Gardasil! 
What Makes The Gardasil Vaccine Even More of a Rip-Off? 
It=92s intended to prevent a virus (human papilloma virus, or HPV) your body can clear up on its own, and does so more than 90 percent of the time! 
Even the National Cancer Institute says: 


So while Merck would like you to hear that 6 million women contract HPV annually -- they do not tell you most of those cases are harmless. Cervical cancer actually claims less than 3,900 women a year -- most of which are due to not getting regular Pap smears. 
You should also know that Gardasil does not protect against all types of HPV, and you can still get cervical cancer even if you=92ve been vaccinated. As the CDC states: 

=93About 30% of cervical cancers will not be prevented by the vaccine.=94 

What this all boils down to is that Gardasil is largely ineffective, potentially very dangerous and a major waste of money. 
If you are a parent considering the Gardasil vaccine for your daughter, you would likely be far better off teaching her how to keep her immune system strong and healthy -- and discussing the realities of sexually transmitted diseases and how to avoid them -- than having her injected with this worthless vaccine. 




Related Links: 
  How the U.S. Government is Covering Up HPV Vaccine Side Effects 

  HPV Vaccine Blamed for Teen's Paralysis 

  Gardasil is Dangerous As Well As Unproven 


