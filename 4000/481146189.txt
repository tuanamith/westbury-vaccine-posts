



From a 2005 Scientific American article: 
 <URL>  
Because people will have had no prior exposure to a pandemic strain of influenza, everyone will need two doses: a primer and then a booster about four weeks later. So even those first in line for vaccines are unlikely to develop immunity until at least seven or eight months following the start of a pandemic. 
And there will undoubtedly be a line. Total worldwide production of flu vaccine amounts to roughly 300 million doses a year. Most of that is made in Europe; only two plants operate in the U.S. Last winter, when contamination shut down a Chiron facility in Britain, Sanofi Pasteur and MedImmune pulled out all stops on their American lines-- and produced 61 million doses. The CDC recommends annual flu immunization for high-risk groups that in the U.S. include some 185 million people. 
Sanofi now runs its plant at full bore 365 days a year. In July it broke ground for a new facility in Pennsylvania that will double its output--in 2009. Even in the face of an emergency, "it would be very hard to compress that timeline," says James T. Matthews, who sits on Sanofi's pandemic-planning w orking group. He says it would not be feasible to convert factories for other kinds of vaccines over to make flu shots. 
My note: Why is this? During the Swine Flu panic in the seventies, millions were dosed with a vaccine that caused Guillain-Barré syndrome in about 500 people. Of course, this led to lawsuits and a cancellation of the program. Further, vaccine production is no moneymaker. The chance of having an adequate vaccine supply before this one hits later this year is ZERO. 
--  Invisible Lurker https://twitter.com/ansamanley  



