


Greg Goss < <EMAILADDRESS> > wrote in  <NEWSURL> :  

Good idea. Different animal than a system in a commercial building,  I expect. We have a six inch high pressure pipe feeding it, and  it's a fairly big building (with very high ceilings). There's a  *lot* of water in the system. Draining it isn't a problem. Draining  it in a way that deals with the waste water properly is another  matter. We're draining it slowly with a garden hose, but we had a  visit from an environmental code enforcement guy who didn't object  to the water, but did object to it being drained in to the part of  the parking lot that was still covered with a lot of dried mud from  the mud volcano (the leak washed out a void that's bigger than my  bedroom). Mind you, he had no idea what had happened or where the  water was coming from (thought it was from a pressure line or a  sump pump), but it's still an issue. 
*During* connection, the main _must_ be at zero pressure.  Obviously. For that matter, so does the sprinkler system, but the  main is about eight or ten feet below the lowest point of the  sprinkler system, so gravity will not be our friend. 
You also have to understand the strictness of the federal water  quality standards, and the zeal with which they can sometimes be  enforced. Even the tiniest backwash from a system that sits  stagnant for months at a time (and isn't subject to the normal  rules for keeping *drinking* water clean) would require shutting  down the entire area's water for, probably, several *days* to flush  it out. The costs are enourmous, and the political repercussions  (especially when they *do* know the issues and *do* have procedures  in place to prevent them) even worse. 
--  Terry Austin 
"Terry Austin: like the polio vaccine, only with more asshole."     -- David Bilek 
Jesus forgives sinners, not criminals. 

