



- - - 
One correction/clarification, + a link to Obama's end of federak embryonic stem cell research ban today: 
"Pro-Humanist FREELOVER" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 

I should've said "She died an agonizing death, after -4- years of remission from breast cancer, due to mestastes which traveled to her brain." 
However, taken poetically, or philosphically, each of us could be any of us, and as such, any of us could've been my sister, and could have experienced her life, thought her thoughts, and suffered her fate. 
Any of you could've experienced my life, ex- perienced my naturalistic totality, thought my thoughts. 
What are we but the totality of the physical naturalism that entails each of us, both within us, heavily contributed to by the particular genetics existing in each of us, and surrounding us, the totality of which has led each of us to our thoughts as of any now. 
Cuts to the oneness of all, something we can only empathetically relate to, as it is impos- sible, now, to actually experience what it is to be someone else. 
As for how this type 1 diabetic feels about the following, I'm elated, overjoyed, and hopeful ... 
- - - Obama Stem Cell Research Executive Order Signing The Huffington Post   |   March 9, 2009 11:07 AM    <URL>  - - - 
Excerpts [with one insert, not part of original article, included in brackets]: 
... 
Obama's full remarks, as prepared for delivery: 
Today, with the Executive Order I am about to sign, we will bring the change that so many scientists and researchers; doctors and innovators; patients and loved ones have hoped for, and fought for, these past eight years: we will lift the ban on federal funding for promising embryonic stem cell research. We will vig- orously support scientists who pursue this research. And we will aim for America to lead the world in the discoveries it one day may yield. 
At this moment, the full promise of stem cell research remains unknown, and it should not be overstated. But scientists believe these tiny cells may have the potential to help us understand, and possibly cure, some of our most devastating diseases and conditions. To regenerate a severed spinal cord and lift someone from a wheelchair. To spur insulin production and spare a child from a life- time of needles. To treat Parkinson's, cancer, heart disease and others that affect millions of Americans and the people who love them. 
But that potential will not reveal itself on its own. Medical miracles do not happen simply by accident. They result from painstaking and costly research - from years of lonely trial and error, much of which never bears fruit - and from a government willing to support that work. 
From life-saving vaccines, to pioneering cancer treatments, to the sequencing of the human genome - that is the story of scientific progress in America. When government fails to make these investments, opportunities are missed. Prom- ising avenues go unexplored. Some of our best scientists leave for other countries that will sponsor their work. And those countries may surge ahead of ours in the advances that transform our lives. 
But in recent years, when it comes to stem cell research, rather than furthering discovery, our government has forced what I believe is a false choice between sound science and moral values. 
In this case, I believe the two are not inconsistent. As a person of faith, I believe we are called to care for each other and work to ease human suffering. I believe we have been given the capacity and will to pursue this research  - and the humanity and conscience to do so responsibly. 
It is a difficult and delicate balance. Many thoughtful and decent people are conflicted about, or strongly oppose, this research. I understand their concerns, and we must respect their point of view. 
- - - [insert -- I believe Obama's religion has led him to posi- tions respecting religious points-of-view differ with and are inconsistent with his well-reasoned and logical posi- tions -- end insert] - - - 
But after much discussion, debate and reflection, the pro- per course has become clear. The majority of Americans  - from across the political spectrum, and of all back- grounds and beliefs - have come to a consensus that we should pursue this research. That the potential it offers is great, and with proper guidelines and strict oversight, the perils can be avoided. 
That is a conclusion with which I agree. That is why I am signing this Executive Order, and why I hope Congress will act on a bi-partisan basis to provide further support for this research. We are joined today by many leaders who have reached across the aisle to champion this cause, and I commend them for that work. 
... 
This Order is an important step in advancing the cause of science in America. But let's be clear: promoting science isn't just about providing resources - it is also about pro- tecting free and open inquiry. 
It is about letting scientists like those here today do their jobs, free from manipulation or coercion, and listening to what they tell us, even when it's inconvenient - especially when it's inconvenient. It is about ensuring that scientific data is never distorted or concealed to serve a political agenda - and that we make scientific decisions based on facts, not ideology. 
By doing this, we will ensure America's continued global leadership in scientific discoveries and technological break- throughs. That is essential not only for our economic pros- perity, but for the progress of all humanity. 
That is why today, I am also signing a Presidential Memor- andum directing the head of the White House Office of Science and Technology Policy to develop a strategy for restoring scientific integrity to government decision mak- ing. 
To ensure that in this new Administration, we base our public policies on the soundest science; that we appoint scientific advisors based on their credentials and experi- ence, not their politics or ideology; and that we are open and honest with the American people about the science behind our decisions. That is how we will harness the power of science to achieve our goals - to preserve our environment and protect our national security; to create the jobs of the future, and live longer, healthier lives. 
As we restore our commitment to science, and resume funding for promising stem cell research, we owe a debt of gratitude to so many tireless advocates, some of whom are with us today, many of whom are not. Today, we honor all those whose names we don't know, who organ- ized, and raised awareness, and kept on fighting - even when it was too late for them, or for the people they love. 
And we honor those we know, who used their influence to help others and bring attention to this cause - people like Christopher and Dana Reeve, who we wish could be here to see this moment. 
One of Christopher's friends recalled that he hung a sign on the wall of the exercise room where he did his grueling regimen of physical therapy. It read: "For everyone who thought I couldn't do it. For everyone who thought I shouldn't do it. For everyone who said, 'It's impossible.' See you at the finish line." 
Christopher once told a reporter who was interviewing him: "If you came back here in ten years, I expect that I'd walk to the door to greet you." 
Christopher did not get that chance. But if we pursue this research, maybe one day ... others like him might. 
There is no finish line in the work of science. The race is always with us - the urgent work of giving substance to hope and ... seeking a day when words like "terminal" and "incurable" are finally retired from our vocabulary. 
Today, using every resource at our disposal, with re- newed determination to lead the world in the discov- eries of this new century, we rededicate ourselves to this work. 
... 
- - - end excerpts - - - 



