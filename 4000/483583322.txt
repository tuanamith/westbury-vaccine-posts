


 <URL>  
Confirmed number of flu cases jumps to 331       (CNN) -- The number of confirmed cases of the H1N1 virus stands at 331 people, the World Health Organization said Friday.    Tourists sunbathe wearing surgical masks in the popular Mexican resort of Acapulco.  
The virus, commonly known as swine flu, has spread to 11 countries, but the hardest hit areas were in the western hemisphere, the organization said.  
"We have not seen sustained human-to-human transmission anywhere outside the Americas region," said WHO spokesman Thomas Abraham. 
In the United States, the Centers for Disease Control and Prevention said it had confirmed 109 cases of the flu with one death.  
The largest outbreak was in Mexico, which had 156 confirmed cases, according to the WHO. It added that Mexico had nine deaths attributed to the virus.  
However, Mexican officials said the death toll had risen to 12 and they suspect more than 150 deaths in the country are linked to the virus.  Watch how Mexican authorities are dealing with the outbreak » 
Mexican Health Minister Jose Angel Cordova Villalobos said Thursday that the country has more than 300 confirmed cases of the virus, a higher number than the WHO reported Friday. 
Despite the jump in confirmed cases, a senior WHO official said Thursday that the higher totals did not necessarily mean the incidence of the disease is increasing. Dr. Keiji Fukuda, assistant director-general of WHO, said it could be more to do with health investigators getting through their backlog of specimens. 
The latest tally was announced two days after WHO raised the pandemic threat level to 5 on a 6-step scale.  Watch Dr. Sanjay Gupta demystify pandemics » 
The level 5 designation means widespread human infection from the outbreak that originated in Mexico has been jumping from person to person with relative ease. 
"It really is all of humanity that is under threat during a pandemic," said Dr. Margaret Chan, the WHO's director-general. "We do not have all the answers right now, but we will get them."  View images of responses in U.S. and worldwide » 
In addition to Mexico and the United States, WHO said, the following countries have confirmed non-lethal cases: Austria (1), Canada (34), Germany (3), Israel (2), Netherlands (1), New Zealand (3), Spain (13), Switzerland (1) and the United Kingdom (8). 
An additional 230 cases are being investigated in the United Kingdom, and Spain has 84 suspected cases. Australia, which has had no confirmed cases, was investigating 114.  See where cases have been confirmed » 
In the United States, the CDC said cases had been confirmed in 11 states; 20 others have suspected cases. 
"This is a rapidly evolving situation, a situation with uncertainty," acting CDC Director Richard Besser said at a news conference Thursday. 
New York has the most confirmed cases, with 50, followed by Texas, with 26. California has 14 cases. 
The CDC added the 11th state Thursday -- South Carolina, with 10 cases. 
The state health departments in Delaware, Colorado, Georgia, Nebraska and Virginia on Thursday said they had confirmed cases: four in Delaware, one in Georgia and two each in Colorado, Nebraska and Virginia. California state officials said they had two new confirmed cases Thursday and Florida's health department said it sent three samples it can't identify to the CDC. Their counts have not been added to the national total. 
"There are many more states that have suspected cases and we will be getting additional results," Besser said. 
There still is just one confirmed death from the virus in the United States. A toddler from Mexico died at a Houston, Texas, hospital Monday. 
As the virus spread to more people, officials took precautions. 
Nearly 300 schools with confirmed or possible H1N1 cases were closed Thursday, affecting about 169,000 students, the U.S. Department of Education reported. No colleges or universities were known to be closed, the agency said. 
Nowhere in the world is the crisis more severe than in Mexico, where the first cases were detected. 
All schools have been closed throughout the nation, and the Mexico City government has shut down most public venues and ordered restaurants to serve only take-out food. That has affected about 35,000 businesses. 
Department of Homeland Security Director Jane Napolitano asked parents of children whose schools closed to keep the children at home instead of taking them out in public -- where the flu could continue to spread. 
"The entire purpose is to limit exposure," she said. "If a school is closed, the guidance is and the request is to keep your young ones home." 
The Mexican federal government will close all nonessential government offices and businesses starting Friday. 
The virus is a contagious respiratory disease that affects pigs and can jump to humans. Symptoms include fever, runny nose, sore throat, nausea, vomiting and diarrhea.  Learn about the virus » 
But the 2009 H1N1 virus is a hybrid of swine, avian and human strains, and no vaccine has been developed for it. 
Ecuador joined Cuba and Argentina in banning travel to or from Mexico. Egypt began culling all pigs Thursday although there have been no reported cases of swine flu in that country. 
China and Russia have banned pork imports from the United States and Mexico, though the WHO says the disease is not transmitted through eating or preparing pork. 
Amidst the anxiety, health officials tried to tamp down concerns. 
"When you think pandemic, people tend to reflect on the pandemics from years past," said Dr. Sanjay Gupta, CNN's chief medical correspondent. "A lot has changed. We are better taking care of people in hospitals, we have antiviral medications. It doesn't mean everyone's going to die."  

