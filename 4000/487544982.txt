


 <URL>  
Missouri Man, 44, Dies From Swine Flu  Tuesday, May 19, 2009   
CLAYTON, Mo.   A 44-year-old St. Louis County man diagnosed with swine flu after traveling to Mexico died Tuesday, state and county health officials said. 
The man's name was not released. He died Tuesday morning at an undisclosed St. Louis area hospital. 
Missouri Department of Health and Senior Services spokesman Kit Wagar said the man had a confirmed case of swine flu. But an autopsy and further testing is necessary to determine if the illness killed him or if another underlying illness caused his death, health officials said. 
County health department director Dr. Delores Gunn said it could be several weeks before the U.S. Centers for Disease Control and Prevention confirms the cause of death. 
State and county health officials said the man traveled to Mexico last month, then became ill about a week after returning home. He went to his doctor, then to an urgent care center on May 9, then to the hospital. He was being treated with anti-viral medication, Gunn said. 
If swine flu is confirmed as the cause of death, the man would be the first person in Missouri to die during the current outbreak of the disease. Missouri has reported 20 cases of swine flu. 
The New York City Health Department is also investigating the death of a toddler as a possible swine flu case. 
The 16-month-old boy died Monday night at Elmhurst Hospital Center in Queens. Hospital officials said he had a high fever when he was brought in. 
Health Department spokeswoman Jessica Scaperotti confirmed the investigation but said the agency does not discuss specific cases. It's not yet known how long the investigation will take. 
On Sunday, a public school assistant principal, Mitchell Wiener became the city's first swine flu death. 
Hospital and city officials say complications besides the virus probably played a part in Wiener's death. But his family has said he suffered only from gout, a joint disease. 
Meanwhile, the husband of the first American with swine flu to die is denying media reports that she had a pre-existing medical condition. 
Steven Trunnell told CNN's "Larry King Live" Monday night that his late wife was "a healthy pregnant woman" who had never been diagnosed with "major medical complications of any kind." 
Judy Trunnell went into the hospital April 19 and remained until her death May 5. While in the hospital she slipped into a coma and gave birth to a healthy baby girl, delivered by Cesarean section. 
Eighty people have died worldwide from swine flu including 72 in Mexico, six in the U.S., one in Canada and one in Costa Rica. Officials said victims from Canada, U.S. and Costa Rica also had other medical conditions. 
The World Health Organization says 40 countries have reported more than 8,829 cases, mostly in U.S. and Mexico. 
Forty-six U.S. states and the District of Columbia have a combined 5,123 confirmed and thousands of other probable cases. Most probable cases are eventually confirmed. 
The WHO says drug manufacturers won't be able to start making a vaccine until mid-July at the earliest. The virus isn't growing very fast in laboratories, making it difficult for scientists to get a key vaccine ingredient. 


