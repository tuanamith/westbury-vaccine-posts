



"Richard Schultz" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
Now, after these insults.  Can you answer his question? 
 <URL>  
Thimerosal Studies 

"Safety of Thimerosal-Containing Vaccines: A Two-Phased Study of  Computerized Health Maintance Organization Database" Pediatrics, Thomas Verstraeten, MD (November 2003) 
Conflicts: 
Written by the Centers for Disease Control, the federal agency in charge of  the vaccine program. The lead author, Thomas Verstraeten, left to take a job  with Glaxo SmithKline - a vaccine manufacturer - after the study was written  and before it was published. The U.S. Congress later cited this as an  ethical violation. 
"Thimerosal and the Occurrence of Autism: Negative Ecological Evidence from  Danish Population-Based Data" Pediatrics, Kreesten M. Madsen, MD (September 2003) 
Conflicts statement from the study: 
"The activities of the Danish Epidemiology Science Centre and the National  Centre for Register-Based Research are funded by a grant from the Danish  National Research Foundation. This study was supported by the Stanley  Medical Research Institute." [Two of the seven authors were employees of  Denmark's largest vaccine manufacturer, Statens Serum Institute] 
"Continuing Increases in Autism Reported to California's Developmental  Services System" Archives of General Psychiatry, Robert Schechter, MD (January 2008) 
Conflicts statement from the study: 
"This study was supported through the California Department of Public  Health." 
"Thimerosal-Containing Vaccines Neuropsychological Performance 10 Years  After Immunization in Infancy With Thimerosal-Containing Vaccine" Pediatrics, Alberto Eugenio Tozzi, Patrizia Bisiacchi (February 2009) 
Conflicts statement from the study: 
The study was supported in part by the US Centers for Disease Control and  Prevention, through contract 2002-N-00448 with the Istituto Superiore di  Sanita. 
"Autism and Thimerosal-Containing Vaccines: Lack of Consistent Evidence for  an Association" American Journal of Preventive Medicine, Paul Stehr-Green, DrPh, MPH (August  2003) 
Conflicts statement from the study: 
"Financial support for the compilation of the data used in this  investigation and the preparation of this report was provided by the  National Immunization Program, Centers for Disease Control and Prevention.  We are grateful to Victoria Romanus of the Swedish Institute for Infectious  Disease Control, Ingrid Trolin of the Swedish Medical Products Agency,  Anne-Marie Plesner and Peter Andersen of the Danish Statens Serum Institut  Institut [Denmark's largest vaccine company], and Roger Bernier and Susan  Chu of the Centers for Disease Control and Prevention for their  contributions in the design and conduct of this investigation, and in the  preparation and review of this manuscript." 
"Thimerosal Exposure in Infants and Developmental Disorders: A Prospective  Cohort Study in the United Kingdom Does Not Support a Causal Association" Pediatrics, John Heron and Nick Andrews, PhD (September 2004) 
Conflicts statement from the study: 
"Financial support for the establishment of the ALSPAC cohort was provided  by the Medical Research Council, the Wellcome Trust, the UK Department of  Health [the British CDC], the Department of the Environment, and DfEE, the  National Institutes of Health, and a variety of medical research charities  and commercial companies [vaccine makers]. Funding for this study was  provided by the Department of Health [the British CDC]." 
"Early Thimerosal Exposure and Neuropsychological Outcomes at 7 to 10 Years" New England Journal of Medicine, Thompson WW et al. (September 27, 2007) 
Conflicts statement from the study: 
"Dr. Thompson reports being a former employee of Merck; Dr. Marcy, receiving  consulting fees from Merck, Sanofi Pasteur, GlaxoSmithKline, and MedImmune;  Dr. Jackson, receiving grant support from Wyeth, Sanofi Pasteur,  GlaxoSmithKline, and Novartis, lecture fees from Sanofi Pasteur, and  consulting fees from Wyeth and Abbott and serving as a consultant to the FDA  Vaccines and Related Biological Products Advisory Committee; Dr. Lieu,  serving as a consultant to the CDC Advisory Committee on Immunization  Practices; Dr. Black, receiving consulting fees from MedImmune,  GlaxoSmithKline, Novartis, and Merck and grant support from MedImmune,  GlaxoSmithKline, Aventis, Merck, and Novartis; and Dr. Davis receiving  consulting fees from Merck and grant support from Merck and GlaxoSmith-  Kline. No other potential conflict of interest relevant to this article was  reported." 
[Seven separate vaccine manufacturers are mentioned - a record!] 
"Association Between Thimerosal-Containing Vaccine and Autism" Journal of the American Medical Association, Anders Hviid, MSc (October  2003) 
Conflicts statement from the study: 
"Danish Epidemiology Science Centre, Department of Epidemiology Research  (Messrs Hviid, Wohlfahrt, and Dr Melbye) and Medical Department (Dr  Stellfeld), Statens Serum Institut [Denmark's largest vaccine company],  Copenhagen, Denmark." 
"Mercury concentrations and metabolism in infants receiving vaccines  containing thiomersal: A descriptive study" The Lancet, Michael Pichichero, MD (November 2002) 
Conflicts statement from the study: 
"The investigation was funded by the US National Institutes of Health  (NIH)." 
From the lead author's website: 
"Dr. Pichichero was a member of the discovery team at the University of  Rochester that invented, tested and licensed a Haemophilus influenzae b  (Hib) conjugate vaccine (HibTITER®) now universally given to children in the  U.S. [he makes vaccines, but didn't report as a conflict]" 
"Thimerosal and Autism?" Pediatrics, Karen Nelson, MD (March 2003) 
Conflicts: 
Lead author works at a government agency, National Institute of Neurological  Disorders and Stroke. 
"Lack of Association Between Rh Status, Rh Immune Globulin in Pregnancy and  Autism" American Journal of Medical Genetics, Judith H. Miles and T. Nicole  Takahashi (May 2007) 
Conflicts statement from the study: 
"This study was supported by a grant from Johnson and Johnson Company and  ongoing autism research support from the Leda J. Sears Trust." [Note:  Johnson & Johnson manufacturers the Rh Immune Globulin vaccine, the subject  of the study] 
MMR Studies 

"Lack of Association Between Measles Virus Vaccine and Autism with  Enteropathy: A Case-Control Study" PLoS One, Mady Hornig, Thomas Briese T, et al. (September 2008) 
Conflicts statement from the study: 
"This work was supported by CDC grant U50 CCU522351 to AAP [American Academy  of Pediatrics] and by National Institutes of Health awards AI57158  (Northeast Biodefense Center-Lipkin), HL083850, and NS47537. 
Role of Study Sponsors: Members of the funding organization (AAP) and its  sponsor (CDC) participated along with experts in virology and neurovirology,  autism pathogenesis, and vaccine design and safety; representatives of the  autism advocacy community; and study collaborators in an Oversight Committee  that reviewed and agreed to all aspects of study design prior to data  collection." 
"MMR Vaccination and Pervasive Developmental Disorders: A Case-Control  Study" The Lancet, Liam Smeeth, MRCGP, Eric Fombonne, MD (September 11, 2004) 
Conflicts statement from the study: 
"E Fombonne has provided advice on the epidemiology and clinical aspects of  autism to scientists advising parents, to vaccine manufacturers (for a fee),  and to several government committees. A J Hall received a financial  contribution from Merck towards research on hepatitis B vaccination in 1998.  He is also a member of the Joint Committee on Vaccines and Immunisation  (2002-present)." 
"Pervasive Developmental Disorders in Montreal, Quebec, Canada: Prevalence  and Links With Immunizations" Pediatrics, Eric Fombonne, MD (July 2006) 
Conflicts statement from the study: 
"In the United Kingdom, Dr Fombonne has provided advice on the epidemiology  and clinical aspects of autism to scientists advising parents, to vaccine  manufacturers, and to several government committees between 1998 and 2001.  Since June 2004, Dr Fombonne has been an expert witness for vaccine  manufacturers in US thimerosal litigation." 
"No Evidence for a New Variant of Measles-Mumps-Rubella-Induced Autism" Pediatrics, Eric Fombonne, FRCPsych (October 2001) 
Conflicts statement from the study: 
None noted, although Fombonne's conflict statement above reads, "Dr Fombonne  has provided advice on the epidemiology and clinical aspects of autism to  scientists advising parents, to vaccine manufacturers, and to several  government committees between 1998 and 2001. Since June 2004, Dr Fombonne  has been an expert witness for vaccine manufacturers in US thimerosal  litigation." 
"No effect of MMR withdrawal on the incidence of autism: a total population  study" Journal of Child Psychology and Psychiatry, Hideo Honda, Michael Rutter 
Conflicts statement from the study: 
None noted, an egregious omission given what is known about Dr. Michael  Rutter of the UK: Not only is Dr. Rutter a paid expert witness for vaccine  makers facing litigation regarding the MMR vaccine, he was also a board  member of the Wellcome Foundation, a front foundation for Glaxo Smithkline,  a vaccine maker. Dr. Rutter is also a primary witness in the case against  Dr. Andrew Wakefiled, a British doctor who published a paper implicating MMR  vaccine in autism. 
"Measles Vaccination and Antibody Response in Autism Spectrum Disorders" Archives of Disease in Childhood, Gillian Baird (February 2008) 
Conflicts statement from the study: 
"Funding: The study was funded by the Department of Health, the Wellcome  Trust, the National Alliance for Autism Research (NAAR) and Remedi. The  sponsors of the study had no role in study design, data collection, data  analysis, data interpretation or writing of the report. The corresponding  author had full access to all the data in the study and final responsibility  for the decision to submit for publication. 
Competing interests: MA and DB have given unpaid advice to lawyers in MMR  and MR litigation. GB has acted as an occasional expert witness for the  diagnosis of autism. AP receives royalties from SCQ and ADOS-G instruments.  PBS has acted as an expert witness in the matter of MMR/MR vaccine  litigation." 
"Neurologic Disorders After Measles-Mumps-Rubella Vaccination" Pediatrics, Annamari Makela, MD (November 2002) 
Conflicts statement from the study: 
"Dr Makela was partially supported by a grant from Merck & Co. [the  manufacturer of the MMR vaccine]." 
"Association of Autistic Spectrum Disorder and the Measles, Mumps, and  Rubella Vaccine" Archives of Pediatrics & Adolescent Medicine, Eric Fombonne, FRCPsych (July  2003) 
Conflicts statement from the study: 
"This study was supported by a grant from the Canadian Institutes for Health  Research, Ottawa, Ontario. Dr Wilson is a Canadian Institutes for Health  Research New Investigator." 
* We are actually reviewing 19 studies. The truth is, the public figures who  discuss the "overwhelming" evidence have different numbers that they cite  for the number of studies done. We chose to name our website "Fourteen  Studies" to match the quote from Amanda Peet. 



