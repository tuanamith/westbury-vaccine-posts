


Swine flu: Pandemic declared Last updated 02:42 12/06/2009 

BREAKING The World Health Organisation has told its member nations it is declaring a swine flu pandemic - the first global flu epidemic in 41 years. 
In a statement sent to member countries, WHO said it decided to raise the pandemic warning level from phase 5 to 6 =96 its highest alert =96 after holding an emergency meeting on swine flu with its experts. 
The long-awaited pandemic decision is scientific confirmation that a new flu virus has emerged and is quickly circling the globe. 
It will trigger drugmakers to speed up production of a swine flu vaccine and prompt governments to devote more money toward efforts to contain the virus. 
"At this early stage, the pandemic can be characterized globally as being moderate in severity," WHO said in its statement to member countries, urging nations not to close borders or restrict travel and trade. "(We) remain in close dialogue with influenza vaccine manufacturers." 
The last pandemic - the Hong Kong flu of 1968 - killed about 1 million people. Ordinary flu kills about 250,000 to 500,000 people each year. 
On Wednesday, the WHO reported that the virus has infected at least 27,737 people in 74 countries and caused 141 deaths. Most cases have been in North America, but Europe and Australia have seen a sharp increase in recent days. 
Four patients with swine flu were fighting for their lives in Melbourne hospitals, while Thailand's Public Health Ministry said the country's case load had nearly tripled to 46, including 17 infected at a disco in the resort town of Pattaya. 
Earlier there had been speculation that a  jump in infections in Australia could push WHO to finally announce a pandemic. Australia's cases reached 1275 by late Wednesday. 
NEW ZEALAND'S SITUATION 
An Auckland daycare centre has been closed and part of Auckland hospital put on lock-down as the number of confirmed cases of swine flu rises to 27. 
An Auckland nurse and her child who returned from a family holiday in the United Kingdom on Air NZ flight NZ1 on June 6 have both tested positive for Influenza A (H1N1). 
The Ministry of Health said the nurse worked one shift at Auckland City Hospital's Renal Medicine and Transplant Ward (Ward 71) on June 8 and the child attended ABC Childcare Centre in Meadowbank the same day. Contacts of both cases are currently being traced. 
Two former patients who had been cared for by the nurse during the 12- hour shift on June 8 have been visited by medical staff and care plans are in place. 
19 ADHB staff who were in close contact with the nurse are also at home in quarantine and taking Tamiflu. Ad Feedback 
The Ministry of Health said none of the close contacts have flu symptoms at present. 
SPREAD OF THE FLU 
A disease is classed as a pandemic when transmission between humans becomes widespread in two regions of the world. 
Australia has the fifth-highest number of H1N1 flu cases worldwide, after the United States, Mexico, Canada and Chile. 
But Australian authorities on Thursday defended their handling of H1N1, saying the high number of cases, the overwhelming majority in the southern state of Victoria, was a result of widespread testing and a normal winter flu season. 
"We have tested 5500 people in the last two weeks, that is more people than we test in our whole influenza season," said Victorian state premier John Brumby. "If you test that many people you will find something." 
In New Zealand the other two new cases were reported in the Waikato, where a factory worker and a New Zealander recently returned from Melbourne have tested positive to the virus. 
Auckland City Hospital chief medical officer Dr David Sage said the nurse did not have flu symptoms when she was at work on June 8. 
However, her child began to show flu-like symptoms that day and immediate medical treatment was sought. 
The nurse returned home after completing her shift. The family voluntarily isolated themselves at home while awaiting the test results for their child at which time the nurse began exhibiting flu symptoms. 
Last night, both swabs returned positive results for Influenza A (H1N1). 
Dr Sage said staff at Auckland City Hospital acted swiftly to identify, isolate and treat all those potentially affected as soon as the alert was raised. 
"At this time, none of these people are showing flu-like symptoms," he said. 
"I would like to commend all the staff who worked tirelessly last night and this morning to help us contain the potential spread of this virus," Dr Sage said. 
Dr Sage said Ward 71 remained open but is closed to new admissions and has strict infection-control procedures in place. 
Five patients are being cared for in isolation and there is a limit on visitors to patients and the number of staff working in the ward has been reduced. 
The ABC Childcare Centre has closed temporarily. The childcare centre's staff are currently working with Auckland Regional Public Health (ARPH) staff to identify children who were at the centre on June 8. 
ARPH's clinical director Dr Julia Peters said staff were working closely with the childcare centre. The 29 children enrolled at the Centre and seven staff are being offered Tamiflu and are in isolation at home. 
The Ministry of Health's chief advisor on public health, Dr Ashley Bloomfield, said as more people tested positive for influenza A (H1N1) "swine flu" there would be more instances where workplaces, schools, and child care centres would be affected. 
"People have been highly cooperative to date.  This has undoubtedly contributed to New Zealand's relatively low number of confirmed cases. 
"As has been demonstrated this week, each case can have a large number of contacts, and already, this virus is causing significant disruption to the regular routines and lives of those affected. 
"Our assessment is that our actions to "keep it out" and "stamp it out" are still appropriate and worthwhile.  There are real benefits to delaying widespread community transmission of the virus for as long as possible," Dr Bloomfield said. 
Health Minister Tony Ryall said the growing number of confirmed Swine Flu cases were "anticipated" and are "no cause for alarm". 
"It is inevitable we will get much wider spread through the community at some stage and New Zealanders should not be worried by this. We have longstanding plans to deal with the situation," said Mr Ryall. 
"So far we have been successful in delaying the spread of swine flu in New Zealand.  Every week we slow the spread is another week our health services aren't overburdened with even more flu sufferers than usual. " 
Mr Ryall said although swine flu had been mild so far in New Zealand, it was contagious. 
"With community spread there will be many more people arriving at EDs and GP clinics on top of the usual winter flu numbers." 
Health officials are constantly reviewing the situation at home and internationally, Mr Ryall said. 
In the meantime the Health Minister says it is important people continue to hide sneezes and coughs in a tissue, then wash and dry their hands thoroughly. If they think they have flu symptoms they should, in the first instance, call their doctor or Healthline (0800 611 116). 
- By CLIO FRANCIS, AP and Reuters 


