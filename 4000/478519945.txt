



Note from Health Lover Ilena Rosenthal  <URL>  
I read this on the Vaccination PR Frontgroup list called Healthfraud (which they are!) led by Stephen Barrett head of Snakeoil Vigilantes:  <URL>  
They want to be able to harm children and not give the families any recourse for destroying the lives of their children. 


 <URL>  
Doctor Groups Seek Overturn of Vaccine Jurisdiction Ruling By John Gever, Senior Editor, MedPage Today Published: April 10, 2009 	Click here to rate this report WHEELING, W.Va., April 10 -- Several medical societies are asking the U.S. Supreme Court to reverse a lower-court ruling that would expose vaccine manufacturers to litigation in state courts. 
In October, the Georgia Supreme Court ruled unanimously that state courts could hear cases involving alleged vaccine design defects, even though the National Vaccine Injury Compensation Act of 1986 established a national "vaccine court" to hear such suits. 
Now the American Academy of Pediatrics (AAP), the American Medical Association (AMA), and eight other groups have filed an amicus brief in the case, which is currently before the U.S. Supreme Court. 
"If this decision is allowed to stand, it could lead to the very same crisis that Congress sought to prevent in passing the original legislation," Stephan E. Lawton, J.D., an AAP attorney, said in a statement announcing the brief's filing. 
"There is a genuine threat to our nation's public health if manufacturers abandon or consider abandoning the production of vaccines. This decision would set our country back decades, and have deadly consequences for our children," Lawton said. 
Prior to the Georgia ruling, courts had consistently ruled that the national "no-fault" system created in the 1986 law pre-empted litigation in state courts. 
The law specifically assigns product liability cases involving vaccines to the Office of Special Masters of the U.S. Court of Federal Claims, known as the "vaccine court." It currently has thousands of cases before it, and until the Georgia decision, both federal and state courts had agreed that the federal law pre-empts action at the state level. 
The medical groups' brief urges the court to uphold the no-fault, vaccine court system as the only venue for vaccine liability cases. 
The case, American Home Products vs Ferrari, was brought by parents of a boy who allegedly developed neurological problems, including autism, after receiving childhood vaccines containing thimerosal. 
Thimerosal is a mercury-based preservative that has become a lightning rod for lawsuits and lobbying activities among parents of children with autism-spectrum conditions. 
Since 2001 thimerosal has been removed from most vaccines given to the youngest children. However, studies by the CDC and other agencies have failed to find any direct link between vaccines and autism. 
The nine vaccine makers named as defendants in the suit are appealing the Georgia decision to the U.S. Supreme Court. 
Joining the AAP and the AMA in the brief were the American Academy of Family Physicians, the AAP Section on Infectious Diseases, the American College of Osteopathic Pediatricians, the American Public Health Association, Every Child By Two, Immunization Action Coalition, the Infectious Disease Society of America, the Pediatric Infectious Disease Society, and the Vaccine Education Center at The Children's Hospital of Philadelphia.  


