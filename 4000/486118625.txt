


Tamiflu Developer: Swine Flu May Have Been Released From A Lab 
One of the first scientists to properly analyze the genetic makeup of the so-called swine flu virus that emerged three weeks ago in Mexico and led to fears of a global pandemic states that the virus may have escaped from a laboratory. 
.................... 
First scientist to study genetic makeup of virus says it may have escaped from a vaccine production facility 
Wednesday, May 13, 2009 
"Adrian Gibbs, 75, who collaborated on research that led to the development of Roche Holding AG's Tamiflu drug, said in an interview that he intends to publish a report suggesting the new strain may have accidentally evolved in eggs scientists use to grow viruses and drugmakers use to make vaccines," reports Bloomberg News. 
The World Health Organization received Gibbs' report study last weekend and is now in the process of analyzing it. 
"One of the simplest explanations is that it's a laboratory escape," said Gibbs, who has studied germ evolution for four decades, adding that the virus could have been released "accidentally" from a virus production facility. 
"In addition, Gibbs said his research found the rate of genetic mutation in the new virus was about three times faster than that of the most closely related viruses found in pigs, suggesting it evolved outside of swine," states the report. 
The U.S. Centers for Disease Control and Prevention in Atlanta were quick to deny Gibbs' report, saying they found no evidence to verify his conclusions. 
In our reports immediately following the swine flu outbreak, we speculated that the virus may have been created in a lab due to its highly synthetic nature, being a combination of several never-before-seen intercontinental human, avian and pig viruses from America, Europe and Asia. 
As we reported in March, Baxter International, the same company chosen to make the swine flu vaccine, were caught distributing vaccines to 18 different labs in Europe that were tainted with the deadly avian flu virus. 
Czech newspapers questioned whether the release was part of a deliberate conspiracy to provoke a pandemic. 







