


            HOTSHEET FOR TUESDAY MAY 5, 2009 

-- RADIO ONE -- 
1. THE CURRENT: 
A new report from Canada's Military Police Complaints Commission has found systemic problems with how Canadian military police are acting in Afghanistan - specifically, that they are not investigating potential abuse cases because they don't understand that is part of their mandate. Today, The Current presents the latest news on the situation and finds out how it's affecting Canada's mission in Afghanistan. 
Also on the program, the race for a vaccine for the H1N1 influenza A virus is underway. Some health experts are hopeful that a vaccine can be developed in a matter of weeks. But others fear that there are risks associated with the rush to get one out so quickly. 
Hear about those issues and more on The Current, with Anna Maria Tremonti, this morning at 8:30 (9:00 NT) on CBC Radio One. 
2. THE POINT: 
For the first time in its history, the CNIB has a sighted president and CEO. 
And the move is igniting quite a debate. 
Find out why some people strongly oppose the decision, today on The Point. 
And later, a discussion about how effective various masks are in preventing the spread of swine flu. 
Don't miss those conversations and more on The Point, today at 2 p.m. (CT 1 p.m., NT 2:30 p.m.) on CBC Radio One. 
3. IDEAS: 
It's considered the world's first novel, written in the 11th century by a 30-year-old Japanese woman. 
The Tale of Genji is a story of politics, intrigue, sexual conquests, frustrated love and jealousy. 
Today, it continues to be the subject of many paintings. And several great writers make reference to it in plays, poems and novels. 
Tonight on Ideas, a Vancouver documentary maker finds out why The Tale of Genji still fascinates people around the world. 
That's tonight on Ideas at 9 (9:30 NT) on CBC Radio One. 
-- RADIO TWO -- 
4. DRIVE: 
Today, award-winning, hit-making musician K-os is live on Drive! Fresh off the release of his album Yes!, he stops by the Drive studio to perform and chat with host Rich Terfry. 
Don't miss K-os, on Drive, this afternoon at 3 (3:30 NT) on CBC Radio Two. 
5. CANADA LIVE: 
Celebrate the musical sounds of Argentina, tonight on Canada Live. MusicFest Vancouver presents three ensembles that showcase some of the country's greatest tunes. 
Argentinean-born Ra=FAl Carnota is a singer, guitarist and composer with a passion for the music of his country. Tonight, he takes to the stage with a talented band for a crowd-pleasing performance. 
Also tonight, music from Cuartoelemento, a quartet of seasoned Argentinean musicians, based in Buenos Aires. Tune in as they draw on musical elements from various genres of world music- and add a touch of jazz! 
And later, Vancouver pianist Linda Lee Thomas joins Franco Luciani, one of Buenos Aires' most renowned musicians, for a special performance. 
Hear all three ensembles on Canada Live, tonight at 8 p.m. (8:30 NT) on CBC Radio Two. 

