


The dumb cluck would rather attack Americans than deal with law- breaking illegal immigrants bringing their diseases over the border. 
 <URL> / 
Government officials have declared a public health emergency in connection  with the swine flu outbreak that has killed dozens in Mexico and sickened  20 in the U.S., said the nations director of Homeland Security Sunday.  
Secretary Janet Napolitano also said border agents have been directed to  begin passive surveillance of travelers from affected countries, with  instructions to isolate anyone who appears actively ill with suspected  influenza.  
The number of cases confirmed in the United States by the federal Centers  for Disease Control and Prevention is now 20, including eight New York  City high school students. Other cases are in Ohio, California, Texas and  Kansas. Patients have ranged in age from 7 to 54.  
Government health officials expect to see more cases of swine flu here,  including possibly serious infections, a senior CDC official said.  
We expect there to be a broader spectrum of disease here in the U.S.,  said Dr. Anne Schuchat, interim deputy director for the agencys Science  and Public Health Program. I do fear that we will have deaths here.  
Napolitano said the emergency declaration is a warning, not a notice of  imminent danger, similar to preparing for a hurricane.  
"I wish we could call it a declaration of emergency preparedness,  Napolitano said.  
Dr. Richard Besser, acting director of the CDC, said that compared to  cases in Mexico, what were seeing in this country is mild disease,  noting that the U.S. cases would not have been detected without increased  surveillance.  
CDC officials said they dont yet have basic information about how the  virus spreads, including how many cases each primary case might create, or  how long it might take for them to be infected. However, agency officials  believe the virus is spreading person-to-person. In the U.S., all the  patients have recovered and only one patient was hospitalized.  
Besser said he still cant say why cases in U.S. are so much milder than  the deadly cases in Mexico. There, the disease has killed up to 86 people  and likely sickened nearly 1,400 since April 13.  
The real important take away is that we have an outbreak of a new  infectious disease that were addressing aggressively, Besser said.  
The incubation period for the virus is 24 to 48 hours, health officials  said. President Barack Obama recently traveled to Mexico but the  presidents health was never in any danger, said John Brennan, Assistant  to the President for Homeland Security.  
The president has received regular briefings from advisers on the swine  flu outbreak and the White House readied guidance for Americans.  
The government cant solve this alone; we need everybody to take some  responsibility, Napolitano said.  
Besser urged Americans to practice frequent handwashing and to stay home  if they feel sick. If your children are sick, have a fever and flu-like  illness, they shouldnt go to school.  
Schuchat said symptoms that would trigger alarm include h igh fever,  cough, sore throat, muscle aches, vomiting and diarrhea. But she cautioned  those could also be signs of any number of respiratory diseases.  
Theres not a perfect test right now that will let a person, a member of  the public or a doctor, know, she said.  
U.S. to screen travelers at borders The U.S. will begin screening travelers at the nations borders and  isolating people who are actively ill with suspected influenza, Napolitano  said. No travel restrictions are issued currently, but that could change,  she said.  
CDC officials said Sunday they would begin handing out yellow cards at  airports with information about signs, symptoms and ways to reduce the  chance of acquiring the virus.  
Health officials said the facts of the outbreak dont yet warrant testing  or quarantine of travelers from Mexico, but that that could change if the  situation gets worse.  
Anne Schuchat reiterated that the outbreak cant be contained.  
We cannot stop this at the border, she said, adding: But we think  theres a lot we can do to limit the impact on health and to slow  transmission.  
We think that slowing transmission can have an impact on health, she  said.  
Officials said Sunday they are considering whether to begin manufacture of  a vaccine.  
At this point, there is not a vaccine for this swine flu strain, Besser  said.  
Deaths in Mexico Symptoms in the eight newly confirmed cases in New York have been mild,  said Health Commissioner Thomas Frieden. City health officials said more  than 100 students at the St. Francis Preparatory School, in Queens,  recently began suffering a fever, sore throat and aches and pains. Some of  their relatives also have been ill. 
Some St. Francis students had recently traveled to Mexico, The New York  Times and New York Post reported Sunday.  
The World Health Organization chief said Saturday that the strain has  "pandemic potential," and it might be too late to contain a sudden  outbreak.  
Monitoring possible cases State infectious-diseases, epidemiology and disaster preparedness workers  have been dispatched to monitor and respond to possible cases of the flu.  Gov. David Paterson said 1,500 treatment courses of the antiviral Tamiflu  had been sent to New York City.  
The city health department has asked doctors to be extra vigilant and test  patients who have flu symptoms and have traveled recently to California,  Texas or Mexico.  
Investigators also were testing children who fell ill at a day care center  in the Bronx. Two families in Manhattan also have contacted the city,  saying they had recently returned ill from Mexico with flu symptoms,  Frieden said.  

Click for related content How concerned are you? Join the Newsvine discussion Newsvine poll: Are you changing your travel plans? 
Frieden said New Yorkers having trouble breathing due to an undiagnosed  respiratory illness should seek treatment but shouldn't become overly  alarmed. Medical facilities near St. Francis Prep have already been  flooded with people overreacting to the outbreak, he said.  
Kansas health officials said Saturday that they had confirmed swine flu in  a married couple living in the central part of the state after the husband  visited Mexico. The couple, who live in Dickinson County, weren't  hospitalized, and the state described their illnesses as mild.  
"Fortunately, the man and woman understand the gravity of the situation  and are very willing to isolate themselves," said Dr. Jason Eberhart- Phillips, the state health officer.  
No immunity Swine flu is a respiratory disease of pigs caused by type A flu viruses,  the CDC's Web site says. Human cases are uncommon but can occur in people  who are around pigs. It also can be spread from person to person.  
Health officials are concerned because people appear to have no immunity  to the virus, a combination of bird, swine and human influenzas. The virus  also presents itself like other swine flus, but none of the U.S. cases  appear to involve direct contact with pigs, Eberhart-Phillips said. 


