


< <URL> > 
As isolated outbreaks of swine flu continue to be confirmed around the world, with new cases reported Tuesday in Canada, Israel, France, New Zealand, Costa Rica and South Korea, Gov. Arnold Schwarzenegger proclaimed a state of emergency and the White House asked Congress for an additional $1.5 billion to fight the outbreak. 
State health officials have been aggressively working to combat the outbreak, and the proclamation is simply one more step in that effort, not an indication that the outbreak in California has become more severe. 
In the U.S. and elsewhere, officials are holding their breath to see whether this outbreak will turn into something more severe or, as many hope, simply peter out. Meanwhile, like Schwarzenegger, they are responding aggressively. 
President Obama, in a letter to Congress, asked for the $1.5 billion with "maximum flexibility to allow us to address this emerging situation." The letter said the money could go toward stockpiling antiviral medicine, vaccine development, disease monitoring and diagnosis, and assisting international efforts to limit its spread. 
"In our opinion, this is about prudent planning moving forward," White House Press Secretary Robert Gibbs told reporters. 
Also Tuesday, Homeland Security Secretary Janet Napolitano said she was forming a swine flu task force to coordinate U.S. efforts, and noted that the government had made 12 million doses of antiviral drugs available to states. She said her agency was resisting calls from Capitol Hill to screen inbound air travelers from Mexico and those crossing at border checkpoints. 
"Our focus is not on closing the border or conducting exit screening," she said. "It is on mitigation." 
The total number of confirmed swine flu cases in the United States had reached 68 as of late Tuesday afternoon and more than 100 worldwide, not counting the still-unknown number of cases in Mexico. At least some of the new cases appear to have come from human to human transmission outside Mexico. 
Such community transmission is one of the early earmarks of a pandemic, and if it continues to be observed, experts predicted, the World Health Organization is likely to raise its alert to Level 5, from the currently elevated Level 4. Such an increase might involve more travel restrictions and stronger efforts to control the spread of the virus. 
At a Tuesday morning news conference in Geneva, Dr. Keiji Fukuda, assistant director-general of the WHO, said a pandemic was not inevitable, but that if one did occur it was likely to be mild -- a conclusion drawn from the lack of deaths outside Mexico. 
But he cautioned that the 1918 Spanish flu pandemic, which killed millions worldwide, also started out mild. In the spring of that year, a mild pandemic petered out, only to return with a vengeance in the fall. 
------------------------------------------ 
Go to the provided link to read the rest of the atricle. 

