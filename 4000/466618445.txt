


On 3/7/09 1:34 AM, in article  <EMAILADDRESS> , "JTEM" wrote: 

Note the word "probably." But let's take as a given that a person can be infected by having oral sex (I'm guessing, only if they swallow). Do you suppose there's any difference in the quantity of semen ejaculated, compared to the minute quantity of OPV swallowed? How much virus does a person have to ingest in order to become infected. We don't know, but the answer is important to your theory. 

Yes, but quantity? For example, HIV is not thought to be transmitted by kissing, so the exchange of small amounts of saliva apparently isn't enough to infect someone. A dose of OPV is very small (although probably larger than the amount of saliva exchanged when kissing, unless someone is a *very* sloppy kisser). 

According to my research, the earliest *confirmed* case of AIDS occurred in 1959 (diagnosed later from a blood sample that had been retained) in an African bushman. No mention of his having been exposed to the CHAT vaccine (no mention that he wasn't, either). But the researchers *speculated* he had probably been infected in the late 1940s or early 1950s. 

The problem is, the OPV theory relies *entirely* on speculation. 

On the other hand... 

It's not wrong information; it's simply incomplete from the point-of-view of someone who believes in the oral vaccine theory. 



