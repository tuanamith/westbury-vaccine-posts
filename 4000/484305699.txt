


NaturalNews.com Originally published May 1 2009 
Timeline: World History of Viral Pandemics: 412BC to 2009 by Mike Adams, the Health Ranger, NaturalNews Editor 


1580 =96 First recorded influenza pandemic begins in Europe and spreads to Asia and Africa. 
1700s =96 Influenza pandemics in 1729-1730, 1732-1733, 1781-1782. 
1781 =96 Major epidemic causing high mortality among the elderly spreads across Russia from Asia. 
1830 =96 Major epidemic causing high mortality among the elderly spreads across Russia from Asia. 
1831, 1833-1834 =96 Influenza pandemics hit. 
1847-1848 =96 Influenza sweeps through the Mediterranean to southern France and then continues across in Western Europe. 
1889-1890 =96 The "Russian flu" spreads through Europe and reaches North America in 1890. 
1900 =96 Major epidemic. 
1924 =96 The first outbreak of HPAI avian influenza -- bird flu -- in the U.S. It does not spread among humans. 
1933 =96 Sir Christopher Andrewes, Wilson Smith and Sir Patrick Laidlaw isolate the first human influenza virus. 
1940 =96 Frank Macfarlane Burnet grows influenza on a laboratory growth system (embryonated chicken eggs). 
1955 =96 Sir Christopher Andrewes, along with Burnet and Bang, coins the term "myxovirus" for the influenza family. 
Mid-1970s =96 Researchers realize that enormous pools of influenza virus continuously circulate in wild birds. 
1977 =96 Mild Russian influenza epidemic occurs. 
1996 =96 HPAI H5N1 bird flu is isolated from a farmed goose in Guangdong, China. 


WHO assistance in responding to the outbreak is requested. WHO headquarters and the regional office in Manila are alerted. 



WHO alerts its partners in the Global Outbreak Alert and Response Network (GOARN). 




Sequencing of virus from one of the fatal cases in Vietnam reveals that all genes are of avian origin. 

Jan. 19, 2003 =96 A fifth fatal case of H5N1 infection is confirmed in Vietnam, also in Hanoi. 





Cambodia reports H5N1 in chickens in a farm near Phnom Penh. 

Jan. 25, 2003 =96 WHO staff and a GOARN international team, with support from Health Canada, arrive in Thailand. 
Jan. 27, 2003 =96 Thailand's third case, reported on Jan. 26, dies. Of the three cases, one remains alive. 

Vietnam reports its eighth case. The child has fully recovered and been discharged from hospital. 
Cambodia reports positive influenza A results from geese at a farm near Phnom Penh. 




Vietnam reports an additional three cases, one fatal, all in young adults. 
Authorities in Vietnam report that 52 of the country's 64 provinces have been affected by H5N1 in poultry. 

Feb. 4, 2003 =96 Chinese authorities report the spread of H5N1 infection in poultry to farms in two additional provinces. 

In Vientiane, Laos, 17 out of 18 farms (including one duck farm) test positive for the H5 subtype. 

Feb. 5, 2003 =96 Vietnam reports two further cases, both fatal, in young adults. 

Thailand confirms the country's fifth case. The patient, a child, died on Feb. 2. 
In Thailand, 40 of the country's 76 provinces have reported H5N1 disease in poultry. 


In Vietnam, 56 of the country's 64 provinces are now affected by H5N1 disease in poultry. 


OIE reports that half a million birds have been culled at nine farms in China where H5N1 infection has been confirmed. 

Feb. 9, 2003 =96 Vietnam reports three additional cases, two of which were fatal. 

The total number of cases in the two affected countries, Vietnam and Thailand, is now 23 cases, of which 18 were fatal. 




The number of farms in China with confirmed H5N1 outbreaks increases from 19 to 23. 

Feb. 12, 2003 =96 Thailand confirms its sixth case, a 13-year-old boy. 

Vietnam confirms its 19th case, which was fatal in a 19-year-old man who had been hospitalized in Ho Chi Minh City. 
The total number of confirmed cases in these two countries combined is 25, of which 19 have been fatal. 
The first clinical and epidemiological data on 10 cases in the Vietnam outbreak is made public by WHO. 

April 2003 =96 The Netherlands reports H7N7 bird flu in over 80 human cases with the death of one veterinarian. 
Mid-2003 =96 H5N1 bird flu spreads in Asia, but it is either undetected or unreported. 
Jan. 23, 2004 =96 Thailand reports human H5N1 bird flu infections. 
Feb. 20, 2004 =96 Thailand reports H5N1 infection of domestic cats in a single household. 
Oct. 11, 2004 =96 H5N1 infection spreads among tigers in a Thai zoo. 
Feb. 2, 2005 =96 Cambodia reports its first human case of H5N1 bird flu. It is fatal. 
July 21, 2005 =96 Indonesia reports its first human case of H5N1 bird flu. 
October 2005 =96 H5N1 is reported in poultry in Turkey and Romania and in wild birds in Greece and Croatia. 







China lifts the quarantine on the areas in northeastern Liaoning province that were affected by bird flu. 
India announces plans to create an emergency stockpile of one million doses of anti-flu drugs to combat the bird flu. 


The widespread sale of fake vaccines threatens to undermine China's plan to vaccinate 14 billion fowl. 




A Vietnamese doctor concludes that Tamiflu does not work after he unsuccessfully treats 41 H5N1 victims with the drug. 


The Ukrainian birds that were tested earlier in the month are confirmed to have the H5N1 strain of bird flu. 
A 41-year-old female factory worker, surnamed Zhou, is admitted to the hospital with symptoms of fever and pneumonia. 


China reports the 31st outbreak among birds in 2005. Dec. 22, 2005 -- Romania reports its 21st outbreak among poultry. 
Indonesia's number of human deaths related to bird flu rises to 11. 

Dec. 29, 2005 =96 China announces its seventh human case of bird flu and its third fatality. 
Jan. 3, 2006 =96 Bogus bird flu drugs begin to flood the internet. 

The boy's 15-year-old sister, Fatma Kocyigit, also tests positive for the H5N1 virus. 

Jan. 7, 2006 =96 Hulya Kocyigit becomes the third person in Turkey to die of the bird flu. 

The European Union bans the import of untreated feathers from six countries neighboring, or close to, Turkish borders. 
Another bird flu outbreak is reported in the Crimean peninsula. 


WHO reports two more bird-flu deaths in China. 

Jan. 13, 2006 -- The World Health Organization confirms Indonesia's 12th bird flu fatality. 


Jan. 14, 2006 -- A 13-year-old Indonesian girl dies of bird flu, bringing the country's bird flu death toll to 13. 

The girl's 5-year-old sister and 3-year-old brother are tested for bird flu, but results are inconclusive. 

Jan. 16, 2006 -- Tests show that Fatma Ozcan died from bird flu, making her Turkey's fourth death related to the illness. 

Turkey kills 764,000 fowl in an attempt to control the virus' spread. 
The WHO asks the Turkish Government for permission to track the virus' spread in humans. 

Jan. 17, 2006 -- The 3-year-old brother of the Indonesian girl who died on Jan. 14, dies. 
Jan. 18, 2006 -- Testing confirms that the Indonesian toddler who died on Jan. 17 had bird flu. 


Jan. 25, 2006 -- Bird flu kills a 29-year old woman Chinese woman, the seventh person to die from the disease in China. 
April, 2009 -- Suspected human-to-human transmission of bird flu reported in Egypt. WHO denies human-to-human transmission. 
April 27, 2009 -- Swine flu confirmed in U.S. schools. Now impacting California, Texas, New York, Kansas and other states. 






---------------------------------------------------------------------------= ----- 
 <URL>  

