


WHO chief Margaret Chan told member states at the opening of the UN  health body's annual assembly on Tuesday they may be facing a "calm  before the storm" but there was no reason to raise the alert level to  its maximum six. 
The current level of five indicates a pandemic is imminent, although  swine flu is now present in 40 nations, infecting nearly 8,300 people  and causing 74 deaths. 
Swine flu not yet a pandemic: WHO 
AAP May 19, 2009, 5:38 pm 
Authorities in Japan have stepped up measures to halt swine flu, which  the World Health Organisation (WHO) is not yet declaring a pandemic,  even as the disease claimed two more lives in Latin America. 
"We need to warn the public whenever necessary, but reassure them  whenever possible. This is a difficult balancing act," Chan told the WHO  meeting. 
Those WHO figures do not include two more deaths reported on Monday in  Mexico , epicentre of the influenza A(H1N1) virus outbreak but where the  disease is now waning. 
In Japan , authorities ordered more than 4,000 schools and kindergartens  to shut - double the previous day - to slow the spread of swine flu,  which has so far infected 178 people in the country. 
Many people in the affected urban areas were wearing face masks after  the western cities of Kobe and Osaka became the first in Japan to suffer  domestic outbreaks of the virus, which spread rapidly through two schools. 
The virus is believed to have spread after high schools from the two  cities played in a volleyball tournament, and some players and coaches  felt feverish afterwards. 
Experts warn the virus will likely soon spread to other regions,  including Tokyo, which with almost 36 million people is the world's most  populous urban area and the heart of the Japanese economy. 
"The virus's spread to Tokyo is near certain and it would be little  wonder if the virus had already landed in Tokyo undetected," said  Yukihiro Nishiyama, virologist at the medical department of Nagoya  University in central Japan . 
Apart from Mexico , swine flu has killed six people in the United States  - although the WHO lists four dead - and one each in Canada and Costa Rica. 
New swine flu cases have been reported in Chile, Greece, Hong Kong ,  Japan , Panama, Peru, South Korea and the United States. 
In New York, 12 schools have been temporarily closed following the death  of a 55-year-old school assistant principal. 
Separately, the US government reported on Monday more than 400 new  cases, to take the nation's total number of infections above 5,000. 
"We cannot stop the virus from spreading. We should not be surprised to  see more cases," New York Mayor Michael Bloomberg warned. 
Meanwhile, Canada ended a recommendation to postpone "non-essential"  travel to Mexico , saying the risk of contracting A(H1N1) had levelled off. 
In Geneva, Chan acknowledged scientific uncertainty surrounding the  virus and the need for more information. 
"We do not know how long this period will take, if this is the calm  before the storm," Chan told the assembly, adding that there was "every  reason to be concerned with the interaction with other viruses." 
Health authorities have said they expect the number of A(H1N1) flu cases  to rise in the southern hemisphere over the next months as the region  enters its autumn and winter seasons. 
The WHO also advised the pharmaceutical industry not to switch the focus  of its production onto the new swine flu virus, recommending that making  seasonal flu vaccines was still the priority. ----------------------------------------------------- Voices of Alarm and Moderation at WHO Meet By Gustavo Capdevila 
GENEVA, May 18 (IPS) - World Health Organisation (WHO) Director General  Margaret Chang issued a warning about the danger posed by the H1N1 flu  epidemic, while health ministers from several countries recommended  avoiding excesses when it came to remarks about a potential pandemic. 
"This virus may have given us a grace period, but we do not know how  long this grace period will last. No one can say whether this is just  the calm before the storm," Chang told health ministers and other  representatives of the WHOs 193 member states at the inauguration of  the 62nd session of the World Health Assembly in Geneva. 
Changs tone contrasted with the position taken by the ministers of  Brazil, the United Kingdom and Japan, who called for maintaining the  current level of influenza pandemic alert, phase 5, without passing to  the highest level, phase 6, which would mean a full-fledged worldwide  pandemic was under way. 
Josi Angel Csrdova, the health minister of Mexico, where the first cases  of the H1N1 influenza virus - popularly as swine flu - appeared in  mid-April, told journalists that he backed the call issued by those  three nations. 
The WHO alert level is based on how fast and widely a disease is  spreading. Phase 6 is characterised by sustained community level  outbreaks in at least two regions. 
So far, this kind of transmission has only clearly occurred in North  America. Other suspected cases of sustained community level outbreaks  have been ruled out, first in Britain and this week in Japan. 
"I will follow your instructions carefully, particularly concerning  criteria for a move to phase 6, in discharging my duties and  responsibilities to member states," said Chang. 
However, the WHO chief did not refrain from pointing to the potential  danger posed by influenza viruses, which she described as "the ultimate  moving target. Their behaviour is notoriously unpredictable. The  behaviour of pandemics is as unpredictable as the viruses that cause  them. No one can say how the present situation will evolve." 
She added that "We have every reason to be concerned about interactions  of the new H1N1 virus with other viruses that are currently circulating  in humans," such as the avian flu virus, which has spread in countries  of Asia and Africa since 2003, causing hundreds of deaths. 
Changs most emphatic warning was directed to the developing world,  "which has, by far, the largest pool of people at risk for severe and  fatal H1N1 infections," she said. 
Without ignoring the risks posed by the epidemic, representatives of  non-governmental organisations said the current global economic crisis  posed an even greater threat. 
Amit Sengupta, a health analyst from India, told IPS that "Im not  undermining the importance of a globally coordinated response to the  influenza pandemic as it really spreads." 
But what civil society is saying, he added, "is that by all accounts,  the global financial crisis is a much bigger threat." 
"We may be entirely wrong, but the evidence that we have before us  today, in terms of the possibility of the pandemic spreading and the  evidence that we have today of the financial crisis actually having  happened and escalating, would suggest that the response to the  financial crisis should at least match if not exceed the response to the  swine flu," said Sengupta. 
"And that is unfortunately something that we do not see. And unlike the  swine flu, and all influenza epidemics, which have a way of containing  themselves over two or three years, the last global financial crisis  that happened actually only contained itself by the Second World War,"  he noted. 
During the WHO assemblys first day of sessions, Mexico proposed the  discussion of "the possibility of creating an economic contingency fund,  supported by financial multilateral organisations such as the World Bank  and the International Monetary Fund. 
"The fund could be used to compensate those countries that notify in a  timely and responsible manner" on events of international relevance for  public health, "as a way to encourage transparency and international  cooperation in public health matters." 
Mexico, which reported a total of 3,646 laboratory-confirmed cases of  H1N1 flu, including 70 deaths, estimates the losses caused by the  outbreak there at around two billion dollars. 
One of the first decisions reached by the assembly was to shorten the  meeting to just five days, through Friday May 22. This was done "for a  good reason," said Chang, who stressed that "health officials are now  too important to be away from their home countries for more than a few  days." 
But Sengupta said the resolution was another show of alarmism. 
WHO authorities have come in for criticism for supposedly exaggerating  the dangers of the epidemic. (END/2009) 
------------------------------------ 
The archives of South News can be found at 
 <URL> / *********************************************************************************** 
Yahoo! Groups Links 
<*> To visit your group on the web, go to:      <URL> / 
<*> Your email settings:     Individual Email | Traditional 
<*> To change settings online go to:      <URL>      (Yahoo! ID required) 
<*> To change settings via email:      <EMAILADDRESS>        <EMAILADDRESS>  
<*> To unsubscribe from this group, send an email to:      <EMAILADDRESS>  
<*> Your use of Yahoo! Groups is subject to:      <URL> / 

