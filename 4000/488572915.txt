


Although there are theories that genetic mutation made the 1918 Spanish flu deadly but the large discrepancies in fatality rate in a short interval of time discount this theory. For example the averate fatality rate is only 10% but in some places, it reaches 80%. One example is among Eskimos at 80%. Other examples are workings living in workers' quaters. 
It is well know that it is the dose of micro organism that determine the severity of flu with the minimum estimate of 10 for those will little immunity. But even those with vaccinations and therefore have high immunity can still catch the flu if there are massive amount of micro organisms invading the host. 
The better theory would be the dosage of attacking virus which tends to be high in winter because of the lack of sunlight and cramped and enclosed living conditions. 
It this were the case, the number of swine flu cases, which is already higher than normal seasonal flu, will increase further in winter and more deaths may result despite the adminstration of Tamilflu that only acts to slow down the flu virus. 



1918 flu worse but slower than this year's 
BY AMANDA O'TOOLE 
Posted on Sunday, May 17, 2009 
Email this story | Printer-friendly version 
Arkansas Democrat-Gazette/WILLIAM MOORE Jessie Holyfield, 96, poses with a photo of the Kingston home she grew up in. The recent outbreak of the H1N1 virus brought back some tough memories for Holyfield, who still remembers the effects the 1918 flu pandemic had on her as a 6- year-old. 
ROGERS - Jessie Holyfield still remembers the distorted taste of foods and the pounding headache she had nearly 90 years ago when her body battled a killer flu. 
Holyfield was 6 years old in the winter of 1918, and she and her sister played with dolls during the days when the deadly Spanish flu forced her school to close and killed dozens in and around rural Kingston, where she lived, and thousands around Arkansas. 
Between 20 percent and 40 percent of the world's population became infected with this variant of the disease, according to estimates. The so-called Spanish flu killed about one in 20, or 5 percent, of those who got sick, its victims often dying days after symptoms appeared. 
To the world's relief, a new flu strain that surfaced last month in Mexico appears to be less severe than officials first thought. 
News of this spring's H1N1 virus, commonly called the swine flu, began to spread after health officials in Mexico reported a string of flu and pneumonia cases occurring after the usual mid-fall to early spring flu season. The World Health Organization, the directing and coordinating authority for health for the United Nations, said the virus had the potential to reach pandemic status, though it hasn't been elevated to that level. 
Worldwide, more than 7,500 cases of the virus have been confirmed in 34 countries, including about 4,300 in the United States and more than 2,400 in Mexico, according to the World Health Organization. Sixty-six people have died from the disease in Mexico and four related deaths have been reported in the United States. 
Five confirmed cases of the flu have been identified in Arkansas, including four military personnel in Pulaski County and an elementary school student in Lawrence County. 
"In retrospect, we obviously didn't have as much to worry about, but early on we didn't know that," said Dr. James Phillips, branch chief of the Arkansas Department of Health's infectious disease branch in Little Rock. 
Some of the deaths Mexico initially connected to the swine flu were wrongly attributed, he said. Overall, the deaths linked nationally to the swine flu have been fewer than in a traditional flu season. During the2008 flu season, which ran between October 2008 and April 2009, two people in Arkansas died from the flu. 
1918 FLU BORN IN KANSAS 
The Spanish flu appeared in Arkansas in the late spring of 1918 and returned with a fury in the fall. 
Scientists think the strain originated in Kansas and that it spread quickly throughout the country and abroad as military troops were mobilized for World War I. 
Arkansas obituaries published in 1918 were peppered with accounts of soldiers killed overseas by the enemy and were accompanied by tales of stateside deaths caused by the flu or the pneumonia-like symptoms that followed. 
Charlie Hubbs of Bentonville was 5 years old when he died in May 1918. 
Otto Miller of Pea Ridge died at Camp Pike, a military base near Little Rock, after just a week with the flu. 
Mrs. Elmer Ross in Garfield was 28 when she died of the "dreaded flu- pneumonia," leaving behind her four young children. 
In all, Arkansas recorded at least 7,000 deaths statewide, though that number is likely underreported, Kim Allen Scott wrote in an article published in 1988. He is a former employee of the Special Collections division of the University of Arkansas Libraries. 
Scott estimates that the death toll, if accurately documented, could be doubled. 
Now living in Montana, Scott remembers interviewing handfuls of flu survivors for his article "Plague on the Homefront: Arkansas and the Great Influenza Epidemic of 1918" and was surprised by the coolness of their recollections. The piece was published in the Arkansas Historical Quarterly. 
"It wasn't nearly as dramatic to them as it seems looking back on it. Death was a more familiar concept to young people back then," he said, noting people of the early1900 s likely had siblings or friends die from measles, other diseases or in accidents. "People back then were more used to knowing people who died when they were younger." 
Jessie Holyfield has an overfilled photo album that carries pictures of her from about the time the Spanish flu made its way to her town. 
In one photo, her hair is bobbed and her face is serious as she stands near her dog Kaiser Bill. 
She laughs as she talks about the hairdo that's unlike her gray curly locks now framing her face. 
She also keeps a framed picture of the old farmhouse that's still standing today. 
She uses her index finger on her right hand to point out areas of interest on the picture. 
Her candy apple-red fingernail moves to the home's bay window, where her mother nursed her back to health when she had the flu, and the covered front porch where she and her late sister Ruby Vaughn could spy funeral processions to the cemetery about a half-mile away. 
From there the girls would watch mule-drawn carts carry wooden caskets to the cemetery, often accompanied only by immediate family members. Other mourners were too scared they'd catch the disease, she said. 
"You heard the grown people talking, but you didn't understand what you heard," said Holyfield, who turns 97 next month. 
BELOW THE EQUATOR 
Health officials are now watching countries in the Southern Hemisphere closely during its flu season to see how the H1N1 virus continues to evolve and spread. 
What happens in those countries will be a good indication what the United States can expect in the fall, said Phillips of the state Health Department. 
Viruses don't remain the same, and their characteristics can change between spikes seen in the late spring and when flu season kicks up in the fall, he said. 
Normally, viruses are less virulent after the summer lull, Phillips said, but that was not the case in 1918. The Spanish flu increased its momentum, spreading across the world. 
Scientists monitoring H1N1 already have noticed changes in its genetic makeup, but there's no way of knowing how severe or mild it may be, Phillips said. 
The Southern Hemisphere's flu season just started and lasts through July. The timing makes decisions about how much and what type of vaccine to order north of the equator difficult because production requires so much lead time, he said. 
The World Health Organization is meeting next week with the federal Centers for Disease Control and Prevention and other interested medical parties to discuss the variables of a H1N1 vaccine, he said. 
They'll discuss whether to include the vaccine with seasonal flu shots or whether to make a vaccine at all, Phillips said. 
Companies that supply the U.S. with vaccines need about eight months before they can start shipping it because flu vaccine production is so complex. 
Production for the U.S. supply for regular winter flu vaccine already has begun. 
"One of the questions that is going to have to be answered - and won't be before we have some of the vaccine to test with - is how much vaccine is going to be required," Phillips said, explaining young people may need two rounds of the vaccine to be adequately protected. 
Arkansas is well equipped to inoculate people if necessary, he said. 
In 2008, the state vaccinated about 124,000 Arkansans through drive- through flu clinics. The state Health Department has plans to increase that number to 700,000 this year by giving shots to most school-aged children. 
JETS CUT SPREAD TIME 
An epidemic now would spread faster than the 1918 Spanish Flu, which took as long as 1920 to reach remote islands and rural communities around the world, Phillips said. 
"You can essentially go to one spot in the world to any other spot in the world within 24 hours," he said. "You can feel perfectly well when you got on the airplane and start shedding viruses, infecting people on the plane and where you landed." 
"That was not possible in 1918," Phillips said. "But one of the good things is we know a lot more about viruses and we are able to monitor and assess them and watch for some differences and how things are changing even in short period of time." 
Radio was in its infancy in 1918, and now cable and the Internet allow for news to spread instantaneously. 
During the early days of this spring's swine flu scare, doctors at the state Health Department had daily conference calls with representatives from each state's health department, the CDC and other medical facilities, Phillips said. 
Country doctors that treated rural Arkansas residents for the Spanish flu may have had little idea what they were up against. 
The doctor in Kingston rode horseback from house to house, eventually treating Holyfield and her sister, who caught the virus first. 
"The doctor came out, but he didn't know anything more than anyone else," said Holyfield, who caught the virus next. 
She said her mother went against the advice of the time to keep patients cool and covered her in blankets and placed her by a fire in the house's main level. 
"I still remember I had a terrible headache, and I couldn't eat because everything tasted horrible," Holyfield said. "The first thing that tasted good was pickled peaches. They were spicy." 
Her mother was next to catch the virus followed by her father. All survived the outbreak. 
A farm hand that lived on the family's property avoided their house while everyone was sick. 
"The truth of the matter is, you didn't know who was going to be next," she said. "There was room to be scared." 
With the recent outbreak of H1N1, Holyfield said she'll be tracking news of the swine flu in the coming months. 

