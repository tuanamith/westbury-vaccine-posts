


New Zealand's health minister said Sunday 10 students who just returned  from Mexico have tested positive for influenza. He said the cases are  "likely" to be swine flu. 
Tony Ryall said there was "no guarantee" the students had swine flu, but  that health officials were taking precautions. 
10 New Zealand students likely have swine flu 
By RAY LILLEY  6 minutes ago 
WELLINGTON, New Zealand (AP)  New Zealand's health minister said Sunday  10 students who just returned from Mexico have tested positive for  influenza. He said the cases are "likely" to be swine flu. 
Tony Ryall said there was "no guarantee" the students had swine flu, but  that health officials were taking precautions. 
At least 81 people have died from severe pneumonia caused by a flu-like  illness in Mexico, according to the World Health Organization, which  declared the virus a public health emergency of "pandemic potential." 
Ryall said that none of the patients were seriously ill and seemed to be  recovering. 
Thirteen high school students from a group of 25 students and teachers   who were quarantined and tested for swine influenza upon returning to  New Zealand early Saturday. 
"Ten students have tested positive for Influenza A, and these results  will now be sent to the World Health Organization laboratory in  Melbourne to ascertain whether it is the H1N1 swine influenza." 
H1N1 influenza is a subset of influenza A. 
The group from New Zealand's largest high school returned to the  northern city of Auckland on Saturday on a flight from Los Angeles.  Thirteen students and one teacher were unwell and one student had to be  hospitalized, said Auckland Regional Public Health Services director Dr.  Julia Peters. 
At this stage other passengers on the flight were not being sought and  the next step would depend on what the tests showed, said Health  Ministry spokesman Michael Flyger. 
Governments across the Asia-Pacific region were stepping up surveillance  for the deadly virus after Mexico closed schools, museums, libraries and  theaters in a bid to contain the outbreak. About 1,000 people may have  been sickened there. 
Some of those who died are confirmed to have a unique version of the  A/H1N1 flu virus that is a combination of bird, pig and human viruses,  WHO said. 
U.S. authorities said 11 people were infected with swine flu, and all  recovered or are recovering and at least two were hospitalized. 
"It would be prudent for health officials within countries to be alert  to outbreaks of influenza-like illness or pneumonia, especially if these  occur in months outside the usual peak influenza season," WHO  Director-General Margaret Chan said in Geneva on Saturday. 
Japan's biggest international airport stepped up health surveillance,  while the Philippines said it may quarantine passengers with fevers who  have been to Mexico. Health authorities in Thailand and Hong Kong said  they were closely monitoring the situation. 
China said anyone experiencing flu-like symptoms within two weeks of  arriving in the country from swine-flu affected territories was required  to report to authorities. 
Australia's Department of Health and Aging urged anyone who had returned  from Mexico with influenza-like symptoms since March to seek advice from  their doctors. 
Malaysia and other Asian nations said they were awaiting further advice  from WHO. 
At Tokyo's Narita airport  among the world's busiest with more than  96,000 people using it daily  officials installed a device at the  arrival gate for flights from Mexico to measure the temperatures of  passengers. 
A Health Ministry official said the government will monitor conditions  of people returning from Mexico with their consent. 
Agriculture Minister Shigeru Ishiba appeared on TV to calm consumers,  saying it was safe to eat pork. 
"Whether it's domestic or imported pork, pork is sanitized when being  shipped" to supermarkets, Ishiba told TV Asahi. "It's perfectly safe to  eat pork." 
Asia has grappled in recent years with the H5N1 bird flu virus, which  has killed at least 257 people worldwide since late 2003, according to  WHO. Nearly 45 percent of the global bird flu deaths have occurred in  Indonesia, with 115 fatalities. 
Scientists have warned for years about the potential for a pandemic  caused by viruses that mix genetic material from humans and animals. 
No vaccine specifically protects against swine flu, and it is unclear  how much protection current human flu vaccines might offer. 
Associated Press writers Shino Yuasa in Tokyo, Gillian Wong in Beijing,  Oliver Teves in Manila, Dikky Sinn in Hong Kong, Grant Peck in Bangkok,  Julia Zappei in Kuala Lumpur, and Kristen Gelineau in Sydney contributed  to this report. __________________________________________ 
Australia braces for possible 'global flu epidemic' 
ABC news 27 minutes ago 
The World Health Organisation (WHO) is warning that an outbreak of the  flu in Mexico and the United States could become a global epidemic. 
But the WHO says it is still too early to say whether this is likely to  happen. 
So far the 11 confirmed cases in the United States have all been mild,  and every person infected has recovered. 
In Mexico, at least 80 people are thought to have died from the human  form of swine flu. 
In a statement, the Department says that antiviral drugs useful in  treating swine flu are readily available through pharmacies and can be  prescribed by GPs. 
It says unlike bird flu, strains of swine influenza do not usually cause  illness in humans, and in developed countries like Australia, relatively  few people are regularly exposed to pigs. 
It says hospital emergency departments and GPs are being provided with  information on the outbreaks. The department also says it's preparing to  implement procedures at Australia's borders if necessary. 
It has urged anyone who has recently returned from the US or Mexico with  flu-like symptoms to see their GP. 
NZ students test positive 
Health authorities in New Zealand say 10 students who were among a group  returning from Mexico have tested positive for influenza-A, of which  swine flu is a sub-set. 
Health Minister Tony Ryall says the 10 were among 13 students from  Rangitoto College in Auckland tested for the virus after developing  flu-like symptoms. 
Three teachers and 22 senior students Auckland's largest high school  arrived back in the country on Saturday after a three-week trip to Mexico. 
The Auckland Regional Public Health Service says the situation is being  closely monitored. 
Reassurance 
Mexican President Felipe Calderon has announced emergency powers to  tackle the crisis. 
He has also sought to reassure people that the authorities have the  drugs necessary to treat the virus. 
"We are facing an epidemic of influenza, not due to the number of cases,  but because it is a new virus - something unknown, but which does have a  cure," he said. 
"It can be cured, it can be treated, and we have the necessary  medication in large supplies." 
The WHO's Margaret Chan says the outbreak is a public health emergency  of international concern. 
"Influenza viruses are notoriously unpredictable and full of surprises  as we are seeing right now," she said. 
"The viruses causing cases in some parts of Mexico and some parts of USA  are genetically the same. However we cannot say whether or not it will  indeed cause a pandemic." 
- ABC/BBC 
------------------------------------ 
The archives of South News can be found at 
 <URL> / *********************************************************************************** 
Yahoo! Groups Links 
<*> To visit your group on the web, go to:      <URL> / 
<*> Your email settings:     Individual Email | Traditional 
<*> To change settings online go to:      <URL>      (Yahoo! ID required) 
<*> To change settings via email:      <EMAILADDRESS>        <EMAILADDRESS>  
<*> To unsubscribe from this group, send an email to:      <EMAILADDRESS>  
<*> Your use of Yahoo! Groups is subject to:      <URL> / 

