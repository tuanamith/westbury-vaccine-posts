


By Tom Randall April 28 (Bloomberg) -- The World Health Organization, acknowledging the growing threat of swine flu, raised its global pandemic alert, saying the disease is no longer containable. 
The alarm level, increased to 4 from 3, is at its highest since the warning system was adopted in 2005, and the virus has been confirmed in the U.K., Mexico, the U.S., Canada, New Zealand and Spain. The emphasis worldwide should be on treating patients and strengthening preparations for outbreaks, said Keiji Fukuda, assistant director- general for health security and environment. 
As many as 152 people have died in Mexico from flu-related causes, and the number of worldwide cases of the virus confirmed by laboratory tests reached 73, officials said. Japan said today it will suspend visa-free entry for Mexican nationals, while Asian countries including Singapore and South Korea are screening air passengers. The Geneva- based WHO isn=92t recommending travel restrictions. 
The increased threat level =93signifies that we have taken a step closer=94 to a pandemic, Fukuda said on a conference call with reporters yesterday. =93It is also possible that as the situation evolves over the next few days we could move into Stage 5.=94 
The number of confirmed cases in Mexico is 26, WHO spokesman Gregory Hartl said on a conference call yesterday. Scientists are trying to determine why swine flu, a respiratory disease that=92s caused by a type- A influenza virus, has been more severe in Mexico. In the U.S., where the number of confirmed cases doubled yesterday to 40, only one person has required hospitalization, said Richard Besser, acting head of the U.S. Centers For Disease Control and Prevention. 
Level 6 
A pandemic, rated 6 on the WHO=92s 6-step alert system, is an unexpected outbreak of a new contagious disease that spreads from person to person across borders. 
Pandemics occur when a new influenza A-type virus, to which almost no one has natural immunity, emerges and spreads internationally. The World Bank, in a worst-case scenario published in October, said a flu pandemic that=92s similar in scope to the 1918 outbreak known as the Spanish flu could kill 71 million people worldwide and push the economy into a =93major global recession=94 costing more than $3 trillion. 
The raised level indicates health officials need to prepare for a pandemic, though it=92s not inevitable, Fukuda said. 
=91Very Fluid=92 
=93The situation is very fluid, very dynamic, and it is rapidly evolving,=94 said Tim Uyeki, an epidemiologist in the CDC=92s flu division. =93The cases in the U.S. don=92t have any links to contact with pigs. This appears to be ongoing human-to-human transmission.=94 
The New York cases appear to involve human-to-human spread, Hartl, the WHO spokesman, said at a briefing today in Geneva. 
Production of influenza vaccine for seasonal outbreaks, which U.S. health officials have said is ineffective against the new flu, should continue, Fukuda said. The WHO is working with companies to prepare for a swine-flu vaccine, and would help produce it if the outbreak becomes a pandemic, he said. 
The U.S. Food and Drug Administration signed emergency authorizations yesterday that will permit the CDC to use an unapproved lab test for swine flu and more dosing options than currently recommended for influenza treatments Tamiflu, sold by Swiss drugmaker Roche Holding AG, and Relenza, from London-based GlaxoSmithKline Plc. 
There are enough stockpiles of Tamiflu to meet current demand, said Roche spokesman Terence Hurley. Roche has the capacity to manufacture, over one year, enough courses of treatment for 400 million people, Hurley said by telephone. 
The WHO has told Roche that it appears Tamiflu would work against this strain of the virus, Hurley said yesterday. Glaxo has increased production of its antiviral Relenza and is in contact with the WHO and CDC, Glaxo spokeswoman Sarah Alspach said. 
Asia Reacts 
U.S. officials yesterday recommended that nonessential travel to Mexico be avoided and the European Union told travelers to avoid outbreak areas. 
Governments in Asia heightened their alert for the spread of the virus. Japan said it will suspend visa-free entry for Mexican nationals and advised its own citizens to defer trips to the Latin American nation. 
South Korea raised its national disaster level to yellow from blue, the lowest on its scale, after it found one suspected case of swine flu, the health ministry said today. 
Taiwan increased its travel alert for Mexico to red, the highest of three levels, meaning citizens are advised against travel to the country, Health Minister Yeh Ching-chuan said at a briefing broadcast on local television today. 
Cambodia is installing thermal scanners at its two international airports to identify passengers who show symptoms of swine flu, joining Australia, Japan, Malaysia, Singapore and South Korea among nations screening air travelers. 
Swine flu results in symptoms similar to seasonal influenza such as fever, lethargy and cough, and may also cause nausea, vomiting and diarrhea, according to the CDC. 
Pork Woes 
Swine-flu viruses aren=92t transmitted by food, and eating properly handled and cooked pork and pork products is safe, according to the CDC. There=92s no evidence the disease is spread by exposure to =93pork or pigs,=94 WHO=92s Fukuda said. 
Indonesia said today it will destroy all imported pork and swine products and fumigate agricultural goods bought from Canada, the U.S. and Mexico as a precaution. 
China, the world=92s top pork consumer, banned imports of swine products from Mexico and parts of the U.S. The Philippines also barred pork product imports from Canada, the U.S. and Mexico. 
No Cause for Alarm 
U.S. President Barack Obama said the emergence and spread of swine flu in the U.S. merits heightened concern =93but it=92s not a cause for alarm.=94 He declared a public emergency after 40 U.S. cases were confirmed in California, Kansas, New York, Ohio and Texas. New Jersey has identified five probable cases, the state=92s Department of Health and Senior Services said. All five cases are awaiting confirmation by the CDC, the department said. 
The U.S. Department of Health and Human Services issued an emergency declaration as a =93precautionary tool=94 to free resources to monitor and respond to the spread of the virus, Obama said yesterday. 
New York City has 28 confirmed cases of swine flu, all from St. Francis Preparatory School in Queens, Mayor Michael Bloomberg said yesterday at a news conference. All the cases were mild and as many as 100 may ultimately be found at the school, the mayor said. 
Mexico=92s Health Minister Jose Cordova said three more flu- related deaths occurred in state-run hospitals yesterday. As many as 149 people may have died in Mexico from the outbreak of swine flu as of April 26, although the cause of the deaths hasn=92t been confirmed. 
Global Cases 
Eight people in Canada contracted swine flu, said WHO Director-General Margaret Chan. New Zealand officials are monitoring 56 people, South Korea has a suspected case, the U.K. confirmed two people contracted the disease, and Spain confirmed one case. 
Separately, 10 students in New Zealand who had tested positive for influenza type A will likely be confirmed as having swine flu this week, a person familiar with the tests told Bloomberg News. 
Travel restrictions are unnecessary and based on political, not medical considerations, Chan said on a conference call with leaders from health groups around the world. 
=93By definition, pandemic influenza will move around the world,=94 Chan said on the call yesterday. =93Does that mean we are going to close every country? Does that mean we are going to bring the world=92s economy to a standstill? 
=93We know from past experience that transmission of influenza or the spread of new influenza disease would not be stopped by closing borders and would not be stopped by restricting movement of people or goods,=94 Chan said. 
The Mexican government requested that bars, movie theaters and churches be closed in Mexico City. It also extended its school closure to May 6 and may shut down more activities, Mexico=92s Cordova said. 
To contact the reporter on this story: Tom Randall in New York at  <EMAILADDRESS> . 
Last Updated: April 28, 2009 06:45 EDT 

