



 <URL>  
Swine Flu May Have Infected More Than 100,000 Americans  
        SUNDAY, May 17 (HealthDay News) -- While the official tally of confirmed U.S. swine flu cases topped 4,700 on Friday, experts at the Centers for Disease Control and Prevention now estimate the true number of infections at more than 100,000 nationwide. 
Also on Friday, health officials announced two new deaths linked to the H1N1 virus, bringing the nationwide total to five. 
"Today we had our fourth death reported from Maricopa County in Arizona," Dr. Daniel Jernigan, from CDC's Influenza Division, said during an afternoon teleconference Friday. "There are more deaths and hospitalizations that we are monitoring," he said. 
The Arizona patient, a woman in her late 40s with an underlying lung condition, died last week, the Associated Press reported. 
Also on Friday, health officials in Nueces County, Texas, announced that state's third swine flu-related death, an unidentified 33-year-old man with multiple underlying health conditions who died May 5 or 6, according to the AP. 
The two new fatalities come after two others in Texas and one in Washington state. All of the victims had underlying health problems besides the flu. 
Most cases of swine flu occurring in the United States appear to be mild, health officials said. 
In fact, "estimates of the confirmed and probable cases in the United States is probably not the best indicator of transmission at this point," the CDC's Jernigan said. "The outbreak is not localized, but is spreading and appears to be expanding throughout the United States. This is an ongoing public health threat." 
It's a little hard to estimate the number of people who may be infected with swine flu, Jernigan said, "but if we had to make an estimate, I would say that the amount of activity we are seeing with our influenza-like illness network is probably upwards of 100,000." 
Jernigan said there also seems to be more cases of flu generally in the United States -- both the seasonal and the new H1N1 swine flu -- than is usually seen at this time of the year. "There are 22 U.S. states that are reporting widespread or regional influenza activity, which is something that we would not expect at this time," he said. 
In other news, the CDC on Friday lifted its general warning that Americans avoid non-essential travel to Mexico, considered the origin and epicenter of the outbreak. Instead, the warning has been downgraded to a "precaution" that now advises people who might be at high risk for complications from the flu to reconsider travel to Mexico. 
But in a troubling sign that the swine flu outbreak has yet to run its current course in the United States, three New York City public schools were closed Thursday after dozens of flu-like infections surfaced and an assistant principal was in critical condition on a ventilator, according to published reports. 
New York City Mayor Michael Bloomberg said four students and the assistant principal at a Queens middle school had diagnosed cases of swine flu. More than 50 students went home sick Thursday with flu-like symptoms. At another middle school in Queens, more than 200 students were absent Thursday, and dozens more were sick at an elementary school, The New York Times and the AP reported. 
The assistant principal reportedly had underlying health problems before he fell ill. The students who have taken sick in this latest round of infections seem to be experiencing mild symptoms, similar to routine flu, as has been the case for most people in the United States touched by the swine flu. 
When the outbreak began more than three weeks ago, hundreds of students and staffers at St. Francis Preparatory School, also in Queens, were sickened. Reports at the time said several St. Francis students had spent spring vacation in Cancun, Mexico. Mexico is believed to be the source of the global outbreak that has now infected more than 7,000 people worldwide. 
Vaccine manufacturers and other health experts met Thursday at the World Health Organization headquarters in Geneva, Switzerland, to plot potential strategies to combat the swine flu virus. 
The AP reported that drug companies were ready to start producing a swine flu vaccine, but many questions remain. They include how many doses to produce, particularly in relation to needed doses of seasonal flu vaccine. 
The expert group's recommendations will be forwarded to the WHO's director-general, Margaret Chan, who is expected to issue advice to vaccine manufacturers and the World Health Assembly next week, the AP said. 
But at least one infectious-disease expert said it was a "foregone conclusion" that drug manufacturers would be told to proceed with a vaccine for the H1N1 flu. 
"If we don't invest in an H1N1 (swine flu) vaccine, then possibly we could have a reappearance of this virus in a mild, moderate, or catastrophic form and we would have absolutely nothing," said Dr. David Fedson, a vaccine expert and former professor of medicine at the University of Virginia. 
One factor complicating a decision is that most flu vaccine companies can only make limited amounts of both seasonal flu vaccine and pandemic vaccine, such as that needed for swine flu, and not at the same time. The producers also can't make large quantities of both types of vaccine because that would exceed manufacturing capacity, the AP said. 
Testing has found that the swine flu virus remains susceptible to two common antiviral drugs, Tamiflu and Relenza, according to the CDC. 
On Friday, the CDC was reporting 4,714 U.S. cases of swine flu in 47 states, and four deaths. For the most part, the infections continue to be mild -- similar to seasonal flu -- and recovery is fairly quick. 
The World Health Organization on Saturday was reporting 8,451 confirmed cases in 36 countries. 
The swine flu is a highly unusual mix of swine, bird and human flu viruses. Experts worry that, if the new flu virus mutates, people would have limited immunity to fight the infection. 
The CDC is concerned with what will happen as this new virus moves into the Southern Hemisphere, where the flu season is about to start. The agency is also preparing for the virus' likely return in the fall to the Northern Hemisphere. 

U.S. Human Cases of H1N1 Flu Infection (As of May 15, 2009, 11:00 AM ET)  
States # of confirmed and probable cases          Deaths  
Alabama 55    Arkansas 2    Arizona 435 1  California 504    Colorado 55    Connecticut 47    Delaware 60    Florida 68    Georgia 18    Hawaii 10    Idaho 5    Illinois 638    Indiana 71    Iowa 66    Kansas 30    Kentucky** 13    Louisiana 57    Maine 14    Maryland 28    Massachusetts 135    Michigan 142    Minnesota 36    Missouri 19    Montana 4    Nebraska 27    Nevada 26    New Hampshire 18    New Jersey 14    New Mexico 68    New York 242    North Carolina 12    North Dakota 2    Ohio 14    Oklahoma 26    Oregon 94    Pennsylvania 47    Rhode Island 8    South Carolina 36    South Dakota 4    Tennessee 74    Texas 506 2  Utah 91    Vermont 1    Virginia 21    Washington 246 1  Washington, D.C. 12    Wisconsin 613    TOTAL*(47) 4,714 cases 4 deaths  *includes the District of Columbia **One case is resident of Ky. but currently hospitalized in Ga. Source: U.S. Centers for Disease Control and Prevention      
More information 
For more on swine flu, visit the U.S. Centers for Disease Control and Prevention.  <URL> /   
  2009 ScoutNews, LLC.  2009 The Atlanta Journal-Constitution      

