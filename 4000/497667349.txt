


 <URL>  
Army Lab Finds 9,200 Uncounted Vials June 18, 2009 
HAGERSTOWN, Md. - An inventory of deadly germs and toxins at an Army biodefense lab in Frederick found more than 9,200 vials of material that was unaccounted for in laboratory records, Fort Detrick officials said Wednesday.  
The 13 percent overage mainly reflects stocks left behind in freezers by researchers who retired or left Fort Detrick since the biological warfare defense program was established there in 1943, said Col. Mark Kortepeter, deputy commander of the U.S. Army Medical Research Institute of Infectious Diseases.  
He said the found material included Korean War-era serum samples from patients with Korean hemorrhagic fever, a disease still of interest to researchers pursuing a vaccine. Other vials contained viruses and microbes responsible for Ebola, plague, anthrax, botulism and host of other ailments, Kortepeter said in a teleconference with reporters.  
"What happens over time, these get moved from one freezer to another, historically. Now we have much better tracking of where they are," Kortepeter said.  
About half of the found material has been destroyed, Kortepeter said. Samples deemed potentially useful were saved and entered into a laboratory database, he said.  
The material was in tiny, 1mm vials that could easily be overlooked in the 25-cubic-foot freezers or even covered by clumps of minus-80-degree ice, said Sam Edwin, the institute's inventory control officer.  
Kortepeter said the inventory found nothing missing from about 70,000 items the institute began cataloging in 2005. He said Army criminal investigators have concluded that three vials of Venezuelan equine encephalitis that were discovered missing last year "were likely used up but for some reason were never recorded with the database."  
The separate search of the institute's 335 freezers and refrigerators began Feb. 4 and ended May 27. Kortepeter said it was prompted by the discovery during a spot check in January of 20 samples of Venezuelan equine encephalitis in a box listed as containing 16.  
He said the review was ordered by the institute's commander, not by higher officials.  
However, the spot check that led to the inventory was part of a tightened security program the Army implemented at all five of its biological and chemical research centers in December in response to an FBI finding that a Fort Detrick scientist was responsible for deadly 2001 anthrax attacks, said Michael Brady, special assistant to Army Secretary Pete Geren.  
Brady said the other labs found no discrepancies, so they did not need inventory reviews.  
The new security measures also include an annual check to ensure that all material in the database is accounted for, Brady said.  
"This is not going to be a once-in-a-while thing," he said. "This is going to be a regular, aggressive inventory accountability program to make sure we don't have the problems we've seen in the past."  
After Fort Detrick anthrax researcher Bruce E. Ivins killed himself in July, the FBI announced that he alone was responsible for the anthrax mailings that killed five people and sickened 17 others in 2001. Ivins committed suicide after learning he would be charged in the attacks. His attorney maintains he was innocent.  


