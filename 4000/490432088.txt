


Since when did LULAC have a say in national security matters? (Rhetorical question).  LULAC has not whined that we are not at war with Iraq or Afghanistan and yet our National Guard personnel are stationed in those countries.  I don't ever recall an official declaration of war against any country where our troops are currently stationed.  LULAC is merely behaving as a Mexican Fifth Column front organization to further the Reconquista and should be charged with treason and sedition.  Hispanics, including the current nominee for the Supreme Court, have one agenda, i.e., the demographic advancement of Latin Americans as they flood into the United States and threaten elected officials with voting them out of office if they enforce our immigration laws or protect our border with Mexico. 
----- Original Message -----  <EMAILADDRESS>  Sent: Thursday, May 28, 2009 5:37:45 PM GMT -07:00 US/Canada Mountain Subject: Border debate 
Border debate  <URL>  1561887.txt Disagreement with Guard's possible return By JASON BUCH LAREDO MORNING TIMES Published: Thursday, May 21, 2009 1:07 AM CDT National Guard troops could be returning to the border, this time in an expanded role that includes "riding shotgun for Border Patrol," if the U.S. House has its way. 
"If we give them a little more training on this, I think we can do some more, have them play a more active role in law enforcement," said U.S. Rep. Henry Cuellar, D-Laredo. 
"That's what I'm looking at." 
Not everyone agrees with that plan. 
"We've always taken the stand that the National Guard should not be called in," said LULAC National President Rosa Rosales. 
"Why? Because you know first and most important we're not at war with Mexico, and the National Guard is not trained to be border agents." 
Last week, the House approved a supplemental appropriations bill that would provide funding for several border initiatives, including a controversial plan to put National Guard troops on the border to assist in counternarcotics efforts. 
The appropriations bill, which now goes to the Senate for approval, provides $350 million for the potential National Guard deployments, Cuellar said. 
The executive branch has not made the decision to send troops to the border, but if the Guard is deployed, some of the funding will be there, Cuellar said. 
Operation Jump Start, which put National Guard troops along the Texas-Mexico border from 2006-2008 in a support role for U.S. Border Patrol agents, garnered praise from the four border governors, who have asked the new administration for a similar program. 
During Operation Jump Start, the guardsmen deployed along the border were assigned office work and providing assistance at Border Patrol checkpoints in an effort to put more agents in the field. 
If they are deployed along the border again, Cuellar said, he doesn't want to see them relegated to answering phones. 
"With all due respect, we have men and women that have other skills," he said of the Guard. 
"We ought to use them in a more active way. 
I'm not talking about having them patrol the river in a military way, but to have them take more active role." 
Guardsmen could be involved in intelligence gathering or "riding shotgun" with Border Patrol agents, Cuellar said. 
But the possibility of having troops on the U.S. side of the border drew criticism from the League of United Latin American Citizens. 
Rosales, the LULAC national president, said the U.S. CBP Border Patrol has dramatically increased the number of agents along the Southwest border, and that combined with new technology along the border should be sufficient to combat issues like illegal immigration. 
Putting troops along the border would not be in the interest of the U.S. or Mexico, she said. 
Sending the Guard to the border was not the same as militarizing the border, Cuellar said. 
They would only be there to bolster law enforcement efforts, he said. 
There's a difference between law enforcement and a military mission," Cuellar said. 
"I'm certainly not interested in militarizing the border. 
We've heard this in past, and I think people are more comfortable when we get (the Guard) to provide assistance and support. 
Putting National Guard troops along the border would in now way suggest the situation on the U.S. side of the border has reached the level of lawlessness the Mexican side has, Cuellar said. 
Mexican President Felipe Calder=F3n has deployed troops across the country, including to border cities, to combat drug violence. 
Soldiers are regularly stationed at the international bridges in Nuevo Laredo. 
"First of all, any assistance that we can give our law enforcement on the border only sends a message that we are committed to help them in any way possible," Cuellar said. 
It has been almost three years since Laredo saw cartel-related killings. 
The supplemental appropriations bill, which Cuellar said would likely be the last of its kind, created to provide funds for the war in Iraq, would provide about $470 million for the Merida Initiative as well. 
The money will go toward providing equipment and intelligence for the Mexican government it its war against drug traffickers, Cuellar said. 
The money will go toward putting U.S. personnel in Mexico and will provide things like X-ray machines to be used at Mexico's southern border with Guatemala and Belize. 
"The more they do on the Mexican side, the less spillover the will be into the U.S." Cuellar said of the drug-related violence. 
The bill would also set aside $2 billion to stockpile antiviral drugs and develop vaccines in response to this year's H1N1 flu vaccine. 
The scare of H1N1, known as the swine flu, prompted some lawmakers to suggest closing the border with Mexico. 
(Jason Buch may be reached at 728-2547 or  <EMAILADDRESS> ) 

