


AT least Singaporean doctors don't pretend that Swine Flu is milder than common flu. 
But to assume that fatality rate is only 0.37% for Swine Flu compared to 0.1% for common flu is wrong. 
These figures are only case fatality rates, not true fatality rates. Common flu figure is for the whole year so it is close to the true fatality rate but for Swine flu, it is only 2 months 
 <URL>  
Singapore changes H1N1 strategy 
By: Dawn Tay 
SINGAPORE is changing its strategy in tackling the Influenza A (H1N1) virus, following the surge in number of infections here. 
The Health Ministry confirmed 26 new cases yesterday, bringing the total number here to 168. Nearly half of those who had caught the virus within the last few days were infected here. 

At a press conference yesterday, Health Minister Khaw Boon Wan said: "We have crossed the tipping point, beyond which local transmissions will grow rapidly." 
But life must go on, he said. 
The Asian Youth Games will continue, and schools will reopen next month as scheduled. Events like the National Day Parade and the F1 race will go ahead as planned. 
The change from the containment phase to the community- spread phase means a shift in strategy, from containing the virus to detecting and treating the large number of infections, in particular "high-risk" patients with existing health conditions. 
Mr Khaw said: "There will be some deaths. We need to allow our hospitals to focus on the high-risk cases and not be distracted or overwhelmed by hundreds of mild cases." 
He outlined several new measures that will be implemented: 
PUBLIC HOSPITALS WILL HANDLE H1N1 CASES 
993 ambulances have started taking suspected H1N1 cases to all public hospitals. 
PANDEMIC PREPAREDNESS 
CLINICS FOR MILD CASES To lighten the load on hospitals, H1N1 suspects with mild symptoms can visit Pandemic Preparedness Clinics, marked by decals with a red tick, at over 450 polyclinics and clinics run by general practitioners. Only high-risk suspected cases will be referred to public hospitals. 
SECURING VACCINE SUPPLIES Singapore is in negotiations with several vaccine manufacturers to acquire supplies of Influenza A vaccine. 
While the virus has been described by the World Health Organization (WHO) as posing a "moderate risk", its death rate in the United States, at 0.37 per cent, is almost four times higher than that for the normal flu. 
As people have little immunity against the new virus, WHO experts estimate that a third of a population could get infected, which means over one million Singaporeans are potentially at risk. 

