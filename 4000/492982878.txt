



"Mark Probert" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... On Jun 7, 2:23 pm, Happy Oyster < <EMAILADDRESS> > wrote: 
He went ahead anyway and published the results as if they were valid. 
This was well documented by sworn testimony of Dr. S. Bustin, et al, during the US Omnibus hearings. The PSC never could refute it. ============================ Exactly what I posted: 
 <URL>  
From: "Jan Drew" < <EMAILADDRESS> > Date: Sat, 6 Jun 2009 02:05:58 -0400 Local: Sat, Jun 6 2009 2:05 am Subject: Re: Diarrhea vaccine urged for all children 
Mark Probert" < <EMAILADDRESS> > wrote in message 
 <NEWSURL> ... 





Dr. Paul Offit, chief of infectious diseases at the Children's Hospital of Philadelphia and the co-inventor of one rotavirus vaccine manufactured by Merck & Co., said immunizing half the children in the United States has reduced the incidence of disease by 80 to 90 percent. 
Merck's RotaTeq and rival GlaxoSmithKline PLC's Rotarix are administered orally in the first six months after birth. 

 <URL> / 

Vol. 1, Issue 1 

Tayloe, Offit, Minshew, Katz, Snyderman, et. al.: Feeding a Hungry Lie 

By J.B. Handley 

(This post originally appeared at Age of Autism Blog on January 9, 2009) 





Stephen Greenspan, a psychologist and expert on gullibility, explains this recurrent experience of smart people falling for big, hungry lies as due to "the tendency of humans to model their actions -- especially when dealing with matters they don't fully understand -- on the behavior of other humans." 

So, some humans purportedly in the position to understand something say the lie, and repeat it over and over again and pretty soon a bunch of people who don't really understand it start saying the same thing. 

What's the big lie? Trust me; I really want to tell you, I've just struggled with which lie-teller to quote first. These days, there are so many people telling the lie I hardly know where to start. 

I thought long and hard about which lie-teller should get us going, and I've settled on Dr. David Tayloe. As the President-elect of the American Academy of Pediatrics, he's supposed to have our kids' back. Of course, he doesn't. And he's certainly not above telling the lie. 

I also picked David Tayloe because he said on Larry King Live that, in all his decades of being a pediatrician, he'd never reported an adverse event from a vaccine. Given how long he's practiced, and how many kids he's shot-up, it's a near impossibility that no child had an adverse event to a vaccine from his practice, so that makes Dr. David Tayloe somewhat or fully full of shit. So, he gets the nod. 

I've dragged this out so long, I almost don't want to drop it on you now, but here goes, here's Dr. David Tayloe, President-elect of the AAP, telling the big, big, very hungry lie: 

"Vaccines do not cause autism and we're not afraid of the truth." 

Dr. Tayloe may not be afraid of the truth, but he's certainly afraid to speak it. 

Lest you think I'm picking on Dr. Tayloe, which I certainly am, just know that he is well-supported by other humans who don't really understand the issue telling the same lie he's telling because they heard other people say it. Consider these luminaries of the hungry lie: 

Dr. Paul Offit: "It's been asked and answered: Vaccines don't cause autism." 

Amanda Peet, spokesperson for Sanofi Pasteur and Every Child By Two: "Fourteen studies have been conducted (both here in the US and abroad), and these tests are reproducible; no matter where they are administered, or who is funding them, the conclusion is the same: there is no association between autism and vaccines." 

Dr. Nancy Minshew, director of the University of Pittsburgh's Center for Excellence in Autism: "The weight of evidence is so great that I don't think that there is any room for debate. I think the issue is done. I'm doing this for all the families out there who don't have a child with autism, who have to deal with the issue of 'Do I get a vaccination or do I risk my child's life' because they don't understand what the science is saying." 

Dr. Michael Katz, senior vice president for Research and Global Programs for the March of Dimes: "The implication that vaccinations cause autism is irresponsible and counter productive." 

Dr. Renee Jenkins, current President of the AAP: "A television show that perpetuates the myth that vaccines cause autism is the height of reckless irresponsibility on the part of ABC." 

Dr. Nancy Snyderman, medical correspondent for NBC: "Sixteen separate studies have shown no causal association [between vaccines and autism]." 

The American Medical Association: "Scientific data overwhelmingly show that there is no connection between vaccines and autism." 

Whew. I'm tired just typing all those quotes. If that's not a sign that there is "consensus" on an issue...I don't know what is. 

What do we make of so many official people saying, and at times shouting, the same thing? The late Michael Crichton, himself an M.D., addressed this notion of a bunch of pedigreed people shouting the same lie, with a level of eloquence I could never summon: 

"I want to pause here and talk about this notion of consensus, and the rise of what has been called consensus science. I regard consensus science as an extremely pernicious development that ought to be stopped cold in its tracks. Historically, the claim of consensus has been the first refuge of scoundrels; it is a way to avoid debate by claiming that the matter is already settled. Whenever you hear the consensus of scientists agrees on something or other, reach for your wallet, because you're being had." 

Birth of the Lie: 2004 

We all know the year. We all know the organization. We all know the document. No document on earth has ever been more widely quoted, misquoted, represented, and misrepresented to prove, once and for all, that vaccines do not cause autism. Because this document is so damn important, and because we know exactly when the document was released, we can be very clear about exactly when this very hungry lie was born: 

May 17, 2004. 

Before I continue, I need to tell you something, and I really, really need you to listen. I'm going to quote a cliché, one that has been used many times, and is used so often that sometimes we may forget to reflect on its meaning, so please, take a moment and really think about this: The devil is in the details. Always. 

With that cliché now bouncing around in your head, let's look at the birth date of this very hungry lie when the Institute of Medicine released a document with the very official sounding name Immunization Safety Review: Vaccines and Autism. 

There it is. There's the very first lie. And, oddly enough, it's the title of the entire document. Stick with me on this, and remember the cliché I just mentioned to you. The title of the document is "Vaccines and Autism." To the average person, this would presume that the document explores the concept of whether or not vaccines cause autism. 

But, it doesn't. And, the study itself is far more honest than its own title. The study itself actually tells you what the IOM looked at and here it is, the IOM summary of what they actually looked at in the study they released on May 17, 2004: 

"In this report, the committee examines the hypothesis of whether the MMR vaccine and the use of vaccines containing the preservative thimerosal can cause autism." 

Wait a minute. The world thinks the IOM considered whether or not vaccines can cause autism. There are 11 separately licensed vaccines given to children (I'm counting DTaP and MMR as one each, even though they are triple shots), many given multiple times. The IOM looked at only one of these vaccines, the MMR, and an ingredient found in many others, Thimerosal. 

But, the hungry lie started that day, two months before my own son was diagnosed with autism. It started with a very odd and very contradictory piece of journalism by a writer from Reuters who seems to be more confused by the issue than most. This writer, Maggie Fox, reported on the IOM's document soon after its release, and her headline is clear enough: 

VACCINE DOES NOT CAUSE AUTISM, PANEL SAYS 

That's as clear and concise a version of the lie as you will ever see, although the use of the word "vaccine" rather than "vaccines" is odd. Within her own story, Ms. Fox goes on to explain what the IOM study actually did do, which contradicts her own headline: 

"Neither the measles, mumps and rubella vaccine nor a mercury-based preservative used in some childhood shots cause autism, a U.S. health panel has found." 

Hmm, that's weird. Even weirder is the quote she pulled out of Marie McCormick, the chairperson of the very IOM committee that issued the report that started the lie: 

"The weight of that evidence is pretty substantial," said Dr Marie McCormick, an expert in child and mother health at the Harvard School of Public Health who chaired the panel. "The overwhelming evidence from several well-designed studies indicates that childhood vaccines are not associated with autism," she added. 

Childhood vaccines are not associated with autism? That's quite a statement; particularly given the panel only contemplated two things: the MMR vaccine and an ingredient (mercury) in some vaccines. It's not just quite a statement; it's an unbelievably bold misrepresentation, which is a nice word for a lie. 

I have noticed this trend a lot lately, where health authorities in positions of influence seem to bounce back and forth between representing honestly what research has actually been done and making sweeping statement of false reassurance. Consider the curious case of Dr. Paul Offit, a henchman for Merck and vaccine patent-holder. In an article in the New England Journal of Medicine several years ago, Dr. Offit spelled out the research fairly clearly: 

"Fourteen epidemiological studies have shown that the risk of autism is the same whether children received the MMR vaccine or not, and five have shown that thimerosal-containing vaccines also do not cause autism." 

Today, why bother with the details? It's much easier for Offit to say, "It's been asked and answered: Vaccines don't cause autism" and be done with it. 

I wish I was done at this point, partly because all these details really wear me out, but the story actually gets a lot worse. 

Devilish Details 

I'm now going to make a point, and this is without a doubt the most important point I'm making today, so I hope you can once again take just a little bit closer of a listen. As we all know, the IOM looked at many different studies regarding both Thimerosal and the MMR vaccine. 

But, there is a point, and it's a point many of us think we know, but it's a point rarely discussed and a point so important that I think someday when they are piecing together how in the world the autism epidemic ever happened and how in the world such a big hungry lie was ever told for so long, I think this is the point they will make, so I'm going to make it first: 

There isn't a single study contemplated by the IOM, or cited by any medical authority whether CDC, AAP, WHO, IOM, or ECBT, that compares anything EXCEPT vaccinated children. 

How can that be? How can the IOM's document that tells the world that vaccines do not cause autism be resting on a foundation of studies that only ever looked at vaccinated children? 

We need some analogies here: 

That would be like looking at people who smoke one pack a day versus two packs a day and seeing no difference in lung cancer rates and saying cigarettes don't cause lung cancer. 

That would be like looking at people who eat chocolate chip cookies with chocolate chips and chocolate chip cookies without chocolate chips and seeing no difference in obesity rates and saying cookies don't contribute to obesity. 

That would be like looking at people who smoke low-tar cigarettes and people who smoke normal cigarettes and seeing no difference in lung cancer rates and saying cigarettes don't cause lung cancer... 

Do I need to continue? When I explained this trick to my 9 year-old, he got it, so I hope you do, too. If you can look at these studies and say that vaccines do not cause autism, well, I think you make Foghorn Leghorn look like Chickenhawk. 

Let's go back to Reuters for a second, because the article is terribly important, being the media's first brush with the lie and all. Let's look at what else Ms. Fox said back in 2004: 

"The panel, which included experts in paediatrics, family medicine, statistics and epidemiology, had reported in 2001 that there was no proven link between vaccines and autism but said there was not quite enough evidence to be definitive. Since then, they have reviewed five large epidemiological studies done in the U.S., the U.K., Denmark, and Sweden that found Children who were vaccinated with thimerosal-containing vaccines were no more likely to have autism than children who received thimerosal-free vaccines." 

Five large studies done? OK. And, these studies were the ones that turned the tide, right? That's certainly what this writer appears to have learned from the IOM. Just for fun, let's actually look at the "new" studies that were contemplated by the IOM. Not all five of them, but just for fun I'll pick two of the studies Ms. Fox is talking about, the one from the US and the one from the UK, published in 2003 and 2004, respectively. 

Before we look at these two studies, I need to make another point: the majority of studies that authorities point to as proof that vaccines do not cause autism have been published in a journal called Pediatrics. As Pediatrics will tell you, they are the official journal of the American Academy of Pediatrics. As we know, the AAP is a trade union for pediatricians with two unfortunate truths: 

- The AAP derives a majority of their outside contributions (estimated at more than $25 million per year) from pharmaceutical companies who make vaccines. 

- The very people the AAP represents, pediatricians, derive the majority of their annual revenues from the administration of vaccines to children. 

Do you think that's a coincidence? 

CDC Study, the one that just won't go away 

The first of the two studies I'll look at, the one that most people cite as the definitive work that vaccines do not cause autism, was published in Pediatrics in November 2003 and was written by the CDC by a lead researcher named Thomas Verstraeten. It's called Safety of Thimerosal Containing Vaccines: A Two-Phased Study of Computerized Health Maintenance Organization Databases. 

To say that much has been written about this study is like saying much has been written about Britney's love life -- it's always an understatement. So, I'm just going to make two points about this study, two points that will show you how big this lie has really become: 

1. The study's authors, after analyzing the only data ever run on American children (data that was later lost by the CDC), concluded that they couldn't prove anything either way. Their study was simply inconclusive. Not positive, not negative. Just neutral. After the press and vaccine talking heads tried to turn the study into the first evidence of the very big lie, the study's lead author, that same guy Verstraeten, wrote a desperate letter to Pediatrics because he was distraught at how his study--the one he was the lead author for--was being misused: 

"Surprisingly, however, the study is being interpreted now as negative [where 'negative' implies no association was shown between Thimerosal and autism] by many...The article does not state that we found evidence against an association, as a negative study would. It does state, on the contrary, that additional study is recommended, which is the conclusion to which a neutral study must come...A neutral study carries a very distinct message: the investigators could neither confirm nor exclude an association, and therefore more study is required." 

It's hard to imagine a second point actually worse than the point I just made, the point that one of the most famous studies routinely held up to support the position that "vaccines don't cause autism" actually reached no decision at all. But, it does get worse. 

2. Like every study the IOM considered in reaching their conclusion, and like every study ever cited by anyone defending the vaccine program, this study only looked at children who had been vaccinated. If that wasn't bad enough, the authors actually went a step further. Because there were so few children available who had received vaccines without Thimerosal, they actually compared children who had received MORE Thimerosal with children who received LESS Thimerosal to try and reach a conclusion. In point of fact, this "large-scale" study, as it's so often portrayed by the media, evaluated a total of exactly 223 children with autism, all of whom had been vaccinated, and over 80% of whom had received vaccines with 87 or more micrograms of mercury. 

Man, I'm really tired of talking about these details. If you think it's hard reading this lengthy piece, try writing it. In fact, it's the painful nature of these pesky details that makes the lie so easy to feed and perpetuate. Who really reads this shit? No one, I think. In fact, I have a little secret to tell you: I have actually read every single study the IOM based their conclusions on and every study the other side claims supports their case. I have them all sitting right next to me here in a tidy little folder. I may be the only human being on earth (except perhaps Bernadine Healy) who has actually done this. 

How am I so sure? I'm not, really. I just know it took me several hours and several hundred dollars to even get all the studies together in one place. You see, many of the actual studies are not freely available online for the average journalist to find, you have to buy them from the websites of the journals. Testing this hypothesis a step further, I asked a journalism friend of mine to ask Every Child By Two if they had copies of all the studies they cite on their website that prove vaccines don't cause autism. Surely they had copies and had read them all...surely? 

Here's what he heard back: 

Unfortunately we do not have copies of all of the studies available. I would suggest that if you check the main library at [your school]. They often get these journals even though your school doesn't have a medical or nursing program and you can copy what you need. Some may also be available online. 

Rich Rich Greenaway Director of Operations and Special Projects Every Child By Two 

Nope, even ECBT doesn't have the studies; they just speak authoritatively about the conclusions of studies they've never actually read... 

The "British" Study -- Am I on Neptune? 

Ok, I'll admit it -- up until a week ago, I'd never read the large-scale epidemiological study from the UK that clearly shows vaccines don't cause autism as was clearly stated in the Reuters article and clearly part of the IOM's very clear conclusion. I feel terrible, but I just never took the time to read it. 

When I finally read it, cover to cover, all 7 pages of this published study, I only had one conclusion: Am I living on fucking Neptune? 

You see, if you think the CDC study I just told you about is a shaky foundation for building a hungry lie, then the British study is the foundation of a thousand year old clay shack in the Sichuan Province during a 9.0 earthquake. 

Like all studies, the UK study has a very official sounding name, and one I will repeat so I can be as clear as possible: 

Thimerosal Exposure in Infants and Developmental Disorders: A Prospective Cohort Study in the United Kingdom Does Not Support a Causal Association 

Wow, that's an earful. Oddly, like few other studies I have seen, the title includes the conclusion of the study, albeit a deceitful representation of the actual conclusion. 

This UK study is such junk, it's really hard to write about. I imagine a journalist trying to read this study for the first time, and probably struggling with what the hell was actually done, sort of like a monthly statement from Bernie Madoff, and falling back to just reading the title again and hoping for the best. Yet, after reading the study enough times, I was able to actually figure out what had been done, and I'll start by quoting you from the authors' own summary of what they did, from the Methods section of the study on page 577 of Pediatrics in September 2004: 

"The study has been monitoring >14 000 Children who are from the geographic area formerly known as Avon, United Kingdom, and were delivered in 1991-1992. The age at which doses of thimerosal-containing vaccines were administered was recorded, and measures of mercury exposure by 3, 4, and 6 months of age were calculated and compared with a number of measures of Childhood cognitive and behavioral development covering the period from 6 to 91 months of age." 

OK, I know. You have no idea what that means, I certainly didn't. So, I will use the kind of English most of us can understand so you can see how amazingly unbelievable this study really is: 

- 100% of the children in this study were vaccinated 

- 100% of the children in this study were vaccinated with the thimerosal-containing DTP vaccine 

- If you were a child who hadn't completed the full series of thimerosal-containing DTP shots, which in Britain is 3 doses, you weren't even in the study 

- The only variable considered, and I'm going to put ONLY in all-caps so you really hear me, the ONLY variable considered was the TIMING of the 3 doses of thimerosal-containing DTP vaccines given to kids 

- And, when I say timing, what I mean is they compared kids who had gotten these shots by 3 months, 4 months, and 6 months 

That's it. 

The timing of thimerosal-containing shots was explored. The authors are actually honest about this in their own conclusion to the study: 

"This study, based on a large United Kingdom -- based prospective cohort, shows no evidence of any harmful effect of an accelerated immunization schedule with thimerosal-containing vaccines." 

There it is, clear as day: an accelerated schedule of TCVs. TIMING is the only variable this study considered. 

One side point, for those of you who noticed. The IOM study came out in May 2004. This study was published in Pediatrics in September 2004, four months later. What gives? What gives is that the AAP did an excellent job of getting a crap study in front of their friends at the IOM to give them more ammunition to birth the hungry lie. 

A "Dose" of Reality 

It's hard to write this piece, because it makes me lose even greater faith in our health authorities. I think about a guy like Dr. David Tayloe and I just want to know what's actually true: 

- Is he the gullible guy who just believes what others say and repeats it? 

- Is he so stupid that he's read all the science and believes it proves that vaccines don't cause autism? 

- Does he know the science doesn't remotely say that, but thinks it's better to say so anyway and protect the vaccine program? 

I don't know, but any of those reasons pretty much suck. 

I mention above about giving a dose of reality, so I'm going to. But first, a question: 

What is the purpose of science, and more specifically, of medical research? 

I think the purpose of science and medical research is the betterment of the human race. To help us live longer, healthier, happier lies. To answer all the tough questions about what's good for us and what's bad for us. The customer of medical research is mankind. 

If mankind is the customer here, I would make another argument. Mankind's most important members are babies and children. Agree? And, nothing is more painful for mankind or more detrimental to mankind's life, liberty, and happiness than a sick child. Ask any family. 

So, here's some reality for you: 

Terry has a daughter named Hannah. Through eighteen months, Hannah's pediatrician notes she is meeting all developmental milestones -- a normal developing child. At nineteen months, Hannah gets taken to the pediatrician, presumably by her Mom, and she gets five shots in one visit: DTaP, Hib, MMR, Varivax, and IPV. A five shot visit? In the U.S., this happens thousands of times a day. 

Suddenly, things for Hannah change. I'll let the now-famous court document tell the story from here: 

"According to her mother's affidavit, Hannah developed a fever of 102.3 degrees two days after her immunizations and was lethargic, irritable, and cried for long periods of time. She exhibited intermittent, high-pitched screaming and a decreased response to stimuli. Terry spoke with the pediatrician, who told her that Hannah was having a normal reaction to her immunizations. According to Hannah's mother, this behavior continued over the next ten days, and Hannah also began to arch her back when she cried. 

On July 31, 2000, Hannah presented to the Pediatric Center with a 101-102 degree temperature, a diminished appetite, and small red dots on her chest. The nurse practitioner recorded that Hannah was extremely irritable and inconsolable. She was diagnosed with a post-varicella vaccination rash. 

Two months later, on September 26, 2000, Hannah returned to the Pediatric Center with a temperature of 102 degrees, diarrhea, nasal discharge, a reduced appetite, and pulling at her left ear. Two days later, on September 28, 2000, Hannah was again seen at the Pediatric Center because her diarrhea continued, she was congested, and her mother reported that Hannah was crying during urination. On November 1, 2000, Hannah received bilateral PE tubes. On November 13, 2000, a physician at ENT Associates noted that Hannah was "obviously hearing better" and her audiogram was normal. On November 27, 2000, Hannah was seen at the Pediatric Center with complaints of diarrhea, vomiting, diminished energy, fever, and a rash on her cheek. At a follow-up visit, on December 14, 2000, the doctor noted that Hannah had a possible speech delay. 

Hannah was evaluated at the Howard County Infants and Toddlers Program, on November 17, 2000, and November 28, 2000, due to concerns about her language development. The assessment team observed deficits in Hannah's communication and social development. Hannah's mother reported that Hannah had become less responsive to verbal direction in the previous four months and had lost some language skills. 

On December 21, 2000, Hannah returned to ENT Associates because of an obstruction in her right ear and fussiness. Dr. Grace Matesic identified a middle ear effusion and recorded that Hannah was having some balance issues and not progressing with her speech. On December 27, 2000, Hannah visited ENT Associates, where Dr. Grace Matesic observed that Hannah's left PE tube was obstructed with crust. The tube was replaced on January 17, 2001. 

Dr. Andrew Zimmerman, a pediatric neurologist, evaluated Hannah at the Kennedy Krieger Children's Hospital Neurology Clinic ("Krieger Institute"), on February 8, 2001. Dr. Zimmerman reported that after Hannah's immunizations of July 19, 2000, an "encephalopathy progressed to persistent loss of previously acquired language, eye contact, and relatedness." He noted a disruption in Hannah's sleep patterns, persistent screaming and arching, the development of pica to foreign objects, and loose stools. Dr. Zimmerman observed that Hannah watched the fluorescent lights repeatedly during the examination and would not make eye contact. He diagnosed Hannah with "regressive encephalopathy with features consistent with an autistic spectrum disorder, following normal development." Dr. Zimmerman ordered genetic testing, a magnetic resonance imaging test ("MRI"), and an electroencephalogram ("EEG"). 

Did you read that whole excerpt? Did you really read it? If you did, and if you are human, it rips your heart out. If you are the parent of a child with autism as I am, it more than rips your heart out, it causes you to die all over again. And, as we both know, Hannah's story is far from unique. In the autism world, it's the norm. I'd hazard to guess that Hannah's story is shared by several hundred thousand families in the US alone. 

How does the experience above benefit mankind? 

Can science help us out of this mess? 

The Failure of Science and the Big Lie 

As we all know, Hannah's Mom is a nurse and her Dad is a neurologist. I've never met the Polings nor have I ever talked to them. But, I know that Jon, Hannah's dad, was very much part of the mainstream medical establishment before seeing what happened to his daughter, as he himself has said. 

I have no doubt that as Dr. Poling was watching these events unfold with his daughter that he was looking for answers through science and from the people and journals he trusted. And, I have little doubt that he found nothing. 

Let me ask you a simple question, and I particularly want to ask it of the liars like David Tayloe, Paul Offit, Nancy Snyderman, and others who falsely reassure parents every day that everything is OK when everything is not OK. Please, a simple question: 

Can you show me the science that would convince the Polings that it wasn't the vaccines? 

Please. Show me. She got five vaccines in one day. She was never the same. Show me the science where you can proudly stand up and say, "Vaccines do not cause autism, I'm sorry about what happened to your daughter but it wasn't the vaccines, please read this." Is it the British study, the one that made me think I was on Neptune? Or, is it that one from CDC, the one where they were unable to determine anything? Which one should the Polings look at so they can move off of vaccines as a likely culprit to their daughter's regressive autism? 

The lie needs to end. Those who have been telling the lie need to be called out. They need to be removed. Every day, another parent is falsely reassured because they listen to someone they think they can trust who is feeding the hungry lie. 

It makes me so damn mad to write this piece, perhaps that's why it's so long, but really, I don't even know how to end it. The more I look at the details, the madder I get. Since I can't trust myself to end this piece in a thoughtful way, I will bring in Dr. Bernadine Healy, the former Director of the National Institutes of Health, herself an M.D. from Harvard and someone who directed the nation's largest organization dedicated to medical research. 

Dr. Healy fits into this story because it was Hannah Poling's case that caused her to take a closer look at the controversy. Unlike many of the feeders of the lie, I have no doubt that Dr. Healy actually has read the science that many of her colleagues claim shows vaccines don't cause autism, and Dr. Healy didn't like what she learned at all. So, I'm going to finish with a quote from her, but not until I get one final request in here: 

If you are reading this, and you can do something about all these liars who falsely reassure parents every day, please do. Thank you. 

Here's Bernadine Healy, talking to CBS Evening News: 

"We have to take another look at that hypothesis, not deny it. I think we have the tools today that we didn't have 10 years ago, 20 yrs ago, to try and tease that out and find out if there is a susceptible group...A susceptible group does not mean that vaccines are not good. What a susceptible group will tell us is that maybe there is a group of individual who shouldn't have a particular vaccine or shouldn't have vaccines on the same schedule...I don't believe that if we identify the susceptibility group, if we identify a particular risk factor for vaccines or if we found out that maybe they should be spread out a little longer, I do not believe that the public would lose faith in vaccines... 

I think that the government or certain public officials in the government have been too quick to dismiss the concerns of these families without studying the population that got sick...I haven't seen major studies that focus on 300 kids who got autistic symptoms within a period of a few weeks of a vaccine...I think public health officials have been too quick to dismiss the hypothesis as irrational without sufficient studies of causation...I think they have been too quick to dismiss studies in the animal laboratory either in mice, in primates, that do show some concerns with regard to certain vaccines and also to the mercury preservative in vaccines...The reason why they didn't want to look for those susceptibility groups was because they were afraid that if they found them, however big or small they were, that that would scare the public...I don't think you should ever turn your back on any scientific hypothesis because you're afraid of what it might show... 

Populations do not test causality, they test associations. You have to go into the laboratory and you have to do designed research studies in animals...The fact that there is concern that you don't want to know that susceptible group is a real disappointment to me. You can save those children...The more you delve into it, if you look at the basic science, if you look at the research that's been done on animals. 

If you also look at some of these individual cases and if you look at the evidence that there is no link what I come away with is the question has not been answered." 

J.B. Handley is co-founder of Generation Rescue and a contributor to Age of Autism. 

 <URL>  

 <URL> ... 

 <URL>  

 <URL>  

 <URL>  

 <URL>  
From: "Jan Drew" < <EMAILADDRESS> > Date: Sat, 6 Jun 2009 21:37:29 -0400 Local: Sat, Jun 6 2009 9:37 pm Subject: Re: Diarrhea vaccine urged for all children 
Citizen Jimserac" < <EMAILADDRESS> > wrote in message 
 <NEWSURL> ... On Jun 6, 2:05 am, "Jan Drew" < <EMAILADDRESS> > wrote: 



Thank you Jan for re-posting. 
I see that one of the MAJOR studies relied on by the "no connection with the vaccines" crowd, was only a cohort study rather than a DOUBLE BLINDED PLACEBO CONTROLLED study which the anti-alternative medicine bunch is always shouting for in Homeopathy. 

Next, I notice that the vast majority of these studies appears to be entirely on VACCINATED populations rather than on a comparison of autism rates from vaccinated and unvaccinated populations. 

I have not yet come to a conclusive opinion on this issue but continue to apply the SAME LEVEL OF SKEPTICISICM and standards applied by the anti-alternative medicine fanatics to their own favorite therapies. 

Most damning of all, again and again I see appeals to AUTHORITY, and soothing reassurances from people in positions of power in standard medicine, INSTEAD OF definitive studies and comparisons, both long and short term. 

Those who blindly support vaccination WITHOUT INVESTIGATION are engaged in propaganda and character assassination, for example against Dr. Wakefield, rather than in science itself - this is a sure indication that we are dealing with an attempt by autism-vaccine connection denialists to gain control of the health care system, exclude competition, discredit opposing viewpoints and resort to innuendo, appeals to authority and attacks against dissenters. 

What is their ultimate goal ?   CONTROL. 

What is their ultimate fear ?   DISSENT and EXPOSURE 

and FREEDOM OF CHOICE. 

Well done, Jan! 

Citizen Jimserac 














Thank you. =============================== So, that makes the both of you liars. 




