


Jun 8, 2009 8:36 
'Hamas risking Operation Cast Lead II' 
By JPOST.COM STAFF AND YAAKOV LAPPIN 
If Hamas continues to assist terrorist groups with attacks against IDF soldiers, it will risk facing "Operation Cast Lead II," a top IDF commander warned on Monday. 
Four terrorists killed in firefight with IDF on Gaza border 
Lt.-Col. Avinoam Stolevitch's comments came after a group of around 10 Palestinian gunmen armed with "huge amounts of explosives" launched a failed Gaza border assault at the Karni Crossing. 
"We are slowly beginning to understand the magnitude of [the threat from the Gaza Strip]," Stolevitch told Army Radio, adding his evaluation that the terrorists had planned a "large explosion to provide cover for a kidnapping" in Monday morning's attack. 
A security source told The Jerusalem Post that the terror cell used the cover of morning fog for their attempt, as well as booby-trapped horses. At least four terrorists were killed in the ensuing exchange of fire with the IDF. No Israeli soldiers were wounded in the incident. 
The terror cell belonged to the Janud Ansar Allah (Soldiers Loyal to Allah) organization, a small group which is linked to Iran and Hizbullah, the security source added. 
Members of the cell, some of whom had suicide bomb belts strapped around their bodies, led the horses from trucks and began planting explosive devices along the fence. They were identified by IDF soldiers on patrol, of Golani's 13th Battalion. The gunmen proceeded to open fire on the troops, while mortar fire from deep within the Gaza Strip was also directed at the soldiers. 
Soldiers returned fire, and called for backup. At first, tanks were dispatched to the scene, and fired on the terror cell. Air Force combat helicopters then joined the fight, also firing on terrorist targets from above. 
"A very big terror attack was thwarted," the security source said. "These terrorists were armed with a huge quantity of explosives. They launched a combined attack, using mortars, and attempted to approach the border fence with booby-trapped horses to harm our soldiers, before firing on our force." 
"Hamas did not carry out this attack but they certainly provide general coverage for these small groups," the source continued, adding that it was too soon to know whether the cell had planned to kidnap soldiers. 
"The area turned into a war zone," the source said. 
"Southern Command forces are prepared for these types of attacks, and are aware of the dangers present in the morning fog. There is always the chance terrorists will try to use it for an attack," he added. 
Defense Minister Ehud Barak on Monday afternoon praised the army's "effectiveness" in foiling the attack, and said it was quite possible that one of the aims of the assault was to kidnap an IDF soldier, a claim made by Hamas television. 
"The results speak for themselves, and prove the preparedness and the alertness of our forces along the Gaza border," Barak told a Labor faction meeting. "I hope that all future operations end with the same type of result." 
Ismail Haniyeh, who heads Gaza's Hamas government, praised the attackers as "martyrs," and said the violence confirmed Israel's "aggressive intentions" toward the Palestinians. 
Following the attack, Israel closed the Karni crossing, the main commercial terminal between Israel and Gaza, as well as the Nahal Oz fuel depot. 
However, 30,000 vaccine units against foot-and-mouth disease were transferred to Gaza via the Erez crossing, despite the thwarted attack. The IDF said that 125,000 units had been supplied to the Strip in the last three months in three separate transfers, due to the importance of preventing the outbreak of the disease. 
In addition, 140 truckloads of humanitarian aid was scheduled to be transferred via the Kerem Shalom crossing. 
AP contributed to this report. 


