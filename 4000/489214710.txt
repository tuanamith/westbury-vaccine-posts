


 <URL>  
CDC Prepares for Swine Flu Surge in Fall Swine Flu Still Hot in Northeast but Easing in Most of U.S. 
May 26, 2009 - As the U.S. swine flu epidemic eases -- except in the Northeast -- the CDC is shifting its focus to preparing for a surge of cases when flu season returns in the fall. 
The count of people who have been infected with the H1N1 swine flu continues to rise. Today's official count is 6,764 cases -- and that number should rise considerably as many state and local health departments did not report new cases over the Memorial Day holiday. 
There have been 10 official U.S. swine flu deaths and two new deaths likely caused by the new flu. More than 300 people in the U.S. have been hospitalized, over half of them previously healthy young people. 
While swine flu remains widespread in the Northeast, the epidemic seems to be waning in the rest of the country. Nationwide, no more people are seeing doctors for flu-like illness than is usual for this time of year -- a key indicator of flu activity. 
It's too soon to say that swine flu has peaked in the U.S., Anne Schuchat, MD, the CDC's interim deputy director for science and health, said at a news conference. While hot spots of swine flu are expected to pop up across the nation throughout the summer, she said, "we are thinking that the warm summer months ahead may give us a little respite." 
Accordingly, the CDC is shifting gears. 
"Now we are at a transition point where we are entering an area of new focus and new priorities," Schuchat said. "We really are on a fast track for the next eight to 10 weeks to learn as much as we can ... and to strengthen our planning for the surge of illness we expect to see here in the fall." 
What will happen over the next two months is that the nation's health experts face a series of decisions about making a swine flu vaccine. 
One decision already has been made: Seasonal flu vaccine is being made now. Soon, vaccine makers will be ready to switch to making a swine flu vaccine. Here are the questions that must be answered: 
         Should we really make a swine flu vaccine? 
         Should we base a vaccine on the current virus? Flu viruses change rapidly. Vaccine against the current virus might be far less effective against a changed virus. 
         Should we wait to see if the virus changes? If vaccine production doesn't start soon, swine flu vaccine won't be ready when it's needed. 
         Will people need one shot of vaccine, or two? 
         Will immune-stimulating adjuvants make the vaccine more potent -- or lead to an unacceptably high level of side effects? 
         As vaccine becomes available, who should get the vaccine first? Who should be sent to the end of the line? 
Only one thing is for sure: There won't be enough information for a definitive answer to these questions. But Schuchat says the CDC is working overtime to come up with as much information as it can to make these decisions as informed as possible. 
"We do try to separate the questions about the initial steps in vaccine development, later steps in vaccine production, and further steps that might involve the decision to vaccinate some or all of the population," Schuchat said. "There needs to be an evidence-based and careful deliberation for each of these steps. We don't intend to make a decision about immunization until as late as possible." 
Key data will come from what swine flu does in the Southern Hemisphere this summer, as that part of the world enters its flu season. 
Meanwhile, the World Health Organization (WHO) today reaffirmed its decision not to declare a world flu pandemic until swine flu clearly becomes as severe in other parts of the world as it has been in the U.S. and Mexico. 
The WHO may decide to raise its caution level in ways less likely to sow the seeds of panic than a pandemic alert, Keiji Fukuda, MD, WHO interim assistant director-general for health security and environment, said today at a news conference. 
"We have no specific plans to raise the alert in one region or another," Fukuda said. "But if the disease got significantly worse in one country, we would get that information out to other countries very quickly independent of 'phases' or 'alerts,'" he said. 

