


Australia upped its pandemic threat level, invoking sweeping powers  allowing for the closure of schools, public places and major events as  the number of confirmed swine flu cases reached 14. 
Federal Health Minister Nicola Roxon says she is concerned authorities  cannot work out how some of the people diagnosed with swine flu caught  the virus. ___________________________ 
Swine flu threat becoming more serious: Roxon 
Abc news  3 hours 26 minutes ago 
Federal Health Minister Nicola Roxon says she is concerned authorities  cannot work out how some of the people diagnosed with swine flu caught  the virus. 
Authorities have confirmed 14 Australians are infected with swine flu,  but Ms Roxon says she expects that number to rise today. 
She says the threat is becoming more serious. 
"We do have a slightly worrying development that we have several cases  where we cannot identify where they might have got the disease from,"  she said. 
"So they're not travellers, not immediate contacts of confirmed cases." 
CSIRO virologist Jennifer McKimm-Breschkin says swine flu has been in  the Australian community for longer than people realise. 
Dr McKimm-Breschkin says the influenza A H1N1 virus could have come into  the country without anyone noticing, because its symptoms cannot be  distinguished from other strains of flu. 
"[That] is easy enough to do because everyone is aware of flu symptoms,  and flu is always circulating all year, so if someone's come in with a  mild case of flu, they may not necessarily have thought about going out  in the community," she said. 
"So once it's out in the community, then other people are in contact  with these people, and the virus just sort of spreads from person to  person." 
She says it is not surprising people with no history of overseas travel  have come down with the illness. 
School monitored 
Health authorities are keeping a close eye on Mill Park Secondary  College in Melbourne's northern suburbs after the latest confirmed case  of the disease. 
A 15-year-old boy from was diagnosed with the virus on Saturday,  bringing to 14 the total number of cases in Australia. 
Twenty-two year nine students have been quarantined at home until  Thursday and given antiviral medication as a precaution. 
Denise McColl's daughter Ashleigh is one of those students and says she  has been pleased with the response by health authorities. 
"[We were] shocked at first, but now we're not so worried," said Ms McColl. 
"She's fine, that's the main thing. They say they show symptoms pretty  quickly, and she's not showing any." 
Cruise ship quarantine 
Speaking to Channel Nine, Ms Roxon also defended authorities who  quarantined a cruise ship in Sydney because of a swine flu scare yesterday. 
The decision affected the travel plans of almost 3,000 passengers and  crew on the Dawn Princess. Four passengers were tested for the virus,  but were later cleared. 
Ms Roxon said the Government is doing everything it can to minimise the  spread of swine flu. 
"I think it is necessary for us to take all the precautionary steps that  we can, particularly while this disease is not yet widespread in  Australia," she said. 
"That's why we are taking the steps that we are. Luckily the testing has  got a lot quicker, although that was an inconvenience for several hours  for people." 
 <URL>  ___________________________________________ 
Australia raises pandemic threat level for swine flu 
AUSTRALIA'S swine flu alert level has been raised from delay to contain,  Federal Health Minister Nicola Roxon announced today. 
 Australian Associated Press /May 22, 2009/ 
The upgrade in alert means further measures are likely to be put in  place across the country to try to contain the spread of the H1N1 virus. 
The decision comes after the first case of human to human transmission  was confirmed in a school in Victoria. 
Ms Roxon said equally concerning was the two cases of the virus being  contracted in people who had not travelled recently or been in contact  with someone carrying the disease. 
In the case of a 17-year-old from Victoria, health officials have traced  his recent movements and are stumped as to how he contracted the virus. 
"He has got it from somewhere,'' Victorian Premier John Brumby said.  "But all the tracing that has been done so far hasn't been able to  establish a link between him and somebody who is known to have the  virus. Clearly, medically, you can't just catch it out of the air.'' 
Ms Roxon said raising the alert level enabled the government to ensure  it acted appropriately as the disease spreads in Australia. 
"We will continue border protection to stop people coming into Australia  with swine flu,'' Ms Roxon told reporters in Melbourne on Friday afternoon. 
"The contained phase is not the top phase; there are three more levels  after that. 
"This doesn't mean each and every state will be undertaking the same  response. It provides us with flexibility for social distancing measures  such as school closures, which we have already seen in South Australia  and Victoria.'' 
Victoria's acting chief health officer Rosemary Lester said drug  companies estimate it will take three months to develop and manufacture  an effective vaccine. Dr Lester said when the vaccine becomes available,  decisions will need to be made over which groups within the community  are given priority access. 
There are still only 11 confirmed cases of the disease across the  country, with 20 test results pending. 
AAP 

