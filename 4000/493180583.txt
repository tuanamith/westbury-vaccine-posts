



Here is Dr. W. Jean Dodds' Latest Recommendation Vaccination Schedule for those of you who are interested. 
'Vaccination Schedule Recommendations For Dogs' ( <URL> ) 
_DR._JEAN_DODDS'_RECOMMENDED_VACCINATION_SCHEDULE_[/B]_ 
*DISTEMPER (MLV)*  -INITIAL- (E.G. INTERVET PROGARD PUPPY) 9 WEEKS, 12 WEEKS, 16 - 20 WEEKS  -1ST ANNUAL BOOSTER- AT 1 YEAR MLV DISTEMPER/ PARVOVIRUS ONLY  -RE-ADMINISTRATION INTERVAL-  *NONE NEEDED.* *DURATION OF IMMUNITY 7.5 / 15 YEARS BY STUDIES. PROBABLY LIFETIME.* LONGER STUDIES PENDING.  -COMMENTS -CAN HAVE NUMEROUS SIDE EFFECTS IF GIVEN TOO YOUNG (< 8 WEEKS).  
*PARVOVIRUS (MLV) -Initial- (e.g. Intervet Progard Puppy) 9 weeks, 12 weeks, 16 - 20 weeks  -1st Annual Booster-At 1 year MLV Distemper/ Parvovirus only   -Re-Administration Interval- *None needed.* DURATION OF IMMUNITY 7.5 YEARS BY STUDIES. PROBABLY LIFETIME*. LONGER STUDIES PENDING.  -COMMENTS -AT 6 WEEKS OF AGE, ONLY 30% OF PUPPIES ARE PROTECTED BUT 100% ARE EXPOSED TO THE VIRUS AT THE VET CLINIC.  
*RABIES (KILLED)  -Initial- 24 weeks or older  -1st Annual Booster-At 1 year (give 3-4 weeks apart from Dist/Parvo booster) Killed 3 year rabies vaccine -Re-Administration Interval-   3 yr. vaccine given as required by law in California (follow your state/provincial requirements)  -Comments -rabid animals may infect dogs.  
_VACCINES_NOT_RECOMMENDED_FOR_DOGS_*_ 
*DISTEMPER & PARVO @ 6 WEEKS OR YOUNGER Not recommended. At this age, maternal antibodies form the mothers milk (colostrum) will neutralize the vaccine and only 30% for puppies will be protected. 100% will be exposed to the virus at the vet clinic.  
CORONA  Not recommended. 1.) Disease only affects dogs <6 weeks of age. 2.) Rare disease: TAMU has seen only one case in seven years. 3.) Mild self-limiting disease. 4.) Efficacy of the vaccine is questionable.  
LEPTOSPIROSIS Not recommended 1) There are an average of 12 cases reported annually in California. 2)  Side effects common. 3) Most commonly used vaccine contains the wrong serovars.  (There is no cross-protection of serovars) There is a new vaccine with 2 new serovars. Two vaccinations twice per year would  be required for protection.). 4) Risk outweighs benefits. 
LYME*  NOT RECOMMENDED 1) LOW RISK IN CALIFORNIA. 2) 85% OF CASES ARE IN 9 NEW ENGLAND STATES AND WISCONSIN. 3) POSSIBLE SIDE EFFECT OF POLYARTHRITIS FROM WHOLE CELL BACTERIN.  
*BORETELLA (Intranasal) (killed) Only recommended 3 days prior to boarding when required. Protects against 2 of the possible 8 causes of kennel cough. Duration of immunity 6 months.  
GIARDIA*  NOT RECOMMENDED EFFICACY OF VACCINE UNSUBSTANTIATED BY INDEPENDENT STUDIES  
THERE ARE TWO TYPES OF VACCINES CURRENTLY AVAILABLE TO VETERINARIANS: MODIFIED-LIVE VACCINES AND INACTIVATED (\"KILLED\") VACCINES. 
[B]IMMUNIZATION SCHEDULES 
There is a great deal of controversy and confusion surrounding the appropriate immunization schedule, especially with the availability of modified-live vaccines and breeders who have experienced postvaccinal problems when using some of these vaccines. It is also important to not begin a vaccination program while maternal antibodies are still active and present in the puppy from the mother's colostrum. The maternal antibodies identify the vaccines as infectious organisms and destroy them before they can stimulate an immune response. 
Many breeders and owners have sought a safer immunization program.  
MODIFIED LIVE VACCINES (MLV) 
Modified-live vaccines contain a weakened strain of the disease causing agent. Weakening of the agent is typically accomplished by chemical means or by genetic engineering. These vaccines replicate within the host, thus increasing the amount of material available for provoking an immune response without inducing clinical illness. This provocation primes the immune system to mount a vigorous response if the disease causing agent is ever introduced to the animal. Further, the immunity provided by a modified-live vaccine develops rather swiftly and since they mimic infection with the actual disease agent, it provides the best immune response. 
INACTIVATED VACCINES (KILLED) 
Inactivated vaccines contain killed disease causing agents. Since the agent is killed, it is much more stable and has a longer shelf life, there is no possibility that they will revert to a virulent form, and they never spread from the vaccinated host to other animals. They are also safe for use in pregnant animals (a developing fetus may be susceptible to damage by some of the disease agents, even though attenuated, present in modified-live vaccines). Although more than a single dose of vaccine is always required and the duration of immunity is generally shorter, inactivated vaccines are regaining importance in this age of retrovirus and herpesvirus infections and concern about the safety of genetically modified microorganisms. Inactivated vaccines available for use in dogs include rabies, canine parvovirus, canine coronavirus, etc. 
W. Jean Dodds, DVM HEMOPET 938 Stanford Street Santa Monica, CA 90403 310/ 828-4804 fax: 310/ 828-8251 
Note: This schedule is the one I recommend and should not be interpreted to mean that other protocols recommended by a veterinarian would be less satisfactory. It's a matter of professional judgment and choice. For breeds or families of dogs susceptible to or affected with immune dysfunction, immune-mediated disease, immune-reactions associated with vaccinations, or autoimmune endocrine disease (e.g., thyroiditis, Addison's or Cushing's disease, diabetes, etc.) the above protocol is recommended. 
After 1 year, annually measure serum antibody titers against specific canine infectious agents such as distemper and parvovirus. This is especially recommended for animals previously experiencing adverse vaccine reactions or breeds at higher risk for such reactions (e.g., Weimaraner, Akita, American Eskimo, Great Dane). 
Another alternative to booster vaccinations is homeopathic nosodes. This option is considered an unconventional treatment that has not been scientifically proven to be efficacious. One controlled parvovirus nosode study did not adequately protect puppies under challenged conditions. However, data from Europe and clinical experience in North America support its use. If veterinarians choose to use homeopathic nosodes, their clients should be provided with an appropriate disclaimer and written informed consent should be obtained. 
I use only killed 3 year rabies vaccine for adults and give it separated from other vaccines by 3-4 weeks. In some states, they may be able to give titer test result in lieu of booster. 
I do NOT use Bordetella, corona virus, leptospirosis or Lyme vaccines unless these diseases are endemic in the local area pr specific kennel. Furthermore, the currently licensed leptospira bacterins do not contain the serovars causing the majority of clinical leptospirosis today. 
I do NOT recommend vaccinating bitches during estrus, pregnancy or lactation. 
W. Jean Dodds, DVM HEMOPET 



--  Kris L. Christine 

