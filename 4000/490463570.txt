


"June 4 (Bloomberg) -- The head of the World Health Organization urged governments not to overreact to global outbreaks of swine flu as the agency moves closer to declaring the first pandemic in four decades.  
Margaret Chan, the WHOs director-general, may declare an influenza pandemic within days, said three people familiar with the organizations plans. Chan, in an interview late yesterday, said a final decision hasnt been taken, and authorities must use the time before swine flu becomes global to revise emergency plans and convey the measures to their countries populations.  
Most pandemic plans were crafted for the H5N1 bird flu virus, deadly in three out of five cases. Swine flu, the new bug racing across the world, so far causes little more than a fever and a cough in most patients. Governments must resist the temptation to restrict travel, close borders and adopt other measures that arent justified, according to Chan.  
Every country has some kind of pandemic preparedness plan originally meant for H5N1, she said in a telephone interview from her office at WHOs Geneva headquarters. Now they need to look at the plan and make appropriate revisions. It is most important to get the messages right.  
The new H1N1 flu strain, discovered in April, has turned up in 66 countries as far removed as Japan, Iceland and New Zealand. The virus is now starting to spread in Australia, Japan, U.K., Spain and Chile among people with no travel history and outside of schools and institutional settings, Keiji Fukuda, the WHOs assistant director-general of health security and environment, said on a conference call with reporters June 2.  
Extremely Cautious  
The United Nations health agency currently is at phase 5 on its six-step pandemic alert scale, meaning a global epidemic is imminent.  
We are closer to phase 6 than we were one week ago, but I dont want to come out and make an announcement of phase 6 and cause debate, Chan said. I am extremely cautious in a sense. By the time I come out, no countries will dispute the evidence that we have on hand.  
The new virus has sickened 19,273 people and caused 117 deaths since it was discovered in Mexico and the U.S. in April, WHO said yesterday. Bird flu, which isnt easily transmitted among people, has killed 262 of the 433 people known to have been infected since 2003.  
WHOs guidelines say a pandemic is imminent when a new virus causes outbreaks in at least two countries in one WHO region. Its under way when the disease is widespread in at least one other country in another WHO region.  
Living With Uncertainty  
The new bug is showing signs of becoming the dominant flu strain this winter in Chile, where as least 360 people have fallen ill, Chan said. A similar picture is emerging in Australia, where 634 cases have been confirmed, said Ian Barr, deputy director of the WHO Collaborating Center for Influenza in Melbourne.  
The trend, if confirmed, will support calls for declaring a pandemic, and a recommendation that drugmakers switch from making a vaccine against seasonal flu to a shot that can protect against the pandemic strain, Chan said. In previous pandemics, seasonal flu was crowded out by the new virus, she said.  
We need to monitor for the next week or two signs that swine flu is supplanting seasonal flu, Chan said. If this is indeed the case, it may have an impact on our decision on when and how to take into action the pandemic vaccine manufacturing.  
WHO also wants to ensure adequate supplies of immunizations against the influenza strains that circulate every winter.  
The agency has said the new virus is more contagious than the germs that cause annual epidemics, which kill 250,000 to 500,000 people each year. A study in the journal Science estimates the virus is as severe as the 1957 Asian flu pandemic that caused about 2 million deaths.  
Chan has said its possible the pig-derived strain might become more virulent later, a pattern that occurred with the 1918 Spanish flu, killing at least 40 million people.  
'The most certain thing about the influenza virus is uncertainty,' Chan said. 'Health officials worldwide, including those of us working in WHO, have to live with this level of uncertainty for the weeks and months ahead.'" 
Source: 
 <URL>  
What we are teaching to prepare folks in our local communities for the probable eventuality of a deadlier Pan-Flu (the deadlier H5N1 virus is persisting in landfills --> water supply to possibly recombine with the more infectious H1N1 in swine for the creation of a deadlier hybrid virus): 
 <URL>  
How to not be fearful: 
Trust the truth, Who is Jesus !!! 
 <URL>  
... by being hungrier:  
 <URL> ? 
Hunger is wonderful ! ! ! 
It's how we know the answer to the question "What does Jesus want?" (WDJW): 
 <URL>    
Yes, hunger is our knowledge of good versus evil that Adam and Eve paid for with their and our immortal lives: 
 <URL> ? 
 <URL> ? 
Moreover, being hungrier is the key to being Jesus' disciples: 
 <URL> ? 
 <URL> ? 
"Blessed are you who hunger NOW...  
... for you will be satisfied." -- LORD Jesus Christ (Luke 6:21)  
Amen.  
Here is a Spirit-guided exegesis of Luke 6:21 given in hopes of promoting much greater understanding:  
 <URL> ? 
Jesus is LORD, forever !!! 
 <URL>  
Be hungrier, which is truly healthier for mind, body, and soul: 
 <URL> ? 
Marana tha 
Prayerfully in the awesome name of our Messiah, LORD Jesus Christ,  
Andrew <>< -- "... no one can say 'Jesus is LORD' except by the Holy Spirit." (1 Cor 12:3)  <URL> ? 
What are the keys of the Kingdom of Heaven?  <URL> ? 
Only the truth can cure the "hunger is starvation" delusion:  <URL> ? 

