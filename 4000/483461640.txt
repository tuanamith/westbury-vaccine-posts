


As 'domestic violence' [beating up women] must also 'ebb', having shut Mexicans behind their homes' doors, Mexican Government "sees less swine-flu cases": 
US Officials: Swine Flu Less Deadly than First Feared By Michael Bowman Washington 03 May 2009 	 
U.S. health officials are expressing cautious optimism the virus causing swine flu A-H1N1 may not be as virulent as initially feared, but they say aggressive steps to combat the infectious disease are still warranted. 
People, wearing face masks as a precaution against swine flu, walk after crossing from U.S. to Mexico at the San Ysidro crossing port in Tijuana, Mexico, 30 Apr 2009 People, wearing face masks as a precaution against swine flu, walk after crossing from U.S. to Mexico at the San Ysidro crossing port in Tijuana, Mexico, 30 Apr 2009 Days after Mexico suspended public activities to reduce the spread of swine flu, the country is reporting a leveling off in the rate of new infections. In the United States, the number of confirmed influenza cases continues to rise, but most flu sufferers report relatively mild symptoms, and only one death has been recorded. 
This is welcome news for U.S. health officials, who initially had to consider the possibility that the new flu strain's impact could mirror the devastation of the 1918 influenza pandemic that led to tens of millions of deaths worldwide. 
The acting director of the U.S. Centers for Disease Control, Richard Besser, spoke on ABC's "This Week" program. 
"What we have found is that we are not seeing the factors that were associated with the 1918 pandemic. We are not seeing the factors that were associated with other H1N1 viruses," Besser said. 
But, he adds, "I do not think it is time to let our guard down. I think we have to continue in an uncertain situation to be aggressive." 
U.S. officials worry that, even if the virus' spread is eventually contained, it could re-emerge months from now during the northern hemisphere's winter flu season, when more than 30,000 Americans die each year from common influenza. 
The Obama administration says it is laying the groundwork to develop a vaccine against swine flu, although no decision has been made on large-scale production of such a vaccine. Health and Human Services Secretary Kathleen Sebelius also appeared on "This Week". Newly-confirmed Health and Human Services Secretary Kathleen Sebelius at a briefing on the government's response to the H1N1 virus, 29 Apr 2009 Newly-confirmed Health and Human Services Secretary Kathleen Sebelius at a briefing on the government's response to the H1N1 virus, 29 Apr 2009 

"We can accelerate the seasonal flu vaccine, which we are doing right now to be prepared for what we know will hit this fall and winter. At the same time, we are in the stages of growing the [swine flu] virus, testing it, and we can be ready to do both [vaccines] simultaneously," Sebelius said. 
In the meantime, the Obama administration is urging common-sense steps to prevent the spread of swine flu, including frequent hand-washing and asking flu sufferers to stay at home. 
Some critics have accused the administration of overstating the dangers of swine flu and unnecessarily alarming the public. Homeland Security Secretary Janet Napolitano dismisses the charge. 
"We had a new strain of flu. We did not really know that its lethality was going to be. We did not know how quickly it was going to move. Once you get behind [the spread of] flu, you cannot catch up. You have to get ahead of it," said Napolitano.   In an appearance on "Fox News Sunday," Napolitano again defended the administration's decision against closing the U.S. border with Mexico, saying it would incur massive economic costs with no meaningful health benefits, since swine flu is already spreading in the United States. 
 <URL>  
 <URL>  

