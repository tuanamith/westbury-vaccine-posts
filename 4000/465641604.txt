


 <URL>  
Newsgroups: alt.support.autism, misc.health.alternative, misc.kids,  misc.kids.health From: "JOHN" < <EMAILADDRESS> > Date: Sat, 28 Feb 2009 18:48:10 -0000 Local: Sat, Feb 28 2009 1:48 pm Subject: Another ruling in the US vaccine court 
 <URL> -... 
Another ruling in the US vaccine court Friday, 27th February 2009 

Melanie Phillips 

Seven days ago, the US vaccine court awarded damages to a ten year-old child, Bailey Banks, who it said had developed acute brain damage involving autistic spectrum disorder as a result of his MMR vaccination. 

This followed a judgment by the same court a few days previously in the 'Cedillo' case which threw out three test claims involving MMR on the grounds that that there was no proven link between the MMR vaccine and autism. The judges in that case said parents had been misled by doctors who were guilty of 'gross medical misjudgment' and had peddled 'speculative and unpersuasive' theories. 

That judgment in turn followed another case in which the vaccine court said nine year-old Hanna Poling had developed autism as a result of a cocktail of nine vaccines administered simultaneously, including MMR, which had 

  significantly aggravated an underlying mitochondrial disorder, which predisposed her to deficits in cellular energy metabolism, and manifested as a regressive encephalopathy with features of autism spectrum disorder. 

In the Bailey Banks case, the ruling was unequivocal. It concluded from the evidence provided by a full neurological examination of the child 16 days after his MMR vaccination that the jab had caused Acute Disseminated Encephalomyelitis (ADEM) which in turn had led to Pervasive Developmental Delay, a disorder on the autistic spectrum. 

Special Master Richard Abell wrote: 

  The Court found, supra, that Bailey's ADEM was both caused-in-fact and proximately caused by his vaccination. It is well-understood that the vaccination at issue can cause ADEM, and the Court found, based upon a full reading and hearing of the pertinent facts in this case, that it did actually cause the ADEM. Furthermore, Bailey's ADEM was severe enough to cause lasting, residual damage, and retarded his developmental progress, which fits under the generalized heading of Pervasive Developmental Delay, or PDD. The Court found that Bailey would not have suffered this delay but for the administration of the MMR vaccine, and that this chain of causation was not too remote, but was rather a proximate sequence of cause and effect leading inexorably from vaccination to Pervasive Developmental Delay. 

Therefore it had been successfully demonstrated that 

  the MMR vaccine at issue actually caused the condition(s) from which Bailey suffered and continues to suffer. 

It also turns out from this ruling that the vaccine court had heard two previous cases where the Special Master had found that the MMR vaccine had caused Acute Disseminated Encephalomyelitis. 

In response to the Bailey Banks case, Dr. Bryan Jepson, an autism specialist at Thoughtful House where Andrew Wakefield now conducts research, said: 

  The contradictory rulings from the Vaccine Court regarding vaccines and autism demonstrate that we still don't have a definitive answer. We need to realize that the question of MMR's potential contribution to autism remains under scientific debate. Ultimately, the correct answer will come through honest, transparent and rigorous scientific study, not from a court bench. 


  


