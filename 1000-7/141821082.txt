


On 9 Aug 2006 01:26:35 GMT,  <EMAILADDRESS>  (Kelly Bert Manning) wrote: 

You asked for an example ... I gave one ... citizens who declaim demanding their "rights" while not taking responsibility for defending them.  Survival isn't a gift, it's a privilege and it's got to be paid for at some point.    

I agree with that, but there are usually some surviors left to tell the tales from which "history" gets molded, which offers "lessons" to learn. 

Stretching is good for the mind, and it was an answer off the top of my head.  I still think it serves the purpose. 

I grew up in or close to Big Cities: Long Island, Chicago, Detroit.  I chose to raise my children away from such places.  Our old house had no central heating.  I learned to handle a wood cookstove (still have it in my kitchen for heat and power outages), and a small pot-bellied stove upstairs; chopped my own wood, drew water in buckets on a sled from the creek 1/4 mile out back during one long cold winter, then filtered, boiled and re-filtered it for consumption; bred and tended a herd of 10-30 dairy goats for milk, a couple dozen chickens for eggs; washed clothes in the bathtub and dried them on a line in the kitchen; I heated water on the wood cookstove to bathe the kids; baked 5-10 loaves of bread a week using home grown yeast; grew the family vegetables in my garden. 
Hmmm ... mine just may be bigger than yours ... ;^> 

"Common knowledge" is often uncommon and less than factual: it's kind of like "common sense". 
If you have a problem with what Morely writes, why not take it up with him?  He's a big, strong fellow and can handle his own battles. I've already posted the fact that the rabies vaccine is only recommended for certain folks ... I don't think Morely fits in any of those categories; I know I don't.   


