



From The San Francisco Gate, 7/28/06:  <URL>  
Dumb People Make Children Cry  
PBS fires young, female kiddie-show host over old, naughty video.  
Smart people groan 
By Mark Morford, SF Gate Columnist 

What sort of people are we?  
What sort of warped and reckless and utterly silly value system do we suck on in this culture?  
Why are we so wildly, preternaturally terrified of all things sexual while at the same time drawn to it all like fat teenagers to French fries?  
It is a fine moment to ask.  
Is it the lingering cancer of puritanism that makes us so bewildered?  
The rash-inducing wail of the evangelical right?  
How can they be slapped upside the head and put into a small cold room without windows for 10 thousand years or until they relax and take off their pants and learn to laugh again?  
Here is one example.  
Here is an insidious nonstory about sex and puritanism and silly stupid people that happened recently in our silly stupid morally damaged culture,  
and you need to know because it infects us all and we need to develop some sort of vaccine, some sort of protective body armor as soon as humanly possible.  
Apparently, the PBS Sprout Network just fired one of its on-air talents, name of Melanie Martinez, the host of a nightly, three-hour show called "The Good Night Show," which, according to the AP story, "airs soothing stories and cartoons designed to get an audience of 2-to-5-year-olds ready for bed. Each night, Martinez guides a puppet character into dreamland."  

Right.  
So "The Good Night Show" ain't exactly "The West Wing."  
It's not exactly "Deadwood" on HBO.  
It's a cute piece of harmless fluff on PBS that airs in about 20 million homes.  
Martinez, it is reported, has a toddler of her own, which we can take to mean she knows something about what kids like.  
I'm guessing she smiles a lot.  
Is maternal.  
Sits on giant, brightly colored furniture and chats with birds.  
Or whatever.  
Now, not owning a toddler of my own and therefore usually watching "The Daily Show" or browsing blogs or surfing for porn during kiddie bedtime hour, I have no idea what the hell this show is.  
But it sounds just insanely sweet and safe, in the way only a show that's designed to be televised Ambien for the pre-complete-sentence set could possibly be.  
But then, a shocker.  
Turns our lovely Melanie Martinez is not what she appeared to be.  
Turns out Melanie Martinez had, prior to her gig smiling a lot and keeping your kids company for three solid hours while you join me in surfing for porn on the Internet, turns out she had a tiny sliver of an adult life before she took the gig at PBS.  
And what's worse, it's on video.  
On the Internet.  
Oh. 
My.  
God.  
Yes indeed, before joining PBS (a full seven years before, mind you), Martinez apparently appeared in two harmless, 30-second satire videos called "Technical Virgin" in which she spoofs PSAs by joking about -- say it with me now -- sex and virginity.  
Gasp.  
And PBS found out about it.  
Because Martinez told them.  
And of course, PBS, spineless as a jellyfish licked by Pat Robertson, immediately fired her.  
They claimed the dialogue in Martinez's humor videos somehow meant she wasn't a good role model for their fluffy kiddie show.  
A show, remember, that's designed for 3-year-olds.  
For creatures who can barely go to the bathroom by themselves, much less understand the meaning of an adult satiric video they will never ever see.  
This is why it is so easy to hate the BushCo-drugged world.  
This is why foreign countries laugh at us and mock and shake their heads and sigh.  
We cannot walk and chew gum and think about an orgasm at the same time.  
Yes, spineless PBS is to blame.  
But so are we in the media, for endlessly hyping hyperbolic fear-addled stories about sexual predation and child abuse so out of scale with actual reality, it trickles down and induces spineless execs at PBS to fire Martinez because they're openly terrified of the whiny backlash that might strike them if groups of ignorant parents  
(read: red-state religious right, mostly)  
found out that the host of a kiddie show actually has a sense of humor about sex in her non-kiddie-show life.  
Plus, it must be said, Martinez is a woman.  
She's relatively young, attractive, alive.  
She's still a vibrant, sexual being.  
She is not a 60-year-old Mrs. Doubtfire type in spectacles and a shapeless housedress.  
And nothing scares media executives and right-wing religious types more than youngish women who are effortlessly unafraid of sex and sly humor.  
Just ask, you know, Eve.  
Still, obvious questions about PBS' cowardly decision linger like acrid smoke.  
Most notably:  
How is any of this logical?  
How could the kids possibly find out about Martinez's old videos?  
How could they possibly be harmed if they never ever know?  
How could Martinez possibly become a threat?  
Was PBS intending to force the kids to watch Martinez's old videos and explain why they should now fear and loathe her?  
Would tremulous, heavily medicated parents start pointing to Martinez when she came on the screen, and scream?  
What, exactly, is the fear here?  
Is it that Martinez would suddenly start extolling kiddies to, say, drink more vodka and turn gay?  
Would she jump through the TV when Mommy's not looking and whisper seductively to the kids, telling them to steal Mommy's stash of Percocet and bring it down to the local PBS affiliate and leave it in an envelope with Martinez's name on it, scrawled in pretty purple crayon?  
Because that would be really cool.  
Now might be a good moment to point out the dozens of adult actors (male, of course) who appear in countless kiddie movies and TV shows, most of whom have happily raunchy personal histories.  
Haven't George Carlin, Ringo Starr and Alec Baldwin all appeared in that sweet little choo-choo train show, "Thomas the Tank Engine"?  
What about Eddie Murphy, Robin Williams, Chris Rock?  
What of all the big (male) stars who appear on, provide voices for, even produce kiddie shows?  
Is it because they're famous?  
Because they're rich older men, whereas Martinez is a nonfamous youngish sexual female and is therefore an eternal danger to all mankind everywhere?  
Don't you already know the answer?  
It must be said:  
The biggest threat to children today is not sex or silly videos or sweet TV hosts.  
It's witless, sexually confused adults.  
It's trembling bipeds who never have sex and who never drink and who never do drugs and who never have sex while drunk and on drugs while hanging from the ceiling, laughing.  
They are the true danger to us all.  
It even says so in the Bible:  
"Beware, ye who eat food from cans. Beware the whiny and the self-righteous and the humorless hand-wringers, for they shall poop upon the earth."  
I think it's in Leviticus.  
Who will save the children?  
Who will save the children from wimpy TV execs and depressive parents and a distended media that sits idly by, hyping sex and abuse and shrugging into their coffee?  
Will it be you?  
Can you write a letter or something?  
Can you send Melanie Martinez some nice cookies by way of an apology?  
Trust me, your kids will thank you, later.  
_____________________________________________________________ 
 <URL>  

Harry 

