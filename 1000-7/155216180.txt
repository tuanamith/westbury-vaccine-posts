



Barb Junkkarinen wrote: 

Here are some notes I made a while back about Ed Haslam's appearance on Black Op Radio, Show #195, October 31, 2004. 
Part One: 
 <URL>  
Part Two: 
 <URL>  
Part Three: 
 <URL>  

Part One: 
Summary of MARY, FERRIE & THE MONKEY VIRUS 
Haslam's theories re: Mary Sherman murder 
Circa 21:30: Judyth Baker cited as witness to Ferrie/Sherman lab 
22:10: 


<QUOTE ON>--------------------------------- 
Haslam: In New Orleans in the fall of 1972, I was introduced to a woman and I was told her name was Judyth Vary Baker, and I was told that she was a close, personal friend of Lee Harvey Oswald's. This woman, I can guarantee you, is not the same woman as the one we now know to be Judyth Vary Baker, that you interviewed on your radio show recently. 
[...] 
When I was at Tulane University, and I was with my girlfriend, I got into a [sic] argument one day in the University Center with some people concerning what is now described as the Ferrie-Sherman lab in the books [sic], about all this underground stuff with the monkey viruses and the research. Several days later I was invited to a party, and it was clear that the invitation to this party had something to do with this conversation that I had. And my girlfriend did not want me talking about this stuff anymore, because I was alienating her fellow graduate students, and I had to promise that if I went to this party that I was not going to shoot off my mouth about all this stuff. So we go to this party and right before we go inside I ask her, I said, well, you know, I'd like to know the name of the host or hostess of this party, because, you know, I'm from New Orleans, and I know what the rules of society are [laughs], and I'd like to able to, you know, be polite to the hostess and thank her and all that stuff. And she tells me very clearly that the woman's name is Judyth Vary Baker. So we go on in there and I meet this woman. And this party is basically set up as a bunch of female graduate students who are networking. They're allowed to bring their spouses or significant others, but everybody, the males all understand they have a secondary role to play at this party. I spend most of my time at the party with the husband of this woman described to me as Judyth Vary Baker and the child that they had. But I did have some conversations with her directly. She told me a lot of the same things about her being from a small town in Florida and stuff like that. And then the women were out on the porch drinking wine and stuff, and my girlfriend came in and she was rather startled by something. And she said, Ed, they're talking about all that stuff you were talking about the other day in the University Center. And I'm thinking, great, you know, I promised that I wasn't going to talk about this at this party, and now I'm surrounded by a bunch of inebriated feminists who are getting into the subject, and all I have to do is stay out of it. And then Barbara says, she says, And they say that Judyth knows Oswald, or knew Oswald. I said, oh, this is great. You know, here's a subject I'm trying to stay out of, and here somebody's throwing information at me like this. And then this Judyth woman walks into the kitchen and Barbara heads out of the room, and says, I understand you're interested in the Garrison investigation. I kind of nod at her. And she says, Is this something that you'd like to talk about? And I said, no, it's not. And I went and got Barbara and I said, look, you asked me not to talk about this, and I'm very distrustful and suspicious about this thing being a set-up for some reason. And I don't know who or why, but I, all my lights are on, and I want to get out of here. And she said, well, that's fair. I asked you not to talk about it, and I don't see how we can stay without you talking about it, so let's go. And then a couple of weeks later we got an invitation from this gal for us to come over just as a couple for dinner. And I said, is this the woman that said she knew Oswald? And she said yes. And I said, well, I don't want to go have dinner with a friend of Lee Harvey Oswald's [laughs], if that's okay with you. You know? And that was the last I heard of it. But I'm absolutely positive of the name. And one of the reasons I'm so positive of the name is, back before the current [laughs] Judyth Vary Baker showed up, I was sitting in an office and there was a plaque on somebody's door, and said the name was Judy Baker. And I just remembered this whole incident. And, you know, I said, well, I didn't put this in the book, because I don't, you know, assume that everything that happened fits into some kind of puzzle. But it certainly was a weird situation. Then one day several [sic] years later, I get a phone call from people saying they're with 60 MINUTES and they're investigating this woman who said that she was in this laboratory I wrote about in my book, and says that she knew Lee Harvey Oswald. And I said, well, why don't you send me something in writing on it? And when they finally sent me something in writing on it, I was reading, and I saw her name. I said, I know this woman, and I assumed it was the same person. And I called her up, and said, I remember you. I had dinner at your house in New Orleans. And she said, when was that? I said 1972. She said, I wasn't there in 1972. And I said, well, now we have a real serious disconnect here. We've got two different people using the same name, both saying they're friends with Lee Harvey Oswald, who obviously aren't the same person. So one of them is an impostor. And it took me a while to sort out which one, because I wasn't necessarily buying into the one that 60 MINUTES showed up with. But now that I know her better, and I know her story, and I've seen her documentation and everything else, it's pretty clear to me that she's the real one. Now, who was the other one and why was I presented with this information and who was behind it, I can't tell you. I can't tell you 'cause I don't know. 
Anita Langley: Well, it sounds like somebody knew that you were interested in the subject and was just on a fishing expedition to see if you were serious. 
Haslam: And I think that's a pretty reasonable read on it, and that's Jim Marrs's take on it, 'cause I've discussed it at length with him. It's just, what I can't tell you is who, was that Garrison behind it, trying to give me information, so I had the right names? Or was that somebody from Army Intelligence who was trying to throw me off? And, one of the things that, you know, it's a classic spook move, is to confuse people's identities, I mean, even to the point where the soldiers in World War II, they used to change their badges, so when they got captured by the Germans or the enemy, that it would confuse their intelligence operations about who they are. 
<QUOTE OFF>------------------------------- 

Part One concludes here. Part Two begins: 

<QUOTE ON>--------------------------------- 
Now, if I had taken the position, when the new Judyth Vary Baker showed up, that she was the impostor, because I knew the real Judyth Vary Baker, then that would have blown her credibility from the beginning. Which is very interesting, because that would be a very interesting thing to happen, to cause confusion and distrust, and to prevent her from being able to show up and tell her story. But these are conjectures. I don't know who was behind the thing. But I do know, positively and absolutely, that I was introduced to somebody named Judyth Vary Baker in 1972 in New Orleans. And I just cannot believe it wasn't some kind of intelligence operation, because I don't know how those things would line up otherwise. 
[...] 
It could have been Ochsner [who was behind it], for that matter. He was still alive at the time. I mean, but I don't know who [it] was. There are a lot of people you could guess at. 
<QUOTE OFF>------------------------------- 

Part Two continues with more on Haslam's book, cancer-causing viruses in vaccines, AIDS epidemic. 
Circa 5:00: It can never be proved that the AIDS epidemic tracks back to Haslam's cancer lab. 
Circa 6:00: Signs that HIV-1 is "unnatural." 
Circa 7:30: SV-40, "the most carcinogenic virus ever discovered," found in polio vaccine. One of the largest political skeletons in the closet in history. Also sexually transmitted. 
Circa 11:00: Propinquity: Oswald's apartment around the corner from some of Haslam's operations. 
Circa 19:30: LBJ visited JFK's grave on July 21, 1964, the day Mary Sherman was killed. 
Circa 20:00: Banister's files, INCA. 
Circa 22:30: Oswald was the agent/mole in Banister's operation behind the government's closing of the exile "camp" in summer 1963. Nixon hides operations from JFK. CIA missing millions of dollars. JFK puts RFK in charge of finding out what happened. Implies that LHO was connected to this. 
Circa 25:45: Relates Adele Edisen story, connecting National Institute of Health to LHO. 
Circa 30:00: Haslam's teacher, foreknowledge of Warren Commission conclusion, cover-up related to who LHO really was, as well as a deeper secret, which Haslam says was the contamination of the polio vaccine. LHO was looking for the money missing from the CIA, Nixon administration. 

Part Three: 
Begins with discussion of INCA. 
Circa 2:00: Using radiation to mutate SV-40, "medical Manhattan Project." 
Circa 8:00: Ferrie 
Circa 9:30: Judyth's claims of developing a bioweapon with Ferrie. 
Circa 10:15: Judyth was manipulated, didn't always know what was being done with the things she was working on. 
Circa 12:00: Haslam has written a screenplay based on the book and done some interviews, but has otherwise "moved on" from his research. 
Circa 16:00: Government censorship of cancer statistics. 
Circa 17:30: Reference to theories of Ruby injected with cancer. 
Circa 18:15: Osanic raises subject of Ferrie demise. Haslam says that Cyril Wecht told him he could prove in a court of law that Ferrie was murdered. 
Circa 21:00: Posner connections to CIA. 
Circa 23:00: Back to Mary Sherman's death, Haslam's theory of an attempt to murder Ochsner in order to expose Oswald, bring down LBJ and Nixon. 
Circa 31:00: Foreknowledge/cover story in place prior to Mary Sherman's murder. 
Dave 



