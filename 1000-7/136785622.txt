



the following is a different list of the same.  Some items may be repeated, however, out of respect for the sourced material of each separate online web site source, the entire lists are presented intact. 
This list below is from Israel 21c - A Focus Beyond 
URL:  <URL> & ___________________ 


Israel, the 100th smallest country, with less than 1/1000th of the world's population, can make claim to the following: 
 An Israeli ornithologist is utilizing barn owls to rid large cities of rodent problems. 
 An Israeli company has developed a device that helps nurses locate those hard-to-find veins. 
 Israeli actress Hanna Laslo took home the "Best Actress" award at the 2005 Cannes Film Festival this year for her performance in Amos Gitai's "Free Zone." 
 An Israeli system to help dyslexic readers is being used throughout the US and Europe. 
 Voice over internet protocol (VoIP) technology was pioneered in Israel. 
 The Israeli women's national flag football team won the largest and most important open football tournament in Europe in 2005. 
 An Israeli FDA-approved device - the VelaSmooth - reduces the appearance of cellulite. 
 Israeli laser technology is powering the latest hair removal devices on the American market. 
 Intel's new multi-core processor was completely developed at its facilities in Israel. 
 An Israeli doctor headed the Merck team that developed a vaccine against cervical cancer. 
 Prof. Robert Aumann is the fourth Israeli in the last four years to win a Nobel prize. 
 Israel's premier basketball team Maccabi Tel Aviv beat the Toronto Raptors in an exhibition game in 2005. 
 AirTrain JFK - the 8.1-mile light rail labrynthe that connects JFK Airport to New York City's mass transit - is protected by the Israeli-developed Nextiva surveillance system. 
 Bill Gates called Israel a major player in the high tech world. 
 The Weizmann Institute of Science has been voted the best university in the world for life scientists to conduct research. 
 An Israeli 'super-sensor' has been installed in Sealy mattresses to control snoring problems. 
 Hawaiian singer Don Ho underwent an Israeli-developed stem cell treatment to strengthen his heart. 
 Israeli company Ultrashape has developed a safe replacement for liposuction - a unique new body-contouring device that "blasts" unwanted fat from the body. 
 Israeli researchers have discovered the molecular trigger that causes psoriasis. 
 A 100-member Israeli delegation flew to Kenya in January, 2006 to rescue survivors of a building collapse. 
 Israeli research has shown that dancers display consistent differences from the general population in two key genes. 
 Israeli research shows that we can find out more about what is buried beneath the earth's surface by launching a satellite into the sky. 
 An Israeli company has unveiled a blood test that via the telephone diagnoses heart attacks. 
 The Israeli-developed Ex-Press shunt is providing relief for American glaucoma sufferers. 
 An Israeli research team has found that the combination of electrical stimulation and chemotherapy makes cancerous metastases disappear. 
 Israel has designed the first flight system to protect passenger and freighter aircraft against missile attack. 
 Jewish and Arab students at Hebrew University participate in the 'Billy Crystal Workshops - Peace Through the Performing Arts' project. 
 Israel has the highest ratio of university degrees to the population in the world. 
 Israel produces more scientific papers per capita than any other nation by a large margin - 109 per 10,000 people - as well as one of the highest percapita rates of patents filed. 
 In proportion to its population, Israel has the largest number of startup companies in the world. In absolute terms, Israel has the largest number of startup companies than any other country in the world, except the US (3,500 companies mostly in hi-tech). 
 Israel is ranked #2 in the world for venture capital funds right behind the US. 
 Israel has the highest average living standards in the Middle East. The per capita income in 2000 was over $17,500, exceeding that of the UK. 
 Israel's $100 billion economy is larger than all of its immediate neighbors combined. 
 On a per capita basis, Israel has the largest number of biotech start-ups. 
 Israel has the largest raptor migration in the world, with hundreds of thousands of African birds of prey crossing as they fan out into Asia. 
 Twenty-four percent of Israel's workforce holds university degrees - ranking third in the industrialized world, after the United States and Holland - and 12 percent hold advanced degrees. 
 Israel is the only liberal democracy in the Middle East. 
 In 1984 and 1991, Israel airlifted a total of 22,000 Ethiopian Jews at risk in Ethiopia to safety in Israel. 
 When Gold Meir was elected Prime Minister of Israel in 1969, she became the world's second elected female leader in modern times. 
 When the U.S. Embassy in Nairobi, Kenya was bombed in 1998, Israeli rescue teams were on the scene within a day - and saved three victims from the rubble. 
 Israel has the third highest rate of entrepreneurship - and the highest rate among women and among people over 55 - in the world. 
 Relative to its population, Israel is the largest immigrant-absorbing nation on earth. Immigrants come in search of democracy, religious freedom, and economic opportunity. 
 Israel was the first nation in the world to adopt the Kimberly process, an international standard that certifies diamonds as "conflict free." 
 According to industry officials, Israel designed the airline industry's most impenetrable flight security. U.S. officials now look to Israel for advice on how to handle airborne security threats. 
 Israel's Maccabi basketball team won the European championships in 2001. 
 Israeli tennis player Anna Smashnova is the 15th ranked female player in the world. 
 Mighty Morphin' Power Rangers was produced by Haim Saban, an Israeli whose family fled persecution in Egypt. 
 Israel has the world's second highest per capita of new books. 
 Israel is the only country in the world that entered the 21st century with a net gain in its number of trees. 
 Israel has more museums per capita than any other country. 
 Israel has two official languages: Hebrew and Arabic. 
 Israeli scientists developed the first fully computerized, no-radiation, diagnostic instrumentation for breast cancer. 
 An Israeli company developed a computerized system for ensuring proper administration of medications, thus removing human error from medical treatment. Every year in U.S. hospitals 7,000 patients die from treatment mistakes. 
 Israel's Given Imaging developed the first ingestible video camera, so small it fits inside a pill. Used the view the small intestine from the inside, the camera helps doctors diagnose cancer and digestive disorders. 
 Researchers in Israel developed a new device that directly helps the heart pump blood, an innovation with the potential to save lives among those with congestive heart failure. The new device is synchronized with the heart's mechanical operations through a sophisticated system of sensors. 
 With more than 3,000 high-tech companies and start-ups, Israel has the highest concentration of hi-tech companies in the world (apart from the 
Silicon Valley). 
 In response to serious water shortages, Israeli engineers and agriculturalists developed a revolutionary drip irrigation system to minimize the amount of water used to grow crops. 
 Israel has the highest percentage in the world of home computers per capita. 
 Israel leads the world in the number of scientists and technicians in the workforce, with 145 per 10,000, as opposed to 85 in the U.S., over 70 in Japan, and less than 60 in Germany. With over 25% of its work force employed in technical professions. Israel places first in this category as well. 
 The cell phone was developed in Israel by Motorola, which has its largest development center in Israel. 
 Most of the Windows NT operating system was developed by Microsoft-Israel. 
 The Pentium MMX Chip technology was designed in Israel at Intel. 
 Voice mail technology was developed in Israel. 
 Both Microsoft and Cisco built their only R&D facilities outside the US in Israel. 
 The technology for AOL Instant Messenger was developed in 1996 by four young Israelis. 
 A new acne treatment developed in Israel, the ClearLight device, produces a high-intensity, ultraviolet-light-free, narrow-band blue light that causes acne bacteria to self-destruct - all without damaging surroundings skin or tissue. 
 An Israeli company was the first to develop and install a large-scale solar-powered and fully functional electricity generating plant, in southern California's Mojave desert. 
 The first PC anti-virus software was developed in Israel in 1979. 
 Christopher Reeve called Israel the "world center" for research on paralysis treatment. 
 That Israel is becoming integrally involved in helping to reshape the post-war Iraqi landscape? 
 Israeli researchers are playing an important role in identifying a defective gene that causes a rare and usually fatal disease in Arab infants. 
 An Israeli company has been given a U.S. grant to develop an anti-smallpox first aid treatment kit. 
 The top women's magazine in the world - Cosmopolitan - is launching a Hebrew version in Israel? 
 An Israeli company M-Systems was the first to patent and introduce key chain storage. 
 Israeli microbiologists have developed the first passive vaccine against the mosquito-borne West Nile virus. 
 A team of Israeli and US researchers has designed a watermelon-picking robot endowed with artificial vision to do the job of harvesting. 
 Israeli researchers are using video games to investigate future treatments for memory disorders such as Alzheimer's disease. 
 An Israeli company has developed sensors that pick up signs of stress in plants. 
 Israeli medical researchers have shown that lycopene - the red pigment found in tomatoes - lowers blood pressure. 
 A small Israel company called Lenslet has developed a revolutionary electro-optic processor which operates one thousand times faster than any known Digital Signal Processor. 
 Israeli stem-cell technology is being used in the U.S. to regenerate heart tissue. 
 An Israeli company has developed a device that could enable millions of American diabetics to painlessly inject themselves with insulin. 
 An Israeli company is providing the technology behind an American all-electric bus for urban use. 
 An Israeli medical delegation from the 'Save a Child's Heart' project recently spent two weeks in China performing open heart surgery on children. 
 Israeli-developed security precautions have been adopted in Maryland and Washington. 
 Scientists in Israel have used strands of DNA to create tiny transistors that can literally build themselves. 
 A week-old Iraqi infant underwent an emergency operation in Israel to correct a congenital heart defect. 
 A new generation of "Sesame Street" programs aimed at teaching tolerance is being produced and broadcast in Israel, Jordan, and in the Palestinian Authority. 
 Some 500 million birds representing 300 species migrate across Israel's skies twice a year in the autumn and spring along the Great Valley Rift. 
 Israeli research shows that a tonsillectomy could be the key solving sleep apnea in children. 
 An Israeli company has developed a new device for monitoring coronary disease that will be integrated into future generation of cellphones. 
 An Israeli-developed alogrithm enabled NASA to transmit images from Mars. 
 An Israeli has invented a 'bone glue' that will reduce the need for bone transplants and heal bone defects caused by cancer. 
 An Israeli team will compete in the Women's International Flag Football Championships. 
 Israeli researchers have developed a 'bone glue' that respectively stimulate speedy bone and cartilage repair, and enable faster and improved healing of injuries. 
 Research by three scientists from the Haifa Technion made the transmission of video pictures Video transmissions from Mars by the NASA explorer "Spirit" have been made possible thanks to a unique algorithm developed by Technion graduates. 
 A joint Israeli-Palestinian expedition recently scaled a peak in Antarctica in the name of coexistence. 
 Over 50 million Israeli flowers were sent to Europe for sale on Valentine's Day. 
 The families of the Columbia shuttle astronauts are going to be the Israeli government's guests on a week long visit in March. 
 Israeli researchers have found a connection between sleep apnea and impotence. 
 Israel, American and Canadian researchers are working together to develop nanotech-based solutions to the water shortage in the Middle East. 
 The founder of the Cancer Prevention and Wellness Program at New York's prestigious Memorial Sloan-Kettering Cancer Center is Israeli. 
 Between 150 to 200 multinational clinical trials are regularly taking place in Israel. 
 An Israeli company is the world's leading sleep disorder sensors manufacturer. 
 Israeli biologists have successfully managed, for the first time, to prepare the flowering of the "Madonna Lily" - a rare white Easter lily - in time for Easter. 
 Over 65,000 patients worldwide have swallowed the M2A capsule, the incredible 'camera in a capsule' technology developed by Israel's Given Imaging. 
 Israel hosts IBM's largest R&D facilities outside the United States. 
 Israeli researchers have developed an engineless, nano-RPV (remote piloted vehicle). 
 Israeli researchers are successfully using magnets to treat post-traumatic stress disorder. 
 Israeli scientists have created a DNA nano-computer that not only detects cancer, but also releases drugs to treat the disease. 
 Tel Aviv has been named a UNESCO World Heritage Site. 
 The U.S. Marines in Iraq are using an Israel-developed hand-held computer for communication purposes. 
 Israel engineers are behind the development of the largest communications router in the world, launched by Cisco. 
 An Israeli doctor claims that he has succeeded in developing a female equivalent to Viagra called Sheagra. 
 An Israeli company - Evogene - is developing cotton plants that are resistant to adverse salinity conditions and drought. 
 An Israeli company has developed the world's first jellyfish repellent. 
 Israel has helped farmers in Niger develop a horticultural production system called the African Market Garden (AMG). 
 Jerusalem hosted an international gay rights parade WorldPride in 2005. 
 A newly developed Israeli cooking oil is capable of breaking up blood fats such as cholesterol. 
 Israeli scientists have alleviated Parkinson's-like symptoms in rats. 
 Israeli scientists are developing a nose drop that will provide a five-year flu vaccine. 
 Israeli researchers have solved the mystery of Lenin's death. 
 An Israeli vaccine for West Nile virus is being tested in the U.S. 
 Israeli scientists have discovered how to turn mismatched cells into cancer fighters. 
 Over 20 Israeli companies provided security and services for the 2004 Olympic games in Athens. 
 Israeli scientists have shown that hypnotism doubles the chances of success of in vitro fertilization. 
 An Israeli two-flush system can save Americans billions of gallons of water a year. 
 A group of 40 American sheriffs were in Israel last week to learn about Israeli counter-terrorism techniques. 
 An Israeli-developed device can painlessly administer medications through microscopic pores in the skin. 
 Israeli air force pilot technology is being used to train American college basketball players. 
 Israeli researchers have shown that a daily dosage of Vitamin E is effective in helping to regain hearing loss. 
 Israeli researchers have created a 'biological pacemaker' which corrects faulty heart rhythms when injected into the failing hearts of pigs. 
 Two Israelis have won the 2004 Nobel Prize for Chemistry for their groundbreaking work in cancer research. 
 An Israeli physicist-turned-inventor has developed the world's first air-conditioned motorcycle. 
 Israeli researchers have proven that Prozac can improve the effectiveness of chemotherapy. 
 An Israeli-developed elderberry extract is one of America's best-selling flu prevention medicines. 
 Millions of American youngsters will soon be able to surf safely in Internet chat rooms thanks to Israeli technology. 
 Children injured in the school siege in Beslan, Russia in 2004 were able to convalesce at an Israeli coastal resort in Ashkelon - at the invitation of that city's mayor. 
 An Israeli company has developed a simple blood test that distinguishes between mild and more severe cases of MS. 
 Israeli scientists have discovered the cause of chronic bad breath and a painless solution. 
 Israeli technology is behind the successful testing of in-flight cell phone use. 
 Israeli research has found that citrus oils may hold the key for asthma treatment. 
 An Israeli study has shown that anger may trigger strokes. 
 An Israeli company - Patus Ltd. - has donated thousands of its OdorScreen olfactory gel product to counter the crippling odors faced by on-scene Tsunami disaster workers. 
 An Israeli company has developed a nano-lubricant that one day could mean the end of changing your car oil. 
 A young Israeli scientist was among those chosen as an example of carrying on the work of Albert Einstein 100 years later. 
 Intel has sold more than $5 billion worth of the Israeli-developed Centrino chipsets since they were introduced in March 2003. 
                                  * * * 


