





    Measures taken by the Clinton administration  to thwart international terrorism and bin Laden's  network were historic, unprecedented and, sadly,  not followed up on.  
Consider the steps offered by Clinton's 1996 omnibus  anti-terror legislation, the pricetag for which stood at  $1.097 billion. The following is a partial list of the  initiatives  offered by the Clinton anti-terrorism bill: 
    Screen Checked Baggage: $91.1 million 
    Screen Carry-On Baggage: $37.8 million 
    Passenger Profiling: $10 million 
    Screener Training: $5.3 million 
    Screen Passengers (portals) and Document  Scanners: $1 million 
    Deploying Existing Technology to Inspect  International Air Cargo: $31.4 million 
    Provide Additional Air/Counterterrorism  Security: $26.6 million 
    Explosives Detection Training: $1.8 million 
    Augment FAA Security Research: $20 million 
    Customs Service: Explosives and Radiation  Detection Equipment at Ports: $2.2 million 
    Anti-Terrorism Assistance to Foreign Governments:  $2 million 
    Capacity to Collect and Assemble Explosives Data:  $2.1 million 
    Improve Domestic Intelligence: $38.9 million 
    Critical Incident Response Teams for Post-Blast  Deployment: $7.2 million 
    Additional Security for Federal Facilities: $6.7 million 
    Firefighter/Emergency Services Financial Assistance:  $2.7 million 
    Public Building and Museum Security: $7.3 million 
    Improve Technology to Prevent Nuclear Smuggling:  $8 million 
    Critical Incident Response Facility: $2 million 
    Counter-Terrorism Fund: $35 million 
    Explosives Intelligence and Support Systems:  $14.2 million 
    Office of Emergency Preparedness: $5.8 million 
    The Clinton administration poured more than a  billion dollars into counterterrorism activities across  the entire spectrum of the intelligence community,  into the protection of critical infrastructure, into  massive federal stockpiling of antidotes and vaccines  to prepare for a possible bioterror attack, into a  reorganization of the intelligence community itself.  
Within the National Security Council, "threat meetings"  were held three times a week to assess looming  conspiracies. His National Security Advisor, Sandy  Berger, prepared a voluminous dossier on al-Qaeda  and Osama bin Laden, actively tracking them across  the planet. 
    Clinton raised the issue of terrorism in virtually  every important speech he gave in the last three  years of his tenure. 
    Clinton's dire public warnings about the threat  posed by terrorism, and the actions taken to thwart  it, went completely unreported by the media, which  was far more concerned with stained dresses and  baseless Drudge Report rumors.  
When the administration did act militarily against bin  Laden and his terrorist network, the actions were  dismissed by partisans within the media and  Congress as scandalous "wag the dog" tactics.  
The news networks actually broadcast clips of the  movie "Wag the Dog" while reporting on his warnings,  to accentuate the idea that everything the administration  said was contrived fakery. --- 

