


GeneK wrote: 
He didn't appear to.  He seemed distressed at the fallout it caused, but didn't appear to think that the act itself was wrong.  Just those darn supply ships coming faster than he thought made it appear wrong. 
There's a really good Danger Man episode called "Judgment Day" in which Drake is ambused in the desert by Israeli renegades after the Spanish doctor he's escorting back to London, who's really a Nazi war criminal.  The guy had been a doctor in a concentration camp who did experiments on the prisoners. 
The kicker is that the guy believes that the lynch party will understand and sympathize once they hear his story.  You see, he wasn't just trying to be mean about it, he was trying to develop vaccines for the next war, just in case it was bacteriological instead of nuclear. Sure, he infected a lot of prisoners with the Plague, but it was all in a good cause, and besides, he had a survival rate 20 points higher than other camps that were doing the same thing. 
Drake, who's job is to get the guy back home, is reduced to having to argue that the guy is a moral imbecile.  He's killed all these people, and has absolutely no idea that he's done anything wrong.  It doesn't quite fly, of course, but that's really all he had at that point. 




There are deductive proofs and inferential evidence.  Spock tended to confuse the two frequently, just as Carl Sagan frequently confused the difference between Science and Philosophy. 


There might, if putting the pieces together was at all difficult.  For example, if we're on some planet, and I said "All the criminals around here are Klingons", someone who didn't know what the Fallacy of False Conversion was might think that I was saying that all Klingons were criminals, when in fact that isn't logically implied at all. 


Yes, the problem with "logic", is that it's extremely narrow.  Like a hammer, it's a tool for some jobs, but not for every job.  To make it an all-encompassing unified field theory of life, it has to be expanded.  A lot.  In Spockspeak, "logic" is a generic catch-all term for any kind of calm, methodical thinking, whether strictly logical or not. 
In the scene in Kirk's cabin, Spock reacts every bit as emotionally as McCoy does, though he conceals it a bit better. 


