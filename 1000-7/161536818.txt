



"David Rind" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
In one recent year, the flu vaccine didn't match the straings going around  very well. In almost all other recent years, the CDC nailed it and had a  good match between the strains going around the and the vaccine (it was not  a perfect match, but the vaccine provided good immunity). So most of the  time, they get a good vaccine. 
However, another thing to consider is that not does the flu vaccine prevent  people from getting the flu, in many people who get the vaccine, the flu is  less severe than in those who didn't get the vaccine. In other words, the  vaccine often protects against severe disease, e.g., disease requiring  hospitalization or resulting in death or requiring a long time off from  work. So the vaccine may protect people, even if they ultimately get sick. 
A final thing to consider is who should get vaccinated. Recent studies  concluded that immunizing school kids is very important, because they are so  very good at spreading it around. 
Jeff 




