



 <EMAILADDRESS>  wrote: 
I don't think there is a cure if the cat has full-blown FeLV. The way I understood it, if a cat has recently been exposed (esp. a kitten), they can "throw" the disease. I googled for a better explanation. This is from a website I wanted to share because it's almost exactly the way my vet explained it to me and it kind of gives hope. Also, the test is notoriously inaccurate. It bugs me to think how many cats are given up on from just that one snap test. 
"A positive test result means that a cat has been exposed to the feline leukemia virus.  It's a good idea to confirm a test done in the office, which is the common procedure here in the States, with a different test done at a lab facility.  Some cats exposed to the virus will throw it off and become negative but you usually don't notice that and, by the time  the cat is tested, it may just be negative.  But they might also want to  test again in 3 months to see if the cat has gone from positive to negative. 
This doesn't mean that the cat has leukemia.  That name was given to the virus several decades ago.  Actually, cats positive for the virus are  more susceptible to other diseases, like respiratory disease, GI problems,  etc. Also lymphoma.  All because the virus affects how their immune system functions. 
If a cat remains positive, it can still lead a good life but the chances are that it won't live as long as a healthy cat.  But with good  nutrition, living indoors so there isn't a lot of exposure to unhealthy stuff, jumping on any illnesses asap, and, perhaps, using drugs like  interferon alpha or Immunoreglan to stimulate the immune system, these cats can do well.  This virus is fairly easy to pass from cat to cat.  (Unlike FIV which is hard to pass.)  So a positive cat should really not be exposed  to other cats.  There's a vaccine against FeLV but it isn't 100%  effective." 
From:  <URL>  


