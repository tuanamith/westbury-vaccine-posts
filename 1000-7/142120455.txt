


On Wed, 9 Aug 2006 14:25:21 -0700, "Kevin W. Miller" < <EMAILADDRESS> > wrote: 

I saw that article, and asked a Chinese friend about it. He says that they probably mean at any given time, there's a 70% chance of seeing a dog when you look out the window :-) Most dogs in China are wild, according to him. 
Another article claims that in one prefecture where 16 people have died from rabies in the last 8 months, there are 500,000 dogs. There are 17 prefectures.  <URL>  
Uh, huh. Let's start with just that one prefecture. Someone has to catch 500,000 dogs, many of which don't want to be caught, and vaccinate them. A year later, they have to do it all over again. (In the meantime, of course, a bunch more have appeared - maybe they could sterilize them at the same time?) After that, it's only every three years, plus keeping up with the new offspring. 
Actually, a baiting program with oral vaccine might be more realistic, but the results would be much less certain, and the number of dogs and attendant problems (rabies is not the only problem caused by millions of roaming dogs, or even the only disease) would continue to increase. 
Most Americans think of dogs as cute domesticated pets, with the occasional exception of a farmer who has experienced the destruction that even a small pack of dogs can cause. In Asia, things are different, in spite of many of the wealthier people becoming more westernized. 
Of course, the US Humane Society has offered $100,000 to help. Probably the Chinese are still giggling about that. 
--  Al Balmer Sun City, AZ 

