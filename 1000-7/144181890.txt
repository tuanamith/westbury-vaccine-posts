


On Tue, 15 Aug 2006 11:30:33 GMT, "RichC" < <EMAILADDRESS> > wrote: 

In 1920, it was illegal to sell tobacco in 14 states. All those laws were repealed by 1927. 
The death rate in California has declined significantly since 1994: 
Death rate per 100K, age adjusted to 2000 census, all ages, both genders, race white, California 
1994  900 1998  842 2002  777 
In looking for something to compare it to, I found that the death rate is declining rapidly in all states, and in most countries, and has been for more than a century.  Life expectancy is now double what it was 100 years ago .. everywhere. What's hard to believe is that NOBODY KNOWS WHY. The explanations usually given turn out to be false when examined closely -- infant mortality, public health (e.g. clean water), vaccines, infectious diseases (stopped by antibiotics) and the most expensive fallacy: better health care. It turns out there is no correlation whatever between money spent on health care and mortality, yet the US collectively spends 15% of GDP on medicine.  
The definitive paper on mortality, called Lee-Carter, is not available online. The first reference below by one of its authors, Lee, has a nice chart on page 5 showing the 100 year trend in life expectancy. The second reference discusses why the usual explanations are erroneous. The third reference contains informed rebuttal to the second reference. 
In one year, 2004, the US mortality rate fell by 4%. The three main causes of death -- heart disease, cancer and stroke -- each fell by 6-7%. Statistically, those are huge declines in one year. It will be difficult to separate health benefits of smoking bans from the larger phenomenon going on. 
 <URL>   <URL>   <URL> = LEE, RONALD D., AND CARTER, LAWRENCE. 1992. Modeling and Forecasting the Time Series of U.S. Mortality, Journal of the American Statistical Association 87, no. 419 (September): 65971. 

