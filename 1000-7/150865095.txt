


On Sun, 27 Aug 2006 16:46:50 GMT, "David" < <EMAILADDRESS> > wrote: 

No, it is not possible.  The net result is the parasite dies or lets go but the immune system is oblivious to those end results.  The response is to proteins produced by the organism.   When the proteins no longer appear, the immune response stops.  It doesn't matter if is because the parasite is alive, dead, MIA or just changed it's makeup.  
 One mechanism used to evade an immune response is to alter the surface proteins produced; the parasite is still there and alive.  The primary examples are the African trypanosomes.  Their genetic makeup can produce over 100 variants of common proteins used as triggers by the host immune system.  Only one is produced at a time and which one changes over time while in the host.  That leads to cycles of high protozan population, the immune system responds, kills a bunch, parasite changes protein, immune raction shuts down, cycle repeats. Malaria does the same thing to a lesser extent; it also hides inside red blood cells.  That's what makes development of a vaccines extremely difficult for internal parasites; you have to use an invariant protein produced in quantity. See Janeway et al, Immunobiology, part V, chapter 11.   Failures of the host defense mechanisms. 
Ticks are external parasites and don't vary epitopes; they are attached too short a period for epitope switching to be a useful strategy.  A vaccine will work to produce this end result.  However, the immune system is using the presence of a tick specific protein to trigger a response.  You can inject the pure protein without any animal attached and get the same immune system response. 

No question that it is a better strategy but developing effective vaccines against internal parasites is a lot harder than you think. 



