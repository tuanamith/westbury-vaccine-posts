


 <URL>  
Posted, Aug. 31, 2006 
Friday Edition:Uptick in tick disease... 
By Al Tompkins 
Tick Diseases 
The CDC just reported a "cluster" of tick paralysis cases in Colorado. 
During May 26 - 31, 2006, the Colorado Department of Public Health and Environment received reports of four recent cases of tick paralysis. The four patients lived (or had visited someone) within 20 miles of each other in the mountains of north central Colorado. 
You may have read that last week, the Governor of Tennessee was hospitalized for nearly a week from an apparent tick bite. 
The GAO reported in 2001: 
disease increased by 72 percent. Although Lyme disease has been reported in 49 states and the District of Columbia, 9 states, mainly in the eastern part of the country, account for 92 percent of the nationally reported cases. Persons of all ages and both sexes are equally susceptible, although the highest attack rates are in children under 15 and in adults from age 45 to 65. 
Could it be that doctors now are diagnosing this disease, or is it that tick bites are getting more dangerous? 
Tick bites carry Lyme disease and it is at the center of a ton of research going on through the National Institutes of Health. Lyme disease can cause heart problems, eye inflammation, arthritis, liver disease, severe fatigue, can affect muscle movement, cause neck soreness and even make limbs go numb. The NIH says: 
The Institute conducts and supports most of the basic and clinical research on Lyme disease funded by the National Institutes of Health (NIH). However, because Lyme disease affects different tissue/organ systems of the body, it is also a matter of great concern to other NIH institutes and centers. 
The National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS) supports research on chronic Lyme-induced arthritis, including the role of the immune system and genetic factors in contributing to its development. The National Institute of Neurological Disorders and Stroke (NINDS) supports research to characterize the neurological, neuropsychological, and psychosocial manifestations of early and late Lyme disease in both adults and children, as well as to characterize pathogenic mechanisms associated with the neurological symptoms of chronic Lyme disease. The National Center for Research Resources (NCRR) provides resource support (non-human primates) for basic and clinical studies on both acute and chronic infection, as well as support for testing and developing candidate vaccines for Lyme disease. In addition, the Fogarty International Center (FIC) funds research on Lyme disease abroad, and the National Institute on Aging (NIA) and the National Institute of Mental Health (NIMH) focus on those aspects of Lyme disease that relate to their specific mission. 


Editor's Note: Al's Morning Meeting is a compendium of ideas, edited story excerpts and other materials from a variety of Web sites, as well as original concepts and analysis. When the information comes directly from another source, it will be attributed and a link will be provided whenever possible. The column is fact-checked, but depends upon the accuracy and integrity of the original sources cited. Errors and inaccuracies found will be corrected. 
Copyright =A9 1995-2006 The Poynter Institute 


