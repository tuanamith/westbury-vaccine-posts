


Epidemic Intelligence Whitewash wrote: 

Zoonotic agents, particularly viruses. That's as specific as I'll get. 

The impetus for the biodefense spending has occurred as a response to potential terrorism. That's something the politicians decided, not the scientists. In fact, many scientists believe that too much is spent on biodefense. You can't blow up sand in Iraq and fund worthwhile science at the same time. Funds are too limiting. 
With that said, it does not require a stretch of the imagination to realize how vulnerable we are to such an event. It would require some technical skill to isolate something like Ebola, but if you can find a few dozen people who will volunteer to be infected, get on a airplanes and fly to the US, you'd soon have outbreaks of Ebola Zaire in major metropolitan areas. I suppose if you're willing to strap a bomb to your body, you'd probably be willing to be infected with a fatal agent. Such an incident could make 9/11 look like a minor event. 

You'll have to take that up with the PHS and NIAID. I'm sure they have good reasons for not providing the amount of funding you think is necessary for your favorite disease. 

They would if tularemia were used as a bioterrorism agent and released in a heavily populated area. It's one of the most likely to be used agents for such an event. Say "tularemia" to a crowd and you get blank stares. Say "anthrax" and everyone's eyes light up. Tularmemia would be a lot worse than anthrax because it's communicable. 

But none of these are potential bioterrorism diseases, except perhaps mad cow - but how do you produce it? How do you distribute it? It would be a highly inefficient weapon. 

Again, it's about the potential of bioterrorism. The tularemia bacterium is easy to grow and disperse on a susceptible population, such as the US. 

Well, by definition, it's only a "pathogen" if it causes disease. Otherwise it's just an innocuous infectious agent. 

If laws were broken then they should have been prosecuted. Shutting down the CDC because of this would be akin to banning religion because of Jimmy Swaggert, James Bakker and sexullly-abusive priests. 

I had addressed that already and kept it in this post as well (see below). You have to assess risk. I'd rather *have* a vaccine than not have one - put the ball in their court for a while. Some protection is better than none. 

Personal opinion. The system works. It's not perfect, but it is the best system of science of any other country. (Although this might change in a few decades because of the way India and China are supporting science.) 

I don't know much about Atlanta, but all the scientists I know at CDC-Fort Collins (and I know more than a few) are decent, dedicated and hard-working people. The only bad thing about CDC now is that in the post-9/11 paranoia it's much more difficult for them to speak openly (because you never know who else might be listening). Do you personally know anyone from CDC-FC? 
Now, some CDC administrators - that's another story... 

Do you have evidence supporting the assertion you made above? 

The Bush administration uses phone monitoring because it is useful (and I'm sure it is, but with a rather high price - privacy). It wouldn't be useful to produce bioweapons. First, we don't need them since we have tactical nukes - superior weapons. Second, the use of bioweapons would result in the pursuit of such weapons by other nations (i.e., we'd lose the "moral authority"). Third, it would violate and effectively end a Convention and treaty obligation. While I disagree with the phone monitoring, I can see why *they* justify it. But I don't see how they could justify the use of bioweapons. Compared to nukes, they're inefficient, unreliable, uncontrollable and unstable. It's a no-brainer. 


