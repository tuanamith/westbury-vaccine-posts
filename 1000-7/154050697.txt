



Tick, Tick, Tick: Lyme disease problematic but preventable 
By Katie Young, Freeman staff 09/03/2006 




WITH THE magnificent mountains, green terrain and flowing river that distinguish the Hudson Valley and Catskills comes a not-so-enjoyable component: deer ticks. 
Roughly the size of a sesame seed, if not engorged, infected deer ticks can spread Lyme disease, a bacterial infection that can cause numerous health problems if left untreated. 
That doesn't mean people should stop spending time outdoors, but they must be aware of the dangers of Lyme and perform proper tick checks to prevent contracting the disease. 
THE DISEASE'S symptoms first appeared in Europe, and the first confirmed case of the disease in the United States was in Lyme, Conn., in 1975. New York state has one of the highest rates of reported cases in the country, with the majority occurring in the southern portion of the state. 
Dean Palen, Ulster County's director of public health, said the Hudson River seems to act as a barrier of sorts because there is a higher number of reported cases east of the river, particularly in Dutchess County, than west. 
Mary Lombardo, Greene County's public health nurse and Lyme disease expert, said the county has seen a gradual decrease in the number of reported cases during the last few years. She believes the tick population may be moving north. 
Dr. H.C. Harde of Hudson agrees the problem seems to be moving toward the Albany area. He said he typically sees three or four cases each week in the warm seasons, fewer than in past years. 
There are a number of factors attributed to the movement and fluctuations of Lyme disease, including human activities, natural ecosystems and habitats. 
NOT ALL deer ticks are infected; only those that have fed on an infected animal, such as a field mouse or deer carrying the disease, will be infected. Female ticks pass on the disease because they are the ones that must feed off a blood source to reproduce. 
Nymphs, or young deer ticks, account for the majority of Lyme disease cases because their smaller size makes them more difficult to spot. An engorged adult tick can swell up to the size of an apple seed and is much easier to notice. 
CONTRARY to popular belief, ticks cannot jump from bushes or trees and land on passersby. An animal, rodent or person must literally brush by the tick, which usually is waiting to latch on. The tick then will travel along a person's body until it finds exposed skin. It prefers creased, moist areas like bellybuttons, the groin or the area behind the knee. 
"You brush against them and they just hitchhike," said Gary Myers, environmental health manager at the Ulster County Department of Health. "They grab right on and start looking for a source of blood." 
MYERS spearheads Ulster County's tick identification program, determining the species of ticks brought in, whether a tick is an adult or nymph, and whether the tick's mouth parts have broken off into the skin or still are intact. Myers said almost 50 percent of the deer ticks he sees have broken mouth parts, which could lead to a secondary infection in the victim's skin. 
Infected ticks that have engorged themselves for longer than one to three days have a greater chance of passing on the disease. Myers can estimate how long the tick has been on the body by examining the size of the tick and how engorged it is. 
STACY Kraft, Ulster County's public health education coordinator, emphasized the importance of performing body checks each day to catch any ticks that may have latched on before the 24- to 36-hour grace period. 
"The good news is there is such a long time period to find them," she said. 
EARLY Lyme disease symptoms include fatigue, stomach pain, headaches, muscle and joint pain and the signature sign of the disease, a red bulls-eye shaped rash that usually encircles the tick bite area and may last for up to two weeks. Not all victims have the rash, but those who do are immediately placed on antibiotics. 
Long-term symptoms that may develop, especially if left untreated, include increased pain in joints and muscles, nervous system problems, chronic arthritis, fatigue and heart problems. 
DR. MARK Foster, of Hudson Valley Primary Care in Wappingers Falls, said the disease can take four to six weeks to show in a blood sample. He said some patients who have Lyme disease must be retested before results are deemed accurate. 
"One of the problems with the Lyme test is that it's not as refined as tests for other illnesses, so we're trying to refine that," said Orange County Health Commissioner Jean Hudson. "It's important to check yourself for ticks." 
THE DISEASE is treated with oral antibiotics, including amoxicillin, doxycycline and cefuroxime, which are typically administered for a month. 
People cannot develop an immunity to Lyme disease, and a person who already has been treated for the disease can contract it again if reinfected. And there is no vaccine for the disease. 
LaGRANGE resident Robert VonPichl, who was infected with Lyme disease about 19 years ago, said the test didn't show the presence of the disease at first. VonPichl said his joints were stiffening up and he was tested repeatedly for Lyme, always with negative results. 
A specialist diagnosed VonPichl with rheumatoid arthritis, a disease that can be hard to distinguish from Lyme disease's similar symptoms. 
VonPichl's condition began to improve until he had a relapse and was diagnosed with Lyme. The antibiotics didn't work, so VonPichl was put on intravenous drip to get rid of the disease. 
"In theory, I guess it could be true that Lyme disease could (cause) neurological problems," VonPichl said. "It's possible it could have started off (the arthritis)." 
THE KEY to fighting Lyme disease is prevention, according to Andrew Evans, Dutchess County's senior public health adviser. 
Like its counterparts in neighboring counties, the Dutchess County Department of Health participates in the "Be Tick Free" initiative and offers free tick identification, information packets, tick-removal kits and educational programs. 
Evans said children can practice locating pretend ticks on "Tick Check Harry," a stuffed sheep dog used for educational programs. 
GREENE County Health Educator Diane Aznoe said the county works with Cornell Cooperative Extension on tick identification and community education. 
Bob Beyfuss, agriculture and natural resources issue leader for Cornell Cooperative Extension in Greene County, assists with educational Lyme disease programs for students, senior citizens and civic and special interest groups. 
Marie Cross Ostoyich, Greene County's director of public health, said the county also tries to get the word out as much as possible with informational packets, bulletin boards, health fairs and newspaper and radio ads. 
"The main thing to tell people is to check themselves daily when they go outside," Ostoyich said. 
COLUMBIA County Public Health Director Nancy Winch is part of a Lyme Disease Task Force that meets quarterly and includes doctors, a veterinarian, nurses and representatives from Cornell Cooperative Extension. The group works primarily to get the word out about Lyme prevention techniques. 
Kraft said it also is important to stress the proper removal of ticks. She said Ulster, like many other counties, offers a tick-removal kit that includes tweezers, a magnifying sheet and a tick identification card. 

Ways to prevent Lyme disease 
* Wear light-colored clothing that covers as much of your body as possible when outdoors and tuck in clothing so skin is not exposed. Ticks cannot travel through clothing, so you may be able to spot them traveling up a leg or arm on light-colored clothes. 
* Check your own body, your children and your pets thoroughly after spending time outdoors. Be especially diligent in checking moist, creased areas like the bellybutton and behind the knees and ears. 
* Some experts recommend the use of tick repellents like DEET, but be careful to avoid contact with the skin. 
* Trim your lawn, bushes and trees to reduce the likelihood of ticks near your home. 

How to properly remove a tick 
* Using a pair of fine-point tweezers, grab the tick at its mouth parts, parallel to the skin. Pull gently, but steadily, straight out. 
* Be sure not to grab the tick's body, because this may cause the mouth parts to break off and remain in the skin. 
* You may wish to use a magnifying glass to make it easier to see the entire tick, especially if it is a small nymph tick. 
* Do not rub the tick with Vaseline or try to kill it with kerosene or flame while on the body. Rumored home remedies like these will only increase stress on the tick and encourage it to burrow deeper into the skin. 
* Disinfect the area and bag the tick to bring to a doctor's office or health department for identification. 

Reported local cases, by county 
Ulster: 289 in 2004; 402 in 2005 
Dutchess: 1,076 in 2004; 1,398 in 2005 
Greene: 134 in 2004; 124 in 2005 
Columbia: 464 in 2004; 379 in 2005  
Orange: 532 in 2004; 483 in 2005 


