


Ionic Silver - The Powerful Defense Against Viruses And Other Microbes 
By: Herbert Slavin, M.D. 

-- Posted 12 September, 2006 
The following is an authorized pre-publication reprint of an article to appear shortly in 
Health Freedom News, Vol. 24, No. 3 (Summer 2006), p 22+ 
Health Freedom News is a publication of The National Health Federation (www.thenhf.com) 




By Herbert Slavin, M.D. 




            As the nation watches and waits to see if HN51, commonly called bird flu or avian flu, will become a threat to humans, and firms are scrambling to hopefully develop a cure before there's even anything to target or conduct tests with, it appears that a new form of ionic silver may already be providing us with a remarkably effective treatment for not only a bird flu pandemic that may or may not occur but also an enormous range of infectious diseases that are a very real part of our world today. 


            The fact is that the medical field has yet to prove itself able to deal effectively with virtually any virus, let alone a new one that does not even yet exist.  Yet a rapidly growing phenomenon is taking place wherein ionic silver is emerging as the new antimicrobial wonder in dealing with bacterial as well as, yes, viral conditions too, both in medicine and in industry. 


A Respected Antimicrobial and More 


            Ionic silver was actually a commonly-used antimicrobial 100 years ago, before the advent of modern antibiotics, which only address bacteria and are becoming largely obsolete while posing risks related to resistant super-germs.  Ionic silver is increasingly being recognized for its broad-spectrum antimicrobial qualities and the fact that it presents virtually none of the side-effects related to antibiotics.  Ionic silver is entirely non-toxic to the body.  Research at Washington University School of Medicine in St. Louis has shown that some resistant strains of disease cannot develop with ionic silver the way that they will with antibiotics.  Reports of any pathogens developing resistance to ionic silver are rare.  Some reports indicate it even kills drug-resistant strains of germs. 


            Ionic silver is also a powerful tissue-healing agent, so much so that it has been used topically for decades in burn centers and currently represents one of the fastest growing sectors - if not the fastest growing sector - in wound care today. 


            The fact that ionic silver is effective against a very broad range of bacteria is well established and, due to recent advances in the delivery of ionic silver, together with the problems associated with antibiotics, it is being used in a rapidly growing range of dietary-supplement, medical, and industrial products.  Illustrating how serious this trend is, in a report published in April 2006 by Chemical & Engineering News about a new method from Nexxion for applying ionic-silver coating to catheters, IV needles, and other medical devices, the chief technical officer of the company is quoted as saying, "To date, no pathogens have been able to survive contact with silver."1 


            And that product is just the tip of the iceberg. 


            AcryMed also recently announced FDA approval of its product SilvaGard, a silver-nanotechnology coating to protect medical devices from bacteria.  The company stated, "Ionic silver has been long recognized and used as a highly effective antimicrobial."2 


            Covalon is a manufacturer that has introduced an antimicrobial silver-ion releasing, collagen-based sheet dressing for wound care.  In January 2006, the president of the company is quoted as having said, "In the wound dressings market, silver dressings growth outperforms all others in the category."3 


            Curad and Johnson & Johnson bandages are now available to the consumer with ionic silver actually impregnated into the gauze so as to destroy bacteria.4  In the medical field, another company, AgION has stated that, "Today silver is a key ingredient in new high-tech, powder coated finishes that hospitals and doctors' offices are using to protect walls, counters and other germ-gathering surfaces." 


            Others are jumping on the bandwagon.  Samsung recently introduced a clothes-washing machine that they claim kills 99% of bacteria in cold water by using ionic silver.5  Sharper Image has a line of slippers and pillows that have ionic silver incorporated into the fabric to prevent odor-causing bacteria.  Containers for food storage are now being impregnated with ionic silver to prevent bacterial growth that contributes to spoilage.  Writing pens, bath mats, cutting boards, and door knobs are being coated with ionic silver to prevent bacteria from being spread.  Water-treatment facilities that service hospitals use silver ions.  And that is still just the tip of the iceberg.  The list goes on and on, and is growing faster every week. 


Is It Also Antiviral? 


            It is noteworthy that the above quotes illustrate that ionic silver is often referred to as having "antimicrobial" qualities, rather than just "antibacterial" qualities. 


            This is a very important distinction because many diseases that affect human beings today - and bird flu if it should ever become a pandemic in the future - all involve "viral" pathogens, not bacteria.  And the medical community truly has nothing that will reliably combat viral pathogens.  While the push is on for a "vaccine," there is enormous controversy as to the efficacy and safety of existing vaccines, let alone one that has yet to be developed, if ever.  Nothing that exists today in our efforts to fight common modern-day viral conditions suggests we should expect any miracles. 


            Using ionic silver as a preventative, a treatment, or both may very well prove to be, if not a miracle, perhaps a major stride in shifting the tide in our favor. 


            In Europe, ionic silver is recognized as an accepted treatment for viral conditions. Walk into a pharmacy in Europe and tell them you've been diagnosed with a cold or flu - which is viral in nature - and they will look in their desk reference and find a number of silver-ion compounds or complexes listed as remedies to sell you. It seems the United States is finally catching on.  A study at the University of Arizona recently showed ionic silver to be effective against the coronavirus that researchers use as the surrogate for SARS.6  A study out of the University of Texas reportedly suggested ionic silver may be effective against HIV-1, and researchers expect it may be shown effective against other viruses and bacteria as well. "We're testing against other viruses and the 'super bug (Methicillin resistant staphylococcus aureus),' said Miguel Jose Yacaman, from University of Texas, Department of Engineering and one of the study's authors. "Our preliminary results indicate that silver nanoparticles can effectively attack other micro-organisms."7 


            It would make sense that this would be the case since the "antimicrobial" effect of ionic silver is recognized to be extremely broad-spectrum when applied to bacterial and fungal pathogens including mold and yeast.  Indeed, it would also make sense that silver would be antiviral since it is clearly recognized to be highly effective against essentially every other class of pathogen.  Phrases like "antimicrobial" and "no pathogens survive" are apparently intended to be quite broad in scope. And the mechanisms of action that make ionic silver effective as an antimicrobial are apparently unique enough to support it. 


            According to AgION, "Ancient Egyptians used it to keep food supplies safe from fungus and mold."  A press release by Curad quoted Philip M. Tierno, Ph.D., Director of Clinical Microbiology and Immunology at New York University Medical Center and author of The Secret Life of Germs (Atria Books 2004), as stating, "Silver is a natural antibacterial that works by killing bacteria, fungi and yeast by interfering with the metabolism necessary for respiration of these microbes.  It fights germs with much less fear of developing antibiotic resistance."8 


            A representative of AgION stated in a recent press release, "Silver has multiple mechanisms of action.  Use of silver as an antimicrobial is therefore unlikely to promote antibiotic resistance." 


            The FDA has regulations that prohibit the manufacturer of any product from making "disease" claims unless the product has been approved for "drug" use (the definition of a drug is any product sold for treating a disease).  Since virtually any presence of a viral pathogen in the human body will by definition be disease related, it is unlikely we would be seeing any product promoted as an antiviral until the tedious and extremely expensive process of FDA drug approval was completed - a seriously-flawed process that has seen deadly drugs like Vioxx approved while beneficial ones languish in limbo. 


            A search on the Internet, however, will turn up literally thousands of testimonials reporting viral conditions across a very broad range being successfully treated with ionic-silver products such as the outdated but still popular colloidal silver.  It is very plausible, if not highly likely, that before long ionic silver will emerge as the recognized solution for the vast majority of viral conditions. 


Delivery for Systemic Human Use 


            Back 100 years ago, major pharmaceutical firms made ionic silver products for systemic human use in the form of what is loosely referred to as "colloidal" silver, a very crude and archaic substance that did the job of delivering silver ions decently for its time.  But after the advent of far more profitable antibiotics and the change of the laws in the 1930s when the FDA as we know it today was formed, silver fell out of favor. 


            In recent decades, colloidal silver has seen a resurgence in popularity, but primarily in the alternative- or natural-medicine field, or when sold as a dietary supplement.  Meanwhile, important advances in the field of ionic-silver delivery, most notably with the Opti-Silver technology, are rapidly rendering colloidal silver obsolete.  A new, patented technology, this special form of ionic silver apparently represents a major breakthrough in the delivery of ionic silver for systemic, or internal, human use. 


            In this regard, there are two pivotal questions to be considered: (1) whether silver ions kill viral pathogens; and (2) the method of delivery for systemic human use. 


            From my perspective, the question of whether ionic silver kills viral pathogens has been answered in the resounding affirmative. It seems that only the tedious process of FDA drug approval - or perhaps a major clinical study that is planned on Opti-Silver hitting the media - is needed before it becomes common knowledge. 


            Even if silver ions are effective against viral pathogens, the delivery mechanism for use in the human body becomes the key issue.  All of the products on the market today that utilize ionic silver for its antimicrobial and tissue-healing qualities incorporate some kind of deliberate delivery mechanism in order to provide for a controlled release of silver ions at the rate that is desired.  This delivery mechanism varies relative to the environment where the ionic silver is being used and relative to the rate of release that is desired.  In each case, the medium is a factor; and, far more importantly, the complexing or compounding agent used to release the silver ion is a factor. 


            Our growing understanding of this area of controlled delivery has contributed greatly to the increase in the number of products using ionic silver for antimicrobial purposes.  The New York Times stated in a December 2005 article, "Silver, one of humankind's first weapons against bacteria, is receiving new respect for its antiseptic powers, thanks to the growing ability of researchers to tinker with its molecular structure."9  What this refers to is the deliberate formation of molecular structures that will release silver ions in the given environment at the desired rate. 


            The same article goes on to say, "But silver's time-tested - if poorly understood - versatility as a disinfectant was overshadowed in the latter half of the 20th century by the rise of antibiotics.  Now, with more and more bacteria developing resistance to antibiotic drugs, some researchers and healthcare entrepreneurs have returned to silver for another look.  This time around, they are armed with nanotechnology, a fast-developing collection of products and skills that helps researchers deploy silver compounds in ways that maximize the availability of silver ions - the element's most potent form.  Scientists also now have a better understanding of the weaknesses of their microbial adversaries." 


            This need for a delivery mechanism to maximize availability is all the more demanding when attempting delivery of ionic silver in the human body, due to the aggressive and fluctuating electrochemical environment the human organism presents.  The common substances listed in the pharmacist's desk reference in Europe, for instance, may work marginally well but lack an efficient delivery mechanism and therefore fail to unlock the potential of what ionic silver might be capable of doing in the human body to kill germs. 


            The patented Opti-Silver technology, which its maker Invision International says is the result of years of research and development, is designed to optimize the delivery of silver ions in the human body.  While the patent covers a broad range of substances, the company has chosen to use citrate as the complexing agent, and potassium as the counter-ion for maximum stability, in the water-based dietary-supplement formulation it currently sells under the brand name of Silver 100.  The recommended usage is entirely safe - the maximum recommended daily amount introduces less silver to the body than may be contained in an individual's ordinary drinking-water intake, with the difference being that it is intended to be released as activated ions. It consists of all-natural nanotechnology.10 


Treatment of Hepatitis B and C 


            I myself have had a patient come into my office at the Institute of Advanced Medicine who was diagnosed as having both hepatitis B and C.  The blood work that came back from the lab showed the viral load and liver enzymes to be extremely elevated, indicating an advanced condition.  The patient, not wanting to be subjected to the Interferon and Ribavirin treatment that was common because of the high risk of side effects and the low incidence of success, chose instead to be put on a 90-day regimen of using Silver 100, at an elevated dosing relative to normal suggested use.  Silver 100 is a water-based dietary supplement made by Invision International that contains the Opti-Silver form of ionic silver.  At the end of 90 days, the same battery of lab tests showed the blood reading to be 100% normalized, indicating the treatment was highly successful.  (See  <URL>  for a write-up of the laboratory work.  Note that the patient's readings elevated again after use of the product was stopped temporarily.) 


            My experience in seeing patients and others in the public sector use ionic silver for a variety of bacterial and viral conditions over a number of years indicates that this was not a fluke at all but was rather a predictable result of the use of ionic silver. 


            The makers of Opti-Silver claim that it represents a quantum leap in the delivery of ionic silver for the human body.  At present, they themselves, however, make no disease claims for their product. 
Silver May Outshine the Threat of Bird Flu 


            As for the bird flu "threat," well, there are many theories.  While some say a pandemic is inevitable, reasonable arguments are also made that suggest it is far from certain that it will ever be a problem - unless some evil person genetically modifies the organism to actually make it lethal to humans. 


            Some even speculate that the prospect of bird flu is more of a political issue than a medical one, claiming that it is being used to fan the flames of fear in order to persuade an unwitting public to give up liberties in order to be "protected" by the authorities. That is not my area of expertise.  I do find it curious, though, that Tamiflu is still often touted as being the treatment of choice when initial studies to date seem to indicate that it is really not an effective treatment for many viruses, least of all a potential human strain of bird flu.  If the stories that say Donald Rumsfeld has a large stake (40% seems most common) in the company that makes it, then that would obviously contribute to the suspicious nature of things. When one thinks of the massive dollars involved, well, one can tend to wonder.  Regardless, whether or not bird flu will ever pose a problem to humans is simply speculative at best today. 


            It seems that perhaps the possible solution to a bird-flu pandemic, if it were to develop, may already exist today in ionic-silver products.  And it may be the solution for a lot of things that really need an answer today. 


            According to Jay Newman, president of Invision, who is a co-member with me on the Committee for the Responsible Use of Silver in Health, hosted at www.SilverFacts.com, he was told a few years ago by the then-director of the largest financial entity in the world that, in her opinion, "The technology behind Opti-Silver has the potential to lead to a billion-dollar company and to make history changing the world of antibiotics, both from a financial point of view and a humanitarian point of view." 


            Based upon my personal observations and the proliferation of products coming out in the medical field that use ionic silver, it seems to me that that statement may prove to have been remarkably prophetic.  At the least, it would seem that this is a technology worth watching and exploring.  Ionic silver may prove to be the answer to not only a bird flu strain that may never emerge but also for a very wide range of infectious diseases that are very real and present threats to the health of the public today. 
_______________________________________________________________________ 
Herbert Slavin, M.D. is the founder and director of the Institute of Advanced Medicine, located in Lauderhill, Florida.  He is a member of the Committee for the Responsible Use of Silver in Health, which is hosted at www.SilverFacts.com and at which more information may be found.  People with medical concerns should check with their knowledgeable physicians before using any of the products mentioned in this article. 
 <URL>  


