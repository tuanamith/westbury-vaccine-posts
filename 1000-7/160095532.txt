


Via NY Transfer News Collective  *  All the News that Doesn't Fit   RHC Cuban Economy Report - , 2006 
by Luis Alberto Chirino 
* Cuban Economy Keeps Growing in 2006 
* High Demand for Cuba's Pharmaceutical Products 
Alberto Agraz, director of the company which is considered as the main pillar of Cuba4s biopharmaceutical industry, recently told reporters in Havana that the products under commercialization include a Hepatitis B vaccine, Interferon, and others which are in high demand world-wide. 
Britain4s Lloyd4s Register Quality Assurance and Cuba4s National Standardization Office granted BIOCEN the ISO 9001-2000 standards due to favourable results in its high production quality. BIOCEN is the only Cuban firm to boast such recognition, said its director. 
* Cuban Shrimp Enterprise Reports High Yield 
Cultivated shrimp sells between 3,500 and 4,700 dollars a ton on the foreign market, according to size. 
* Cuban Metal Industry Increases Exports 
Cuba4s metal industry has reported increased exports of goods and services to 22 countries. 
* Indian Oil Corporation Pens Contract with Cuba 
For us, Cuba represents a great opportunity to explore and see results, added Butola. 
ONGC currently has projects in 14 countries, including Egypt, Libya, Syria, Iraq, Iran, Russia, Viet Nam and Brazil. 

