


Gates, Wife Stress Urgency in Search for AIDS Prevention By Jia-Rui Chong Times Staff Writer 
August 14, 2006 
TORONTO - Bill and Melinda Gates, whose foundation has contributed $1.9 billion to fight AIDS, said Sunday that developing antiviral gels and pills that would allow women to protect themselves is an "urgent priority" in fighting the epidemic. 
"We want to call on everyone here and around the world to help speed up what we hope will be the next big breakthrough on the fight against AIDS - the discovery of a microbicide or an oral prevention drug that can block the transmission of HIV," Bill Gates said at the opening session of the world's largest AIDS conference. 
"The goal of universal treatment - or even the more modest goal of significantly increasing the percentage of people who get treatment - cannot happen unless we dramatically reduce the rate of new infections." 
Gates, chairman of Microsoft Corp. and of the Bill and Melinda Gates Foundation, emphasized the need for such prophylactic tools to give women the means to protect themselves. 
"No matter where she lives, who she is or what she does - a woman should never need her partner's permission to save her own life," he said. 
The Gateses pledged to increase their funding for this research, though they did not specify by how much. 
The microbicides could take the form of gels, creams or vaginal rings. Five such formulations are in advanced trials. Antiviral drugs intended for treatment after infection have already been proven to lower the risk of infection for babies born to mothers with HIV. Some of those drugs have also prevented the monkey version of acquired immune deficiency syndrome in macaques. 
Researchers at UCLA have been working on microbicides and hope to make them available in about five years. 
"If Gates put his voice behind it, that could be faster," said Edwin Bayrd, associate director of the UCLA AIDS Institute. 
Dr. Daniel Kuritzkes, director of AIDS research at Brigham and Women's Hospital in Boston, said, "It's clear a vaccine is a long way off. In the absence of vaccines, we need more effective preventive measures." 
Kuritzkes said he hoped resistance to studies that would test the effectiveness of antiviral drugs as a preventive measure had ebbed. He recalled that in 2004 at the last AIDS conference in Bangkok, Thailand, protesters were angry about a proposed study in Cambodia of these drugs, in part because they wanted a guarantee that participating women would be medically cared for after the trial. 
Melinda Gates said she hoped the World Health Organization and other international agencies would develop ethical standards for such clinical trials so they could start faster and run without interruption. 
She called for help from activists, saying they had shown they could provoke action. "You proved this when you pushed for new treatment," she said. "The world now needs you to push even harder for prevention." 

She also encouraged governments and pharmaceutical companies to join the research. 
"If all these players do their part," she said, "we will move forward, as fast as science can take us, to discoveries that can help block the transmission of HIV." 
The AIDS conference, which takes place every two years, drew an estimated 24,000 participants this year, organizers said. The crowd responded to the couple's speeches with cheers and standing ovations. 


