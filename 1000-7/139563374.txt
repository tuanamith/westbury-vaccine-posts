


 <EMAILADDRESS>  wrote: 

The vaccination was made compulsory as soon as the infrastructure for its administration to the entire population existed. Once again, the main royal achievement in this respect was pretty much that the monarch merely placed his signature on a program that had already predated him. 
The first initiative against smallpox dates back to the demographic study of the newly-established Statistics Commission in 1758, and three years later the administration of the inoculations was made the responsibility of the clergy. By 1774, the new instructions made the inoculations the responsibility of the district doctors, but they were still very few and scarce. As late as in 1803, the sextons of the congregations were still instructed to educate themselves in the administration of vaccinations, due to the shortage of professional physicians. 
A year later, the organization of vaccination programs became the responsibility of the local Economic Societies; frex. in Finland, of the Finnish Economic Society. At this point, about 25% of each age group was already being vaccinated, so much remained to be done. More professional doctors were needed, more vaccine stores were needed around the country, and also, detailed lists on the vaccinations needed to be written. So, the work proceeded, and by 1816, the number had already risen to 80% of each age group vaccinated. 
(Presumably the sudden rise of the figure was also facilitated by the removal of the backward Finnish provinces from the equation, even though I'm not sure of that.) 
Again, a long trend, underway long before the Gascon marshal showed up. 

When you include your comments on enclosure and vaccinations within a paragraph which both starts and ends with the clear and explicit argument that Bernadotte "helped to set the stage" for further positive developments, the subsequent impression is that you're listing both of these as achievements for which Bernadotte receives personal credit; perhaps indirectly, but nonetheless. 
Hey, I'm not a telepath. If you write something that's confusing and/or admits of multiple interpretations, and I take an interpretation that's not the one you intend... well, whose fault is that? 

Different times, different factors; different rules, different interpretations. 

Blessed aircrafts, eh? 

Hey, I don't _want_ it, but I guess I have no choice but to take it. 


Cheers, Jalonen 


