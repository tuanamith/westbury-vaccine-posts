


 <URL>  
Researcher Issues Caution On Live Virus Vaccines 6/18/2004 

Source: New York Medical College 

A New York Medical College microbiologist warns that live virus vaccines to prevent infectious diseases like West Nile virus and yellow fever could have dire consequences. Should one of the vaccine flaviviruses recombine with a wild-type virus, a new microbe with potentially undesirable properties could result, according to Stephen J. Seligman, M.D., research professor of microbiology and immunology. 

His paper, "Live flavivirus vaccines: reasons for caution" appears as a rapid review article in the June 19, 2004, issue of the journal Lancet. Co-author is Ernest A. Gould, Ph.D., of CEH-Oxford in the UK. 

Flaviviruses, which can recombine within species and may recombine between species, also include dengue, Japanese encephalitis and tick-borne encephalitis. They cause substantial sickness and death each year. Although live-virus vaccines offer great promise in terms of cost and efficacy, their use should be approved by an international authority due to safety concerns, the authors write. 

The article lists five main lessons learned from other live virus vaccines: reversion of vaccine strains to increased virulence; development of disease in individuals with compromised immune systems; birth defects, particularly if the vaccine is given in the first trimester; spread of vaccine strains to unvaccinated persons and; the discovery of new, previously undetermined complications. __________________________________________ 

source:  Immunization- Theory Versus Reality by Neil Miller 

How Are Vaccines Made? 

"Vaccine production is a disgusting procedure. To begin, one must first acquire the disease germ  -- a toxic bacterium or a live virus. To make a "live" vaccine, the live virus must be attenuated, or weakened for human use. This is accomplished by serial passage -- passing the virus through animal tissue several times to reduce its potency. For example, measles virus is passed through chick embryos, polio virus through monkey kidneys, and the rubella virus through human diploid cells -- the dissected organs of an aborted 
fetus!(49-51) "Killed" vaccines are "inactivated" through heat, radiation, or chemicals.(52) 

"The weakened germ must then be strengthened with adjuvants (antibody boosters) and stabilizers. This is done by adding drugs, antibiotics, and toxic disinfectants to the concoction: neomycin, streptomycin, sodium chloride, sodium hydroxide, aluminum hydroxide, aluminum hydrochloride, sorbitol, 
hydrolized gelatin, formaldehyde, and thimerosal (a mercury derivative).(53,54) 

"Aluminum, formaldehyde, and mercury are extremely toxic substances with a long history of documented hazardous effects. Studies confirm again and again that microscopic doses of these substances can lead to cancer, neurological damage, and death.(55-58) Yet, each of them may be found in childhood vaccines. 


"In addition to the deliberately planned additives, unanticipated matter may contaminate the shots. For example, during serial passage of the virus through animal cells, animal RNA and DNA -- foreign genetic material  -- is transferred from one host to another. Because this biological matter is injected directly into the body, researchers say it can change our genetic makeup.(59-61) 


"Undetected animal viruses may jump the species barrier as well. This is exactly what happened during the 1950s and 1960s when millions of people were infected with polio vaccines that were contaminated with the SV-40 virus undetected in the monkey organs used to prepare the vaccines.(62-69) SV-40 (Simian Virus #40 -- the 40th such virus detected since researchers began looking),(70) is considered a powerful immunosuppressor and trigger for HIV, the name given to the AIDS virus. It is said to cause a clinical condition similar to AIDS, and has been found in brain tumors, leukemia, and other human cancers as well. Researchers consider it to be a cancer-causing virus.(71,72)" 

     You can't test for something you don't know exists.  I think that Lyme disease has been around for a very long time.  Why are people's immune systems having so much trouble fighting it now ?  There are too many young people that are very ill. ______________________ 

MEASLES VACCINE: 

Thrombocytopenic Purpura Following Vaccination With Attenuated Measles Virus Amer J Dis Child Jan 1968 Vol 115 [3pgs] 

 Facial Palsy Following Measles Vaccination a Possible Connection [1pg] 
Investigation of a measles outbreak in a fully vaccinated school population including serum studies before and after revaccination (Pediatr Infec Dis J 1993 12) [8pgs.] Risk of Aseptic Meningitis after Measles, Mumps , and Rubella Vaccine in UK Children 

Lancet 1993 Vol 341 [4pgs] Failure of Measles Vaccine Sprayed into the Oropharynx of Infants 

(this is on an inhaled vaccine not shot vaccine using the EZ strain) Lancet May 1983 [1pg] 

 High Titre Measles Vaccine Dropped (this is on the Experimental E-Z Measles vaccine) 

Lancet 1992 Vol 340 [1pg] 

 Failure to Reach the Goal of Measles Elimination Arch Intern Med 1994 vol 154 [6pgs] 

 A Measles Outbreak at a College with Prematriculation Immunization Requirements 

Am J Of Pub Health Vol 81 no 3 [4pgs] 

An Explosive point-source measles outbreak in a highly vaccinated population (American Journal of Epidemiology 1989 Vol 129 no 1) [10] 

 Atypical measles in children previously immunized with attenuated measles virus vaccines 

(PEDIATRICS VOL 50 NO 5) [6pgs] 

 Neurological disorders Following Live Measles-Virus Vaccination (JAMA March 1973, Vol 223 No 13) [4pgs] 

 A Persistent Outbreak of Measles Despite Appropriate Prevention And Control Measures (American Journal of Epidemiology Vol 126 No3) [13pgs.] 

 Exaggerated Natural Measles Following attenuated Virus Immuization (PEDIATRICS 1976 VOL 57 NO 1) [3pgs.] 

 Child Mortality After High-Titre Measles Vaccines (EZ measles) Lancet Vol 338 1991 [4pgs] 

 Measles Vaccine and Crohn's Disease Gastroenterology vol 108 no 3 1995 [3pgs] 

Severe Hypersensitivity or Intolerance Reactions To Measles Vaccine In Six Children 

(ALLERGY 1980 35) [7] 

 Pathogenesis of Encephalitis Occurring with Vaccination , Variola and Measles Arch of Neurology and Psychiatry 1938 Vol 39 [8pgs] 

 Aseptic Meningitis after Vaccination Against Measles and Mumps (Pediatr Infec Dis J 1989 8 pg 302-308) [7pgs] 

 Optic Neuritis Complicating Measles, Mumps, and Rubella Vaccination American Journal of Opthalmology 1978 :86 [4pgs.] 

 Measles Vaccine Associated Encephalitis in Canada Lancet Sept. 1983 [2pgs 

Guillain -Barre Syndrome Following Administration of Live Measles Vaccine Amer J of Med 1976 Vol 60 [3pgs] 

summary: In a 19 month old girl and a 16 month old girl the gullian barre syndrome developed within a week after they received, respectively, live measles-rubella vaccine and live measles vaccine. The older child was immune to rubella at the time of vaccination, but both girls demonstrated a primary measles antibody response. Serum obtained during the acute and convalescent stages from the younger child was tested for antibodies against the herpes virus, epstein barre virus, cytomeglovirus and varicella -zoster and found to be negative. the author goes on to state vaccine and wild strains can in the pathological process lead to demyelinzation. These two cases again emphasize the need to carefully document the neurological diseases which follow infections with live virus vaccines. ________________________________________ Immunization- Theory Versus Reality by Neil Miller 

 "According to Dr. Robert Gallo, the chief AIDS researcher at the National Cancer Institute. The use of live vaccines such as that used for smallpox can '  activate ' a dormant infection such as HIV. In fact, the greatest spread of HIV infection coincides with the most intense  and recent smallpox vaccination campaigns.  . Shortly after the link between smallpox vaccines and AIDS was publicly revealed, WHO scientists indicated their desire to abolish the world's last remaining vials of smallpox.  Although the claim smallpox was eradicated and we can no longer risk an accidental reinfection, others question whether valuable evidence is being destroyed.(102)" 


