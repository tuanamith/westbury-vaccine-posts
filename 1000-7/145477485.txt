



Under-5 mortality rate (1990)  13 
Under-5 mortality rate (2004)  7 
Infant mortality rate (under 1) (1990)  11 
Infant mortality rate (under 1) (2004)  6 
Life expectancy at birth (years) (2004)  78 
Total adult literacy rate (2000-2004*)  100 
Net primary school enrolment/ attendance (%) (1996-2004*)  93 
% of infants with low birthweight (1998-2004*)  6 
% of under-fives (1996-2004*) suffering from: underweight (moderate)  4 
% of under-fives (1996-2004*) suffering from: underweight (severe)  0 
% of under-fives (1996-2004*) suffering from: wasting (moderate & severe)  2 
% of under-fives (1996-2004*) suffering from: stunting (moderate & severe)  5 
Health 
% of population using adequate sanitation facilities (2002) (total)  98 
% of population using adequate sanitation facilities (2002) (urban)  99 
% of population using adequate sanitation facilities (2002) (rural)  95 
% of routine EPI vaccines financed by government (2004) total  99 
Immunization 2004: 1-year-old children immunized against: Tuberculosis (TB) (BCG)  99 
Immunization 2004: 1-year-old children immunized against: Diphtheria, pertussis and tetanus (DPT1)  89 
Immunization 2004: 1-year-old children immunized against: Diphtheria, pertussis and tetanus (DPT3)  88 
Immunization 2004: 1-year-old children immunized against: Polio (polio3)  98 
Immunization 2004: 1-year-old children immunized against: Measles (measles)  99 
Immunization 2004: 1-year-old children immunized against: Hepatitis B (hepB3)  99 
Immunization 2004: 1-year-old children immunized against: Haemophilus influenzae type B (Hib3)  99 
HIV AIDS 
HIV Prevalence: Adult prevalence rate (15-49 years), end 2003, estimate  0.1 
Adult literacy rate, 2000-2004*, male  100 
Adult literacy rate, 2000-2004*, female  100 
Life expectancy, 1970  70 
Life expectancy, 1990  74 
Life expectancy, 2004  78 
Adult literacy rate: females as a % of males, 2000-2004*  100 
Enrolment ratios: females as a % of males, primary school (2000-2005*), gross  96 
Enrolment ratios: females as a % of males, primary school (2000-2005*), net  99 
Enrolment ratios: females as a % of males, secondary school (2000-2004*), gross  98 
Enrolment ratios: females as a % of males, secondary school (2000-2004*), net  100 
Antenatal care coverage (%), 1996-2004*  100 
Skilled attendant at delivery (%), 1996-2004*  100 
Maternal mortality ratio=86 , 1990 - 2004*, reported  34 
Maternal mortality ratio=86 , 2000, adjusted  33 
Maternal mortality ratio=86 , 2000, Lifetime risk of maternal death. 1 in:  1600 
Child Protection  to the top 
Average annual rate of reduction (%) (1990-2004) [under 5 mortality rate]  4.4 =20 Reduction since 1990 (%)  46 


