


JohnDoe wrote: 

No, it describes a demonized version of Western medicine, and cherry picks a  made-up example of how people handle it. Notice that in the real world, if  Western medicine didn't actually work, the screaming would come much faster.  Mr. "old man in the woods" would simply be asked to demonstrate the  effectiveness of his approach. Of course, the admittedly cute little cartoon  doesn't have a damn thing to do with hoomeopathy: it has to do with  preventive medicine and naturopathy, which is a whole different ball of wax,  and worshipping at the feet of a guru who rips you off. 
Homeopathy is classically defined as "treating sick persons with therapeutic  agents [drugs, remedies] that are deemed to produce similar symptoms in a  healthy individual". In some cases, this is silly as hell, and you get  exactly the same sort of fraud in homeopathy as described for allopathic  medicine in the cartoon. There have been some uses of homeopathy, though:  vaccines and allergy desensitization work this way. But the "dilute it an  even number of times until there's not a molecule left of hte medicine and  the mystical spirit energies help you" is just damned silly. You get frauds  who are even *WORSE* in homeopathy. 
It is demonstrably true that for many problems, like obesity, stress, and  repetitive stress injuries, a more naturopathic approach works better, by  helping eliminate the causes. But that has almost *nothing* to do with  homeopathy. It's naturopathy: using the body's own innate capabilities to  improve your life. And for a whole statck of reasons, it's getting more  common in medical schools and among the more allopathic trained personnel.  Hey, it worked for Hippocrates, it still has good uses. 
So far, so good, but when you look at the actual treatment provided by a lot  of uneducated, guru-like naturopaths, you basically find people trying to  build a cult. They discredit the people doing real work and providing  demonstrable results, and verifying results is amazingly helpful. The  ability to measure and throw out ineffective tools helps reduce a random  conglomeration of herbs that worked on your grandma's chest pain to an  inhaler with a known dosage that helps a 10 year old's asthma, with dosages  and warnings about side effects, and some knowledge that it should not be  taken with some other drugs. 
I'd rather have the side effects known: that takes allopathy, to measure and  report the other effects.  



