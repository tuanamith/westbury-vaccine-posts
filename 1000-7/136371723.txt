




A bird flu vaccine for humans that uses only a very low dose of active  ingredient has proved effective in clinical tests and could be mass produced  in 2007, claims its maker GlaxoSmithKline Plc. Europe's biggest pharmaceuticals group said it was on track to start  manufacturing by the end of 2006 and could make hundreds of millions of  doses next year, assuming the product is approved by regulators. 
It will probably cost around $7.40 and Glaxo is talking to groups like the  Bill & Melinda Gates Foundation and the Global Fund to Fight AIDS, TB and  Malaria about funding it in poor countries. 
Glaxo believes its H5N1 vaccine will work more efficiently than rival ones  in development because of the proprietary adjuvant used in its manufacture. 
Adjuvants are additives put into vaccines that boost the immune system and  make it respond more efficiently. 
A key challenge in the race to produce a vaccine for millions of people  around the world, which governments are keen to stockpile, is how to make  the maximum number of shots from the minimum amount of antigen, or active  ingredient. 
Antigen is produced in chicken eggs in a slow and laborious process. 
Glaxo's vaccine contains just 3.8 micrograms of antigen, yet more than 80  per cent of healthy adult volunteers who received two doses had a strong  immune response. 
That level of protection meets or exceeds requirements set by regulatory  agencies for approving new flu vaccines. 
'Significant' 
Glaxo chief executive Jean-Pierre Garnier said it was a "significant  breakthrough". 
"All being well, we expect to make regulatory filings for the vaccine in the  coming months," he said. 
In May, manufacturer Sanofi-Aventis reported good responses with a vaccine  using a conventional adjuvant given at two doses of 30 micrograms. 
But when the dose was reduced to 7.5 micrograms, only 40 per cent of people  were protected. 
While Glaxo's vaccine offers protection against the deadly H5N1 avian flu  virus now circulating, its impact on any mutated strain of virus is not  certain. 
However, experts say it could "prime" a person's immune system so they will  get stronger effects from a later, better-matched vaccine. 
Glaxo said it would now also study the ability of its vaccine to offer  cross-protection to variants of the H5N1 virus. 
Deutsche Bank analysts said an H5N1 vaccine could have sales potential of $2  billion a year, which would add 3.5 per cent to Glaxo's long-term earnings. 
The H5N1 strain of avian influenza has spread rapidly out of Asia and has  killed more than 130 people who have come into close contact with infected  birds. 
Experts fear it could trigger a pandemic, a global epidemic of flu that  could kill millions, if it acquires the ability to pass easily from human to  human. 
Companies are racing to develop pandemic H5N1 vaccines that could save lives  and buy time to develop a vaccine against a pandemic strain. 
It could take from four to six months from the start of a pandemic before a  specific vaccine will be ready. 
-Reuters 



