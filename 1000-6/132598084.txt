


Jason Johnson wrote:   > Bryan, 
I should point out that the only research I've done on autism is reading  the scientific literature.  My full-time job doesn't involve autism;  rather, I'm an immunologist who studies aspects of inflammatory  responses in healthy people, as well as in septic and HIV patients. 


Yes, but: 
1) Contamination was methyl mercury, not the form which is in vaccines.    Ethyl mercury (what thimerisol breaks down into) is a very different  chemical, with different toxicology and different effects on our body. 2) Neurological defects were not like those seen in the autistic, nor  were the symptoms anything like autism. 
Keep in mind that neurological damage doesn't always lead to the same  things.  Depending on the form of damage you can experience all kinds of  symptoms - from memory loss, to intelligence loss, to loss of motor  control, to loss of emotional control, to paranoia, to learning  disabilities, to language problems, and many more.  The symptoms of  autism do not match those of mercury poisoning.  In and of itself a lack  of similar symptoms wouldn't be enough to disprove a link between autism  and mercury, but given all of the other data out there destroying the  link, it amounts to another nail in the coffin. 



Problem is that kids without autism have similar levels of mercury in  their systems.  So why don't they have autism, if they have similar  mercury levels?  This is why so many of these studies qualify as bad  science - if you don't look at non-autistic, or compare your group to a  group of people who didn't receive mercury-containing vaccines, how can  you make any conclusions at all? 
Also, some forms of mercury testing (i.e. hair) are notoriously  inaccurate.  Keep that in mind when reading some of those papers.  Blood  testing is much more accurate, if preformed properly. 



Who says I discount them?  In many cases (for both the pro-autistic  papers and anti-autistic papers) there are huge holes in their  scientific methodology.  The original paper linking mercury to autism  being a classic example; I would have been embarrassed to have my name  linked to that. 
If a paper passes the methodology/stats test, then I tend to consider  its results valid.  This leaves a lot of papers showing no link b/w  autism and mercury, but still leaves a few which show a link.  At this  point I rely on two things - the scientific consensus (which right now  is no link), as well as the overall conclusions of the studies.  At this  point the only evidence which links mercury to autism is correlative,  which is the weakest form of proof (correlation cannot prove a link,  although a lack of correlation can disprove a link).  In the case of the  no-link to autism papers, the data is much more robust.  There are  animal experiments showing that continued exposure to ethyl mercury  doesn't do much at the doses people receive.  Then there are human  studies showing that autism rates stay the same in populations where  mercury is removed from vaccines, as well as a few of those weaker  correlation studies I mentioned above. 
Overall, the scientific literature screams "no link".  There's only a  few, faint voices left saying there is a link. 



The nature of the mental defects resulting in autism vs. mercury  poisoning are very different.  Mercury poisoning tends to result in  nervousness/paranoia, trembling and dementia.  Autism is characterized  by a poor interpersonal skills and communication defects.  Very, very  different symptomology.  So it is unlikely that there is a link - if  mercury was causing damage to infants I would have expected damage  similar to that seen in adults. 
Bryan 

