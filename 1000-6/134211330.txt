


Ellen Goodman: 'The right gives itself a wedgie' 
Ellen Goodman, The Seattle Times 
BOSTON - So once more we reach into the right-wing toolbox, a political chest so spare that it holds almost nothing but a wide assortment of wedges. Who would have believed that the wedges used so successfully to divide America would end up dividing conservatives? That they would finally expose the differences between the right and the, um, loony right? 
The latest of these wedge issues is stem-cell research. But it's not the only one. Over the past year, we've begun to see daylight emerge between common sense and nonsense. 
Wedge One: Abstinence or Death. Remember last October, when the vaccine against HPV - the leading cause of cervical cancer - was first announced? Pro-family groups were less than enthusiastic about this breakthrough. Cervical cancer was, after all, a mainstay of the abstinence-only miseducation textbooks. A vaccine, said the Family Research Council's Tony Perkins, "sends the wrong message." The far-right message was that losing your virginity could give you cancer. 
Today, the FRC and its cohorts still oppose routine vaccination for girls. But after the sex-or-death brouhaha, they were compelled to regroup and offer choked approval of a "tremendous medical achievement." 


Wedge Two: South Dakota or Bust. In February, South Dakota showed the country what a pro-life America would really look like. The Legislature passed a law directly confronting Roe v. Wade, banning all abortions except to save the life of the pregnant woman. 
This fulfilled the infamous views of Bill Napoli, the state senator who could publicly imagine only one rape victim who could qualify for an abortion: a religious girl, planning "on saving her virginity until she was married" who "was brutalized and raped, sodomized as bad as you can possibly make it, and is impregnated." A law like that may be too much even for South Dakota; a measure to repeal the ban is on the ballot. 
Wedge Three: Plan B or Else. A cowed FDA has still not allowed emergency contraception onto the local drugstore shelf. Plan B could prevent thousands of unwanted pregnancies if it were available over the counter. But opponents argue that the morning-after pill would change the night-before behavior if it fell into the hands of young teenagers. They also claim Plan B could conceivably stop a fertilized egg from getting into the womb. 
Go figure. Easy access to contraception that could prevent thousands of abortions is being delayed because of claims it could stop a fertilized egg from getting implanted ... and probably aborted? More and more people are hearing the loony tunes behind that logic. 
Now back to the big one. 
Wedge Four: The Frozen Embryo or the Seriously Ill. This week the Senate wrangled over a bill to expand federal funding for research using leftover embryos from fertility clinics. The debate was between the value of the potential life of an embryo and the actual life of a sick person. 
Opponents of the bill brought forth "Snowflakes" in July, a handful of the 100-plus children born from donated frozen embryos. Proponents brought in patients who hope for cures and reminded people that 400,000 spare embryos would never find wombs. One side talked about the "innocent human life" of an embryo. The other talked about the "innocent victim" of disease or accident. 
But then something unusual happened. Republicans and pro-lifers split. Orrin Hatch and John McCain voted for the bill. So did Senate Majority Leader Bill Frist. The Senate voted in favor of expanding stem-cell research by 63-37. 
About 70 percent of Americans favor this research. But way beyond that, stem-cell research has become an issue prompting people to draw new lines. Yes, between the right and the, um, loony right. 
When the president surrounded himself with 18 families with "adopted" embryos and stamped this bill with his first-ever veto, he set back the science another year. Case closed. But when he described this research as "the taking of innocent human life," he placed the Oval Office on the far side of the new line. 
Values voters, anyone? They may yet qualify as the most oversimplified, overused demographic of the 2004 election. But today there are a lot of Americans, secure in their own values, wondering: What kind of people would choose a leftover frozen embryo over a cure for my cousin's diabetes? Pro-whose-life? 
Politicians have become accustomed to bowing to the right. Many, especially in red states, worry about their vulnerability to those values voters. Now we are looking at a subtly changing landscape. The right wing has performed its own incredible alchemy. They've finally turned a wedge into a double-edged sword. 
Ellen Goodman's column appears Friday on editorial pages of The Times. Her e-mail address is  <EMAILADDRESS>  
Source: The Seattle Times Company  <URL> / opinion/2003141109_goodman21.html 






