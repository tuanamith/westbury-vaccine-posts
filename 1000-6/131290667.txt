


There is no complete list because the list would be to long to publish.  Unlike the list of black inventors, which qualifies as one of the 3  shortest books on earth (after the Ethiopian cookbook and the list of  Arab war heors), white people have invented basically everything you see  today. If you just start with inventors and leave composers, artists,  and philosophers out there'd be already then thousands. I give you just  an random list for the letters A, B and C: 
Edward Goodrich Acheson Received a patent for carborundum -the hardest man-made surface and was  needed to bring about the industrial age. 
Thomas Adams The history of how Thomas Adams first tried to change chicle into  automobile tires, before making it into a chewing gum. 
Howard Aiken Worked on the Mark computer series. An in-depth feature from the  "History of Computers". 
Ernest F. W. Alexanderson The engineer whose high-frequency alternator gave America its start in  the field of radio communication. 
George Edward Alcorn Alcorn invented a new type of x-ray spectrometer. 
Andrew Alford Invented the localizer antenna system for radio navigation systems. 
Randi Altschul Randice-Lisa Altschul invented the world's first disposable cell phone.  The history of cell phones. 
Luis Walter Alvarez Received patents for a radio distance and direction indicator, a landing  system for aircrafts, a radar system for locating planes and the  hydrogen bubble chamber, used to detect subatomic particles. 
Virgie Ammons Invented a firepace dampening device. 
Dr. Betsy Ancker-Johnson The third women elected to the National Academy of engineering.  Ancker-Johnson holds US patent #3287659. 
Mary Anderson Anderson patented the windshield wipers in 1905. 
Virginia Apgar Invented a newborn scoring system called the "Apgar Score" for assessing  the health of newborn infants. 
Archimedes The history of Archimedes, a mathematician from ancient Greece. He  invented the Archimedes screw (a device for raising water). 
Edwin Howard Armstrong Invented a method of receiving high-frequency oscillations, part of  every radio and television today. 
Barbara Askins Developed a totally new way of processing film. 
John Atanasoff Determining who was first in the computing biz is not always as easy as ABC. 
Charles Babbage English mathematician that invented a precursor to the computer. 
George H. Babcock Received a patent for the water tube steam boiler, a safer and more  efficient boiler. 
John Backus The first high level computer programming language, Fortran was written  by John Backus and IBM. 
Leo Baekeland & Plastic Leo Hendrik Baekeland patented a "Method of Making Insoluble Products of  Phenol and  Formaldehyde". Research plastic history, uses for and the  making of plastic, plastic in the fifties, and visit an online plastic  museum. 
John Logie Baird Remembered for the mechanical television (an earlier version of  television) Baird also patented inventions related to radar and fiber  optics. 
Benjamin Banneker His inventive spirit would lead Banneker into publishing a Farmers' Almanac. 
Robert Banks Robert Banks and fellow research chemist Paul Hogan invented a durable  plastic called Marlex®. 
John Bardeen Received a patent for the transistor invented in 1947. 
Frédéric-Auguste Bartholdi - Statue of Liberty Earned U.S. Patent #11,023 for a "Design for a Statue". 
Earl Bascom Earl Bascom invented and manufactured the rodeo's first one-hand  bareback rigging. 
Patricia Bath The first African American woman doctor to receive a patent for a  medical invention. 
Alfred Beach Editor and co-owner of "Scientific American", Beach was awarded patents  for an improvement he made to typewriters (1857), for a cable traction  railway system (1864) and for a pneumatic transit system for mail and  passengers (1865). 
Andrew Jackson Beard Received a patent for a railroad car coupler and a rotary engine. 
Arnold O. Beckman Invented an apparatus for testing acidity. 
George Bednorz In 1986, K. Alex Müller and Johannes Georg Bednorz invented the first  high-temperature superconductor. 
S. Joseph Begun Patented magnetic recording. 
Alexander Graham Bell Bell and the telephone -- the history of the telephone and cellular  phone history. 
Vincent Bendix Automotive and aviation inventor and industrialist. 
Miriam E. Benjamin Ms. Benjamin was the second black woman to receive a patent. She  received a patent for a "Gong and Signal Chair for Hotels". 
Willard H. Bennett Invented the radio frequency mass spectrometer. 
Karl Benz On January 29, 1886, Karl Benz received his first patent for a crude  gas-fueled car. 
Emile Berliner Invented the disk gramophone. The history of the gramophone. 
Tim Berners-Lee Invented the World Wide Web and HTML or hypertext markup language. 
Clifford Berry Determining who was first in the computer biz is not always as easy as ABC. 
Henry Bessemer An English engineer who invented the first process for mass-producing  steel inexpensively. 
Patricia Billings Invented a indestructible and fireproof building material--Geobond®. 
Edward Binney Co-invented Crayola Crayons. 
Gerd Karl Binnig Co-invented the scanning tunneling microscope. 
Forrest M. Bird Invented the fluid control device; respirator and the pediatric ventilator. 
Clarence Birdseye Invented a method to make commercial frozen foods. 
Harold Stephen Black Invented the wave translation system that eliminates feedback distortion  in telephone calls. 
Henry Blair The second black man issued a patent by the United States Patent Office. 
Lyman Reed Blake An American who invented a sewing machine for sewing the soles of shoes  to the uppers. In 1858, he received a patent for his special sewing machine. 
Katherine Blodgett Invented the non-reflecting glass. 
Bessie Blount Invented a device to help a disabled person eat. 
Baruch S. Blumberg Co-invented a vaccine against viral hepatitis and developed a test that  identified hepatitis B in blood sample. 
Joseph-Armand Bombardier Bombardier developed in 1958 the type of sport machine that we know  today as a "snowmobile". 
Sarah Boone An improvement to the ironing board (U.S. Patent #473,653) was invented  by African American Sarah Boone on April 26, 1892. 
Eugene Bourdon In 1849, the Bourdon tube pressure gauge was patented by Eugene Bourdon. 
Robert Bower Invented a device that provided semiconductors with more speed. 
Bill Bowerman - Sneakers Co-invented the modern athletic shoe. 
Herbert Boyer Considered the founding father of genetic engineering. 
Otis Boykin Invented an improved "Electrical Resistor" used in computers, radios,  television sets, and a variety of electronic devices. 
Louis Braille Invented braille printing. 
Joseph Bramah A pioneer in the machine tool industry. 
Dr. Jacques Edwin Brandenberger Cellophane was invented in 1908 by Brandenberger, a Swiss textile  engineer, who came up with the idea for a clear and protective,  packaging film. 
Walter H. Brattain Co-invented the transistor - invented in 1947. 
Karl Braun Electronic television is based on the development of the cathode ray  tube that is the picture tube found in modern television sets. German  scientist, Karl Braun invented the cathode ray tube oscilloscope (CRT)  in 1897. 
Allen Breed Patented the first successful car air bag. 
Charles Brooks C. B. Brooks invented an improved street sweeper truck. 
Phil Brooks Patented the a "Disposable Syringe". 
Henry Brown Patented a "receptacle for storing and preserving papers" on November 2,  1886. It was special in that it kept the papers separated. Perhaps an  early forerunner to the Filofax? 
Rachel Fuller Brown Invented the world's first useful antifungal antibiotic, Nystatin. 
John Moses Browning Prolific gun inventor known for his automatic pistols. 
Luther Burbank Holds agricultural patents on different types of potatoes (Idaho),  peaches etc. 
Joseph H. Burckhalter Co-patented first antibody labeling agent. 
William Seward Burroughs Invented the first practical adding and listing machine. 
Nolan Bushnell Invented the video game Pong and is perhaps the father of computer  entertainment. 
Vinton Cerf Invented Internet protocols. 
Emmett W Chappelle A noted biochemist, photobiologist, and astrochemist. 
William Hale Charch Moisture proof cellophane. 
John B Christian Invented and patented new lubricants, used in high flying aircraft and  NASA space missions. 
Josephine Garis Cochran In 1886, Cochran invented the dishwasher in Shelbyville, Illinois. 
Adam Cohen Invented the "electrochemical paintbrush", nanotechnology used in  etching microchips. 
Stanley Cohen The founding father of genetic engineering. 
Harry A. Cole Invented Pine-Sol in 1929. 
Samuel Colt Inventor of the colt revolver. 
Frank B Colton Invented Enovid - the history of the first oral contraceptive. 
Lloyd H Conover Invented the antibiotic tetracycline, the most prescribed broad-spectrum  antibiotic in the history of the United States. 
William D Coolidge Invented the X-Ray tube - the history of the "Coolidge Tube". 
Martin Cooper Inventor of the modern cell phone. 
Peter Cooper The history of the inventor of the Tom Thumb locomotive and Jello. 
Martha J Coston Recieved a patent for the pyrotechnic signaling system known as maritime  signal flares. 
Donald Cotton Invented propellants for nuclear reactors. 
Frederick G Cottrell Invented a electrostatic precipitator called the 'Cottrell' that removed  particles/pollution smoke or gases in smokestacks. 
Ed Cox Ed Cox invented a pre-soaped pad with which to clean pots. 
Joseph Coyetty Designed and sold toilet paper. 
Seymour Cray Invented the Cray Supercomputer - the history of supercomputers. 
David Crosthwait Crosthwait holds thirty-nine patents for heating systems and temperature  regulating devices. He is known for creating the heating system in New  York City's famous Radio City Music Hall. 
Dianne Croteau Invented Actar 911, the CPR mannequin. 
Marie Curie Also known as Madame Curie - discovered radium and furthered x-ray  technology. 
Marvin Camras His famous inventions are used in modern recording heads, magnetic sound  for motion pictures, tape machines and video tape recording decks. 
Chester F Carlson Received a patent for electrophotography, the history of the Xerox or  photocopy machine. 
Wallace Hume Carothers A brilliant and tragic mind, Carothers was the brains behind Dupont and  the history of synthetic fibers. 
Willis Carrier Brought us the comfort zone with "Air Conditioning." 
George Carruthers Behind the invention of the far-ultraviolet camera and spectrograph. 
Alexander J Cartwright The game of baseball was invented by Cartwright. 
Edmund Cartwright A cleric and the inventor of the power loom. 
Benjamin Carson A pioneer in surgery technology. 

