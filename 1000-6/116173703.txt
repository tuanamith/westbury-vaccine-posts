



Earl Evleth wrote: 
e, 
The problem is that you view economic output as some undifferentiated pie with no concern for who made what or how it got there.  The pie doesn't magically appear to be shared; it is created in differing amounts by labor and investment.  If we keep the market free there is no need for a top down decision about how to split the pie because the actors involved will be behaving voluntarily; no need for government intervention. 

You think the labor situation is comparable now to the one Marx chronicled? :) 

Which is a huge problem for Marxism.  The developed capitalist societies didn't have the revolt he predicted as the path of their natural development.  It was the agrarian countries that went Commie. 

So are you disagreeing that Malthus was wrong or not?  Free markets don't drive destroying ecologies.  Primary ownership of natural resources is unrelated to whether or not government intervention is allowed in a trade market.  Don't mix your concepts. 

I wish Julian Simon was alive to make a bet with you. 

Haha!  You're an unreformed Marxist!  Don't let the facts get in your way or anything.  Sure, I can make anything fit a Marxist analysis; most of it is meaningless.  You can derive anything from meaningless propositions. 

Your proof that anything you've cited could NOT have happened in a free market is what? 

In what manner would you call the US market "free"?  When was it free to demonstrate that it had to be controlled to produce the effects you wanted?  Perhaps vaccines would be higher profit without market intervention?  Perhaps the atomic bomb wasn't even needed to be produced? 

Trades to mutual benefit don't happen now?  That is simply silly. Every time rational actors trade it benefits them both. 

What kind of overly broad generalization is this?  Sure some are, some aren't.  It depends on the company they work for.  Overall, I'd say yes in the US because we don't have broad nation-wide strikes and deep structural unemployment, but I am sure your Marxist analysis can cook some reason up for why we're in the worst position in 500 years. 

The US has never HAD a total market economy.  Importantly, anti-trust legislation was in place in the 1800's and the Federal Reserve, a chief tool of market intervention, was created just PRIOR to the Great Depression.  Get your facts straight. 
 > > Is the whole ian 
Total bullshit.  Only a left-wing mouthpiece would claim that rational self-interest leads to the war of all against all. 1) Adam Smith noted, quite rightly, that the maximum benefit was produced by pursuit of self-interest.  2)  I am well aware of the fact that my individual interest lies in a well-functioning healthy society. 
You offer a false dichotomy of self-sacrifice vs. self-interest. Humans are social animals and their self-interest cannot be held separate from that fact. 

Wrong, the above demonstrates the moral and rational foundation of capitalism. When some business refuses to lower costs another supplants it due to individual consumer choice.  People don't want to pay more rather than less.  This is not a demonstration of immorality but of rationality on the part of consumers.  The business that ignores market forces ignores the nature of humanity to its own peril; it will lose out to competitors.  To put it another way, why should the workers of the factory that makes X be allowed to dictate to everyone else what wage they will be paid irrespective of what wage other people are willing to produce X for? 
ight aw 
HAHA!  Yes, the people who have jobs they couldn't create on their own are truly victims.  See Earl this is why I can't discuss much with you.  You have an inverted morality or at least your rhetoric is nonsensical.  I come to a business and ask for employment and they victimize me by offering me cash in exchange for my time?  Ludicrous. 

Where the workers are good AND cheap.  Even  you must realize that this situation will not maintain in the status quo.  Wages will rise even in the third world, which I would have assumed you would think was fair and egalitarian, and then overseas won't be so cheap any longer. 

Another reason may be the numbers of jobs that have become salaried to avoid hour restriction in labor law. 

IOW you can't.  I've yet to meet a Marxist who could. 

Simple human nature requires the consumption of goods; is the world enslaving us?  The term "wage slave" is why Marxist are inauthentic liars.  All the choice in the world exists.  The person could join a commune, scavenge from trash cans, make their own business, go rob someone, do whatever they want to avoid accepting someone else's offer of employment and keep on living.  If an offer of employment is better in some worker's eyes than his alternatives, he has not been enslaved. He's gotten a better offer than he would have had otherwise. This is the bait and switch rhetoric that Marxism has foisted upon the world. Wow do I hate this thinly veiled short-sighted self interest masquerading as concern for the proles.  As soon as this tripe got in place in the USSR it became apparent just how well public property ownership works. 


