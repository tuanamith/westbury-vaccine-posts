


 <URL>  
Autism rate in children has doubled, say doctors By Sam Lister, Health Correspondent 
THE number of children suffering from autism and related disorders may be twice as high as thought, with the condition likely to affect one per cent of British children. 
A study by scientists at Guy¹s and St Thomas¹ hospitals indicates that the prevalence of autism is much higher than current rates suggest. They said that improved diagnosis, a broader definition of the condition or an increase in the number of children affected could be responsible for the rise. 
Autism is a lifelong disability affecting the way that a person communicates and relates to other people. All people with autism have impaired social interactions, communication and imagination. 
People with related disorders, known as autism spectrum disorders (ASDs), usually have a mixture of autistic features. The doctors said that accurate estimates of ASD prevalence were essential for the planning of educational, social and medical services. 
Before the 1990s, researchers estimated that there were four to five cases of autism per 10,000 people, but recent reports suggest that those figures are far too low. 
Gillian Baird, of Guy¹s and St Thomas¹, and her colleagues calculated the prevalence of ASDs in children in South London aged 9 and 10. They identified 255 with any form of ASD, while 1,515 children with special educational needs were categorised as possible cases of ASD. Of this second group, gathered from a special-needs register, 255 children were selected for in-depth clinical assessment. 
The researchers found that 39 children per 10,000 had autism and 77 per 10,000 had ASD, making the total prevalence of all types of ASD 116 per 10,000. They then calculated the prevalence based on children previously identified as having ASD, which they found to be only 44 per 10,000. 
Professor Baird said the findings indicated that children with some form of ASD constituted 1 per cent of the child population in Britain. 
³Prevalence of autism and related ASDs is substantially higher than previously recognised,² she said. ³Whether the increase is due to better ascertainment, broadening diagnostic criteria or increased incidence is unclear. Services in health, education and social care will need to recognise the needs of children with some form of ASD.² 
Various studies have failed to find any evidence of a suggested link to the combined MMR vaccine given to children. A Canadian study published this month in the journal Pediatrics also rejected the link between autism and MMR. Researchers at the Montreal Children¹s Hospital dismissed suspicions that thimerosal, a mercury-based preservative in some vaccines, was behind increased rates of the disorder. 
Many scientists attribute the rise to a broader definition and greater awareness of the disorder. 
Writing in The Lancet, Hiroshi Kurita, of the Zenkoku Ryoiku Sodan Centre in Tokyo, suggests that the recent surge in cases can be attributed to improved diagnosis. 
³Such progress seems to have resulted in the identification of a greater number of high-functioning pervasive developmental disorders, which are harder to detect than those disorders with mental retardation because of their milder autistic symptoms,² he wrote. 

