




    THE ALUMINUM IN BAKING POWDER MAKES ME ILL.  Aluminum is a toxic metal and a neurotoxin.  You sound pretty toxic to me. It also makes mercury 50 times more toxic. 


        I DONT THINK PEOPLE WHO ARE SO SICK THEY WANT TO KILL THEMSELVES IS TRIVIAL YOU DO.  You have no solutions except CTB. 

The following information was compiled and submitted by Frank Hartman. 
"From the earliest days of food regulation, the use of alum (aluminum sulphate) in foods has been condemned. It is universally acknowledged as a poison in all countries. If the Bureau of Chemistry had been permitted to enforce the law ... no food product in the country would have any trace of ... any aluminum or saccarin. No soft drink would contain caffeine or hebromin; no bleached flour would be in interstate commerce. Our food and drugs would be wholly without adulteration ... and the health of our people would be vastly improved and their life greatly extended." 
prime mover behind the original Pure Food Law and Director of the FDA. He resigned in disgust in 1912 over exceptions granted to the law and lack of enforcement. 
Aluminum has been exempted from tesitng for safety by the FDA under a convoluted logic wherein it is classified as GRAS. (Generally Regarded As Safe.) It has never been tested by the FDA on its safety and there are NO restrictions whatever on the amount or use of aluminum. 
There are over 2000 references in the National Library of Medicine on adverse effects of alumium. The following were extracted to provide a small sample of the range of toxicity of aluminum. 
Chemical Registry Aluminum toxicity has been recognized in many settings where exposure is heavy or prolonged, where renal function is limited, or where apreviously accumulated bone burden is released in stress or illness. Toxicity may include: encephalopathy (stuttering, gait disturbance, myoclonic jerks, seizures, coma, abnormal EEG) osteomalacia or aplastic bone disease ( associated with painful spontaneous fractures, hypercalcemia, tumorous calcinosis ) proximal myopathy, increased risk of infection, increased left ventricular mass and decreased myocardial function microcytic anemia with very high levels, sudden death. 
Aluminum is ubiquitous in our environment; it is the third most prevalent element in the earth's crust. The gastrointestinal tract is relatively impervious to aluminum, absorption normally being only about 2%. Aluminum is absorbed by a mechanism related to that for calcium. Gastric acidity and oral citrate favors absorption, and H2-blockers reduce absorption. As is true for several trace elements, transferrin is the primary protein binder and carrier for aluminum in the plasma, where 80% is protein bound and 20% is free or complexed to small molecules such as citrate. 
Cells appear to take up aluminum from transferrin rather than from citrate. Purified preparations of ferritin from brain and liver have been found to contain aluminum. 
It is not known if ferritin has a specific binding site for aluminum. Factors regulating the migration of aluminum across the blood-brain barrier are not well understood. 
Serum aluminum correlates with encephalopathy; red cell aluminum correlates with microcytic anemia, and bone aluminum correlates with aluminum bone disease. 
Basal PTH when elevated appears to protect bone and thereby favor CNS toxicity. 
Other factors favoring one form of toxicity over another are not well understood. 
Aluminum toxicity has been reported to impair the formation and release of parathyroid hormone. The parathyroid glands concentrate aluminum above levels in surrounding tissues. Treatment of aluminum toxicity in renal failure patients often reactivates hyperparathyroidism, which to a certain extent is helpful for bone remodeling and healing. 


Distilled Water Placed in Various Containers Image from Control of Colloid Stability through Zeta Potential by Thomas M. Riddick - Link to Front Page of Book 


    Distilled water was placed in metal containers and the amount of the "Metal Can" that disolved into the distilled water was measured daily using Specific Conductance readings. You can divide the SC number by 2 to get the approxamite amount of atoms in ppm ( mg / l ). 
    4 ppm of aluminum in human blood can cause it to colagulate.     Aluminum in humans is documented to Inhibit Learning.  See Below .=2E. 
Aluminum neurotoxicity in preterm infants receiving intravenous-feeding solutions. 
Bishop N.J. - Morley R. - Day J.P. - Lucas A. 
From:   N Engl J Med (1997 May 29) 336(22):1557-61 
Aluminum, a contaminant of commercial intravenous-feeding solutions, is potentially neurotoxic. We investigated the effect of perinatal exposure to intravenous aluminum on the neurologic development of infants born prematurely. 
RESULTS: The 90 infants who received the standard feeding solutions had a mean (=B1 SD) Bayley Mental Development Index of 95 =B122, as compared with 98 =B120 for the 92 infants who received the aluminum-depleted solutions (P=3D0.39). The former were significantly more likely (39 percent, vs. 17 percent of the latter group; P=3D0.03) to have a Mental Development Index of less than 85, increasing their risk of subsequent educational problems. For all 157 infants without neuromotor impairment, increasing aluminum exposure was associated with a reduction in the Mental Development Index (P=3D0.03), with an adjusted loss of one point per day of intravenous feeding for infants receiving the standard solutions. In preterm infants, prolonged intravenous feeding with solutions containing aluminum is associated with impaired neurologic development. 
Aluminum-containing emboli in infants treated with extracorporeal membrane oxygenation. 
Vogler C. - Sotelo-Avila C. - Lagunoff D. - Braun P. - Schreifels J.A. - Weber T. 
From:   N Engl J Med (1988 Jul 14) 319(2):75-9 
We found fibrin thrombi or thromboemboli at autopsy in 22 of 23 infants with respiratory failure who had been treated with venoarterial extracorporeal membrane oxygenation (ECMO). In addition, distinctive basophilic aluminum-containing emboli were found in 12 of the infants; the distribution of these emboli was similar to that of the thromboemboli, except that an aluminum-containing embolus was found in a lung in only 1 infant. Sixteen infants had pulmonary thrombi or thromboemboli. We also found friable aluminum-containing concretions adhering loosely to the mixing rods of heat exchangers that had been used to warm the blood flowing through the ECMO circuit; such concretions were not present on unused mixing rods. We propose that these aluminum-containing concretions developed as the silicone coating of the heat exchanger wore away and aluminum metal was exposed to warm, oxygenated blood and that fragments of aluminum-containing concretions formed emboli. This hypothesis is supported by the fact that aluminum-containing emboli were generally not present in the lungs, which are bypassed by ECMO. 
Sequential serum aluminum and urine aluminum: creatinine ratio and tissue aluminum loading in infants with fractures/rickets. 
Koo W.W. - Krug-Wispe S.K. - Succop P. - Bendon R. - Kaplan L=2EA. 
From:   Pediatrics (1992 May) 89(5 Pt 1):877-81 
Aluminum toxicity is associated with the development of bone disorders, including fractures, osteopenia, and osteomalacia. Fifty-one infants with a mean (=B1 SEM) birth weight of 1007 =B134 g, gestational age of 28.5 +/-0.3 weeks, and serial radiographic documentation at 3, 6, 9, and 12 months for the presence (n =3D 16) or absence (n =3D 35) of fractures and/or rickets were studied at the same intervals to determine the serial changes in serum aluminum concentrations and urine aluminum-creatinine ratios. Autopsy bone samples were used to determine the presence of tissue aluminum. One infant who received aluminum-containing antacid had marked increase in serum aluminum to 83 micrograms/L while urine aluminum-creatinine ratio increased from 0.09 to a peak of 8.53. Vertebrae from three infants at autopsy (full enteral feeding was tolerated for 37 and 41 days in two infants, respectively) showed aluminum deposition in the zone of provisional calcification and along the newly formed trabecula. 
Aluminum in parenteral solutions revisited - again. 
Klein G.L. 
From:   Am J Clin Nutr (1995 Mar) 61(3):449-56 
It has been a dozen years since aluminum was first shown to contaminate parenteral nutrition solutions and to be a contributing factor in the pathogenesis of metabolic bone disease in parenteral nutrition patients as well as in uremic patients. However, there are no regulations in place to effectively reduce aluminum contamination of various parenterally administered nutrients, drugs, and biologic products. The purpose of this review is fourfold: 1.) to summarize our knowledge of the adverse effects of aluminum on bone formation and mineralization in parenteral nutrition patients; 2.) to discuss the possible role of aluminum in the osteopenic bone disease of preterm infants; 3.) to show how lack of regulations covering aluminum content of parenteral solutions can lead to vulnerability of new groups of patients to aluminum toxicity, the example being given here is that of burn patients 
Aluminum-induced anemia. 
From:   Am J Kidney Dis (1985 Nov) 6(5):348-52 
.=2E. many questions still remain unanswered, it is clear that aluminum causes a microcytic hypoproliferative anemia and is a factor responsible for worsening anemia in patients with end-stage renal disease. 
Arch Dermatol (1984 Oct) 120(10):1318-22 
Three patients had subcutaneous nodules at the sites of previous injections of vaccine containing tetanus toxoid, showed aluminum crystals in the nodules from two patients. From the evidence available, we believe that these nodules are a complication of inoculations with aluminum-containing vaccines. 
Persistent subcutaneous nodules in patients hyposensitized with aluminum-containing allergen extracts. 
Garcia-Patos V. - Pujol R.M. - Alomar A. - Cistero A. - Curell R=2E - Fernandez-Figueras M.T. - de Moragas J.M. 
From:   Arch Dermatol (1995 Dec) 131(12):1421-4 
These lesions have been mainly attributed to a hypersensitivity reaction to aluminum hydroxide, which is used as an absorbing agent in many vaccines and hyposensitization preparations. Patch tests with standard antigens and aluminum compounds and histopathologic and ultrastructural studies were performed on 10 patients with persistent subcutaneous nodules on the upper part of their arms after injection of aluminum-adsorbed dust and/or pollen extracts. The nodules appeared 1 month to 6.5 years after injections. 
Trace metals and degenerative diseases of the skeleton. 
Savory J. - Bertholf R.L. - Wills M.R. 
From:   Acta Pharmacol Toxicol (Copenh) (1986) 59 Suppl 7:282-8 
Aluminum related osteodystrophy is the most important manifestation of trace metal toxicity related to degenerative diseases of the skeleton. 
Postvaccinal sarcomas in the cat: epidemiology and electron probe microanalytical identification of aluminum. 
Hendrick M.J. - Goldschmidt M.H. - Shofer F.S. - Wang Y.Y. - Somlyo A.P. 
From:   Cancer Res (1992 Oct 1) 52(19):5391-4 
An increase in fibrosarcomas in a biopsy population of cats in the Pennsylvania area appears to be related to the increased vaccination of cats following enactment of a mandatory rabies vaccination law. 
The majority of fibrosarcomas arose in sites routinely used by veterinarians for vaccination, and 42 of 198 tumors were surrounded by lymphocytes and macrophages containing foreign material identical to that previously described in postvaccinal inflammatory injection site reactions. Some of the vaccines used have aluminum-based adjuvants, and macrophages surrounding three tumors contained aluminum oxide identified by electron probe microanalysis and imaged by energy-filtered electron microscopy. Persistence of inflammatory and immunological reactions associated with aluminum may predispose the cat to a derangement of its fibrous connective tissue repair response, leading to neoplasia. 
Aspects of aluminum toxicity. 
Hewitt C.D. - Savory J. - Wills M.R. 
From:   Clin Lab Med (1990 Jun) 10(2):403-22 
Attention was first drawn to the potential role of aluminum as a toxic metal over 50 years ago, but was dismissed as a toxic agent as recently as 15 years ago. The accumulation of aluminum, in some patients with chronic renal failure, is associated with the development of toxic phenomena; dialysis encephalopathy, osteomalacic dialysis osteodystrophy, and an anemia. Aluminum accumulation also occurs in patients who are not on dialysis, predominantly infants and children with immature or impaired renal function. Aluminum has also been implicated as a toxic agent in the etiology of Alzheimer's disease, Guamiam amyotrophic lateral sclerosis, and parkinsonism-dementia. 
Soft tissue sarcoma associated with aluminum oxide ceramic total hip arthroplasty. A case report. 
Ryu R.K. - Bovill E.G. Jr - Skinner H.B. - Murray W.R. 
From:   Clin Orthop (1987 Mar)(216):207-12 
Malignant tumors around fracture fixation implants have been reported sporadically for many years. Recently, however, reports of sarcomatous degeneration around a standard cemented hip arthroplasty and around cobalt-chromium-bearing hip arthroplasties raise new questions of the malignant potential of metallic ends prostheses. Sarcomatous changes around aluminum oxide ceramics seem not to have been reported in the literature. The present report may be the first documented case of an aggressive soft tissue sarcoma detected 15 months after the patient had an uncemented ceramic total hip arthroplasty. If a causal relationship exists, the incidence of this phenomenon in the United States is 250 times greater than would be expected from statistics on soft tissue sarcoma at the hip. 
Aluminum-induced granulomas in a tattoo. 
McFadden N. - Lyberg T. - Hensten-Pettersen A. 
From:   J Am Acad Dermatol (1989 May) 20(5 Pt 2):903-8 
Aluminum was the only nonorganic element present in the test site tissue. This is the first report of confirmed aluminum-induced, delayed-hypersensitivity granulomas in a tattoo. 
Delayed healing in full-thickness wounds treated with aluminum chloride solution. A histologic study with evaporimetry correlation. 
Sawchuk W.S. - Friedman K.J. - Manning T. - Pinnell S.R. 
From:   J Am Acad Dermatol (1986 Nov) 15(5 Pt 1):982-9 
Wounds were treated either with 30% aluminum chloride solution or ferric subsulfate solution or were allowed to clot with minimal pressure from a gauze pad. Delay in reepithelialization was noted histologically both in wounds treated with aluminum chloride and in those treated with ferric subsulfate compared to controls. Presumably this delay was the result of tissue necrosis caused by these hemostatic agents, resulting in slightly larger and less cosmetically acceptable scars. Plots of evaporimetry data revealed a biphasic pattern of water loss during healing, with an initial rapid decline in water loss followed by a much slower decline. 
Aluminium and injection site reactions. 
Culora G.A. - Ramsay A.D. - Theaker J.M. 
From:   J Clin Pathol (1996 Oct) 49(10):844-7 
To alert pathologists to the spectrum of histological appearances that may be seen in injection site reactions related to aluminium, showed unusual features not described previously. In one, there was a sclerosing lipogranuloma-like reaction with unlined cystic spaces containing crystalline material. The other case presented as a large symptomatic subcutaneous swelling which icroscopically showed diffuse and wide-spread involvement of the subcutis by a lymphoid infiltrate with prominent lymphoid follicles. 
CONCLUSIONS: This report highlights the changes encountered in aluminium injection site reactions and emphasises that the lesions have a wider range of histological appearances than described previously. 
Aluminum and gallium arrest formation of cerebrospinal fluid by the mechanism of OH- depletion. 
Vogh B.P. - Godman D.R. - Maren T.H. 
From:   J Pharmacol Exp Ther (1985 Jun) 233(3):715-21 
AlCl3 or GaCl3 was added to artificial cerebrospinal fluid and perfused through the cerebral ventricles of the rat. Depending on the metal and its concentration (1-10 mM) the pH of the perfusate ranged from 7.2 to 3=2E5. At 10 mM metal chloride, yielding pH 4.7 (Al) or 3.5 (Ga), formation of cerebrospinal fluid was suppressed 100%. This mechanism may also account for the antiperspirant action of Al salts. 
Aluminum toxicity and albumin. 
Kelly A.T. - Short B.L. - Rains T.C. - May J.C. - Progar J.J. 
From:   ASAIO Trans (1989 Jul-Sep) 35(3):674-6 


