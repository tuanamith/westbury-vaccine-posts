



Women and Children of CFIDS WACOC 
A partial excerpt from: 
NVIC Guaranteeing Vaccine Use, Profits  <URL>  

National Vaccine Information Center Newsletter 


Guaranteeing Vaccine Use, Profits 
BL Fisher Note: 

The U.S. government, with backing from the world's pharmaceutical industry, is leading an effort to persuade seven of the world's most powerful nations to guarantee vaccine manufacturers a world vaccine market with plenty of profits. Apparently Italy and Great Britain have joined the US in trying to persuade Germany, Japan, France and Russia to go along with the "advance market commitment" to underwrite the pharmaceutical industry so their stockholders can get rich vaccinating the world. 


To help finance the program, government officials from the G-8 nations are talking about whether to levy a tax on airline tickets to help finance the program. Apparently there isn't enough money being generated by forced vaccination of populations in the U.S. and other wealthier countries to fully finance forced vaccination of populations in poorer countries. An airline tax would force the flying taxpayer to cough up a little bit more (pardon the pun) to further reduce the threat of infectious diseases being "just a plane ride away." 


What appears to be absent in their profit-making scheme, is the cost of caring for the people who are crippled by unsafe vaccines and one-size-fits-all forced vaccination policies. So many of the vaccine injured are swept under the rug now that it is almost impossible to imagine what governments will decide to do with all the brain damaged and immune compromised vaccine injured people in the future. ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Third-World Vaccine Development Plan Stalls 
Positions of France, U.S. Means Endorsement Unlikely During G-8 Summit 
The Washington Post 
July 7, 2006, Page D05 

By Michael M. Phillips 
The Wall Street Journal 
A spat between the United States and France is holding up progress on a novel, business-friendly plan to persuade drug companies to develop vaccines for killer diseases in the Third World. 
Despite lobbying from the United States, Italy and Britain, chances have faded that President Bush and other leaders of the Group of Eight major powers will endorse the proposal next weekend in St. Petersburg, Russia, at their annual summit. 
The proposal "is kind of trembling on the cusp," said a senior official with direct knowledge of the G-8 vaccine discussions. "It is now in substantial danger of flopping even though there is an extraordinary level of support among some key stakeholders." 
A summit-preparatory meeting last month ended with senior officials from France and the United States in a heated exchange over the plan, in which the G-8 would guarantee a market for pharmaceuticals companies that create successful vaccines. 
France refused to endorse the vaccine proposal unless the United States backed a French proposal for a new international airline-ticket tax to pay for aid to poor countries, and the Bush administration refused to do so, according to three people familiar with the internal G- 8 talks. 


Complicating matters was the reluctance of Germany and Japan to put large amounts of money into the vaccine plan, called an advance market commitment. 


The situation marks a setback for the vaccine initiative. The proposal appeared to be on the fast track in February when G- 8 finance ministers, including then-U.S. Treasury Secretary John W. Snow, endorsed the idea. At that time, the ministers expected to agree by April on a pilot project to tackle at least one deadly infectious disease. 

That meeting led to the creation of a panel of experts, who recommended that the first advance market commitment be used to promote development of a vaccine for pneumococcal disease. The bacterial infection killed 1.6 million people in 2002, 716,000 of them under the age of 5, according to the most recent World Health Organization figures. 


G-8 officials say drug companies, although initially skeptical, have rallied behind the idea. "They're ready to give this a go," said the official familiar with the G-8 discussions. 


The plan is intended to correct a flaw in global drugs markets: The countries that most need new vaccines for diseases such as AIDS and tuberculosis can least afford to pay for them. An Italian Finance Ministry paper concluded last year that weak demand has drug companies reluctant to develop vaccines aimed at diseases found mostly in poor nations. 

Under the plan, the G-8 would guarantee a market -- valued at $800 million to $6 billion depending on the disease -- for any company or companies that produce vaccines that meet agreed-upon standards for safety and efficacy. Once the donors spend that initial subsidy, the pharmaceuticals companies would discount the vaccine sharply for developing- world customers. 
"All of the technical work that can be done on an abstract level has been done," said Orin Levine, an epidemiologist who works on pneumococcus vaccines at Johns Hopkins University. "It's time to just commit, to take that step and move into the first stage, which is negotiating the first-ever advance market commitment for a vaccine." Wyeth, the U.S. pharmaceuticals company, already makes a pneumococcus vaccine, but it doesn't prevent infection by bacterial strains common to the developing world, according to the U.S. Treasury Department, which has been supporting the vaccine initiative in the Bush administration. 
"Wyeth has always been supportive of the G-8's efforts around advanced market commitments for vaccine development," company spokesman Christopher Garland said in a statement. Hopes that the G-8 leaders would push the plan in St. Petersburg, however, have gone astray at the negotiating table. The advance-market-commitment plan is one of three major drug-finance plans floating around the G-8. While the plans could complement each other, they also compete for the limited pool of aid resources-and political bragging rights. 
Britain's chancellor of the exchequer, Gordon Brown, has championed what he calls the International Finance Facility, in which the G-8 would issue bonds to raise money to purchase drugs for poor nations. Several countries, including France and Italy, have signed on, according to a French government official. The Bush administration has said the British plan doesn't fit with the U.S. system of yearly budget appropriations. 
France has lobbied for a new airline-ticket tax to fund drug purchases. The revenue will go for AIDS drugs and others in poor countries, according to a French official, who says 13 other countries have agreed to the tax plan. 


At the summit-preparatory meeting last month, France argued that the G-8 should endorse the ticket-tax approach, provoking opposition from the United States and reluctance from Japan. Even Britain gave only tempered support to its European Union partner. A British Treasury spokesman said London would only go so far as to divert some of its current ticket- tax revenue to the French effort; it won't impose a new tax. 


Failing to win support for its ticket tax, the French negotiator blocked the advance-market-vaccine proposal from the G-8 leaders' statement being drafted for the coming summit, according to three people familiar with the discussions. 
The French government official played down the disagreement. At the same time, Japan and Germany shied away from the vaccine plan because of concern about the cost. "A number of other governments in the G-8 don't want to pony up more money for something right now," said a senior U.S. Treasury Department official. 
Click here for the URL: subscription required -  <URL>  


