


On 21 Jun 2006 10:23:35 -0700, "sokpupetz" < <EMAILADDRESS> > wrote: 

I have no doubt that that could happen. But I am not responsible for how others see me. I am a confident person with strong opinions that I support with logic and facts. Some people may see that as arrogant. So be it. I have no control over how others see me. I sure am not going to change my personality due to concerns about how others may see me. In my day to day life I have not generally had people criticize me for being arrogant so I doubt that this is a major problem for me. 
Well  wonder no more. Here is your very first response to one of my posts: 

Maybe now you see how we got off on the wrong foot. I had not had any direct interactions with you until you made that post to me. 
Well the fact that you could admit that indicates that you are not that sensitive or defensive. If you were then you would have completely denied it like some in this newsgroup do. 


Condescending or crude manner? You mean like how you first responded to me (above)?:-) 
Yep. That was it (see above). 



And you did "throttle" me. But don't you see how you are criticizing me for the exact same thing that you did? Hopefully you will not be like Suzanne and blame me for your reaction. If you did not like what I said you could have expressed it in a less volatile manner. 


There is nothing necessarily wrong with being sensitive and protective.  A problem may arise if your sensitivity results in an over-reaction to a set of circumstances. 
I could see that. But as long as the other person is trying to rationalize their opinion I feel compelled to present my side of the case even if I have done it over and over and over again. It is in my nature. It might be better to drop it after a while but that is just the way I am. The other person could drop it also. 


Sometimes I do say that.  



Well I agree with part of what you said. Everyone has a right to their beliefs. I don't agree that each of our beliefs are just as right as the other. Often one person is right and the other wrong. Sometimes there is little doubt about that. 



Is it your business? If you care about their children it is. Let us say, for example, that you know someone who beats the shit out of their children resulting in bruises and injury. Are you going to say it is none of your business or will you take steps to protect the children? You say that you are sensitive and protective. I would think your instinct would be to intervene to protect the children even if your friend would not agree. 


I guess it depends on what they are trying to butt into your life about. Let us say you were a heavy drinker and your health was deteriorating and you were in complete denial. Would you want a friend to "butt" in and try to help or just stand by and watch you die of alcoholism? 
I disagree. When a child's life is at stake it is difficult to cross the line to protect them. 


You are right it is her family. You are right she will do as she please in terms of raising her family. But you are wrong to think that I am wrong to confront her about neglecting to protect her infant son against a life threatening disease like pertussis by her obstinate refusal to get him vaccinated. I feel strongly about it because I have personally witnessed an infant die as a result of this disease. So maybe you could understand why I am so vocal about it. 
Yes, there are a lot of ifs. You are right that people have to do the best they can. The problem is that Suzanne and her husband are clearly *not* doing the best they can by refusing to get the pertussis vaccination. I don't know how much clearer I can make it. This is a no brainer. It is not an issue of the vaccine causing harm to the infant. Suzanne even admitted that she is not worried about adverse effects of the pertussis vaccination.  
She just thinks that it is completely unnecessary. She wants her infant, if he is exposed to deal with it "naturally" instead of having his body "altered" by a vaccine. This is insane. The "natural" course of pertussis in an infant is a very serious illness. Even if the baby survives he will suffer greatly.  
The vaccination OTOH is a simple needle prick altering the body by priming the immune system to fight off the bacterium when it is exposed to it. You *want* the baby's body to be altered in this way. It is absolute insanity to want the baby to deal with pertussis "naturally".  
Over the years, before vaccination, hundreds of thousands of children died of pertussis by dealing with it "naturally". Many more suffered greatly. 
 In countries where there is mass vaccination, this number has dropped dramatically. I posted links to sites like CDC and NIH to prove my point. Sadly it fell upon deaf ears. I don't regret trying to get Suzanne to see what is so obvious to me and others. Suzanne thinks she is part of a "micro revolution"; parents who don't vaccinate. If enough parents join her revolution you will start seeing the incidence and mortality figures go through the roof. Sorry but I am not going to stand by and let that happen without voicing my strong opinion supported by epidemiological information and facts. 




I do have children. They are grown. It is not easy. Not vaccinating makes it much harder as you have an increased chance of dealing with serious illnesses. 


I agree that it is unlikely that she would do this intentionally. I think she is very misguided and sadly her infant son may suffer as a result of this. My God, Suzanne does not even have a regular pediatrician to see her children nor does she feel she needs one. She thinks that she has access to all the info that her doctors have so that she could evaluate them as well as the doctor. This is lunacy. Suzanne is not a doctor. Even if she had access to all the info is she able to understand and process this information in order to diagnose illnesses?  


In many cases even if I disagree with a parent it is not a matter of life and death. Not vaccinating an infant against pertussis especially when you live in an area where polygamists don't vaccinate is complete insanity. I feel compelled to speak up even if it is unlikely that it will change Suzanne's mind. Even if there is a one in a thousand chance that something I say will click in her mind and have her decide to vaccinate, it is well worth it. 
YOu are right. See above. 

That is a ridiculous statement. My complaint was made in good faith. I did not make up any of the information that I provided to the authorities. You cannot be sued for slander for telling the truth. It may be that the information was not sufficient to warrant an investigation but at least I can say I tried and if something does happen to one of her children as a result of her neglect I  will know that I did all that I could.   
OK. There you have it. 
We can agree to disagree. 



There is a difference between self confidence and refusal to reconsider one's opinion based upon new information. For example I was critical of Kittyn for something and then she pointed out that my criticism was unfounded. I agreed and apologized. You see being self confident means that you can admit that you are wrong because you realize that no one is perfect. We all make mistakes. 


That is how I see Suzanne and her stance on vaccination against pertussis. I offered facts to support my position. Suzanne offered nothing much other than hope that if her infant got infected he would do as well as other family members who got ill. 

  
Based upon this most recent post of yours, it does appear to me that we agree more than we disagree. In any case it seems that we can discuss it without getting mean and/or nasty. 


Every person draws the line in a different place. I don't generally explicitly state that I am confident other than when we are discussing that issue. It is just that my opinions come across as being certain and I back them up with logic and facts. But to me that is a good thing.  



You are correct. I am humble enough to know that I am far from perfect and if someone identifies something I said that is wrong I will admit it and apologize if warranted as I have done in this newsgroup.   
Actually I think you do want to get to that point. Just because you are not bothered by what other people think does not mean that you cannot consider their POV and change if you think it appropriate. When I say "bothered" I mean that it causes undue emotional distress. 


I guess we all do. Thanx for being much more civil with me. Hopefully you felt that your civility was reciprocated by me. 
Ryan 
------------------------------------------------------ ------------------------------------------------------ 
2006 ATAI Bragging Rights Champion 

