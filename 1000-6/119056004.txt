


jumpin jeffery wrote: 

Flea infestation kills animals as well. I've seen cats and kittens so  riddled with fleas that they actually are anemic because of the lack of  blood - kittens usually don't recover when they're infected like that.  And don't forget the ticks. I always use the vet stuff - never buy  anything for my animals from any store - cept for brushes, food, etc.  When it comes to medicine - it all comes from the vets. Don't use  collars either - my cats are all indoors - collars get hooked on things  - don't wanna come home to one of mine hanging. Also trim their nails  regularly. 
Regarding vaccines - I don't get vaccinated every year - why should they  (if they are indoor only)? Most vets I've worked for and known aren't  rich - by far. A lot of their money is made on the regular check-up and  vaccinations. Plus it's good to have them checked up on a regular basis.  It's also good for us to try to wrestle them into a carrier at least  once a year. Helps us remember that we are truly only human. 

