


Job Title:     Clinical Research Scientist - Neuroscience Job Location:  PA: Philadelphia Pay Rate:      Open Job Length:    4-6 months (contract) Start Date:    2006-06-20 
Description:   Title:       	Clinical Research Scientist - Neuroscience   Company:  Pharmaceutical Company 
Location:    Philadelphia Metro Area 
Status:      Full-time consulting engagement, 40 hrs/week 
Length:      Long-term assignment in 6 month increments 
Our client is a research-based, global pharmaceutical company responsible for the discovery and development of some of today's most innovative medicines.   The product portfolio includes innovative treatments across a wide range of therapeutic areas with a discovery and development platform encompassing pharmaceuticals, vaccines, and biotechnology.  The goal of our client is to be recognized as one of the best pharmaceutical companies in the world. In this position, the Clinical Research Scientist - Early Development and Clinical Pharmacology (Neuroscience) drafts protocols, formulates requirements for clinical supplies and develops CRFs. 
Please refer to Job code cb3529 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



