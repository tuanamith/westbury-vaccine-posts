


SUNDAY EXPRESS 18 June 2006 
by Lucy Johnston HEALTH EDITOR 
Can we ever trust MMR? 
'The Government has not looked at the whole picture' 
Four years ago, the Sunday Express revealed that at least 26 child deaths  have been linked with the measles, mumps and rubella vaccine. In many cases,  the Government - or leading medical officials - accepted the connection. 
Parents were awarded vaccine damage payments of up to £100,000 and, in other  cases, experts drew up post-mortem reports blaming the MMR jab as the most  likely cause of death. 
Now, as we report today, two more parents have come forward claiming their  babies died as a result of the jab. And, last month, Vietnamese health  authorities withdrew the MMR jab after the death of one child and  hospitalisation of five others. The World Health Organisation is now  investigating this scare. 
Since its launch in 1988, thousands of parents have reported unwanted  reactions to the triple jab, from moderate - rash, headache, temperature -  to severe, including brain damage, autism and convulsions. In 1992, the  Department of Health conceded it got the pre-licence trials wrong when the  chief medical officer announced the withdrawal of two of the three brands of  MMR because they were found to be causing meningitis. 
All drugs, including vaccines can have side effects.The Government accepts  this - why else would it make vaccine damage pay-outs of up to £100,000?  But, publicly, it claims no deaths have been associated with MMR. How can it  do this when its own officials and post-mortem reports state otherwise? 
Vaccine manufacturers accept there can be serious side effects, and have  informed the Government of this. So why does the Government's publicity  machine continue to insist that the triple jab is entirely safe? 
Instead of being open and investigating potential dangers in what appears to  be a minority of children, the Government polarises the debate by implying  there are no risks.The Whitehall propaganda machine really kicked in eight  years ago when the press reported findings of Dr Andrew Wakefield's  explosive paper linking the MMR jab with autism. 
At the time, his work was accepted as credible by experts in the field. But,  instead of making stocks of single vaccines available, as Wakefield advised,  policy chiefs made it difficult for parents to obtain them. 
MMR uptake continued to fall. With an outbreak of disease on the horizon,  publich health officials panicked. The Department of Health launched a  campaign to rubbish Wakefield's research. He was ostracised by his peers and  forced to resign his post at the Royal Free. The Government risked losing  face if it changed its stance and accepted MMR might cause problems in some  children, but it also stood to lose millions in compensation claims. Action  had also been taken against the drug companies, which is still ongoing. 
Dr Wakefield has become the scapegoat for the frenzy over MMR but he is not,  as the Government likes to portray him, a lone maverick. Many other doctors  have concerns, and other scientists have found evidence to support his  findings. But the Department of Health insists that research proves the jab  is safe. 
However, the Government has not looked at the whole picture. Instead of  looking at the affected children themselves, the studies it cites are based  on patterns of disease taken from medical records of large populations,  which are unable to detect adverse reactions in small numbers of children. 
When Dr Wakefield alerted the Government and vaccine chiefs to his research  before publication, it promised an independent forum into his findings. This  has never happened. Instead, it has called for an investigation into Dr  Wakefield. The General Medical Council is considering whether to charge him  with serious professional misconduct. 
Figures released by the Health Protection Agency last week reveal the number  of potentially deadly measles cases seen by doctors since January is five  times higher than during all last year, prompting fears of an epidemic. 
As Richard Halvorsen, vaccine expert and central London GP said: "With the  threat of a measles epidemic, the only way many parents will protect their  children is with the single vaccine. By refusing to allow this, the  Government is contributing to the epidemic it seeks to prevent." 
One has to ask the question: where does the Department of Health's interests  lie? Is it to protect the nation's health, or to protect officials, and the  pharmaceutical industries' lucrative patents for new combination jabs? 



