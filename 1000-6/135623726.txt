


Anne Rogers wrote: 
Well, I will try not to worry too much about the bugs, but it's hard not to. We'll all get flu vaccines, but it's hard to avoid things like the common cold, which could still be very dangerous to Hale.  I am trying to focus more on enjoying my baby rather than worrying about him, but I think, for me, some worry is almost reassuring -- makes me confident that I'm on my toes and doing what we're supposed to do for him. 

I don't know if RSV is less common in the UK, but here, they say most kids get it by the age of 5 (though most also get it over the age of six months or so, when it's not really a problem).  Breastfeeding sure helps, but my daughter got it at 6 months, and she was exclusively breastfed at that time, too.  Most kids who are close to a year or older and get it are never diagnosed, since it appears to be little more than a cold for older kids.  So I'm still pretty fearful of this one, even though I know he probably will be fine if we keep him away from other children. 

I do remember that you were having coughing problems, too.  Interesting that it also ended up being heart related -- certainly not the "normal" cause of a cough, for both you and DS to have that be the reason.  I sure hope it's minor and gets better soon.  Anything related to the heart just feels extra scary to me, but I know it often is no more serious than anything else that goes on.  Good luck. 
Thanks.  :-) 
-Carlye DS 6-2-06 DD 9-29-04 


