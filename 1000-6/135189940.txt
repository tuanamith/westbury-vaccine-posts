



< <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
Thanks Eric, great comments. And right on the money. 
We often have little squabbles in the Child Protective group about issues  similar to this.  Parent's protecting their children to the highest degree  possible with responsible decison making. 
We've talked about many different aspects of the same issue...parents who  neglect proper medical care, parents who refuse vaccine, parents who allow  children to be abused per religious beliefs, and parents who simply don't  give a damn. 
I suspect that this boy's parents do love him and want the best for him...in  the capacity that they are able to love him and discern what is best for  him. 
That does not mean that they love him and want the best for him as is  reasonable by the standards of the rest of the world. 
Take a child from their parents and watch the parents cry and become upset.  Even parents who do not feed their children, who do not properly supervise  or care for their children, etc...they all love their children to a  degree...but sometimes that just isn't enough. 
Drug addicts don't see anything wrong with doing drugs while they are  pregnant, or around thier kids.  Alcoholics don't see anything wrong with  drinking while pregnant or around their kids.  Some Baptists don't see  anything wrong with putting their children in a pit with rattlesnakes. 
Though there are many mistakes made by state agencies and courts that are  charged with protecting children when their parents fail to, and they take  much slack for it, the bottom line is that these courts and agencies would  not exist if parents properly cared for their children. 
Allowing a sixteen year old child to choose treatments that may well kill  him is not proper care. 
This is quite an emotionally charged issue, and as a parent, I don't know  how well I would make decision that would affect the very life of my child. 
I know I would not do what these people are doing. 
Some people's perception of 'right and wrong' is so off kilter that it  really shocks me sometimes. 
I hope for the best for this young man, however, I feel that his parents  will regret that they made the decisions that they did. 





