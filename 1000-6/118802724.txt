


Marc wrote: 
What I mean by mutation is, any kind of genetic change in an individual organism, which has the possibility of being spread throughout (and possibly fixed) in a population. Could be any of that stuff, right? A mutation could also be caused by a virus, right? 

Isn't mutation the ultimate origin of phenotypic variability, so that selection has something to work with, and so that biological evolution may occur? 

We should certainly use the best tools that we have to do the tasks we think are important. I apologize that "viruses are bad" is a sloppy way to communicate "medical science would like to eradicate human disease." I'm all for the phages. Go phages! Are they playing in the World Cup? ;) 

Yeah, and tons of others, I'm sure. Though the only ones I can think of right off the top aren't viral. I'm ashamed. Even so, imagine a scenario where we have eradicated human disease. Wouldn't we still commonly get virus crossovers from (especially) our domesticated animals? So then we have both an economic and a medical incentive (if the goal of medicine is to eradicate disease in humans) to destroy viral diseases of those animals, too. Possibly also we'd want to eradicate potential pathogens from populations of animals which are closely related to us (I'm thinking the HIV crossover from ... was it chimpanzees?). 

Ah, that "most creative force" bit is going in the "freaking cool" category. I also like this, from the article: "A big question now is the degree to which this super-organism [the "massive pool of genetic information" that viruses represent] extends its tentacles into host genomes." (Ooh, tentacles.) But I can't tell if this article is really reflecting the beliefs of virologists in saying that they're the "most creative force in evolution," or whether it's just the journalism. So I can't really tell from this whether the influence of viral-induced mutations on most populations of animals are more important that other types of mutations; further, my feeling is that virologists and ecologists haven't decided whether this is the case. 

Well, yeah, but are you going to let Mr. and Mrs. Smallpox raise their kids in *your* town? ;) 
So how important are viruses in ecology? If we rid humans of disease-causing viruses, and rid our relatives and our domestic animals, too, will we have f'd up either a necessary part of maintaining healthy ecosystems (which might involve individual organisms getting sick), or a major (perhaps the most important) generator of diversity? 

Yeah, I know very little about population dynamics. That's part of why I'm wondering about all this. Feel free not to educate me, I'm not going to pay you. :) 
Would you say that, in general, it's a bad idea to combat (or attempt to eradicate) diseases in wild populations? Whirling disease is a big deal in trout that apparently many people would like to eliminate; and there's this whole deal about brucellosis in bison in southwestern Montana, with some people afraid of crossovers from bison to cattle, and with some other people afraid to vaccinate the bison. These aren't viral diseases, but they do represent combatting diseases in wild populations. What if we could just get rid of them (somehow)? How likely is it that that would be an extremely bad idea? Surely they play an important ecological role. (Although it might be that humans introduced these diseases, and they are decimating populations which aren't their "natural" targets. I don't know. In cases like that, we'd probably want to eliminate the disease, if possible.) 

Probably. I'm not saying there need to be Senate hearings. 

So genomes change, right? And that's mutation, right? So are mutations which are induced by viruses more common than other types of mutations, in, say, a hypothetical "typical" animal population (or, if you prefer, albino cave salamanders)? How much more likely is a virus to affect a coding region than some other type of mutation, so that it may be selected for or against? Is there a satisfactory (to virologists and ecologists) theoretical framework in viral ecology that provides answers here, or are these mysteries? 

The former; I was thinking specifically of retroviruses. I recall (perhaps mistakenly) that retroviruses are one explanation for the origin of domain shuffling in proteins (which I'd say is a mutation, and a pretty darn good one, sometimes). Not sure about the latter; what's MHC? 

That question, together with "How much impact do viruses have as agents of genetic change?" imply the very interesting (well, I think so, anyway), "Assuming we could eliminate all sources of viral disease in humans, would doing so be unwise, especially in wild populations?" 

There are rural parts of New Jersey? That place still gives me horrible nightmares about concrete. 

And to wash their hands! 

I suppose I should indicate the inspiration of my question. Yesterday morning the SF Chronicle reported on a vaccine for human papilloma virus. I thought, maybe if this works we could eliminate the most common sexually transmitted disease in humans. We've already almost spanked smallpox and polio off to oblivion, haven't we? But if we eliminated all human viral diseases, would we spell our own doom? (And doom is easy to spell.) 
Thanks very much for your reply! Feel encouraged to fix my ignorance and stupidity, if you're so inclined. 
Dan 


