



Islander wrote: 
Like? 

The argument presented by opponents - NOTE THIS IS NOT MY ARGUMENT - is that the unused "fetus" is still viable regardless of the decision of the care giver. 
NOT MY ARGUMENT - but the argument presented by opponents to the bill. 


You never want to argue numbers with me.  The largest of the institutes (NCI) has a 4.8 billion budget of which 2.1 billion are slated for basic research grants.  Most of the 24 institutes will spend less than 1 billion each and much of this is not spent on research but rather other programs (education, for example). 
With the Buffet monies, B&M will spend about 3 billion per year or more.  A large part of this is in applied research - for example, the HIV vaccine program budgeted for 300 million. 
The B&MGF will outspend most of the institutes. 

Applied research is research. 

There are documented therapeutic successes using "adult".  There is no evidence of therapeutic success in humans with embryonic. 

There is scientific debate on this. 

Rove said adult was promising - he is scientifically correct.  The promise of embryonic is not clear.  If I were to make an investment decision between two research activities, I would tend to the one with the greater likelihood of success. 
I'm not defending the president or his position - I simply pointed out that irrespective of what Clarke said, Rove was correct in his statement.  I don't think he made the statement to be scientific and that is what bugs me.  I don't like science becoming a political football and I point you to the issue of global warming last week and stem cells this week. 
js 


