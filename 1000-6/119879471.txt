



Women and Children of CFIDS WACOC 
NVIC Is Wakefield Right? 
A partial excerpt: 
E-NEWS FROM THE NATIONAL VACCINE INFORMATION CENTER Vienna, Virginia             <URL>  
"Protecting the health and informed consent rights of children since 1982." 
==========================================================================================  <URL>  The Washington Post June 13, 2006, 16:22 GMT 
The Age of Autism: But is Wakefield right? By Dan Olmsted 

WASHINGTON, DC, United States (UPI) -- Dr. Andrew Wakefield, the British gastroenterologist who first raised the prospect of a link between the measles-mumps-rubella vaccine and autism, is being pursued by British medical authorities. 
According to the BBC: 'The Independent newspaper reports that the General Medical Council will accuse Mr. Andrew Wakefield of carrying out `inadequately founded` research. Vaccination rates fell sharply after Dr Wakefield questioned the safety of MMR, raising fears of a measles epidemic. His initial Lancet paper has since been disowned by the journal.' 
Let`s put aside the issues surrounding the Lancet paper and concerns about a measles epidemic and go straight to the heart of the matter: Does the MMR cause autism? In other words, is Wakefield right? 
After looking into the topic for more than a year, I`m very concerned that he may be -- that, especially in children whose immune systems have been rendered susceptible by any number of possible exposures, the combined live-virus vaccine has its fingerprints all over numerous cases of regressive autism. 
Until researching the seven-part Age of Autism series in Olympia, Wash., that concluded last month, I would not have said that. But when you encounter case after case of perfectly normal children regressing after live-virus vaccinations -- in this case, the MMR in close proximity to the chickenpox shot -- you have to keep your options open. 
The families in Olympia noticed a common thread: They had unusual histories of chickenpox and other herpesviruses in their families; their child got the chickenpox and MMR shots in close temporal proximity, often at the same 12-month office visit when both are first recommended; and the child subsequently was diagnosed with regressive autism. 
Despite the sweeping assurances that there`s no link between the MMR and autism, no one seems to have looked at whether such a family history of susceptibility to viruses used in vaccines might raise a risk factor. Call me hypervigilant, but I would have expected that to be rigorously reviewed a long time ago. 
Two of the Olympia children, in fact, were in small trials at age 12 months of chickenpox and MMR vaccines. One of the vaccines, called ProQuad, combines the MMR and chickenpox, kicking in 10 times the standard amount of chickenpox vaccine to overcome the 'immune interference' that can occur when live viruses interact. 
Such interference is at the heart of Wakefield`s concern about the combined MMR vaccine -- that the viruses suppress the immune system in such a way that weakened-but-live measles viruses can set up house and trigger a delayed neurological infection: autism. 
And measles is not benign -- that`s why there`s such a push to vaccinate against it. In a small percentage of cases, the wild, or naturally occurring, infection can lead to delayed brain damage and death. 
It`s a neurotoxic virus, in short. Wakefield`s question and concern is whether in some cases the live-virus vaccine is neurotoxic, too. 
Not such a wild idea, really, and listening to him talk makes you hope to God the vaccine manufacturers and regulators are a lot smarter than he makes them sound: 
'What alarms me about the cavalier approach of the industry and everybody else, the regulators, to these viruses is they presume the wild infection to be nasty and the vaccines to be innocuous -- that they can manipulate something that is biologically highly intelligent and exploit it to their advantage. 
'And they can`t. The viruses don`t behave like that and they never will. They merely come back to haunt you as something different.' 
Multiple epidemiological studies have allegedly ruled out this chilling scenario as a factor in autism -- the Institute of Medicine calls it 'theoretical only.' But epidemiology is only as good as its data and its practitioners, and well-known for its potential pitfalls and flaws. What concerns me is, if the epidemiology is wrong, preventable cases of autism are going to keep happening till the cows come home. 
Recall, also, that Wakefield never suggested banning the measles, mumps or rubella immunizations. He suggested separating them and giving them a year apart. 
Especially concerning are the stories that parent after parent tells about physical illness after the shots, followed by autistic regression. It`s kind of freaky, really, the way they keep popping up. 
After finishing the Pox series, I attended the Autism One convention in Chicago and happened to be interviewed by a Web-based documentary filmmaker. During a break, I asked how he got involved. He told me his daughter got the MMR, came down immediately with a 103-degree fever and regressed forthwith into autism. 
'It`s like someone took out her good brain and replaced it with a bad brain,' he said. It was that immediate. 
I had another conversation with the mother of fraternal twins who told me this story: Both sons were scheduled to get two shots -- the MMR and another vaccination -- on the same day at the same office visit. 
But -- oops -- the healthcare worker gave the first child two MMR shots, not the MMR and the second vaccine. That child soon developed autism; the second one didn`t. 
And I spoke recently with a Texas man whose son got the MMR in 1993; the injection site swelled up to the size of his father`s fist; he had seizures at the dinner table that night, and within days was spinning, flapping, chewing wood and not talking ever again. 
You get the picture. 'Anecdotal evidence.' But you have to wonder how many of these stories -- one is tempted to say, bodies -- must pile up before the medical authorities go back and take a fresh look at the issue. 
This blithe disregard for case histories -- for what parents, the supposed bedrock of our 'family-friendly' society, say -- is one of the most appalling features of the current climate surrounding autism research. In fact, Sen. Joseph Lieberman, D-Conn., has talked publicly of forcing the Centers for Disease Control and Prevention, which sets the childhood immunization schedule and stoutly rejects a link with autism, to actually go out and interview some of these parents. 
One person who is making things awkward for the authorities is Dr. Peter Fletcher, another British ne`er-do-well -- or, to use his official title, the former chief scientific officer at Britain`s Department of Health. 
As I noted in a column earlier this year, the Daily Mail reported: 'A former British government medical officer responsible for deciding whether medicines are safe has accused the government of `utterly inexplicable complacency` over the MMR triple vaccine for children.' 
The official, Dr. Peter Fletcher, became an expert witness for parents` lawyers, which of course creates a competing interest that needs to be factored in. But Fletcher said his new role gave him access to documents that deeply concerned him. 
'There are very powerful people in positions of great authority in Britain and elsewhere who have staked their reputations and careers on the safety of MMR and they are willing to do almost anything to protect themselves,' he said. 
Gosh, this is starting to get interesting, and not just for Andrew Wakefield. 
-- 
E-mail:  <EMAILADDRESS>  
To sign up for a free e-mail subscription  <URL>  


