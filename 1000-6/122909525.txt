



THE SAN FRANCISCO CHRONICLE (California) 
June 23, 2006 Friday Meredith May 

Warren Simmons -- Pier 39 developer, Chevy's diner founder 

Warren L. Simmons, developer of San Francisco's Pier 39,  commercial airline pilot and founder of the Chevy's Mexican  restaurant chain, died Wednesday at Queen of the Valley  Hospital in Napa. He was 79. 
Mr. Simmons was known as a charismatic entrepreneur who used  humor and grace to withstand five years of permit hearings  to get his restaurant and retail shops built on Pier 39. 
When he needed fresh cranberries for his seasonal margaritas  at Chevy's, he bought land in Chile and against everyone's  predictions turned the property into the world's largest  grower and processor of cranberries. 
Mr. Simmons' life as an entrepreneurial adventurer began at  age 9, peddling newspapers on the streets of San Francisco.  Two years later, he went to work as a soda jerk at Keiser's  Colonial Creamery in the Sunset District. 
During World War II, while enrolled at Lowell High School,  Mr. Simmons worked 12-hour shifts on the waterfront. At  school he was the head yell-leader, rallying the crowds  during sporting events. 
After graduating from UC Berkeley, he went to work as a  pilot for Pan American Airlines, a job he would hold from  1950 to 1970. 
In addition to his day job, he founded an electronics  factory, was a partner in a real estate and construction  business, imported monkeys from the Philippines for the  production of the Salk vaccine, started a department store  chain and founded the Tia Maria restaurant chain. 
After leaving Pan Am, Mr. Simmons turned his interest to  developing Pier 39. After five years of presenting his slide  show to neighborhood groups and city permit officials,  Simmons saw his restaurant and retail stores open on the  pier in 1978. 
As part of the remodel, he gave a nod to his alma mater and  launched the Pier 39 Blue and Gold Fleet to compete with the  other ferries operating in the area, the boats of the Red  and White fleet. 
Mr. Simmons sold Pier 39 in 1981, and five years later  opened a restaurant with his son and daughter-in-law in  Alameda and named it Chevy's. Within a decade his restaurant  had expanded to 38 eateries. 
When Mr. Simmons ran into a shortage of U.S.-grown  cranberries for margaritas, he solved the problem by  planting thousands of acres of cranberries near Santiago,  Chile. While many farmers predicted disaster, his farm  CranChile became the world's largest grower and producer of  cranberries. 
The Simmons family sold Chevy's to PepsiCo. in 1993. 
Besides business, Mr. Simmons' other big passion was tennis.  He had two courts installed at his Napa home and he was a  tennis champion at the nearby Silverado Country Club for  many years. 
Mr. Simmons married four times. He is survived by his  current wife, Caroline; sons "Scooter" Warren Jr. of  Belvedere, Stuart of Santa Clarita and Gregory of Aspen,  Colo.; daughter Leslie Brille of New York City; stepdaughter  Natasha Simmons of Berkeley; stepson Damian Simmons of Napa;  sister Margaret Brunhouse of San Jose, and six  grandchildren. 
Donations may be made to the Hanna Boys Center, P.O. Box  100, Sonoma, CA. 95476. 
A private family service is planned. 
GRAPHIC: PHOTO Warren Simmons bought land in Chile and turned it into the  world's largest grower of cranberries.  



