



"David Wright" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 
      Hep A-B      Twinrix      GlaxoSmithKline      *      * 


1 
PRESCRIBING INFORMATION 
TWINRIX® 
[Hepatitis A Inactivated & Hepatitis B (Recombinant) Vaccine] 
DESCRIPTION 
TWINRIX® [Hepatitis A Inactivated & Hepatitis B (Recombinant) Vaccine] is a  sterile 
bivalent vaccine containing the antigenic components used in producing  HAVRIX® (Hepatitis A 
Vaccine, Inactivated) and ENGERIX-B® [Hepatitis B Vaccine (Recombinant)].  TWINRIX is a 
sterile suspension of inactivated hepatitis A virus (strain HM175)  propagated in MRC5 cells, and 
combined with purified surface antigen of the hepatitis B virus. The  purified hepatitis B surface 
antigen (HBsAg) is obtained by culturing genetically engineered  Saccharomyces cerevisiae cells, 
which carry the surface antigen gene of the hepatitis B virus, in synthetic  media containing 
inorganic salts, amino acids, dextrose, and vitamins. Bulk preparations of  each antigen are 
adsorbed separately onto aluminum salts and then pooled during formulation. 
A 1.0-mL dose of vaccine contains not less than 720 ELISA Units of  inactivated hepatitis A 
virus and 20 mcg of recombinant HBsAg protein. One dose of vaccine also  contains 0.45 mg of 
aluminum in the form of aluminum phosphate and aluminum hydroxide as  adjuvants, amino 
acids, 5.0 mg 2-phenoxyethanol as a preservative, sodium chloride, phosphate  buffer, 
polysorbate 20, Water for Injection, traces of formalin (not more than 0.1  mg), a trace amount of 
thimerosal (<1 mcg mercury) from the manufacturing process, and residual  MRC5 cellular 
proteins (not more than 2.5 mcg). Neomycin sulfate, an aminoglycoside  antibiotic, is included in 
the cell growth media; only trace amounts (not more than 20 ng) remain  following purification. 
The manufacturing procedures used to manufacture TWINRIX result in a product  that contains 
no more than 5% yeast protein. 
TWINRIX is supplied as a sterile suspension for intramuscular  administration. The vaccine is 
ready for use without reconstitution; it must be shaken before  administration since a fine white 
deposit with a clear colorless supernatant may form on storage. After  shaking, the vaccine is a 
slightly turbid white suspension. 
CLINICAL PHARMACOLOGY 
Several hepatitis viruses (A, B, C, D, and E) are known to cause a systemic  infection resulting 
in major pathologic changes in the liver. Features of hepatitis A and  hepatitis B are described 
below. 
Hepatitis A: The hepatitis A virus (HAV) belongs to the picornavirus family.  Only one 
serotype of HAV has been described.1 
Hepatitis A is a highly contagious disease with the predominant mode of  transmission being 
person-to-person via the fecal-oral route. Infection has been shown to be  spread (1) by 
contaminated water or food; (2) by infected food handlers2; (3) after  breakdown in usual sanitary 
conditions or after floods or natural disasters; (4) by ingestion of raw or  undercooked shellfish 
(oysters, clams, mussels) from contaminated waters3; (5) during travel to  areas of the world with 
2 
poor hygienic conditions4; (6) among institutionalized children and adults5;  (7) in day-care 
centers6; and (8) by parenteral transmission, either blood transfusions or  sharing needles with 
infected people.7 
In the United States, attack rates for hepatitis A disease are cyclical and  vary by population. 
The rates have increased gradually from 10.4 per 100,000 in 1987 to 11.7 per  100,000 in 1996.8 
The incubation period for hepatitis A averages 28 days (range: 15 to 50  days).9 The course of 
hepatitis A infection is extremely variable, ranging from asymptomatic  infection to icteric 
hepatitis. However, most adults (76% to 97%) become symptomatic.10 Symptoms  range from 
mild and transient to severe and prolonged, and may include fever, nausea,  vomiting, and 
diarrhea in the prodromal phase, followed by jaundice in up to 88% of  adults, as well as 
hepatomegaly and biochemical evidence of hepatocellular damage.10 Recovery  is generally 
complete and followed by protection against HAV infection. However, illness  may be prolonged, 
and relapse of clinical illness and viral shedding have been described.11 Up  to 22% of adults who 
contract hepatitis A are hospitalized and approximately 100 patients die  annually in the United 
States from complications of hepatitis A.12 
Chronic shedding of HAV in feces has not been demonstrated, but relapses of  hepatitis A can 
occur in as many as 20% of patients11,13 and fecal shedding of HAV may recur  at this time.11 
Approximately 70% of pediatric patients less than 6 years of age infected  with hepatitis A are 
asymptomatic, and serve as a reservoir for infection among adults.12 
The presence of antibodies to HAV, as detected in a standardized assay  (HAVAB), is an 
indication of the presence of protective antibodies against hepatitis A  disease. Natural infection 
provides lifelong immunity even when antibodies to hepatitis A are  undetectable. At present, 
studies show the duration of protection afforded by TWINRIX against  hepatitis A lasts at least 4 
years.14 
Hepatitis B: The hepatitis B virus (HBV) belongs to a family of genetically  related 
DNA-containing animal viruses, which are hepatotropic. The incubation period  of hepatitis B 
ranges between 30 and 180 days. The mode of transmission of hepatitis B may  be: By contact 
(contaminated body secretions including semen, vaginal secretions, blood,  saliva); 
percutaneously (usually through accidental needlesticks or by sharing  needles with infected 
people); or by maternal-neonatal transmission.15 
HBV infection occurs throughout the world with highly variable prevalences.  A human 
reservoir of persistently infected persons is present in nearly all  communities of the world. In the 
United States, parenteral drug abuse, unprotected sexual activity,  occupationally acquired 
infection, or travelers returning from high prevalence countries may be the  principal mechanisms 
of HBV transmission. 
Clinical infection with hepatitis B may occur in 2 major forms: Asymptomatic  or symptomatic 
hepatitis. Asymptomatic HBV infection can be subclinical or inapparent. In  subclinical infection, 
patients have abnormal liver enzymes without jaundice, while inapparent  asymptomatic infection 
is identified only by serological testing. One in 4 adults who has  symptomatic disease has 
jaundice (anicteric/icteric hepatitis). 
3 
HBV infection can have serious consequences including acute massive hepatic  necrosis, 
chronic active hepatitis, and cirrhosis of the liver. As many as 90% of  infants and 6% to 10% of 
adults who are infected in the United States will become HBV carriers.12 An  estimated 200 to 
300 million people are chronic carriers of HBV worldwide.12 The Centers for  Disease Control 
and Prevention (CDC) estimates that there are approximately 1 million to  1.25 million chronic 
carriers of HBV in the United States.12 About 50,000 cases of hepatitis are  reported per year, 
about half of which are hepatitis B. Unreported cases may be 10 times  greater. Close contact 
(sexual contact or household contact) or exposure to blood from infected  individuals is 
associated with increased risk of infection. Those patients who become  chronic carriers can 
infect others and are at increased risk of developing primary hepatocellular  carcinoma. Among 
other factors, infection with HBV may be the single most important factor  for development of 
this carcinoma.12,16 
Reduced Risk of Hepatocellular Carcinoma: A clear link has been demonstrated  between 
chronic HBV infection and the occurrence of hepatocellular carcinoma. In a  Taiwanese study, 
the institution of universal childhood immunization against HBV has been  shown to decrease the 
incidence of hepatocellular carcinoma among children.17 In a Korean study in  adult males, 
vaccination against HBV has been shown to decrease the incidence of, and  risk of, developing 
hepatocellular carcinoma in adults.18 
There is no definitive treatment for acute HBV infection. However, those who  develop 
antibodies to HBsAg after active infection are protected against subsequent  infection. Antibody 
titers ?10 mIU/mL against HBsAg are recognized as conferring protection  against HBV.19 
Seroconversion is defined as an antibody titer ?1 mIU/mL. 
Clinical Trials: Immunogenicity in Adults: Sera from 1,551 healthy adult  volunteers ages 
17 to 70, including 555 male subjects and 996 female subjects, in 11  clinical trials were analyzed 
following administration of 3 doses of TWINRIX on a 0-, 1-, and 6-month  schedule. 
Seroconversion for antibodies against HAV was elicited in 99.9% of  vaccinees, and protective 
antibodies against HBV were detected in 98.5%, 1 month after completion of  the 3-dose series. 
Table 1. Immunogenicity in TWINRIX Worldwide Clinical Trials 
TWINRIX Dose N 
% Seroconversion 
for Hepatitis A* 
% Seroprotection 
for Hepatitis B? 
123 
1587 
1571 
1551 
93.8 
98.8 
99.9 
30.8 
78.2 
98.5 
*Anti-HAV titer ?assay cut-off: 20 mIU/mL (HAVAB Test) or 33 mIU/mL 
(ENZYMUN-TEST®). 
?Anti-HBsAg titer ?10 mIU/mL (AUSAB®). 
One of the 11 trials was a comparative trial conducted in a US population  given either 
TWINRIX (on a 0-, 1-, and 6-month schedule) or HAVRIX (0- and 6-month  schedule) and 
ENGERIX-B (0-, 1-, and 6-month schedule). The monovalent vaccines were given  concurrently 
in opposite arms. Of a total of 773 adults (ages 18 to 70 years) enrolled in  this trial, an 
4 
immunogenicity analysis was performed in 533 subjects who completed the  study according to 
protocol. Of these, 264 subjects received TWINRIX and 269 subjects received  HAVRIX and 
ENGERIX-B. Seroconversion against HAV and seroprotection against HBV are  shown in Table 
2. 
Table 2. Percentage of Seroconversion or Seroprotection Rates in the TWINRIX  US 
Clinical Trial 
Vaccine N Timepoint 
% Seroconversion 
for Hepatitis A* 
(95% CI) 
% Seroprotection 
for Hepatitis B? 
(95% CI) 
TWINRIX 264 Month 1 
Month 2 
Month 7 
91.6 
97.7 
99.6 (97.9-100.0) 
17.9 
61.2 
95.1 (91.7-97.4) 
HAVRIX and 
ENGERIX-B 
269 Month 1 
Month 2 
Month 7 
98.1 
98.9 
99.3 (97.3-99.9) 
7.5 
50.4 
92.2 (88.3-95.1) 
*Anti-HAV titer ?assay cut-off: 33 mIU/mL (ENZYMUN-TEST®). 
?Anti-HBsAg titer ?10 mIU/mL (AUSAB®). 
Since the immune responses to hepatitis A and hepatitis B induced by TWINRIX  were 
non-inferior to the monovalent vaccines, efficacy is expected to be similar  to the efficacy for 
each of the monovalent vaccines (Table 3). 
Table 3. Geometric Mean Titers in the TWINRIX US Clinical Trial 
Vaccine N Timepoint 
GMT to Hepatitis A 
(95% CI) 
GMT to Hepatitis B 
(95% CI) 
TWINRIX 263 
259 
264 
Month 1 
Month 2 
Month 7 
335 
636 
4756 (4152-5448) 
8 
23 
2099 (1663-2649) 
HAVRIX and 
ENGERIX-B 
268 
269 
269 
Month 1 
Month 2 
Month 7 
444 
257 
2948 (2638-3294) 
6 
18 
1871 (1428-2450) 
It was noted that the antibody titers achieved 1 month after the final dose  of TWINRIX were 
higher than titers achieved 1 month after the final dose of HAVRIX in these  clinical trials. This 
may have been due to a difference in the recommended dosage regimens for  these 2 vaccines, 
whereby TWINRIX vaccinees received 3 doses of 720 EL.U. of hepatitis A  antigen at 0, 1, and 
6 months, whereas HAVRIX vaccinees received 2 doses of 1440 EL.U. of the  same antigen (at 0 
and 6 months). However, these differences in peak titer have not been shown  to be clinically 
significant. 
Two clinical trials involving a total of 129 subjects demonstrated that  antibodies to both HAV 
and HBV persisted for at least 4 years after the first vaccine dose in a  3-dose series of 
TWINRIX, given on a 0-, 1-, and 6-month schedule. For comparison, after the  recommended 
immunization regimens for HAVRIX and ENGERIX-B, respectively, similar  studies involving a 
5 
total of 114 subjects have shown that seropositivity to HAV and HBV also  persists for at least 
4 years. 
The effect of age on immune response to TWINRIX was studied in 2 trials  comparing 
subjects over 40 years of age (n = 183, mean age = 48 in one trial and n =  72, mean age = 50 in 
the other) with those ?40 (n = 191; mean age 32.5). The response to the  hepatitis A component 
of TWINRIX declined slightly with age, but >99% of subjects achieved  protective antibody 
levels in both age groups, and antibody titers were comparable to 2 doses of  hepatitis A vaccine 
alone in age matched controls. 
The response to hepatitis B immunization is known to decline in vaccinees  over 40 years of 
age. TWINRIX elicited a seroprotective response to hepatitis B in 97% of  younger subjects and 
93% to 94% of the older subjects, as compared to 92% of older subjects given  hepatitis B 
vaccine alone. Geometric mean titers elicited by TWINRIX were 2,285 in the  younger subjects 
and 1,890 or 1,038 for the older subjects in the 2 trials. Hepatitis B  vaccine alone gave titers of 
2,896 in younger subjects and 1,157 in those over 40 years of age. 
It has been shown in open randomized clinical trials that combining the  hepatitis A antigen 
with the hepatitis B surface antigen in TWINRIX resulted in comparable  anti-HAV or 
anti-HBsAg titers, relative to vaccination with the individual monovalent  vaccines or the 
concomitant administration of each vaccine in opposite arms. 
Immune Response to Simultaneously Administered Vaccines: There have been no 
studies of concomitant administration of TWINRIX with other vaccines. 
INDICATIONS AND USAGE 
TWINRIX is indicated for active immunization of persons 18 years of age or  older against 
disease caused by hepatitis A virus and infection by all known subtypes of  hepatitis B virus. As 
with any vaccine, vaccination with TWINRIX may not protect 100% of  recipients. As 
hepatitis D (caused by the delta virus) does not occur in the absence of HBV  infection, it can be 
expected that hepatitis D will also be prevented by vaccination with  TWINRIX. 
TWINRIX will not prevent hepatitis caused by other agents such as hepatitis  C virus, 
hepatitis E virus, or other pathogens known to infect the liver. 
Immunization is recommended for all susceptible persons 18 years of age or  older who are, or 
will be, at risk of exposure to both hepatitis A and hepatitis B viruses,  including but not limited 
to: 
. Travelers: Persons traveling to areas of high/intermediate endemicity for  both HAV and 
HBV (see Table 4) who are at increased risk of HBV infection due to  behavioral or 
occupational factors. (See CLINICAL PHARMACOLOGY.) 
6 
Table 4. Hepatitis A and Hepatitis B Endemicity by Region 
Geographic Region HAV HBV 
Africa High High (most) 
Caribbean High Intermediate 
Central America High Intermediate 
South America (temperate) High Intermediate 
South America (tropical) High High 
South and Southeast Asia* High High 
Middle East? High High 
Eastern Europe Intermediate Intermediate 
Southern Europe Intermediate Intermediate 
Former Soviet Union Intermediate Intermediate 
*Japan: Low HAV and intermediate HBV endemicity. 
? Israel: Intermediate HBV endemicity. 
. Patients With Chronic Liver Disease, including: 
- alcoholic cirrhosis 
- chronic hepatitis C 
- autoimmune hepatitis 
- primary biliary cirrhosis 
. Persons at Risk Through Their Work: 
- Laboratory workers who handle live hepatitis A and hepatitis B virus 
- Police and other personnel who render first-aid or medical assistance 
- Workers who come in contact with feces or sewage 
. Others: 
- Healthcare personnel who render first-aid or emergency medical assistance. 
- Personnel employed in day-care centers and correctional facilities.  Residents of drug and 
alcohol treatment centers. Staff of hemodialysis units. 
- People living in, or relocating to, areas of high/intermediate endemicity  of HAV and who 
have risk factors for HBV. 
- Men who have sex with men. 
- Persons at increased risk of disease due to their sexual practices.20, 21 
- Patients frequently receiving blood products including persons who have  clotting factor 
disorders (hemophiliacs and other recipients of therapeutic blood products). 
- Military recruits and other military personnel at increased risk for HBV. 
- Users of injectable illicit drugs. 
- Individuals who are at increased risk for HBV infection and who are close  household 
contacts of patients with acute or relapsing hepatitis A and individuals who  are at increased 
risk for HAV infection and who are close household contacts of individuals  with acute or 
chronic hepatitis B infection. 
7 
CONTRAINDICATIONS 
Hypersensitivity to any component of the vaccine, including yeast and  neomycin, is a 
contraindication (see DESCRIPTION). This vaccine is contraindicated in  patients with previous 
hypersensitivity to TWINRIX or monovalent hepatitis A or hepatitis B  vaccines. 
WARNINGS 
There have been rare reports of anaphylaxis/anaphylactoid reactions  following routine clinical 
use of TWINRIX. (See CONTRAINDICATIONS.) 
The vial stopper is latex-free. The tip cap and the rubber plunger of the  needleless prefilled 
syringes contain dry natural latex rubber that may cause allergic reactions  in latex sensitive 
individuals. 
Hepatitis A and hepatitis B have relatively long incubation periods. The  vaccine may not 
prevent hepatitis A or hepatitis B infection in individuals who have an  unrecognized hepatitis A 
or hepatitis B infection at the time of vaccination. Additionally, it may  not prevent infection in 
individuals who do not achieve protective antibody titers. 
PRECAUTIONS 
General: As with other vaccines, although a moderate or severe acute illness  is sufficient reason 
to postpone vaccination, minor illnesses such as mild upper respiratory  infections with or without 
low-grade fever are not contraindications.22 
Multiple Sclerosis: Results from 2 clinical studies indicate that there is  no association 
between hepatitis B vaccination and the development of multiple sclerosis,23  and that vaccination 
with hepatitis B vaccine does not appear to increase the short-term risk of  relapse in multiple 
sclerosis.24 
TWINRIX should be administered with caution to people on anticoagulants, and  those with 
thrombocytopenia or a bleeding disorder since bleeding may occur following  intramuscular 
administration to these subjects. 
As with any vaccine, if administered to immunosuppressed persons or persons  receiving 
immunosuppressive therapy, the expected immune response may not be  obtained.25 
Before the injection of any vaccine, the physician should take all  reasonable precautions to 
prevent allergic or other adverse reactions, including understanding the use  of the vaccine 
concerned, and the nature of the side effects and adverse reactions that may  follow its use. 
Prior to immunization with any vaccine, the patient's history should be  reviewed. The 
physician should review the patient's immunization history for possible  vaccine sensitivity, 
previous vaccination-related adverse reactions, and occurrence of any  adverse event-related 
symptoms and/or signs in order to determine the existence of any  contraindication to 
immunization with TWINRIX and to allow an assessment of benefits and risks.  As with any 
parenteral vaccine, epinephrine injection (1:1,000) and other appropriate  agents used for the 
control of immediate allergic reactions must be immediately available should  an acute 
anaphylactic reaction occur. 
8 
A separate sterile syringe and needle or a sterile disposable unit must be  used for each patient 
to prevent the transmission of infectious agents from person to person.  Needles should be 
disposed of properly and should not be recapped. 
Information for Patients: Patients should be informed of the benefits and  risks of 
immunization with TWINRIX, and of the importance of completing the  immunization series. As 
with any vaccine, it is important when a subject returns for the next dose  in a series that he or she 
be questioned concerning the occurrence of any symptoms and/or signs after a  previous dose of 
the same vaccine and that adverse events be reported. The US Department of  Health and Human 
Services has established the Vaccine Adverse Events Reporting System (VAERS)  to accept 
reports of suspected adverse events after the administration of any vaccine  including, but not 
limited to, the reporting of events required by the National Childhood  Vaccine Injury Act of 
1986. The toll-free number for VAERS forms and information is  1-800-822-7967.26 
Carcinogenesis, Mutagenesis, Impairment of Fertility: TWINRIX has not been 
evaluated for its carcinogenic potential, mutagenic potential, or potential  for impairment of 
fertility. 
Pregnancy: Pregnancy Category C. Animal reproduction studies have not been  conducted with 
TWINRIX. It is also not known whether TWINRIX can cause fetal harm when  administered to a 
pregnant woman or can affect reproduction capacity. TWINRIX should be given  to a pregnant 
woman only if clearly indicated (see INDICATIONS AND USAGE). 
Pregnancy Exposure Registry: Healthcare providers are encouraged to register  pregnant 
women who receive TWINRIX in the GlaxoSmithKline vaccination pregnancy  registry by 
calling 1-888-825-5249. 
Nursing Mothers: It is not known whether TWINRIX is excreted in human milk.  Because 
many drugs are excreted in human milk, caution should be exercised when  TWINRIX is 
administered to a nursing woman. 
Pediatric Use: Safety and effectiveness in pediatric patients below the age  of 18 years have not 
been established. 
Geriatric Use: Clinical studies of TWINRIX did not include sufficient  numbers of subjects 
aged 65 and over to determine whether they respond differently from younger  subjects. 
ADVERSE REACTIONS 
In clinical trials involving the administration of 6,543 doses to 2,299  individuals and during 
routine clinical use of the vaccine outside the United States, TWINRIX has  been generally well 
tolerated. 
Of 773 volunteers who participated in the comparative trial conducted in the  United States, 
389 subjects received at least 1 dose of TWINRIX and 384 received at least 1  dose each of 
ENGERIX-B and HAVRIX as separate but simultaneous injections. Solicited  adverse events 
reported following the administration of TWINRIX are shown in Table 5,  compared with 
adverse events reported after administration of ENGERIX-B and HAVRIX. 
9 
Table 5. Rate of Adverse Events Reported After Administration of TWINRIX or 
ENGERIX-B and HAVRIX 
Adverse TWINRIX ENGERIX-B HAVRIX 
Event Dose 1 Dose 2 Dose 3 Dose 1 Dose 2 Dose 3 Dose 1 Dose 2 
Local 
(N = 385) 
% 
(N = 382) 
% 
(N = 374) 
% 
(N = 382) 
% 
(N = 376) 
% 
(N = 369) 
% 
(N = 382) 
% 
(N = 369) 
% 
Soreness 
Redness 
Swelling 
37 
84 
35 
94 
41 
11 
6 
41 
63 
25 
75 
30 
95 
53 
75 
47 
95 
Adverse TWINRIX ENGERIX-B and HAVRIX 
Event Dose 1 Dose 2 Dose 3 Dose 1 Dose 2 Dose 3 
General 
(N = 385) 
% 
(N = 382) 
% 
(N = 374) 
% 
(N = 382) 
% 
(N = 376) 
% 
(N = 369) 
% 
Headache 
Fatigue 
Diarrhea 
Nausea 
Fever 
Vomiting 
22 
14 
5441 
15 
13 
4331 
13 
11 
6220 
19 
14 
5741 
12 
93321 
14 
10 
3541 
Adverse reactions seen with TWINRIX were similar to those observed after  vaccination with 
the monovalent components. The frequency of solicited adverse events did not  increase with 
successive doses of TWINRIX. Most events reported were considered by the  subjects as mild 
and self-limiting and did not last more than 48 hours. 
Among 2,299 subjects in 14 clinical trials, the following adverse  experiences were reported to 
occur within 30 days following vaccination with the frequency shown below. 
Incidence 1% to 10% of Injections, Seen in Clinical Trials With TWINRIX: 
Local Reactions at Injection Site: Induration. 
Respiratory System: Upper respiratory tract infections. 
Incidence <1% of Injections, Seen in Clinical Trials With TWINRIX: 
Local Reactions at Injection Site: Pruritus, ecchymoses. 
Body as a Whole: Sweating, weakness, flushing, influenza-like symptoms. 
Cardiovascular System: Syncope. 
Gastrointestinal System: Abdominal pain, anorexia, vomiting. 
Musculoskeletal System: Arthralgia, myalgia, back pain. 
Nervous System: Migraine, paresthesia, vertigo, somnolence, insomnia,  irritability, agitation, 
dizziness. 
Respiratory System: Respiratory tract illnesses. 
Skin and Appendages: Rash, urticaria, petechiae, erythema. 
As with any vaccine, it is possible that expanded routine clinical use of  the vaccine could 
reveal rare adverse events. 
10 
Incidence <1% of Injections, Seen in Clinical Trials With HAVRIXa and/or 
ENGERIX-Bb: 
Body as a Whole: Tingling.b 
Cardiovascular System: Hypotension.b 
Gastrointestinal: Constipation,b dysgeusia.a 
Hematologic/lymphatic: Lymphadenopathy.a+b 
Musculoskeletal System: Elevation of creatine phosphokinase.a 
Nervous System: Hypertonic episode,a photophobia.a 
Postmarketing Reports With HAVRIX and/or ENGERIX-B: Since market  introduction, 
more than 61 million doses of HAVRIX and more than 600 million doses of  ENGERIX-B have 
been distributed worldwide (circa 2000).27 Voluntary reports of adverse  events in people 
receiving either ENGERIX-B or HAVRIX that have been reported since market  introduction of 
the vaccines include the following: 
Body as a Whole: Anaphylaxis/anaphylactoid reactions and allergic  reactions.a 
Hypersensitivity: Erythema multiforme including Stevens-Johnson syndrome,b  angioedema,b 
arthritis,b serum sickness-like syndrome days to weeks after vaccination  including 
arthralgia/arthritis (usually transient), fever, urticaria, erythema  multiforme, ecchymoses, and 
erythema nodosum.b 
Cardiovascular System: Tachycardia/palpitations.b 
Skin and Appendages: Erythema multiforme,a hyperhydrosis,a angioedema,a  eczema,b herpes 
zoster,b erythema nodosum,b alopecia.b 
Gastrointestinal System: Jaundice,a hepatitis,a abnormal liver function  tests,b dyspepsia.b 
Hematologic/lymphatic: Thrombocytopenia.b 
Nervous System: Convulsions,a paresis,b encephalopathy,a neuropathy,a+b  myelitis,a 
Guillain-Barré syndrome,a+b multiple sclerosis,a+b Bell's palsy,b transverse  myelitis,b optic 
neuritis.b 
Respiratory System: Dyspnea,a bronchospasm including asthma-like symptoms.b 
Special Senses: Conjunctivitis,b keratitis,b visual disturbances,b  tinnitus,b earache.b 
Other: Congenital abnormality.a 
aFollowing HAVRIX. 
bFollowing ENGERIX-B. 
a+bFollowing either HAVRIX or ENGERIX-B. 
DOSAGE AND ADMINISTRATION 
TWINRIX should be administered by intramuscular injection. Do not inject  intravenously or 
intradermally. In adults, the injection should be given in the deltoid  region. TWINRIX should 
not be administered in the gluteal region; such injections may result in a  suboptimal response. 
For individuals with clotting factor disorders who are at risk of hemorrhage  following 
intramuscular injection, the ACIP recommends that when any intramuscular  vaccine is indicated 
for such patients, ". . . it should be administered intramuscularly if, in  the opinion of a physician 
11 
familiar with the patient's bleeding risk, the vaccine can be administered  with reasonable safety 
by this route. If the patient receives antihemophilia or other similar  therapy, intramuscular 
vaccination can be scheduled shortly after such therapy is administered. A  fine needle (23 gauge 
or smaller) can be used for the vaccination and firm pressure applied to the  site (without rubbing) 
for at least 2 minutes. The patient should be instructed concerning the risk  of hematoma from the 
injection."28 
When concomitant administration of other vaccines or immunoglobulin (IG) is  required, they 
should be given with different syringes and at different injection sites. 
Preparation for Administration: Shake vial or syringe well before withdrawal  and use. 
Parenteral drug products should be inspected visually for particulate matter  or discoloration prior 
to administration. With thorough agitation, TWINRIX is a slightly turbid  white suspension. 
Discard if it appears otherwise. 
The vaccine should be used as supplied; no dilution or reconstitution is  necessary. The full 
recommended dose of the vaccine should be used. After removal of the  appropriate volume from 
a single-dose vial, any vaccine remaining in the vial should be discarded. 
Primary immunization for adults consists of 3 doses, given on a 0-, 1-, and  6-month schedule. 
Each 1-mL dose contains 720 EL.U. of inactivated hepatitis A virus and 20  mcg of hepatitis B 
surface antigen. 
STORAGE 
Store refrigerated between 2o and 8o C (36o and 46o F). DO NOT FREEZE;  discard if 
product has been frozen. Do not dilute to administer. 
HOW SUPPLIED 
TWINRIX is supplied as a slightly turbid white suspension in vials and  prefilled TIP-LOK® 
syringes containing a 1.0-mL single dose. 
Single-Dose Vials 
NDC 58160-850-01 (package of 1) 
NDC 58160-850-11 (package of 10) 
Single-Dose Prefilled Disposable TIP-LOK Syringes (packaged without needles) 
NDC 58160-850-46 (package of 5) 
REFERENCES 
1. Day SP, Lemon SM. Hepatitis A virus. In: Gorbach SL, Bartlett JG,  Blacklow NR, eds. 
Infectious diseases. Philadelphia, PA: WB Saunders Company; 1992:1787-1791.  2. Dienstag JL, 
Routenberg JA, Purcell RH, et al. Foodhandler-associated outbreak of  hepatitis type A. An 
immune electron microscopic study. Ann Intern Med 1975;83:647-650. 3.  Mackowiak PA, 
Caraway CT, Portnoy BL. Oyster-associated hepatitis: Lessons from the  Louisiana experience. 
Am J Epidemiol 1976;103(2):181-191. 4. Woodson RD, Clinton JJ. Hepatitis  prophylaxis abroad. 
Effectiveness of immune serum globulin in protecting Peace Corps volunteers.  JAMA 
1969;209(7):1053-1058. 5. Krugman S, Giles JP. Viral hepatitis. New light on  an old disease. 
12 
JAMA 1970;212(6):1019-1029. 6. Hadler SC, Erben JJ, Francis DP, et al. Risk  factors for 
hepatitis A in day-care centers. J Infect Dis 1982;145(2):255-261. 7. Hadler  SC. Global impact 
of hepatitis A virus infection changing patterns. In: Hollinger FB, Lemon  SM, Margolis H, eds. 
Viral hepatitis and liver disease. Baltimore, MD: Williams & Wilkins;  1991:14-20. 8. Centers 
for Disease Control and Prevention. Summary of Notifiable Diseases, United  States, 1996. 
MMWR 1997;45(53):73. 9. Centers for Disease Control and Prevention.  Prevention of hepatitis 
A through active or passive immunization: Recommendations of the Advisory  Committee on 
Immunization Practices (ACIP). MMWR 1999;48(RR-12):1-31. 10. Lemon SM. Type  A viral 
hepatitis. New developments in an old disease. N Engl J Med  1985;313(17):1059-1067. 11. 
Sjogren MH, Tanno H, Fay O, et al. Hepatitis A virus in stool during  clinical relapse. Ann Intern 
Med 1987;106:221-226. 12. Centers for Disease Control and Prevention.  Atkinson W, Wolfe C, 
Humiston S, Nelson R (eds). Epidemiology and prevention of  vaccine-preventable diseases. 6th 
ed. Atlanta, GA: Public Health Foundation; 2000:191-229. 13. Chiriaco P,  Guadalupi C, 
Armigliato M, et al. Polyphasic course of hepatitis type A in children. J  Infect Dis 
1986;153(2):378-379. 14. Data on file (TWR101), GlaxoSmithKline. 15. Koff  RS. Hepatitis B 
and hepatitis D. In: Gorbach SL, Bartlett JG, Blacklow NR, eds. Infectious  diseases. 
Philadelphia, PA: WB Saunders Company; 1992:709-716. 16. Beasley RP, Hwang  LY, Stevens 
CE, et al. Efficacy of hepatitis B immune globulin for prevention of  perinatal transmission of the 
hepatitis B virus carrier state: Final report of a randomized double-blind,  placebo-controlled trial. 
Hepatology 1983;3(2):135-141. 17. Chang MH, Chen CJ, Lai MS. Universal  hepatitis B 
vaccination in Taiwan and the incidence of hepatocellular carcinoma in  children. N Engl J Med 
1997;336(26):1855-1859. 18. Lee MS, Kim DH, Kim H, et al. Hepatitis B  vaccination and 
reduced risk of primary liver cancer among male adults: A cohort study in  Korea. Int J 
Epidemiol 1998;27:316-319. 19. Frisch-Niggemeyer W, Ambrosch F, Hofmann H.  The 
assessment of immunity against hepatitis B after vaccination. J Bio Stand  1986;14(3):255-258. 
20. Centers for Disease Control and Prevention. 1998 Guidelines for  treatment of sexually 
transmitted diseases. MMWR 1999;47(RR-1):99-104. 21. Centers for Disease  Control and 
Prevention. Hepatitis surveillance report No. 57. Atlanta, GA: DHHS;  2000:12. 22. Centers for 
Disease Control and Prevention. Health information for international travel,  1999-2000. Atlanta, 
GA: DHHS. 23. Ascherio A, Zhang SM, Hernán MA, et al. Hepatitis B  vaccination and the risk 
of multiple sclerosis. N Engl J Med 2001;344(5):327-332. 24. Confavreux C,  Suissa S, Saddier 
P, et al. Vaccination and the risk of relapse in multiple sclerosis. N Engl  J Med 
2001;344(5):319-326. 25. Centers for Disease Control and Prevention.  Recommendations of the 
Advisory Committee on Immunization Practices (ACIP): Use of vaccines and  immune globulins 
for persons with altered immunocompetence. MMWR 1993;42(RR-4):1-18. 26.  Centers for 
Disease Control and Prevention. Vaccine adverse event reporting system -  United States. 
MMWR 1990;39(41):730-733. 27. Data on file (TWR201), GlaxoSmithKline. 28.  Centers for 
Disease Control and Prevention. General recommendations on immunization.  Recommendations 
of the Advisory Committee on Immunization Practices (ACIP). MMWR  1994;43(RR-1):23. 
13 
Manufactured by GlaxoSmithKline, Rixensart, Belgium, US License No. 1617 
Distributed by GlaxoSmithKline, Research Triangle Park, NC 27709 
TWINRIX, HAVRIX, ENGERIX-B, and TIP-LOK are registered trademarks of 
GlaxoSmithKline. ENZYMUN-TEST is a registered trademark of Boehringer  Mannheim 
Immunodiagnostics. AUSAB is a registered trademark of Abbott Laboratories. 
©2003, GlaxoSmithKline. All rights reserved. 
August 2003 TW:L5 





