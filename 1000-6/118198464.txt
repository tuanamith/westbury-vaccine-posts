


At any age, prevention is key to women's health 

By Jodi Mailander Farrell Knight Ridder Newspapers 
Dr. Onelia Lage's teenage patients look nothing like the fragile-boned, curved-shouldered elderly victims of osteoporosis, yet the advice she gives them today - go for the grilled cheese for lunch, join a sports team or dance regularly, don't watch TV or play on the computer more than two hours a day - are tips that could prevent young girls from developing the painful disease. 
It's the kind of lifespan approach to health that doctors are preaching to their patients - that is, how you treat your body as a child, teenager and young adult has a direct impact on your quality of life as a senior citizen. 
"Good nutrition really begins in infancy," says Lage, an associate professor of clinical pediatrics at the University of Miami and an adolescent medicine board certified specialist. 
As new research delves into the causes of diseases and how they may differ for females and males - heart disease, for example - there is an emerging emphasis on prevention, especially for problems that are exclusively female. 
In addition to osteoporosis - which women are four times more likely than men to develop - preventive care for young to middle-age women includes mammograms, breast exams, pap smears and cholesterol screenings. 
You may know that heart disease, cancer and stroke are the leading causes of death for women in the United States. But did you know an unhealthy diet and lack of physical exercise contribute more to disease and death than alcohol use, flu, car accidents, firearms, sexual behavior and illegal drugs. 
The prescription for fighting some diseases can be as simple as adding 15 minutes to your workout or taking a morning walk. One recent study found that duration of exercise increases the degree of protection against cardiovascular disease, breast cancer and diabetes. 
Much of what we know about women's health today comes courtesy of the Women's Health Initiative, the most comprehensive study of postmenopausal women's health conducted in this country. From revealing the effects of a low-fat diet and calcium supplements to the dangers of hormone replacement therapy, the study has helped to advance the health care of women for more than a decade. 
"There's so much information available for women in the last 10 to 15 years, and it's out there now in magazines, on TV and the Internet," says Jo Parrish, vice president of the Society for Women's Health Research, a Washington, D.C.-based nonprofit that encourages the study of differences between women and men. 
What's so different about women? Just a sampling: 
Women wake from anesthesia more quickly than men. 
Medications, such as antihistamines and antibiotics, cause different side effects in women, partly due to women's hormonal cycles. 
Women are more susceptible to immunity diseases like multiple sclerosis and lupus. 
Women who are the same weight and consume the same amount of alcohol as men have higher blood alcohol levels because they don't have a certain enzyme in their stomach that helps men break alcohol down faster. 
Women are more likely to contract a sexually transmitted disease. 
Approaching health from a long-term, big-picture perspective is important because we're living longer. Today, the approximate life expectancy is 78 years for men and 82 years for women. 
But while longer life spans appear to be good news, there is a growing concern that our "health span" is shrinking. People may be living longer, but they're spending more years unhealthy. 
"With longevity comes the fact that we may have more than one chronic condition," Parrish says. "But if we can have a healthy weight and maintain bone density, then those issues of diabetes, cardiovascular disease and other diseases so common in the elderly don't have to take place." 

Starting in childhood 
Poor eating habits can start as early as infancy, with parents introducing solid foods too early or giving their babies too much cereal, says Lage, the adolescent physician who sees patients from ages 7 to 25 in her Miami office. The cycle continues with parents insisting that older children "clean the plate" at meals. 
"Kids are really good about regulating their appetites," Lage says. "They know when they're hungry. They're sometimes nibblers. Parents just need to make sure they're getting the fruits, vegetables and proteins throughout the day." 
Physical activity is critical, too. Kids should spend no more than two hours a day at the computer, watching TV or playing video games, according to the American Academy of Pediatrics. 
Lage says some of her young female patients who are overweight have high blood pressure, blood sugar that is borderline diabetic and menstrual cycles that are stalled or irregular. The hormonal imbalances caused by obesity are of particular concern because menstrual cycles are critical to bone health, Lage says. 
"This is where they start loading their bones so they're ready when they hit menopause to have enough stored bone density so they're less likely to have fractures from osteoporosis," Lage says. 
Most girls should develop breast buds around age 10. The average age of U=2ES. girls who get their first period is 12 to 12=BD. 
But if a girl is overweight, that development may never happen or be delayed. In extreme cases, overweight girls develop male characteristics: deeper voices, body hair. Lage says she had one obese patient whose mother had to shave her three times a day because she began growing a beard. 
The tween gap 
Girls from pre-teens to the college years can easily fall through medical care cracks because they often stop seeing a pediatrician and aren't ready to see a gynecologist yet. 
Yet this point in a girl's health can be crucial. There are body issues related to anorexia and bulimia, and addiction problems often develop in this stage of life. Studies show girls develop nicotine addictions faster than boys and progress from alcohol use to abuse faster than boys, Parrish says. 
Girls this age also are more susceptible to stress and low self-esteem from peer pressure. 
"Our goal at this age is body-mind-spirit," Lage says. "You can't forget about mental health. With a lot of my girls, I'm prescribing yoga. A lot of times there's a lack of nurturing at this age. I hug my patients." 
Statistics show that about 25 percent of 15-year-old boys and girls in the United States have had sexual intercourse. About 35 percent of teen girls become pregnant at least once between 15 and 19, according to the National Center for Health Statistics. STDs - gonorrhea, syphilis, herpes simplex and chlamydia, among others - are generally graver in females. Untreated, these STDs can lead to infertility or cause miscarriage, premature birth or newborn infections. 
If a teenage girl is sexually active, doctors recommend that she get an annual screening for chlamydia, which is most prevalent in ages 15 to 19. Another important vaccine, now required by schools in some states, is for Hepatitis B, which can cause scarring of the liver, liver cancer or failure and death. 
Young women 
Now that women are bearing children later in life, infertility and reproductive issues are the leading health concerns among women ages 25-44. More than 6 million Americans suffer from infertility, half of them women. 
But the focus on reproduction overshadows other mounting problems: cancer, heart disease and HIV/AIDS. HIV among American women is on the rise and is now the leading cause of death for black women. 
Smoking and its related problems also are on the rise. Smoking at this age has long-term consequences; smoking and taking oral contraceptives after age 35 is an especially deadly combination. 
Many of the preventive screenings and tests necessary at this age - Pap smears, pelvic exams, breast exams and urine tests for STDs - occur in the office of a gynecologist, who often becomes the primary doctor for young women. 
The American Cancer Society recommends that a woman get her first baseline mammogram between the ages of 35 and 40. 
And if a woman has a history of heart disease, high blood pressure or stroke, she should get an HDL blood cholesterol test and a blood pressure check every one to two years. 
Middle years, 45-64 
Menopause, arthritis, osteoporosis-related fractures, heart disease and breast and cervical cancer are the big issues. 
"A colorectal cancer screening is hugely important," Lage says. "So many patients come annually for a Pap smear and won't get a colorectal screening. It's the second-leading factor of cancer mortality in women over 50 (behind lung cancer and ahead of breast cancer). Women are very focused on their breasts; nobody wants to focus on their colon." 
Most insurance policies now cover some type of colon screening, which is recommended every 10 years. If a parent had colorectal cancer, get a colonoscopy 10 years earlier than the age he or she was first diagnosed. 
Only 61 percent of women over 50 had undergone a complete physical exam in the past year; 64 percent a Pap test; 66 percent a breast exam; 55 percent a blood cholesterol test and 69 percent a mammogram, according to a study commissioned by Women's Policy Inc., a Washington, D=2EC.-based nonprofit. 
Senior years, 65 and over 
In women 65-74, cancer is the leading cause of death, while heart disease dominates in those over 75. 
The relationship between sex hormones and blood vessels is likely what keeps heart disease rates low in women before menopause. But a popular way to combat the problem in older women - hormone replacement therapy - has become more complicated today, ever since a major clinical trial found increases in breast cancers, heart disease, strokes and blood clots in women taking estrogen-progestin pills. Some women have stopped taking the pills, but many are going on lower doses or using them only for a short term. 
Additionally, 23 million women in the U.S. have osteoporosis. One of the most important screenings for women in their 60s is a bone scan for osteoporosis. 
Women in their senior years should keep up with regular blood pressure and blood cholesterol checks, and blood sugar tests. They also should be immunized against pneumonia if they haven't bee 


