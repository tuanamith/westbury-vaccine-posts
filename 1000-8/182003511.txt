



T Pagano wrote: 
I don't see where he said that.  He simply said that understanding it is a prerequisite for criticizing it. Do you disagree? 
It happens to be true, however, that I cannot remember a creationist posting here who understood it. I suspect there are some, but they don't post here, because they know the difference between science and religious faith, and they understand that they don't have anything to contribute to a scientific argument. Nor would they likely be interested in one. 
I suppose I may have missed somebody, however. If you know of any creationist who has posted here and correctly represented evolutionary theory, perhaps you would be kind enough to offer a link. 

People who honestly search for the truth of a complicated matter, and have incomplete evidence, will disagree at times on the most likely explantion, yes. What is your point? Are you suggesting that theists all agree? All Christians? All Protestants? 

OneLook Dictionary, definition 2 of 2: "viable: adjective:   capable of being done with means at hand and circumstances as they are" 
Seems to be an apt word. "Objectively true" can only be known beyond reasonable doubt, never with certainty. That's for math and other closed systems of logic. Whenever you come to a conclusion based on evidence, there will be a possibility of being wrong, however small it may be. 
As opposed to fundamentalist religious doctrines, which are not checked against reality, and whose adherents *feel certain. Since they are unwilling to consider the possibility that they are wrong, it is nearly certain that they *are, in at least some respects. 
You, like the others of this mindset, claim that God is infallible, but offer contradicting interpretations. Obviously it is you yourselves that you are claiming are infallible, not any god. 
Pounding the pulpit as my grandfather did, or typing in all caps, as some have done here, or citing bible verses as most creationists eventually do, or quoting a personal cult leader, as one of our regular creationists does ...is not verifiable evidence. 

Oh? Could you briefly describe one? 

What in Sam Hill does that even mean? How could we know something is objectively true without comparing it to external reality? You are clumsily groping for the term "scientific method". The products of science - vaccines, engineered crops, rocket ships, the internet - are convincing evidence that it works better than anything else for understanding the external world (that is, not in our minds or a social construct). 
What do you have as an alternative, that can be so successfully verified objectively? 

Which discordant evidence were you thinking of, or is this entirely hypothetical? 

You clearly do not undertand the scientific process. Reputations are established by any young punk who can overturn a model established by one of the grand old men of science. 

Bwahahahaha! 
And sparrows dug burrows in the Garden, and snakes played patty-cake, and pigs flew about the flower fields, singing cheerful song and sipping nectar. You do believe your god is quite mad, don't you? 
Most Christians dispute vegetarian T. Rexes in the Garden. Ask a rabbi about it; his people have had longer to think about these myths. In any event, what does this dream analysis have to do with science? 

Well, except there was no Garden of Eden, The Earth is about 750,000 times older than the bible says, there was no Global Flood, there was no Tower of Babel, Jericho fell centuries before Joshua lived, the Hebrews were never slaves in Egypt, and there was no census during the reign of Augustus requiring people to return to their home towns. 
But who's quibbling about details? 

Except there was no Garden. And what makes you think that this means all the animals that lived, lived in the Garden? And what makes you think you understand what "lived in peace" means? 

T. Rex died about 65,000,000 years before the first modern humans. 

Well, no. Do you have *any evidence? Here's an alternative history, with evidence as strong as yours: "Odin, the son of Bur's son Borr, killed Ymir. The blood pouring out of the giant's body killed all the frost giants Ymir had created, except Bergelmir. From the Ymir's dead body, Odin created the world. Ymir's blood was the sea; his flesh, the earth; his skull, the sky; his bones, the mountains; his hair, the trees. The new Ymir-based world was Midgard [Middle Earth]. Ymir's eyebrow was used to fence in the area where mankind would live. Around Midgard was an ocean where a serpent, Jormungand, who was big enough to form a ring around Midgard by putting his tail in his mouth, lived." 
From:  <URL>  

Kermit 


