


There's a piece of writing known simply as "Welcome to Holland" which is well known and insprational to many perants of special-needs kids (not to mention very true). 
Recently someone reinterpretted it in a way my wife and I find as true, as inspirational, and as charming, in its own way, as the oiriginal to which it repsonds.  Both are below.  Enjoy 



                                                               Welcome To Holland                                                                 by: Emily Perl Kingsley 


But there's been a change in the flight plan. They've landed in Holland and there you must stay. 
It=A2s just a different place. It's slower-paced than Italy, less flashy than Italy. But after you've been there for a while and you catch your breath, you look around.... and you begin to notice that Holland has windmills....and Holland has tulips. Holland even has Rembrandts. 
And the pain of that will never, ever, ever, ever go away... because the loss of that dream is a very, very significant loss. 
=A91987 by Emily Perl Kingsley All rights reserved 


Holland....Schmolland! 

Having a child with special needs is supposed to be like this -- not any worse than having a typical child -- just different. 
When I read this my son was almost 3, completely non-verbal and was hitting me over 100 times a day. While I appreciated the intention of the story, I couldn't help but think, "Are they kidding? We're not in some peaceful country dotted with windmills. We are in a country under siege -- dodging bombs, boarding overloaded helicopters, bribing officials -- all the while thinking, "What happened to our beautiful life?" 
That was five years ago. 
My son is now 8 and though we have come to accept that he will always have autism, we no longer feel like citizens of a battle-torn nation. With the help of countless dedicated therapists and teachers, biological interventions, and an enormously supportive family, my son has become a fun-loving, affectionate boy with many endearing qualities and skills. In the process we've created . . . well . . . our own country, with its own unique traditions and customs. 
It's not a war zone, but it's still not Holland. Let's call it Schmolland. In Schmolland, it's perfectly customary to lick walls, rub cold pieces of metal across your mouth and line up all your toys end-to-end. You can show affection by giving a "pointy chin." A "pointy chin" is when you act like you are going to hug someone and just when you are really close, you jam your chin into the other person's shoulder. For the person giving the "pointy chin" this feels really good, for the receiver, not so much -- but you get used to it. 
For citizens of Schmolland, it is quite normal to repeat lines from videos to express emotion. If you are sad, you can look downcast and say, "Oh, Pongo." When mad or anxious, you might shout, "Snow can't stop me!" or "Duchess, kittens, come on!" Sometimes, "And now our feature presentation" says it all. 
In Schmolland, there's not a lot to do, so our citizens find amusement wherever they can. Bouncing on the couch for hours, methodically pulling feathers out of down pillows, and laughing hysterically in bed at 4:00 a.m. are all traditional Schmutch pastimes. 
The hard part of living in our country is dealing with people from other countries. We try to assimilate ourselves and mimic their customs, but we aren't always successful. It's perfectly understandable that an 8 year-old from Schmolland would steal a train from a toddler at the Thomas the Tank Engine Train Table at Barnes and Noble. But this is clearly not understandable or acceptable in other countries, and so we must drag our 8 year-old out of the store kicking and screaming, all the customers looking on with stark, pitying stares. But we ignore these looks and focus on the exit sign because we are a proud people. 
Where we live it is not surprising when an 8 year-old boy reaches for the fleshy part of a woman's upper torso and says, "Do we touch boodoo?" We simply say, "No, we do not touch boodoo," and go on about our business. It's a bit more startling in other countries, however, and can cause all sorts of cross-cultural misunderstandings. 
And, though most foreigners can get a drop of water on their pants and still carry on, this is intolerable to certain citizens in Schmolland, who insist that the pants must come off no matter where they are and regardless of whether another pair of pants is present. 
Other families who have special needs children are familiar and comforting to us, yet are still separate entities. Together we make up a federation of countries, kind of like Scandinavia. Like a person from Denmark talking to a person from Norway (or in our case, someone from Schmenmark talking to someone from Schmorway.), we share enough similarities in our language and customs to understand each other, but conversations inevitably highlight the diversity of our traditions. "My child eats paper. Yesterday he ate a whole video box." "My daughter only eats four foods, all of them white." "We finally had to lock up the VCR because my child was obsessed with the rewind button." "My son wants to blow on everyone." 
There is one thing we all agree on. We are a growing population. Ten years ago, 1 in 10,000 children had autism. Today the rate is approximately 1 in 250. Something is dreadfully wrong. Though the causes of the increase are still being hotly debated, a number of parents and professionals believe genetic predisposition has collided with too many environmental insults -- toxins, chemicals, antibiotics, vaccines -- to create immunological chaos in the nervous system of developing children. One medical journalist speculated these children are the proverbial "canary in the coal mine", here to alert us to the growing dangers in our environment. 
While this is certainly not a view shared by all in the autism community, it feels true to me. 
I hope that researchers discover the magic bullet we all so desperately crave. And I will never stop investigating new treatments and therapies that might help my son. But more and more my priorities are shifting from what "could be" to "what is." I look around this country my family has created, with all its unique customs, and it feels like home. For us, any time spent "nation building" is time well spent. -- The End -- 


