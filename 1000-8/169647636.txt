



Thanks for posting this, Lou. 
Here is another article about Irwin Buffet, with a photograph of him: 
Flooded Seniors Face a Tarnished Twilight They May Not Be Equipped to Handle 
 <URL>  
FROM:  The New Orleans Times-Picayune ~ By Chris Bynum, Staff Writer 
A picture to Irwin Buffet is worth a thousand words. But now that his pictures have been washed away by Hurricane Katrina, the 88-year-old photographer can only stand silently in the ruins of the Lakeview home he built 54 years ago. 
A matted black-and-white photograph of sunlight filtering through an iron cross at Metairie Cemetery lies inside, puckered by floodwaters. Buffet remembers how the angle of the sunlight was something he had been patient to capture. 
"I had to go back four times to get the sun just right," he says. 
That was back when there was time for patience, when most of Buffet's life was ahead of him. 
Buffet paid $6,000 for a Lakeview lot the same year Jonas Salk invented the polio vaccine and Princess Elizabeth became queen of England. Since that time, he has made a little history of his own, becoming a grandfather, a widower and a retiree. 
"My future is behind me now," Buffet says. 
He, like many of the elderly impacted by Katrina, is not battling just the aftermath of a storm and the uncertainty of a future. Buffet is racing against time. It's not an easy race for a man whose steps are now much slower and less sure. He is painfully aware that while others can afford to wait -- for FEMA, for insurance adjusters, for contractors, for garbage men, for the doctor, for neighbors to return, for a whole hometown to recover -- the clock is no longer on his side. 
That awareness adds another layer of stress to the post-K lives of senior citizens, who face a unique set of emotional and medical challenges in their tarnished twilight years. 
"This is a generation that is very self-reliant with a strong work ethic," says Dr. Michael Knight, a psychiatrist at Ochsner Clinic Foundation. "They have a pull-yourself-up-by-your-bootstraps mentality. They are accustomed to doing everything on their own." 
To suddenly cede control of seemingly every aspect of your day-to-day life is particularly difficult for someone like Buffet, who served in the Navy during World War II. 
"I have lost my house, my hobby, my friends and my car," he says. 
His car was his independence. And now that it is gone, Buffet says he has reached the end of his driving days and the end of his freedom to come and go as he pleases. 
His only daughter, Janis Shreve, lived in the other side of his Lakeview double with her husband. The three of them now share a residence in Ascension Parish, 45 miles from Buffet's friends. 
This loss of community, which has been hard on Buffet, is even more devastating for elderly people struggling with dementia. 
"They have lost familiar surroundings, their routine, familiar faces and family coming to visit," Knight says. "Many are having failure to thrive. They are more confused and agitated." 
More cognitively intact seniors "might be experiencing depression and even despair," says Dr. Rick Streiffer, chairman of the department of family and community at Tulane University School of Medicine. "When people look around, they see that what was once familiar is now changed, or gone. Many say they are ready to die. Some have refused treatment." 
Many others are more independent and have become involved in fixing their homes or helping family members cope. 
"The elderly are living with the same hassles as the rest of us: insurance, repairs, etc.," Streiffer says. How well they cope, he says, has much to do with their pre-Katrina personalities. 
As with any age group, support systems are key to recovery. New Orleans, at the moment, does not offer many. 
"People are looking for things to do," says Debbie Pesses, director of older adult programs for the Jewish Community Center. "They don't want to be alone." 
Pesses organizes such activities as book clubs and a Katrina oral history project for the elderly at the community center to give to their families. 
What she has observed among those who have returned to the city is that many elderly can't comprehend the slow recovery process. 
"They don't understand why it takes so long, something as simple as calling a cab," she says. "When they go to a restaurant early, they don't understand the long lines and the limited menu or why they have to wait overnight for a prescription to be refilled. They think things should be back to normal. They don't realize we have a new normal." 
One of the harsh realities, Streiffer says, is that New Orleans may not be the best place for the elderly. 
"They can't stop talking about coming back. But I don't think this is a very safe place if you need urgent medical care," he says. "The medical system is stretched. ... Home health agencies are low-staffed." 
The solution, he says, is time. But time is not on the side of the elderly. So rather than focus on the city's recovery, the elderly should concentrate on healing their own psyches. 
Acceptance is the first step to healing, the experts say. And that begins with making sure that those who need to visit their ruined homes are given that opportunity, says Howard Rodgers of the New Orleans Council on Aging. 
"There is a need for closure," he says. 
They also need patience and understanding, he says. 
"I've gotten many phone calls from individuals who were part-time caregivers to a parent. Maybe they stopped in once a day, once a week, once a month. Now the elderly parent is living with them. Neither has ever been in that position before. Patience is the best lesson the caregiver can learn," Rodgers says. 
And while the child taking care of the parent may think he is the one facing the challenge, it works both ways. 
A 90-year-old woman now living with her children told Streiffer: "They want me to be closer to them. It's not always easy to be near your children because you have to give so much." 
And that, he says, is the biggest lesson we have learned about tending to the needs of the elderly: Psychological and social factors are as vital in treatment of a patient as biological factors. It's a lesson to all the baby boomers, he says. 
"I'm hoping that my generation will learn from this and we will talk to our children about what we want as we face the aging process," Streiffer says. "It should open a dialogue." 
"Family is a primeval support for the elderly to get through the grief process and move on," Knight adds. "But the biggest issue is for the patient to come to acceptance." 
The acceptance he describes does not end with facing the destruction of a home or the loss of friends. It is a realization that the person has survived a major event, is still in relatively good health, is perhaps still independent. All these factors focus on the positive, rather than the negative outcome. 
"In many cases, faith can be pivotal and can improve internal stamina. The holding pattern is what is so difficult. That is why many look for emotional and spiritual support outside of themselves and through a church or their families," Knight says. 
In this context of support, he says, it is important for the elderly not to feel guilt. 
"People often get stuck on if-I-had-done-this-or-that," Knight says. "You have to let go of these things -- not just the circumstances, but also the guilt that many elderly feel by thinking they are a burden. They have done a lot for themselves and for others." 
The other kind of acceptance, he says, is "accepting help." These elements can provide a sense of strength for moving forward. 
Buffet spent $20,000 building his house in Lakeview more than half a century ago. His property, of course, increased in value, despite Katrina. But there is no monetary value on the life he built there. The only thing he took when he evacuated is proof of that: His dog, Dahli, has been by his side since the storm. 
When Buffet visited his home recently, he held a new Nikon in his hands, a gift from a fellow photographer. Buffet has no choice but to focus on a post-Katrina world in which a picture is still worth a thousand words. 
He has lived long enough to have a clear depth of field. The picture is in focus. 
"Everything is different," Buffet says. 


