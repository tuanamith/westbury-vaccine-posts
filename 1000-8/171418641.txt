




Conway Caine wrote: 
Believe me, Conway, you _don't_ want _this_ spell.  I have an over-active immune system and it's thought to arise from a combination of things...the first being that compliments of my work, I'm continually exposed to various critters which seems to increase my resis- tance overall...and in the early '90's I was exposed to high levels of something else...I'll leave it to you to  guess what that something might have been. 
It's a double edged sword...while I rarely get ill, or if I do, I throw whatever it is off faster than average,  my immune system, when there's nothing interesting for it to do, tends to take potshots at organs and  physiological systems which I'm rather fond of in an attempt to justify its position.  (Not unlike a lab in- spector who just _has_ to find a safety violation to write up.)   
So far, it seems to have cheerfully murdered a num- ber of receptors in my gut which necessitates I inject myself daily with various trace elements to compen- sate...I'm told that as I age, the number of elements will, in all likelihood, increase, so far we're at four and holding.  Happily, I've never had a fear of needles. 
It's this over-reaction to stimulus, however, which  makes me wary of vaccines...under normal circum- stances my immune system putters around swatting at the odd pathogen while sulking at the overall lack of activity.  Give it something to obvious to fight how- ever, and it gets over-excited, goes into overdrive  and lashes out at _everything_...not unlike a certain political party I could name.   The process of getting it back in line is not pretty or comfortable, so I tend to avoid poking at it. 
If you want it, Conway, I'll cheerfully trade...but I'll wager you won't like it. 
Deirdre 

