


FYI.... 
TMT 


Experts ponder bird flu's disappearance By MARIA CHENG, AP Medical Writer Sun Dec 10 
Earlier this year, bird flu panic was in full swing: The French feared for their foie gras, the Swiss locked their chickens indoors, and Americans enlisted prison inmates in Alaska to help spot infected wild birds. 
The H5N1 virus - previously confined to Southeast Asia - was striking birds in places as diverse as Germany, Egypt, and Nigeria, and a flu pandemic seemed inevitable. 
Then the virus went quiet. Except for a steady stream of human cases in Indonesia, the current flu epicenter, the past year's worries about a catastrophic global outbreak largely disappeared. 
What happened? 
Part of the explanation may be seasonal. Bird flu tends to be most active in the colder months, as the virus survives longer at low temperatures. 
"Many of us are holding our breath to see what happens in the winter," said Dr. Malik Peiris, a microbiology professor at Hong Kong University. "H5N1 spread very rapidly last year," Peiris said. "So the question is, was that a one-off incident?" 
Some experts suspect poultry vaccination has, paradoxically, complicated detection. Vaccination reduces the amount of virus circulating, but low levels of the virus may still be causing outbreaks - without the obvious signs of dying birds. 
"It's now harder to spot what's happening with the flu in animals and humans," said Dr. Angus Nicoll, influenza director at the European Centres for Disease Control and Prevention. 
While the pandemic has not materialized, experts say it's too early to relax. 
"We have a visible risk in front of us," said Dr. Keiji Fukuda, coordinator of the World Health Organization's global influenza program. But although the virus could mutate into a pandemic strain, Fukuda points out that it might go the other direction instead, becoming less dangerous for humans. 
H5N1 has primarily stalked Asia. This year, however, it crossed the continental divide, infecting people in Turkey, Iraq, Egypt, Djibouti, and Azerbaijan. 
But despite the deaths of 154 people, and hundreds of millions of birds worldwide dying or being slaughtered, the virus still has not learned how to infect humans easily. 
Flu viruses constantly evolve, so the mere appearance of mutations is not enough to raise alarm. The key is to identify which mutations are the most worrisome. 
"We don't really know how many changes this virus has got to make to adapt to humans, if it can at all," said Dr. Richard Webby, a bird flu expert at St. Jude Children's Research Hospital in Tennessee. 
The most obvious sign that a pandemic may be under way will almost certainly come from the field: a sudden spike in cases suggesting human-to-human transmission. The last pandemic struck in 1968 - when bird flu combined with a human strain and went on to kill 1 million people worldwide. 
In May, on Sumatra island in Indonesia, a cluster of eight cases was identified, six of whom died. The World Health Organization immediately dispatched a team to investigate. 
The U.N. agency was concerned enough by the reports to put pharmaceuticals company Roche Holding AG on standby in case its global antiviral stockpile, promised to WHO for any operation to quash an emerging pandemic, needed to be rushed to Indonesia. 
Luckily, the Sumatra cluster was confined to a single family. Though human-to-human transmission occurred - as it has in a handful of other cases - the virus did not adapt enough to become easily infectious. 
This highlighted many of the problems that continue to plague public health officials, namely, patchy surveillance systems and limited virus information. 
Even in China, where H5N1 has circulated the longest, surveillance is not ideal. 
"Monitoring the 14 billion birds in China, especially when most of them are in back yards, is an enormous challenge," said Dr. Henk Bekedam, WHO's top official in China. Of the 21 human cases China has logged so far, 20 were in areas without reported H5N1 outbreaks in birds. 
"We need to start looking harder for where the virus is hiding," Bekedam said. 
To better understand the virus' activity, it would help to have more virus samples from every H5N1-affected country. But public health authorities are at the mercy of governments and academics. Scientists may hoard viruses while waiting for academic papers to be published first. And developing countries may be wary of sharing virus samples if the vaccines that might be developed from them might ultimately be unaffordable. 
That leaves public health officials with an incomplete viral picture. 
"It shouldn't just be WHO as a lonely voice in the desert, calling for more viruses (to be shared)," said Dr. Jeff Gilbert, a bird flu expert with the Food and Agriculture Organization in Vietnam. All countries, need to understand that sharing will help them better prepare for a flu pandemic, he said. 
Though scientists are bracing themselves for increased bird flu activity in the winter, there are no predictions about where it might appear next. The WHO's Fukuda said it would not be a surprise to see it appear in new countries. 


