


"cindys" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... Hi Cindy, I don't think it's as contagious as some things.  I had an elderly cat that  we couldn't vaccinate due to extreme allergic reactions to vaccines (she  would go into anaphylactic shock).  She was exposed without being vaccinated  for three years to another of our cats who turned out to have leukemia (long  story--tested negative as a wee kitten but had to be euthanized at about  three years due to intestinal sarcoma.  Vet had a hunch it might be leukemia  and tested him and he was positive.  Several mom cats were nursing these  kittens communally and one of them probably passed it on to the kittens.  We  found out later that all 3 of his siblings contracted leukemia also, so sad.  He probably tested negative initially because his immune system was not well  developed yet.) 
Cats that fight are more likely to pass it on.  I'm glad you're having your  foster re-tested.  It'll give you some peace of mind!  It sounds like your  cat had minimal exposure and I doubt it'd be enough to transmit leukemia  even if the foster had it. 
Good luck and bless you for taking him in! 
Bonnie 



