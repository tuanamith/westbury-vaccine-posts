


Via NY Transfer News Collective  *  All the News that Doesn't Fit   Progreso Weekly - Oct 19, 2006  <URL>  
Cuban Radar Newsbriefs - October 19, 2006 A service of Radio Progreso Alternativa's Havana Bureau 
* Cuba's "Rebel Youth" Newspaper Exposes More Corruption 
* More sugar mills will work the next harvest 
Up to the moment there is no official declaration whether Cuba plans to use alcohol as fuel. 
* U.S. blocks Cuba from obtaining vaccine  
The $730,000 project would have been sponsored by the Bill and Melissa Gates Foundation, which has financed other Cuban projects selected in international contests for its high scientific level, said Dr. Gerardo Guillin Nieto, head of biomedical research at the Center of Genetic Engineering and Biotechnology. 
* Houston Company fined by US Treasury Dept for Brazilian Cuba Trade 
Due to the more than 45-year blockade on the island, Cuba has lost approximately $86 billion. 
* Biological pest control can replace chemical insecticide 
According to the Adelante newspaper in Camag|ey (Central Cuba), a plant was inaugurated in the province for the production of a biological pest control that radically eliminates mosquitoes and other insects in their larval stage, but with the additional advantage of not harming humans and the environment. 
The plant, located in the outskirts of the city of Camag|ey, plans to process 10,000 liters of the product which has as active ingredient the Bacillus thuringiensis, a bacteria that is an efficient larvae killer. 
Production of the first 2,000 liters has already begun and soon will be used in the ongoing campaign to reduce the population of the Aedes aegypti mosquito, a transmitter of several diseases, among them dengue fever.                                         * ================================================================ .NY Transfer News Collective    *    A Service of Blythe Systems .          Since 1985 - Information for the Rest of Us         . .339 Lafayette St., New York, NY 10012      <URL>  .List Archives:   https://olm.blythe-systems.com/pipermail/nytr/ .Subscribe: https://olm.blythe-systems.com/mailman/listinfo/nytr ================================================================ 

