


 <URL>  
NewsTarget.com printable article Originally published October 31 2006 Scientists discover new bird flu strain that evades all vaccines and  eradication measures (NewsTarget) The Proceedings of the National Academy of Sciences today heard  about a previously unknown, dangerous strain of H5N1 bird flu discovered in  southern China, which is spreading from birds to people in Southeast Asia  and sparking fears of a third wave of infection from the disease. According to a team from State Key Laboratory of Emerging Infectious  Diseases and St. Jude's Children's Research Hospital in Memphis, Tenn., the  H5N1 strain still mostly affects birds, and only infects people with close,  prolonged association with infected birds or people, but this new strain  could be the key to its development into a version easily transmittable  between humans. If that happens, it would likely kick off a pandemic that  could claim millions of lives. 
The team monitored H5N1 in market chickens, ducks and geese, and found the  new strain emerged last year, displacing previous strains in southern China  by early this year. The compulsory chicken vaccination program did not stop  the strain; indeed the scientists suggest it may have even exacerbated the  virus. 
The new strain has spread across China, especially in urban and rural areas,  and also to Hong Kong, Laos, Malaysia and Thailand in what the team is  calling a new outbreak wave in Southeast Asia. 
"The predominance of this virus over a large geographical region within a  short period directly challenges current disease control measures," the team  concluded. 
"The implications of this study are that current control measures, as  generally practiced to control avian influenza, are ineffective," said  professor Yi Guan of the University of Hong Kong, leader of the team  describing the virus at the proceedings. He added that there was no  information yet that suggested the new strain was any more highly pathogenic  or a more likely candidate for pandemic than any other H5N1 subtype. A  strain currently found in Eurasian and African poultry populations is  considered the most likely candidate for pandemic influenza. 
However, the new strain illustrates how more frequent and broader flu  surveillance is needed in both poultry and humans, the team said, otherwise  identifying a human outbreak will be difficult. 
According to official World Health Organization figures, H5N1 has so far  infected a total of 256 people, 152 of who have died. 



