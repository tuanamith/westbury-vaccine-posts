


109 Reasons To Dump The 109th Congress 
We need a new Congress -- here's why: 
1. Congress set a record for the fewest number of days worked -- 218 between  the House and Senate combined. [Link] 
2. The Senate voted down a measure that urged the administration to start a  phased redeployment of U.S. forces out of Iraq by the end of 2006. [Link] 
3. Congress failed to raise the minimum wage, leaving it at its lowest  inflation-adjusted level since 1955. [Link] 
4. Congress gave itself a two percent pay raise. [Link] 
5. There were 15,832 earmarks totaling $71 billion in 2006. (In 1994, there  were 4,155 earmarks totaling $29 billion.) [Link] 
6. Congress turned the tragic Terri Schiavo affair into a national spectacle  because, according to one memo, it was "a great political issue" that got  "the pro-life base...excited." [Link] 
7. The chairman of the Senate Committee on Environment and Public Works  thinks global warming is the "greatest hoax ever perpetrated on the American  people." [Link] 
8. The House leadership held open a vote for 50 minutes to twist arms and  pass a bill that helped line the pockets of energy company executives.  [Link] 
9. Congress fired the Special Inspector General for Iraq Reconstruction, the  lone effective federal watchdog for Iraq spending, effective Oct. 1, 2007.  [Link] 
10. The Chairman of the Senate Commerce Committee thinks the Internet is "a  series of tubes." [Link] 
11. Congress established the pay-to-play K Street corruption system which  rewarded lobbyists who made campaign contributions in return for political  favors doled out by conservatives. [Link] 
12. The lobbying reform bill Congress passed was a total sham. [Link] 
13. Rep. Jean Schmidt (R-OH) shamefully attacked Rep. John Murtha (D-PA) on  the House floor, telling him that "cowards cut and run, Marines never do."  [Link] 
14. Congress passed budgets that resulted in deficits of $318 billion and  $250 billion. [Link] 
15. House Majority Leader John Boehner (R-OH) said Donald Rumsfeld "is the  best thing that's happened to the Pentagon in 25 years." [Link] 
16. House Intelligence Committee Chairman Pete Hoekstra (R-MI) baselessly  announced that "we have found the WMD in Iraq." [Link] 
17. Congress passed a special-interest, corporate-friendly Central American  trade deal (CAFTA) after holding the vote open for one hour and 45 minutes  to switch the vote of Rep. Robin Hayes (R-NC). [Link] 
18. Senate conservatives threatened to use the "nuclear option" to block  members of the Senate from filibustering President Bush's judicial nominees.  [Link] 
19. Congress stuck in $750 million in appropriations bills "for projects  championed by lobbyists whose relatives were involved in writing the  spending bills." [Link] 
20. The typical Congressional work week is late Tuesday to noon on Thursday.  [Link] 
21. Congress has issued zero subpoenas to the Bush administration. [Link] 
22. Congress eliminated the Perkins college loan program and cut Pell Grants  by $4.6 billion. [Link] 
23. Rep. Don Sherwood (R-PA) paid $500,000 to settle a lawsuit alleging that  he strangled his 29-year-old mistress. [Link] 
24. Congress decreased the number of cops on the streets by cutting nearly  $300 million in funding for the Community Oriented Policing Services (COPS)  program. [Link] 
25. In a debate last year over the reauthorization of the Patriot Act, the  chairman of the House Judiciary Committee abruptly cut off the microphones  when Democrats began discussing the treatment of detainees at Guantanamo  Bay. [Link] 
26. Just two out of 11 spending bills have made it out of Congress this  year. [Link] 
27. 1,502 U.S. troops have died in Iraq since Congress convened. [Link] 
28. The House Ethics Committee is "broken," according to the Justice  Department. [Link] 
29. The FBI continues to investigate Rep. Curt Weldon's (R-PA) willingness  to trade his political influence for lucrative lobbying and consulting  contracts for his daughter. [Link] 
30. Congress failed to protect 58.5 million acres of roadless areas to  logging and road building by repealing the Roadless Rule. [Link] 
31. Congress spent weeks debating a repeal of the estate tax (aka the Paris  Hilton Tax), which affects a miniscule fraction of the wealthiest Americans.  [Link] 
32. The percentage of Americans without health insurance hit a record-high,  as Congress did nothing to address the health care crisis. [Link] 
33. Both the House and Senate voted to open up our coasts to more oil  drilling, "by far the slowest, dirtiest, most expensive way to meet our  energy needs." [Link] 
34. Congress stripped detainees of the right of habeas corpus. [Link] 
35. The House fell 51 votes short of overriding President Bush's veto on  expanding federal funding of embryonic stem cell research. [Link] 
36. Only 16 percent of Americans think Congress is doing a good job. [Link] 
37. Congress confirmed far-right activist Supreme Court Justice Samuel  Alito. [Link] 
38. Congress spent days debating a constitutional amendment that would  criminalize desecration of the U.S. flag, the first time in 214 years that  the Bill of Rights would have been restricted by a constitutional amendment.  [Link] 
39. Congress raised the debt limit by $800 billion, to $9 trillion. [Link] 
40. Rep. William Jefferson (D-LA) hid bribe money in his freezer. [Link] 
41. Congress passed an energy bill that showered $6 billion in subsidies on  polluting oil and gas firms while doing little to curb energy demand or  invest in renewable energy industries. [Link] 
42. Rep. Jerry Lewis (R-CA) used his seat on the House Appropriations  Committee to steer earmarks towards to one of his closest friends and major  campaign contributor. [Link] 
43. Congress passed a strict bankruptcy bill making it harder for average  people to recover from financial misfortune by declaring bankruptcy, even if  they are victims of identity theft, suffering from debilitating illness, or  serving in the military. [Link] 
44. The House passed a bill through committee that that would "essentially  replace" the 1973 Endangered Species Act with something "far friendlier to  mining, lumber and other big extraction interests that find the original act  annoying." [Link] 
45. Congress failed to pass voting integrity and verification legislation to  ensure Americans' votes are accurately counted. [Link] 
46. House Majority Leader John Boehner (R-OH) distributed a memo urging  colleagues to exploit 9/11 to defend Bush's Iraq policy. [Link] 
47. Congress repeatedly failed to pass port security provisions that would  require 100 percent scanning of containers bound for the United States.  [Link] 
48. Ex-House Majority Leader Tom DeLay (R-TX) declared an "ongoing victory"  in his effort to cut spending, and said "there is simply no fat left to cut  in the federal budget." [Link] 
49. Congress allowed Rep. Bob Ney (R-OH) stay in Congress for a month after  pleading guilty in the Jack Abramoff investigation. [Link] 
50. Congress didn't investigate Tom DeLay and let him stay in Congress as  long as he wanted. [Link] 
51. The Justice Department and the Securities and Exchange Commission are  investigating the Senate Majority Leader's sale of HCA stock a month before  its value fell by nine percent. [Link] 
52. Congressional conservatives pressured the Director of National  Intelligence to make public documents found in Iraq that included  instructions to build a nuclear bomb. [Link] 
53. Conservatives repeatedly tried to privatize Social Security, a change  that would lead to sharp cuts in guaranteed benefits. [Link] 
54. Congress is trying to destroy net neutrality. [Link] 
55. Rep. Katherine Harris (R-FL) accepted contributions from disgraced  lobbyist Mitchell Wade and MZM, Inc., her largest campaign contributor, in  return for a defense earmark. [Link] 
56. Former Rep. Randy "Duke" Cunningham (R-CA) was sentenced to eight years  federal prison for taking $2.4 million in bribes in exchange for lucrative  defense contracts, among other crimes. [Link] 
57. Congress passed a $286 billion highway bill in 2005 stuffed with 6,000  pork projects. [Link] 
58. House Intelligence Committee Chairman Peter Hoekstra (R-MI) abused his  power and suspended a Democratic staffer in an act of retribution. [Link] 
59. Congress failed to offer legal protections to states that divest from  the Sudan. [Link] 
60. The Senate Appropriations Committee Chairman Ted Stevens (R-AK) tried to  earmark $223 million to build a bridge to nowhere. [Link] 
61. Congress spent days debating an anti-gay constitutional ban on same-sex  marriage. [Link] 
62. Congress isn't doing anything significant to reverse catastrophic  climate change. [Link] 
63. House Speaker Dennis Hastert (R-IL) secured a federal earmark to  increase the property value of his land and reap at least $1.5 million in  profits. [Link] 
64. Senate Majority Leader Bill Frist (R-TN) used a video tape "diagnosis"  to declare that Terri Schiavo, who was later found to be blind, "certainly  seems to respond to visual stimuli." [Link] 
65. Rep. Mark Foley (R-FL) resigned in disgrace after ABC News revealed  explicit instant messages exchanges between Foley and former congressional  pages. [Link] 
66. Half of all Americans believe most members of Congress are corrupt.  [Link] 
67. Rep. Marilyn Musgrave (R-CO) said that gay marriage "is the most  important issue that we face today." [Link] 
68. The House voted against issuing a subpoena seeking all reconstruction  contract communications between Cheney's office and Halliburton. [Link] 
69. Sen. Conrad Burns (R-MT) told a Virginia-based volunteer firefighting  team they had done a "piss-poor job" in fighting wildfires in Montana.  [Link] 
70. The House voted against amendments prohibiting monopoly contracts and  requiring congressional notification for Department of Defense contracts  worth more than $1 million. [Link] 
71. Congress failed to pass comprehensive immigration reform. [Link] 
72. During a floor debate on embryonic stem cell research, Sen. Sam  Brownback (R-KS) held up a picture of an embryo drawn by a 7-year-old girl.  Brownback explained that one of the embryos in the picture was asking, "Are  you going to kill me?" [Link] 
73. Sen. George Allen (R-VA) used the slur "macaca" to describe an opposing  campaign staffer of Indian descent, and has been repeatedly accused by  former associates of using racial epithets to refer to African-Americans.  [Link] 
74. Congress refused to swear in oil executives testifying about high  prices. [Link] 
75. Against congressional rules, ex-House Majority Leader Tom DeLay (R-TX)  accepted expensive foreign trips funded by Jack Abramoff. [Link] 
76. Rep. Steve King (R-IA) went on the House floor to unveil a fence that he  "designed" for the southern border. King constructed a model of the fence as  he said, "We do this with livestock all the time." [Link] 
77. Ex-House Majority Leader Tom DeLay (R-TX) threatened the judges who  ruled in the Terri Schiavo case, saying the "time will come" for them "to  answer for their behavior." [Link] 
78. Congressional conservatives wanted to investigate Sandy Berger, but not  the Iraq war. [Link] 
79. Rolling Stone called the past six years "the most shameful, corrupt and  incompetent period in the history of the American legislative branch."  [Link] 
80. Not a single non-appropriations bill was open to amendment in the second  session of the Congress. [Link] 
81. House Speaker Dennis Hastert (R-IL) claimed that supporters of Bush's  Iraq policy "show the same steely resolve" as did the passengers on United  93. [Link] 
82. Senate Majority Leader Bill Frist (R-TN) appeared with prominent  Christian conservatives in a telecast portraying opponents of Bush's  judicial nominees as "against people of faith." [Link] 
83. Under the guise of "tort reform," Congress passed legislation that would  "undermine incentives for safety" and make it "harder for some patients with  legitimate but difficult claims to find legal representation." [Link] 
84. Despite multiple accidents in West Virginia and elsewhere, Congress  passed legislation that failed to adequately protect mine workers. [Link] 
85. House Speaker Dennis Hastert (R-IL) said "if you earn $40,000 a year and  have a family of two children, you don't pay any taxes," even though it  isn't true. [Link] 
86. Monthly Medicare Part B premiums have almost doubled since 2000, from  $45.50 in 2000 to $88.50 in 2006. [Link] 
87. Senate Majority Leader Bill Frist (R-TN) and House Speaker Dennis  Hastert (R-IL) inserted a provision in the Defense Appropriations bill that  granted vaccine manufactures near-total immunity for injuries or deaths,  even in cases of "gross negligence." [Link] 
88. Congress appropriated $700 million for a "railroad to nowhere, but just  $173 million to stop the genocide in Darfur. [Link] 
89. Congress included a $500 million giveaway to defense giant Northup  Grumman in a bill that was supposed to provide "emergency" funding for Iraq,  even though the Navy opposed the payment. [Link] 
90. Ex-Rep. Bob Ney (R-OH), who has since pled guilty to talking bribes, was  put it charge of briefing new lawmakers "on congressional ethics." [Link] 
91. Rep. Lynn Westmoreland (R-GA) can't tell the difference between the  Voting Rights Act and the Stamp Act. [Link] 
92. Three days before Veterans Day -- House Veterans' Affairs Committee  Chairman Steve Buyer (R-IN) announced that for the first time in at least 55  years, "veterans service organizations will no longer have the opportunity  to present testimony before a joint hearing of the House and Senate  Veterans' Affairs Committees." [Link] 
93. Members were caught pimping out their offices with $5,700 plasma-screen  televisions, $823 ionic air fresheners, $975 window blinds, and $623 popcorn  machines. [Link] 
94. House Speaker Dennis Hastert (R-IL) skipped a vote on Katrina relief to  attend a fundraiser. [Link] 
95. Congress made toughening horse slaughtering rules the centerpiece of its  agenda after returning from summer recess this year. [Link] 
96. Sen. John McCain (R-AZ) wants to send 20,000 more troops into the middle  of a civil war in Iraq. [Link] 
97. Katrina victims were forced to take out ad space to plead "with Congress  to pay for stronger levees." [Link] 
98. Congress passed the REAL ID Act, "a national ID law that will drive  immigrants underground, while imposing massive new burdens on everyone  else." [Link] 
99. Congress extended tax cuts that provided an average of $20 relief but an  average of nearly $42,000 to those earning over $1 million a year. [Link] 
100. Congress received a "dismal" report card from the 9/11 Commission --   five F's, 12 D's, nine C's, and only one A-minus -- for failing to enact the  commission's recommendations. [Link] 
101. Congress won't let the government negotiate lower prices for  prescription drugs for people on Medicare. [Link] 
102. Congress has left America's chemical plants vulnerable to terrorist  attack. [Link] 
103. Sen. Ted Stevens (R-AK) "threw the senatorial version of a hissy fit"  when he threatened to resign unless the Senate approved funding for his  bridge to nowhere. [Link] 
104. Congress didn't simplify the tax code. [Link] 
105. Seventy-five percent of voters can't name one thing Congress has  accomplished. [Link] 
106. House Majority Leader John Boehner (R-OH), has "raised campaign  contributions at a rate of about $10,000 a day since February, surpassing  the pace set by former Representative Tom DeLay." [Link] 
107. Congress failed to ensure Government Accountability Office oversight of  Hurricane Katrina relief funds, resulting in high levels of waste, fraud,  and abuse. [Link] 
108. When a reporter asked Rep. Don Young (R-AK) if he would redirect  spending on his bridge projects to Katrina victim housing, Young said, "They  can kiss my ear!" [Link] 
109. There were just 12 hours of hearings on Abu Ghraib. (There were more  than 100 hours of hearings on alleged misuse of the Clinton Christmas card  list.) [Link] 
 <URL>  
subscribe to the Progress Report for free HERE: 
 <URL>  



