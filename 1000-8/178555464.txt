


< <URL> > Study says new H5N1 strain pervades southern China 
A new subtype of H5N1 avian influenza virus has become predominant in southern China over the past year, possibly through its resistance to vaccines used in poultry, and has been found in human H5N1 cases in China, according to researchers from Hong Kong and the United States. 
The rise of the "Fujian-like" strain seems to be the cause of increased poultry outbreaks and recent human cases in China, according to the team from the University of Hong Kong and St. Jude's Children's Research Hospital in Memphis. The researchers also found an overall increase of H5N1 infection in live-poultry markets in southern China. 
"The predominance of this [Fujian-like] virus appears to be responsible for the increased prevalence of H5N1 in poultry since October 2005 and recent human infection cases in China," says the report, published this week in the Proceedings of the National Academy of Sciences. 
But other disease experts said they could see no evidence that the new strain increases the risk of a human influenza pandemic or is more virulent than other H5N1 strains. Meanwhile, Chinese authorities rejected the report, while a World Health Organization official in China renewed previous complaints that the Chinese have been stingy with information about H5N1 in poultry. 
Infection rate in market poultry rises The research team, including Yi Guan of the University of Hong Kong and Robert Webster of St. Jude's, tested more than 53,000 birds from live-poultry markets in six southern Chinese provinces from July 2005 through June 2006. About 2.4% of the birds (1,294 of 53,220) tested positive for the H5N1 virus, more than double the 0.9% positive rate in the preceding 12 months, according to the report.  
snip 
 <URL>  The World Health Organisation blasted China's agriculture ministry yesterday for not sharing samples of a newly discovered strain of bird flu, complicating the health watchdog's efforts to track the virus's spread. 
The WHO's comments came after a scientific report published earlier this week suggested the new strain - called H5N1 Fujian-like - is now widespread across much of southern China and South-east Asia.  
snip 

-- WÂ§ mostly in m.s -  <URL>  

