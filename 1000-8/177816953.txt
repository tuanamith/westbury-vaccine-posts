


-----BEGIN PGP SIGNED MESSAGE----- Hash: SHA1 
"Tell Someone" - Price of HPV Vaccine Stuns 
Womens ENews - Nov 6, 2006  <URL>  
Price Tag of HPV Vaccine Stuns College Students 
By Hannah Seligson WeNews correspondent 
"Everyone is confused about what to do with this sexually active population. The answer is: Vaccinate them," says Haupt. 
Sources of Confusion 
However, HPV has dozens of strains, so the vaccine may be effective against strains that have not yet infected the women. 
But the government's age cutoff of 18 does not match the recommendations for this vaccine. 
"This means that a 16, 18 or 26-year-old women can get the vaccine," says Curtis Allen, a spokesperson for the CDC. 
Coverage Lag 
Merck reports that in its latest survey of insurers, 90 percent of insurance companies said they plan to cover the vaccine. 
Rodewald advises young women to lobby their insurance providers. "That's one of the ways to expedite the process." 
Pap Smears Still Needed 
Martin, the New York psychology graduate student, personally encountered clinical confusion about the vaccine. 

For more information: 
Merck's Cervical Cancer Center: -  <URL> / 
Centers for Disease Control, HPV Information: -  <URL>  
National Women's Health Network: -  <URL>  
-----BEGIN PGP SIGNATURE----- Version: GnuPG v1.4.5 (FreeBSD) 
iD8DBQFFT6Bwiz2i76ou9wQRAqQyAJ4y7nLxQK5tIfT4l/ZxgqYsLoGyagCdEN1L fCn7jp7jq0A38ySNcQwhEGw= =1aOj -----END PGP SIGNATURE----- 


