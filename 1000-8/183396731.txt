



 <URL>  
Ped Med: Counting on autism counts 
Excerpts: 
SAN FRANCISCO, Oct. 19 (UPI) -- Many are counting on the numbers keepers to  provide critical clues to some fundamental questions about the rising rates  of autism diagnoses in America's children. 
In particular, those who hold the mercury-based vaccine preservative  thimerosal responsible for the increase have been eagerly awaiting a verdict  on their prediction that as children's exposure to the compound decreases,  so, too, will their autism rates. 
Phased out of most childhood shots around the turn of the century,  thimerosal remains in some booster and flu vaccines recommended for pregnant  women and babies. 
With the release of California's special-education statistics in the summer  of 2005, the thimerosal skeptics gleaned a glimmer of substantiation of  their suspicions. 
The data were compiled by the state Department of Developmental Services.  They showed the sum total of autistic children in the system continues to  grow -- by now topping 28,000. 
There were 1,451 new cases in 2001-2002; 1,981 in 2002-2003; 3,707 in  2003-2004; and 3,178 in 2004-2005. 
Even if the waning caseload numbers in the earlier California report  represented a true downward trend in actual autism rates, there remain other  uncertainties that could stand in the way of connecting the dots directly to  vaccines. 
Take a March 2006 study showing newborns may be 65 to 130 times more  sensitive than adults -- and even 26 to 50 times more vulnerable than fellow  infants -- to certain pesticides. This variability is far greater than  anyone had predicted. 
What makes the findings potentially relevant to the autism debate is that  the chemicals under study, so-called organophosphate compounds like diazinon  and chlorpyrifos, have been shown, in high doses, to have profound effects  on the central nervous system. 
Growing evidence from animal and human studies also suggests chronic  low-level exposure may affect neurodevelopment. 
Next: Looking for safety standards in all the wrong places. 



