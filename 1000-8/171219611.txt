





Fairfield County Weekly,  May 20, 1999 "The Dirty Truth Behind Lyme Disease Research" 
THE DIRTY TRUTH ABOUT LYME            DISEASE RESEARCH 


By Stefanie Ramp 

elaborately twisted power struggle that challenges the established myths about the disease. 











patients are suffering in the process. Until more research can be done, 




isn't particularly serious and that patients really aren't chronically infected. 



possibilities," Katz continued. "I don't know if it's an ego issue or if this is the only way to deal with the problem according to their training and beliefs. There are some sound reasons on both sides of the fence and people are not hearing them -- that's a shame." 

Katz also suggested that more conservative doctors might fear "misuse of treatment facilities and antibiotics, and they're afraid that people will suffer from the side-effects of those prolonged treatments." Katz himself believes that there is not enough information about repeated courses of 
long-term IV therapy and he limits such treatment in his own practice to three months, at which time therapy is extended if a careful reassessment of the patient seems to warrant it. 

In most cases, antibiotic-related risks are not severe, according to Dr. Robert T. Schoen, a leading figure at the Yale Lyme Clinic, but in rare 
cases prolonged use of antibiotics can cause gallstones, temporary and occasionally permanent liver damage, and more commonly, mild allergic reactions, susceptibility to yeast and fungal infections, gastro-intestinal dysfunction, localized inflammation or infection from the IV line, and antibiotic-resistant strains of the bacteria. 

For patients who have successfully undergone long-term antibiotic treatment, the potential risks do not negate the beneficial effects of therapy. Bonnie Friedman, a representative of Dr. Bernard Raxlen's office in Greenwich and a Lyme patient herself, explained that she and her daughter have been on long-term antibiotic therapy several times. Her daughter was treated for 10 months and started having seizures when she discontinued therapy. Said Friedman, "No parent wants to put that amount of antibiotics into their 
child; it takes a lot of heartfelt thinking to make a decision like that." 

While both have suffered some side-effects from the antibiotics, Friedman remarked, "They were nothing compared to what my daughter's life would've been like without therapy. She was in bed and totally incapacitated last June, and on May 23, she will graduate cum laude from Fairfield University." 

Both Schoen and Shapiro believe that Lyme is over-diagnosed and over-treated. "There's this myth that's been perpetrated that Lyme is difficult to treat and causes horrible complications, and that rarely is true," remarked Shapiro. "I also think it's more socially acceptable to have Lyme disease than to have, say, depression, and people like to put a name on things...There are a ton of people with non-specific symptoms and most of their positives are false positives [for the ELISA and Western Blot], so that they think they have this diagnosis of Lyme." 

Schoen suggested that long-term treatments carry a psychological risk as well as a physical one. "There are many patients who are being treated and told that they have a chronic, very-difficult-to-cure problem with long-term, wide-spectrum antibiotics as the only treatment option. If I feel that the patient doesn't have Lyme disease and doesn't need that treatment, then I mean that to be reassuring to that patient -- that they don't have this disease." 

What has been less reassuring to some patients is the fact that Schoen consults for several insurance companies. Critics feel that the situation invites a conflict of interest which may prevent chronic Lyme patients from getting insurance coverage for the care they need. While most insurance 
companies pay for the standard oral antibiotic treatments indicated for 
acute early-stage Lyme, most insurance providers have been far less willing to pay for long-term IV therapy, which can cost in the neighborhood of $6,000 a month. 

Shapiro says that he has never reviewed individual cases for treatment for insurance companies but has been asked to help formulate the coverage policies. 

Even though many patients suffering from chronic Lyme symptoms have been prescribed IV treatment by their doctors, most insurance companies won't approve coverage unless one of their own consultants concludes that it's necessary. For consultants then, medical opinion and money -- several hundred dollars an hour -- become irrevocably intertwined. 

According to Pete McFadden, a propulsion engineer for a Connecticut-based company, based on Schoen's review of his case, he was denied coverage for antibiotic treatment despite a well-documented, diagnostically definitive "bulls-eye" rash noted by his treating physician as "being the size of a dinner plate." McFadden also tested positive for Lyme. Further frustrating McFadden, Schoen wouldn't or couldn't explain what exactly McFadden was 
suffering from. 


Schoen did not deny his work for the insurance companies, but explained to the Weekly, "I deal with questions about Lyme disease all day long in all kinds of different settings. I don't feel anyone has any leverage over me at all. Wherever the questions are coming from, I give the answers I consider to be in the patient's best medical interest." 

How cut-throat the insurance companies are with their consultants is a well-guarded secret, but according to the Lyme Disease Foundation's Forschner, Blue Cross/Blue Shield, which Schoen has consulted for, is not renewing a contract with Hamden doctor Ray Jones because he's treating too much Lyme. 

Disagreements over the nature and appropriate treatment of Lyme have evolved from academic squabbles to active animosity on occasion. Forschner recalled, and provided documentation for, one 1993 Connecticut case when Dr. Larry Zemel, a pediatric rheumatologist at Newington Children's Hospital, reported Dr. Phil Watsky, a Bristol-based physician, to the state medical licensing board for over-diagnosis and treatment of Lyme, as well as profiteering 
based solely on rumor. "I don't know if it's ethical or not, but it certainly is despicable to do something like that based on a rumor," remarked Forschner. 

Zemel had heard that a Lyme info hotline, which was referring callers to Watsky along with other physicians, was thought to be run by an IV company which would stand to profit from Watsky's approach to intravenous antibiotic Lyme treatment. While investigators ultimately determined that Watsky had no knowledge of the hotline or the infusion company, and Watsky was cleared of any malpractice, he still had to pay the financial and emotional costs of defending himself. Watsky has purportedly distanced himself from Lyme treatment and doesn't accept new Lyme patients. 

According to Liegner, this form of bullying is only one of several similar scenarios locally and around the country. "It has been a very real threat to people I know and has had a very chilling effect on doctors in general. 
Doctors by and large don't want to go near a Lyme patient with a 10-foot pole," Liegner said. 

"It's not an exaggeration to call it a war -- a war of ideas -- and there have been casualties in the process. People have been scapegoated, careers have been, if not permanently destroyed, seriously damaged." Liegner notes that syphilis engendered a similarly ferocious controversy around the turn of the century and that spirochetal diseases seem to have had the innate power to spark chaos throughout history. 

Such a raging conflict has arisen between insurance companies, doctors and patients over chronic Lyme disease care that Connectict's Attorney General Richard Blumenthal is pushing for legislation that would mandate coverage of long-term treatments prescribed by a patient's doctor without requiring a second opinion from insurance consultants. 

"I received numerous letters from people suffering from Lyme disease and their families about the inadequacy of coverage and denial of coverage, and I feel very strongly that there ought to be coverage for diagnosis and care when a treating physician and his patient feel that a certain type of treatment is warranted," Blumenthal explained to the Weekly. 

House bill 5694, "An Act Requiring Health Insurers to Cover Continued Lyme Disease Treatment," was introduced by Rep. Anthony Tercyak and was promptly rewritten by the Committee on Insurance and Real Estate. The original draft mandated that treatment be provided according to the opinion of "the attending health-care provider." 

The committee's wording added, "in the opinion of the attending health-care provider and an independent health-care provider," and that as long as the patient would "benefit medically from such care." This loophole could potentially negate protection for patients if the "independent health-care provider" is assigned by an insurance company and said provider doesn't 
believe antibiotics could medically benefit a patient because he or she 
doesn't believe in persisting infection. The bill is still pending. 

"One of the problems in the statute with this additional provision is that there is absolutely no definition for provider," said Blumenthal. "I oppose the change in the bill from what I submitted in the draft...That provision will raise the extent and time; it will cause additional cost and delay in diagnosis and treatment." 

Blumenthal couldn't say with "absolute certainty" who pushed for the secondary wording but noted, "It's the kind of provision that the insurance industry would favor because it provides additional hurdles for people seeking coverage...There's a great deal of confusion and controversy that the experts aren't going to resolve overnight, so in the meantime, bonafide medical professionals and their patients should be credited and covered. 

"Quite bluntly, government shouldn't be deciding what kind of treatment is provided and insurance companies shouldn't be making arbitrary and capricious decisions about coverage to save money when certain kinds of 
treatment have been shown to work and improve the condition of people suffering from Lyme disease, so I want to leave the decision to medical 
professionals," he added. 

Blumenthal convened a conference in February which gathered patients, academicians, clinicians and insurance companies to discuss the issue of coverage for chronic Lyme care. Noting some of the more compelling testimony, he said, "Emotions run very deep and strong amongst the patients because the physical and emotional effects of Lyme are devastating, so denial of coverage is extraordinarily hurtful." 

"Having the insurance company, which is financially obligated to pay for your treatment, be in the position of making the decision as to whether or not you need treatment is kind of a conflict of interest," remarked NIAIDS's Brenner. "That to me is sort of like a District Attorney being in a position to render a verdict in a criminal trial." Brenner added that he doesn't 
think that the insurance companies are invariably wrong, either -- just that they have an inherent conflict of interest with prescribing treatment. 

The insurance companies -- of which Blue Cross/Blue Shield and Oxford are reputed to be the most difficult on this issue -- are undoubtedly feeling bombarded by patients requesting a costly treatment with a short track record. Albert May, a spokesperson with Blue Cross/Blue Shield, said that there is not enough evidence in reputable journals indicating that long-term antibiotic treatment works, and that one can't base policy on what a doctor thinks might work. 

He declined to comment on how Blue Cross' policy accounts for the number of patients and doctors reporting the efficacy of long-term IV therapy, except to say that there is not a significant enough portion of the medical community that believes in such therapy to make anecdotal evidence adequate for policy changes. He claimed that Blue Cross/Blue Shield is willing to change its policy if the medical community reaches a consensus. 

"They have a protocol that they follow, and basically it states: If you have a negative Western Blot, you don't have Lyme disease, even though the test is only 30-40 percent accurate," a spokesperson from Dr. Raxlen's office said of Blue Cross/Blue Shield's policy on Lyme coverage. "Even if you have a positive Western Blot, Blue Cross very often says that it's a false positive. It's really a difficult position for Lyme patients because if they go to somebody that they can trust, the insurance will not agree to pay." 

Oxford provided the Weekly with a rather vague written statement, noting that it covers the new Lymerix vaccine and oral antibiotics "usually effective in treating early stages of Lyme disease." If symptoms continue, Oxford implies that it will review the case, stating: "Oxford's review process is not one designed to scrutinize whether or not we will 'pay' for IV therapy, but rather whether IV therapy is truly medically appropriate, asking 'Is the diagnosis of Lyme Disease substantiated by ruling out other possible causes of similar symptoms.' " Bruce Fletcher, a patient advocate who's involved with local politics, suggested, "The insurance industry will argue that Lyme is over-diagnosed and that everybody thinks they have Lyme. They probably feel besieged by Lyme cases, but there frankly is an epidemic in Connecticut. There were over 3,000 cases last year." Those were just the reported cases meeting CDC criteria; both the CDC and the state of Connecticut admit that there could be as many as 34,000 actual cases. 

"I think it would have a positive economic impact on these insurance companies if they allowed the earlier-stage patients to be treated more 
aggressively because they wouldn't be paying these big bills on the back end. They've never done a study on that," Fletcher added. Fletcher's wife has chronic Lyme disease, and he's been actively advocating for the original chronic Lyme legislation. "It's immoral on the part of the insurance companies not to provide coverage. The legislators need to tighten the language in the bill and tell the insurance companies that if they want to make money off the people in the state of Connecticut, they need to cover one of our biggest health problems." 

Two other bills are wending their way through the Connecticut Legislature, one mandating insurance coverage of all prescription drugs prescribed by a patient's physician and the other a Senate bill similar to the aforementioned House legislation concerning coverage of chronic Lyme, and including coverage of the Lymerix vaccine. If the pharmaceutical bill is passed, it will quite possibly achieve the same goal for chronic Lyme patients as the insurance coverage mandate. New York currently has no similar legislation in the works. 

Despite research such as Reinhard Straubinger's work (Journal of Clinical Microbiology in January of 1997), which provided fairly definitive evidence of persisting infection in dogs, definitive research in humans is lacking, in part because you can't euthanize people and study their brain tissue. 

Hopefully, the National Institutes of Health's chronic Lyme study, the first major research endeavor of its kind, will help clarify the confusion surrounding Lyme disease. Dr. Phillip Baker, program officer for the Lyme Disease Program at NIH, explained that there are two arms of the study: the intramural program focuses on acute early-stage Lyme, while the extramural portion looks at patients with chronic symptoms. Both sets are subjected to the same vigorous battery of tests. 

Mark Klempner, principal investigator for the Chronic Lyme Disease Study, and Louisa C. Endicott, professor of medicine at Tufts University School of Medicine, explained that the extramural study seeks to answer five questions: does long-term, intensive, intravenous and oral antibiotic therapy benefit the patients; does persistent infection exist; is their 
evidence of co-infection or other causes of symptoms; can better diagnostic tools be created -- especially culturing tests; and can a profile of patients who do and do not benefit from the treatment be created to help prescribe treatment in the future? 

A related non-human primate study, which many believe will be the most helpful and which was in part the result of lobbying by The Lyme Coalition of New York and Connecticut, is being conducted at Tulane Regional Primate Center in New Orleans. Rhesus monkeys will be infected, the spirochete will be allowed to disseminate, and then the same treatment regimen as is used in the extramural study will be administered. Researchers will study the monkey tissue in an attempt to ascertain whether the spirochete has survived treatment. 

One of the most beneficial aspects of the extramural study is that patients report on their symptoms via a variety of neuro-cognitive tests. "If the patient doesn't feel better, then you haven't really accomplished much," noted Klempner. A new testing site in New Haven has recently opened headed by Dr. Janine Evans. 

Some patients and doctors have reservations about researcher bias (although the study is double-blind), overly strict diagnostic parameters for subjects, effects of discontinuing therepy for the placebo group, and the moderate length of the antibiotic courses being used. However, no one argues that it's not a good start. 

"I don't think that answers are imminent," cautioned Brenner, a member of the Advisory Committee. "It's an extremely complex disease and it's painful to admit that. I don't care how much money and effort you throw at this and how much good faith people on both sides can show -- I don't think the answers are right around the corner." 

So what do we do in the meantime to ward off this varmint of doom? There is the vaccine. Lymerix, manufactured by SmithKline Beecham, which hit the 
market in January with shaky FDA approval. Developed from a recombinant 
protein, OspA, which was pioneered at Yale, the vaccine was tested over four years involving 11,000 patients, 10 states and 31 researchers. 

While everyone seems to have a very adamant opinion about the safety and efficacy of Lymerix, it strikes one as slightly prophetic that the conservative Dr. Allen Steere, one of the leading researchers of the vaccine, doesn't plan to use it -- supposedly because he doesn't live in a Lyme-endemic area (Massachusetts). 

Some studies have suggested that the vaccine may reactivate Lyme disease in people who have already had Lyme, and is generally not recommended for them or for those with a family history of rheumatoid arthritis.     Even after three doses, the vaccine is only 78 percent effective and may require subsequent boosters. A novel and unprecedented vaccine, Lymerix 
works by generating antibodies that kill the Bb spirochete inside the gut of the tick itself as it ingests its host's blood; there is concern about how much bacteria will manage to evade the antibodies and slip into the host's vascular system, at which point the vaccine is completely ineffective. Once a person has had the vaccine, antibody tests will no longer be effective, creating an even larger diagnostic nightmare. It also doesn't protect against all strains of the disease or other tick-born diseases such as babesiosis, ehrlichiosis or Rocky Mountain spotted fever. On the other hand, if you work outside in an endemic area, some protection is better than  none. 


