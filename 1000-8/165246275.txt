


Hello! 
Have you ever thought that you can do a Scoutish good deed and help the mankind and science in an extremely easy way? Almost doing nothing at all! If you got interested, do read on... 
This message gives you a brief explanation about the idea behind the network computing and participating to it - as a Scout or Guide. These are one way of Being Friend to All, being Courteous and fulfilling one's Duty to be Useful. Yes, you can do your part about living up to the Scout/Guide Law even with your computer. This is an example about that. 
You can find this same information at  <URL> / - feel free to spread the URL and this message. If you translate even a part of this, I would be delighted to be able to add also that for everyone to read in the web, please! 

WHAT IS BOINC? BOINC, Berkeley Open Infrastructure for Network Computing, is an Open Source platform, open for new projects as well, offering a way for individual users to give a share of their computers' calculating power for greater good cause of their own choice. When you are doing nothing with your computer, a scientific project will gain those idle computing cycles. There are several projects going on and the same BOINC software can be used for all of those. In each of the projects, there are also so-called Teams, groups of people/computers that want to be identified as something they represent. There are universities, cities etc. - and of course, us Scouts and Guides! Read more about BOINC at  <URL> / 

WHAT IS  <EMAILADDRESS> ?  <EMAILADDRESS>  is a continuation of the original NASA SETI project - Search for Extra-Terrestrial Intelligence. If you take part to the  <EMAILADDRESS> , your computer will get data collected with the radio telescopes, see it through and return the finished calculations to the server. If there is something peculiar, someone will have a closer look at it - and if not, no actual work will be wasted on that bit of data. It's just those idle cycles that would have gone past anyway. Read more about  <EMAILADDRESS> :  <URL> / 
You can participate to the  <EMAILADDRESS>  effort and identify yourself as a Scout or a Guide by joining to the Team Scoutnet in the address  <URL>  - on that page, click on the link after "Join this team" if you already have an account, or click on the link after "Create team account URL" to create yourself a new account that is automatically a member of Team Scoutnet. 

WHAT IS  <EMAILADDRESS> ?  <EMAILADDRESS>  was started in the World Year of Physics 2005, to process the data gathered by the American LIGO and European GEO projects, aiming to detect and analyze gravitational waves, "ripples in the structure of spacetime", caused by e.g. pulsars (spinning neutron stars). The @home project analyzes these findings just like in the SETI project and reports them back to the server. Read more about  <EMAILADDRESS> :  <URL> / 
You can participate to the  <EMAILADDRESS>  effort as a Scout or Guide by joining the Team ScoutNet (just like described above) in the address  <URL>  

WHAT IS  <EMAILADDRESS> ? The  <EMAILADDRESS>  is a project to calculate new 3D models of proteins in medical research. The protein design is aiming to find cure for diseases like the HIV, cancer and Alzheimer's. Finding and understanding the shapes of the proteins is important, because the shape has a major role in the interaction between the protein and other molecules. To create synthesized proteins, scientists must first know how the amino acid chains fold into complex proteins. Read more about  <EMAILADDRESS> :  <URL> / 

WHAT ARE  <EMAILADDRESS>  AND MALARIACONTROL?  <EMAILADDRESS>  is a volunteer computing project to contribute to African humanitarian causes. The fields of the project are mainly health and environmental problems in the developing world. It is not a project only FOR the Africa, it is mainly a project WITH the Africa, as an important goal of  <EMAILADDRESS>  is to involve African students and African universities in the development and running of the volunteer computing projects. The Western world, on the other hand, does have the largest computing power to use for the project. That's collaborative for us! Read more about  <EMAILADDRESS> :  <URL> / 
MalariaControl.net is the first project under the  <EMAILADDRESS>  effort. The goal of the project is to compute large-scale simulations involved with the malaria control efforts. They are to be used for example to determine optimal strategies for delivering new vaccines, chmeotherapy and mosquito nets to the areas where the need is the highest. Read more about MalariaControl:  <URL> / 
You can participate to the  <EMAILADDRESS>  effort as a Scout  or Guide by joining the Team ScoutNet (just like with  <EMAILADDRESS> ) in the  address  <URL>  - do  note, though, that at the time of writing this, the project is still in  beta testing stage and might not let you create new accounts. 

Choose your way to help based on your own interest - and join in! 

You can also have a look at what other possibilities there are to use  your computer, related to Scouting/Guiding, at the Global Scoutnet's  website:  <URL> / 
--  Yours in Scouting and Guiding - Kimmo "Kipe" Vääriskoski <Kipe at scoutnet.fi>    Global Scoutnet, Country Coordinator Finland    Scoutnet ry - Scoutnet Finland, pj/Chairman 

