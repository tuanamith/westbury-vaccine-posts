



The Kentucky Wizard wrote: 
Not at all. Once you catch an infectious disease, your body begins to produce antibodies that (once you survive it) either prevent you from getting the disease again or, in rare cases, greatly reduce the severity of the disease if you do get it again. This is true even of colds - the reason we get colds again and again is that there are hundreds if not thousands of different cold viruses, each of which trigger the production of different antibodies. What's more, everybody's antibodies to the same disease will be the same. Everybody who has survived typhus, for instance, has identical antibodies to the typhus pathogen coursing through their systems. 
Knowing the shape of the antibody carried by survivors of the 1918 flu (shape is what's important in antibodies) might make it easier for scientists to prevent a future outbreak. This is especially important because they have only a few tiny samples of the 1918 virus, and working with antibodies is far safer than working with live viruses. If researchers know the shape of the antibody they're trying to create, that makes their job a lot easier. 
The ideal would be to create an immunization that caused the body to create antibodies of exactly the same shape (without causing the disease), but antibodies that are similar enough will work too. That's why giving people the mild disease cowpox prevented them from getting smallpox - the body creates antibodies after being infected with cowpox that are similar enough to those created by smallpox survivors to protect cowpox vaccinees from catching smallpox. 
There are probably a lot more 1918 flu survivors out there than people realize. Probably one-third of the people born before 1918 had the flu, and there are still millions of people over 88 out there.  
wd42 


