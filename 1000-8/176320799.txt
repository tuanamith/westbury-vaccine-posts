


Vaccine may work against common infection: researchers Tue Oct 31, 2006 2:19pm ET   Health News Manufacturers' anti-smoking ads ineffective: study Brain anomaly linked to sudden infant death Study confirms way to predict bad prostate cancer More Health News... Email This Article | Print This Article | Reprints [-] Text [+] WASHINGTON (Reuters) - A newly developed vaccine may work against Staphylococcus aureus, a bacteria that causes a range of potentially fatal infections and has become resistant to many antibiotics, U.S. researchers reported on Tuesday. 
The government-funded team said their vaccine worked against several different strains of the bacteria in mice. 
S. aureus is the most common cause of hospital-acquired infections, and can cause inflammation of the heart or endocarditis, toxic-shock syndrome, severe lung infections and food poisoning. 
Every year, infections caught in U.S. hospitals kill 90,000 people and cost $4.5 billion, according to the U.S. Centers for Disease Control and Prevention. Up to 80 percent of hospital infections are caused by Staph aureus.  

Many strains have evolved resistance to antibiotics, making them difficult and sometimes even impossible to treat. 
"One by one, this organism has learned how to evade nearly all of our current antibiotics. So, generating protective immunity against invasive S. aureus has become an important goal," Dr. Olaf Schneewind of the University of Chicago, who led the study, said in a statement. 
The CDC says that in 1972, only 2 percent of Staphylococcus aureus bacteria infections were drug-resistant but in 2004, 63 percent were. 
Schneewind's team created a vaccine that combines four of the proteins that make up Staphylococcus aureus. 
Writing in the Proceedings of the National Academy of Sciences, they said they tested 19 different proteins found in Staph bacteria, and chose the four that stimulated the most immune response in the mice.   Continued...  

"When we challenged the immunized mice by exposing them to a human strain of S. aureus, the combination vaccine provided complete protection, whereas the control group developed bacterial abscesses," Schneewind said in a statement. 
The researchers tested the mice using five different S. aureus strains that infect humans. The vaccine offered significant protection against all strains, they said. 
"This finding represents a promising step toward identifying potential components to combine into a vaccine designed for people at high risk of invasive S. aureus infection," said Dr. Anthony Fauci, director of the National Institute of Allergy and Infectious Diseases, which paid for the study. 
This approach is called rational drug design. Most vaccines use a whole virus or bacterium, which has been crippled or killed and injected into the body to help stimulate immune system recognition.  

But this approach has not worked against Staph aureus. 
Some companies are working on staph vaccines, too, including Nabi Biopharmaceuticals, whose experimental StaphVAX vaccine failed final human clinical trials last year.  



