


So much news about stem cells; in politics, in science, in biotech companies. 
And the other day I came across a good idea, but whether it is practical or beneficial, I do not know. 
What I am considering is the old teaching that a embryonic stem cell can grow up to become any cell. Now can it become a white-blood cell? Or can it become a lymphocyte cell? Or can it become any cell that attacks or removes foreign invaders of viruses or bacteria? 
Here is the method: take embryonic stem cells and you wish them to become white blood cells or lymphocytes and introduce these stem cells to say the bird flu virus H5N1, or say the AIDS virus, or say a cancer cell. 
So we have the embryonic stem cells and we are turning them into white blood cells or cells that protect the body from viruses or bacteria or cancer. And as we are in the early process of turning these embryonic stem cells into white blood cells we introduce the microbe or cancer as the white blood cell is being grown. Will the stem cell *remember* those microbes or cancer long enough so that once grown fully into the white blood cell or lymphocyte that it attacks such invaders? 
I do not know? Perhaps someone had already done such experiments. I am guessing that the stem cells somehow *remember* what it is they are to attack once grown. 
So if this is viable, then a new method of obtaining vaccines would be created. It would be especially useful should H5N1 go pandemic. For to obtain the vaccine, we simply take embryonic stem cells and introduce them to the virus, and then make the stem cell grow into a white blood cell. And so the virus it is meant to attack is well ingrained into the white blood cell. 
This method may also cure cancers. A person with a cancer prepares stem cells and introduces them to the cancer. Then the stem cells are grown to become white blood cells and proceed to attack the cancer. 
It depends on whether stem cells can be turned into any cell desired-- white blood or lymphocyte and whether they have a *memory*. Those are two big "ifs". 


