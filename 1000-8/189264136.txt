


In article < <EMAILADDRESS> >,   <EMAILADDRESS>  says... 
Well, yes - which strains are prevalent changes over time.  I  believe I mentioned something to this effect in my previous posting.   The key is surveillance.  If we can see which strains are causing  problems in other parts of the world, we can predict how those  strains will spread via population movements, and prepare  appropriate vaccines.  Unfortunately, there's a certain amount of  guesswork involved, and at a fine grained resolution sometimes the  local viral strains are not sufficiently close to the vaccine  produced for it to be effective. 

[snip] 
Yes, there are certainly major hurdles to be overcome in any sort of  mass vaccination programme.  Technical difficulties aside, the  social dimensions - such as media (and professional) exaggeration of  risks, both of the expected strain and of the vaccine, and of course  political/bureaucratic sluggishness - can be very challenging.  All  the more reason to practice before a serious epidemic *does* occur. 
GBS is certainly a problem, but rates are low (I last saw numbers in  the range of 1/100,000 for the US - your link says 8.3/1,000,000 for  the 76 swine flu vaccine) and 90% of those who react in this way to  the vaccine recover completely within a year.  Given the danger  inherent in contracting influenza to at risk segments of the  population (esp.elderly), the relative increase in GBS risk seems  like a fair trade off. 
Also, in the summer I read an interesting study published in the  Archives of Internal Medicine which used the UK-GPRD, and seems to  cast doubt on the correlation between vaccination and GBS.  The  confidence intervals for some of their numbers are a little large,  though, because of the extremely low incidence of GBS they found.   It does cover 8 years of data, however, and represents a mean sample  of more than 1 million patients. 
Here's the source: 
Hughes, Charlton, Latinovic, Gulliford (2006) "No association  between immunization and Guillain-Barre syndrome in the United  Kingdom" Archives of Internal Medicine 166(12)1301-1304 
And I found they have this article online now, in case you don't get  Archives:  <URL>  

That's why I offered several different studies which between them  cover a wide temporal span.  All the studies I offered appear to  show that the vaccines used in those cases were effective; this  implies that prediction methods are fairly accurate, and vaccine  production matches the predicted strains fairly well. 
Can I read you as saying that, in principle, influenza vaccines do  work?    

OK.  But then your objection to flu vaccines seems to revolve around  doubt that the current methods for predicting dominant strains are  accurate enough.  True? 
[snip a big bit we either agree on or you have yet to comment on] 

Sure, there's lots of stuff on vit.C and viral immunity generally,  and I agree that it seems well supported.  The source I cited is  particularly telling in this regard.  I just can't seem to find  anything specific to influenza.  Much obliged if you can provide  sources of this type. 

It's been suggested, and it's been studied.  It seems as though most  of the evidence suggests Hg poisoning was a bit of a blind alley,  though.  Hviid et al in JAMA 290pp1763-1766 (2003) found no  statistical correlation in their smallish Danish study.  This was  followed up with a long range study by Madsen et al (Pediatrics  112(3)604-606(2003)) which looked at autism rates between 1971 and  2000, compared to vaccination rates.  Denmark discontinued use of  thimerosal-containing vaccines in 1992, so they were particularly  interested in new diagnoses of autism btwn '92 and 2000 - rates  continued to rise. 
An extended critique of the vaccination-autism correlation  hypothesis is here: 
Nelson & Bauman (2003) "Thimerosal and Autism?" Pediatrics 111(3)  674-679 
 <URL>  

Actually, one of the problems is that ethyl mercury toxicity is not  well studied.  It's been presumed that methyl mercury toxicity could  be used as a model, but as Nelson and Bauman note in the commentary  I linked to above, there are some significant differences, including  the fact that ethyl-Hg breaks down more rapidly (i.e. is  metabolized to inorganic Hg) and doesn't cross the blood-brain  boundary as readily (methyl mercury is facilitated by active  transport).  
In any case, I was under the impression that nearly all childhood  vaccines in the US had had thimerosal eliminated.  Certainly, the  literature seems to suggest this. 
A nice summary of Hg toxicity: 
Clarckson, Magos & Myers (2003) "The Toxicology of Mercury ? Current  Exposures and Clinical Manifestations" New England Journal of  Medicine 349(18):1731-7  <URL>  
Specific to ethyl mercury: 
Magos (2001) "Review on the toxicity of ethylmercury, including its  presence as a preservative in biological and pharmaceutical  products" Journal of Applied Toxicology 21(1)1-5  <URL>  T (no abstract, but you can get to the pdf from this page)   

Neither. 
I appreciate your neighbourly expression of interest, but I'm not  certain what the parchment on my wall has to do with things.   


