


When Wacko had money, did he ever donate it to others?  Did he ever setup a legitimate charity? 
------------------------------------------- 
Mon Dec 11, 4:39 PM ET 
SEATTLE (Reuters) - The Bill & Melinda Gates Foundation on Monday pledged $83.5 million to fight malaria by paying for better controls, vaccine research and prevention of a disease that kills more than a million people a year. 
The Gates Foundation, the world's largest charity, said the new grants would also help pay for better malaria tests and for advocacy to get more attention for the killer disease. 
All totaled, the charity started by Bill Gates, the Microsoft Corp. chairman and world's richest man, and his wife, Melinda, has donated $765 million to fight malaria, which kills one person every 30 seconds. 
"The continuing toll of malaria is a moral outrage -- we would not allow it in the U.S., and we should not allow it anywhere," Melinda Gates, co-chair of the Gates Foundation, said in a statement. 
Melinda Gates will address a White House summit on Tuesday on malaria, which will bring together institutions, African civic leaders and non-governmental organizations to discuss and highlight measures for fighting the disease. 
Malaria infects between 300 million and 500 million people each year in more than 100 countries and kills at least 1.3 million. The parasite that causes the disease is transferred to humans from the bite of a malaria-infected mosquito. 
It has developed resistance to many of the drugs used to fight it, and work on a vaccine has been slow and not terribly successful. 
In her remarks to the summit, Gates plans to call for more resources to fight the disease. According to the Roll Back Malaria Partnership, global spending on malaria control falls far short of the $3.1 billion needed annually. 
"It's time to close the gap in funding, accelerate research and work together in a more strategic way to strengthen the global malaria fight," Melinda Gates said. 
She said she would also urge world leaders to agree on a new, coordinated global strategy to fight malaria. 
Of the Gates Foundation's grant, $29 million is earmarked for the development of a network of African countries committed to fighting malaria and $7.1 million to the Roll Back Malaria Partnership to provide technical assistance to malaria programs in southern Africa. 
About 90 percent of all deaths related to malaria occur in Africa and the disease is the leading killer of African children, claiming more than 2,000 lives a day. 
The grant also will fund $29.3 million for malaria vaccine research and $9.8 million to evaluate existing diagnostic tests for malaria and develop guidelines for the use of malaria tests in the field.  

