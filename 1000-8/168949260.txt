


Job Title:     Drug Safety Specialist Job Location:  NY: Nyack Pay Rate:      Open Job Length:    4-6 months (contract) Start Date:    2006-10-14 
Description:   Title: Drug Safety Specialist 
Company:  Pharmaceutical Company 
Location:  Rockland County, NY 
Status: Full-time consulting engagement, 40 hrs/week 
Length:  Long-term assignment in 12 month increments 
The purpose of this job is to coordinate timely review and efficient tracking of Serious Adverse Event (SAE) information reported from vaccines clinical trials worldwide.  Review and process SAE reports to facilitate expedited (24-48 hour) turn around of information to the Global Safety Surveillance and Epidemiology (GSSE) Department and Regulatory Affairs. All work is to be performed in a compliance manner according to internal policies, procedures, and government regulations.  
Requirements:   Bachelors Degree in Biological Sciences, Nursing or related discipline plus 3 years Clinical Research/Pharmaceutical or related experience.  Thorough understanding of GCPs, ICH Guidelines and US Federal Regulations on Adverse Event Reporting.  Working knowledge of medical terminology and disease states.  Proficient data entry and computer skills including: GroupWise, MS Word, Excel, Access, PowerPoint. 2+ years of experience in clinical research, safety or healthcare related field.  Project management skills, time management skills and ability to prioritize, troubleshoot and handle multiple tasks.  Detail oriented with a high degree of accuracy.  Excellent written and verbal communication skills.  Committed to working in a fast paced, team-oriented environment.  Ability to meet and maintain very tight deadlines while managing organization and punctual work ethics.   
-Three different Blue Cross Health Plans  
-Dental Insurance  
-401k  
-Life Insurance & AD&D  
-Long-term Disability  
-Paid Holidays  
-Up to 4 weeks of Vacation  
-Continuing Education Reimbursement up to $5,250 /yr  
-Section 529 Tax-Deferred College Savings Plan 
Please refer to Job code CB3788 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



