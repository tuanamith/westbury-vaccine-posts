


In article < <EMAILADDRESS> >,  < <EMAILADDRESS> > wrote: 



   			...................... 













If one looks at things carefully, essentially nothing is repeatable, especially on people.  While probability came from a generalization of relative frequency, it is not relative frequency. 


I am quite aware of this.  However, statistical significance does not tell me what is needed.  What I have to compare are the expected risks of the various treatments, and while for a given sample size, a given significance level usually  indicates a better assessment of the risk, even this is not always true.  You are on my grounds now, as the subject of decision making under uncertainty is my field. 



But they should be used in public health.  And if an avian flu vaccine is made, it will not meet the FDA standard for a drug; we are not going to test it on even dozens of people who will receive avian flu. 


Who is going to do an adequate study on nutritional  therapy?  With at least 100 nutrients involved, and combinations being important, trillions will not suffice. And how are you going to TELL the treatment and control groups just what nutrients to consume, what exercises to carry out, etc.?  Many nutrients are toxic. 
There is not ONE study which shows that a low fat diet is better than a high fat diet with the same number of calories.  We understand drugs and surgical treatments far better than we do nutrients. 


Decision theory is what an individual should use.   That most are ignorant of it does not mean that it is not to be used; the values in making a decision come from the individual, not the doctor or  the  FDA.  Now approximations have to be made, but a logical approach to decision making under uncertainty is what we have known for more than a half century. 



