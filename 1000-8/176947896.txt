



From:  <URL>  
What is Atheism Really All About? (1996) Richard Carrier 
"He who decides a case without hearing the other side, even if he decides justly, cannot be considered just" -- Seneca 

-------------------------------------------------------------------------------- 
What is an Atheist? 
An atheist is a person who does not believe that any gods exist. 
Inner Quest (IQ) - Belief in any God or gods is not required. 
Why don't you believe in God? 
There is simply no more evidence for Jehovah than there is for Zeus. Christians find no reason to believe that Zeus exists, so they do not believe in him. For the same reason, I do not believe in Jehova. God himself is more than welcome to share an honest conversation with me. Until he does, I have no reason to trust that anyone is a reliable spokesman for any god. 
IQ - Stick with reason and logic. Analyze, test and question everything. Accept only that which is reasonable based on your personal experience and reject all that are not. 
Don't you want to go to heaven? 
I do not believe there is a heaven. But even if a real heaven did exist, and for some reason a god chose who went and who didn't, if that god is a good and noble being he will judge me for my value as a human being, and not for my belief in him. 
IQ - Belief is not required. Provided we do to and for others what we want them to do for us, then we will do well, indeed. For as long as we live, think and act according to what we believe is right, this will suffice. God will approve. 
How can you turn your back on true happiness? 
I cannot imagine being happier than I have been already. I live a very spiritual, fulfilling life, and am filled with an abiding love of being and thinking. I find love, reason and a practical, humble approach to life to be more than enough for me. 
IQ - So will it be enough also for God. 
How can you trust sinful humans, ignoring all the good god does? 
It offends me that an invisible god is given credit for every good thing that happens in the world, while every evil is blamed on humanity. There is much evil in the world that is not the fault of human beings, such as ignorance and disease and droughts, and most of the things that are good are entirely the product of human love, effort or genius, such as friendship and vaccines and even irrigation pipes. 
Not all human beings are evil. We all possess great potential for good. Yet a god could do so much good in the world that is not being done, such as warning innocent children when to stay away from danger, or preventing too many people from being born, or turning all the weapons in the world into flowers. Surely a loving god would do these things, and more, just as any wise and compassionate human being would if they had the means. 
And so, when a doctor saves someone's life, we truly owe our thanks to the doctor, and the society that made her education possible. It is insulting to both when a god is thanked for something that he could have done himself but didn't. If a loving god really existed, we would not need doctors in the first place. 
IQ - Personal responsibility and accountability is the law and applies to everyone. Just to keep this explanation short for now but again, no one needs to believe us. For lack of space, I have to redirect you to this link:  <URL> . 
If there is no god, then where do you think the universe came from? 
I do not even know if the universe had a beginning, much less what may have started it. No one knows. Inventing a god to do the creating only leaves open the question of where that god came from. 
IQ -- Those of us still here one earth have no urgent need to have this information. However, these studies are taken up in the study of Theosophy and anyone interested can check out those studies. 
So why be moral? 
I dislike the kind of people who hurt me or lie to me or who are insincere or inconsiderate. Thus, if I were to be like such people, I could not escape disliking myself. I could never do something that would make me the sort of person I hate, because I could never be truly happy if I hated myself, no matter how hard I tried to rationalize what I have done. But this also means that to truly like myself, and thus to be truly happy, I must be the sort of person I really like, and I like people who are honest and principled and who care about others. So I strive to be like the sort of person I see to be good. I have also found that virtue earns stronger and fonder friendships, and secures the trust of my neighbors, and both of these things are essential to living a good, full life. 
IQ - This is all that God, Jesus, Buddha and Mohammed advise that we do. Righteousness is love in action. This is the only Way to God and eternal life. 
What do you think happens when you die? 
I see that the brain is what gives me existence, and I depend on its health for my ability to think and survive. When the brain dies, I die, and when the brain ceases to exist, so do I. I do not find this to be sad. We all enjoy everything we experience, even when it doesn't last. I love life deeply, and as death would end my experience of living and loving I do not want to die. But I do not fear death, because there is no reason to fear the end of fear itself. 
IQ -- Because the material sciences is almost exclusively based on what we can perceive with our 5 senses, then their findings leave much to be desired. There are higher planes of existence. They can be apperceived by availing of our spiritual faculties, abilities that anyone who wishes can develop thereby proving their validity and truth. The brain dies since it is a physical organ. The spirit, mind or consciousness survives. 
What about all the people who experience god? 
There are people in the world who experience the essence of Buddha, who remember past lives, who truly feel the power of ritual magic in their lives, or who walk with the spirits of their ancestors. There are so many different experiences I do not think it is wise to arbitrarily assume that any one of them is truer than another. 
I have looked all over the world, and I see Buddhists are mostly in Asia, Hindus mostly in India, Muslims mostly in the Middle East, and Christians mostly in the West. The idea of god, and all the assumptions of our respective religions, are taught to us as children. That Americans are mostly Christian is more likely the result of Christianity being taught there, and not the result of that religion actually being true or superior to any other. 
IQ -- Truth is relative depending on the level of the spirit entity in the order of evolution. These are higher concepts that can be learned if one should decide to pursue these higher studies. 
Haven't Christian values done much good in the world? 
I know that people have done much good in the world, whether they were Christians like Martin Luther King Jr., or Hindus like Gandhi, or atheists like Elizabeth Cady Stanton. Honesty and compassion are good values anywhere. They are not unique to Christianity. 
IQ -- Of course. All men are brothers regardless of their creed, color and whatever difference. 
So what do you believe in? 
I believe in many things. I believe in the potential of humanity, in the power of reason, in the comfort of love, and in the value of truth. I also believe in the beauty and joy of human experience, and the nearly unlimited power of the human will to endure almost any hardship or solve almost any problem. 
I believe that faith can mislead people into falsehood, and that we need reason and doubt as necessary checks against our capacity for error. I believe that we need to allow our fellow human beings to make choices for themselves and to live the life they wish to, in mutual peace and goodwill. 
I believe that political negotiation and compromise -- fuelled by an honest measure of respect for different opinions, beliefs and lifestyles -- is the only way the world will find universal peace and goodwill, and that using the scientific method is the only way the world can arrive at an agreement on the truth about anything. 
I believe that it is better to preach the gospel of "be good to your fellow man, and love each other as life itself," than to preach the gospel of "believe in our religion or be damned." For it is better to be good to each other and to build on what we all agree to be true, than to insist that we all think alike. 
IQ -- Absolutely. We agree. 


