


Dear Alan, 
Alan Meyer wrote: 

Do you mean even if the science is hopeless or it's hopeless to persuade the person to believe in it? Supposing science cannot offer a solution to a problem. Then what? 
Like you I operate my life according to the rules of science and If I have a health problem I go to the best allopathic doctor (e.g., non-alternative, western) I can find. 

Yes. These are theories not gospel truths. 
The truth is we know very little about what causes cancer.  For example, now that they are decoding the human genome and are looking at cancer at the cellular level they are learning more about "on" and "off" switches and other mechanisms that cause cancer.  Let's hope a cure follows. 

They also kill healthy cells which leads to other dysfunctions in the body which may cause cause a lot of pain and suffering as well as damage to other organs. 

Yes, and has been said many times, sometimes the cure is worse than the disease.  As the wife of a prostate cancer patient and a melanoma patient I am very disappointed in what science has achieved in this area.  To borrow a phrase, "Science, be not proud." 
Re the melanoma: It was cut out by the doctor and all my husband was left with is a scar.  He participated in a trial of a vaccine and that was painless. All in all I would say that was a compassionate cure. All he has to do now is go for periodic checkups and body scans. 
Re prostate cancer: Husband had robotic laparascopic surgery done by world-famous surgeon at world-famous hospital. You call it "surgery" because that sanitizes the whole thing but I call what was done to my husband "mutilation."  A robot cut my husband's genitals and rendered him completely impotent (and very mildly incontinent).  Science can brag about robots, but you know it doesn't really matter if you were cut by a robot or by a cave man with a butter knife if the if result is the same. Being rendered impotent for a man in his prime is probably the worst thing that could happen.  I know I grieve everyday for his loss and I can't even begin to imagine how he feels.  I'd rather not go there.  All in all,  I would call this treatment savage. 
On the subject of treating prostate cancer in a more humane  way. Here's a hypothetical: 
Suppose the medical establishment held a conference about a new disease called "prostate cancer". All the luminaries were present and they were brainstorming about treatment ideas.  Well, the first person would say (logically) "'What we are looking for here is a treatment that does not have to be done ON-SITE.  First thing, lets spare the genitals. We know how important they are to us and it says, "Do unto others." 
Given what's in the neighborhood of the tumor, namely penis, tesicles, scrotum, etc., we must come up with a non-invasive cure such as a DRUG, a VACCINE or a GENE THERAPY, or something completely new.  Such treatments would surely cause less misery than the present ones.  I have had to watch my husband's genitals "change shape," as Dr. Scardino puts it. It shocks me and distresses me to no end to look at my husband's shrunken testicles. 
Millions of people are defaced and mutilated every year in the name of cancer treatment.  Women having their breasts cut off is so common that we don't even think anything about it. But the breastless woman does. I could give you an example: my husband mother was a beautiful, vibrant person but at age 30  was diagnosed with breast cancer.  She had a double mastectomy.  She was never the same person afterwards, became seriously depressed and committed suicide leaving behind three children.  I would have to say that cutting off the breasts of a very young woman is a pretty brutal way to treat a disease. 

What can be more brutal to a man than "hormone therapy"? 

They will be the first to admit there is a whole lot they don't know about cancer. 
There are many cancers in which medicine is rendered completelly powerless. Forex, pancreatic.  I have a cousin and a friend who had pancreatic cancer and both died within months of diagnosis.  One was a young woman whose husband is a friend of ours.  The woman left behind a 15-year-old son who was so traumatized by her sudden death that he went into a psychotic state and has been in and out of hospitals.  Before that he was an honor student. 

I think modern medicine has done miraculous things for mankind but I don't think cancer treatment is one of them.  Based on my own experience it has a long way to go. 
On the other hand, amazing thing have been done in epidemiology, for example.  Malaria is now curable, but the best example of all is AIDS: When it first arrived on the scene only 20+ years ago it was viewed as completely incurable.  It was a death sentence.  Now it can be controlled with a "cocktail" of drugs and the patient can lead a normal life. 
What we need is a "Manhattan Project" that would focus on curing prostate and other cancers in a compassionate way  Let's hope it happens soon. 
All the best. 
Leah 


