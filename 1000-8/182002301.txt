



 <EMAILADDRESS>  wrote: 
I don't believe I've ever seen a regular refer anyone to the Encyclopedia Britannica. The talk.origins FAQ, yes. It can be tiresome at times to repeat the same introductory information over and again when it has been well organized and clearly presented, and is readily available in the FAQ. 
Did you have trouble with some of it, or do you resent having to look something up, even given a link? If you had trouble with a portion of it, it might be too sparse or poorly presented, and we would welcome ideas for improvement. 

Einstein said science should be as simple as possible, but not too simple. Some subjects just can't be simplified any further than when they are presented here, without being misrepresented. OTOH, evolutionary theory itself is fairly straightforward. Too subtle for some folks apprently, but not that complicated, nor does it require much background knowledge to understand it. 

Rather all-inclusive. Did you have a specific suggestion in mind? 

I've seen folks get impatient, but I've never seen any claim that knowledge necessary for understanding is only for the approved inner circle. Cold it be that you asked a technical question about one subject, and were told that you needed certain background information to understand it? That's a little different, and can't always be helped. 

Perhaps. Without funding, research stops, and so learning stops. For scientists and science groupies, it's all about the learning. Some folks do not understand that, and think that it's all about the money. I feel sorry for those people. 

Evolutionary science is the foundation of biology. It is directly used for engineering new crops  and breeds of domestic animals, understanding wetlands and wildlife refuge management, attempting to control destruction of the ocean ecosystem and anticipating some of the effects of global warming, rapid development of vaccines and pharmaceuticals, and investigations into the pathology and treatments of cancer, autoimmune disorders, etc. To name a few "hobbies". 
The attack on evolutionary science is just one of many fronts in an ongoing war on science and reason. 

True. All we have is evidence, testable models, successful predictions, and a theory that is supported by multiple fields of science. Can't get much more hollow than that, I guess. 

99.9% of scientists would disagree with you. Why is that? 

You're right, it does sometimes happen. 
But suppose the attack isn't an attack? Here are examples of things which are *not attacks: Pointing out that a theory doesn't answer every question yet. Claiming that "all evolutionists are atheists". Asserting that a theory is "just a theory". Repeating that "there is no evidence", even when evidence has been offered to you. Etc. 
Sometimes when these are repeated several times in one thread folks can get testy. But this isn't a fist fight, in which if I block your jab I might miss your next jab. In a philosophical or scientific argument, if an assertion is refuted it stays refuted. Forever, actually, altho it's especially bad form to repeat them in the same conversation (or thread). 
Kermit 


