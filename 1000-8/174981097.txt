


 <URL>  
Drug Errors Send 700,000 to ER Every Year Wednesday, October 18, 2006 

By Miranda Hitti 

Experts from the CDC and FDA estimate that every year, more than 700,000 people visit U.S. emergency departments because of side effects, accidental overdoses, and other adverse drug events. 

Those patients are particularly likely to be 65 years old or older, note the researchers, who included the CDC's Daniel Budnitz, MD, MPH. 

In fact, nearly as many people aged 65 and older visit emergency departments for drug events as for automobile accidents, Budnitz's team notes. 

Their study appears in The Journal of the American Medical Association. 

The study's message: America may need to do more to help people avoid adverse drug events. If you're taking any drugs, you may want to check that you're doing so correctly. 

Seven Dangerous Drug Mistakes 

About the Study 

Budnitz and colleagues tracked adverse drug events reported at 63 U.S. hospitals from 2004 through 2005. 

"Drugs" included prescription drugs, over-the-counter drugs, vaccines, vitamins, dietary supplements, and herbal products. 

"Adverse drug events" included allergic reactions, undesirable side effects at recommended doses, accidental overdoses, or "secondary effects," such as falls or choking. 

The data don't cover drug-related deaths, suicide attempts, drug abuse, symptoms of drug withdrawal, or drug failures. 

Report: Drug Errors Injure 1.5 Million 

Thousands of Cases 

During the study period, the hospitals reported nearly 21,300 emergency department visits due to adverse drug events. 

That equals 2.4 cases per 1,000 people, or more than 701,000 patients per year nationwide, Budnitz and colleagues estimate. 

Most patients were treated and released from emergency departments. But about 117,000 patients per year require hospitalization for adverse drug events, the researchers estimate. 

Medical Errors Still Plague U.S. Hospitals 

Drug Data 

Most adverse drug events were due to accidental overdoses and allergic reactions. 

Many of those cases stemmed from a "relatively small set of drugs" that require monitoring to avoid toxic build-up, the researchers note. 

They write that 16 of the 18 drugs most commonly linked to adverse drug events in their study "have been in clinical use for more than 20 years." 

The five most common drug classes noted in those events were insulin, painkillers containing opioids, anticlotting drugs, drugs containing the antibiotic amoxicillin, and antihistamines/cold remedies. 

Health Care Varies Greatly at U.S. Hospitals 

Age Pattern 

Many people treated for adverse drug events were at least 65 years old. 

A quarter of adverse drug events and half of those requiring hospitalization were seen in people aged 65 and older, the researchers note. 

Compared with younger adults, those aged 65 and older were: 

More than twice as likely to be treated in emergency departments for adverse events Nearly seven times as likely to be hospitalized because of adverse drug events 

People aged 65 and older tend to use more drugs than younger adults, the researchers point out. 

Nearly a third of ER-treated cases in people aged 65 and older were linked to three drugs: 

--Insulin 

--Coumadin, which helps prevent blood clots 

--Digoxin, which helps injured or weak hearts work more efficiently and send blood through the body 

The researchers call for renewed efforts to prevent adverse drug events. 

By Miranda Hitti, reviewed by Louise Chang, MD 

SOURCES: Budnitz, D. The Journal of the American Medical Association, Oct. 18, 2006; vol 296: pp 1858-1866. WebMD Medical Reference in collaboration with The Cleveland Clinic: "Heart Disease: Warfarin and Other Blood Thinners." WebMD Medical Reference in collaboration with The Cleveland Clinic: "Heart Disease: Treatment With Digoxin." News release, JAMA/Archives 




