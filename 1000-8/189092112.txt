


By MARTHA IRVINE, AP National Writer Sat Nov 25, 6:46 PM ET Yahoo News 


Zach Plante is close with his parents - he plays baseball with them and, on weekends, helps with work in the small vineyard they keep at their northern California home. 

Lately, though, his parents have begun to notice subtle changes in their son. Among other things, he's announced that he wants to grow his hair longer - and sometimes greets his father with "Yo, Dad!" 
"Little comments will come out of his mouth that have a bit of that teen swagger," says Tom Plante, Zach's dad. 
Thing is, Zach isn't a teen. He's 10 years old - one part, a fun-loving fifth-grader who likes to watch the Animal Planet network and play with his dog and pet gecko, the other a soon-to-be middle schooler who wants an iPod. 
In some ways, it's simply part of a kid's natural journey toward independence. But child development experts say that physical and behavioral changes that would have been typical of teenagers decades ago are now common among "tweens" - kids ages 8 to 12. 
Some of them are going on "dates" and talking on their own cell phones. They listen to sexually charged pop music, play mature-rated video games and spend time gossiping on MySpace. And more girls are wearing makeup and clothing that some consider beyond their years. 
Zach is starting to notice it in his friends, too, especially the way they treat their parents. 
"A lot of kids can sometimes be annoyed by their parents," he says. "If I'm playing with them at one of their houses, then they kind of ignore their parents. If their parents do them a favor, they might just say, 'OK,' but not notice that much." 
The shift that's turning tweens into the new teens is complex - and worrisome to parents and some professionals who deal with children. They wonder if kids are equipped to handle the thorny issues that come with the adolescent world. 
"I'm sure this isn't the first time in history people have been talking about it. But I definitely feel like these kids are growing up faster - and I'm not sure it's always a good thing," says Dr. Liz Alderman, an adolescent medicine specialist at Montefiore Medical Center in New York City. She's been in practice for 16 years and has noticed a gradual but undeniable change in attitude in that time. 
She and others who study and treat children say the reasons it's happening are both physical and social. 
Several published studies have found, for instance, that some tweens' bodies are developing faster, with more girls starting menstruation in elementary school - a result doctors often attribute to improved nutrition and, in some cases, obesity. While boys are still being studied, the findings about girls have caused some endocrinologists to lower the limits of early breast development to first or second grade. 
Along with that, even young children are having to deal with peer pressure and other societal influences. 
Beyond the drugs, sex and rock'n'roll their boomer and Gen X parents navigated, technology and consumerism have accelerated the pace of life, giving kids easy access to influences that may or may not be parent-approved. Sex, violence and foul language that used to be relegated to late-night viewing and R-rated movies are expected fixtures in everyday TV. 
And many tweens model what they see, including common plot lines "where the kids are really running the house, not the dysfunctional parents," says Plante, who in addition to being Zach's dad is a psychology professor at Santa Clara University in California's Silicon Valley. 
He sees the results of all these factors in his private practice frequently. 
Kids look and dress older. They struggle to process the images of sex, violence and adult humor, even when their parents try to shield them. And sometimes, he says, parents end up encouraging the behavior by failing to set limits - in essence, handing over power to their kids. 
"You get this kind of perfect storm of variables that would suggest that, yes, kids are becoming teens at an earlier age," Plante says. 
Natalie Wickstrom, a 10-year-old in suburban Atlanta, says girls her age sometimes wear clothes that are "a little inappropriate." She describes how one friend tied her shirt to show her stomach and "liked to dance, like in rap videos." 
Girls in her class also talk about not only liking but "having relationships" with boys. 
"There's no rules, no limitations to what they can do," says Natalie, who's also in fifth grade. 
Her mom, Billie Wickstrom, says the teen-like behavior of her daughter's peers, influences her daughter - as does parents' willingness to allow it. 
"Some parents make it hard on those of us who are trying to hold their kids back a bit," she says. 
So far, she and her husband have resisted letting Natalie get her ears pierced, something many of her friends have already done. Now Natalie is lobbying hard for a cell phone and also wants an iPod. 
"Sometimes I just think that maybe, if I got one of these things, I could talk about what they talk about," Natalie says of the kids she deems the "popular ones." 
It's an age-old issue. Kids want to fit in - and younger kids want to be like older kids. 
But as the limits have been pushed, experts say the stakes also have gotten higher - with parents and tweens having to deal with very grown-up issues such as pregnancy and sexually transmitted diseases. Earlier this year, that point hit home when federal officials recommended a vaccine for HPV - a common STD that can lead to cervical cancer - for girls as young as age 9. 
"Physically, they're adults, but cognitively, they're children," says Alderman, the physician in New York. She's found that cultural influences have affected her own children, too. 
Earlier this year, her 12-year-old son heard the popular pop song "Promiscuous" and asked her what the word meant. 
"I mean, it's OK to have that conversation, but when it's constantly playing, it normalizes it," Alderman says. 
She observes that parents sometimes gravitate to one of two ill-advised extremes - they're either horrified by such questions from their kids, or they "revel" in the teen-like behavior. As an example of the latter reaction, she notes how some parents think it's cute when their daughters wear pants or shorts with words such as "hottie" on the back. 

"Believe me, I'm a very open-minded person. But it promotes a certain way of thinking about girls and their back sides," Alderman says. "A 12-year-old isn't sexy." 
With grown-up influences coming from so many different angles - from peers to the Internet and TV - some parents say the trend is difficult to combat. 
Claire Unterseher, a mother in Chicago, says she only allows her children - including an 8-year-old son and 7-year-old daughter - to watch public television. 
And yet, already, they're coming home from school asking to download songs she considers more appropriate for teens. 
"I think I bought my first Abba single when I was 13 or 14 - and here my 7-year-old wants me to download Kelly Clarkson all the time," Unterseher says. "Why are they so interested in all this adult stuff?" 
Part of it, experts say, is marketing - and tweens are much-sought-after consumers. 
Advertisers have found that, increasingly, children and teens are influencing the buying decisions in their households - from cars to computers and family vacations. According to 360 Youth, an umbrella organization for various youth marketing groups, tweens represent $51 billion worth of annual spending power on their own from gifts and allowance, and also have a great deal of say about the additional $170 billion spent directly on them each year. 
Toymakers also have picked up on tweens' interest in older themes and developed toy lines to meet the demand - from dolls known as Bratz to video games with more violence. 
Diane Levin, a professor of human development and early childhood at Wheelock College in Boston, is among those who've taken aim at toys deemed too violent or sexual. 
"We've crossed a line. We can no longer avoid it - it's just so in our face," says Levin, author of the upcoming book "So Sexy So Soon: The Sexualization of Childhood." 
Earlier this year, she and others from a group known as the Campaign for a Commercial-Free Childhood successfully pressured toy maker Hasbro to drop plans for a line of children's toys modeled after the singing group Pussycat Dolls. 
Other parents, including Clyde Otis III, are trying their own methods. 
An attorney with a background in music publishing, Otis has compiled a line of CDs called "Music Talking" that includes classic oldies he believes are interesting to tweens, but age appropriate. Artists include Aretha Franklin, Rose Royce and Blessid Union of Souls. 
"I don't want to be like a prude. But some of the stuff out there, it's just out of control sometimes," says Otis, a father of three from Maplewood, N.J. 
"Beyonce singing about bouncing her butt all over the place is a little much - at least for an 8-year-old." 
In the end, many parents find it tricky to strike a balance between setting limits and allowing their kids to be more independent. 
Plante, in California, discovered that a few weeks ago when he and Zach rode bikes to school, as the two of them have done since the first day of kindergarten. 
"You know, dad, you don't have to bike to school with me anymore," Zach said. 
Plante was taken aback. 
"It was a poignant moment," he says. "There was this notion of being embarrassed of having parents be too close." 
Since then, Zach has been riding by himself - a big step in his dad's mind. 
"Of course, it is hard to let go, but we all need to do so in various ways over time," Plante says, "as long as we do it thoughtfully and lovingly, I suppose." ***************************** 

This is happening in LoS as well,  we were shopping at Central Laard Praaw this afternoon and the numbers of early teenagers dressed in short skirts, or skintight jeans and skimpy tops at the weekends is much more evident than even 2 or 3 years ago.  Another source of controversy at present is the school uniforms worn by female students, the (very) tight white blouses and (very) short skirts that the girls wear have many parents concerned.  
Sandy Huay Khwaang 


