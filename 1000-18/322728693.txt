


Job Title:     Biochemist - Vaccines Research - Immunoassays - Wayne, PA Job Location:  PA: Wayne Pay Rate:      Open Job Length:    full time Start Date:    2007-12-28 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Biochemist - Vaccines Research - Immunoassays - Wayne, PA &#150; BIO001653 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. The successful candidate will be a self-motivated, team player responsible for clinical trial sample testing for a specific scientific area. Experience and working knowledge of standard biochemical, immunological and virological methods is required. Knowledge of GLP/GMPs is highly desirable, and The candidate must have demonstrated computer proficiency in word processing and spreadsheet applications. Requires excellent communication and interpersonal skills to contribute to a cohesive, working team environment Responsibilities also include, but are not limited to: .      Executing laboratory testing with limited supervision .      Analyzing and producing highest quality data. .      Establishing time management skills for efficiency with daily activities .      Maintaining laboratory reagents. .      Trouble shooting assay/laboratory equipment issues .      Recording, analyzing, interpreting, preparing and presenting complex data.  .      Ensuring compliance with regulatory and safety practices .      Calibrating laboratory instruments. .      Qualifying assay reagents .      Writing/reviewing standard operating procedures (SOPs) 
 Qualifications 
The qualified applicant should possess a BS in Biology or related areas with 1+ years experience. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #BIO001653. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-Wayne   Employee Status  Regular  
Additional Information   Posting Date  12/20/2007, 02:00 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/03/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-363738 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



