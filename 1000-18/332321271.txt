


Job Title:     Biologist - Biochemistry - Alzheimers Disease Research -... Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-01-17 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Biologist - Biochemistry - Alzheimers Disease Research - West Point, PA &#150; BIO001675 
Job Description  
Submit 
Description At Merck, we believe in putting patients first in all we do. This is the commitment that Merck &amp; Co., Inc. stands on and, it is what has distinguished us as one of the worlds leading research-driven pharmaceutical companies. Consistently cited as a great place to work, we discover, develop and manufacture a wide range of novel medicines and vaccines that deliver true advances in patient care. Each of our employees is joined by an extraordinary sense of purpose&#8212;bringing Merck&#8217;s finest achievements to people around the world. We are seeking a highly motivated Research Biologist to work in a team identifying and developing novel therapeutics for Alzheimers disease, Neurodegenerative and Neuropsychiatric Disorders. Experience within a Molecular and Cellular biology laborartory with Protein biochemistry is required.  Prior pharmaceutical experience is a plus although highly motivated candidates with Academic experience looking for a demanding and rewarding position will be considered.  In vivo pharmacological experience as well as prior experience developing pharamcodynamic measues is highly desirable. A candidate with experience in separation- based technologies such as HPLC and LC-MS/MS is also highly desirable. The duties may also include siRNA and cDNA transfections, Recombinant protein cloning and expression, Site-directed mutagenesis, ELISA, Immunopurification, Fluorescence Immunocytochemistry, Protein/RNA extraction, purification and separation from animal tissues, SPA assay, automation and HTS-assay development. Additional duties will include analysis of primary data, laboratory notebook maintenance and presentation to management. The candidate would be well served to have a desire to work within a collaborative, fast-paced and intellectually challenging environment. 
Qualifications 
Immunodetection skills - ELISA, Mesoscale, alphascreen, Immunoprecipitation, etc. Molecular biology skills - subcloning cDNA, expression analysis by taqman, bDNA, etc..BS/BA + 1-3 years research experience in vivo pharmacology, LC-MS/MS, HPLC.MS + 3 years pharmaceutical research experience. 
Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #BIO001675.Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  01/15/2008, 07:15 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  03/16/2008, 12:59 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-369236 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



