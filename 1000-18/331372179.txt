






Sometimes, I can be! 

ax 


I get tired of doing all your home work! 
Vaccine Excise Tax Revision 
The Vaccine Injury Compensation Trust Fund (the Fund) provides funding for vaccine-related injuries or deaths from vaccines administered on or after October 1, 1988. The Trust Fund is currently funded by an excise tax imposed on each dose of covered vaccine. Originally, these taxes were imposed on each vaccine using a risk-based formula in which DTP, DTaP, or any pertussis containing combination was taxed at $4.56 per dose; DT, Td, or TT was taxed at $0.06; MMR, MR, M, or R was taxed at $4.44; and polio (both OPV & IPV) was taxed at $0.29. Over time, changes in the recommended schedule of vaccinations, the increasing use of combination vaccines, and the addition of new vaccines for coverage made the risk assessment for covered vaccines increasingly difficult to determine. Recognizing these limitations, the Secretary of Health and Human Services proposed legislation which eventually passed by Congress called the Taxpayer Relief Act of 1997 (P.L. 105-34). Signed into law by the President on August 5, 1997, the legislation repealed the existing risk-based excise tax structure, replacing it with a flat-tax rate of 75 cents per dose for all covered vaccines. Three-dose vaccines such as DTP, DtaP and MMR are now taxed at $2.25; two-dose vaccines (e.g., Td, MR or DT) are now $1.50; and single-dose vaccines (e.g., OPV, TT, R) are now 75 cents. These revisions went into effect on August 6, 1997. 
 I hope that helps, Pie Man! 

On Jan 12, 5:12=A0pm, "vernon O" < <EMAILADDRESS> > wrote: ax t 


