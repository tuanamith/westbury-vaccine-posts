


Job Title:     Senior Statistician Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-02-06 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Statistician &#150; STA000435 
Job Description  
Submit 
This position will support the Clinical Biostatistics and Research Decision Sciences (&#8220;CBARDS&#8221;) Department in the Merck Research Laboratories Division. The Senior Statistician is responsible for the planning of study designs and execution of statistical analyses of clinical trial data, under the general guidance of management. This position requires a sufficient knowledge of statistical methodology and experimental study design to ensure that sound scientific principles and statistical methods are applied to the planning and execution of analyses. This position requires expert programming skills for the extraction and manipulation of clinical trial data for the purpose of analyses. Responsibilities of this position include but are not limited to the following: write programs to retrieve data, perform statistical analyses and ensure that all programs meet analysis requirements, internal SOP&#8217;s, validation, and external regulatory requirements, design studies, analyze study data and interpret results from clinical trials to meet objectives of study protocol, and prepare oral and written reports of clinical trials to senior management and/or regulatory agencies. Excellent oral and written communication skills are required for interaction with outside investigators, clinical and regulatory staff in the planning and analysis of clinical trial data and for the preparation of statistical analysis plans and reports.  
Qualifications 
Requires a Masters degree in Biostatistics. Requires knowledge of statistical methodology, experimental design, and some medical training background, plus at least two years of related experience. Requires solid programming expertise including a working knowledge of SAS, particularly SAS/BASE, SAS/STAT, SAS/IML, SAS/GRAPH, SAS/MACRO, SAS/SQL, SAS/ODS, S-plus, R, SPSS, M-Plus, nQuery and Amos. Must be familiar with the basic operation of a computer system, other database processing systems such as Oracle, Unix/Linux and M.S. SQL Server. Must have proof of legal authority to work in the United States. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # STA000419. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  02/05/2008, 02:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/19/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-368476 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



