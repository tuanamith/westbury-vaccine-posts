


How politics pushed the HPV vaccine ...  

 <URL>  ANDRÉ PICARD  
From Saturday's Globe and Mail 
August 11, 2007 at 1:10 AM EDT 
Related Articles Recent 
HPV vaccinations could be routine for B.C. girls   From the archives 
This has caused a lot of us in public health and medical circles to flinch, she said.  
There has never been an issue around women's health that has had this level of unanimity. It wasn't a difficult decision. 
Why are politicians making medical decisions? This is not how health-care delivery should be decided. 
Yet, there are real effects from the politicization of the process.  
There was an opportunity for political gain and it looks like that took priority over everything else, she said. 
Yet, the momentum to vaccinate, to invest billions of dollars in the HPV vaccine, seems unstoppable. 
Ms. Rochon Ford has a similar wish. 
I hope the moms of 12-year-olds who are worried about cervical cancer don't lose sight of that reality. 

