


Mercury and Vaccines (Thimerosal) 
Thimerosal is a mercury-containing preservative used in some vaccines and other products since the 1930s. There is no convincing scientific evidence of harm caused by the low doses of thimerosal in vaccines, except for minor reactions like redness and swelling at the injection site. However, in July 1999, the Public Health Service agencies, the American Academy of Pediatrics, and vaccine manufacturers agreed that thimerosal should be reduced or eliminated in vaccines as a precautionary measure. 
Since 2001, with the exception of some influenza (flu) vaccines, thimerosal is not used as a preservative in routinely recommended childhood vaccines. CDC Statement on Autism and Thimerosal 
As the country's leading public health agency, the Centers for Disease Control and Prevention (CDC) is committed to protecting the health of all Americans--including infants, children, and adolescents. CDC shares with parents and many others great concern about the number of children with autism spectrum disorders (ASD). We are committed to understanding what causes autism, how it can be prevented, and how it can be recognized and treated as early as possible. 
Recent estimates from CDC's Autism and Developmental Disabilities Monitoring network found that about 1 in 150 children have an ASD. This estimate is higher than estimates from the early 1990s. Some people believe increased exposure to thimerosal (from the addition of important new vaccines recommended for children) explains the higher prevalence in recent years. However, evidence from several studies examining trends in vaccine use and changes in autism frequency does not support such an association. Furthermore, a scientific review by the Institute of Medicine (IOM) concluded that "the evidence favors rejection of a causal relationship between thimerosal-containing vaccines and autism." CDC supports the IOM conclusion. 
CDC recognizes that autism is an urgent health concern and supports comprehensive research as our best hope for understanding the causes of autism and other developmental disorders. Through collaborations with partners in government, research centers, and the public, CDC is focusing on three areas-- 
   1. Understanding the frequency and trends of autism spectrum disorders.    2. Advancing research in the search for causes and effective treatments.    3. Improving early detection and diagnosis so affected children are treated as soon as possible. 
Related Links 
CDC's Autism Information Center General information about autism and CDC's autism research: 
    * Definition of autism     * Causes of autism     * Sources for further information 
Other Organizations 
    * Studies on Mercury and Vaccines (exit site)     * Thimerosal Content in some U.S. Licensed Vaccines (exit site)     * Agency for Toxic Substances and Disease Registry ToxFAQ on Mercury (April 1999) (exit site)     * Food and Drug Administration/Center for Biologics Evaluation and Research - Thimerosal (exit site)     * Recommendations Regarding the Use of Vaccines That Contain Thimerosal as a Preservative       (November 5, 1999) MMWR Vol. 48(43); 996-998 
Page last modified October 23, 2007 
This page was last reviewed on 19 December 2006. 

