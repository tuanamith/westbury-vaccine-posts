


Job Title:     (Senior) Biometrician Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-02-08 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   (Senior) Biometrician &#150; STA000440 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. Incumbent works with clinicians, biostatisticians, programmers and data management staff on clinical development plans, the design and conduct of clinical studies and in the evaluation, interpretation and preparation of study results. For assigned clinical development project(s), provides statistically sound experimental design and data analysis input to meet project objectives and regulatory agencys requirements. Author statistical analysis section in the protocol and the statistical analysis results in the clinical study report. Supplies statistical input for regulatory submissions and in response to regulatory agencys questions.Conduct statistical research and participate in external professional activities. 
Qualifications 
Doctorate or Masters degree in Statistics, Biostatistics or a related field. Sound knowledge of theoretical and applied statistics including experimental design and linear models, familiarity with statistical software, effective communication skills, and a strong interest in applications are required. Some exposure to basic sciences and/or medical subjects is desirable, along with a high interest in medical research. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site at www.merck.com/careers to create a profile and submit your resume for requisition # STA000440. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck _____________________________ Search Firm Representatives Please Read Carefully:       Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails.  All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means.  _____________________________________________________ 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  02/07/2008, 01:16 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  03/08/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-374956 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



