


BIOFUELS - THE GREAT SWINDLE 
Forwarded message  
October 20, 2007 
Biofuels: The Great Swindle.       Environment: 
Biofuels - Great Green Hope or Swindle  
Stephen Leahy  
Brooklin, Canada, Oct 20 (IPS) - A raft of new studies reveal European and American multibillion dollar support for biofuels is unsustainable, environmentally destructive and much more about subsidising agri-business corporations than combating global warming.  
Not only do most forms of biofuel production do little to reduce greenhouse gas emissions, growing biofuel crops uses up precious water resources, increasing the size and extent of dead zones in the oceans, boosting use of toxic pesticides and deforestation in tropical countries, such studies say.  
And biofuel, powered by billions of dollars in government subsidies, will drive food prices 20-40 percent higher between now and 2020, predicts the Washington-based International Food Policy Research Institute.  
"Fuel made from food is a dumb idea to put it succinctly," says Ronald Steenblik, research director at the International Institute for Sustainable Developments Global Subsidies Initiative (GSI) in Geneva, Switzerland.  
Biofuel production in the U.S. and Europe is just another way of subsidising big agri-business corporations, Steenblik told IPS.  
"It's (biofuel) also a distraction from dealing with the real problem of reducing greenhouse gas emissions," he asserts.  
Making fuel out of corn, soy, oilseeds and sugar crops is also incredibly expensive, Steenblik and his co-authors document in two new reports on the U.S. and the European Union that are part of a series titled Biofuels at What Cost? Government Support for Ethanol and Biodiesel.  
Their analysis shows that by 2006 government support for biofuels had reached 11 billion dollars a year for Organisation of Economic Development and Co-operation (OECD) countries. More than 90 percent of those subsidies came from the European Union and the U.S.  
These subsidies will likely climb to 13-15 billion dollars this year the report estimates.  
"More subsidies are coming as the biofuel industry expands," says Steenblik.  
In fact, countries will have to spend more than 100 billion dollars a year to get biofuel production levels high enough to supply 25 or 30 percent of transport fuel demands.  
And those levels of annual subsidies will have to continue because the industry is dependent on them, he says.  
It might be worth it if biofuels resulted in significant reductions in greenhouse gas emissions (GHGs) but Steenblik calculates the amount of subsidies that goes into making enough ethanol to reduce emissions equivalent of a tonne of carbon dioxide (CO2) is between 2,100 to 4,400 euros (2,980 to 6,240 dollars) depending on the support programmes.  
However, the European carbon trading markets sells a similar saved or sequestered tonne of CO2 for less than 25 euros (35 dollars) through various projects like planting trees or installing solar panels.  
Various analysis that take the full environmental costs of growing, shipping and processing maize into ethanol show there is only a small reduction in GHG emissions over burning fossil fuels. Newer research shows some biofuels could even be far worse.  
Rapeseed biodiesel and maize ethanol may produce up to 70 percent and 50 percent more GHG emissions respectively than fossil fuels, according to work published in September by Nobel prize-winning chemist Paul Crutzen and University of Edinburgh colleague Keith Smith.  
They found that growing biofuel crops releases around twice the amount of the potent greenhouse gas nitrous oxide (N2O) than previously thought. The N2O results from using nitrogen fertilisers.  
About 80 percent of Europe's biodiesel comes from rapeseed and in America the vast majority is maize ethanol.  
"What we are saying is that growing biofuels is probably of no benefit and in fact is actually making the climate issue worse," Smith has said in media reports.  
Last January, U.S. President George W. Bush set a biofuel target of 35 billion gallons per year by 2017, more than five times the current production of less than 7 billion gallons.  
However that target would leave some U.S. waterways polluted and some regions with severe water shortages the National Research Council (NRC) said in a report released this month. The NRC is the research arm of the US National Academy of Sciences.  
The additional fertilisers used to grow all that maize will contribute to the overgrowth of aquatic plant life that produces "dead zones" like those in the Gulf of Mexico, Chesapeake Bay and elsewhere, the report said.  
Similar water warnings were issued by the International Water Management Institute (IWMI) in Sri Lanka regarding India and China's growing interest in biofuels. IWMI, part of the Consultative Group on International Agricultural Research, recommends in its October report that the two countries invest in cellulosic biofuel the so-called second generation biofuel technology that is still a number of years from commercialisation.  
"Subsidies for ethanol are more about securing votes from the powerful agricultural lobby than bringing environmental benefits," says Walter Hook, executive director of the Institute for Transportation and Development Policy, an environmental NGO based in New York City.  
Simple and cheap programmes like a congestion charge -- an extra fee for driving in city centres -- and the widely successful Paris, France free bike programme reduce air pollution and GNG emissions immediately at very low cost, Hook said in an interview.  
Launched in July, Paris put thousands of low-cost rental bikes -- the first 30 minutes of use are free -- at hundreds of high-tech bicycle stations. A million trips were taken in just 17 days. "Absolutely amazing, every city should be thinking of doing this," he said.  
In Paris, an advertising company provides the bikes for free, runs the system, gives all the revenue to the city and pays 4.3 million dollars a year in exchange for exclusive control of the citys advertising billboards.  
Mobility -- getting from A to B -- with the minimum of GHG emissions is the core problem we should be addressing not finding greener fuels, says Steenblik.  
Indeed, Canadian transportation analyst Todd Alexander Litman has demonstrated greener fuels and improvements in fuel efficiency result in people driving more because they can afford to. And that just makes "traffic congestion, accidents, road and parking facility costs, and the lack of options for non-drivers worse," said Litman, director of the Victoria Transport Policy Institute, in British Columbia, Canada.  
In his Win-Win Transportation Solutions report released in September, Litman documents a variety of cost-effective transportation strategies that could reduce motor vehicle travel by 30-50 percent, produce substantial reductions in GHGs and bring a range of economic developments. His simple solutions include making urban areas more walkable, creating bike lanes, improving the quality of mass transit and a dozen more ideas. None involved producing more biofuels.  
"Subsidising biofuels is just about the dumbest way to go," Litman told IPS.  
Jean Ziegler, the United Nations Special Rapporteur on the Right to Food, uses stronger language: increasing biofuel production is a "total disaster" for starving people, he told a Swiss media outlet last week.  
"There are serious risks of creating a battle between food and fuel that will leave the poor and hungry in developing countries at the mercy of rapidly rising prices for food, land and water," Ziegler warned the UN General Assembly last August.  
On Oct. 25, he will ask the UN General Assembly, to adopt a five-year global ban on the conversion of land for the production of biofuels.  
Despite the growing evidence that biofuels are a huge mistake, governments will continue to pour billions more tax dollars into boosting production levels.  
"Governments rarely phase out subsidies," laments Steenblik. "We're hoping that countries will come to their senses in the next few years."   (END/2007) 
 <URL>   
 - - - 
If I came to you and said, "I'm going to perform a little sexual assault on you -- a small rape -- because, one day you could meet a rapist and you could be raped. But, it won't be as bad the second time as the first time." This is exactly the same thing as giving someone a vaccine, or a little bit of disease. It's nonsense!    - An Interview With Guylaine Lanctot, M.D. By Kenneth &       Dee Burke      <URL>   
End of forwarded message  
Jai Maharaj   <URL>    <URL>    <URL>   Om Shanti  
Hindu Holocaust Museum  <URL>  
Hindu life, principles, spirituality and philosophy  <URL>   <URL>  
The truth about Islam and Muslims  <URL>  

