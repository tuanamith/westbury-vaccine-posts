


Pat's Note: 
"Apparently, a successful vaccine has not been able to stop the problem, he  said." 
Many a true word spoken in total confusion. The vaccines give peace of mind  "we are doing something." 
They do not actually have to work to be sucessful. It is a point of view. 
PMWS is, of course, circovirus 
 <URL>  
Virus cancels pig weigh-in at Tehama District Fair By KAREN McINTYRE-DN Staff Writer Article Last Updated: 01/30/2008 08:20:42 AM PST 

RED BLUFF - The Tehama District Fair, along with all other fairs in the  state, is canceling its swine pre-tagging and pre-fair weigh-ins this year  because of an increase of disease in the animals. Porcine circovirus, a viral disease in pigs, has become a major problem,  according to information released by the California Department of Food and  Agriculture. 
The increase in cases is the reason CDFA animal health officials and the  Division of Fairs and Expositions is requiring all fairs cancel swine  weigh-ins. However, regular breeding and market shows and livestock auctions  may continue, CDFA Fairs Management Consultant Tomme Jo Dale said in a news  release. 
Pigs that have mingled with other swine at fairs should not return home  because the risk of disease transmission is greatly increased. Pigs return  home after weigh-ins, but not after fairs, which is why the fair can go on,  but the weigh-ins cannot. 
Canceling weigh-ins affects the Tehama District Fair because tagging and  weighing the hogs ahead of time helps to establish ownership, Tehama  District Fairground CEO Mark Eidman said. 
The pigs are required to be owned for at least 60 days before the fair. In  the past, some people did not own the pigs long enough, and there is no way  to tell without weigh-ins. But the problem has subsided in recent years, and  Eidman said he encourages honesty. 
Weigh-ins were canceled for last year's fair for the same reason, Eidman  said. Apparently, a successful vaccine has not been able to stop the  problem, he said. 
Conditions that trigger the disease are not understood, the CDFA said, but  the disease comes from porcine circovirus type 2. Most swine are infected  with PCV2, but only some are affected with clinical signs of the disease. 
Symptoms include poor growth and weight loss, resulting in severe thinning  and weakness in pigs aged 5 to 14 weeks. 
Affected swine can also show enlarged lymph nodes, skin rashes, difficulty  breathing, jaundice, fever, stomach ulcers, diarrhea or sudden death. 
The disease can be transferred through bodily secretions such as blood,  urine, feces or mucus. Exposing pigs to other pigs, boots, clothes or  equipment that has been around other pigs increases the risk. 
Most pigs will give a positive blood test, but not all will show signs.  Symptomatic pigs should be removed from other hogs. Other livestock species  and humans have not been infected. 
CDFA information said the three commercial vaccines in the country are being  manufactured on a large scale, but supplies are limited. Piglets should be  given the vaccination at 3 and 5 weeks old. A vaccine for sows is not  approved in the U.S., and the vaccine is not as effective on older pigs.  Antibiotics will no directly help with the disease, but may eliminate other  bacterial challenges. 
Breeders can still buy pigs at sales this spring and attend county fairs,  but the CDFA recommends that livestock owners be aware of the disease and  talk with breeders about the health of their herd before buying swine. 
"This isn't that critical," Eidman said. "We've had worse issues." 

--  Regards Pat Gardiner www.go-self-sufficient.com 



