


'Safe Ebola' created for research 
 <URL>  
Scientists have made the lethal virus Ebola harmless in the lab, potentially aiding research into a vaccine or cure. 
Taking a single gene from the virus stops it replicating, US scientists wrote in the Proceedings of the National Academy of Sciences journal. 
Ebola, currently handled in highly secure labs, kills up to 80% of those it infects. 
However, one expert said the new method may not yet be a fail-safe way of dealing with the virus. 
The need for a "biosecurity level 4" (BSL4) laboratory for any work involving Ebola means that very few research institutions are capable of doing this. 
Researchers wear biosafety suits with their own air supply, and the air pressure in the room is less than the pressure outside, so any leak would mean air flowing inwards rather than outwards. 
This makes anything more than small-scale study of the virus very difficult to arrange. 
If Ebola could be kept in a viable form, yet with the risk of infection removed, then conventional labs might be able to study it. 
The researchers, from the University of Wisconsin at Madison, say that they have found a "great system" to do this. 
Key gene 
They said that a single one of Ebola's eight genes, called VP30, is the key, as without it, the virus cannot replicate within host cells by itself.  
However, the scientists still want the virus to replicate in order to study it, so they developed monkey kidney cells which contained the protein needed. 
Because the cell was providing the protein, and not the virus itself, it could only replicate within those cells, and even if transferred into a human, would be harmless. 
In an effort to prove this, they used the monkey cells for dozens of "cycles" of infection and replication, without once encountering a form of the virus capable of making another creature ill. 
"We wanted to make biologically contained Ebola virus," said Yoshihiro Kawaoka. 
"The altered virus does not grow in any normal cells. This system can be used for drug screening and for vaccine production." 
Monkey tests 
However, not everyone in Ebola research is convinced. 
Professor Susan Fisher-Hoch, at the University of Texas Health Science Center at Houston, was among those at the forefront of Ebola study in the early 1980s, at the UK's BSL4 lab at Porton Down. 
She said that she would need to see more proof that the modified virus could do no harm. 
"I wouldn't be comfortable using it until it had been thoroughly tested and did not cause disease in live monkeys, at a high dose. 
"There is no way you can prove that it is non-toxic unless this has been done."  
--  Bob. 


