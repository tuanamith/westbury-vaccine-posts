


Job Title:     Senior Packaging Supervisor Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-01-19 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Senior Packaging Supervisor &#150; PRO006650 
Job Description  
Submit 
This position is responsible for independently coordinating all activities associated with timely packaging, labeling, assembly and shipment of clinical materials for designated studies. This individual is responsible for execution of clinical supplies activities (i.e. ordering, processing, release, accountability) at any GCSO site. This person may manage technical personnel, graduate level personnel/both. Ensures all clinical supplies are processed according to cGMPs and appropriate Merck safety requirements to meet exacting standards defined by the FDA and EU or other international regulatory agencies. Prepares and approves all study May be required to print and approved labels. This will involve determining language requirements and using appropriate source text such as the Territory Manual to ensure local regulations are met with regard to label requirements. 
Qualifications 
Education: Qualifications for this position require a B.S. in Pharmaceutics or health related science. Required: At least (2) years experience in the Pharmaceutical industry. This position requires very strong communication skills, both written and verbal as well as strong interpersonal skills. This role requires a comprehensive knowledge of pharmaceutical development processes. Desired: Preference for experience in clinical packaging.  Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation.  To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #PRO006650. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm Representatives Please Read Carefully:  Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular   Travel  No  
Additional Information   Posting Date  01/17/2008, 09:15 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  01/22/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-362077 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



