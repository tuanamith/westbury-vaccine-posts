


 HPV causing more oral cancer in men 
By MIKE STOBBE, AP Medical Writer Fri Feb 1, 7:02 PM ET 
ATLANTA - The sexually transmitted virus that causes cervical cancer in women is poised to become one of the leading causes of oral cancer in men, according to a new study. 

The HPV virus now causes as many cancers of the upper throat as tobacco and alcohol, probably due both to an increase in oral sex and the decline in smoking, researchers say. 
The only available vaccine against HPV, made by Merck & Co. Inc., is currently given only to girls and young women. But Merck plans this year to ask government permission to offer the shot to boys. 
Experts say a primary reason for male vaccinations would be to prevent men from spreading the virus and help reduce the nearly 12,000 cases of cervical cancer diagnosed in U.S. women each year. But the new study should add to the argument that there may be a direct benefit for men, too. 
"We need to start having a discussion about those cancers other than cervical cancer that may be affected in a positive way by the vaccine," said study co-author Dr. Maura Gillison of Johns Hopkins University. 
The study was published Friday in the Journal of Clinical Oncology. 
Human papillomavirus, or HPV, is the leading cause of cervical cancer in women. It also can cause genital warts, penile and anal cancer  risks for males that generally don't get the same attention as cervical cancer. 
Previous research by Gillison and others established HPV as a primary cause of the estimated 5,600 cancers that occur each year in the tonsils, lower tongue and upper throat. It's also been known that the virus' role in such cancers has been rising. 
The new study looked at more than 30 years of National Cancer Institute data on oral cancers. Researchers categorized about 46,000 cases, using a formula to divide them into those caused by HPV and those not connected to the virus. 
They concluded the incidence rates for HPV-related oral cancers rose steadily in men from 1973 to 2004, becoming about as common as those from tobacco and alcohol. 
The good news is that survival rates for the cancer are also increasing. That's because tumors caused by HPV respond better to chemotherapy and radiation, Gillison said. 
"If current trends continue, within the next 10 years there may be more oral cancers in the United States caused by HPV than tobacco or alcohol," Gillison said. 
Studies suggest oral sex is associated with HPV-related oral cancers, but a cause-effect relationship has not been proved. Other researchers have suggested that even unwashed hands can spread it to the mouth as well. 
Gillison pointed toward sex as an explanation for the increase in male upper throat cancers. However, HPV-related upper throat cancers declined significantly in women from 1973 to 2004. 
Merck's vaccine, approved for girls in 2006, is a three-dose series priced at about $360. It is designed to protect against four types of HPV, including one associated with oral cancer. 
Merck has been testing the vaccine in an international study, but it is focused on anal and penile cancer and genital warts, not oral cancers, said Kelley Dougherty, a Merck spokeswoman. 
"We are continuing to consider additional areas of study that focus on both female and male HPV diseases and cancers," Dougherty said. 
Merck officials praised Gillison's research, saying it will elevate the importance of HPV-related oral cancers. 
Government officials and the American Cancer Society say they don't know yet whether Merck's vaccine will be successful at preventing disease in men. No data from the company's study are available yet. 
Indeed, it's not clear yet that the vaccine even prevents the HPV infection in males, let alone cancer or any other illness, said Debbie Saslow of the American Cancer Society. 
Merck plans to seek U.S. Food and Drug Administration approval for the vaccine in men later this year, meaning a government decision would be likely in 2009.  

