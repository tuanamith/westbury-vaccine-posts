



From The Catoosa County News, 1/29/08:  <URL>  
Jeannie Babb Taylor: Setting the record straight: evaluating election rumors 

With a flimsy platform and no strong candidate, Republicans are hoping to win the November election on whisper campaigns and character assassination.  
Lets check out some of their claims. 

-----No, Hillary Clinton did not defend the Black Panthers who killed and tortured Alex Rackley.  
Hillary Clinton was a student at that time, not an attorney or a politician.  
She attended the trial as a volunteer observer for the ACLU, but had no impact on the outcome.  
Like many students, she was concerned about whether the black defendants were receiving a fair trial, and she participated in protests calling for a change of venue. 

-----No, people who oppose the Clintons do not meet an untimely demise.  
The Clinton Body Count is so preposterous that no reasonable person could entertain the idea.  
For a body-by-body debunking, see www.snopes.com.  
The shorter version is: If Hillary Clinton had a 50-person hit list, wouldnt the Republicans be all over that?  
She would certainly be sitting in jail for connections to even one murder. 

-----No, Secret Service agents did not claim Hillary Clinton was rude and arrogant, mistreating her agents and even charging them rent.  
As early as 1993 Time Magazine reported a known political trick in which spurious Clinton stories were leaked to the press.  
Often these stories were attributed to anonymous Secret Service agents as a way to lend credibility to the false claim.  
As for rent, the Clintons are entitled to receive $1,100 per month for housing Secret Service agents in their Chappaqua, N.Y. home  but they turned down the money. 

-----No, Obama does not refuse to pledge the flag and yes, he has flags on his Web site.  
Obama has been videotaped pledging the flag.  
His Web site is red, white and blue (mostly blue).  
The background centers on an eagle holding a shield and flag.  
His logo, shown multiple times on every page, is an interpretation of the American flag and the theme of hope (the sun rising over a field) all framed as a big O. 

-----No, Obama does not attend a covertly Muslim church that excludes whites.  
Obama is a member of Trinity United Church of Christ (TUCC).  
The membership of TUCC is predominantly black, and the church places great emphasis on honoring African heritage and promoting the idea that black is beautiful.  
However, all people are welcome at the church, which adheres to the theology of the United Church of Christ. 

-----No, Obama did not take the oath of office by swearing on the Koran.  
That would be a strange thing for a Christian to do.  
Obama was sworn in on the Bible. 

-----No, John Edwards did not cause the 2004 flu vaccine shortage.  
The urban legend states that John Edwards sued a pharmaceutical company on behalf of a man who contracted the flu after receiving the vaccine.  
Supposedly the threat of further litigation ensures that no pharmaceutical company in the United States will dare to make the flu vaccine.  
The legend claims that the 2004 flu vaccine shortage resulted from contamination of a flu vaccine facility in the UK.  
This one is false all the way around.  
John Edwards never litigated a flu case.  
Anyway, the flu vaccine is manufactured in the United States.  
It was a U.S. facility that was shut down due to contamination, resulting in the shortage.  
The real reason few pharmaceutical companies produce flu vaccine is because the profit margin for flu vaccine is very slim. 

What about the Republican candidates?  
Is there a whisper campaign against them?  
Every e-mail I have received has been against a Democrat.  
Even searching for GOP candidate names along with urban legend, I came up with very few stories, all of which are substantiated by reputable media outlets. 

-----Yes, Senator McCain supported amnesty for illegal immigrants.  
In 2006 and 2007, McCain joined with Ted Kennedy in supporting Senate bills that would give amnesty to millions of illegal immigrants.  
He also denounced and voted against an amendment designed to stop illegal immigrants from receiving social security benefits through identity fraud.  
McCain co-sponsored the Dream Act, which provided in-state tuition rates for illegal immigrants.  
Later he said he would have voted against his own legislation  but in fact he was absent when the vote was taken. 

-----Yes, McCain is being swift-boated.  
There really is a group called Vietnam Veterans against John McCain.  
They claim that Senator McCain committed treason and does not deserve his medals because he gave the enemy information while he was being tortured as a POW.  
According to McCains own account, he did give the enemy information  some true and some false.  
For example, when asked to name the members of his squadron, he listed the names of the Green Bay Packers offensive line.  
McCain is a war hero as far as I am concerned, but it is true that this group exists and that they insist otherwise. 

-----Yes, Mitt Romney transported his dog in a cage strapped to the top of the car during a 12-hour journey to visit his parents.  
The 1983 misadventure was reported in the Boston Globe last June.  
Romney clarified that he attempted to shield the dog with some sort of makeshift windshield.  
The scared pooch developed diarrhea, so Romney stopped at a gas station and hosed down the dog, the carrier, and the back of the car.  
Romneys campaign-trail response to pet-loving critics:  
Theyre not happy that my dog loves fresh air. 

-----Speaking of dogs, Snopes confirms that Mike Huckabees son was fired from his job as a Boy Scout Camp counselor after he killed a dog by hanging.  
John Bailey, then director of Arkansas state police, claims Huckabee refused to allow police to investigate whether the boy violated animal cruelty laws.  
Huckabee says that Bailey is just a disgruntled employee.  
Huckabee says the dog was mangy, emaciated, and threatening, and that his son acted out of compassion. 

-----Yes, Huckabee had a prominent role in the release of a serial rapist in Arkansas.  
Worse, the decision to release Wayne Dumond 25 years early appears to be politically motivated. Dumond was convicted and incarcerated while Bill Clinton was governor of Arkansas.  
One of the victims, a 17-year-old high school cheerleader named Ashley Stevens, was distantly related to Clinton.  
Republicans seized on the connection to claim that the man had been wrongfully convicted. 
Soon after election, Gov. Huckabee began to agitate for Wayne Dumonds release.  
In his book, From Hope to Higher Ground, Huckabee states that he worried Dumond might be innocent.  
He was callous enough to say this to Ashley Stevens when she begged him to keep Dumond behind bars.  
According to the Huffington Post, Huckabees office kept the visit secret, as well as letters from numerous victims warning that Dumond would strike again.  
They were right.  
Dumond then raped and then suffocated a 39-year-old woman.  
He was arrested again, the day after he allegedly raped and murdered a pregnant woman.  
Huckabees response amounts to Who knew?  
Other times he has blamed Clinton for Dumonds release, pretending the commutation happened before his term. 
In an election of this import, voters must make the effort to find out the truth.  
Dont go into the voting precinct next Tuesday with a head full of lies.  
Cut through the urban legends  and even the campaign rhetoric  to consider a candidates true stance on the issues.  
Past voting records are the best clue. 
We can believe that Democrats will institute nationwide healthcare coverage  and that Republicans consider it unnecessary.  
We can believe that Huckabee will be soft on crime and add to his 1,000+ pardons.  
We cannot believe McCain on immigration or Romney on abortion, because their positions are shifting and do not match their voting patterns.  
We can believe the Republicans when they say they will extend the war in the Middle East for 100 years or more.  
We can believe Democrats when they say they will end the war and bring troops home. 
_______________________________________________________ 
Harry 

