


Job Title:     Research Biologist - Biologics Research Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-02-02 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Research Biologist - Biologics Research &#150; BIO001686 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand &#8212; one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;will have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We are seeking a highly motivated Research Biologist to work in a team identifying and developing novel therapeutic Biologics. The duties will include cDNA construction and transfection, Recombinant protein cloning and expression, Site-directed mutagenesis, PCR, ELISA, Immunopurification, Fluorescence Immunocytochemistry, Protein/RNA/DNA extraction. Experience with cell based assays for surface receptor binding, receptor signaling, and cell function assays such as ADCC is desired. Some animal model work may be required. Additional duties will include analysis of primary data, laboratory notebook maintenance and presentation to management. The candidate should bring a desire to work within a collaborative fast-paced and intellectually challenging environment. 
Qualifications The ideal candidate will have experience in the following: Molecular biology skills - subcloning cDNA, Stable cell line generation, Site directed mutagenesis, Protein/RNA/DNA extraction and purification.  Immunodetection skills - ELISA, Multiplex detection methods, alphascreen, Immunoprecipitation, Immunopurification, FACS etc. Tissue culture - cell signaling/functional assays  Degree required: MS or BS/BA + 2-4 years research experience, preferably in the pharmaceutical industry. Prior pharmaceutical experience is a plus although highly motivated candidates with academic experience looking for a demanding and rewarding position will be considered Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition #BIO001686. Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck Search Firm Representatives Please Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-PA-West Point   Employee Status  Regular  
Additional Information   Posting Date  01/31/2008, 05:00 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  03/01/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-375096 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



