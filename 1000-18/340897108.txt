


Job Title:     Pharmaceutical Sales Representative- Staten Island, NY Job Location:  NY: Staten Island Pay Rate:      Open Job Length:    full time Start Date:    2008-02-13 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Pharmaceutical Sales Representative- Staten Island, NY &#150; PRI004330 
Job Description  
Submit 
Description Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics and integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, youll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We are seeking Pharmaceutical Sales Representatives for potential openings for our upcoming training classes to represent Merck and Merck products in a complex, dynamic business environment for the following territories: Staten Island, NY. The primary responsibility of the Pharmaceutical Sales Representative is to effectively promote Merck products through a Consultative based selling approach. Representatives provide accurate information to physicians and other health care personnel so Merck Products will be prescribed when indicated. Management of the territory business is conducted in concert with other territory team members, specialty representatives, hospital representatives and other business resources. Progressive promotion to Senior or Executive Professional Representative may occur through demonstration of sustained high performance and leadership. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Mercks finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Mercks retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # PRI004330. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someones hope. Join us.  Merck - Where Patients Come First Search Firm Representatives Please read carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Bachelors Degree Required Major Preferred: Sciences, Business, Healthcare fields 
Travel- 25%  
Profile   Locations  US-NY-Staten Island   Job Type  Standard   Employee Status  Regular  
Additional Information   Posting Date  02/11/2008, 11:26 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/25/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-378196 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



