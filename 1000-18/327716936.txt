


Title:  A Winter's Tale 17/23 
Author:  Anjou ( <EMAILADDRESS> ) 
Posting Date: December 2007/January 2008 
Rating:  R for language and sexuality; M for Mature readers  
Classification:  Mulder/Scully, UST/MSR, AU 
Archive: No archival until the story is completed, please. I'll be  submitting to Ephemeral and Gossamer myself. 
Spoilers: Through Two Fathers/One Son (S6), then AU. In other words,  no Arcadia and beyond. Mytharc-y. 
Disclaimer: All X-Files personnel belong to 1013 and Fox. All other  elements are mine.  
Author's Note: FYI, tomorrow's post is likely to be after 9:00 pm  EST. Sorry, but it can't be helped! 
As always, thanks to my sister and editrix, Suzanne, for her  support. 
This is but one.   
~*~ 
  Mulder made a noise that she'd only heard once, when a suspect's  punch had caught him in the solar plexus. She stepped away from Kurt  toward Mulder who was staring at the clone in open-mouthed shock.  Through the fog of her own surprise, she registered the anomaly of  his appearance. Finally, here was a situation that Mulder, the man  who regularly believed six impossible things before breakfast, had  never considered. As she watched, still stunned herself, Mulder's  expression transformed from disbelief to seething rage. In all the  years that she had known him, she had never seen look so angry. 
  "When?" Mulder gritted out through bared teeth. "How was my sperm  acquired?" 
  Scully could see that Kurt was literally afraid of Mulder,  remembering his earlier words about Dr. Calderon and the opinion of  scientists involved in the project. "Mulder," she said to him in a  low voice, stepping in front of him and putting her hand on his  forearm.  
  His hands were planted on his hips, and he shifted sideways at her  touch, almost as if he was going to shake her hand off, but then  thought better of it. He was steadfastly staring at the clone. 
  "Mulder," she said to him in a sharper tone, holding his arm  firmly. "Ellen's Airbase, for one." His head swiveled to hers, but  he stared at her blindly. "Think about it, Mulder," she said,  "Alaska, Tunguska  there's been plenty of opportunity." 
  "Scully," he said suddenly, as if seeing her for the first time.  "Hannah " his voice was choked, and he couldn't continue. 
  "I know, Mulder," she said softly, her own eyes filling with  tears.  
  "So Scanlon made her to be a hostage?" Mulder demanded of Kurt.  
  Scully bit her lip, trying to find the words to remind him that  Hannah had not always been alone in that hospital room in Albany,  but Kurt spoke first.  
  "As I indicated to Agent Scully earlier, I believe the motivation  would be replication of your immunological system."  
  "Scully?" Mulder's look was accusatory.  
  "A few minutes ago, Kurt told me that you've been infected with  the black oil virus multiple times." 
  Mulder was shaking his head in denial before the words had  finished leaving her mouth. "That's not true," he said. 
  "It's extremely well-documented," the clone said. "From your  earliest tests until the most recent ones." 
  "What are you talking about?" Mulder demanded. "What early tests?" 
  "You were first infected in 1967," the clone answered. "You were  hospitalized and given a small dose of the black oil, and then given  what they thought was a vaccine at the time." 
  "That did not happen," Mulder said in a certain tone. "I'd  remember that." 
  "Mulder," Scully soothed. "Do you remember being in the hospital  when you were six?" 
  "I was five," he snapped. "It was in June. We drove to Boston, and  I had my tonsils out."  
  "They may have also taken your tonsils out," Kurt said, "but you  were most certainly infected with the black oil. You were the only  child to survive the treatment. They thought they were on the right  track with the vaccine for years because you survived. Later they  realized that you were simply resistant." 
  Mulder gaped at him. "What are you talking about?" 
  "When no one else survived the experimentation, your pediatrician  was replaced with a project-approved doctor in 1971." Scully felt  Mulder start. "You were injected with small doses of the vaccine,  which also proved to have no effect on you. I told Agent Scully  earlier that virtually no one who has received a vaccine as a  preventative has survived. You are the exception."  
  "This has got to be a lie," Mulder insisted. "My parents "  
  She could see the thoughts cascading through his mind, but couldn't even  guess at where he was going with them. From her perspective, his  parents had never protected either of their children from harm. She  couldn't believe that he was defending them more than reflexively. 
  "Your parents consented to the first test without really  understanding what it entailed," the clone answered. "None of the  parents did. It was a test of their loyalty to the project." 
  "But if I was resistant," Mulder said, his voice rising, "then why  take Samantha? Why not take me and test me?" 
  "They did test you, Agent Mulder," the clone said. "You must have  noticed that your pediatrician drew blood when you went for your  frequent check-ups." 
  "I was anemic," Mulder whispered, "that was why I had to go so  often. I had to take those  " Mulder stopped and stared at the  clone. "Those weren't iron pills that we kept in the freezer, were  they?" 
  "No," the clone said levelly. "They were not. And no matter how  many times or ways they tried to infect you, you shed the virus.  They took your blood to figure out how and why. It was your blood  that led to the vaccine that you administered to Agent Scully in the  Antarctic." 
  Mulder was shaking his head 'no'. "This doesn't make any sense!"  he yelled. "If this is true, then why take Samantha? Why take any of  them? Why not just take me?" 
  "That was proposed," the clone answered, "but they didn't make you  resistant." 
  "I don't understand," Mulder said, but Scully believed that she  was starting to. 
  "Mulder," Scully said, "there is always a minority that is immune  to a virus."  
  He stared at her, clearly not comprehending.  
  "Why didn't the Black Death kill everyone who was exposed to it?"  she asked. "Some people, a few people, were infected and survived.  That's you. They infected you when you were a child, and somehow you  survived it. The problem is that they don't know how your body  fought off the original infection; they thought you had responded to  their vaccine. They were trying to formulate a process for making a  resistant human, but your body fought off the infection and became  resistant without them having done anything. They didn't know how  you made the antibodies to the virus, just that you did." 
  Mulder was still shaking his head 'no', but he was listening to  her. He raised the arm that she wasn't holding onto and raked it  through his hair, then broke away from her hold and began to stalk  back and forth in the hallway. Occasionally, he would stop and stare  at Kurt, then turn and look at her with an inscrutable expression on  his face.  
  After a few minutes of this, she finally broke the silence.  "Mulder?" 
  "So, this is why they never killed me, isn't it?" he addressed the  question to Kurt. "All these years, I could never understand why  they didn't just shoot me in the head and dump my body someplace  where it would never be found. It's not like they haven't made  people disappear." He was as still now as he'd been frantic before,  but the air around him was coiled with tension. "That's it, right?  If all of their plans failed, and colonization started, I'd still be  alive. I won't be changed, and I'd still be able to fight the  aliens." 
  "That is the argument that's been forwarded, yes," the clone said.  "My understanding is that your continued existence has been a  subject of much discussion for years, with many dissenting  opinions." 
  "Such as?" he demanded. 
  "Some project directors felt that you should be killed, that the  risk of letting you discover what was going on was too high. Some  felt that you should be captured for study." 
  Mulder nodded. "I assume that whoever took my sperm at Ellen's was  in that latter category."  
  "Yes," the clone answered. "I understand that you were released  only because your disappearance would have been difficult to cover  up. You had been very visible at the base." 
  "And where does Dr. Scanlon fit into this?"  
  "When he heard that another division of the project had gotten  some of your genetic material, he was determined to have it." 
  "And he obviously did!" Mulder shouted. "How many other children  do I have?" 
  Kurt shrank backwards at Mulder's raised voice, and Scully stepped  between them. In the long corridor that ran back toward the front  door, she could see a dizzying number of clones, all of them looking  fearfully back at her.  
  "Mulder!" she said sharply. "Kurt told me that he has never  participated in the creation of any clone other than himself." 
  "And you believed him?" he asked incredulously, his voice cracking  in strain. 
  "You're scaring him," she said firmly and then held up her hands  when it seemed he would keep speaking. "I know how you feel," she  reminded him. "I know."  
  Mulder sagged at her words.  
  "He didn't do this to us," she added softly. 
  "Scully " he said in a low murmur that only she could hear, his  eyes closing in pain. Her throat constricted at the agony in his  voice. She reached out to touch his face, but for the first time  that she could recall, he withdrew from her. "I can't," he said to  her, opening his eyes and pulling away. "I just can't." He turned  away from her, only to face the door to Hannah's room.  
  "Mulder," she said, in surprise. 
  "I can't," he said again, and then as she watched in disbelief, he  began to walk rapidly down the long hall that led outside, while the  clones who had been watching in the doorways ducked out of his way. 
  "Mulder!" She couldn't believe that he was doing this to her. He  broke into a run as she called his name, and then disappeared  through the doorway that led to the reception room and the winter's  twilight beyond.  
 "Mulder " she exhaled his name in a sigh of disappointment, but he  was already gone. She closed her eyes, her body heavy with  exhaustion and sorrow.  
  "Scully?" It wasn't Mulder calling her name. She could hear the  fear in Hannah's voice, and she felt an ache that this would be the  first time that Hannah had ever called for her. She felt the weight  of all that had happened bearing down on her, but Hannah called for  her again.  
  She squared her shoulders, opened her eyes, and walked into their  daughter's room. 
~*~ 

[Non-text portions of this message have been removed] 


AXF is your list for ALL X-Files Fanfic... all genres, all characters, all ratings. 
 Automatic newsgroup posting too!  <URL>   Yahoo! Groups Links 
<*> To visit your group on the web, go to:      <URL> / 
<*> Your email settings:     Individual Email | Traditional 
<*> To change settings online go to:      <URL>      (Yahoo! ID required) 
<*> To change settings via email:      <EMAILADDRESS>        <EMAILADDRESS>  
<*> To unsubscribe from this group, send an email to:      <EMAILADDRESS>  
<*> Your use of Yahoo! Groups is subject to:      <URL> /   


