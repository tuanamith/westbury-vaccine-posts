


Title:  A Winter's Tale 16/23 
Author:  Anjou ( <EMAILADDRESS> ) 
Posting Date: December 2007/January 2008 
Rating:  R for language and sexuality; M for Mature readers  
Classification:  Mulder/Scully, UST/MSR, AU 
Archive: No archival until the story is completed, please. I'll be  submitting to Ephemeral and Gossamer myself. 
Spoilers: Through Two Fathers/One Son (S6), then AU. In other words,  no Arcadia and beyond. Mytharc-y. 
Disclaimer: All X-Files personnel belong to 1013 and Fox. All other  elements are mine.  
Author's Note:       :: takes deep breath in ::  
Here we go.  
This is but one.   
~*~    
  Dana Scully rotated her neck gingerly as she sat in the chair next  to Hannah's bed. She'd fallen asleep with her head in Hannah's lap,  and only awoken hours later when Mulder tried to move her so that  Hannah could use the bathroom. Her neck had been bothering her ever  since, and the data that she was reading did nothing to ease her  discomfort, or improve her dark mood.  
  As promised, the records of the how of Emily's creation had been  provided for her. Mulder had pointed them out to her when she sat  up, but firmly insisted that she eat some lunch before she dove into  them. His concern for her led her to believe that he knew exactly  what was in the files, although much of the documentation was highly  technical. After her late lunch, Mulder had gone off to explore the  facility while Hannah dozed again in the high hospital bed. She  would have been more concerned about Hannah's continued sleepiness  if not for the fact that her temperature had dropped as well, an  apparent good response to the administration of the antibiotics. Her  oxygen saturation was still slightly lower at 96% than what Scully  would like, but the rails of Hannah's coughing had not seemed to  deepen, either. Scully hoped that the administration of the  cephalosporin had come at the appropriate time, and that maybe the  grip of Hannah's infection had been broken. 
  She stood and placed the files on the bedside tray table and then  stretched, working the kinks out from her head down to her toes.  She'd have given anything to take her shoes off and really stretch,  but she wasn't trusting her bare feet on the floors of any kind of  medical facility. She was moving her head from side to side when she  heard a calm voice ask, "Agent Scully?" 
  "Kurt," she said, opening her eyes in surprise. It was far too  soon for any kind of results from Hannah's tests. "Is everything all  right?" 
  "I believe so," he answered evenly. "I've had a chance to go  through more of the data that you brought, and " 
  "Let's step outside," she ushered the clone away from Hannah's  bedside. "I'd like Hannah to have some uninterrupted rest." 
  The clone glanced up at the wall clock. "I believe that she'll be  served dinner with the first group in thirty-five minutes." 
  "I'd like her to rest as much as possible, nonetheless," Scully  countered, and then gave into her curiousity. "Who cooks for you?" 
  "We do," Kurt answered, and once again Scully got the feeling he  was amused by her question. "We rotate responsibility for feeding  ourselves. Food supplies are ordered via the internet and delivered  to the building. It's relatively easy for us to avoid outside  contact."  
  She nodded, thinking how sad and isolated their lives seemed to  her. "Thank you for taking care of us." 
  "We couldn't do anything other than that," the clone said easily. 
  She tilted her head to one side, considering her next question.  "Because Agent Mulder and I are part of this project?"  
  The clone blinked. "I suppose one could view it from that  perspective," he said, "but we've always considered that we are on  the same side as you and Agent Mulder. That consideration obligates  us."  
  Scully wondered who exactly had instilled this sense of duty and  ethics within these clones. How much, really, was due to influence  and how much to the nature of the original Kurt Crawford who'd never  been allowed to become the man standing in front of her? If the  clone noticed anything odd about her continued silence, he did not  say so, just stood there quietly while she pondered.  
  "Scully?" Mulder's voice as he walked up the hallway was wary. 
  "Is everything all right?" she asked for the second time in a few  minutes. 
  "That was going to be my question," he asked her. She looked at  Mulder as he stood in the hallway watching her with a searching  gaze. He'd removed his suitcoat, and rolled up his sleeves, as if  this were any other workday, exploring in this warehouse of  oddities. He seemed tense and discomposed, and she wondered  fleetingly if he'd asked the clones about his sister.   
  "I'm fine," she answered, and hurried to continue on with her  thoughts when Mulder's hazel eyes had darkened at her use of the  phrase. "I'm trying to comprehend some things, Mulder." 
  He observed her for a few seconds, hands on his hips, examining  her face for any hint of a lie. "OK," he said. His shoulders relaxed  fractionally. "How's the pumpkin?" he asked with real concern. 
  "Dozey," Scully said, "but she's not coughing as much, and her  fever keeps going down, bit by bit." 
  Mulder sighed in relief. "I'll go sit with her, then." His  statement was more of a question directed at her, but she nodded. 
  "I'll be a while," she added. 
  She and Kurt moved away from the center of the hallway to the lab  doorway opposite the infirmary as Mulder turned into Hannah's room,  giving her one last searching gaze over his shoulder.  
  "What did you learn about Hannah?" Scully asked the clone. 
  "As I suspected, she is one of the 'H' series of experiments that  used your ova." 
  Scully nodded, but felt no surprise at his confirmation. Instead,  she asked the question to which she dreaded to hear the answer.   "Which would mean that there are series of experiments of which I've  been unaware," she said evenly. "A through D, F & G " she paused.  "Are there other series currently being conducted?" 
  "The A through D series were unsuccessful," Kurt said. "They were  part of an earlier attempt to clone individuals who might prove  disruptive to the Project's general mission. Those clone lines were  susceptible to the same kind of problems that befell the original  Dolly project, however." 
  Scully's eyebrows were at her hairline. "They were trying to clone  me?" she asked incredulously. Her hand unconsciously went to the  back of her neck, "To replace me," she said.  
  "Yes," Kurt said, "and the cloning process employed the same rapid  aging technology that had been utilized successfully in the  amplification of our original self, as well as others. However, the  older the original self " 
  "The more rapidly the clone degraded," Scully mused. "This is  incredible science that you're talking about." 
  "Not to those of us involved in this project," he answered.  "However, the limitations of that kind of parthenogenetic  reproduction were not yet evident to those running those  experiments."  
  "So, Emily?" she asked, beginning to think she knew the answer. 
  "When they realized that replacement was not going to be an  option, they reverted to using your genetic material in the same way  that other female abductees' ova were being used." Kurt paused. "Dr.  Scanlon was in charge of the projects from E onward." 
  Scully nodded. "And how far into the alphabet has he gone?" 
  Kurt looked puzzled by her question. "H," he said.  
  She didn't even attempt to hide her surprise. "Why did he stop at  H?" 
  Kurt really did seem confused by her question.  
  "Kurt?" she prompted. 
  "I'm very sorry, Agent Scully, but I assumed that you knew more of  this information. To the best of our knowledge, no planned project  using your ova has gone forward since you discovered the E line in  San Diego. In fact, initial work on planned experiments I through L  was discarded." 
  "Because I found out?" Scully could not imagine that the answer  was so simple. 
  "No," the clone said slowly, "because of Agent Mulder."  
  "What?" For a moment, she was filled with a burning rage. If there  was more that Fox Mulder had kept from her she would  but then she  remembered her own discovery about Samantha earlier in the day, and  the rage was replaced with fear. "Why?" 
  "When Agent Mulder went to Dr. Calderon to demand answers, he was  extremely forceful." 
  She was sure that her mouth was hanging open. "Forceful?" 
  "Extremely," the clone said, with no sense of irony. "He not only  physically assaulted Dr. Calderon, but he destroyed his office.  Later, he destroyed experiments associated with your ova at another  facility." 
  "Fox Mulder?" She said to the clone. Of course, she knew that he  had a temper, and a highly honed sense of protectiveness, but she  had never seen him use raw physical force as a weapon - he was far  more likely to wield his intellect than his fists. 
  "Yes," Kurt said implacably, "and when Dr. Calderon ended up dead,  many labs affiliated with the project refused to have anything to do  with your ova." 
  She was reduced to stuttering again. 
  "In fact, most labs destroyed your ova." 
  "Most," she said, hoarsely.  
  "Yes," he said, "and then, of course, his recent physical assault  on Dr. Scanlon at the now defunct Norfolk facility just cemented the  opinion that using your ova was far too dangerous. Project  researchers believe that if they're using your ova, Agent Mulder  will find out and come after them." 
  "I see," she said faintly. "He did tell me that he'd recently seen  Dr. Scanlon." 
  "Well, when he saw him, he beat him quite severely," the clone  said. "It took a number of project security guards and a taser to  get him off Dr. Scanlon." 
  "A number of security guards?" She repeated. She was still not  sure that they were talking about the same man. It was unfathomable.  Perhaps a shapeshifter had impersonated him - but that would not  explain his bruised hands from last week. She shook her head in  wonder. She knew that Mulder was capable of a certain level of  violence, that like all agents he had been trained to react and to  fight, but this  she had never imagined that Mulder would behave in  such a way. 
  "And a taser," the clone said. "The rumor is that all of your ova  have been destroyed." 
  "All of them?" she asked. "Are you sure?" 
  "We're trying to find out definitively, but the rumor is that they  were all destroyed, over Dr. Scanlon's strenuous objections." 
  She opened her dry mouth. "So Dr. Scanlon isn't afraid of Mulder?" 
  "I have no way of knowing that," Kurt said thoughtfully.  "Historically, Dr. Scanlon has argued that your responsiveness to  the vaccine when you were exposed to the black oil during your first  abduction made your genetic material valuable for immunological  experimentation. However, since the other scientists on the project  do not like to be beaten, it appears his argument has been  nullified." 
  "I was exposed to the black oil when I was abducted in 1994?" 
  "Actually, both times you were abducted," the clone said  succinctly. 
  She turned and walked around the hallway with her hand on her  aching head, thinking furiously. "I was not expected to survive,"  she posited, but the clone did not respond, until another thought  occurred to her. "The virus is why my DNA was branched," she said. 
  "Yes," the clone answered. "You're one of the few abductees who've  been infected with more than one variant of the virus and responded  to the treatments in both cases." 
  "Which variants?"  
  "Like Agent Mulder, you were infected with the Tunguska variant  and received the so-called Russian vaccine. The variant that you  received from the Africanized honeybee was a sub-variant that's been  altered for ease of delivery." 
  "And you have a vaccine for that variant?" she asked, dizzily. 
  "As I said earlier, not as a reliable preventative, no," the clone  answered. "When we have tried to vaccinate prophylactically, the  subject would invariably become infected and resistant to further  intervention. We have theories about why this is occurring, and  actually, would be very grateful if you'd give us your perspective." 
  "I " she held up her hand, "forgive me, but I'm very far behind  you on this learning curve. Are you telling me that Mulder and I are  both immune to the black oil virus?" 
  "Most likely, in your case," the clone answered. "Agent Mulder is  a different order of magnitude as far as immunity is concerned. He  alone has resisted every variant that he's been infected with. He  has even survived infection when no treatment was offered." 
  She couldn't believe what she was hearing. "I thought Mulder was  only infected in Russia." 
  "Agent Mulder was first infected in childhood." 
  She remembered a changed file folder label in the bowels of a coal  mine in Virginia. "I'd like to see those records, please." She took  a deep breath and turned to see the clone moving into the lab.  "Kurt, not at this moment. I'd like to go back to our previous  conversation." 
  "Of course," the clone said.  
  "Emily was created using some variant of somatic cell nuclear  transfer, correct?"  
 The clone nodded. "Yes. Although both ova were yours, the nucleus  of one ovum was altered with alien DNA." 
  She continued, "With the failure of the earlier cloning attempts,  why persist in using an almost parthenogenetic process? My cell line  was obviously older, and there were males abducted by the project,  both children and adult. Why not use a more conventional  reproductive methodology to start the process of fertilized cell  division?"  
   "The aliens reproduce asexually, which is what they were trying  to replicate." 
  "The aliens reproduce asexually," Scully murmured aloud. "Oh my  God. So, the original experimenters thought that they could alter  our genome to reproduce asexually?" 
  "Not exactly," the clone said serenely, "the hypothesis was that  creation of a successfully resistant human/alien hybrid would  probably require cloning to mimic the asexual reproductive process  of the aliens. Ultimately, they hoped this hybrid line of humans  would breed, creating a resistant population via sexual  reproduction." 
  She started to interrupt the clone, but he anticipated her  question. "Yes, most of the cloned lines were functionally sterile,  which was problematic, but negligible considering the high rate of  mortality, short lifespans and general instability of the  recombination. In addition, there were too many offshoot experiments  that came out of those experiments, which severely undercut the  focus necessary for perfecting the cloning process."     "Such as?" 
  "The notion of creating adult replicants such as the A through D  series that utilized your ova is one example. That scheme itself was  born from the founders' desire to replicate resistant versions of  themselves that could survive colonization. They were, after all,  more than middle-aged in most cases." 
  She nodded, but did not comment. The selfishness of the men that  Kurt referred to as the founders was no longer surprising, even  though it was still staggering in its scope. 
  "However the plan changed over time, the notion of creating a  resistant human using alien DNA and regrafting that into a  population has always been the stated goal." 
  "But not the general population," Scully said angrily, thinking of  Operation Paperclip. "This was genetic engineering designed for the  chosen few, specifically the families of the collaborators. The rest  of the human population would fall to the bees, right? And if any of  the collaborators became infected, they could be restored via your  vaccine." 
  The clone nodded. "Yes." 
  "Have they created a resistant alien/human hybrid?"  
  "Not as such," Kurt said. "We are an example of a more successful  line, but as I said, our lifespans are short."  
  "And Hannah?"  
  "She could be described as a kind of hybrid," the clone said  slowly, "but that is not entirely accurate. She is far more human  than anything else." 
  "What does that mean?!" Scully was at the end of her rope now, on  information overload. 
  "As you know, the E line was not successful"  
  Scully drew in a long breath through her nose, and stifled the  urge to scream. She could see that her emotional responses were  making the clone nervous. He was blinking quite rapidly, and his  words had become more rushed. 
  "The decision was made to alter the hybridization process to more  closely mimic human reproduction," he said.  
  "In what way?" she demanded. 
  "The F and G lines were created using genetic material from two  different abductees," he answered, "one of which had been altered  with alien DNA." 
  Scully had a momentary flash of Penny Northern's kind face and her  russet curls, and felt a sinking in the pit of her stomach. She  closed her eyes. All she could see were little girls like Hannah  with bags of poison attached to their arms.  
  "The distinction between the two lines was the amount of alien DNA  in the reproductive equation." 
  She looked askance at the clone. 
  "Their immune systems are not that robust," he answered her.  "Probably as a result of their parasitic form of reproduction, and  the manner in which it uses the virus as a primary means of doing  so. Their species does not mutate new viruses the way we do, the way  this world does, which makes them vulnerable. We have data on that  as well, if you'd care to see it."  
  She nodded wearily. "Not today, Kurt. Please continue." 
  "The F and G lines also failed. My understanding is that Dr.  Scanlon created the H series using a different set of standards.  That was the rumor at the time, but as he no longer trusted any of  us, that information could not be verified. It's only upon a  preliminary viewing of the data you procured that any understanding  in this matter has been confirmed." 
  "Go on," she said. "What do you mean by standards?" 
  "The H series is not a line of clones," Kurt said. "They were,  more accurately, siblings, although not full siblings. The amount of  alien genetic material in each iteration in the line is different,  although the parental genetic material remained the same." 
  "I understood that from the data I was able to decipher," Scully  said. 
  "The decision was made to create the H series using more  traditional human biological mechanisms, but to focus on the  immunological systems by utilizing two immune genetic donors." 
  "Are you saying that the all of the abductees weren't infected  with the black oil?"  
  "There is always a control group," the clone answered.  
  "Oh my God," Scully answered, "but if they infected those children  without giving them full immunity - why would they do that?" She  saw the answer in the clone's eyes, and let out a breath. "To see  what would happen. To see where the line was."  She closed her eyes  in thought. "But Hannah's other mother is also immune, that's what  you're saying? I don't understand then. Why didn't her siblings  survive the experiments?" 
  "Oh," the clone said, "I'm afraid I didn't explain that clearly.  Hannah was not created by egg fusion, but by an altered sperm being  injected into an ovum." 
  There was silence in the corridor for a full ten seconds, and when  the question came, it was not her voice that spoke it. Mulder's  voice, low and lethally angry, cut across the hallway like a whip  crack as she turned her head in surprise. "Whose sperm was it?" 
  She turned back in time to see Kurt blink, as if the answer was  obvious. "It was yours, Agent Mulder," he answered simply. 
~*~ 


[Non-text portions of this message have been removed] 


AXF is your list for ALL X-Files Fanfic... all genres, all characters, all ratings. 
 Automatic newsgroup posting too!  <URL>   Yahoo! Groups Links 
<*> To visit your group on the web, go to:      <URL> / 
<*> Your email settings:     Individual Email | Traditional 
<*> To change settings online go to:      <URL>      (Yahoo! ID required) 
<*> To change settings via email:      <EMAILADDRESS>        <EMAILADDRESS>  
<*> To unsubscribe from this group, send an email to:      <EMAILADDRESS>  
<*> Your use of Yahoo! Groups is subject to:      <URL> /   


