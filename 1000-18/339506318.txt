


Addictive drugs bind to existing brain receptors to create the high.  By creating an immune response to the drug, there may be a cross-over effect which affects the native endorphin (in this case, D-3 dopamine   <URL>  ) rendering the person incapable of naturally feeling the pleasure associated with that receptor.  This would be a kind of lobotomy which would affect the person's emotions and empathy, and probably cause depression or worse.  <URL>   <URL>  But apparently this is of little concern to our self-appointed moral guardians.  After all, pleasure deprivation is associated with workaholism.  <URL>  
 <URL>  
Vaccine for drug addiction could offer hope to users 
by Regan Doherty Wed Jan 30, 10:19 AM ET 
CHICAGO (AFP) - In a search for what could be the ultimate cure for drug addiction, scientists have developed a vaccine which prevents the body from getting high. 
The hope is that it can stop people from falling back into a spiral of addiction if they have a relapse. 
The most promising results so far have been with cocaine, but researchers hope it could also one day be used to cure addiction to methamphetamine, heroin and even cigarettes. 
"The vaccine slowly decreases the amount of cocaine that reaches the brain," said Thomas Kosten, a professor of psychiatry and neuroscience at Baylor College of Medicine in Houston, who has been working on the vaccine since 1995. 
"It's a slow process, and patients do not go through any significant withdrawal symptoms." 
The vaccine works by getting the body's immune system to recognize the drug as foreign and attack it in the blood stream. 
It does so by injecting an altered version of the drug into the body which has been attached to a protein that the body will recognize as a threat. 
"The body then says, 'This is a foreign article. I should start making antibodies to it,'" Kosten said in a telephone interview. 
The cocaine molecules eventually pass through the kidneys and are excreted through the urine. 
That stops the drug from reaching the brain and producing a sought-after high. 
Use of the vaccine would lead to a gradual tapering of dependence, Kosten said. 
"Gradually, antibody levels would rise. If you kept using (cocaine), you'd get less and less of an effect." 
Of all the drugs tested, cocaine is the easiest one for which to develop a vaccine because of an enzyme in the bloodstream, cholinesterase, that helps break it down, Kosten said. 
He has also begun to test vaccines for methamphetamine and heroin in animal studies, and hopes to eventually add nicotine to the list. 
"That's going to be the moneymaker," he said. 
The injections are designed for therapeutic -- not for preventative -- use, and are meant for those already suffering from addiction. 
That, however, does not rule out other possible future uses, Kosten said. 
"You could potentially inject pregnant cocaine users with the vaccine to prevent their fetuses from becoming contaminated," he explained. 
Other uses could include administering the vaccine to high-risk adolescents in order to prevent them from becoming addicted early on, he said, while acknowledging that this would raise serious ethical and legal questions. 
Testing for the cocaine vaccine has included a series of five injections over a period of three months, Kosten said. 
The vaccine has one more large scale human study scheduled before it is ready for the federal Food and Drug Administration approval process. 
A similar nicotine vaccine is also in the early stages of testing by several groups of European researchers. Kosten hopes to have the vaccine on the market in two to three years. 

