


Job Title:     Sr. Research Immunologist Job Location:  NJ: Rahway Pay Rate:      Open Job Length:    full time Start Date:    2008-02-13 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Sr. Research Immunologist &#150; BIO001689 
Job Description  
Submit 
Description 
Merck &amp; Co. Inc., established in 1891, is a global research-driven pharmaceutical company dedicated to putting patients first. Join us and experience our culture first-hand - one of strong ethics &amp; integrity, diversified experiences and a resounding passion for improving human health. As part of our global team, you&#8217;ll have the opportunity to collaborate with talented and dedicated colleagues while developing and expanding your career. We are seeking a highly motivated Sr. Research Scientist who will join a team focused on identifying and developing novel therapeutics for inflammation and pain. Responsibilities will include characterization of target enzyme inhibitors with the goal of providing detailed kinetic analysis to assist in lead optimization. Development of assays and participation in identification and characterization of novel enzymes and pathways critical to pathology will be required. A candidate with expertise in recombinant protein cloning and expression, site-directed mutagenesis, automation and high throughput assay development, and separation- based technologies such as HPLC and LC-MS/MS is highly desirable. Additional responsibilities include supervision of associate scientists and presentation to management. Due to the highly collaborative nature of the position, interpersonal, communication, and leadership skills will be extremely important. 
Qualifications Required-Ph.D and/or MD in, Enzymology, Biochemistry, or related discipline with at least 2 years experience inrelevant academic or industrial setting. Strong analytical, organizational, team-work and supervisory skills are essential. Education Requirement-PhD or MD Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. We offer an excellent salary and an industry-ranked benefits program, including tuition reimbursement, work-life balance initiatives and developmental programs at all levels. Merck&#8217;s retirement package includes a pension plan and one of the best 401(k) plans in the nation. To be considered for this position, please visit our career site atwww.merck.com/careersto create a profile and submit your resume for requisition # BIO001689. Merck is an equal opportunity employer, M/F/D/V - proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first - Merck Search Firm RepresentativesPlease Read Carefully: Merck is not accepting unsolicited assistance from search firms for this employment opportunity. Please, no phone calls or emails. All resumes submitted by search firms to any employee at Merck via-email, the Internet or in any form and/or method without a valid written search agreement in place for this position will be deemed the sole property of Merck. No fee will be paid in the event the candidate is hired by Merck as a result of the referral or through other means. 
Profile   Locations  US-NJ-Rahway   Employee Status  Regular  
Additional Information   Posting Date  02/12/2008, 11:19 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  03/29/2008, 12:59 AM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merck-377836 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



