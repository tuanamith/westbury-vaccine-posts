


 <URL>  California study finds no link between vaccine ingredient, autism 
Erin Allday, Chronicle Staff Writer 
Tuesday, January 8, 2008 
Rates of autism have increased in California despite the removal of the preservative thimerosal from childhood vaccines seven years ago, a finding that researchers say disproves the theory that the mercury in thimerosal causes the mysterious neurological disorder. 
The study, which analyzed autism rates in young children over a 12- year period, is the first to offer hard evidence that thimerosal plays no role in autism. Results of the study were released Monday in the Archives of General Psychiatry, a publication of the Journal of the American Medical Association. 
"Whatever the explanation for this increase in children with autism, exposure to mercury in vaccines is not it," said Robert Schechter, a medical officer with the California Department of Health Services and lead author of the study. "Vaccines with thimerosal and without have been safe and appropriate to give to our children." 
But even as researchers held up the study as absolute evidence that childhood vaccinations do not cause autism, some parents were quick to point out what they saw as flaws in the report. They stand by their claim that exposure to mercury - be it in a vaccine or from environmental sources - is a major cause of autism. 
Cases of autism, a neurological disorder marked by profound communication problems and impaired social skills, have exploded in the past two decades, pushing the condition to the forefront of medical research. Autism was considered rare before the 1990s, afflicting as few as 5 children per 10,000 births, but the Centers for Disease Control and Prevention estimated last year that as many as 1 in 150 children is diagnosed with autism now. 
At least 300,000 children ages 4 to 17 had autism in 2004, according to the CDC, and as many as 1.5 million people in the United States currently have autism. 
Symptoms usually appear in the first three years after birth. There is no cure, although therapy can help alleviate symptoms. Boys are nearly four times more likely to be diagnosed with autism than girls. 
Many researchers believe there is a genetic component to autism, but several large and vocal parent groups are convinced that exposure to heavy metals - especially mercury - is a major culprit. 
Until 2001, most childhood vaccinations included thimerosal, exposing children to a small but significant amount of mercury. Based on recommendations from pediatricians, thimerosal was removed from all childhood vaccinations in 2001. But rates of autism continued to increase, according to the new study. 
The study found that rates of autism for 3-year-old children climbed from 0.3 per 1,000 births in 1993 to 1.3 per 1,000 births in 2003. Similar increases were shown for children of all ages. 
Many neurological experts and child psychiatrists say the rise in autism rates is most probably due to increased awareness among parents and doctors, possibly resulting in over-reporting of symptoms. There also are cases of doctors giving autism diagnoses to children who have mild developmental disorders just to give them access to disability services, said Bryna Siegel, director of the Autism Clinic at UCSF. 
"It's being over-reported, and we're just starting to do studies to figure out why that is," Siegel said. "A lot of clinicians will tell you off the record that they're well aware that kids with a diagnosis of autism will get a variety of services. They're willing to err on the side of autism to get those services." 
Siegel said she understands why parents have been eager to prove a relationship between thimerosal and autism, but she thinks it's time to move past that theory. 
"It's very hard for parents to find out that they've somehow passed this horrible disorder to their child," Siegel said. "But people like me are exasperated that so much money and attention has gone into disproving the mercury hypothesis, when it could have been going toward treatment and research." 
Lyn Redwood, a board member of the National Autism Association who has a child with autism, said it's too soon to rule out environmental factors or even the role of thimerosal. 
Many of the children in the California study probably got childhood vaccinations outside the United States, she pointed out. And it's now common for pregnant women to get flu shots, which still contain thimerosal and can therefore expose a fetus to mercury, she said. 
"We need to look at these children and the metals in their body and not close the door on any theory," Redwood said. "When we get to the point where there is no more mercury in the vaccines, if they could go back to the database and figure out exposures for each child, then that's something. Otherwise, we're just guessing about whether or not there's an association." 

-- Veronique Chez Sheep 

