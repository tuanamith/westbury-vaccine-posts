


Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   BS/MS Information Technology - Summer Internship 2008 &#150; INF003285 
Job Description  
Submit 
Qualifications 
.     Pursuit of an undergraduate or graduate degree in business, engineering and related fields, information technology, mathematical subjects, systems engineering. .     Previous IT experience a plus. .     Aptitude for and interest in working with computer technologies that support: drug discovery and drug development; automation of factory and laboratory processes; novel and creative sales, marketing and e-commerce initiatives; and building the computing architecture that provides the foundation for innovative development. .     Strong interpersonal skills; proficient oral and written communications skills; desire to work in a collaborative team environment; ability to adapt to new technologies, business (situations), ability to adapt to a rapidly changing work environment, and ability to solve problems by creative application of new and existing technology, processes and information. .     Eligible to work in U.S. on a permanent basis (US Citizen or Permanent Resident) without sponsorship LOCATIONS: .     Rahway, New Jersey (20 miles southwest of New York City) .     West Point, Pennsylvania and local satellite offices in area (25 miles north of Philadelphia) .     Whitehouse Station and Cokesbury, New Jersey (48 miles west of New York City) .     Charlotte Technology Center, North Carolina (10 miles north of Charlotte, NC) Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose - bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition INF003285 or any other opportunities for which you meet the qualifications. Please be advised:  All expressions of interest in Merck employment must be received through the Merck career website. After bidding for the position of interest on-line, you will receive an e-mail confirmation that your resume was received. If you are considered for a position, you will be notified directly. 



Please refer to Job code merckcollege-326816 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



