


Job Title:     Co-Operative Assignment-Biologics and Vaccines (Mach) Job Location:  PA: West Point Pay Rate:      Open Job Length:    full time Start Date:    2008-02-07 
Company Name:  Merck Contact:       HR Phone:         email only please Fax:           email only please 
Description:   Co-Operative Assignment-Biologics and Vaccines (Mach) &#150; PHA000755 
Job Description  
Submit 
The Biologics and Vaccines department in Pharmaceutical Research and Development (located in West Point, PA) is responsible for the formulation of biological therapeutics for preclinical and clinical development. We are currently seeking motivated undergraduate students for a 6 month co-op position starting in July 2008. The candidate will be involved in the development of biophysical and biochemical methods for the characterization of vaccines and therapeutic proteins in the context of preclinical development.  Note that this is a paid co-operative assignment whereby a weekly stipend will be provided. Housing subsidy is not available as part of this program and if required by the student must be funded 100% by the student.  
Qualifications 
- Pursuing degree inbiochemistry, biophysics, pharmaceutical sciences, chemical engineering or a related field. - Grade Point Average (GPA) of at least 3.0 or higher preferred. - Applicants must be available for full time employment (40 hours per week) for 6 months targeted to begin in July 2008 and - Currently enrolled in an academic program Completion of coursework in molecular biology and biochemistry preferred. Candidate should have knowledge and understanding of biochemical/biological assays as they relate to protein characterization and posses strong scientific, technical and communication skills. Consistently cited as a great place to work, we discover, develop, manufacture and market a wide range of vaccines and medicines to address unmet medical needs. Each of our employees is joined by an extraordinary sense of purpose &#8212; bringing Merck&#8217;s finest achievements to people around the world. To be considered for this position, please visit our career site atwww.merck.com/careers. At this site you can search our &#8220;University Opportunities&#8221;, create a profile and submit your resume for requisition # PHA000775.  Merck is an equal opportunity employer, M/F/D/V &#8212; proudly embracing diversity in all of its manifestations. Our work is someone&#8217;s hope. Join us. Where patients come first &#8212; Merck 
Profile   Locations  US-PA-West Point   Job Type  Cooperative   Employee Status  Regular  
Additional Information   Posting Date  02/06/2008, 07:29 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Unposting Date  02/11/2008, 11:59 PM, Montreal, New York, Washington D.C. - (UTC -5:00)   Number of Positions  1 



Please refer to Job code merckcollege-377236 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



