


Job Title:     Manufacturing Biotechnician Job Location:  PA: West Point Pay Rate:      Competitive Job Length:    full time Start Date:    2007-01-16 
Company Name:  Merck & Company Contact:       Brian Marczyk Phone:          Fax:            
Currently, we are in search of qualified individuals for the following position:  
In this role, you will be involved in the biological production of microbial and tissue-culture grown virus vaccines, as well as in maintaining all production records and associated documentation, and reviewing processes to ensure that only the highest quality product is manufactured. Additionally, you will be required to clean, assemble, operate, disassemble, maintain, troubleshoot, and initiate corrective action on manufacturing equipment to meet production schedules and perform fermentation, purify proteins/polysaccharides, grow tissue culture/viruses, and prepare trypsinized cell suspensions. 
To qualify, a BS in one of the following Life Sciences is required: Biology, Chemistry, Biochemistry, Biochemical Engineering, Chemical Engineering, Microbiology, Molecular Biology, Medical Technology, or any other related Life Science. The flexibility to work 2nd or 3rd shift, as well as weekends, is also required. 
Merck is an equal opportunity employer, M/F/D/V  proudly embracing diversity in all of its manifestations.  Our work is someones hope. Join us.  Where patients come first  Merck 
For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



