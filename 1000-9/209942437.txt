



"Wendy" < <EMAILADDRESS> > wrote in message   <NEWSURL> ... 

Unfortunately when it comes to rabies the animal usually loses.  I do rabies  testing for our county public health department and we see so many animals  euthanized for really stupid reasons because there "may" have been an  exposure.  One of the saddest was a Saint Bernard  who had slobbered on  someone--the poor beast was euthanized and tested for rabies.  I'd like to  see a St. Bernard that doesn't slobber.  Rabies is a real risk in much if  not most of the US (lucky Brits!) and there are times when testing is  warranted.  It is a horrible disease, painful and invariably fatal but so  many times testing seems needless. People panic, the shots are expensive  (and a lot of people hate shots!) and many people don't want to pay for  their animals to be quarantined for several weeks. 
In the lab, we (the microbiologists) are vaccinated against rabies and that  might be a good idea for those who work extensively with feral animals,  including cats.  Unfortunately many of the cats we end up testing for rabies  are cats that are injured or being rescued that bite their would-be  rescuers.  Also, many people don't keep up on the rabies vaccines. 
People can get cat-scratch disease by cat scratches and by biting, so  declawing ain't going to help and in healthy people it's not that serious.  (It can be a real problem for people that are immunocompromised.)  On the  other hand, cat bites can be very serious, especially if the bite is deep.  A couple of years ago one of the county animal control officers ended up  spending several weeks in the hospital on IV antibiotics due to a deep cat  bite to the bone. 
Just my 2 cents 
Bonnie 








