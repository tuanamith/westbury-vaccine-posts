


On Tue, 19 Dec 2006 14:26:13 -0500,  <EMAILADDRESS>  wrote: 

When the breed/gene is a rare breed AND it is not subsumed into some other breed, then when the breed disappears its particular unique genes and characteristics are lost forever.  For example, if there's a rare breed of chicken in a remote section of Africa, and modern chickens are imported to that area and the old breed dies out (e.g. villagers slaughter them wholesale for food because they have the "new and improved" birds to breed and go forward with) without interbreeding with the modern birds, then those genes are lost forever.  If we later discover that the imported chickens are susceptible to something (e.g. WNV) that the local population was naturally immune to, too bad - the gene that made that local population immune is now GONE.  Maybe a similar gene reappears after some generations of "natural selection" within this transplanted population.  Maybe that gene is present in some other rare breed.  But as rare breeds go extinct the odds that the genes are lost AND that we subsequently discover we needed them go up.  Then what? 
Some hybrids and vaccines which improve our modern breeds were developed by using genes from rare breeds or species.  There is real worry that as rare breeds die out, the remaining breeds become increasingly at risk to afflictions or diseases that can't be cured. The Irish potato famine occurred because all the potato plants in Ireland were from one strain that was susceptible to the blight.  If breed extinction continued until there was one sole remaining potato breed anywhere in the world, potatoes themselves could cease to exist anywhere on the planet.  This is one reason that scientists are loathe to destroy the remaining smallpox cultures - the genes in those stores might someday be needed to fight a different (related) disease.  (Even if we have plenty of smallpox vaccine and don't need the smallpox culture itself to make more.) 
jc 
--  

