


Who would have thought!!! Grrrrrrrrrrr just goes to show what we take for  granted.  <URL>  
According to a poll of more than 11,000 readers worldwide, the British  Medical Journal (BMJ) says that sanitation is the greatest medical advance  since 1840, the year that the BMJ was born. 
Sanitation, defined as access to clean water and the disposal of sewage, got  15.8 per cent of the votes, just ahead of other advances such as antibiotics  (14.5 per cent), anaesthesia (13.9 per cent) and vaccines (11.8 per cent). 
The poll was conducted to celebrate the launch of the new format BMJ. The  poll was launched by asking readers to say what they thought the best  medical advance was since 1840. They received more than 70 categories, which  a panel of experts narrowed down to 15 "milestones". 
Readers were then invited to vote for one of the 15. Each of the 15  categories also had a "champion" in the form of a leading doctor or expert  with some connection to the "milestone". For instance Doctor Stephanie J  Snow, a descendant of John Snow, who developed the first anaesthetic, ether,  that was later replaced with James Young Simpson's chloroform. 
The champion for sanitation was Johan Mackenbach, Professor of Public Health  at the Erasmus University Medical Centre in Rotterdam. He was said to be  delighted that so many people acknowledged this important milestone. He said  that the general lesson that still holds true "is that passive protection  against health hazards is often the best way to improve population health". 
In his defence of the sanitation milestone where he explains why he thinks  that deserves the number one place, he describes the history of sanitation.  John Snow, the same guy again, proved that cholera was being spread in piped  water when he shut off a pump in a district of London and stopped the spread  of the disease in that area. 
Edwin Chadwick thought about linking up homes to clean drinking water and  properly drained sewage to reduce the spread of illnesses. Between 1901 and  1970, as a result of taking up his ideas, deaths due to diarrhea and  dysentery went down by 12 per cent in England, Wales and the Netherlands. 
Professor Mackenbach says that "environmental measures may be more effective  than changing individual behaviour", explaining that installing pipes and  sewers is more effective at reducing death and disease than trying to  persuade people to change their health and hygiene habits. 
Poor sanitation is still a significant problem in the developing world says  Professor Mackenbach. 
In the developed world sanitation has advanced technologically to include  not only the removal of sewage but also the treatment of wastewater,  including for reuse in cities and urban areas. 
A more recent advance, and in most instances still at the experimental  stage, in the technology of sanitation has been the idea of ecological  sanitation, where urine and feces are separated at source. This removes the  fecal pathogens from the wastewater which can then be treated as "greywater"  which is kept for non-sanitary use such as to water the garden. 
Click here to read more about the BMJ's 15 milestone categories. 
The World Toilet Organization. 
Written by: Catharine Paddock Writer: Medical News Today Article URL:  <URL>  





