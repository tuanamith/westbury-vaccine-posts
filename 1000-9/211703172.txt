



I took a microbiology class last fall, and the proffessor had an interesting take on it. 
She thought the vaccine would be useful, but that the way it was being handled was designed to enrich the pharmaceutical companies - and the obvious way to handle it was to vaccinate boys also, not just girls, since they spread the disease, so that the disease could be wiped out eventually (like smallpox) rather than just adding a vaccine that children would be recieving indefinitely. 
I think there is a point there. If there is no nonhuman reservoir of a disease, why not wipe it out? In the long run, it's much cheaper than vaccinations continuing indefinitely. 
As for the vaccine debate, I think they do an awful lot more good than harm, but there are a significant group of people in this country that oppose them for religious reasons, misinformation, or distrust of pharmaceutical companies. Part of the problem is that they're not finding ways to justify them right now. Vaccination may have destroyed smallpox, ended polio in most of the world... but what is heard now is about the diseases that were mostly eradicated in the U.S. like whooping cough and measles that infect people that are vaccinated... never mind that the source of these outbreaks is almost always unvaccinated people bringing the disease in from other countries, and 9/10ths of the people that get sick are usually unvaccinated, and if no one was vaccinated, the outbreaks would spread like wildfire... 
Another problem is flu shots. They really are designed just to enrich the pharmaceutical companies, and a lot of people see that program, how well it works, get the flu, either right away or later on since the shots don't work quite often (they say you can't get it from the shots... you can't, but you can have flulike symptoms as a reaction to the shots...), realize how worthless the flu shots are, and assume other vaccines are all like that. 
What needs to be done for vaccination is more focus on diseases that scare people. The only significant new vaccination in the past 10 years is for Hepatitis B. People in this country are scared of new diseases, not existing ones that they know and understand. We've conquered the worst of the common diseases that can be stopped through vaccines (smallpox, polio, measles). We need to focus on other stuff. 
The HPV vaccine is useful, but there's bound to be pushback. An awful lot of people didn't think of HPV as a significant threat before... they just weren't worried about it. And now, they still aren't, and the vaccine seems useless to them. It's the same problem as the chicken pox vaccine. People aren't scared enough of the disease to really be excited about the vaccine. 
-- theoneflasehaddock 












On Feb 8, 7:59 am, "ren" < <EMAILADDRESS> > wrote: 



