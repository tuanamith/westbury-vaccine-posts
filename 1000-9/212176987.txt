


ALLIANCE FOR HUMAN RESEARCH PROTECTION (AHRP)  Promoting Openness, Full Disclosure, and Accountability   <URL>   and  <URL>       FYI    There are 61.5 million dogs in the U.S.-- So with help from psychiatry and the FDA guess who has been eyeing this "underutilized" market population? 
Eli Lilly is repackaging its antidepressant, Prozac, as a veterinary drug under brand name, RECONCILE. 
"I've seen shy dogs become sociable, fierce dogs become friendly, and neurotic dogs become normal," says Manhattan animal psychiatrist Bert Barkowitz. And his patients seem to agree.  
And Pfizer received approval for a weight loss drug, SLENTROL, for dogs!!!!!!!!!!!!! 
Now if Lily and Pfizer provide payola-as Merck did to promote its vaccine Gardasil--expect a brigade from Women in Government to push governors to issue mandates for every female dog to be medicated with RECONCILE!! 
Best article on the subject by humorist Mitch Limus (below) --See article with pictures  <URL>   
  
Contact: Vera Hassner Sharav  212-595-8974   <EMAILADDRESS>  < <EMAILADDRESS> >      
New York Post  RX TO GET MUTTS OUT OF RUTS Post Wire Services  Does your doggy get down when you're out of town?   
 Put pep your pet's step and get that tail wagging again with pooch Prozac.  
The Food and Drug Administration has just approved a version of the classic antidepressant to help dogs cope with separation anxiety.  
The Eli Lilly & Co. product, to be sold under the name Reconcile, is designed to help quell panic attacks and bad behavior caused by being away from their owners. The FDA said that the chewable drug should be used in conjunction with behavior counseling.  
~~~~~~~~~~~  
 <URL>   
Pooches Pop Prozac to Treat Behavioral Problems  by Mitch Lemus < <EMAILADDRESS> >   
"Playing fetch just wasn't what it used to be," says Zach, a five-year old Golden Retriever. "Throw the stick, fetch the stick ... throw the stick, fetch the stick ... My attitude was, 'how about I hide the stick, and you fetch it?'"  
Sheeba, a Collie, says she suffered from a "Lassie Complex," trying to live up to the standards of the legendary star.  
And King, a 60 pound German Shepherd, often barked at the mailman with rabid abandon. "Once, I even gnawed his post office-issue pant leg right off, he says."  Zach, Sheeba, and King, (not their real names) are among a growing breed of dogs -- dogs on Prozac.  
Originally developed for humans, the controversial antidepressant is now being prescribed by veterinarians for canines suffering from a variety of emotional disorders.  "I've seen shy dogs become sociable, fierce dogs become friendly, and neurotic dogs become normal," says Manhattan animal psychiatrist Bert Barkowitz. And his patients seem to agree.  
Zach not only shunned playing fetch, but had also lost interest in scent-marking and crotch sniffing. Then, he was prescribed Prozac. "I'm my good ol' horn-dog self again," he said, eyes riveted on a slinky Labrador strutting through Central Park. "Look at the tail on that bitch! Wow. I mean bow wow!" he howled.  
Some owners report that the drug has been useful in treating obsessive compulsive disorders, as well. Dot, a dalmatian, would spend hours trying to scratch her spots off. But after just five days on Prozac, her nervous habit ceased. "I spent a fortune on trainers, therapy, and new-age diets, but nothing ever worked until my vet suggested I share my Prozac with Dot. All I can say now is that she's better than well," raved Dot's owner.  
But not everyone endorses the practice of dispensing drugs designed for humans to pets. When one owner sought Prozac to keep her Beagle from licking his testicles, her vet threw her out of his office. "It's disturbing how some people insist on meddling with normal animal behavior," says Dr. Murray Muzzleman. "Yes, dogs lick their balls. Why? Because they can."  
Dr. Muzzleman shares his bias against psychotropic drugs with the Church of Zooinology. Zooinologists charge that Prozac can unleash violent tendencies in otherwise passive pooches. They cite the case of Skippy, a fluffy white poodle who went on a bizarre biting spree, mauling his owner and 3 small children. The attack finally ended when Skippy intentionally ran in front of a Greyhound bus, killing himself.  
"Skippy never exhibited violent behavior before Prozac," insisted family members. "Instead of helping him, the pill turned him into a doggone killer."  
Eli Lilly, Prozac's maker who is fighting a spate of Prozac-related law suits, contends there is no proven link between the drug and canine violence or suicide.  
Not so says Rusty, a watchdog who sought relief from depression when the security firm he worked for downsized, eliminating his job. "Soon after starting Prozac, I began to froth at the mouth," he says. "I ripped the stuffing out of my owner's couch, peed on her bed, swallowed her engagement ring, attacked the cat next door, knocked over the garbage can, ate the kids' homework, then threw up on a $10,000 Persian rug. Some miracle drug. It's a miracle they didn't impound me -- or worse, put me to sleep," laments Rusty.  
Says one animal lover, "So many dogs have problems these days because neglectful owners leave them cooped up in small apartments for hours. Then, when the dog mauls their Gucci shoes, they punish him. These animals don't need drugs. What they need is love."  
Indeed, so-called "cosmetic canine pharmacology" has been a bone of contention, raising a plethora of ethical issues. Currently, the Westminster Kennel Club is grappling whether to allow show dogs to take mood enhancers. If the drugs are banned, should show dogs be subject to mandatory blood tests, the same way olympic athletes are?  
Other organizations, like the ASPCA, are concerned pet stores and puppy farms will put pooches on Prozac to make them more outgoing and friendly, and thus, more saleable. They argue that dogs who don't take the medication are automatically at a disadvantage, and more likely to be abandoned or destroyed.  
Winki, President of Pooches Union of Prozac Poppers (PUPP), feels the decision to take mood-lifting drugs should be left up to the individual animal. "As a Chihuahua, I used to be shy and easily intimidated, yapped the diminutive five-pounder. "But on Prozac, I feel like top dog. Now I don't take crap from nobody, be it a Doberman, Pit Bull Terrier, or authoritarian human being.  
Other PUPP members are equally outspoken. "All I know is that it has worked for me, says Gulliver, a spayed Cocker Spaniel. "Thanks to Prozac, I'm now able to cope with the fact that I'm never going to have puppies."  
"Dog food has taste again," says another formerly depressed pooch. "For the longest time, not even Milk Bones, my favorite treat, could spark my appetite. Now, for the first time in years, I'm savoring those chunks of luscious meat by-products."  
Perhaps the controversy is best put into perspective by Dr. Barkowitz, the animal psychiatrist, who asks, "What does it mean to live a dog's life, anyway?"  
________________________________ 
 <URL>  e BLOOMBERG NEWS Lilly Drug, Form of Prozac, Approved for Anxious Dogs (Update3)  By Shannon Pettypiece 
Feb. 9 (Bloomberg) -- A variation on Eli Lilly & Co.'s depression pill Prozac can now be used to help dogs cope with the anxiety of being separated from their owners.  
The product, to be sold under the name Reconcile, is designed to help quell panic attacks and bad behavior that separation anxiety can cause. The chewable drug should be used in conjunction with behavior modification, the U.S. Food and Drug Administration said today in a notice on the agency's Web site.  
Reconcile would compete with Novartis AG's Clomicalm, approved in 1999.  
The FDA last month cleared the first prescription weight-loss drug for dogs, Pfizer Inc.'s Slentrol.  
There are about 61.5 million pet dogs in the U.S., according to the American Veterinary Medical Association, based in Schaumburg, Illinois.  
Dogs suffering from separation anxiety may ``go into a funk'' or have a panic attack, said Nicholas Dodman, who directs the animal behavior clinic at the Tufts Cummings School of Veterinary Medicine, in Grafton, Massachusetts, in an interview last month.  
Dogs on the drug should also receive behavior counseling, such as rewards for good behavior and training to be content alone, according to the drug's prescribing information.  
Shares of Lilly, based in Indianapolis, rose 2 cents to $54.23 at 4 p.m. in New York Stock Exchange composite trading.  
To contact the reporter on this story: Shannon Pettypiece in Washington at  <EMAILADDRESS>  < <EMAILADDRESS> >  .  
Last Updated: February 9, 2007 16:19 EST  
  
_______________________________________________ Infomail1 mailing list to unsubscribe send a message to  <EMAILADDRESS>  

