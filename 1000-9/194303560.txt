



 <EMAILADDRESS>  wrote: 
I agree with you; science often discovers and describes events before the mechanism is understood. What it requires is evidence, a model that fits the evidence, and a way to test it. The testing usually involves predictions unique to the model, and confirming whether they hold up or not. 
So, what evidence indicates design? What predictions does it make that, say, evolutionary science doesn't make? How could it be falsified, and what would we look for that we haven't seen yet? 
Evolutionary science predicted, for example, the existence of tiktaalik, its major characteristics, and even where its fossils would be found. 
If I were to fund a research team in intelligent design science, what would they look for? Two independent paleontologists can look for a fossil with certain characteristics, and agree independently whether a candidate fossil shows those characteristics. What would two ID researchers look for in an object to confirm ID, and how would they agree? Can you give an example? 

Ummm... the causes of mutations are pretty well understood. 

That's what the evidence indicates. Patterns have been looked for, and no pattern indicating a bias associated with a gene's expression and its usefulness has been found. Of course certain kinds of mutations are more common than others; certain locations are more likely to show mutations than other locations; exposure to mutagens like gamma radiation or halogenic solvents increase the incidence of mutations, etc. But purpose or response to the "need" of a species does not show up. 
Feel free to establish otherwise. 

Of course. Let us know if you find any. It would be hard to quantify. How would you scan the genome of an organism's offspring for a useful mutation? It might be found retroactively. So far, however, it looks as though the only pattern is imposed by natural selection, acting on a pool of variability caused by random mutation. 

True. But scientists do determine what science is. Who else? Jazz musicians determine what jazz is, and Baptist ministers determine what Baptist doctrine is. 
Scientists have a methodology developed over generations of observation - it gets results. 

Did something get snipped? 

Evolutionary theory is observable and falsifiable. You have been told this over and again. 
The flu shot I got last week was developed using the evolutionary model. 

<snort> You don't have to watch the whole Grand Canyon erode to confirm it. 

Um... you could in principle. In any event, evolutionary theory  would not disintegrate if there were bias found in mutations. It would change significantly, tho. 
If fossil transitions were not associated with continental drift, or the chronology of the strata, it would have been falsified. If the geneomes were not a nested hierarchy, or if tit did not match the nested hierarchy of morphology. For that matter, if Linnaeus could not have determined a morphological nested hierarchy, the ToE would never have gotten off the ground. 

Design of computer chips. Development of flu vaccines. Management of wilderness reserves, wetlands, and marine environments. Engineering of pest resistant crops. New breeds of animals. Treatment of congenital, genetic diseases. To name a few. 

I don't care much for baseball. Yet I pay for the stadium. And I never volunteered my taxes to kill 655,000 Iraqis, nor send my neighbor's kids to war. Democracies are like that. 

Wait - we don't know how something happened, therefore Jesus? 
Up there ^^^ you said that we couldn't know if the mutations were *really random... and now you claim that there couldn't possibly be any natural path to the cell. That could be true only if we knew all possible paths. Do we? 

The constitutional scholars indicate otherwise. 

OK with me. But not in science class, nor in history. Teaching comparative religion is not history, nor science. 

Fine. But in the US, you are not teaching this in public schools. Nor is it science, anywhere. 

There are many things I don't know. Making up stories is not an explanation. 

 A law is a simple description of universal behavior, usually expressed as a formula. We deduce laws by observation. We don't have a constitution of the universe. We might be wrong on any of them. Newton, for instance, was wrong at the extremes of scale. 

You may be right. But you may be wrong, and there's no way of telling. Therefore, it's not science. When you can test it, let me know. 

Why not? IT makes you unhappy to think so? 

Don't know. Making stuff up is not knowing; it's closing your account with reality. 

Don't know yet. And neither do you. 
Kermit 


