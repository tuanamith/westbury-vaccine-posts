


 <URL>  
Judge blocks Perry's coal power plant order 
By R.G. RATCLIFFE 

AUSTIN - A state district judge today blocked Gov. Rick Perry's executive order creating a fast-track permitting process for coal- fired electric generating plants, a ruling that also could impact the debate over Perry's order requiring cervical cancer vaccinations for pre-teen girls. 
State District Judge Stephen Yelenosky ruled that four environmental groups "are likely to prevail on their argument that the governor lacks the authority" to issue an executive order telling a state agency to hold hearings and reach a decision by a specific deadline. 
Yelenosky ordered the State Office of Administrative Hearings to ignore Perry's demand that hearings on new power plants be limited to six months. 
He did not specifically halt a hearing scheduled for today on TXU Corp.'s request for permits for six coal-fired plants, but he told the state's administrative law judge to reconsider requests for a delay from environmental groups. 
While Yelenosky's order did not specifically relate to the controversy over the human papillomavirus vaccine, HPV, it was the first judicial ruling that indicated a governor does not have the power to direct state agency operations by executive order. 
Legislators have asked Attorney General Greg Abbott to rule on whether Perry had the authority to order HPV vaccinations for all girls entering the sixth grade in 2008. Lawmakers also are considering legislation to overturn Perry's executive order. 
In 2005, Perry had issued an executive order putting the permitting of new electric generating plants on a fast track. That meant administrative hearings on the permits, which normally take a year, would be reduced to six months. 
TXU Corp. is seeking permits for 11 new coal-fired electric generating units. The State Office of Administrative Hearings had been scheduled to start taking testimony Wednesday on permits for six of the units. 
The ultimate decision on whether to issue the permits is up to the Texas Commission on Environmental Quality based on a recommendation to be made by the administrative law judge. 
TXU spokeswoman Kim Morgan said the company is "disappointed" in the decision. 
"Every day that a solution is delayed leaves older, less efficient power plants online too long - affecting prices and clean air," Morgan said. "And it brings us one day closer to the potential of widespread blackouts. 
Morgan said lawyers for the interested parties have had "significant" discovery in the case so far. 
"There is no reason why SOAH shouldn't go forward with the hearing tomorrow," Morgan said. 
Lawyers for four North Texas citizens groups that oppose the power plants said the state permit hearings should be delayed because Perry lacked the power to order the expedited hearings process. 
Attorneys representing Perry and the State Office of Administrative Hearings said the citizen groups did not have standing to sue unless the administrative law judge hearing the environmental case ruled against them. 
The attorney general's office referred all questions about Yelenosky's ruling to the State Office of Administrative Hearings. That office did not immediately return telephone calls. 
The lawsuit was brought by Citizens Organizing for Resources and Environment; Texans Protecting Our Water, Environment and Resources; Citizens for Environmental Clean Air; and East Texas Environmental Concerns. All four groups consist of people who live near the proposed coal-fired power plants. 
Robert Cervenka, a Waco-area farmer with TPOWER, said if the plants are built he will have to move away because his wife has respiratory problems. There are nine units proposed for the Waco area. 
"We're actually in a ring of fire," Cervenka said. 
TXU was not a party to the court proceedings. The company has argued that the plants it is proposing to build will be cleaner than some older coal-fired facilities. They also have said the technology for a newer coal gasification facility is not developed to the point of being commercially viable. 
Blackburn said the administrative law hearings need to be delayed so all sides have an adequate amount of time to submit evidence into the record that can then be used in any appeals. He said only that evidence in the record can be considered on appeal. 
"All of those issues will not be fully developed. Once you go into a hearing like this, if you don't get the evidence on the record, you don't get to do it over again," Blackburn said. 
Blackburn said Perry might have been acting in what he believed was the state's best interests, but he said the governor lacked the official power to tell a state agency how to act. 
"This is a grab of power that is unprecedented and it's wrong," Blackburn said. 
Blackburn said neither the state Constitution nor state law gives the governor the power to tell state agencies how to operate or order them to change their rules unless it is a time of emergency, such as after a hurricane. 
Assistant Attorney General Shelley Dahlberg told Yelenosky that the timing of the lawsuit was inappropriate. She said the citizen groups will not have grounds to sue the state until after the administrative hearings have ended and then only if the administrative law judge rules against them. 
"The (executive) order was issued in October of '05. It did not harm them then, and it does not harm them now," Dahlberg said. 
Dahlberg said the citizen groups also should not be able to pursue their claims that they have been denied constitutional due process because that applies to harm of property rights. 
"There's no property right that these people have clean air," Dahlberg said. 
But David Kahne, another of the groups' lawyers, argued that there are property rights involved. 
"The members of our groups do own real estate," Kahne said. "They have the right not to suffer invasion of their lands by these pollutants." 


