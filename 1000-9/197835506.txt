





This scares me... I have a little one, who due to RSV as a 2 week old, gets every bug that comes within yards of her.  Further, she does not fight things off well.    Thank heavens I have her pulled from daycare for the next month or so (for a different reason, but good nonetheless) 
This is bad news all around, especially for the eldery and sickly kids... 
What makes me maddest are the regular joes who go to work and send kids to school with sniffles just bc their bodies can handle it-- these are the jerkoffs who spread illness to those who cannot handle it. Give it to some coworker who unknowingly passes it off to an infant or gramma.  GRRrrr! 
For Gods sake, if you're sick, stay at HOME and dont go out to resturants and malls or work or school --- stop coughing and sneezing on the rest of us!!!!!!!!!!!!!!!!! 

And, break out the antibacterial gel.  Word to the wise. 
Please read below: __________________________________ 

State, Warwick close school 
01:00 AM EST on Sunday, December 31, 2006 
BY SCOTT MacKAYJournal Staff Writer 
CRANSTON - An outbreak of a form of walking pneumonia among children at Greenwood Elementary School in Warwick that may have led to the death of one student and a serious illness in another has prompted state health officials and Warwick Mayor Scott Avedisian to close the school for a week and treat all the school's students and their families with antibiotics in an effort to contain the spread of the disease. 
Beginning today at 11 a.m. at the school, doctors will distribute antibiotics at no cost to school staff, children who attend Greenwood Elementary and their families, Avedisian and Dr. Robert Crausman, acting state health director, said at a news conference last night at the state National Guard Headquarters in Cranston. 
Students will be tested with throat swabs so health officials can determine the extent of the spread of the bacterial illness. 
Distribution of antibiotics will begin today and continue through Tuesday. In addition, state health officials will hold meetings with families of students and staffers at the school to answer any questions they may have about the illness. 
Those meetings will be held today at 10 a.m., noon and 2 p.m. The same schedule will hold for tomorrow and Tuesday. 
State health officials have established a toll-free telephone information line at 800-942-7434. 
In medical terms, the bacteria mycoplasma pneumoniae. It usually shows up as bronchitis or walking pneumonia. In rare cases, people sick with the mycoplasma pneumoniae bacteria can develop meningitis or encephalitis. 
State Health Department officials said they began their investigation after Dylan Gleavey, a second grader at Greenwood, became sick with encephalitis, a brain infection, in November. He died Dec. 21. A second student in the classroom, Hannah Leahy, was diagnosed with meningitis, an infection around the brain. She is progressing well after treatment at Hasbro Children's Hospital. 
As recently as last Wednesday, Dr. Utpala Bandy said state officials did not believe there was any public health threat. That changed last night. 
Since Dylan Gleavey's death, two more Greenwood students have been diagnosed with the bacteria and doctors from the state Health Department and the federal Centers for Disease Control have suspected two additional cases, bringing the total to six. The disease is easily spread among children because it is transmitted via saliva, especially during the winter cold season when youths do not cover their mouths when coughing or sniffling, said Crausman. 
It has a fairly long incubation period - two to four weeks in most cases - said Crausman. 
Still, Crausman said, it is "very, very rare" for mycoplasma to lead to such life-threatening illnesses as meningitis and encephalitis. Greenwood school has about 275 students and 40 staff members. There is no preventative vaccine, but if caught early mycoplasma is usually effectively treated by a five-day antibiotic regime. 
That is why all students and their immediate families will be given antibiotics, Crausman said. Adults will likely be given the drugs in tablet form and children will be given a syrup. 
It is sometimes difficult to detect, Crausman said. For example, even after an autopsy of Gleavey, state health and CDC officials are not sure whether he had mycoplasma pneumoniae. 
Avedisian said all families of students will also be notified about the meetings and antibiotic treatment. 
State health officials announced these rules: 
=B7Only Greenwood Elementary School students, staff, and their immediate family members will receive antibiotics. 
=B7Not all family members need to be present to receive antibiotics. 
=B7Testing will be made available to all students, staff, and family members who participate in antibiotic distribution. 
To receive antibiotics, you must bring: 
=B7Photo identification. 
=B7The names of Greenwood students who are family members. 
=B7The ages, weights, medication allergies and medications for each student and family member. 
=B7The name of the primary care doctor for each student and family member. 


