



Fred G. Mackey wrote: 
No, I criticized *one* part of the city's government for having poorly chosen goals resulting in unwise policies in *one* aspect of city planning.  That's hardly "crazy", nor it is an indictment of the entire city government. 
Perhaps you aren't aware that city governments do more than plan traffic and parking issues.  This city is about to announce a free citywide wireless internet access plan.  It has a highly innovative plan for helping the homeless, called "Care Not Cash".  It is conducting human trials of a potential HIV vaccine.  Replacing light industrial districts south of Market with new housing developments that include 16% below-market-rate units to help lower income people become homeowners.  Creating incentives for developers to build environmentally friendly buildings. 
But oh, you'd rather think the place is "crazy".  Ever been here?  It's the best city in the United States. What claim to fame does your home town have?  Did they found the United Nations there - oh,wait, no, that was San Francisco.  Is your city the most popular tourist destination in North America?  Nope, that would be San Francisco again. 


 > > 
A lack of parking is the equivalent of descending in to chaos?   Oh, hilarious.  More envy from someone who no doubt lives in a dusty little place that people don't move to, don't spend their vacations in, unlike my city, but instead some place from which they only want to escape. 


