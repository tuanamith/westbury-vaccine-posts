


Daniel T. wrote: 
The accepted premise is that one footnotes and references one's sources. 
Sorry that seems yet past your abilities. 

P_R_O_V_E  I_T! 
 <URL>  
The Charge: Fraud by Kevin Vaughan, Rocky Mountain News 
None of that is in dispute. 
One key fact is: how the disease got there. 
Enter Ward Churchill. 
None mention the Army's presence at Fort Clark - it wasn't a military post. 
None mention an alleged "parlay" at which smallpox-infested blankets  supposedly were distributed to Indians. 
None mention a store of vaccine locked away, deliberately withheld from  Indians. 
Churchill, however, stood by his assertions in a recent interview with  the News. 
He said he could make a "slam-dunk" case for his version of events. 
However, the sources he pointed the News to don't support his assertion  either. 
Life and death at Fort Clark 
It was a different place and a different time. 
Trappers, traders and Indian tribes called the region home, coexisting  peacefully at times, not so peacefully at others. 
Enter Chardon. 
In June 1834, Chardon became the bourgeois - or manager - at Fort Clark. 
His first entry, dated June 18, 1834, simply read: "Steam Boat  Assinniboine arrived." 
He recorded the event, and the day's other goings-on, in the most  austere fashion. 
"The Steam Boat St Peters hove in sight at 2 P.M." he wrote, in part,  that Sunday. 
'Fabricated all these events' 
In 1992, he wrote a legal brief after being arrested during Denver's  Columbus Day parade. 
Churchill reproduced the brief in his 1994 book, Indians Are Us?:  Culture and Genocide in Native North America. 
Then Churchill veers to Fort Clark. 
Thornton's passage dealing with the arrival of smallpox appears on page 96. 
"I don't know what else to say - it's black and white what he said, and  black and white what I said." 
Churchill responded that he cited Thornton as a source "on demography  only." 
But a year after Indians Are Us?, Churchill wrote Since Predator Came. 
Churchill also denied that he based the core of his assertion on  Thornton's work. 
Churchill declined follow-up requests from the News to address the subject. 
Churchill began his discussion with a look at the Amherst incident.  Then, on page 155, he moved to Fort Clark. 
He had not mentioned the steamboat St. Peters or "trade" blankets in his  two earlier versions of the story. 
But that wasn't the end of the account. 
"I don't think I mentioned it anywhere in the book. . . . I think he  invented that." 
But Churchill hasn't backed off the assertion. 
Nowhere on that page does Connell accuse a soldier of intentionally  infecting anyone with smallpox. 
Witness lost son in epidemic 
If anyone had a reason to lash out over the 1837 smallpox epidemic, it  was Francis Chardon. 
 From there, he began chronicling the sight of death. 
"Several more Mandans died last night," he wrote July 29. 
For Chardon, the burden was, on one hand, economic - he depended on the  Indians of the area as trading partners. 
It eventually also took a very personal toll. 
"My youngest son died to day," Chardon wrote Sept. 22, a Friday. 
The child was 2 years old. 
'Irrespective of particulars' 
After a lull of several years, Churchill returned to the smallpox story  in a flurry of writings in 2003. 
Once again, he attributed the passage to Thornton's book. 
On page 48, Churchill revisited the Army-smallpox story - with new details. 
This time, he wrote that the blankets were distributed at a "parlay  requested by the military." 
In it, Churchill told his most expansive version of the Army-smallpox  story. 
He began, again, in the wrong year - 1836 - but quickly moved to new  territory. 
But Robertson was skeptical of Churchill's contention. 
"The military didn't have anything to do with it." 
'A well-documented story' 
It helped fuel a much closer examination of Churchill's scholarship. 
Churchill dismissed Brown as a "punk." 
But Brown wasn't alone. 
That, he said, "would seem to be a distortion or a misquote." 
Until now. 
As far as Thornton is concerned, there is no controversy: Churchill  mischaracterized and misrepresented his work. 
At its core, the allegation that anyone intentionally infected the  Indians doesn't pass the common sense test, he said. 
Changing numbers 

