


 <URL>  
 Thursday, December 21st, 2006 
Bush "Developing Illegal Bioterror Weapons" for Offensive Use Sherwood Ross 
In violation of the US Code and international law, the Bush administration  is spending more money (in inflation-adjusted dollars) to develop illegal,  offensive germ warfare than the $2 billion spent in World war II on the  Manhattan Project to make the atomic bomb. 
So says Francis Boyle, the professor of international law who drafted the  Biological Weapons Anti-terrorism Act of 1989 enacted by Congress. He states  the Pentagon "is now gearing up to fight and 'win' biological warfare"  pursuant to two Bush national strategy directives adopted "without public  knowledge and review" in 2002. 
The Pentagon's Chemical and Biological Defense Program was revised in 2003  to implement those directives, endorsing "first-use" strike of chemical and  biological weapons (CBW) in war, says Boyle, who teaches at the University  of Illinois, Champaign. 
Terming the action "the proverbial smoking gun," Boyle said the mission of  the controversial CBW program "has been altered to permit development of  offensive capability in chemical and biological weapons!" [Original  italics.] 
The same directives, Boyle charges in his book Biowarfare and terrorism  (Clarity Press), "unconstitutionally usurp and nullify the right and the  power of the United States Congress to declare war, in gross and blatant  violation of Article 1, Section 8, Clause 11 of the United States  Constitution." 
For fiscal years 2001-2004, the federal government funded $14.5 billion "for  ostensibly 'civilian' biowarfare-related work alone," a "truly staggering"  sum, Boyle wrote. 
Another $5.6 billion was voted for "the deceptively-named 'Project  BioShield,'" under which Homeland Security is stockpiling vaccines and drugs  to fight anthrax, smallpox and other bioterror agents, wrote Boyle.  Protection of the civilian population is, he said, "one of the fundamental  requirements for effectively waging biowarfare." 
The Washington Post reported December 12 that both houses of Congress this  month passed legislation "considered by many to be an effort to salvage the  two-year-old Project BioShield, which has been marked by delays and  operational problems." When President Bush signs it into law, it will  allocate $1 billion more over three years for additional research "to pump  more money into the private sector sooner." 
"The enormous amounts of money" purportedly dedicated to "civilian defense"  that are now "dramatically and increasingly" being spent," Boyle writes,  "betray this administration's effort to be able to embark on offensive  campaigns using biowarfare." 
By pouring huge sums into university and private-sector laboratories, Boyle  charged, federal spending has diverted the US biotech industry to  biowarfare. 
According to Rutgers University molecular biologist Richard Ebright, over  300 scientific institutions and 12,000 individuals have access to pathogens  suitable for biowarfare and terrorism. Ebright found that the number of  National Institute of Health grants to research infectious diseases with  biowarfare potential has shot up from 33 in 1995-2000 to 497. 
Academic biowarfare participation involving the abuse of DNA genetic  engineering since the late 1980s has become "patently obvious," Boyle said.  "American universities have a long history of willingly permitting their  research agendas, researchers, institutes, and laboratories to be co-opted,  corrupted, and perverted by the Pentagon and the CIA." 
"These despicable death-scientists were arming the Pentagon with the  component units necessary to produce a massive array of .  genetically-engineered biological weapons," Boyle said. 
In a forward to Boyle's book, Jonathan King, a professor of molecular  biology at Massachusetts Institute of Technology, wrote that "the growing  bioterror programs represent a significant emerging danger to our own  population" and "threaten international relations among nations." 
While such programs "are always called defensive," King said, "with  biological weapons, defensive and offensive programs overlap almost  completely." 
Boyle contends the US is "in breach" of both the Biological Weapons and  Chemical Weapons conventions and US domestic criminal law. In February 2003,  for example, the US granted itself a patent on an illegal long-range  biological-weapons grenade. 
Boyle said other countries grasp the military implications of US  germ-warfare actions and will respond in kind. "The world will soon witness  a de facto biological arms race among the major biotech states under the  guise of 'defense,' and despite the requirements of the Biological Warfare  Convention." 
"The massive proliferation of biowarfare technology and facilities, as well  as trained scientists and technicians all over the United States, courtesy  of the Neo-Con Bush Jr. administration will render a catastrophic biowarfare  or bioterrorist incident or accident a statistical certainty," Boyle warned. 
As far back as September 2001, according to a report in the New York Times  titled "US Pushes Germ Warfare Limits," critics were concerned that "the  research comes close to violating a global 1972 treaty that bans such  weapons." But US officials responded at the time that they were more worried  about understanding the threat of germ warfare and devising possible  defenses. 
The 1972 treaty, which the US signed, forbids developing weapons that spread  disease, such as anthrax, regarded as "ideal" for germ warfare. 
According to an article in the Baltimore Chronicle & Sentinel of last  September 28, Milton Leitenberg, a veteran arms-control advocate at the  University of Maryland, said the government was spending billions on germ  warfare with almost no analysis of threat. He said claims terrorists will  use the weapons have been "deliberately exaggerated." 
In March of the previous year, 750 US biologists signed a letter protesting  what they saw as the excessive study of bioterror threats. 
The Pentagon has not responded to the charges made by Boyle in this article. 



