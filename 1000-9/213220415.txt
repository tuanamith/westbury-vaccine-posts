


Dying for free trade Malcolm Cook February 21, 2007 11:31 AM 
 <URL> = de.html 
Last year was a bad one for free trade. The Doha Round was supposed to make agriculture the centrepiece of negotiations to assuage the deep frustrations of developing countries. But, instead of breathing life into free trade in food, rural protectionism in rich countries seems to have killed the Doha Round - and, with it, potentially the whole multilateral trading regime. 
Agriculture has always presented the greatest challenge to free-trade doctrine and its promise of empowering the poor, for it is one of the most distorted areas of global trade. In 2004, OECD countries spent more than four times their official development aid budgets on support for domestic farmers. 
The language of trust Jean-Roger Kaseki February 21, 2007 9:30 AM 
 <URL> = e_a_member_of_th.html 
The Democratic Republic of the Congo is a former Belgian colony. It should have been a country where people could have spoken either Flemish, French or German. But instead, the language chosen by Belgian colonialists in Congo was French. This happened because the Belgium constitution of 1831 guaranteed linguistic liberty, but French became the only official language in Belgium. Soon after Belgium's independence, intellectuals in the Flemish urban centres began to advance language grievances. 
The 19th century Flemish movement was an urban-based phenomenon. But the intransigence of the French-speaking elite radicalised the movement. Under this pressure, language policy in Belgium evolved gradually. The first series of language laws adopted in the late 19th century imposed asymmetrical bilingualism. The most significant measure was the legislation in 1898 that made Dutch an official language on an equal footing with French. 
Faith in selection? Mike Ion February 21, 2007 8:58 AM 
 <URL> = k_in.html 
Back in 2003, in a written submission to the House of Commons education select committee (November 17 2003) the Catholic Education Service (CES) stated that: 
"Catholic schools are truly comprehensive, resisting grammar school routes and welcoming children of every ability" (Section 6) 
Why pandering matters Bradford Plumer February 20, 2007 9:00 PM 
 <URL> = ive_pandering.html 
There's a slightly comical element to the political two-step that John McCain, Rudy Giuliani, and Mitt Romney have all had to perform lately. You know the drill: Former socially-moderate Republicans who, in past lives, have either said unkind things about evangelicals (in McCain's case), supported abortion rights (in Romney's), or, heavens, shacked up with a couple of gay men while weathering a nasty divorce (that would be Giuliani) now have to prove to primary voters that they're really, truly socially conservative. Let the delicate dance begin! 
And dance they do. This week, McCain told a flock of South Carolina Republicans that Roe v Wade "should be overturned", and recently spoke out in favor of abstinence-only education in public schools. Romney has frantically tried to disavow his past as a pro-choice, pro-gay rights governor of Massachusetts. Giuliani, who once had the temerity to speak out against a ban on partial-birth abortion, is now backpedaling so hard he may well break something. 
An extraordinary problem Spencer Ackerman February 20, 2007 7:00 PM 
 <URL> = ry_problem.html 
There wasn't much in the way of public fanfare, but last week was the worst ever for the transatlantic counterterrorism relationship of the post-9/11 era. But don't expect much of anything to change for the better - especially not the underlying reasons for the furor. 
It was a perfect storm of European outrage over US intelligence activities. What started as a European Parliament investigation into a Washington Post report about off-the-books CIA detention facilities on the continent became, on Wednesday, a sweeping indictment of nearly all US-sponsored covert action in Europe. Member states were denounced for "turning a blind eye" to secret arrests, torture, widespread abductions and even hosting "at least 1245" CIA flights into and out of European airports between 2001 and 2005, some number of which are presumed to have ferried terrorism suspects from Europe to holding facilities in Afghanistan and Guantanamo Bay. 
The road to happiness Derek Draper February 20, 2007 6:02 PM 
 <URL> = se_tomorrows.html 
I wanted to organise tomorrow's discussion on the "Politics of Wellbeing" because I believe that a historic and fundamental shift has begun to take place in Britain's national psyche. But it is just the beginning, and we should not pretend otherwise. That is why the debate at the House of Commons - and the one taking place here on Cif - are so important. We are in uncharted waters, and we should all be contributing to the development of our new map. 
The change taking place is, I think, being driven by the combination of three trends. First, there is a growing awareness of the huge prevalence of mental illness in our society, especially regarding the millions of people suffering from emotional problems like depression and anxiety - 1 in 6 by the latest estimates. Lord Layard has taken this issue to the heart of government policy-making. But we also see it illustrated every day in terms of "celebrities" entering rehab; young girls (and now boys) suffering a plague of anorexia and self- harming; and the relentless rise in prescriptions written for anti- depressants. 
The shame of fear Dan Bell February 20, 2007 4:44 PM 
 <URL>  
What is the connection between the execution by firing squad of 300 men for cowardice, and the execution-style murder of a 15-year-old boy in his own home? Answer: The enduring expectation placed upon men to never show fear. 
The shootings in south London and the pardons of the men shot for desertion during the first world war are both expressions of the brutality we tolerate against young men, and the catastrophic shame we impose on them when they show fear. 
Get the message: it's the medium Aric Sigman February 20, 2007 3:45 PM 
 <URL> = .html 
While controversy continues to surround the way the content of screen media affects our thoughts and behaviour, I have just reviewed a growing body of empirical evidence for the academic journal Biologist indicating that watching television causes physiological changes, and not for the better. Most of these effects occur irrespective of the type of programme people watch - whether it's Reservoir Dogs or the Teletubbies. It is the medium, not the message. 
Reviewing 35 studies in well-respected scientific and medical journals, I identified 15 biological and cognitive effects linked to levels of television exposure. There was a dose-response relationship: both the average number of hours watched and the age at which a child begins watching television are central to the association with negative effects later on. 
Those in power are right to see multiculturalism as a threat  <URL>  
Diversity isn't dangerous because it breeds suicide bombers, but because the state depends on a tight cultural consensus 
Terry Eagleton Wednesday February 21, 2007 The Guardian 

There is an insuperable problem about introducing immigrants to British values. There are no British values. Nor are there any Serbian or Peruvian values. No nation has a monopoly on fairness and decency, justice and humanity. Some cultures cherish one kind of value more than others do (Arabs and hospitality, for example, or the British and emotional self-discipline), but there is nothing inherently Arab about hospitality, or inherently British about not throwing a hysterical fit. Tolerance and compassion, like sadism and supremacism, can be found anywhere on the planet. 
This is a fork in the road  <URL>  
Control orders for terror suspects were meant to be a temporary measure. They should not be extended 
Nick Clegg Wednesday February 21, 2007 The Guardian 

Can you fight terror with justice? Tomorrow the Commons will vote on the annual extension of control orders, under which terror suspects are subject to a range of measures from tagging to virtual house arrest. The government may seek to portray this as simply an administrative vote. In truth, it marks a fork in the road in the debate on how to tackle terrorism. 
Those of us who reluctantly accepted the introduction of control orders in emergency legislation two years ago did so following assurances that they were a temporary measure, introduced in response to the ruling that the detention of foreign nationals without trial was illegal. The government promised to review the orders, and replace or improve them after 12 months. In the wake of the bombings on July 7 2005, we accepted that an extra year would be needed to complete that review. 
The west may yet come to regret its bullying of Russia  <URL>  
Putin has no interest in a new cold war and is struggling to modernise his economy. Yet he is rebuffed and insulted 
Simon Jenkins in Moscow Wednesday February 21, 2007 The Guardian 

We can only tackle teenage gangs with the help of young people  <URL>  
Empowerment is a key element in changing an unacceptable culture, says Teddy Gold 
Wednesday February 21, 2007 The Guardian 

Joseph Harker identified several pertinent questions following the teenage killings in south London (This isn't about guns, February 16). Some answers can be found through experience gained in Liverpool following the Toxteth riots of 1981. Harker says "we need to quickly find answers as to why urban youth culture, as also witnessed in Manchester and Nottingham, has become so violent". We found that when children grow up on a housing estate where gang culture has taken a hold, and where the adult residents have no organisation, gangs gain control. Young people have little choice. They belong or are seen as outsiders and do not come under the protection of the gang. As such they become vulnerable. 
'There's a lot left to be done'  <URL>  
Before the formal handover to the Iraqis, Paul Bremer, the US viceroy in Baghdad, insisted the country was 'fundamentally changed for the better' by the occupation. But assassinations, car bombs and attacks on the country's oil supply told a different story, says Rajiv Chandrasekaran in the final exclusive extract from his new book 
Wednesday February 21, 2007 The Guardian 

In the summer of 2004, as Lewis Paul Bremer III, America's viceroy to Iraq and head of the Coalition Provisional Authority (CPA), which had run Iraq's government for 14 months, prepared to depart, electricity generation remained stuck at around 4,000 megawatts. This resulted in less than nine hours of power a day to most Baghdad homes - instead of the 6,000 megawatts he had pledged to provide. The new army had fewer than 4,000 trained soldiers, a third of what he had promised. Seventy per cent of police officers on the street had not received any CPA- funded training. Attacks on American forces and foreign civilians averaged more than 40 a day, a threefold increase since January. Assassinations of political leaders and sabotage of the country's oil and electricity infrastructure occurred almost daily. In a CPA- sponsored poll of Iraqis taken a few weeks before the handover of sovereignty, 85% of respondents said they lacked confidence in Bremer's occupation administration. 
Iran shrugs off sanctions threat as nuclear talks end in stalemate  <URL>  
=B7 UN will declare country in breach of resolution =B7 US aircraft carrier arrives in Gulf in show of strength 
Julian Borger in Vienna Wednesday February 21, 2007 The Guardian 
Iran will today be declared in violation of a UN resolution calling for a halt to its enrichment of uranium, after last-minute negotiations in Vienna failed to reach a compromise in the nuclear stand-off. 
Ari Larijani, Iran's chief nuclear negotiator, emerged from talks with Mohamed ElBaradei, the director general of the International Atomic Energy Agency (IAEA), insisting that Iran had a right to pursue a peaceful nuclear programme and warning against any use of force to stop it. 
Brain cells clue to genius of Einstein  <URL>  
Ian Sample and John Hooper in Rome Wednesday February 21, 2007 The Guardian 
Scientists may be a step closer to understanding one of the most brilliant minds ever to grace the field, that of Albert Einstein, the man who unravelled the mysteries of the atom. 
Researchers at Lausanne University identified an unknown role for a type of brain cell which Einstein is thought to have had in more copious supply than the average male. The scientists said the cells provide energy for neural circuits and help build connections, leading to a more complex brain structure. 
Robert Fisk: Lebanon will be first victim of Iran crisis  <URL>  
Published: 21 February 2007 
HIV: Is this the breakthrough?  <URL>  
An antibody neutralises the world's most destructive virus - Steve Connor reports on how scientists may at last have the key to defeating Aids 
Published: 21 February 2007 
It could be one of the most important discoveries in the 20-year history of research into an Aids vaccine. Scientists have captured an image of the crucial moment when the human immunodeficiency virus can be blocked from gaining entrance to the human cells it ravages. In the words of the American research agency behind the study, the findings could have "profound implications" for the design and development of a future Aids vaccine. 
The importance of the study, published in the current issue ofNature, should not be underestimated. "Creating an HIV vaccine is one of the great scientific challenges of our time," says Elias Zerhouni, director of the US National Institutes of Health. "NIH researchers and their colleagues have revealed a gap in HIV's armour and have thereby opened a new avenue to meeting that challenge." 


