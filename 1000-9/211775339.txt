



 <URL>  
Polio cases jump in Pakistan as clerics declare vaccination an American plot 

· Rumours leave thousands of children unprotected · Aid workers increasingly targeted by tribal militants 
Declan Walsh in Peshawar Thursday February 15, 2007 The Guardian 
A child is given the vaccine at a Peshawar hospital A child is given the vaccine at a Peshawar hospital. Some mothers have to smuggle their children in. Photograph: Declan Walsh   
The parents of 24,000 children in northern Pakistan refused to allow health workers to administer polio vaccinations last month, mostly due to rumours that the harmless vaccine was an American plot to sterilise innocent Muslim children. 
The disinformation - spread by extremist clerics using mosque loudspeakers and illegal radio stations, and by word of mouth - has caused a sharp jump in polio cases in Pakistan and hit global efforts to eradicate the debilitating disease. 
Article continues The World Health Organisation (WHO) recorded 39 cases of polio in Pakistan in 2006, up from 28 in 2005. The disease is concentrated in North-West Frontier Province, where 60% of the refusals were attributed to "religious reasons". 
"It was very striking. There was a lot of anti-American propaganda as well as some misconceptions about sterilisation," said Dr Sarfaraz Afridi, a campaign manager with the WHO in Peshawar. 
The scaremongering and appeals to Islam echoed a similar campaign in the Nigerian state of Kano in 2003, where the disease then spread to 12 polio-free countries over the following 18 months. Pakistan is one of just four countries where polio remains endemic. The others are Nigeria, India and Afghanistan. 
The North-West Frontier Province government made strenuous efforts to counter talk of an "infidel vaccine". Health workers fanning across the province last month were equipped with copies of a fatwa, or religious order, endorsing the vaccinations and signed by Maulana Fazlur Rehman and Qazi Hussain Ahmed, the leaders of Pakistan's most powerful religious parties. 
The move reassured many doubters. More than 5.7 million children were vaccinated in January, with another 3 million targeted in a second round due to start next Tuesday. "The elephant is over. We are left with just the tail," said Dr Afridi. 
But the tail has a deadly sting. Even though only 24,000 children missed the vaccine, the WHO officials said failure to vaccinate in small pockets of the country gave the virus a fresh toehold to spread. 
The vaccination struggle is entangled with the confrontation between the government and powerful militants in the tribal areas. Refusals were highest in areas where conservative clerics and self-styled "Pakistani Taliban" fighters hold sway, flouting government authority and making their own strict laws. 
Almost 2,000 children were not vaccinated in Bajaur, a tribal agency on the Afghan border where US warplanes bombed a house last year in the hope of killing al-Qaida's No2, Ayman al-Zawahiri. The jets missed their target but inflamed extremist sentiment. Recently militants ordered Bajaur's barbers to stop shaving beards on the grounds that it was "un-Islamic". The barbers complied. 
In nearby Swat Valley, a young firebrand cleric, Maulana Fazlullah, denounced the polio campaign through a local FM radio station. His brother was killed in a Pakistani army attack on a madrasa, or Islamic school, late last year. Almost 4,000 children were not vaccinated in Swat. 
Imran Khan, of the Human Rights Commission of Pakistan, said: "Some people feel they are under attack here ... That is clouding their attitudes." 
Demands for "assistance" from local officials and elders was the other major factor behind the refusals. In the Mohmand tribal agency, policemen demanded their salaries before allowing vaccination to proceed. Other villagers asked for money or the release of criminals from jail. 
"Demand" refusals accounted for about one-third of cases, the WHO said. 
But some brave women were uncowed by the extortion or demagoguery. Up to 200 babies a day are vaccinated at the Khyber teaching hospital in Peshawar, where burka-clad women arrive with children in their arms. Some arrive in secret, slipping into the clinic in defiance of male relatives who oppose vaccination. "One woman told me, 'My husband is illiterate. He has no idea how important this vaccine is,'" said Muhammad Islam, a male nurse. 
Aid workers fear they are being pushed into the frontline of the struggle between the government and tribal militants, some linked to the Taliban and al-Qaida. Last weekend a grenade was lobbed into a Red Crescent compound in Peshawar, damaging vehicles but killing nobody. 
Some linked the attack to a fatwa issued in Dara Adam Khel, a lawless town famous for its gunsmiths, just before Christmas. A cleric named Mufti Khalid Shah declared a fatwa on employees of the UN, WHO and all other foreign organisations. "Killing their employees is in line with the teachings of jihad in Islam," said a notice. 
"We are very worried," said Mr Khan, of the Human Rights Commission. "You have to be very careful about admitting to working for an NGO these days." 
Recently aid workers in Bannu, near North Waziristan, were sent a letter and a 500 rupee (£4.50) note, he said. "The letter said they had a choice. They could either stop work or buy their own coffin." 
Backstory 
Poliomyelitis is an acute viral infection of the nervous system. Worldwide more than half of infections are in children under five. One in two hundred infections leads to permanent paralysis, usually in the legs. In 5-10% of these cases the victims die when the breathing muscles are paralysed. 
Since the launch of the Global Polio Eradication Initiative in 1988 the number of reported cases worldwide has fallen from 350,000 to 1,968 - a decrease of over 99%. Today it remains endemic in four countries: Nigeria, India, Afghanistan and Pakistan. In 1988 affected countries numbered 125. While there remains no cure for polio the progress towards its eradication is due to widespread use of polio vaccines. By 2002 the WHO had certified 124 countries polio-free. 
More than 2 billion children have been immunised against the disease since 1988. The WHO estimates that because of the initiative five million fewer people have been paralysed by the disease. 
Source: WHO 


