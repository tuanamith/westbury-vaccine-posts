


On Feb 9, 4:20 am, "Rupert" < <EMAILADDRESS> > wrote: 

An opinion is not an argument. 

Uh, this supports my point. I said 'it's harder to persuade people of circumcision than it is of vaccination'. 

This is rather vague, isn't it. Can you commit yourself to saying anything specific about mandatory vaccination? I want yoou to say something about the HPV vaccine, I know you don't want to as it would hurt your case against circumcision. 

Well, do you agree with this? 

Sigh. You just asked me what age I would set, thereby asking me to draw such a distinction as you're now claiming can't be done! 

No comment here, either. I guess you've accepted my statement that anti-circ beliefs dominate public discourse. 

I have explained my beliefs. I am not going to repeat them endlessly. 

I have no doubt that you have no sources for that claim. 

Do you really suppose that most uncircumcised men dwell on the issue? And that they would do so never hearing anti-circ stuff? 

I can't believe you really think that an appropriate analogy. Look, there's a reason why I compared it to smoking and not to a purely matter of taste like that! 

OK, try another one. What do you say to someone that refuses to wear a seat-belt while driving and stoutly defends that by saying that they are fully aware of the risks and still choose not to wear a seat-belt? Most intelligent people would call that irrational, and say our driver is very likely either not fully aware of the risks or in denial. Same thing about smoking, right? 

Then why are you so afraid of them? 

If you truly believed that 'bodily integrity' trumps everything, you would oppose even life-saving surgery done without the patient's explicit consent. Since I know you wouldn't, you clearly place _some_ limitation on the right, only to a different extent than I would. 
Andrew Usher 


