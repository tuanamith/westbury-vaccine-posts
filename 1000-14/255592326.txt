


On Tue, 03 Jul 2007 21:10:23 -0500, Don Kresch wrote: 

Oh no, I'm sure god will provide. 
<eye roll> 
Evolutionary psychology is giving us better and better insight into how  we work. And I'm much more interested in systems based on reality than  something out of a "think tank".   
Uh huh. Because I cling to ideology no matter what and refuse to explain  how things would work? 
Why are you avoiding explaining how a tort system would deal with the  issues? 

Sorry, I no longer buy the "pure free market" idea. That would, for  example, entail legalizing all medications across the board.  
We *already are facing health risks (all of us, you included) because  people overuse anti-biotics. If they could just buy them OTC, we'd be in  even worse shape. 
People *do need to educate themselves and participate in their health  care (which they are not doing well at all now) but we're not born  experts.  
I do not believe you have any "right" to buy and use whatever anti- biotics you feel like because you can damage *my health. And there is no  way at our current level of technology for me to isolate myself from your  stupidity. If there was, you might have an argument but we just don't  have that kind of thing. Your health effects my health. 
And I'm not even talking about a regieme that would *compell you, I'm  talking about being willing to pay to make basic health care *available  to you and everybody else.   
I also deny the profit motive works for health care. Relying on profit  motive has led us to a situation in which vaccines are being abandoned  but boner pills are widely available. It's more profitable to make pills  that make dicks hard than it is to improve the general health of the  entire population by providing cheap vacinations. 
Many vaccines would simply vanish from the face of the earth if  governments weren't buying them. Especially because cheap, effective  things are quite often beyond their patent protection. 
Personal example. I refuse to take anything more "advanced" than  erythromyacin for ordinary infections. It's $10 a bottle (at most) and  effective in most all cases. But it's generic, everybody and his dog can  make it, and it's not all that profitable. So pharma pushes expensive  "fourth generation" anti-biotics which are swatting flies with bazookas  and weakening immune systems all over the country. 
It's insane. I know people in my family who almost *died because they had  been taking the Latest And Greatest (that is, patented and profitable)  anti-biotics for minor infections. So when something serious came along,  there wasn't anything left to go "up the scale" in power. 
I almost lost two of my relatives because of this crap. 
The incentives are fucked up. And our patent system is beyond belief.  
Another personal example. I had acid reflux years ago. Prilosec was about  to go generic (and eventually OTC). I started taking it and haven't had a  problem since. But the patent, you see, expired. 
So the company "reformulated" and twiddled Prilosec a trivial amount and  introduced the patented Nexium and began pushing it, and hard, with  physcians and insurance companies. 
Nexium--which *is Prilosce--at the Walgreen's pharmacy site, is $5.40 per  pill. Prilosec, at the same location, runs 71.4 cents per pill. 
Wanna know why your insurance is getting so pricey? There's one example.  We're talking seven and a half times more money for what amounts to the  same medication. 
(By the way, in a purely free market, where do patents fit? They are,  after all, monopoly grants enforced by the power of the state.) 
This is widespread in the industry now. Patent tricks are being used to  extend the profits on medications that have gone generic or even OTC. The  profit motive drives pharma to push people to take as many high margin  pills as possible. 
Preventive care, by the way, is the most cost effective (as in vacinating  every child in the country). But it is also the least profitable. 
How do you make this work? How do you make the incentives be health over  high margin medications? 
I don't know.  

Are you *sure you're an atheist? I actually went and double checked that  you had an a.a. number. 
Of course I believe in moral relativism. It *exists. The reason we have  the whole relativism concept is anthropologists could not find "moral  absolutes". As in people went looking and they just weren't there. A lot  like that god thingie. 
On the *other hand, evolutionary psychology is beginning to identify  human universals. There is evidence they exist but they are far more  subtle than simple "moral absolutes". There do appear to be universals  underlying our moral systems but they can be expressed in widely variable  ways.  
Again, I'm utilitarian. Science is starting to uncover how we humans  work. Ideology is, to my mind, the equivalent of debating how many angels  can dance on the head of a pin. I want research and evidence. 

Haven't I seen you quite freely tossing about the phrase "strawman"? 
Did I *say "government is supposed to always solve it"? Where?    
I accept responsibility for everything I write. Why? 
You're being awfully evasive. I scroll back up and find: 

But you've only replied with vague comments about "self-ownership" and  "right to the property of others". 
What does that have to do with the question of whether you would survive  financially if a pandemic triggers a severe recession or even depression? 

