


Job Title:     Manager QA Job Location:  NY: Pearl River Pay Rate:      competitive Job Length:    full time Start Date:    2007-07-10 
Company Name:  Wyeth Contact:       Wyeth Phone:         email only Fax:           email only 
Job Summary 
Responsible for ensuring final dispositions are assigned to all incoming materials received for use in vaccines operations (e.g. raw materials, packaging components, critical processing aids, cleaning agents, etc). Responsible for ensuring QA review of supplier/manufacturer and testing documents to assure that the material has met release specifications.  Coordinates the efforts of Quality Assurance Specialists to ensure timely review and disposition of materials to meet Materials Managements deadlines and increase customer service. Tracks the progress of investigations and confirms that the scope of the problem is accurate and the root cause analysis has been adequately investigated. Follow up to ensure that all commitments to prevent reoccurrence are completed.  Follows and works in accordance with company policies, SOPs and cGMP standards to ensure compliance with all applicable regulations. 
Job Responsibilities 
*Ensures Disposition and dating periods are assigned to incoming materials (e.g. raw materials, packaging components, critical processing aids, cleaning agents, etc). *Identifies complex technical problems and provides guidance and support to staff members for quality systems development, implementation and maintenance efforts to ensure alignment with departmental business process needs ad user requirements. *Attend production planning meetings to communicate status and determine priorities responsible for ensuring record/review disposition, the timely review of all documentation relating to the manufacturing process and to ensure compliance with cGMPs and company/standards. *Develop and maintain a responsive action oriented and value added organization by recruiting skilled personnel, providing internal cross training and external training opportunities, and exposing staff to all aspects of the business via multi-disciplinary team meetings. *Author/review/Approve SOPs as needed.  Author, edit, review and approve documents as required (e.g. NOVA manage, change controls, monographs) 
Basic Qualifications 
*A minimum of a BS/BA in Biology, Microbiology, Chemistry, Biochemistry or other Life Sciences. *In depth knowledge in cGMPs, management training and quality assurance.  Technical expertise in the manufacturing of biological products would be beneficial. *Previous experience in QA or QC required with some time spent in compliance.  Previous experience in biological or aseptic processing beneficial.  A minimum of 4-8 years is required with at least 3 years spent in product release or compliance. 
For more information and to apply online, please visit us at: www.wyeth.com/careers 
Wyeth is an Equal Opportunity Employer, M/F/D/V. 
Search Firm Representatives: 
Please Read Carefully. 


Please refer to Job code 17190 when responding to this ad. 

For FASTEST PROCESSING of your resume, please visit  <URL>  to apply online. 



