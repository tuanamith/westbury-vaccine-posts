


On Jul 5, 12:47 am, Jim Chandler < <EMAILADDRESS> > wrote: 
Jim...maybe you can remember this as well. 
KING: What are you saying in "Sicko?" 
MOORE: I'm saying that we have a broken health care system and -- 
KING: Was it ever fixed to be broken? 
MOORE: Well, I think -- I remember as a child -- and I'm sure you remember -- many years ago people didn't have to worry if they got sick. They were able to go to the doctor. They could afford it. 
Do you remember in your time anybody actually going bankrupt because of medical bills? 
MOORE: I remember house calls. 
MOORE: House calls, yes. 
Well, now they take your house away. It's the number one cause of bankruptcy in the United States now, medical bills. And it's the number one cause of homelessness. 
How did we get that way? 
Because it wasn't that way. And pharmaceutical companies and people that used to invent medicines -- I mean Jonas Salk, back in the '50s, invented the polio vaccine. They said to him, aren't you going to patent that, you know, you could make a lot of money? 
He said no, I'm not going to patent it. That would -- that would be immoral. This belongs to the people. 
The guy who invented the kidney dialysis machine, he wouldn't patent it. He said this belongs to the people. I'm giving this to society. 
And that's the way people used to think. 


