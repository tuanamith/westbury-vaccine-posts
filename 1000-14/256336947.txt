



James Madden July 05, 2007 
THOUSANDS of patients of a Sydney doctor are being urged to be tested for blood-borne viruses, including HIV, after three women contracted hepatitis C as a result of poor infection control at the eastern suburbs clinic. In March this year, it was revealed that the three women - who had visited Daniel Hameiri's general practice clinic for mineral or vitamin injections - had been diagnosed with hepatitis C. 
The clinic in upmarket Double Bay was closed and an investigation launched. 
The NSW Health Department said yesterday the spread of the virus was probably due to inadequate infection control practice at the clinic between November 2004 and January this year. 
Although the clinic was allowed to reopen, 1975 patients who received injections at the clinic during that period were yesterday notified that they should be tested for hepatitis C, hepatitisB and HIV. 
"Although the risk is low, we also want to encourage anyone else who has received injections at this clinic to contact their doctor," NSW Health Minister Reba Meagher said. 
The director of public health for South Eastern Sydney and Illawarra Area Health Service, Mark Ferson, said it was still not clear how the women had contracted hepatitis C. 
The disease could have been spread by a drop of infected blood on a tourniquet being transferred on to another patient, he said. 
"That tourniquet might have a drop of blood from someone on it that was infected and then placed over the wound of the next person - that's one of the theoretical possibilities," Professor Ferson said. 
He agreed that such a scenario represented a failure to comply with basic medical hygiene practices. 
There is no vaccine for hepatitis C, which can cause liver damage and liver failure over many years. 
Professor Ferson said Dr Hameiri had co-operated fully with the investigation and had closed his clinic to upgrade his infection control procedures. 
Dr Hameiri's clinic was inspected prior to reopening. 
The infection control knowledge and practice of clinic staff were found to be up to standard. 
The director of communicable diseases for NSW Health, Jeremy McAnulty, said Dr Hameiri had been referred to the Medical Board and Health Care Complaints Commission, with a further determination to be made by those two bodies. 
Dr Hameiri was unavailable for comment yesterday. 



