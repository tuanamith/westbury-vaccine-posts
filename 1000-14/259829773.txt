


By ANITA CHANG, Associated Press Writer Tue Jul 3, 6:45 AM ET 
BEIJING - Chinese inspectors found excessive amounts of additives and  preservatives in dozens of children's snacks and seized hundreds of bottles  of fake human blood protein from hospitals, officials said Tuesday. 
China's dismal health and safety record - both within and outside its  borders - has increasingly come under the spotlight as its goods make their  way to global markets. Major buyers like the United States, Japan, and the  European Union have pushed Beijing to improve inspections. 
China accused the media of hyping the problems. 
"I think it would be better if the media would stop playing up this issue,"  Foreign Ministry spokesman Qin Gang told reporters. 
"China has taken measures and enacted relevant legislation regarding  inspection and monitoring of its food export process. China has been very  responsible in this regard to ensure the good quality and safety of its  exports," he said. 
Inspectors in southwest China's Guangxi region found excessive additives and  preservatives in nearly 40 percent of 100 children's snacks sampled during  the second quarter of 2007, according to a report on China's central  government Web site. 
The snacks - including soft drinks, candied fruits, gelatin desserts and  some types of crackers - were taken from 70 supermarkets, department stores  and wholesale markets in seven cities in the region, it said. 
Only 35 percent of gelatin desserts sampled met food standards, the report  said, while two types of candied fruit contained 63 times the permitted  amount of artificial sweetener. 
The report did not say whether any snacks were recalled or if any  manufacturers faced discipline. Calls to the Guangxi Industrial and  Commercial bureau rang unanswered Tuesday. 
Some 420 bottles of fake blood protein, albumin, were found at hospitals in  Hubei province but none had been used to treat patients, Liu Jinai, an  official with the inspection division of the provincial food and drug  administration, said in a telephone interview. No deaths or illnesses were  reported. 
A shortage of albumin triggered a nationwide investigation in March into  whether fakes were being sold. 
A state media report last month centered on an inquiry in the northeastern  province of Jilin, where 59 hospitals and pharmacies sold more than 2,000  bottles of counterfeit blood protein. One person died from use of the fakes,  state media said. 
Albumin is a primary protein in human plasma that is important in  maintaining blood volume. It is used to treat conditions including shock,  burns, liver failure and pancreatitis, and is needed by patients undergoing  heart surgery. 
Chinese authorities have struggled with recalls following the widespread  sale of fake polio vaccines, vitamins and baby formula. Such incidents  threaten both public health and faith in the government's ability to control  crime and corruption and ensure safety of food and drug supplies. 
In May, the country's former top drug regulator was sentenced to death for  taking bribes to approve substandard medicines, including an antibiotic  blamed for at least 10 deaths. 
Fears that China's chronic food safety problems were going global surfaced  earlier this year with the deaths of dogs and cats in North America blamed  on Chinese wheat gluten tainted with the chemical melamine. 
U.S. authorities have also turned away or recalled toxic fish, juice  containing unsafe color additives and popular toy trains decorated with  leaded paint. Chinese-made toothpaste has also been banned by numerous  countries in North and South America and Asia for containing diethylene  glycol, or DEG, a toxic ingredient more commonly found in antifreeze. 
Beijing has striven to appear active in cleaning up problem areas.  Inspectors recently announced they had closed 180 food factories in China in  the first half of this year and seized tons of candy, pickles, crackers and  seafood tainted with formaldehyde, illegal dyes and industrial wax. 



