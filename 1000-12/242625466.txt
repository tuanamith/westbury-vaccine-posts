


 <URL>  Oral sex linked to throat cancer 
Scientists looked at tissue samples from patients A virus contracted through oral sex is the cause of some throat cancers, say US scientists. 
HPV infection was found to be a much stronger risk factor than tobacco or alcohol use, 
the Johns Hopkins University study of 300 people found. 
But experts said a larger study was needed to confirm the findings. 
 It is important for health care providers to know that people without the traditional risk factors of tobacco and alcohol use can nevertheless be at risk of oropharyngeal cancer 
Dr Gypsyamber D'Souza, study author 
The Johns Hopkins study took blood and saliva from 100 men and women newly diagnosed with oropharyngeal cancer which affects the throat, tonsils and back of the tongue. 
They also asked questions about sex practices and other risk factors for the disease, such as family history. 
Those who had evidence of prior oral HPV infection had a 32-fold increased risk of throat cancer. 
Risk factors 
There was no added risk for people infected with HPV who also smoked and drank alcohol, 
suggesting the virus itself is driving the risk of the cancer. 
Oral sex was said to be the main mode of transmission of HPV but the researchers said mouth-to-mouth transmission, 
for example through kissing, could not be ruled out. 
Study author Dr Gypsyamber D'Souza said: 
"It is important for health care providers to know that people without the traditional risk factors of tobacco and alcohol use can nevertheless be at risk of oropharyngeal cancer." 
Co-researcher Dr Maura Gillison said previous research by the team had suggested there was a strong link. 
But she added: 
"People should be reassured that oropharyngeal cancer is relatively uncommon and the overwhelming majority of people with an oral HPV infection probably will not get throat cancer." 
But whether the vaccine would protect against oral HPV infection is not yet known. 
Dr Julie Sharp, science information officer at Cancer Research UK, said: 
"There is conflicting evidence about the role of HPV, and this rare type of mouth cancer. 
"As this was a small study, further research is needed to confirm these observations." 








