


Oral sex can lead to throat cancer ANDR=C9 PICARD 

May 10, 2007 at 9:38 AM EDT 
The same virus that causes cervical cancer is the principal cause of throat cancer, according to a new study. 
The research also suggests that unprotected oral sex is a major reason people are contracting throat cancer - not just smoking and excessive alcohol consumption, as previously believed. 
"It's the human papillomavirus that drives the cancer," said Maura Gillison, assistant professor of oncology and epidemiology at Johns Hopkins University in Baltimore, Md., and lead author of the study. 
She said the more oral-sex partners a person has, the greater the risk of contracting oral cancers (located in the tonsils, back of the tongue and throat). The good news is that the risk remains low over all. 
"People should be reassured that oropharyngeal cancer is relatively uncommon, and the overwhelming majority of people with an oral HPV infection probably will not get throat cancer," Dr. Gillison said. 
A new vaccine protects against infection by several strains of HPV, including the one associated with oral cancer, HPV-16. However, Dr. Gillison said it has not been specifically tested for its effectiveness against oral cancer. 
The new research is published in today's edition of the New England Journal of Medicine, which features several articles about HPV and the effectiveness of the vaccine. 
Sold by Merck Frosst Canada Ltd. under the brand name Gardasil, the vaccine prevents infection by four strains of human papillomavirus that account for roughly 70 per cent of cases of cervical cancer. 
Gardasil offers protection that lasts for at least three years, according to newly published data in the New England Journal of Medicine. 
Another study, published today in the journal, suggests the vaccine protects against cancer of the vulva and cancer of the vagina, which are principally caused by HPV. (Penile cancer is also caused largely by the virus, although the effectiveness of the vaccine in men has not yet been demonstrated.) 
In the most recent federal budget, the Conservative government set aside $300-million for an HPV vaccination program, which would target girls aged 9 to 11. 
Health groups are divided on the wisdom of an immunization program, given the cost of the vaccine (more than $400 for the required three doses) and the limited data on its long-term effectiveness. There is also debate about whether vaccination should be limited to girls, and whether the vaccine should be offered to older teens who are already sexually active. 
The new research should help assuage fears a bit, and broaden the appeal of the vaccine because of its effectiveness against other types of cancer. 
About 1,350 women will be diagnosed with cervical cancer in 2007, and an estimated 390 will die. By contrast, approximately 3,200 Canadians will be diagnosed with oral cancer this year, and 1,100 will die, according to the Canadian Cancer Society. 
Dr. Gillison's study involved 100 men and women who were newly diagnosed with oral cancer. They were compared to 200 similar people without cancer. 
Dr. Gillison and her team found that those with HPV infection were 32 times more likely to have developed cancer. By comparison, the risk increased threefold for smokers and twofold for drinkers. 
Study participants who reported having oral sex - be it fellatio or cunnilingus - with six or more partners were at greatest risk of contracting oropharyngeal cancer. 
There is no screening test for oral cancer; it is usually detected when there is a sore in the mouth that does not heal. 

 <URL> = ry/specialScienceandHealth/home 
Of course, he will get a sanitized version from LifeSite news that forgets to mention reduced risk through safe sex. 


