


Neither an abortion nor an unplanned childbirth is palatable. 
Once a woman's pregnant, the damage has been done. 
In the abortion debate, both sides should focus on the use of birth control, before a pregnancy occurs. 
If people use two forms of birth control there is virtually no risk of pregnancy. 
Hormonal & Condoms Hormonal & Pull-Out Hormonal & Diaphragm Condoms & Pull-Out Condoms & Diaphragm Diaphragm & Pull-Out 

Risk of accidental pregnancy BEST case scenario assuming PERFECT use              Per Year (assuming 100 sex acts)    Per Act No protection  8 in 10 (1 in 1.25)                   1 in 125 Condoms                      1 in 50                      1 in 5,000 Birth Control                 1 in 333                    1 in 33,300 Condoms & Birth Control 1 in 1,665,000      1 in 166,500,000* 

Given 100,000,000 women of child bearing age (ages 14-59*), 
Unplanned pregnancies result: 100,000,000/1,665,000= 60 

Risk of accidental pregnancy WORST case scenario assuming TYPICAL use         Per Year (assuming 80 sex acts)        Per Act No protection  8 in 10 (1 in 1.25)              1 in 125 Condoms                        1 in 6.6               1 in 533 Birth Control                    1 in 12.5            1 in 1,000 Condoms & Birth Control 1 in 6,666          1 in 533,333 

533*1000=533,333 533,333/80=6,666 
Unplanned pregnancies result: 100,000,000/6,666=15,000 

Comparisons of pregnancy rates of different contraceptives are from:  <URL>  ; scroll down to "Table 2" 
Risk of accidental pregnancy WORST case scenario assuming TYPICAL use of the 2 riskiest methods                                     Per Year (assuming 80 sex acts)        Per Act Diaphragm                               1 in 6.25                                   1 in 500 Pull-Out                                    1 in 3.7                                     1 in 296 Diaphragm & Pull-Out               1 in 1,850                                 1 in 148,000 
Calculation is 148,000/80=1,850 
As you can see, even with combining the two riskiest methods of birth control, you are still  safer than if you get a vasectomy - 1 in 1,000 per year or a tubal ligation - 1 in 200 per year. 
Unplanned pregnancies result: 100,000,000/1,850=54,054 

It's just like rolling two 30 sided die.  The chance that one comes up with a negative number is 1 in 30.  The chance that both come up with the one negative number is 1 in 900. If you roll each die 10 times, the chance that one comes up with a negative number is 10 in 30 or 1 in 3.  The chance that both come up with a negative number is 10 in 900 or 1 in 90.  Not 10 in 9. 
Chance of getting 1 negative number out of 30, rolling two 30 sided die                            Per Set (assuming 10 rolls)        Per roll Die 1                            1 in 3                                 1 in 30 Die 2                            1 in 3                                 1 in 30 Die 1 & 2 combined     1 in 90                               1 in 900 - chance both come up negative 

Currently there are an average 1 million abortions per year  <URL>   <URL>  
48% of all U.S. pregnancies (and 78% of teen pregnancies) are unplanned That's roughly 2,500,000  <URL>  
And 4 million new babies are born per year  <URL>  
The below statistics give you an idea of how much safer contraception makes us, and when compared to the numbers above, specifically how much safer two forms of contraception make us.  Of course, not all women are having sex, so all estimates of unplanned pregnancy above and below are exaggerated. 

unplanned pregnancies using two forms of birth control (perfect use): 60 (typical use): 15,000 unplanned pregnancies result with no contraception: 100,000,000/1.25 = 80,000,000 unplanned pregnancies result with condoms alone (perfect use): 100,000,000/50 = 2,000,000 (typical use) 100,000,000/6.667 = 15,000,000 unplanned pregnancies result with birth control alone (perfect use): 100,000,000/333=  300,300 (typical use) = 100,000,000/12.5 = 8,000,000 

The reason sex has often been proclaimed to be immoral is specifically because it takes a risk with another person's life.  The baby's. Unsafe sex is immoral.  I have shown above that safe sex is no more immoral than transporting your baby is a car.  People transport their children in cars, without their "informed consent" and we assume that nearly all children when older approve of this, thus I conclude the unborn baby accepts the small risk of being conceived unplanned in exchange for a sex life, just as we accept the risk of driving in exchange for the convenience of travel. 
But failing to promote and provide birth control is one of the most immoral things in the world.  Of the 4 million babies born per year, 1.5 million, or 38% of all newborns are unplanned.  This must surely have a demonstrative effect upon the socio-economic status of our society. 



-------------- STD INFORMATION: 
10 most common STDs 
Type of disease: 
Herpes - Skin / Virus Warts/HPV - Skin / Virus HIV - Blood / Virus Molluscum Contagiosum - Skin / Virus 
Gonorrhea - Bacteria Chlamydia/NGU - Bacteria Trichomoniasis - Bacteria Crabs - Skin / Parasite Scabies - Skin / Parasite Syphilis - Bacteria 

You can get tested for at least four of these, Gonorrhea-urine, Chlamydia-urine, Syphilis-blood, and HIV-blood.  A Trichomoniasis test requires a wet mount or a culture. There are blood tests for Herpes, but they aren't normally given. You can check your partner's genitals for the five others; Herpes, Warts, Molluscum Contagiosum, Crabs, and Scabies.  However it's possible that these diseases could be present even if not noticeable. 

The last six on the list are entirely curable and condoms provide some protection against nearly all of them.  If you are sexually active and not monogamous, you should have a test for Gonorrhea and Chlamydia every six months because they may not have symptoms, and if left untreated, they could cause PID Pelvic Inflammatory Disease in women and epididymitis in men which can cause infertility.   At the same time you may be tested for Syphilis and HIV which are relatively uncommon.  Syphilis is curable, but can cause serious conditions if not treated.  The risk of HIV is decreased by 90% if you wear a condom.  The risk is even less if your partner has had a  recent HIV test.  There is now a new vaccine against Warts/HPV which decreases the risk of genital warts by 90%.  Genital Warts and Molluscum Contagiosum are not curable, but treatable, and the body generally clears theses viruses within five years or sooner.  Hopefully there will soon be a vaccine for Herpes. 
If you choose to maintain multiple long term relationships, or open relationships, tell your partner this before you have sex with them and then they won't be pissed at you. 
Two others: Bacterial Vaginosis is _not_ an STD but may occur in women caused by something new in the vagina such as penis which may upset the normal balance of the vagina. 
And Hepatitis B there is a vaccine you may consider getting, and there is a blood test. 
A condom decreases the risk of these two as well.  The crown skinless skin condom feels as good as condomless sex you can buy it here:  <URL>  
Here's a good website with info on STD's:  <URL>  


