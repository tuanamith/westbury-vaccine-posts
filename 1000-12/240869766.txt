


Bird flu not only pandemic risk, U.S. experts warn Tue May 8, 2007 7:03PM EDT By Julie Steenhuysen 
CHICAGO (Reuters) - While many health experts see the H5N1 bird flu virus as a likely cause for an influenza pandemic, another influenza virus could just as likely mutate into a global killer, U.S. health experts said on Tuesday. 
"You can not accurately predict if and when a given virus will become a pandemic virus," said Anthony Fauci, head of the U.S. National Institute of Allergy and Infectious Diseases. 
Fauci said too little is known about exactly how and when a virus will mutate. Focusing too much on one suspect -- even a very likely suspect such as H5N1 -- may be a mistake. 
"We should not ... forget the fact that historically pandemics have evolved. We should be building up the knowledge base and expanding the capabilities of making vaccines," he said in a telephone interview. 
Fauci and colleagues, writing in a commentary in the Journal of the American Medical Association, said pandemic prevention strategies must based on "expecting the unexpected and being capable of reacting accordingly." 
They recommend companies expand their research on vaccine design, develop new classes of drugs and improve tests to diagnose influenza. 
The H5N1 avian influenza virus that is now killing birds in countries from Indonesia to Nigeria rarely infects people. 
But many believe if it mutates in just the wrong way, it could start passing easily from one person to another and would sweep the globe, killing millions. 
Companies and governments are working feverishly to prepare for a pandemic by developing vaccines and stockpiling drugs to treat viruses. 
But focusing too much on just one viral suspect may leave the world vulnerable in the event that another virus causes a deadly flu pandemic. 
"The ability of these types of viruses to ultimately spread from birds to humans is a very complex process involving multiple genetic evolutions. It's a complicated issue that is very difficult to predict," Fauci said. 

"Civilization is the interval between Ice Ages." -- Will Durant. 

"Progress is the increasing control of the environment by life.  --Will Durant 
Joseph R. Darancette  <EMAILADDRESS>  

