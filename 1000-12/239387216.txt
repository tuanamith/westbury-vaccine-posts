


On Thu, 26 Apr 2007 18:11:43 GMT, "Allen" < <EMAILADDRESS> > wrote: 

Then every other Biologist is confused, Allen. Why not post a reference to the definitions you are using so I can be educated as to the point of my confusion. I will do the same for my part... 
Here is an easy to read reference to evolution, complete with the kind of diagrams that I like to read. It was prepared by one of the World's most distinguished institutions of higher education in science: 
 <URL>  


Ummm. Allen. Influenza is a virus. They do not mate. They do mutate. They do become separate species, so distinct that old vaccines fail. Some are much more virulent than others, as predicted by evolutionary theory (killing the host is selected against). ... 

The idea of a family tree, ending in Genus, Species, was set out by Carolus Linnaeus over a hundred years before Darwin published his work on evolution. Species was defined by morphological similarity, in other words, they looked the same from the outside. 
There are other ways of classifying living creatures, such as by their physiology (how they are built from the inside out) and by their genetics and by their DNA as a whole. 
Evolution works NO MATTER HOW you classify creatures. It is about the decent with modification from a common ancestry, and always has been: 
	 <URL>  

Evolution is the mechanism for speciation. It just so happens that some species can breed with each other and some can't. 
For example, Lions and Tigers are different species but they CAN breed successfully with each other. Great Danes and toy dogs are the same species but they cannot breed with each other due to size differences. 
Evolution can handle them all. 
In evolution there is often a common ancestor for two families of creatures that are so different there is no way they can breed with each other, like Reptiles and Birds. Well, the fossil record has a complete record of gradual change from a common ancestor to reptiles on the one hand and birds on the other. Here is the reference: 
 <URL>  
There is not just a common ancestor for Reptiles and Birds, but a common ancestor for Reptiles and Mammals. 
Here is an FAQ that might help with other questions like this: 
	 <URL>  
... 
Allen, evolution will never be anything more than a theory. That's as far as science goes.  
It is settled science, though.  
That doesn't mean it is a belief. It does mean that we have a solid base of integrated evidence that fits a certain model and not others, and enough work has been done to know it is a broadly useful model. 
That doesn't mean the model isn't being refined. For example, areas of evolution are being worked together with DNA processes. Proteins from a T.Rex have just been analyzed. A number of them are identical to the proteins a chicken makes, as expected from the Theory of Evolution. 
Does that make us believers? Every scientist would love nothing better than to unseat Darwin the way Einstein unseated Newton, and Newton unseated Galileo. Have no fear that beliefs would stand in our way. 

