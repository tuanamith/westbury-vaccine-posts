



"Gandalf Grey" < <EMAILADDRESS> > wrote in message  <NEWSURL> ... You lose BIG TIME ASSHOLE!! 

Margaret Whitman Killed in her apartment prior to the tower shootings. 

Kathy Whitman Killed while she slept, prior to the tower shootings. 

Edna Townsley 47-year-old receptionist and divorced mother of Danny (16) and Terry (12). Whitman encountered her on the 30th floor. He hit her in the head with the back end of his shotgun. 

Marguerite Lamport Mike and Mark Gabour, the 16- and 19-year-old sons of M.J. Gabour, led their father, mother, aunt and uncle up to the observation deck of the tower just as Charles Whitman was just assembling his arsenal. He opened fire on them. They died nearly two minutes later. 

Mark Gabour 16-years old. 

Thomas Frederick Eckman 18 or 19 years old, Eckman's parents were Mary Louise and Frederick. A native of Barcelona, Spain, Eckman was studying anthropology at UT. He huddled with Claire Wilson, his girlfriend, outside Benedict Hall after the shooting started, possibly shielding her. Thomas was shot after Claire Wilson was shot in her abdomen, injuring their unborn child, who was in its third trimester. Thomas died in the hospital. 

Robert Hamilton Boyer A 33-year old mathematics professor at UT, Robert Boyer had just finished a month teaching in Mexico and had secured a job at Liverpool University to be nearer his pregnant wife Lyndsay, and their children Laura and Matthew. He was shot in the back. 

Thomas Ashton A 22-year old Peace Corps trainee from Redlands, California, Thomas Ashton was a recent USC graduate attending UT for orientation on his upcoming deployment to Iran on September 14, 1966. He was walking atop the University's Computation Center when he was shot in the chest. He died at Brackenridge, a local Austin hospital. His parents were George F. and Alice Ryan Ashton. 

Thomas Karr A 24-year old UT student, Karr had just finished writing a Spanish exam when he was shot in the spine. He was walking alongside Karen Griffith. He died after being taken to the hospital. His father was Ray T. Karr. 

Billy Paul Speed A 22-year old policeman, Billy Speed was married to Jean and had a daughter named Rebecca Lynn. He had joined the Austin City Police on July 2, 1965 after a stint in the 82nd Airborne Division. He was eating lunch when he heard the gunfire, and was one of the first police officers on the scene. He had taken cover behind a stone railing beneath a statue of Jefferson Davis in The Mall, but Whitman managed to shoot him as he peered out.[1] 

Harry Walchuk A 39-year old PhD student, married to Marilyn, they had six children named John, Peter, Christopher, Jennifer, Thomas, and Paul. He had gone to Guadalupe Street to purchase a magazine during his lunch hour. Died while in emergency surgery. A memorial fund was established in his name and raised $850. 

Paul Bolton Sonntag An 18-year old graduate of Stephen F. Austin High, Paul Sonntag had been accepted to University of Colorado for the upcoming term, and was working as a lifeguard at the local pool. Initially out with his fiancee Claudia Rutt and their friend Carla to pick up his paycheck and get Claudia a polio vaccine, when the shooting started the three of them had taken cover behind construction barriers in front of the Snyder-Chenards dress shop. At one point Paul stood up exclaiming "Carla! Come look, I can see him. This is for real!" and was shot in the mouth. His grandfather, KTBC anchor Paul Bolton, learned of his death when he asked reporter Joe Roddy to read out a list of casualties on-air. He was buried in Austin Memorial Park.[2] 

Claudia Rutt An 18-year old who dreamed of becoming a dancer, Claudia Rutt had been accepted to Texas Christian University for the upcoming term. She was initially out with her fiancee Paul Sonntag, whose engagement ring she wore around her neck, and their friend Carla to pick up his paycheck and get herself a polio vaccine. When the shooting started the three of them had taken cover behind construction barriers in front of the Snyder-Chenards dress shop. At one point Paul Sonntag stood up exclaiming "Carla! Come look, I can see him. This is for real!" and was shot in the mouth. Claudia moved out from the barriers to try and help Paul, but was shot in the chest by Whitman moments later. She was buried in Oakwood Cemetery. Her father was Melvin, her mother is Diane, and her sister is Mary Anne. 

Roy Dell Schmidt A 29-year old electrical repairman, Roy Dell Schmidt had just finished making a phone call when a bystander announced the gunfire. He was running towards his truck with a co-worker when he was shot in the stomach. He left behind his wife Nancy White and their daughter, Kimberly Dawn. 

Karen Griffith 17-year old daughter of Harvey Griffith, Karen also had a sister Pamela. She was a student from Sidney Lanier High School, where Whitman's wife Kathy was a teacher. She was shot in the lung and died after a week in the hospital. Her funeral was held the following day at Crestview Memorial Park in Wichita Falls. Together with Kathy, she was the subject of dedication for her school's 1967 yearbook. 

David Gunby A 23-year old electrical engineering student, Gunby was shot in the lower-left back, while walking towards the University library. During surgery to reconnect his severed small intestine, doctors realised that Gunby's sole kidney had also been damaged by the shooting. He later required a kidney transplant, and to go on dialysis. He moved to Fort Worth, Texas and on November 7, 2001 announced he was stopping dialysis. He died a week later at Harris Methodist Hospital. 




