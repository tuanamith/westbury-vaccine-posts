


REGIONAL CO-OPERATION ACMECS SUMMIT 
Contract farming on Thai agenda ACHARA ASHAYAGACHAT 
Thailand is finalising a framework agreement on contract farming with neighbouring countries according to Virachai Plasai, head of the Foreign Ministry's International Economics Department. 

Foreign Minister Nitya Pibulsonggram is visiting the northern Burmese city of Mandalay today for the ministerial meeting of the five-member Ayeyawady-Chao Phraya-Mekong Economic Strategy (Acmecs) group. 

Mr Virachai said Thailand would push for progress on three issues: co- operation on health, contract farming and infrastructure, especially road transport. 

With an initial fund of 100 million baht, Thailand has so far trained villagers and local authorities in Burma on preventive measures against bird flu so that they would not have to rely on vaccines only, he said. 

The efforts will also be extended to other member countries. ''Health will be a key issue for the economic prosperity of this region, where international organisations do not have easy access in certain countries,'' he noted. 

On contract farming, Thailand would focus on promoting crops that cannot be grown in Thailand or those that yield poorly when grown in the country. 

''We will purchase crops from villagers but we and our partners have to ease transport procedures and tariffs in line with local and international laws,'' he said. 

Thailand is expected to sign a framework on contract farming with some of the Acmecs members during the summit to be held in Hanoi later this year. 

The Charoen Pokphand Group has so far been the only Thai firm implementing contract farming in the region, but the framework will benefit other business entries as well and facilitate the ongoing business efforts with neighbouring countries, he said. 

The Acmecs meeting in Mandalay is also expected to discuss how to streamline the road development plans initiated under various co- operation frameworks including the Greater Mekong Sub-region (GMS), Bangladesh-India-Myanmar-Sri Lanka-Thailand Economic co-operation (Bimstec), and the Indonesia-Malaysia- Thailand Growth Triangle (IMT- GT), as well as Asean. 

Prime Minister Surayud Chulanont has given a guideline at the Asean Cebu summit to work out how to materialise a one-stop service at border checkpoints for cross-border passengers. 

Mr Virachai said authorities in charge of labour, customs, and health would further discuss the matter. 

''Infrastructure should be treated at a wider context, say, Acmecs Plus to tap India and China's potential, with Thailand acting as a hub linking also the other two fast-growing economies of Singapore and Malaysia.'' 

At the summit, Thailand and Cambodia will sign a visa-free agreement as partners in integrated movement of people. 


