


 <URL>  
GSK at centre of Russian vaccine scandal 
By Andrew Osborn in Moscow Published: 04 April 2007 
The pharmaceutical giant GlaxoSmithKline has become embroiled in a  vaccine-testing scandal in southern Russia after prosecutors set out  criminal charges against three doctors involved in a trial of the company's  drugs. 
Prosecutors claimed that the doctors broke Russian law and ethics, but have  so far stopped short of criticising GSK. The pharma titan has denied any  wrongdoing, calling the allegations "unsubstantiated and untrue". 
The dispute centres on a series of trials conducted at Volgograd's  Independent Clinical Hospital on GSK's behalf on 100 babies between the ages  of one and two, starting in 2005. The trials involved GSK-branded vaccines  for chickenpox (Varilrix), measles (Priorix), mumps and rubella (a combined  MMR Priorix Tetra vaccine) and were part of a larger series of trials  involving almost 6,000 adults and children in ten European countries  including Russia. Problems arose after some parents of the babies involved  claimed that they did not give their consent and were not even aware that  their children were taking part in the trials. 
Prosecutors said they have found evidence to back up parents' concerns.  "Preliminary investigations showed that the doctors, seeking material  benefits, conducted clinical tests of the vaccines with no regard for the  children's lives and health," they said. "The parents believed these were  routine vaccinations, they were not told that new vaccines were being tested  on their children." The paediatricians received 1.5 million rubles ($57,670)  and 700,000 rubles from GSK, prosecutors added, something they said parents  were also unaware of. 
The claims are however robustly rejected by the hospital, which insists it  has the paperwork to prove that the parents knew what was happening and gave  their written consent. Perhaps more seriously, the parents of one baby -  Vika Gerasinka - also alleged that GSK's drugs had caused serious damage to  their child's health, retarding her development. They claimed that Vika was  developing normally before she was given her shots in November of 2005 and  could say 10 words, but that she became disturbed and ill after being  inoculated. Vika, now two and a half, has developed serious speech and  psychological problems. 
Prosecutors appeared to back up her parents' allegations. "Medical  examination revealed the girl's health problems occurred as a result of the  vaccination," they said in a statement to which GSK has so far declined to  respond. The scandal has triggered a bout of hand-wringing in local and  national media about how Russia is allegedly being used as a laboratory for  questionable experiments by unscrupulous foreign firms. 
This is not the first time GSK's Priorix has been called into question. Last  year a child in Vietnam died from toxic shock syndrome after being  administered a shot and five other infants fell ill. 
However, an investigation suggested the side effects were unrelated to the  vaccine which has been tested extensively.  



