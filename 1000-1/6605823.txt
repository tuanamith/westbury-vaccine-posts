


Bird Flu: Too Soon To Panic 
NEW YORK, Oct. 12, 2005 -------------------------------------------------------------------------------- Dr. Marc Siegel, on The Early Show Wednesday (CBS/The Early Show) 

Quote 
" It only happens about three times per century that a real killer virus  occurs, so we may be getting alarmed way too prematurely here." Dr. Marc Siegel -------------------------------------------------------------------------------- 

(CBS) Bird flu is appearing on radar screens of more and more public health  officials worldwide, and is worrying more and more Americans. 
But Dr. Marc Siegel, an associate professor of medicine at New York  University and author of "False Alarm: The Truth About the Epidemic of  Fear," says it's way too early for panic. 
He tells The Early Show co-anchor Julie Chen experts simply don't know how  likely it is that bird flu will mutate into a form that can be transmitted  from human-to-human, rather than the bird-to-human transmission suspected in  the 65 human bird flu deaths reported so far around the globe. 
"Keep in mind," he points out, "that this virus has been around since 1997,  and it has increased in the bird population. But the key is controlling it  in the bird population. 
"The chances are that some virus will mutate so that it can go  human-to-human, but it may not be this one. It only happens about three  times per century that a real killer virus occurs, so we may be getting  alarmed way too prematurely here. .It's something we should cool down about  right now." 
Not only that, but, "We already have a vaccine for it. It's being tested now  in elderly people and looks very effective. 
"But what we need to do is upgrade our ability to create vaccines quickly.  Currently, we're using a chicken-egg medium to make vaccines, and we need  genetic engineering. We have the ability, but it's expensive. I think,  rather than stockpiling vaccines, we ought to focus on our ability to make  vaccines quickly." 
In addition, "People need to know that they should not worry about this  because, again, if millions of birds have it, occasionally a human will get  it who has close contact or repeated exposure to birds. 
"There's no problem traveling to Asia, or with casual contact. You cannot  get it from eating a chicken at all. It's from repeated, repeated exposure  to birds that they could get it. 
If anything, Siegel stresses, we shouldn't take our attention off the  regular flu, which puts more than 100,000 people in hospitals each year. 
And, "We should be concerned about knowing that our government is upgrading  our vaccine policy, that we're look at potential epidemics that can occur.  We need to gear up for potential epidemics, knowing it may not be this one  that's gonna be the epidemic." 
As for all the media attention bird flu is getting, Siegel calls it both  good and bad: "It's a good thing because it puts attention on public health  needs. It's a bad thing if it makes people think it will happen to them.  Something only affecting birds and in Asia is not a risk to us in the United  States right now." 

©MMV, CBS Broadcasting Inc. All Rights Reserved.  



