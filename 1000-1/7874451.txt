


On Sat, 08 Oct 2005 23:20:25 -0400, Harry Krause < <EMAILADDRESS> > wrote: 

Harry, you really shouldn't snip and paste to protect your viewpoint. 
Unless of course you are into some kind of Orwellian "newspeak" in which only those parts that directly your view of the President are important. 
I'm very disappointed in you - I thought you were for truth and honesty - in particular being a journalist and having worked for a newspaper at one time. 
Here's the whole article. 
October 7, 2005 
After Delay, U.S. Faces Line for Flu Drug  
By GARDINER HARRIS 
In an interview on Tuesday, Mr. Leavitt said that the government would buy more Tamiflu although he did not specify how much.  
Mr. Leavitt said the Bush administration planned to prepare for a possible influenza pandemic by strengthening both international and domestic disease surveillance programs, buying drugs like Tamiflu and investing in research to develop alternative methods of making flu vaccines.  
Preparing the vaccines usually takes nine months and involves the eggs of thousands of chickens. Because chickens themselves could be wiped out in a pandemic, the present system of manufacturing vaccines is highly vulnerable.  
Mr. Hurley said that Roche would be able to deliver all the courses that the United States government has currently ordered, including at least two million courses ordered this year.  
Since 1997, avian flu strains have killed millions of birds in nearly a dozen countries. But so far, nearly all of the people infected - more than 100 so far, including some 60 who died - got the sickness directly from birds. Until the virus passes easily among humans, it is unlikely to cause a pandemic that could kill millions.  
An outbreak, therefore, may still be years away or may never occur. But news this week that the 1918 flu virus, which killed at least 50 million worldwide, was also a form of avian flu raised concerns further.  
And here's today's article from the New York Times 
October 9, 2005 
Danger of Flu Pandemic Is Clear, if Not Present   By DENISE GRADY 
Fear of the bird flu sweeping across Asia has played a major role in the government's flurry of preparations for a worldwide epidemic. 
That concern prompted President Bush to meet with vaccine makers on Friday to try to persuade them to increase production, and it led Health and Human Services Secretary Michael O. Leavitt to depart yesterday for a 10-day trip to at least four Asian nations to discuss planning for a pandemic flu.  
But scientists say that although the threat from the current avian virus is real, it is probably not immediate.  
Dr. Anthony S. Fauci, director of the National Institute of Allergy and Infectious Diseases, said a bird flu pandemic was unlikely this year.  
Dr. Jeffery Taubenberger, chief of the molecular pathology department at the Armed Forces Institute of Pathology, said, "I would not say it's imminent or inevitable." Dr. Taubenberger said he believes that there will eventually be a pandemic, but that whether it will be bird flu or another type, no one can say.  
The Bush administration is in the final stages of preparing a plan to deal with pandemic flu. A draft shows that the country is woefully unprepared, and it warns that a severe pandemic will kill millions, overwhelm hospitals and disrupt much of the nation.  
What worries scientists about the current strain of bird flu, known as H5N1, is that it has shown some ominous traits. Though it does not often infect humans, it can, and when it does, it seems to be uncommonly lethal. It has killed 60 people of the 116 known to have been infected.  
Alarm heightened on Thursday when a scientific team led by Dr. Taubenberger reported that the 1918 flu virus, which killed 50 million people worldwide, was also a bird flu that jumped directly to humans.  
There is a crucial difference, however; the 1918 flu was highly contagious, while today's bird flu has so far shown little ability to spread from person to person. But a mutation making the virus more transmissible could set the stage for a pandemic.  
Another concern is that H5N1 has become widespread, killing millions of birds in 11 countries and dispersing further as migratory birds carry it even greater distances. This month, it was reported in Romania. 
Meanwhile, the flu is spreading widely among birds in Asia. And it has unusual staying power, persisting in different parts of the world since it emerged in 1997.  
"Most bird flus emerge briefly and are relatively localized," said Dr. Andrew T. Pavia, chief of the division of pediatric infectious diseases at the University of Utah and chairman of the pandemic influenza task force of the Infectious Diseases Society of America. The most worrisome thing about H5N1, Dr. Pavia said, is that it has not gone away.  
Meanwhile, H5N1 seems to be finding its way into more and more species. Once known to infect chickens, ducks and the occasional person, the virus is now found in a wide range of birds and has infected cats.  
"It killed tigers at the Bangkok zoo, which is quite remarkable because flu is not traditionally a big problem for cats," Dr. Pavia said.  
It has also infected pigs, which in the past have been a vehicle to carry viruses from birds to humans.  
"We should be worried but not panicked," Dr. Pavia said.  
The timing of the bird flu's emergence also makes scientists nervous, because many believe that based on history, the world is overdue for a pandemic. Pandemics occur when a flu virus changes so markedly from previous strains that people have no immunity and vast numbers fall ill. 
"In the 20th century there were three pandemics, which means an average of one every 30 years," Dr. Fauci said. "The last one was in 1968, so it's 37 years. Just on the basis of evolution, of how things go, we're overdue."  
In just a few instances, Dr. Gellin noted, the virus does appear to have spread from person to person.  
But not everyone is equally worried about the bird flu.  
Scientists also say that the death rate may not be as high as it appears, because some milder cases may not have been reported.  
Dr. Kilbourne and other experts also noted that when viruses become more transmissible, they almost always become less lethal. Viruses that let their hosts stay alive and pass the disease on to others, he explained, have a better chance of spreading than do strains that kill off their hosts quickly.  
But an essential difference is that people carrying the flu today can board international flights and carry the disease around the world in a matter of hours.  
Dr. Kilbourne emphasized that medical care had improved greatly since 1918. Although some flu victims then turned blue overnight and drowned from blood, with fluid leaking into their lungs, many more died of what are now believed to be bacterial infections, which can be treated with antibiotics.  
Although the death toll from that flu was high, the actual death rate was less than 5 percent.  
What researchers wish they could do now is look at a flu virus like H5N1 and predict whether it is heading down the genetic road to becoming a pandemic strain.  
Andrew Pollack and Donald G. McNeil Jr. contributed reporting for this article. 
---------------------------- 
It would appear that we should have been doing more about this ten years ago - which would make that who's administration? 
Interesting how the viewpoint changes when you have the whole picture doesn't it? 

