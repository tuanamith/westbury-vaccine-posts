


Vietnam, Indonesia and Cambodia were most vulnerable due to the large domestic poultry populations, he said. 
"Theoretically it is possible to contain the virus if we have early signs of a pandemic detected at the source," he said. 
Even when an avian flu vaccine is fully developed, production limitations will mean there will not be enough vaccine. 
But Oshitani fears that once a pandemic occurs, the world's rich nations may dominate vaccine supply. 
"The pandemic is likely to be like the seasonal influenza, which is much more infectious than the SARS virus," he said. 
 <URL>  


