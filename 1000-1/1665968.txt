


Via NY Transfer News Collective  *  All the News that Doesn't Fit   Workers World - Sep 29, 2005 issue  <URL> / 
Book Review: 
Tactics for counter-recruiting 
By Almahdi 
"We Won't Go! The Truth on Military Recruiters & the Draft, A Guide to Resistance" published by the International Action Center, New York, 2005, 90 pages, index. $14.95. Available at Leftbooks.com 
Since the beginning of the illegal war on Iraq in 2003, almost 2,000 U.S. soldiers have given their lives for the lies of the Bush administration. More than 100,000 Iraqi civilians have also lost their lives as a result of the "precise" and "accurate" weapons that are being used. And up to this day, the war is costing more than $200 billion, which could have been spent on jobs, education and health care. 
So the question arises: How can people here end this war? The answer to this question is found in the book, "We Won't Go!, A Guide to Resistance," published by the International Action Center. 
As the bloody war continues, joining the Armed forces grows less popular every day. In response, military recruiters have launched massive campaigns targeting and encouraging high school and college students to join up. 
"The Guide to Resistance" documents how recruiters specifically target people of color and working class youth who have few other economic options. The lack of jobs and the racist criminal justice system conspire to make the army seem a good option for many youth. This results in a very real poverty draft, but even these severe conditions are not enough to force the needed numbers to enlist. 
As the recruiters fail to meet their quotas, they increase the bonuses offered and false promises of money for college education, on-the-job training and other benefits most recruits never get. In return, the young recruits are asked to be war criminals and tools for corporations like Chevron and Halliburton. 
"We Won't Go!" describes, in detail, the different lies and conscious omissions military recruiters routinely make. It also shows how anti-war activists, parents and others can expose the recruiters' lies and mobilize to bar them from our high schools and college campuses. 
The book also shows how it is possible to get out of the military, even for those who have already signed up and then realized the war is not about liberation and nothing but a brutal occupation. 
As of last March 31, the Selective Service reported to President Bush that it is ready to reinstate the draft. Military conscription is another major threat to youth. The book states that the draft needs to be stopped before it starts and explains how to start organizing to stop it. 
"We Won't Go!" also reflects on the experiences of war resisters, uncovers the brutal and dehumanizing treatment of lesbian/gay/bi/trans people in the military, and exposes the experiments of internationally banned weapons such as depleted uranium and the anthrax vaccine on soldiers. Publishing "We Won't Go!" is an important step in stopping the destruction of another generation of youth, whether through poisoning, mental health crises, sexual abuse or as casualties of war. 
The book is not simply an academic presentation of facts, but recognizes that only the youth who have already enlisted and those being recruited have the power to end this war. The tools and strategies suggested in this book can give youth activists the strength to kick the recruiters out of their neighborhoods and campuses and help mobilize support for GI resisters. 
Without soldiers, the Pentagon can't fight its wars. 

