



Rich wrote: 
Note that Rich chose not to respond to this. 

Natural immunity still determines the tolerance threshold, the "tipping point" unique to each individual.  Your notion that illness is a product of what's "out there" without regard to other variables is irrational. 

This is just another macro factor at work, it doesn't mean that natural immunity is not integral to how illness progresses.  For instance, we can't say that everyone who sleeps near a window is going to survive a bout with influenza, just because it improves the odds in aggregate. As the two groups split apart, survivors and victims, you have to explain what is responsible for each successive improvement in the chance for survival, and at some point natural immunity is what explains the "tipping point." 

How is conscious, directed thought equivalent to biological homeostasis?  There is a body/mind link, but you are describing intent as the sole province of health.  The body is a physical organism, and no analogy that avoids that makes sense.  Physical health is inseparable from cellular vitality. 

Funny, I think of belts as part of the musculoskeletal system, air bags as lungs, and tires as part of ATP transport into the mitochondria. You know, physical things associated with other physical things. 

Nothing is absolute, however natural immunity is the primary determinant in survival and longevity regardless of environmental triggers in disease.  The scientific press is proving this every day. 


Vaccines are good at creating an anti-body response, which may provide a degree of immunity for some people, however not without risk.  Until your sponsors conduct real studies, that's all anyone can be sure of. 

The answer is that children are genetically adapted to have improved survival after millions of years of evolution.  Our knowledge of immunity is only partial, so measurements we think are comprehensive overlook a level of biochemistry that demonstrates a superior immune response in children. 

I understand the purpose of vaccine.  Antibodies responding to dead virus are supposed to recognize living ones when they knock on your front door.  As I've said many times, however, anti-body titres are not equivalent to immunity.  If it were actually effective, something equivalent would have evolved with us over the past few million years. 

Your belief that vaccine provides compensation for that short-fall is just a nice idea. 

I think you are saying that antibodies are the result of a viral agent making it through the other defenses, which is certainly true.  It's important to point out, though, that antibodies are a separate layer of immunity and other layers have completely unique functions, so the integrity of each system is wholly dependent on one's overall health. 

And it's an evolved response many millions of years old. 

If only it worked in reality as well as it does in your dreams. 

That's because virtually every other organism synthesizes its own ascorbate, however all life requires vitamin C for survival.  I meant that vitamin C was the first nutrient required for life after the gases. 


That may have been true before the depletion of nutrients in modern agriculture, which has seen reductions of 50% or more for certain key vitamins and minerals.  That combined with a slew of environmental triggers such as pollution, pesticides, fluoridation, and other chemical exposures, make supplementation essential to optimal health. 

And that doesn't change the role of nutrient sufficiency in human health. 

And I'll bet they didn't have coupons back then, either. 
PeterB 


