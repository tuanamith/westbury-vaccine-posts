


 <URL>  The United Kingdom,for example, has ordered enough for 14.6 million people, a quarter of its population. 
Thursday, October 13, 2005 

Tamiflu, a pricey antiviral pill invented in a Bay Area lab and made in part from a spice used in Chinese cookery, 
As nations begin to stockpile the drug in anticipation of a flu pandemic, 
calls are mounting for countries to sidestep patents on the drug --  
as Brazil first did for AIDS medications --  and make their own generic versions. 
But Swiss pharmaceuticals giant Roche, which acquired rights to the drug from Gilead Sciences Inc. of Foster City in 1996, 
said Wednesday it had no intention of letting others make it. 
"Roche ... 
fully intends to remain the sole manufacturer of Tamiflu,'' 
said company spokesman Terry Hurley. 
The immediate problem is not the cost of Tamiflu, 
which runs about $60 for a 10-pill course of treatment, 
but a staggering gap between the sudden demand for it and the capacity of its sole manufacturer to produce it. 
Although Roche has increased production of Tamiflu eightfold in the past two years, 
it will take $16 billion and 10 years to make enough of the drug for 20 percent of the world's population, 
said Klaus Stohr, 
director of the World Health Organization's Global Influenza Program, 
in comments to reporters in San Francisco last week. 
"Something has to be done,'' said Ira Longini, 
"It makes sense to do something along the lines of what was done with AIDS drugs.'' 
James Love, director of the Consumer Project on Technology in Washington, D.C., 
said that during the anthrax bioterrorism scare in 2001, 
The Tamiflu problem is similar. 
"The WHO should buy stockpiles from generic suppliers,'' he said. 
"If patents are in the way, the WHO should ask the manufacturing country to issue the appropriate compulsory licenses. 
The patent owner will receive royalties, but we will have the stockpiles." 
U.N. Secretary-General Kofi Annan has signaled a willingness to consider generic production of flu drugs and vaccines. 
During remarks at the World Health Organization headquarters in Geneva last week, 
he said drug companies should be "helpful" by not letting their patent claims interfere with access to medicines. 
"I wouldn't want to hear the kind of debate we got into when it came to the HIV anti-retrovirals,'' he said. 
Roche will not release its Tamiflu production figures, deeming it 
"commercially sensitive" information, 
said Hurley, the company spokesman. 
However, he said the company produced "many hundreds of millions" of the pills annually. 
In response to WHO concerns about bird flu this summer, 
the company agreed to donate enough Tamiflu to treat 3 million people. 
Although public awareness of the pandemic threat posed by the bird flu has blossomed in recent weeks, 
Tests on laboratory mice strongly suggest that Tamiflu --  and a lesser-known inhaled antiviral, Relenza --  
are the only medications that can treat infection with the H5N1 strain. 
Tamiflu has not been effective in the treatment of the small number of people who've contracted the H5N1 virus in Asia. 
Of 116 people infected since 2003, half have died. However, 
most of those patients were admitted to hospitals days or weeks after they became ill. 
Tamiflu is thought to work best within 36 hours of symptoms. 
"Late treatment is clearly ineffective,'' said Dr. Frederick Hayden, 
a University of Virginia expert on flu drugs. 
As a treatment for ordinary flu, Tamiflu has been effective, but in a Japanese study of children treated with the drug, 
At WHO's urging, 40 nations last year began building stockpiles of Tamiflu. 
The United Kingdom,for example, has ordered enough for 14.6 million people, a quarter of its population. 
although no timeframe for the purchase has been revealed. 
E-mail Sabin Russell at  <EMAILADDRESS> . 




