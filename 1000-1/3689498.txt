


Bird flu epidemic could kill millions worldwide: experts Sun Sep 18, 4:36 PM ET 


ADVERTISEMENT 
"There is very good momentum, but a lot of work remains to be done," McNab said. 
For the WHO it is question of when, not if, the virus crosses over to a strain affecting humans, experts said. 
Health professionals say an outbreak would appear in three phases: 
- an "emerging phase"; 
- the "declared pandemic phase" when the virus rages unchecked across national borders. 
Ninety percent of global influenza vaccine production is located in Europe and North America. 


