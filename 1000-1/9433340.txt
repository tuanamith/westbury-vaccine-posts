


 <URL>  
Four Minnesota Amish children contract polio 13 Oct 2005 19:38:11 GMT 
Source: Reuters 
CHICAGO, Oct 13 (Reuters) - Four children in two Amish families in Minnesota have been infected with polio after not being vaccinated against the disease, probably for religious reasons, state health officials said on Thursday. 
Genetic testing shows the polio involved likely originated overseas in an infant who had been vaccinated with a live polio virus, state epidemiologist Dr. Harry Hull said. That kind of vaccine has not been used in the United States for five years. 
He said a Minnesota Amish infant who was first diagnosed with polio in September would have had direct contact with someone exposed to the imported virus, perhaps through a chain of people. 
Three other children, all members of a second Amish family, have now been found to be infected with the virus. None of the four has shown signs of paralysis, the Minnesota Department of Health said. 
"Disease investigators say there are direct links between the family of the three children and the family of an infant found to be infected with the virus at the end of September. The two families are not related by blood," the department said. 
Hull said in an interview that the Amish community in Minnesota, like others spread across the United States, is insular, operating its own schools and often refusing vaccinations for religious reasons. 
"This is very unusual," he said. "Our concern is that since the virus has started to spread there is the potential for it to spread to other Amish communities across the country," he added. 
The Amish are descendants of the Swiss Anabaptists who eschew modern devices such as motorized vehicles and live in rural areas. 
An estimated 93 percent of infants in Minnesota have been immunized against polio, and 98 percent of Minnesota children have been immunized by the time they start school, health officials said. 




