


On Sun, 09 Oct 2005 08:24:45 -0700, Robert Sturgeon < <EMAILADDRESS> > wrote: 

< <URL> > < <URL> > 
I'm very far from an expert, but I've gathered from reading that: 
1. The bird population is doomed to a 'selection' event.   
2. There are certain cases where people got it from birds.  That was debated early on but now seems agreed on by medicos.   
3.  There are 'apparent' cases where people, that had no contact with birds, got it from another person.  It seems likely 'apparent' will be dropped very soon. 
Point 3 makes it a human to human disease and it has all the classic earmarks of past pandemics.  That can be addressed by travel restrictions but it probably won't be. 
Point 1 leads to decimation of commercial food fowls and the farmers loss of bug control by wild birds.  Expect familiar insect transmitted diseases to increase. 

What really worries me is if an as-yet-not-existing-mutation let's birds catch the people to people version and transmit it.  There is no way to stop that given natural migration.  And there are examples of animals carrying diseases that do not harm them but are serious for people. 

Actually there are recent stories that they have doped out the virus DNA and that's a good start on developing a vaccine.  We are in a race. 
Any honest answer now has to be that nobody knows.  As a survivalist, I'll assume the worse and be very happy if my fears don't materialize. 
If all this is not enough, there are any number of exotic animals handled and eaten in exotic ways in Asia.  Historically, many new diseases seem to have originated in that area.  There are a number of specifics possibilities being watched the same way bird flu was watched years ago.  If bird flu peters out or there is a vaccine, there are more candidates in the wings. 
-- WÂ§ mostly in m.s -  <URL>  

