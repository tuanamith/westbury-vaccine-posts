


	The Clinton 200 
	Why Clinton should go. 	By NR Editors  
 <URL>  
EDITOR'S NOTE: "The Clinton 200" appeared in the October 26,  1998, issue of National Review in "The Week" section. 
Ken Starr had 11 reasons why President Clinton should go,  the Judiciary Committee added another 4,  
and now-so as to hold up our end-we suggest another 185,  for a clean 200 total. 

 * HRC on health care and small business: "I can't save   every undercapitalized business in America" 
 * Siccing the FBI on Billy Dale  * Siccing the IRS on conservatives 
 * Defending partial-birth abortion 
 * Keeping D.C. kids trapped in rotten schools 
 * Suing California for repealing preferences 
 * The DNA speech 
 * Screwing around with the Census  * Screwing around with the interns 
 * More rather than less, sooner rather than later 
 * The Fleetwood Mac revival 
 * Filegate 
 * Tax hikes 
 * "It might surprise you to know that I think I raised them   [taxes] too much too" 
 * Jaw-jutting 
 * Phony privilege claims 
 * Underhanded privilege claims 
 * Soldiers and Sailors Act 
 * Making lying to one's diary safe, legal, and common 
 * "The most ethical Administration in history"  * Firing all the U.S. attorneys  * Draft-dodging 
 * Eliminating Beck enforcement upon taking office 
 * ABM treaty 
 * Hoarding children's vaccines 
 * Making us long for Jimmy Carter 
 * Making us long for George Bush  * Making us long for Warren Harding 
 * Scripting aides' reaction to his confession speech 
 * Exploiting church burnings  * Attacking conservatives for Oklahoma City 
 * Sidney Blumenthal  * Joe Conason (sorry, same thing) 
 * Boxers or briefs 
 * Apologizing to whole continents 
 * The tobacco jihad  * The tobacco abuse 
 * Ira Magaziner  * Peanut-free zones on airplanes 
 * Trying to take over health care  * Violence Against Women Act 
 * "Unspeakably vulgar pleasure" he takes in the office   (Mark Helprin) 
 * A Cabinet that looks like America 
 * A Cabinet that looks like the bar scene in Star Wars (Don   Imus) 
 * Letting China dictate his travel schedule  * Letting China fund his campaign 
 * The small-breast defense 
 * Telling Monica his phones are tapped by foreign   governments and then proceeding 
 * Jogging shorts 
 * Pasty white thighs in jogging shorts 
 * Scorched-earth policy 
 * Cheating at golf 
 * Mediscare 
 * Screwing up traffic every time he visits NYC 
 * John Travolta as foreign-policy advisor  * John Travolta as President 
 * Trailer-park trash 
 * Making state troopers pimps  * Making Betty Currie a pimp 
 * Half-hearted salutes 
 * The 60 Minutes interview 
 * Kyoto treaty 
 * Refusing to lift arms embargo on Bosnia 
 * Sending troops instead 
 * While receiving oral sex 
 * Threatening Secret Service agents during Eleanor Mondale   incident 
 * Calling George Bush dishonest 
 * The Hamptons 
 * George Stephanopoulos 
 * Haircut on the LAX runway 
 * Renaissance Weekends 
 * "For the children" 
 * Hush money for Hubbell 
 * Loathing the military 
 * Giving Geraldo Rivera an exclusive interview in China 
 * Using anti-mob laws against peaceful abortion protestors 
 * "If everybody in the country had the character that my   wife has, we'd be a better place to live" 
 * Registering illegal immigrants as voters 
 * Tax deductions for depreciated underwear 
 * How they got depreciated 
 * Sara Lister 
 * Gutting the military 
 * Timber "summits" 
 * Strobe Talbott 
 * Footnote 210 
 * Barbra Streisand 
 * "Kiss it" 
 * "Not appropriate" 
 * "Misleading" 
 * "It depends on how you define alone" 
 * "That depends what the meaning of the word 'is' is" 
 * Town meetings 
 * Mike McCurry explaining Catholic theology to Cardinal   O'Connor 
 * McCurry 
 * All-nighters 
 * Toe-sucking advisors 
 * Waco 
 * Dancing on the beach with Hillary 
 * Jane Doe #5 
 * Seances with Eleanor Roosevelt 
 * Wanting to talk to Eleanor Roosevelt 
 * Years of "I feel your pain" jokes 
 * It Takes a Village 
 * 24 hours a day of Greta Van Susteren 
 * Walt Whitman revival 
 * Suggesting a vote against David Dinkins would be racist 
 * Coddling Saddam Hussein 
 * Smearing Scott Ritter 
 * Being a Rhodes scholar 
 * The dress 
 * "Legally accurate" 
 * Smearing Paul McHale 
 * Taking credit for balanced budget 
 * Taking credit for economy 
 * Wishing he had a challenge like WWII 
 * Complaining about own leaks 
 * Gore telling soldiers' families their sons died "in the   service of the United Nations" 
 * Making every other sentence a double entendre 
 * Monica loves him 
 * Did we mention Fleetwood Mac? 
 * "I haven't been to a McDonald's since I've been   President." 
 * Al Gore 
 * Susan Carpenter-McMillan 
 * Friends of Bill  * Paying people to volunteer  * Multiple Ron Brown funerals 
 * Easter Sunday 1996 
 * The politics of meaning  * Astroturf  * Telling MTV that this time he'd inhale 
 * Having own VH1 special 
 * Lani Guinier 
 * Supposedly breaking the news to HRC re Monica: "You're   not going to believe this but . . ." 
 * "Every day can't be sunshine"  * "Four years and $ 40 million" 
 * Lanny Davis  * Not drinking beer 
 * Standing ovations at the UN  * "Vast right-wing conspiracy"  * Picking spiritual advisor with CNN contract 
 * Mexican bailout  * Government shutdowns  * Banning ugly guns 
 * Roger Clinton  * Wanting to be President when he was 10  * "In the present tense, that is an accurate statement" 
 * Naming dog "Buddy"  * Claiming ignorance about Rwanda genocide 
 * Salon 
 * That picture of him shaking hands with JFK 
 * Hugging everyone 
 * Hillary  * Having a First Cat  * Psychobabble  * Pizza delivery 
 * The oh-so-morally-serious Paul Begala  * He believes Anita Hill 
 * He's even made ministering a double entendre 
 * "Don't ask, don't tell"  * Smoked pot, lied about it, and supports the drug laws 
 * Making French intellectuals relevant 
 * Permanent "interim appointments" 
 * "I did not have sexual relations with that woman, Monica   Lewinsky." 
 * There are no tanks in Mogadishu 
 * Getting back to the business of the people 
 * He thinks he's black 
 * "It's the right thing to do" 


	*  *  *  


 <URL>  

