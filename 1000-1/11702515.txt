


Deadly bird flu strain confirmed in Turkey 
13.10.2005 - 18:03 CET | By Teresa Kuchler 
The test had been conducted by the Community Reference Laboratory for Avian  Influenza Weybridge in the UK. 
The assumption is that it is the H5N1 strain, he added. 
On Monday (10 October) a similar ban was imposed on Turkey. 
Pandemic scare 

Throughout Europe concerns about an avian influenza pandemic is rising. 
In France, people have hoarded face masks and medicines for days, and some  pharmacies are completely out of flu medicines. 
French authorities estimate that between 9 and 21 million French could be  infected in the event of an influenza pandemic. 
The Nordic countries renewed discussions on producing vaccine for their home  populations. 
"The virus in its actual form is extremely difficult for humans to catch." 

--  
Coins, travels and more:  <URL>   <URL>   <URL>  



