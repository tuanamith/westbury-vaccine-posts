


In 1985, at the beginning of HIV testing, it was known that 68% to 89% of all repeatedly reactive ELISA (HIV antibody) tests [were] likely to represent false positive results. (NEJM  New England Journal of Medicine. 312; 1985). 
In 1992, the Lancet reported that for 66 true positives, there were 30,000 false positives. And in pregnant women, there were 8,000 false positives for 6 confirmations. (Lancet 339; 1992) 
In September 2000, the Archives of Family Medicine stated that the more women we test, the greater the proportion of false-positive and ambiguous (indeterminate) test results. (Archives of Family Medicine. Sept/Oct. 2000). 
The tests described above are standard HIV tests, the kind promoted in the ads. Their technical name is ELISA or EIA (Enzyme-linked Immunosorbant Assay). They are antibody tests. The tests contain proteins that react with antibodies in your blood. 
In the U.S., youre tested with an ELISA first. If your blood reacts, youll be tested again, with another ELISA. Why is the second more accurate than the first? Thats just the protocol. If you have a reaction on the second ELISA, youll be confirmed with a third antibody test, called the Western Blot. But thats here in America. In some countries, one ELISA is all you get.  
It is precisely because HIV tests are antibody tests, that they produce so many false-positive results. All antibodies tend to cross-react. We produce antibodies all the time, in response to stress, malnutrition, illness, drug use, vaccination, foods we eat, a cut, a cold, even pregnancy. These antibodies are known to make HIV tests come up as positive.  
The medical literature lists dozens of reasons for positive HIV test results: transfusions, transplantation, or pregnancy, autoimmune disorders, malignancies, alcoholic liver disease, or for reasons that are unclear(Archives of Family Medicine, Sept/Oct. 2000). 
[H]uman or technical errors, other viruses and vaccines (Infectious Disease Clinician of North America 7; 1993) 
[L]iver diseases, parenteral substance abuse, hemodialysis, or vaccinations for hepatitis B, rabies, or influenza (Archives of Internal Medicine August, 2000). 
[U]npasteurized cows milkBovine exposure, or cross-reactivity with other human retroviruses (Transfusion,1988) 
Even geography can do it: 
Inhabitants of certain regions may have cross-reactive antibodies to local prevalent non-HIV retroviruses (Medicine International 56; 1988).  
The same is true for the confirmatory test  the Western Blot.  
Causes of indeterminate Western Blots include: lymphoma, multiple sclerosis, injection drug use, liver disease, or autoimmune disorders. Also, there appear to be healthy individuals with antibodies that cross-react. (Archives of Internal Medicine, August 2000). 



