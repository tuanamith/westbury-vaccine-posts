


Health officials have warned for years that a virulent bird flu could kill millions of people, but few in Washington have seemed  alarmed. After a closed-door briefing last week, however, fear of an outbreak swept official Washington, which was still reeling  from the poor response to Hurricane Katrina. 
The day after the briefing, led by Michael O. Leavitt, the secretary of Health and Human Services, and other senior government  health officials, the Senate squeezed $3.9 billion for flu preparations into a Pentagon appropriations bill. 
On Wednesday, Senate Democrats plan to introduce another bill calling for the creation of a flu pandemic coordinator within the  White House and a federal buy-back program for unused flu vaccines, among other measures, according to a draft of the bill. Its  authors include the Senate minority leader, Harry Reid of Nevada; Senator Barack Obama of Illinois; and Senator Edward M.  Kennedy of Massachusetts. 
Thirty-two Democratic senators sent a letter to President Bush on Tuesday expressing "grave concern that the nation is  dangerously unprepared for the serious threat of avian influenza." 
Mr. Bush spent a considerable part of his news conference Tuesday talking about the risks of an outbreak and the measures the  administration is considering to combat one, including whether to use the military to enforce quarantines. 
"I take this issue very seriously," he said. "The people of the country ought to rest assured that we're doing everything we can." 
But after the administration's widely criticized response to Hurricane Katrina, such assurances are no longer enough, several  
Democratic senators said. 
" 'Trust us' is not something the administration can say after Katrina," Senator Tom Harkin, Democrat of Iowa, said in an  interview. "I don't think Congress is in a mood to trust. We want plans. We want specific goals and procedures we're going to  take to prepare for this." 
So far, Mr. Harkin said, the administration has provided neither, despite requests from Congress. 
Mr. Leavitt acknowledged in an interview that the United States was not prepared for a pandemic flu outbreak. He plans to spend  next week touring Thailand, Vietnam, Laos and Cambodia, the countries most likely to be the source of an avian flu outbreak, and  talking with health ministers there about a coordinated surveillance of outbreaks. 
"No one in the world is ready for it," Mr. Leavitt said. "But we're more ready today than we were yesterday. And we'll be more  prepared tomorrow than we are today." 
Since 1997, avian flu strains seem to have infected thousands of birds in 11 countries. But so far, nearly all of the people  infected with the disease - more than 100, including some 60 who died - got the sickness directly from birds. There has been  very little transmission between people, a requirement for an epidemic. 
An outbreak, therefore, may be years away, or may never occur. And if a strain does jump to people, such a mutation may make it  far less lethal than it has been to those who have contracted it from birds. 
Mr. Leavitt warned in the briefing last week that an outbreak could cause 100,000 to 2 million deaths and as many as 10 million  hospitalizations in the United States, one person who was present said. Those numbers have been presented publicly many times  before. But hearing them in closed session gave them urgency, some who were at the meeting said. 
The briefing "scared the hell out of me," Senator Reid said recently. 
The Senate majority leader, Bill Frist of Tennessee, said he had been delivering speeches about improving the nation's  preparedness for a flu pandemic since December. But as more birds have been discovered with the virus, concerns have grown. 
The poor response to Hurricane Katrina is also a factor, Mr. Frist said. "People watching on TV see that the government wasn't  there in times of need," he said. 
Irwin Redlener, director of the National Center for Disaster Preparedness at Columbia University, called the sudden interest in  preparing for a flu epidemic the latest "post-Katrina effect." 
"I don't think politically or perceptually the government feels that it could tolerate another tragically inadequate response to  a major disaster," Dr. Redlener said. He said a flu epidemic was the "next big catastrophe that we can reasonably expect, and  the country is phenomenally not prepared for this." 
Mr. Leavitt said several steps must be taken to prevent a flu pandemic. First, he said, there must be an effective global  surveillance program for the disease, something he will discuss in his trip to Asia next week. 
Second, the United States must construct its own comprehensive disease surveillance system, he said. And third, antiviral drugs  like Tamiflu, made by Roche Laboratories, and Relenza, made by GlaxoSmithKline, must be made available. 
The government has purchased "millions of courses" of treatment, said Christina Pearson, a spokeswoman for the Health and Human  Services Department, and it has a goal of having on hand 20 million. A course includes enough doses for a full treatment. 
Finally, the government is underwriting research that it hopes will speed the creation of vaccines against the disease, Mr.  Leavitt said. Flu vaccines take nearly nine months to be manufactured. 
Democrats say the Bush administration has failed to spend the money needed to prepare for a flu pandemic. Though Mr. Leavitt  said that preparations by state and local health officials were vital to flu planning, Mr. Harkin said that the administration  had proposed cutting the financing needed for such planning. 
And the Democrats said in their letter to President Bush that it was past time for the administration to finish its flu plan,  which has been under review for a year. 
Mr. Leavitt responded: "We need a plan. I'm resolved to make sure we have one and so is the president." 

 <URL>  

