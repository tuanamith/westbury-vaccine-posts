


This could generate some interesting discussion, it's not political, but definitely has some ethical issues. A number of ideas here, really. 
What is it to be human? 
Innovations that will let us alter bodies and minds will tempt, test our society 
By Joel Garreau / Special to the Los Angeles Times 
In the next few years, your child will come home from school in tears. He'll say, once again, that he is unable to compete with the children who are brighter, better behaved and physically more capable than he is because their parents have bought them technological enhancements and you have not. What will you do? 
You could say, "That's all right dear, we don't care what other parents do with their children's minds and bodies, we love you just the way you are." 
Or: 
You could remortgage the house, again, to try to allow your child to keep up with the human enhancements that ever so rapidly are coming onto the market. 
Or: 
You could try to get the enhanced kids thrown out of your school, widening the chasms that already will be separating "enhanced" humans from the "naturals" who could adopt such enhancements but, for aesthetic or political reasons, choose to abstain, like today's vegetarians or fundamentalists. 
What you and your children can't do is ignore the enhancement products that are already realigning our concept of what it means to be human. 
We are at a turning point in history. For millenniums our technologies --  fire, clothes, agriculture, cities, space travel - have been aimed outward at modifying our environment. Now, for the first time, our technologies are increasingly aimed inward -- at altering our minds, memories, metabolisms, personalities and progeny. 
This is not some science fiction future. Such innovations are in the labs and hitting the market on our watch. Inexorable increases in ingenuity are opening vistas, especially in what we may call GRIN -- genetic, robotic, information and nanotechnologies. 
The curve of innovation is rising exponentially. The significance of all this is not the gee-whiz gear. It is in how we will wrestle with what this means to relationships, loves and lives as we enter a period of engineered evolution that we create and adopt ourselves. 
The scientists, engineers and philosophers pondering the reshaping of humankind see three scenarios. 
There are those who think we're bound for a near-future technological "heaven" in which we conquer suffering, stupidity, ugliness, even death. 
Then there are those who think powers of such scope could be used for supreme evil in the next 25 years, threatening the very existence of our species -- the "hell" scenario. 
Finally, there are those who have sufficient faith in human cussedness that they think we will be able to control our futures rather than be the pawns of technology -- the "prevail" scenario. 
Ethically, intellectually, technologically - it won't be an easy for us to choose. Yet we must. 
Consider the genetic temptations you and your budding young scholar-athlete will face. Scientists' understandings of the human genome and the proteins that control all cellular processes in the body are leading to the creation of remarkable pharmaceuticals. One example is the pain vaccine being tested by a San Francisco Bay Area company. It would cancel acute suffering for a month. Ready for that marathon, son? 
Five companies in the U.S. alone are competing to bring memory pills to market. No wonder. They could be a bigger commercial blockbuster than Viagra. They promise not only to ban the senior moments of the baby boomers but to revolutionize education of the young. Think of what it will do to language acquisition alone. Some analysts believe that it could increase our kids' SAT scores by 200 points or more. 
Born versus Made 
Robotics is allowing unprecedented connections between the human brain and our machine creations, blurring the distinction between the made and the born. 
Matthew Nagel, a paralyzed Boston man, last summer became the first human to send an e-mail with his thoughts. A device implanted into his brain by a company called Cyberkinetics can read his neurons as they fire. This also allows him to control a robotic arm. 
The Defense Advanced Research Projects Agency, the research arm of the Pentagon, is funding similar research because an F-22 is a difficult machine to fly with a joystick. How much simpler it would be, they reason, if a pilot could control it directly with his brain. Images and alarms collected by the plane could be piped directly to the pilot's senses, bypassing computer screens and keyboards. 
Nanotechnology refers to building objects and substances one atom or molecule at a time. The results can be amazing, including forms of carbon that exist by the beaker-full that are 60 times stronger than steel, have the weight of plastic, sport the clean electrical conductivity of diamond and display the precision of DNA. 
Nanotechnology is projected to be a trillion-dollar industry by 2015. Some say it will revolutionize everything: warfare, energy use, medicine. 
Doping becomes norm 
These GRIN technologies, in short, mean that every power of our comic-book superheroes from the 1930s and '40s either exists or is now in active development. A technique called "gene doping," for example, produces such muscle strength in rodents that some University of Pennsylvania researchers flatly doubt we'll ever have an Olympics again that does not involve genetically altered humans. 
Brain-scanning devices at Temple University, meanwhile, offer tantalizing hope of lie-detection so meticulous that, like the Shadow, we may indeed know what evil lurks in the hearts of men. 
Advocates of the "prevail" scenario think back to the Dark Ages. Humans then were beset by apparently insurmountable trials similar to our own. But with the rise of the printing press, we could store and share our ideas in ways never before possible. Print allowed the rise of institutions beyond the imagination of one country, much less one person: global trade, and then the Enlightenment, which gave us democracy and modern science itself. 
There's reason to hope that such co-evolution is occurring again. 
In fact, the signs that humanity can make good use of technology to thwart the bad abound. On 9-11, the fourth plane didn't make it to its target. Why? Because the Air Force was so quick? No. It was because dozens of ordinary people on that plane -- empowered by mobile phone technology --in less than an hour analyzed, diagnosed and cured their society's problem, at the greatest cost to themselves. 
150th birthdays 
Still, we have many questions with which to grapple. 
Already we see people living much longer, healthier lives than in the past. Researchers at the National Institutes of Health think that the first human to robustly live 150 years is already alive today. What will it mean to marriage and parenting -- not to mention Social Security -- if this curve of increased vigor continues? 
What will it mean to your family if your kids are greatly more capable mentally than you are? Did any fathers ask their daughters to fix the Model T, the way we now routinely hand a balky cell phone to our offspring? The relationship between who learns and who teaches, which has been stable for millenniums, has been upturned: "You looked at the previous generation to learn how to live yourself. That's no longer possible," explains anthropologist Mary Catherine Bateson. 
And what does it mean to the institutions we cherish --like democracy -- if it is no longer clear that all men and women are created equal? 
The measure of success in the "prevail" scenario that I hope for would not be how many transistors we can get to talk to each other. That way merely leads to the other two scenarios, in which our future is technologically determined and we are just along for the ride. Rather, "prevailing" in the midst of such radical evolution would be measured by the richness, depth, variety and ease of the most important connections -- the ones among unpredictably clever humans like ourselves. 
Joel Garreau, a Washington Post reporter and editor, is the author of "Radical Evolution: The Promise and Peril of Enhancing Our Minds, Our Bodies - and What It Means to Be Human." 






