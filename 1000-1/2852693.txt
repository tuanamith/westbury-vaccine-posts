


On Sun, 18 Sep 2005 09:44:00 -0700, "Andy S." < <EMAILADDRESS> > wrote: 

Well, I did the one who had diabetes. Insulin injections every day, counted out dry food, fed at the same time every day.  
A friend has doing the saline in her cat every few days, to keep it flushed. The cat hated it. 
I decided not to put my cats through that.  
I have this thing about not wanting to give them a great life, then making them go through bad stuff at the end. 
I'll do anything to try to save them, for example Brady's surgery was $2,000 and it bought me two more months. 
If an animal is terminal I don't try to keep them going as long as possible.... I want to say goodbye before the bad stuff. 
My last cat, Rowdy, who died of vaccine site sarcoma, was going into kidney failure.  I changed his food to Prescription Diet KD and it slowed the kidney problem down.   
I put him down because of the cancer, not the kidney. 
Many people think when cats purr they are happy, I don't. I think they purr when they have needs. Attention and being happy is a need so they purr. 
They also purr when they are in pain.  
I don't take the chance that mine are in pain.  One way to tell it's time is their breath smells like ammonia. 
I'd rather we a week too early, than an hour too late. 
I'm sorry about your kitty.  Ask the vet about Prescription Diet. It's pricey, but good stuff. 
Hunter 

-- 
 <URL>  

