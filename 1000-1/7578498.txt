


Via NY Transfer News Collective  *  All the News that Doesn't Fit   Prensa Latina, Havana  <URL>  
US Blockade of Cuba Affects Other Countries 
Havana, Oct 12 (Prensa Latina) Washington's claims -- that its blockade against Cuba is a bilateral affair -- crumble as nations, companies and individual businesspeople from countries other than the  US face increasing hurdles in trying to do business with the island. 
In 2004, about 77 companies, banks and non-governmental organizations worldwide were fined $1.2 million for actions considered by United States a violation of the blockade. 
Of those entities fined, 11 are foreign enterprises or subsidiaries by US companies settled in Mexico, Canada, Panama, Italy, United Kingdom, Uruguay and Bahamas. 
Organizations like Iberia, Alitalia, Air Jamaica, Daewoo and the Bank of China were sanctioned because their branch offices in the US violated the embargo. 
Alpha Pharmaceutical Inc., ICN Farmaceutica S.A. and Laboratorios Grossman S.A. settled in Panama and Mexico received fines valued at $198,000. 
Other companies also affected were the Mexican Trinity Industries and Chiron Corporation Ltd., entities that in the name of Chiron S.A. and Chiron Behring GMBH, from California, had to pay $168,500 for exporting vaccines to Cuba from 1999 to 2002. 
Likewise, Daewoo Heavy Industries America Corporation, with headquarters in Georgia, United states, was fined with $55,000 for exporting goods to Cuba in 1999. 
This year, the company Martinair Holland, based in Netherlands, was also fined for traveling to Cuba without a US license. 
Regulations of the economic, trade and financial war against the Caribbean island forbid US companies settled in other countries to carry out any transaction with Cuban enterprises. 
Third-country companies are also impeded from exporting US any product with Cuban raw materials. 
Furthermore, Washington prohibits Havana from selling goods or services with US technology or those manufactured in that northern nation, even when owners of those companies are from other countries. 
To oblige other countries to be part of the blockade, the US government impedes the entrance of ships that have transported goods to or from Cuba, as well as prohibits foreign banks from opening accounts in US dollars or making financial transactions in that currency with Cuban people or legal entities. 
The document "The Need to End the US Economic, Trade and Financial Blockade of Cuba" will be presented for the fourteenth time at the UN General Assembly [TODAY?]. A year ago, 179 countries voted for, four against and one abstention. 
mh/iff/abm/mf                                         * ================================================================ .NY Transfer News Collective    *    A Service of Blythe Systems .          Since 1985 - Information for the Rest of Us         . .339 Lafayette St., New York, NY 10012      <URL>  .List Archives:   https://olm.blythe-systems.com/pipermail/nytr/ .Subscribe: https://olm.blythe-systems.com/mailman/listinfo/nytr ================================================================ 

