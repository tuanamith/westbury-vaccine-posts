


On Sat, 08 Oct 2005 14:38:46 -0700, Winston Smith < <EMAILADDRESS> > wrote: 

Please pardon my ignorance.  I haven't been following the bird flu business much.  AIUI, the bird flu spreads among birds and it's possible for humans to get it from an infected bird, but not from an infected human.  Is that right?  So, will it mutate into a virus that can use an infected human to infect other humans?  What are the chances of that happening? 

If it mutates into a human-human virus, will it still also be a bird-human virus?  And just how easily is it transmitted from an infected bird to a human? 
I suppose the reason there is no vaccine for it now is that it is not (yet) a human-human virus, and they really can't make a vaccine for a virus that hasn't yet mutated into the form it would need to be human-human. 
-- Robert Sturgeon Summum ius summa inuria.  <URL> / 

