


On 9/10/05 15:20, in article  <EMAILADDRESS> , "Runge" < <EMAILADDRESS> > wrote: 
Since you keep posting, your interest must be keen. 
Here is some more help for you. 
Earl 
***** 
   MARKET VIEWS  From S&P By Pearl Wang  

Avian Flu: Inoculate Your Portfolio 
If the worst happens -- a global outbreak -- it could hit travel and leisure industries but boost gold, Treasuries, and some drug stocks Worries grew about an outbreak of the avian flu after scientists revealed this week that the 1918 Spanish flu epidemic that killed 50 million people started in birds and then mutated and spread to humans. At an Oct. 4 press conference, President Bush said any part of the U.S. where the virus breaks out could face a quarantine, potentially enforced by the military. The U.S. Senate has already approved a $3.9 billion package to buy vaccines and antiviral medications, and the Administration is also preparing a request for an additional $6 billion to $10 billion. 
"It could mean a substantial reduction of global trade," says David Braverman, vice-president of Standard & Poor's portfolio services group. "That could lead to new energy shortages and affect a wide variety of imports and exports, particularly manufactured goods from the Pacific Basin."  
"ALMOST CERTAIN."   Although the U.S. government is preparing for a potential avian-flu outbreak, it's uncertain how other countries plan to address H5N1, a type of Avian influenza. H5N1 has killed more than half of those infected since late 2003. The World Health Organization (WHO) has placed H5N1 in Phase 3 of its 6-phase Pandemic Watch. 
The effect of a H5N1 pandemic on the global markets will depend on how well the disease is contained. If H5N1 were confined to rural regions in Asia, it would have little global economic impact. If it spread to major cities in Asia, however, it could turn into something worse than the 2003 SARS virus outbreak, which infected approximately 8,000 people and killed roughly 775, according to WHO.  
VULNERABLE SECTORS.   Health-care officials predict that an H5N1 pandemic would likely hit the most economically productive age group -- those people between 20 and 40 -- the hardest. The financial impact could end up greater than that of SARS, which mainly confined itself to Toronto, Hong Kong, Singapore, and parts of China. 
Casinos and hotels would likely experience lower demand depending on their location. S&P equity analyst Tom Graves points to the second quarter of 2003, when room demand in Toronto suffered due to fear of SARS, he believes. The outbreak also hurt Starwood Hotels & Resorts ( HOT ; ranked 3 STARS, or hold; recent price: $58), which owns two Sheraton hotels in the Canadian city.  
FINANCIAL HAVENS.   Graves also notes that cruise-ship operators and sporting events would lose business if people avoided enclosed places where large groups of people are typically in close proximity. However, in recent years, outbreaks of viral illness on cruise ships don't seem to have had a major negative impact on longer-term industry demand, Graves says. 
S&P's senior media and entertainment equity analyst, Tuna Amobi, says the SARs outbreak also adversely affected attendance at Disney's ( DIS ; 4 STARS, or buy; $24) theme parks. The financial performance of major film studios, as well as exhibitors such as Regal Entertainment ( RGC ; 3 STARS; $20), will likely experience attendance declines, he says. 
STOCKPILING VACCINE.   Some pharmaceutical companies may benefit from the need for an effective vaccine for avian flu. The National Institutes of Health is funding research by Sanofi-Pasteur, the vaccines branch of Sanofi-Aventis ( SNY ; 3 STARS; $42) and Chiron ( CHIR ; 3 STARS; $43). 
EQUIPMENT TO GAIN.   S&P analyst Ken Leon says increased telecommuting would benefit IP service providers based in Asia, perhaps along with Skype Communications, which has agreed to be acquired by eBay ( EBAY ; 3 STARS; $39), and traditional wireline and wireless carriers. Cable-TV companies offering home connections via broadband could also benefit as service providers.  
Equipment suppliers that may gain additional revenue from increased networking include Huwei Technologies and giant gearmaker Cisco Systems ( CSCO ; 4 STARS; $18). Ditto for Alcatel ( ALA ; 3 STARS; $13), the industry's leader in broadband access equipment, which has a strong presence in Asia and has been in China for many years. Another equipment company with relationships throughout Asia, Lucent Technologies ( LU ; 3 STARS; $3), may see business increase. 
Of course, predicting the precise impact of a potential avian-flu pandemic on the global markets presents a difficult task. 
The SARS outbreak was controlled relatively quickly, within months. If global governments can decrease exposure to infected animals, strengthen an early-warning system, and contain the spread of disease at the source, they can likely avert a potential avian-flu pandemic.  


