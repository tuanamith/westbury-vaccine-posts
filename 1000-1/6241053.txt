


 Bush Plan Shows US is Not Ready for Deadly Flu           By Gardiner Harris          Friday 07 October 2005  
    Washington - A plan developed by the Bush administration to deal with any possible outbreak of pandemic flu shows that the United States is woefully unprepared for what could become the worst disaster in the nation's history.  
    A draft of the final plan, which has been years in the making and is expected to be released later this month, says that a large outbreak that began in Asia was likely, because of modern travel patterns, to reach the United States within "a few months or even weeks."  
    If such an outbreak occurred, hospitals would become overwhelmed; riots would engulf vaccination clinics; and even power and food would be in short supply, according to the plan, which was obtained by The New York Times.  
    The 381-page plan calls for quarantine and travel restrictions but concedes that such measures "are unlikely to delay introduction of pandemic disease into the US by more than a month or two."  
    The plan's 10 supplements suggest specific ways that local and state governments should prepare now for an eventual pandemic by, for instance, drafting legal documents that would justify quarantines. Written by health officials, the plan does yet address responses by the military or other governmental departments.  
    The plan outlines a worst-case scenario in which more than 1.9 million Americans would die and 8.5 million would be hospitalized with costs exceeding $450 billion.  
    It also calls for a domestic vaccine production capacity of 600 million doses within six months, more than 10 times the present capacity.  
    On Friday, President Bush asked the leaders of the nation's top six vaccine producers to the White House to cajole them into increasing their domestic vaccine capacity, and the flu plan demonstrates just how monumental a task these companies have before them.  
    In the wake of Hurricane Katrina, the Bush administration's efforts to plan for a possible pandemic flu have become controversial, with many Democrats in Congress charging that the administration has not done enough. Many have pointed to the lengthy writing process of the flu plan as evidence of this.  
    But while the administration's flu plan, officially called the Pandemic Influenza Strategic Plan, closely outlines how the Health and Human Services Department may react during a pandemic, it skirts many essential decisions, like how the military may be deployed.  
    "The real shortcoming of the plan is that it doesn't say who's in charge," said a top health official who provided the plan to The Times. "We don't want to have a FEMA-like response, where it's not clear who's running what."  


