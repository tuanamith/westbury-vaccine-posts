


On Sat, 08 Oct 2005 16:00:04 GMT,  <EMAILADDRESS>  (Kurt Ullman) wrote: 

That's incorrect: there have been two species-jumping pandemics since the 1918 epidemic, the Asian flu of 1957-58 and the Hong Kong flu of 1968-69. The reason those pandemics weren't as lethal as the 1918 flu is that they lacked the genes that attack lung tissue. Unfortunately, the current Avian flu does have those genes: 
 <URL>  

1. We knew only a tiny fraction then of what we know now. We didn't know how flu pandemics begin. We had no samples of the 1918 flu. We had no ability to sequence genes. Epidemiologists made a call on the basis of the information and understanding they had at the time. 
2. If you're unwilling to take the risk that some disaster preparations won't be used, you'll be up to your nose in water when the levees fail. And here's the thing: 
	After Delay, U.S. Faces Line for Flu Drug  
By GARDINER HARRIS Published: October 7, 2005 
 <URL>  
President Ford was willing to accept the political risk of strong action that might be proven unnecessary, and to use the best means at his disposal to get. By way of contrast, this administration is repeating the mistakes it made last year when we were left without flu vaccine, the same mistakes it made in New Orleans. To whit: 
1. Appointing poorly-qualified cronies to run agencies and punishing those who question policies 
2. Failure to make adequate plans and preparations for and commit adequate resources to high-risk contingencies 
3. An ideologically motivated failure to take firm Federal action when such contingencies are identified 

That, unfortunately, is the course they took with the levees. The prospect of a bird flu epidemic doesn't call for hysteria, but neither should we ignore the huffing, puffing wolf. 
--  Josh 
"It was amazing I won. I was running against peace and prosperity and incumbency." - George W. Bush 

