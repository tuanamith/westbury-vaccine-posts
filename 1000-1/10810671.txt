


Via NY Transfer News Collective  *  All the News that Doesn't Fit   The Irish Times - Oct 14, 2005  <URL>  
EU warning over danger of avian flu pandemic 
The European Commission yesterday warned member states of a potential avian flu pandemic, and urged EU governments to stockpile anti-viral drugs and prepare to vaccinate people at high risk. 
A Department of Health spokesman said in Dublin last night that 400,000 units of the newly developed H5N1 vaccine had been ordered, but would not be available until well into next year, write Jamie Smyth, in Brussels, and Sean Mac Connell 
He said this would be stockpiled in the event of the danger of an outbreak of the human form of the disease and would be given to essential workers. 
He said 600,000 units of an anti-viral drug which could be used to ease the symptoms of flu were already in stock and an additional 400,000 units of the drug, Tamiflu, would be imported early next year. 
This could cover 25 per cent of the population in the State. 
Many other plans had been prepared in the event of the human form of the flu emerging here. But he stressed the World Health Organisation had said there was a low risk of human to human spread at this stage. 
Dr Darina O'Flanagan, director of the Health Protection Surveillance Centre, said a stockpile of drugs was in place to cope with any outbreak of the disease. "We do have enough anti-virals here to cope, should there be an outbreak of avian influenza." 
She said those travelling to areas of Europe and Asia where the disease has been found in birds, should avoid direct contact with birds and ensure that food from infected areas was thoroughly cooked. 
Minister for Agriculture and Food Mary Coughlan announced yesterday that additional staff will be deployed at Dublin airport; she has asked for additional resources for Customs and Excise staff to prevent the disease arriving here. 
In addition, Ms Coughlan said she was drawing up an order which will require poultry flock owners to maintain records of poultry mortalities, egg production and food and water intake, and the reporting of such details to the department. 
EU health commissioner Markos Kyprianou issued his stark warning as experts confirmed that the most dangerous variant of bird flu, the H5N1 strain, had been identified in an outbreak in Turkey. 
He called on member states to prepare emergency plans for a potential flu pandemic if the virus spread to the rest of Europe. 
"What is important is that it does become a priority for all member states and that they make an investment for preparing for this event," said Mr Kyprianou. 
He advocated an increase in influenza vaccinations for those most at risk from flu, typically people over 65, young children and those with weak respiratory systems. 

