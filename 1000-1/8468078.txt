


Joe Avalon wrote: 

*No Nation Is Ready For Bird Flu Outbreak* 
WASHINGTON -- The Bush administration's top health official said  Thursday that *no one in the world is ready* for a potentially  catastrophic outbreak of bird flu as President Bush summoned vaccine  manufacturers to the White House to discuss the situation. 
But Health and Human Services Secretary Mike Leavitt also said that U.S.  officials and their counterparts around the globe recognize that a  pandemic is possible and are working hard on ways to protect people from it. 
"The good news is, we do have a vaccine," Leavitt said on CBS's "The  Early Show." But he cautioned that officials do not currently have an  ability to mass produce it or get it to people quickly. 
"It's enough of a possibility that it demands our attention," he said.  "We have to be prepared all the time ... for that type of problem and we  need to improve." 
Bush was meeting with top advisers Thursday on the matter and will meet  on Friday with representatives from U.S. companies and some foreign  vaccine manufacturers, said White House spokesman Scott McClellan. 
"We want to press ahead to expand our manufacturing capacity to address  this risk," McClellan said. 
Outlining the pandemic plan in an interview Wednesday with The  Associated Press, Leavitt said U.S. health officials would rush overseas  to wherever a bird flu outbreak occurred and work with local officials  to try to contain it. 
"If you can get there fast enough and apply good public health  techniques of isolating and quarantining and medicating and vaccinating  the people in that area, you can ... squelch it or you can delay it,"  Leavitt said in an interview with The Associated Press. 
Leavitt is traveling to Asia to shore up international cooperation  should bird flu mutate to easily infect people. 
To further that goal, more than 65 countries and international  organizations were to participate in discussions Thursday at the State  Department about preparations for the possibility of worsening bird flu. 
Next week, Leavitt plans to meet with leaders of the Southeast Asia  countries that are the epicenter of the virus. 
There have been three flu pandemics in the last century; the worst, in  1918, killed as many as 50 million people worldwide. 
Scientists say it is only a matter of time before the next worldwide  influenza outbreak. Concern is rising that it could be triggered by the  avian flu called H5N1. 
That virus has killed or led to the slaughter of millions of birds,  mostly in Asia, but also in parts of Europe. It has killed about 60  people, mostly poultry workers, because so far the virus does not spread  easily from person to person. 
The fear is that it will mutate to spread easily, a catastrophe because  H5N1 is so different from annual flu strains that people have no natural  immunity. 
"The probability that the H5N1 virus will create a pandemic is  uncertain. The signs are worrisome," Leavitt said. He added that the  updated pandemic plan, due this month, envisions other super-strains of  flu, too. 
Role-playing different outbreak possibilities over the past few months  led federal health officials to broaden their focus on how to detect a  bird-flu mutation in another country and quickly send overseas help. 
If that fails, the pandemic plans' first draft last year called for  closing schools, restricting travel and other old-fashioned quarantine  steps, depending on how fast the super-strain was spreading and its  virulence. Those steps are getting renewed attention after President  Bush's comments Tuesday that troops might have to be dispatched to  enforce a mass quarantine. 
Typically, state and local authorities deal with quarantine decisions --  isolating the sick and closing large gatherings where diseases might spread. 
"They have to be prepared, and frankly they're not," Leavitt said. 
The updated plan will outline when federal health officials will take  over for the locals, something that will depend on how the flu is  spreading, he said. For instance, mass quarantines were needed in 1918,  but not during the pandemics of 1957 and 1968, he said. 
As for treatment, HHS last month began spending $100 million for the  first large-scale production of a bird flu vaccine. But the department  has been criticized for only stockpiling enough of the anti-flu drug  Tamiflu for several million people. The Senate last week passed  legislation that would increase those purchases by $3 billion. 
A bigger gap is how to create quickly a vaccine to match whatever  pandemic flu strain erupts, Leavitt said. That currently takes months.  The new plan will focus on rejuvenating vaccine production to speed the  process, he said. 

