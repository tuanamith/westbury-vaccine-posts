


On Fri, 07 Oct 2005 08:45:09 -0600, Dover Beach < <EMAILADDRESS> > wrote: 


1957 was called the "Asian Flu," and the 1968 was called the "Hong Kong Flu." See any similarities yet? 
What makes these things take off like a rocket is a cascade of predictable and unpredictable events. Sometime the planets all align and we get pandemics. 
Boron 

 <URL>  Asian flu: A pandemic of influenza A (H2N2) in 1957-58. First identified in China in late February 1957, the Asian flu spread to the United States by June 1957 where it caused about 70,000 deaths. Also known as Asian influenza.  
Immunity to this strain of influenza A (H2N2) was rare in people less than 65 years of age, and a pandemic was predicted. In preparation, vaccine production began in late May 1957, and health officials increased surveillance for flu outbreaks. Unlike the virus that caused the 1918 pandemic, the 1957 pandemic virus was quickly identified, due to advances in scientific technology. Vaccine was available in limited supply by August 1957.  
The virus came to the United States quietly, with a series of small outbreaks over the summer of 1957. When children went back to school in the fall, they spread the disease in classrooms and brought it home to their families. Infection rates were highest among school children, young adults, and pregnant women in October 1957. Most influenza-and pneumonia-related deaths occurred between September 1957 and March 1958. The elderly had the highest rates of death. By December 1957, the worst seemed to be over.  
However, during January and February 1958, there was another wave of illness among the elderly. This is an example of the potential "second wave" of infections that can develop during a pandemic. The disease infects one group of people first, infections appear to decrease and then infections increase in a different part of the population. 

 <URL>  Hong Kong flu: A pandemic of influenza A (H3N2) in 1968-69. This virus was first detected in Hong Kong in early 1968 and spread to the United States later that year. where it caused about 34,000 deaths, making it the mildest pandemic in the 20th century. Also known as Hong Kong influenza.  
There could be several reasons why fewer people in the US died due to this virus. First, the Hong Kong flu virus was similar in some ways to the Asian flu virus that circulated between 1957 and 1968. Earlier infections by the Asian flu virus might have provided some immunity against the Hong Kong flu virus that may have helped to reduce the severity of illness during the Hong Kong pandemic.  
Second, instead of peaking in September or October, like pandemic influenza had in the previous two pandemics, this pandemic did not gain momentum until near the school holidays in December. Since children were at home and did not infect one another at school, the rate of influenza illness among schoolchildren and their families declined.  
Third, improved medical care and antibiotics that are more effective for secondary bacterial infections were available for those who became ill. 



